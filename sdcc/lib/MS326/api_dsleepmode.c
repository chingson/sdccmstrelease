#include "ms326sphlib.h"

void api_enter_dsleep_mode(void)
{
    //SYSC=0x25; // div2!!
    PAWKDR=0;
    PAWK=0;
    PBWK=0;
    PBWKDR=0;
    DUMMYC=0x20;
    LVRCON=0x10;
    LVRCON=0x18;
    TIMERC=0;
    GIF=0;
    SYSC&=0xfd;
    while(SYSC&0x2); // skcmd clear
    ADCON=0; // force reset
    SYSC2&=0x7f; // mbias off
    PAG=0;
    LVDCON=0;
    SYSC=0x41;

}

