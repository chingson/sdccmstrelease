;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_modslong.c"
	.module _modslong
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$_modslong$0$0({2}DF,SL:S),C,0,0
	;--cdb--F:G$_modslong$0$0({2}DF,SL:S),C,0,0,0,0,0
	;--cdb--S:G$_modulong$0$0({2}DF,SL:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _modslong-code 
.globl __modslong

;--------------------------------------------------------
	.FUNC __modslong:$PNUM 8:$C:__modulong\
:$L:r0x1162:$L:__modslong_STK00:$L:__modslong_STK01:$L:__modslong_STK02:$L:__modslong_STK03\
:$L:__modslong_STK04:$L:__modslong_STK05:$L:__modslong_STK06:$L:r0x1167:$L:r0x1168\
:$L:r0x1169:$L:r0x116A:$L:r0x116B
;--------------------------------------------------------
;	.line	3; "_modslong.c"	_modslong (long a, long b)
__modslong:	;Function start
	STA	r0x1162
;	;.line	7; "_modslong.c"	r = (unsigned long)(a < 0 ? -a : a) % (unsigned long)(b < 0 ? -b : b);
	XOR	#0x80
	ROL	
	CLRA	
	ROL	
	XOR	#0x01
	STA	r0x1167
	JZ	_00110_DS_
	SETB	_C
	CLRA	
	SUBB	__modslong_STK02
	STA	r0x1168
	CLRA	
	SUBB	__modslong_STK01
	STA	r0x1169
	CLRA	
	SUBB	__modslong_STK00
	STA	r0x116A
	CLRA	
	SUBB	r0x1162
	STA	r0x116B
	JMP	_00111_DS_
_00110_DS_:
	LDA	__modslong_STK02
	STA	r0x1168
	LDA	__modslong_STK01
	STA	r0x1169
	LDA	__modslong_STK00
	STA	r0x116A
	LDA	r0x1162
	STA	r0x116B
_00111_DS_:
	LDA	r0x1168
	STA	__modslong_STK02
	LDA	r0x1169
	STA	__modslong_STK01
	LDA	r0x116A
	STA	__modslong_STK00
	LDA	r0x116B
	STA	r0x1162
	LDA	__modslong_STK03
	JPL	_00112_DS_
	SETB	_C
	CLRA	
	SUBB	__modslong_STK06
	STA	r0x1168
	CLRA	
	SUBB	__modslong_STK05
	STA	r0x1169
	CLRA	
	SUBB	__modslong_STK04
	STA	r0x116A
	CLRA	
	SUBB	__modslong_STK03
	STA	r0x116B
	JMP	_00113_DS_
_00112_DS_:
	LDA	__modslong_STK06
	STA	r0x1168
	LDA	__modslong_STK05
	STA	r0x1169
	LDA	__modslong_STK04
	STA	r0x116A
	LDA	__modslong_STK03
	STA	r0x116B
_00113_DS_:
	LDA	r0x1168
	STA	__modulong_STK06
	LDA	r0x1169
	STA	__modulong_STK05
	LDA	r0x116A
	STA	__modulong_STK04
	LDA	r0x116B
	STA	__modulong_STK03
	LDA	__modslong_STK02
	STA	__modulong_STK02
	LDA	__modslong_STK01
	STA	__modulong_STK01
	LDA	__modslong_STK00
	STA	__modulong_STK00
	LDA	r0x1162
	CALL	__modulong
	STA	r0x1162
;	;.line	9; "_modslong.c"	if (a < 0)
	LDA	r0x1167
	JZ	_00106_DS_
;	;.line	10; "_modslong.c"	return -r;
	SETB	_C
	CLRA	
	SUBB	STK02
	STA	r0x1167
	CLRA	
	SUBB	STK01
	STA	__modslong_STK06
	CLRA	
	SUBB	STK00
	STA	__modslong_STK05
	CLRA	
	SUBB	r0x1162
	STA	__modslong_STK04
	LDA	r0x1167
	STA	STK02
	LDA	__modslong_STK06
	STA	STK01
	LDA	__modslong_STK05
	STA	STK00
	LDA	__modslong_STK04
	JMP	_00108_DS_
_00106_DS_:
;	;.line	12; "_modslong.c"	return r;
	LDA	r0x1162
_00108_DS_:
;	;.line	13; "_modslong.c"	}
	RET	
; exit point of __modslong
	.ENDFUNC __modslong
	;--cdb--S:G$_modslong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$_modulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:L_modslong._modslong$b$65536$1({4}SL:S),R,0,0,[__modslong_STK06,__modslong_STK05,__modslong_STK04,__modslong_STK03]
	;--cdb--S:L_modslong._modslong$a$65536$1({4}SL:S),R,0,0,[__modslong_STK02,__modslong_STK01,__modslong_STK00,r0x1162]
	;--cdb--S:L_modslong._modslong$r$65536$2({4}SL:S),R,0,0,[__modslong_STK02,__modslong_STK01,__modslong_STK00,r0x1162]
	;--cdb--S:G$_modslong$0$0({2}DF,SL:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__modulong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__modslong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__modslong_0	udata
r0x1162:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__modslong_STK00:	.ds	1
	.globl __modslong_STK00
__modslong_STK01:	.ds	1
	.globl __modslong_STK01
__modslong_STK02:	.ds	1
	.globl __modslong_STK02
__modslong_STK03:	.ds	1
	.globl __modslong_STK03
__modslong_STK04:	.ds	1
	.globl __modslong_STK04
__modslong_STK05:	.ds	1
	.globl __modslong_STK05
__modslong_STK06:	.ds	1
	.globl __modslong_STK06
	.globl __modulong_STK06
	.globl __modulong_STK05
	.globl __modulong_STK04
	.globl __modulong_STK03
	.globl __modulong_STK02
	.globl __modulong_STK01
	.globl __modulong_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:__modslong_STK00:NULL+0:14:0
	;--cdb--W:__modslong_STK01:NULL+0:13:0
	;--cdb--W:__modslong_STK02:NULL+0:12:0
	;--cdb--W:__modslong_STK03:NULL+0:4459:0
	;--cdb--W:__modslong_STK04:NULL+0:4458:0
	;--cdb--W:__modslong_STK05:NULL+0:4457:0
	;--cdb--W:__modslong_STK06:NULL+0:4456:0
	end
