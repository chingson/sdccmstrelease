;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"sinf.c"
	.module sinf
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fsinf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$sinf$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$sincosf$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; sinf-code 
.globl _sinf

;--------------------------------------------------------
	.FUNC _sinf:$PNUM 4:$C:_sincosf\
:$L:r0x1162:$L:_sinf_STK00:$L:_sinf_STK01:$L:_sinf_STK02
;--------------------------------------------------------
;	.line	36; "sinf.c"	float sinf(float x) _FLOAT_FUNC_REENTRANT
_sinf:	;Function start
	STA	r0x1162
;	;.line	38; "sinf.c"	if (x==0.0) return 0.0;
	LDA	_sinf_STK02
	ORA	_sinf_STK01
	ORA	_sinf_STK00
	ORA	r0x1162
	JNZ	_00106_DS_
	CLRA	
	STA	STK02
	STA	STK01
	STA	STK00
	JMP	_00107_DS_
_00106_DS_:
;	;.line	39; "sinf.c"	return sincosf(x, 0);
	CLRA	
	STA	_sincosf_STK03
	LDA	_sinf_STK02
	STA	_sincosf_STK02
	LDA	_sinf_STK01
	STA	_sincosf_STK01
	LDA	_sinf_STK00
	STA	_sincosf_STK00
	LDA	r0x1162
	CALL	_sincosf
_00107_DS_:
;	;.line	40; "sinf.c"	}
	RET	
; exit point of _sinf
	.ENDFUNC _sinf
	;--cdb--S:Lsinf.sinf$x$65536$26({4}SF:S),R,0,0,[_sinf_STK02,_sinf_STK01,_sinf_STK00,r0x1162]
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$sincosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_sincosf

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_sinf
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_sinf_0	udata
r0x1162:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_sinf_STK00:	.ds	1
	.globl _sinf_STK00
_sinf_STK01:	.ds	1
	.globl _sinf_STK01
_sinf_STK02:	.ds	1
	.globl _sinf_STK02
	.globl _sincosf_STK03
	.globl _sincosf_STK02
	.globl _sincosf_STK01
	.globl _sincosf_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:_sinf_STK00:NULL+0:14:0
	;--cdb--W:_sinf_STK01:NULL+0:13:0
	;--cdb--W:_sinf_STK02:NULL+0:12:0
	;--cdb--W:r0x1162:NULL+0:-1:1
	end
