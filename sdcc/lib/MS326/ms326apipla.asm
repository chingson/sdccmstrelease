;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.1 #3de0c6772 (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"C:\work\ms326sphlib"
;;	.file	"ms326apipla.c"
	.module ms326apipla
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fms326apipla$spiastru[({0}S:S$addrhm$0$0({2}SI:U),Z,0,0)({2}S:S$addrl$0$0({1}SC:U),Z,0,0)]
	;--cdb--T:Fms326apipla$adps[({0}S:S$predict$0$0({2}SI:S),Z,0,0)({2}S:S$index$0$0({1}SC:U),Z,0,0)]
	;--cdb--T:Fms326apipla$touchen[({0}S:S$toff$0$0({1}SC:U),Z,0,0)({1}S:S$nmossw$0$0({1}SC:U),Z,0,0)({2}S:S$period$0$0({2}SI:U),Z,0,0)({4}S:S$threshold$0$0({2}SI:U),Z,0,0)({6}S:S$count$0$0({2}SI:U),Z,0,0)]
	;--cdb--T:Fms326apipla$pwmleds[({0}S:S$period$0$0({1}SC:U),Z,0,0)({1}S:S$counter$0$0({1}SC:U),Z,0,0)({2}S:S$threshold$0$0({1}SC:U),Z,0,0)]
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$api_play_start$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_pre_erase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start_no_erase$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop_noerase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_noer$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_chspick$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_init$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_tk_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:Fms326apipla$get12halfpage$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:Fms326apipla$get12halfpage$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$api_decode_12bit$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$api_decode_12bit$0$0({2}DF,SC:U),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; ms326apipla-code 
.globl _api_play_start

;--------------------------------------------------------
	.FUNC _api_play_start:$PNUM 11:$C:_api_clear_filter_mem:$C:_api_decode_12bit\
:$L:r0x11CC:$L:_api_play_start_STK00:$L:_api_play_start_STK01:$L:_api_play_start_STK02:$L:_api_play_start_STK03\
:$L:_api_play_start_STK04:$L:_api_play_start_STK05:$L:_api_play_start_STK06:$L:_api_play_start_STK07:$L:_api_play_start_STK08\
:$L:_api_play_start_STK09
;--------------------------------------------------------
;	.line	126; "ms326apipla.c"	BYTE api_play_start(USHORT start_page, USHORT end_page, USHORT period, BYTE playmode, BYTE dacosr, BYTE dacgcl, BYTE *fifo)
_api_play_start:	;Function start
	STA	r0x11CC
;	;.line	128; "ms326apipla.c"	if(ADCON&1) // if ADC already enabled
	LDA	_ADCON
	SHR	
	JNC	_00181_DS_
;	;.line	129; "ms326apipla.c"	api_clear_filter_mem(1); // clear 700~77F
	LDA	#0x01
	CALL	_api_clear_filter_mem
	JMP	_00182_DS_
_00181_DS_:
;	;.line	131; "ms326apipla.c"	api_clear_filter_mem(0);
	CLRA	
	CALL	_api_clear_filter_mem
_00182_DS_:
;	;.line	132; "ms326apipla.c"	SYSC2|=0x08; // 128 byte wake up a time!!
	LDA	_SYSC2
	ORA	#0x08
	STA	_SYSC2
;	;.line	133; "ms326apipla.c"	api_play_startpage = start_page;
	LDA	_api_play_start_STK00
	STA	_api_play_startpage
;	;.line	134; "ms326apipla.c"	SPIH=start_page>>8;
	LDA	r0x11CC
	STA	(_api_play_startpage + 1)
	STA	_SPIH
;	;.line	135; "ms326apipla.c"	SPIM=start_page&0xff;
	LDA	_api_play_start_STK00
	STA	_SPIM
;	;.line	136; "ms326apipla.c"	SPIL=0;
	CLRA	
	STA	_SPIL
;	;.line	137; "ms326apipla.c"	api_play_endpage=end_page;
	LDA	_api_play_start_STK02
	STA	_api_play_endpage
	LDA	_api_play_start_STK01
	STA	(_api_play_endpage + 1)
;	;.line	138; "ms326apipla.c"	api_play_mode=playmode;
	LDA	_api_play_start_STK05
	STA	_api_play_mode
;	;.line	139; "ms326apipla.c"	DMA_IL=0; // use 1 as wakeup address
	CLRA	
	STA	_DMA_IL
;	;.line	140; "ms326apipla.c"	DACGCL=dacgcl;
	LDA	_api_play_start_STK07
	STA	_DACGCL
;	;.line	144; "ms326apipla.c"	if(playmode&API_PLAYMODE_12BIT)
	LDA	#0x02
	AND	_api_play_start_STK05
	STA	_api_play_start_STK00
	JZ	_00184_DS_
;	;.line	147; "ms326apipla.c"	api_play_spibuf=(((USHORT)fifo)&0x7ff)>>4; // unit is 16byte , usually this is 0x50
	LDA	_api_play_start_STK08
	AND	#0x07
	STA	_api_play_start_STK01
	LDA	_api_play_start_STK09
	SWA	
	AND	#0x0f
	STA	_api_play_start_STK07
	LDA	_api_play_start_STK01
	SWA	
	PUSH	
	AND	#0xf0
	ORA	_api_play_start_STK07
	STA	_api_play_start_STK07
	POP	
	AND	#0x0f
	LDA	_api_play_start_STK07
;	;.line	148; "ms326apipla.c"	api_play_fifostart=api_play_spibuf+8; //16*8=128
	STA	_api_play_spibuf
	ADD	#0x08
;	;.line	149; "ms326apipla.c"	api_play_fifoend=api_play_fifostart+15; // we use XF as the page addr 0x6f
	STA	_api_play_fifostart
	ADD	#0x0f
	STA	_api_play_fifoend
;	;.line	150; "ms326apipla.c"	api_play_spiind=0; // start from 0;
	CLRA	
	STA	_api_play_spiind
	JMP	_00185_DS_
_00184_DS_:
;	;.line	156; "ms326apipla.c"	api_play_fifostart=(((USHORT)fifo)&0x7ff)>>4; // unit is 16byte , usually this is 0x50
	LDA	#0x07
	AND	_api_play_start_STK08
	STA	_api_play_start_STK08
	LDA	_api_play_start_STK09
	SWA	
	AND	#0x0f
	STA	_api_play_start_STK02
	LDA	_api_play_start_STK08
	SWA	
	PUSH	
	AND	#0xf0
	ORA	_api_play_start_STK02
	STA	_api_play_start_STK02
	POP	
	AND	#0x0f
	LDA	_api_play_start_STK02
;	;.line	157; "ms326apipla.c"	api_play_fifoend=api_play_fifostart+15; // we use XF as the page addr 0x6f
	STA	_api_play_fifostart
	ADD	#0x0f
	STA	_api_play_fifoend
_00185_DS_:
;	;.line	159; "ms326apipla.c"	ADP_IND=0x80;// following is 8 bit wide
	LDA	#0x80
	STA	_ADP_IND
;	;.line	160; "ms326apipla.c"	PPAGES=(api_play_fifostart&0xf)|((api_play_fifoend&0xf)<<4); // f0
	LDA	_api_play_fifostart
	AND	#0x0f
	STA	_api_play_start_STK02
	LDA	_api_play_fifoend
	AND	#0x0f
	SWA	
	AND	#0xf0
	ORA	_api_play_start_STK02
	STA	_PPAGES
;	;.line	161; "ms326apipla.c"	ADP_IND=0;// following is 6 bits only
	CLRA	
	STA	_ADP_IND
;	;.line	162; "ms326apipla.c"	PPAGES=(api_play_fifostart>>4)|((api_play_fifoend&0xf0)>>1);// 35
	LDA	_api_play_fifostart
	SWA	
	AND	#0x0f
	STA	_api_play_start_STK02
	LDA	#0xf0
	AND	_api_play_fifoend
	STA	_api_play_start_STK07
	CLRA	
	SHRS	
	LDA	_api_play_start_STK07
	ROR	
	ORA	_api_play_start_STK02
	STA	_PPAGES
;	;.line	164; "ms326apipla.c"	if(!dacosr)
	LDA	_api_play_start_STK06
	JNZ	_00187_DS_
;	;.line	165; "ms326apipla.c"	RCLKDIV|=0x80;
	LDA	_RCLKDIV
	ORA	#0x80
	STA	_RCLKDIV
	JMP	_00188_DS_
_00187_DS_:
;	;.line	167; "ms326apipla.c"	RCLKDIV&=0x7f;
	LDA	#0x7f
	AND	_RCLKDIV
	STA	_RCLKDIV
_00188_DS_:
;	;.line	169; "ms326apipla.c"	SPIOP=0x10; // clear end-code
	LDA	#0x10
	STA	_SPIOP
;	;.line	170; "ms326apipla.c"	PDMAH=0x80;
	LDA	#0x80
	STA	_PDMAH
;	;.line	171; "ms326apipla.c"	ULAWC&=0xdf; // no touch record settings!!
	LDA	#0xdf
	AND	_ULAWC
	STA	_ULAWC
;	;.line	172; "ms326apipla.c"	if(playmode&API_PLAYMODE_NOF)
	LDA	#0x08
	AND	_api_play_start_STK05
	STA	_api_play_start_STK02
	JZ	_00190_DS_
;	;.line	173; "ms326apipla.c"	period<<=2;
	LDA	_api_play_start_STK04
	SHL	
	STA	_api_play_start_STK04
	LDA	_api_play_start_STK03
	ROL	
	STA	_api_play_start_STK03
	LDA	_api_play_start_STK04
	SHL	
	STA	_api_play_start_STK04
	LDA	_api_play_start_STK03
	ROL	
	STA	_api_play_start_STK03
_00190_DS_:
;	;.line	174; "ms326apipla.c"	DAC_PH=period>>8;
	LDA	_api_play_start_STK03
	STA	_DAC_PH
;	;.line	175; "ms326apipla.c"	if(!(period&0x10))
	LDA	_api_play_start_STK04
	AND	#0x10
	JNZ	_00192_DS_
;	;.line	176; "ms326apipla.c"	period-=32;
	LDA	#0xe0
	ADD	_api_play_start_STK04
	STA	_api_play_start_STK04
	LDA	#0xff
	ADDC	_api_play_start_STK03
_00192_DS_:
;	;.line	178; "ms326apipla.c"	DAC_PL=(period&0xff)|0x1F; // sync to pwm freq
	LDA	_api_play_start_STK04
	ORA	#0x1f
	STA	_DAC_PL
;	;.line	180; "ms326apipla.c"	api_play_rampage=api_play_fifostart;
	LDA	_api_play_fifostart
	STA	_api_play_rampage
;	;.line	182; "ms326apipla.c"	if(!(playmode&API_PLAYMODE_12BIT))
	LDA	_api_play_start_STK00
	JNZ	_00194_DS_
;	;.line	184; "ms326apipla.c"	SPIOPRAH=api_play_rampage>>3; // initially, read 256 byes
	LDA	_api_play_rampage
	SHR	
	SHR	
	SHR	
	STA	_SPIOPRAH
;	;.line	185; "ms326apipla.c"	SPIOP=0x08; // read page to 0x2xx
	LDA	#0x08
	STA	_SPIOP
;	;.line	186; "ms326apipla.c"	SPIOP=0x20; // address increase a page
	LDA	#0x20
	STA	_SPIOP
	JMP	_00195_DS_
_00194_DS_:
;	;.line	190; "ms326apipla.c"	SPIOPRAH=api_play_spibuf>>3;
	LDA	_api_play_spibuf
	SHR	
	SHR	
	SHR	
	STA	_SPIOPRAH
;	;.line	191; "ms326apipla.c"	SPIOP=0x48; // initially, read half page
	LDA	#0x48
	STA	_SPIOP
;	;.line	192; "ms326apipla.c"	SPIL=0x80;
	LDA	#0x80
	STA	_SPIL
_00195_DS_:
;	;.line	194; "ms326apipla.c"	if(SPIOP&0x80)
	LDA	_SPIOP
	JPL	_00197_DS_
;	;.line	196; "ms326apipla.c"	return 0;
	CLRA	
	JMP	_00213_DS_
_00197_DS_:
;	;.line	198; "ms326apipla.c"	if(playmode&API_PLAYMODE_ULAW)
	LDA	_api_play_start_STK05
	SHR	
	JNC	_00202_DS_
;	;.line	200; "ms326apipla.c"	U2LSH=0;
	CLRA	
	STA	_U2LSH
;	;.line	201; "ms326apipla.c"	ULAWC|=0xA0;
	LDA	_ULAWC
	ORA	#0xa0
	STA	_ULAWC
	JMP	_00203_DS_
_00202_DS_:
;	;.line	202; "ms326apipla.c"	} else if(playmode & API_PLAYMODE_12BIT)
	LDA	_api_play_start_STK00
	JZ	_00199_DS_
;	;.line	204; "ms326apipla.c"	api_decode_12bit(api_play_rampage);// unit is 16
	LDA	_api_play_rampage
	CALL	_api_decode_12bit
;	;.line	205; "ms326apipla.c"	api_decode_12bit(api_play_rampage+8); // decode 2 half pages
	LDA	_api_play_rampage
	ADD	#0x08
	CALL	_api_decode_12bit
	JMP	_00203_DS_
_00199_DS_:
;	;.line	207; "ms326apipla.c"	DACON=0x20; // bit 5 is adpcm enable
	LDA	#0x20
	STA	_DACON
_00203_DS_:
;	;.line	208; "ms326apipla.c"	if(playmode&API_PLAYMODE_NOF) // skip filter
	LDA	_api_play_start_STK02
	JZ	_00205_DS_
;	;.line	209; "ms326apipla.c"	DACON|=0x1E;
	LDA	_DACON
	ORA	#0x1e
	STA	_DACON
	JMP	_00206_DS_
_00205_DS_:
;	;.line	211; "ms326apipla.c"	DACON|=0x1a;
	LDA	_DACON
	ORA	#0x1a
	STA	_DACON
_00206_DS_:
;	;.line	212; "ms326apipla.c"	api_play_rampage=api_play_fifostart; // change it back
	LDA	_api_play_fifostart
	STA	_api_play_rampage
_00207_DS_:
;	;.line	214; "ms326apipla.c"	while(PDMAL==0);
	LDA	_PDMAL
	JZ	_00207_DS_
;	;.line	215; "ms326apipla.c"	GIF=0xbf; // clear all pending flags
	LDA	#0xbf
	STA	_GIF
;	;.line	216; "ms326apipla.c"	if(playmode&API_PLAYMODE_PA7)
	LDA	_api_play_start_STK05
	AND	#0x10
	JZ	_00211_DS_
;	;.line	219; "ms326apipla.c"	SPKC|=0x48; ;// PA7 out
	LDA	_SPKC
	ORA	#0x48
	STA	_SPKC
;	;.line	220; "ms326apipla.c"	PADIR|=0x80;
	LDA	_PADIR
	ORA	#0x80
	STA	_PADIR
	JMP	_00212_DS_
_00211_DS_:
;	;.line	223; "ms326apipla.c"	DACON|=1; // enable PA finally
	LDA	_DACON
	ORA	#0x01
	STA	_DACON
;;d:\common\sdccofficial\sdcc\src\ms322\gen.c:9244: size=1, offset=0, AOP_TYPE(res)=9
_00212_DS_:
;	;.line	225; "ms326apipla.c"	api_play_spi_addr.addrhm=SPIMH;
	LDA	_SPIMH
	STA	_api_play_spi_addr
;;d:\common\sdccofficial\sdcc\src\ms322\gen.c:9244: size=0, offset=1, AOP_TYPE(res)=9
	LDA	(_SPIMH + 1)
	STA	(_api_play_spi_addr + 1)
;;d:\common\sdccofficial\sdcc\src\ms322\gen.c:9244: size=0, offset=0, AOP_TYPE(res)=9
;	;.line	226; "ms326apipla.c"	api_play_spi_addr.addrl=SPIL;
	LDA	_SPIL
	STA	(_api_play_spi_addr + 2)
;	;.line	227; "ms326apipla.c"	return 1;
	LDA	#0x01
_00213_DS_:
;	;.line	229; "ms326apipla.c"	}
	RET	
; exit point of _api_play_start
	.ENDFUNC _api_play_start
.globl _api_decode_12bit

;--------------------------------------------------------
	.FUNC _api_decode_12bit:$PNUM 1:$C:_get12halfpage\
:$L:r0x11B2:$L:r0x11B3:$L:r0x11B4:$L:r0x11B5:$L:r0x11B7\
:$L:r0x11B6
;--------------------------------------------------------
;	.line	61; "ms326apipla.c"	USHORT voiceptr=(page<<4)|0x8000;
_api_decode_12bit:	;Function start
	STA	r0x11B3
	CLRA	
	SWA	
	AND	#0xf0
	STA	r0x11B5
	LDA	r0x11B3
	SWA	
	STA	_PTRCL
	AND	#0x0f
	ORA	r0x11B5
	STA	r0x11B5
	LDA	_PTRCL
	AND	#0xf0
	STA	r0x11B3
	LDA	r0x11B5
	ORA	#0x80
	STA	r0x11B4
;	;.line	62; "ms326apipla.c"	USHORT dataptr=(api_play_spibuf<<4)|api_play_spiind|0x8000;
	CLRA	
	SWA	
	AND	#0xf0
	STA	r0x11B7
	LDA	_api_play_spibuf
	SWA	
	STA	_PTRCL
	AND	#0x0f
	ORA	r0x11B7
	STA	r0x11B7
	LDA	_PTRCL
	AND	#0xf0
	STA	r0x11B6
	CLRA	
	STA	r0x11B5
	LDA	_api_play_spiind
	ORA	r0x11B6
	STA	r0x11B6
	LDA	r0x11B5
	ORA	r0x11B7
	ORA	#0x80
	STA	r0x11B7
;	;.line	90; "ms326apipla.c"	HWPSEL=0;
	CLRA	
	STA	_HWPSEL
;	;.line	91; "ms326apipla.c"	HWPA=dataptr;
	LDA	r0x11B6
	STA	_HWPA
	LDA	r0x11B7
	STA	(_HWPA + 1)
;	;.line	92; "ms326apipla.c"	HWPSEL=1;
	LDA	#0x01
	STA	_HWPSEL
;	;.line	93; "ms326apipla.c"	HWPA=voiceptr;
	LDA	r0x11B3
	STA	_HWPA
	LDA	r0x11B4
	STA	(_HWPA + 1)
_00172_DS_:
;	;.line	95; "ms326apipla.c"	HWPSEL=0;
	CLRA	
	STA	_HWPSEL
;	;.line	97; "ms326apipla.c"	byte0=HWDINC;
	LDA	_HWDINC
	STA	r0x11B2
;	;.line	98; "ms326apipla.c"	if(!(HWPAL&0x7f)) {
	LDA	_HWPAL
	AND	#0x7f
	JNZ	_00163_DS_
;	;.line	99; "ms326apipla.c"	if(!get12halfpage()) return 0;
	CALL	_get12halfpage
	JNZ	_00163_DS_
	CLRA	
	JMP	_00175_DS_
_00163_DS_:
;	;.line	101; "ms326apipla.c"	byte1=HWDINC;
	LDA	_HWDINC
	STA	r0x11B3
;	;.line	102; "ms326apipla.c"	if(!(HWPAL&0x7f)) {
	LDA	_HWPAL
	AND	#0x7f
	JNZ	_00167_DS_
;	;.line	103; "ms326apipla.c"	if(!get12halfpage()) return 0;
	CALL	_get12halfpage
	JNZ	_00167_DS_
	CLRA	
	JMP	_00175_DS_
_00167_DS_:
;	;.line	105; "ms326apipla.c"	byte2=HWDINC;
	LDA	_HWDINC
	STA	r0x11B4
;	;.line	106; "ms326apipla.c"	if(!(HWPAL&0x7f)) {
	LDA	_HWPAL
	AND	#0x7f
	JNZ	_00171_DS_
;	;.line	107; "ms326apipla.c"	if(!get12halfpage()) return 0;
	CALL	_get12halfpage
	JNZ	_00171_DS_
	CLRA	
	JMP	_00175_DS_
_00171_DS_:
;	;.line	109; "ms326apipla.c"	HWPSEL=1;
	LDA	#0x01
	STA	_HWPSEL
;	;.line	112; "ms326apipla.c"	HWDINC=(BYTE)(byte2<<4);
	LDA	r0x11B4
	SWA	
	AND	#0xf0
	STA	_HWDINC
;	;.line	113; "ms326apipla.c"	HWDINC=byte0;
	LDA	r0x11B2
	STA	_HWDINC
;	;.line	114; "ms326apipla.c"	HWDINC=(byte2&0xf0);
	LDA	#0xf0
	AND	r0x11B4
	STA	_HWDINC
;	;.line	115; "ms326apipla.c"	HWDINC=byte1;
	LDA	r0x11B3
	STA	_HWDINC
;	;.line	117; "ms326apipla.c"	} while(HWPAL&0x7f);
	LDA	_HWPAL
	AND	#0x7f
	JNZ	_00172_DS_
;	;.line	118; "ms326apipla.c"	HWPSEL=0;
	CLRA	
	STA	_HWPSEL
;	;.line	119; "ms326apipla.c"	api_play_spiind=HWPAL&0x7f;
	LDA	_HWPAL
	AND	#0x7f
	STA	_api_play_spiind
;	;.line	123; "ms326apipla.c"	return 1;
	LDA	#0x01
_00175_DS_:
;	;.line	125; "ms326apipla.c"	}
	RET	
; exit point of _api_decode_12bit
	.ENDFUNC _api_decode_12bit

;--------------------------------------------------------
	.FUNC _get12halfpage:$PNUM 0:$L:r0x11AC
;--------------------------------------------------------
;	.line	24; "ms326apipla.c"	SPIOPRAH=api_play_spibuf>>3;
_get12halfpage:	;Function start
_00105_DS_:
	LDA	_api_play_spibuf
	SHR	
	SHR	
	SHR	
	STA	_SPIOPRAH
;	;.line	25; "ms326apipla.c"	SPIOP=0x48;// read a half page
	LDA	#0x48
	STA	_SPIOP
;	;.line	26; "ms326apipla.c"	if(SPIL&0x80)
	LDA	_SPIL
	JPL	_00107_DS_
;	;.line	28; "ms326apipla.c"	SPIOP=0x20;
	LDA	#0x20
	STA	_SPIOP
;	;.line	29; "ms326apipla.c"	SPIL=0;
	CLRA	
	STA	_SPIL
	JMP	_00108_DS_
_00107_DS_:
;	;.line	33; "ms326apipla.c"	SPIL=0x80;
	LDA	#0x80
	STA	_SPIL
_00108_DS_:
;	;.line	35; "ms326apipla.c"	if((SPIOP&0x80)||((SPIH==(BYTE)(api_play_endpage>>8))
	LDA	_SPIOP
	JMI	_00112_DS_
	LDA	(_api_play_endpage + 1)
	XOR	_SPIH
	JNZ	_00113_DS_
;	;.line	36; "ms326apipla.c"	&& SPIM==(BYTE)(api_play_endpage&0xff)))
	LDA	_api_play_endpage
	XOR	_SPIM
	JNZ	_00113_DS_
_00112_DS_:
;	;.line	38; "ms326apipla.c"	if(api_play_mode & API_PLAYMODE_REPEAT)
	LDA	_api_play_mode
	AND	#0x20
	JZ	_00110_DS_
;	;.line	40; "ms326apipla.c"	SPIH=api_play_startpage>>8;
	LDA	(_api_play_startpage + 1)
	STA	_SPIH
;	;.line	41; "ms326apipla.c"	SPIM=api_play_startpage&0xff;
	LDA	_api_play_startpage
	STA	_SPIM
;	;.line	42; "ms326apipla.c"	SPIL=0; // from 0
	CLRA	
	STA	_SPIL
;	;.line	43; "ms326apipla.c"	SPIOP=0x10; // reset endcode count
	LDA	#0x10
	STA	_SPIOP
;	;.line	45; "ms326apipla.c"	goto again;
	JMP	_00105_DS_
_00110_DS_:
;	;.line	48; "ms326apipla.c"	return 0;
	CLRA	
	JMP	_00116_DS_
_00113_DS_:
;	;.line	53; "ms326apipla.c"	HWPA=0x8000|(api_play_spibuf<<4);
	LDA	_api_play_spibuf
	CLRA	
	SWA	
	AND	#0xf0
	STA	r0x11AC
	LDA	_api_play_spibuf
	SWA	
	STA	_PTRCL
	AND	#0x0f
	ORA	r0x11AC
	STA	r0x11AC
	LDA	_PTRCL
	AND	#0xf0
	STA	_HWPA
	LDA	r0x11AC
	ORA	#0x80
	STA	(_HWPA + 1)
;	;.line	55; "ms326apipla.c"	api_play_spiind=0;
	CLRA	
	STA	_api_play_spiind
;	;.line	56; "ms326apipla.c"	return 1;
	LDA	#0x01
_00116_DS_:
;	;.line	58; "ms326apipla.c"	}
	RET	
; exit point of _get12halfpage
	.ENDFUNC _get12halfpage
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_pre_erase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start_no_erase$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop_noerase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_noer$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_chspick$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_init$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_tk_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:Fms326apipla$get12halfpage$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_decode_12bit$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$PAR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PADIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOB$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMA_IL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIDAT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCLKDIV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CMPCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTVC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMICON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PRG_RAM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIDMAC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECOAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECLEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECMODE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIOPRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMALEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULB$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULBL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULBH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULO$0$0({4}SL:U),E,0,0
	;--cdb--S:G$MULO1$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULSHIFT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUF$0$0({2}SI:S),E,0,0
	;--cdb--S:G$L2UBUFL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUFH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2USH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$U2LSH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTPRI$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRA$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$LPWMRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMINV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPICK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPSEL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$HWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWDINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PATEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TRAMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMBUFP$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$PASKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBSKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABSKIP$0$0({2}SI:U),E,0,0
	;--cdb--S:G$PATR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTR$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TOUCHC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DUMMYC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IRCD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMDUTY$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMPER$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACGCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECPWR$0$0({4}SL:U),E,0,0
	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$HWPALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0UW$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$EA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_play_endpage$0$0({2}SI:U),E,0,0
	;--cdb--S:G$api_play_startpage$0$0({2}SI:U),E,0,0
	;--cdb--S:G$api_play_mode$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_play_rampage$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_play_fifostart$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_play_fifoend$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_play_spibuf$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_play_spiind$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_play_spi_addr$0$0({3}STspiastru:S),E,0,0
	;--cdb--S:Lms326apipla.api_decode_12bit$page$65536$43({1}SC:U),R,0,0,[r0x11B2]
	;--cdb--S:Lms326apipla.api_decode_12bit$voiceptr$65536$44({2}SI:U),R,0,0,[r0x11B3,r0x11B4]
	;--cdb--S:Lms326apipla.api_decode_12bit$dataptr$65536$44({2}SI:U),R,0,0,[r0x11B6,r0x11B7]
	;--cdb--S:Lms326apipla.api_decode_12bit$byte0$65536$44({1}SC:U),R,0,0,[r0x11B2]
	;--cdb--S:Lms326apipla.api_decode_12bit$byte1$65536$44({1}SC:U),R,0,0,[r0x11B3]
	;--cdb--S:Lms326apipla.api_decode_12bit$byte2$65536$44({1}SC:U),R,0,0,[r0x11B4]
	;--cdb--S:Lms326apipla.api_play_start$fifo$65536$49({2}DG,SC:U),R,0,0,[_api_play_start_STK09,_api_play_start_STK08]
	;--cdb--S:Lms326apipla.api_play_start$dacgcl$65536$49({1}SC:U),R,0,0,[_api_play_start_STK07]
	;--cdb--S:Lms326apipla.api_play_start$dacosr$65536$49({1}SC:U),R,0,0,[_api_play_start_STK06]
	;--cdb--S:Lms326apipla.api_play_start$playmode$65536$49({1}SC:U),R,0,0,[_api_play_start_STK05]
	;--cdb--S:Lms326apipla.api_play_start$period$65536$49({2}SI:U),R,0,0,[_api_play_start_STK04,_api_play_start_STK03]
	;--cdb--S:Lms326apipla.api_play_start$end_page$65536$49({2}SI:U),R,0,0,[_api_play_start_STK02,_api_play_start_STK01]
	;--cdb--S:Lms326apipla.api_play_start$start_page$65536$49({2}SI:U),R,0,0,[_api_play_start_STK00,r0x11CC]
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_api_clear_filter_mem
	.globl	_PAR
	.globl	_PADIR
	.globl	_PIOA
	.globl	_PAWK
	.globl	_PAWKDR
	.globl	_TIMERC
	.globl	_THRLD
	.globl	_RAMP0L
	.globl	_RAMP0H
	.globl	_RAMP1L
	.globl	_RAMP1H
	.globl	_PTRCL
	.globl	_PTRCH
	.globl	_ROMPL
	.globl	_ROMPH
	.globl	_BEEPC
	.globl	_FILTERGR
	.globl	_ULAWC
	.globl	_STACKL
	.globl	_STACKH
	.globl	_ADCON
	.globl	_DACON
	.globl	_SYSC
	.globl	_SPIM
	.globl	_SPIH
	.globl	_SPIMH
	.globl	_SPIOP
	.globl	_SPI_BANK
	.globl	_ADP_IND
	.globl	_ADP_VPL
	.globl	_ADP_VPH
	.globl	_ADL
	.globl	_ADH
	.globl	_ZC
	.globl	_ADCG
	.globl	_DAC_PL
	.globl	_DAC_PH
	.globl	_PAG
	.globl	_RDMAL
	.globl	_RDMAH
	.globl	_SPIL
	.globl	_IOMASK
	.globl	_IOCMP
	.globl	_IOCNT
	.globl	_LVDCON
	.globl	_LVDCTH
	.globl	_LVRCON
	.globl	_OFFSETL
	.globl	_OFFSETH
	.globl	_RCCON
	.globl	_BGCON
	.globl	_PWRL
	.globl	_CRYPT
	.globl	_PWRH
	.globl	_PWRHL
	.globl	_IROMDL
	.globl	_IROMDH
	.globl	_RECMUTE
	.globl	_SPKC
	.globl	_DCLAMP
	.globl	_SSPIC
	.globl	_SSPIL
	.globl	_SSPIM
	.globl	_SSPIH
	.globl	_PBR
	.globl	_PBDIR
	.globl	_PIOB
	.globl	_PBWK
	.globl	_PBWKDR
	.globl	_PAIE
	.globl	_PBIE
	.globl	_PAIF
	.globl	_PBIF
	.globl	_GIE
	.globl	_GIF
	.globl	_WDTL
	.globl	_WDTH
	.globl	_RPAGES
	.globl	_PPAGES
	.globl	_DMA_IL
	.globl	_FILTERGP
	.globl	_SPIDAT
	.globl	_RSPIC
	.globl	_RCLKDIV
	.globl	_PCR
	.globl	_PCDIR
	.globl	_PIOC
	.globl	_CMPCON
	.globl	_INTVC
	.globl	_INTV
	.globl	_DMICON
	.globl	_PRG_RAM
	.globl	_PDMAL
	.globl	_PDMAH
	.globl	_PDMALH
	.globl	_SPIDMAC
	.globl	_SDMAAL
	.globl	_SDMAAH
	.globl	_SDMAALH
	.globl	_ECRAL
	.globl	_ECRAH
	.globl	_ECRALH
	.globl	_ECOAL
	.globl	_ECOAH
	.globl	_ECOALH
	.globl	_ECLEN
	.globl	_ECCON
	.globl	_ECMODE
	.globl	_SPIOPRAH
	.globl	_SDMALEN
	.globl	_MULA
	.globl	_MULAL
	.globl	_MULAH
	.globl	_MULB
	.globl	_MULBL
	.globl	_MULBH
	.globl	_MULO
	.globl	_MULO1
	.globl	_MULSHIFT
	.globl	_L2UBUF
	.globl	_L2UBUFL
	.globl	_L2UBUFH
	.globl	_ULAWD
	.globl	_L2USH
	.globl	_U2LSH
	.globl	_INTPRI
	.globl	_LPWMRA
	.globl	_LPWMRAL
	.globl	_LPWMRAH
	.globl	_LPWMEN
	.globl	_LPWMINV
	.globl	_SPICK
	.globl	_SYSC2
	.globl	_HWPSEL
	.globl	_HWPAL
	.globl	_HWPAH
	.globl	_HWPA
	.globl	_HWD
	.globl	_HWDINC
	.globl	_PATEN
	.globl	_PBTEN
	.globl	_PABTEN
	.globl	_TRAMAL
	.globl	_TRAMAH
	.globl	_TRAMBUFP
	.globl	_PASKIP
	.globl	_PBSKIP
	.globl	_PABSKIP
	.globl	_PATR
	.globl	_PBTR
	.globl	_PABTR
	.globl	_TOUCHC
	.globl	_DUMMYC
	.globl	_IRCD
	.globl	_FPWMEN
	.globl	_FPWMDUTY
	.globl	_FPWMPER
	.globl	_DACGCL
	.globl	_RECPWR
	.globl	_ICE0
	.globl	_ICE1
	.globl	_ICE2
	.globl	_ICE3
	.globl	_ICE4
	.globl	_RAMP0
	.globl	_RAMP0INC
	.globl	_RAMP1
	.globl	_RDMALH
	.globl	_HWPALH
	.globl	_RAMP1INC
	.globl	_RAMP1INC2
	.globl	_ROMP
	.globl	_ROMPINC
	.globl	_ROMPINC2
	.globl	_ACC
	.globl	_RAMP0UW
	.globl	_RAMP1UW
	.globl	_ROMPLH
	.globl	_ROMPUW
	.globl	_OFFSETLH
	.globl	_ADP_VPLH
	.globl	_TOV
	.globl	_EA
	.globl	_OV
	.globl	_api_play_endpage
	.globl	_api_play_startpage
	.globl	_api_play_mode
	.globl	_api_play_rampage
	.globl	_api_play_fifostart
	.globl	_api_play_fifoend
	.globl	_api_play_spibuf
	.globl	_api_play_spiind
	.globl	_api_play_spi_addr

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_api_play_start
	.globl	_api_decode_12bit
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_ms326apipla_0	udata
r0x11AC:	.ds	1
r0x11B2:	.ds	1
r0x11B3:	.ds	1
r0x11B4:	.ds	1
r0x11B5:	.ds	1
r0x11B6:	.ds	1
r0x11B7:	.ds	1
r0x11CC:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_api_play_start_STK00:	.ds	1
	.globl _api_play_start_STK00
_api_play_start_STK01:	.ds	1
	.globl _api_play_start_STK01
_api_play_start_STK02:	.ds	1
	.globl _api_play_start_STK02
_api_play_start_STK03:	.ds	1
	.globl _api_play_start_STK03
_api_play_start_STK04:	.ds	1
	.globl _api_play_start_STK04
_api_play_start_STK05:	.ds	1
	.globl _api_play_start_STK05
_api_play_start_STK06:	.ds	1
	.globl _api_play_start_STK06
_api_play_start_STK07:	.ds	1
	.globl _api_play_start_STK07
_api_play_start_STK08:	.ds	1
	.globl _api_play_start_STK08
_api_play_start_STK09:	.ds	1
	.globl _api_play_start_STK09
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x11D6:NULL+0:-1:1
	;--cdb--W:r0x11D7:NULL+0:-1:1
	;--cdb--W:_api_play_start_STK01:NULL+0:-1:1
	;--cdb--W:_api_play_start_STK07:NULL+0:-1:1
	;--cdb--W:_api_play_start_STK02:NULL+0:-1:1
	;--cdb--W:_api_play_start_STK09:NULL+0:-1:1
	;--cdb--W:_api_play_start_STK08:NULL+0:-1:1
	;--cdb--W:_api_play_start_STK03:NULL+0:-1:1
	;--cdb--W:_api_play_start_STK04:NULL+0:-1:1
	;--cdb--W:_api_play_start_STK00:NULL+0:-1:1
	;--cdb--W:r0x11CC:NULL+0:0:0
	;--cdb--W:_api_play_start_STK01:NULL+0:4576:0
	;--cdb--W:_api_play_start_STK02:NULL+0:4577:0
	;--cdb--W:_api_play_start_STK07:NULL+0:4571:0
	;--cdb--W:r0x11D6:NULL+0:4556:0
	;--cdb--W:r0x11CC:NULL+0:-1:1
	;--cdb--W:_api_play_start_STK06:NULL+0:0:0
	;--cdb--W:_api_play_start_STK06:NULL+0:-1:1
	;--cdb--W:r0x11B2:NULL+0:-1:1
	;--cdb--W:r0x11B4:NULL+0:-1:1
	;--cdb--W:r0x11B5:NULL+0:-1:1
	;--cdb--W:r0x11B7:NULL+0:-1:1
	;--cdb--W:r0x11B3:NULL+0:-1:1
	;--cdb--W:r0x11A9:NULL+0:-1:1
	;--cdb--W:r0x11AA:NULL+0:-1:1
	;--cdb--W:r0x11AB:NULL+0:-1:1
	end
