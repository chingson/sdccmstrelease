;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_fs2ulonglong.c"
	.module _fs2ulonglong
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:F_fs2ulonglong$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$__fs2ulonglong$0$0({2}DF,SI:U),C,0,0
	;--cdb--F:G$__fs2ulonglong$0$0({2}DF,SI:U),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _fs2ulonglong-code 
.globl ___fs2ulonglong

;--------------------------------------------------------
	.FUNC ___fs2ulonglong:$PNUM 4:$C:__shr_ulonglong\
:$L:r0x1162:$L:___fs2ulonglong_STK00:$L:___fs2ulonglong_STK01:$L:___fs2ulonglong_STK02:$L:___fs2ulonglong_fl1_65536_21\
:$L:r0x1166:$L:r0x116B:$L:r0x1171:$L:r0x1172:$L:r0x1173\
:$L:r0x1174:$L:r0x117B:$L:r0x117A:$L:r0x1179:$L:r0x1178\
:$L:r0x1177:$L:r0x1176:$L:r0x1175
;--------------------------------------------------------
;	.line	42; "_fs2ulonglong.c"	__fs2ulonglong (float a1)
___fs2ulonglong:	;Function start
	STA	r0x1162
	LDA	___fs2ulonglong_STK02
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	48; "_fs2ulonglong.c"	fl1.f = a1;
	LDA	___fs2ulonglong_STK02
	STA	___fs2ulonglong_fl1_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	___fs2ulonglong_STK01
	STA	(___fs2ulonglong_fl1_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	___fs2ulonglong_STK00
	STA	(___fs2ulonglong_fl1_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1162
	STA	(___fs2ulonglong_fl1_65536_21 + 3)
;	;.line	50; "_fs2ulonglong.c"	if (!fl1.l || SIGN(fl1.l))
	LDA	___fs2ulonglong_fl1_65536_21
	ORA	(___fs2ulonglong_fl1_65536_21 + 1)
	ORA	(___fs2ulonglong_fl1_65536_21 + 2)
	ORA	(___fs2ulonglong_fl1_65536_21 + 3)
	JZ	_00105_DS_
	LDA	(___fs2ulonglong_fl1_65536_21 + 3)
	AND	#0x80
	DECA	
	CLRA	
	ROL	
	JZ	_00106_DS_
_00105_DS_:
;	;.line	51; "_fs2ulonglong.c"	return (0);
	CLRA	
	STA	STK06
	STA	STK05
	STA	STK04
	STA	STK03
	CLRA	
	STA	STK02
	STA	STK01
	STA	STK00
	JMP	_00108_DS_
_00106_DS_:
;	;.line	53; "_fs2ulonglong.c"	exp = EXP (fl1.l) - EXCESS - 24;
	LDA	(___fs2ulonglong_fl1_65536_21 + 2)
	ROL	
	LDA	(___fs2ulonglong_fl1_65536_21 + 3)
	ROL	
	CLRB	_C
	ADD	#0x6a
	STA	___fs2ulonglong_STK02
;	;.line	54; "_fs2ulonglong.c"	l = MANT (fl1.l);
	LDA	#0x7f
	AND	(___fs2ulonglong_fl1_65536_21 + 2)
	ORA	#0x80
	STA	r0x116B
	CLRA	
	JPL	_00119_DS_
	LDA	#0xff
	JMP	_00120_DS_
_00119_DS_:
	CLRA	
_00120_DS_:
	STA	r0x1171
	STA	r0x1172
	STA	r0x1173
	STA	r0x1174
;	;.line	56; "_fs2ulonglong.c"	l >>= -exp;
	SETB	_C
	CLRA	
	SUBB	___fs2ulonglong_STK02
	STA	r0x1166
	LDA	r0x1174
	STA	STK00
	LDA	r0x1173
	STA	STK01
	LDA	r0x1172
	STA	STK02
	LDA	r0x1171
	STA	STK03
	CLRA	
	STA	STK04
	LDA	r0x116B
	STA	STK05
	LDA	(___fs2ulonglong_fl1_65536_21 + 1)
	STA	STK06
	LDA	___fs2ulonglong_fl1_65536_21
	STA	_PTRCL
	LDA	r0x1166
	CALL	__shr_ulonglong
	LDA	STK00
	STA	r0x117B
	LDA	STK01
	STA	r0x117A
	LDA	STK02
	STA	r0x1179
	LDA	STK03
	STA	r0x1178
	LDA	STK04
	STA	r0x1177
	LDA	STK05
	STA	r0x1176
	LDA	STK06
	STA	r0x1175
	LDA	_PTRCL
;	;.line	58; "_fs2ulonglong.c"	return l;
	STA	STK06
	LDA	r0x1175
	STA	STK05
	LDA	r0x1176
	STA	STK04
	LDA	r0x1177
	STA	STK03
	LDA	r0x1178
	STA	STK02
	LDA	r0x1179
	STA	STK01
	LDA	r0x117A
	STA	STK00
	LDA	r0x117B
_00108_DS_:
;	;.line	59; "_fs2ulonglong.c"	}
	RET	
; exit point of ___fs2ulonglong
	.ENDFUNC ___fs2ulonglong
	;--cdb--S:G$__fs2ulonglong$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:L_fs2ulonglong.__fs2ulonglong$a1$65536$20({4}SF:S),R,0,0,[___fs2ulonglong_STK02,___fs2ulonglong_STK01,___fs2ulonglong_STK00,r0x1162]
	;--cdb--S:L_fs2ulonglong.__fs2ulonglong$exp$65536$21({1}SC:S),R,0,0,[]
	;--cdb--S:L_fs2ulonglong.__fs2ulonglong$l$65536$21({8}SI:U),R,0,0,[___fs2ulonglong_STK02,r0x1175,r0x1176,r0x1177r0x1178r0x1179r0x117Ar0x117B]
	;--cdb--S:L_fs2ulonglong.__fs2ulonglong$fl1$65536$21({4}STfloat_long:S),E,0,0
	;--cdb--S:G$__fs2ulonglong$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__shr_ulonglong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___fs2ulonglong
	.globl	___fs2ulonglong_fl1_65536_21
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
___fs2ulonglong_fl1_65536_21:	.ds	4

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__fs2ulonglong_0	udata
r0x1162:	.ds	1
r0x1166:	.ds	1
r0x116B:	.ds	1
r0x1171:	.ds	1
r0x1172:	.ds	1
r0x1173:	.ds	1
r0x1174:	.ds	1
r0x1175:	.ds	1
r0x1176:	.ds	1
r0x1177:	.ds	1
r0x1178:	.ds	1
r0x1179:	.ds	1
r0x117A:	.ds	1
r0x117B:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
___fs2ulonglong_STK00:	.ds	1
	.globl ___fs2ulonglong_STK00
___fs2ulonglong_STK01:	.ds	1
	.globl ___fs2ulonglong_STK01
___fs2ulonglong_STK02:	.ds	1
	.globl ___fs2ulonglong_STK02
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x1164:NULL+0:-1:1
	;--cdb--W:r0x1165:NULL+0:-1:1
	;--cdb--W:r0x1166:NULL+0:-1:1
	;--cdb--W:r0x1167:NULL+0:-1:1
	;--cdb--W:___fs2ulonglong_STK02:NULL+0:-1:1
	;--cdb--W:r0x116C:NULL+0:-1:1
	;--cdb--W:___fs2ulonglong_STK02:NULL+0:4461:0
	;--cdb--W:r0x1166:NULL+0:4478:0
	;--cdb--W:r0x116C:NULL+0:0:0
	;--cdb--W:r0x116F:NULL+0:4459:0
	;--cdb--W:r0x117B:NULL+0:4468:0
	;--cdb--W:r0x117A:NULL+0:4467:0
	;--cdb--W:r0x1179:NULL+0:4466:0
	;--cdb--W:r0x1178:NULL+0:4465:0
	;--cdb--W:r0x1177:NULL+0:4464:0
	;--cdb--W:r0x1176:NULL+0:4459:0
	;--cdb--W:r0x116B:NULL+0:-1:1
	;--cdb--W:r0x1170:NULL+0:0:0
	;--cdb--W:r0x1170:NULL+0:-1:1
	end
