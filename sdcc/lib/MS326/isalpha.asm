;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"isalpha.c"
	.module isalpha
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isalpha$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isblank$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$islower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isupper$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; isalpha-code 
.globl _isalpha

;--------------------------------------------------------
	.FUNC _isalpha:$PNUM 2:$L:_isalpha_STK00:$L:r0x1161
;--------------------------------------------------------
;	.line	80; "/home/chingson/sdccofficial/sdcc/device/include/ctype.h"	return ((unsigned char)c >= 'A' && (unsigned char)c <= 'Z');
_isalpha:	;Function start
	LDA	_isalpha_STK00
	ADD	#0xbf
	JNC	_00114_DS_
	SETB	_C
	LDA	#0x5a
	SUBB	_isalpha_STK00
	JC	_00110_DS_
_00114_DS_:
;	;.line	71; "/home/chingson/sdccofficial/sdcc/device/include/ctype.h"	return ((unsigned char)c >= 'a' && (unsigned char)c <= 'z');
	LDA	_isalpha_STK00
	ADD	#0x9f
	JNC	_00117_DS_
	SETB	_C
	LDA	#0x7a
	SUBB	_isalpha_STK00
	JC	_00110_DS_
_00117_DS_:
;	;.line	39; "isalpha.c"	return (isupper (c) || islower (c));
	CLRA	
	STA	_isalpha_STK00
	JMP	_00111_DS_
_00110_DS_:
	LDA	#0x01
	STA	_isalpha_STK00
_00111_DS_:
	LDA	_isalpha_STK00
	JPL	_00130_DS_
	LDA	#0xff
	JMP	_00131_DS_
_00130_DS_:
	CLRA	
_00131_DS_:
	STA	r0x1161
	LDA	_isalpha_STK00
	STA	STK00
	LDA	r0x1161
;	;.line	40; "isalpha.c"	}
	RET	
; exit point of _isalpha
	.ENDFUNC _isalpha
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:Lisalpha.isblank$c$65536$13({2}SI:S),E,0,0
	;--cdb--S:Lisalpha.isdigit$c$65536$15({2}SI:S),E,0,0
	;--cdb--S:Lisalpha.islower$c$65536$17({2}SI:S),E,0,0
	;--cdb--S:Lisalpha.isupper$c$65536$19({2}SI:S),E,0,0
	;--cdb--S:Lisalpha.isalpha$c$65536$21({2}SI:S),R,0,0,[_isalpha_STK00,r0x1160]
	;--cdb--S:Lisalpha.isalpha$__1310720004$131072$22({2}SI:S),R,0,0,[]
	;--cdb--S:Lisalpha.isalpha$__1310720001$131072$22({2}SI:S),R,0,0,[]
	;--cdb--S:Lisalpha.isalpha$__1310720002$131072$23({2}SI:S),R,0,0,[]
	;--cdb--S:Lisalpha.isalpha$c$196608$24({2}SI:S),R,0,0,[]
	;--cdb--S:Lisalpha.isalpha$__1310720005$131072$26({2}SI:S),R,0,0,[]
	;--cdb--S:Lisalpha.isalpha$c$196608$27({2}SI:S),R,0,0,[]
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_isalpha
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_isalpha_0	udata
r0x1161:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_isalpha_STK00:	.ds	1
	.globl _isalpha_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1160:NULL+0:-1:1
	;--cdb--W:r0x1160:NULL+0:4450:0
	;--cdb--W:r0x1161:NULL+0:4450:0
	end
