;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_modschar.c"
	.module _modschar
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$_modsuchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$_modsuchar$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$_moduschar$0$0({2}DF,SC:S),C,0,0
	;--cdb--F:G$_moduschar$0$0({2}DF,SC:S),C,0,0,0,0,0
	;--cdb--S:G$_modschar$0$0({2}DF,SC:S),C,0,0
	;--cdb--F:G$_modschar$0$0({2}DF,SC:S),C,0,0,0,0,0
	;--cdb--S:G$_modsint$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _modschar-code 
.globl __modsuchar

;--------------------------------------------------------
	.FUNC __modsuchar:$PNUM 2:$C:__modsint\
:$L:r0x116E:$L:__modsuchar_STK00:$L:r0x1172
;--------------------------------------------------------
;	.line	43; "_modschar.c"	_modsuchar (signed char x, signed char y)
__modsuchar:	;Function start
	STA	r0x116E
;	;.line	45; "_modschar.c"	return ((int)((unsigned char)x) % (int)y);
	LDA	__modsuchar_STK00
	JPL	_00127_DS_
	LDA	#0xff
	JMP	_00128_DS_
_00127_DS_:
	CLRA	
_00128_DS_:
	STA	r0x1172
	LDA	__modsuchar_STK00
	STA	__modsint_STK02
	LDA	r0x1172
	STA	__modsint_STK01
	LDA	r0x116E
	STA	__modsint_STK00
	CLRA	
	CALL	__modsint
	LDA	STK00
;	;.line	46; "_modschar.c"	}
	RET	
; exit point of __modsuchar
	.ENDFUNC __modsuchar
.globl __moduschar

;--------------------------------------------------------
	.FUNC __moduschar:$PNUM 2:$C:__modsint\
:$L:r0x1168:$L:__moduschar_STK00:$L:r0x116B
;--------------------------------------------------------
;	.line	37; "_modschar.c"	_moduschar (unsigned char x, unsigned char y)
__moduschar:	;Function start
	STA	r0x1168
;	;.line	39; "_modschar.c"	return ((int)((signed char)x) % (int)y);
	JPL	_00117_DS_
	LDA	#0xff
	JMP	_00118_DS_
_00117_DS_:
	CLRA	
_00118_DS_:
	STA	r0x116B
	LDA	__moduschar_STK00
	STA	__modsint_STK02
	CLRA	
	STA	__modsint_STK01
	LDA	r0x1168
	STA	__modsint_STK00
	LDA	r0x116B
	CALL	__modsint
	LDA	STK00
;	;.line	40; "_modschar.c"	}
	RET	
; exit point of __moduschar
	.ENDFUNC __moduschar
.globl __modschar

;--------------------------------------------------------
	.FUNC __modschar:$PNUM 2:$C:__modsint\
:$L:r0x115F:$L:__modschar_STK00:$L:r0x1162:$L:r0x1163
;--------------------------------------------------------
;	.line	31; "_modschar.c"	_modschar (signed char x, signed char y)
__modschar:	;Function start
	STA	r0x115F
;	;.line	33; "_modschar.c"	return ((int)x % (int)y);
	JPL	_00107_DS_
	LDA	#0xff
	JMP	_00108_DS_
_00107_DS_:
	CLRA	
_00108_DS_:
	STA	r0x1162
	LDA	__modschar_STK00
	JPL	_00109_DS_
	LDA	#0xff
	JMP	_00110_DS_
_00109_DS_:
	CLRA	
_00110_DS_:
	STA	r0x1163
	LDA	__modschar_STK00
	STA	__modsint_STK02
	LDA	r0x1163
	STA	__modsint_STK01
	LDA	r0x115F
	STA	__modsint_STK00
	LDA	r0x1162
	CALL	__modsint
	LDA	STK00
;	;.line	34; "_modschar.c"	}
	RET	
; exit point of __modschar
	.ENDFUNC __modschar
	;--cdb--S:G$_modsuchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$_moduschar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$_modschar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$_modsint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:L_modschar._modschar$y$65536$1({1}SC:S),R,0,0,[__modschar_STK00]
	;--cdb--S:L_modschar._modschar$x$65536$1({1}SC:S),R,0,0,[r0x115F]
	;--cdb--S:L_modschar._moduschar$y$65536$3({1}SC:U),R,0,0,[__moduschar_STK00]
	;--cdb--S:L_modschar._moduschar$x$65536$3({1}SC:U),R,0,0,[r0x1168]
	;--cdb--S:L_modschar._modsuchar$y$65536$5({1}SC:S),R,0,0,[__modsuchar_STK00]
	;--cdb--S:L_modschar._modsuchar$x$65536$5({1}SC:S),R,0,0,[r0x116E]
	;--cdb--S:G$_modschar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$_moduschar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$_modsuchar$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__modsint

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__modsuchar
	.globl	__moduschar
	.globl	__modschar
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__modschar_0	udata
r0x115F:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1168:	.ds	1
r0x116B:	.ds	1
r0x116E:	.ds	1
r0x1172:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__modschar_STK00:	.ds	1
	.globl __modschar_STK00
	.globl __modsint_STK02
	.globl __modsint_STK01
	.globl __modsint_STK00
__moduschar_STK00:	.ds	1
	.globl __moduschar_STK00
__modsuchar_STK00:	.ds	1
	.globl __modsuchar_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1170:NULL+0:-1:1
	;--cdb--W:__modsuchar_STK00:NULL+0:-1:1
	;--cdb--W:r0x116E:NULL+0:14:0
	;--cdb--W:r0x1170:NULL+0:4467:0
	;--cdb--W:r0x1170:NULL+0:14:0
	;--cdb--W:r0x1171:NULL+0:0:0
	;--cdb--W:r0x1171:NULL+0:-1:1
	;--cdb--W:r0x116A:NULL+0:-1:1
	;--cdb--W:__moduschar_STK00:NULL+0:-1:1
	;--cdb--W:r0x1168:NULL+0:14:0
	;--cdb--W:r0x116A:NULL+0:4461:0
	;--cdb--W:r0x116A:NULL+0:14:0
	;--cdb--W:r0x116C:NULL+0:0:0
	;--cdb--W:r0x116C:NULL+0:-1:1
	;--cdb--W:__modschar_STK00:NULL+0:-1:1
	;--cdb--W:r0x115F:NULL+0:4452:0
	;--cdb--W:r0x115F:NULL+0:14:0
	;--cdb--W:r0x1161:NULL+0:4447:0
	;--cdb--W:r0x1161:NULL+0:14:0
	end
