;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"floorf.c"
	.module floorf
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Ffloorf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$floorf$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; floorf-code 
.globl _floorf

;--------------------------------------------------------
	.FUNC _floorf:$PNUM 4:$C:___fs2slong:$C:___slong2fs:$C:___fslt\
:$L:r0x1162:$L:_floorf_STK00:$L:_floorf_STK01:$L:_floorf_STK02:$L:r0x1166\
:$L:r0x1165:$L:r0x1164:$L:r0x1163:$L:r0x116A:$L:r0x1169\
:$L:r0x1168:$L:r0x1167
;--------------------------------------------------------
;	.line	33; "floorf.c"	float floorf (float x) _FLOAT_FUNC_REENTRANT
_floorf:	;Function start
	STA	r0x1162
;	;.line	36; "floorf.c"	r=x;
	LDA	_floorf_STK02
	STA	___fs2slong_STK02
	LDA	_floorf_STK01
	STA	___fs2slong_STK01
	LDA	_floorf_STK00
	STA	___fs2slong_STK00
	LDA	r0x1162
	CALL	___fs2slong
	STA	r0x1166
	LDA	STK00
	STA	r0x1165
	LDA	STK01
	STA	r0x1164
	LDA	STK02
	STA	r0x1163
;	;.line	37; "floorf.c"	if (r<=0)
	SETB	_C
	CLRA	
	SUBB	r0x1163
	CLRA	
	SUBB	r0x1164
	CLRA	
	SUBB	r0x1165
	CLRA	
	SUBSI	
	SUBB	r0x1166
	JNC	_00106_DS_
;	;.line	38; "floorf.c"	return (r+((r>x)?-1:0));
	LDA	r0x1163
	STA	___slong2fs_STK02
	LDA	r0x1164
	STA	___slong2fs_STK01
	LDA	r0x1165
	STA	___slong2fs_STK00
	LDA	r0x1166
	CALL	___slong2fs
	STA	r0x116A
	LDA	STK02
	STA	___fslt_STK06
	LDA	STK01
	STA	___fslt_STK05
	LDA	STK00
	STA	___fslt_STK04
	LDA	r0x116A
	STA	___fslt_STK03
	LDA	_floorf_STK02
	STA	___fslt_STK02
	LDA	_floorf_STK01
	STA	___fslt_STK01
	LDA	_floorf_STK00
	STA	___fslt_STK00
	LDA	r0x1162
	CALL	___fslt
	JZ	_00110_DS_
	LDA	#0xff
	STA	_floorf_STK02
	STA	_floorf_STK01
	JMP	_00111_DS_
_00110_DS_:
	CLRA	
	STA	_floorf_STK02
	STA	_floorf_STK01
_00111_DS_:
	LDA	_floorf_STK01
	JPL	_00120_DS_
	LDA	#0xff
	JMP	_00121_DS_
_00120_DS_:
	CLRA	
_00121_DS_:
	STA	r0x1167
	STA	r0x1168
	LDA	r0x1163
	ADD	_floorf_STK02
	STA	_floorf_STK02
	LDA	r0x1164
	ADDC	_floorf_STK01
	STA	_floorf_STK01
	LDA	r0x1165
	ADDC	r0x1167
	STA	r0x1169
	LDA	r0x1166
	ADDC	r0x1168
	STA	r0x116A
	LDA	_floorf_STK02
	STA	___slong2fs_STK02
	LDA	_floorf_STK01
	STA	___slong2fs_STK01
	LDA	r0x1169
	STA	___slong2fs_STK00
	LDA	r0x116A
	CALL	___slong2fs
	JMP	_00108_DS_
_00106_DS_:
;	;.line	40; "floorf.c"	return r;
	LDA	r0x1163
	STA	___slong2fs_STK02
	LDA	r0x1164
	STA	___slong2fs_STK01
	LDA	r0x1165
	STA	___slong2fs_STK00
	LDA	r0x1166
	CALL	___slong2fs
_00108_DS_:
;	;.line	41; "floorf.c"	}
	RET	
; exit point of _floorf
	.ENDFUNC _floorf
	;--cdb--S:Lfloorf.floorf$x$65536$25({4}SF:S),R,0,0,[_floorf_STK02,_floorf_STK01,_floorf_STK00,r0x1162]
	;--cdb--S:Lfloorf.floorf$r$65536$26({4}SL:S),R,0,0,[r0x1163,r0x1164,r0x1165,r0x1166]
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	___fs2slong
	.globl	___slong2fs
	.globl	___fslt

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_floorf
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_floorf_0	udata
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_floorf_STK00:	.ds	1
	.globl _floorf_STK00
_floorf_STK01:	.ds	1
	.globl _floorf_STK01
_floorf_STK02:	.ds	1
	.globl _floorf_STK02
	.globl ___fs2slong_STK02
	.globl ___fs2slong_STK01
	.globl ___fs2slong_STK00
	.globl ___slong2fs_STK02
	.globl ___slong2fs_STK01
	.globl ___slong2fs_STK00
	.globl ___fslt_STK06
	.globl ___fslt_STK05
	.globl ___fslt_STK04
	.globl ___fslt_STK03
	.globl ___fslt_STK02
	.globl ___fslt_STK01
	.globl ___fslt_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1162:NULL+0:4460:0
	;--cdb--W:_floorf_STK00:NULL+0:4461:0
	;--cdb--W:_floorf_STK00:NULL+0:14:0
	;--cdb--W:_floorf_STK01:NULL+0:13:0
	;--cdb--W:_floorf_STK02:NULL+0:12:0
	;--cdb--W:r0x1169:NULL+0:14:0
	;--cdb--W:r0x1168:NULL+0:13:0
	;--cdb--W:r0x1167:NULL+0:12:0
	;--cdb--W:_floorf_STK02:NULL+0:-1:1
	;--cdb--W:r0x1162:NULL+0:-1:1
	end
