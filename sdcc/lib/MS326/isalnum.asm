;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"isalnum.c"
	.module isalnum
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isalnum$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isblank$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$islower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isupper$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; isalnum-code 
.globl _isalnum

;--------------------------------------------------------
	.FUNC _isalnum:$PNUM 2:$C:_isalpha\
:$L:r0x1160:$L:_isalnum_STK00:$L:r0x1161
;--------------------------------------------------------
;	.line	37; "isalnum.c"	int isalnum (int c)
_isalnum:	;Function start
	STA	r0x1160
;	;.line	39; "isalnum.c"	return (isalpha (c) || isdigit (c));
	LDA	_isalnum_STK00
	STA	_isalpha_STK00
	LDA	r0x1160
	CALL	_isalpha
	ORA	STK00
	JNZ	_00109_DS_
;	;.line	62; "/home/chingson/sdccofficial/sdcc/device/include/ctype.h"	return ((unsigned char)c >= '0' && (unsigned char)c <= '9');
	LDA	_isalnum_STK00
	ADD	#0xd0
	JNC	_00113_DS_
	SETB	_C
	LDA	#0x39
	SUBB	_isalnum_STK00
	JC	_00109_DS_
_00113_DS_:
;	;.line	39; "isalnum.c"	return (isalpha (c) || isdigit (c));
	CLRA	
	STA	_isalnum_STK00
	JMP	_00110_DS_
_00109_DS_:
	LDA	#0x01
	STA	_isalnum_STK00
_00110_DS_:
	LDA	_isalnum_STK00
	JPL	_00124_DS_
	LDA	#0xff
	JMP	_00125_DS_
_00124_DS_:
	CLRA	
_00125_DS_:
	STA	r0x1161
	LDA	_isalnum_STK00
	STA	STK00
	LDA	r0x1161
;	;.line	40; "isalnum.c"	}
	RET	
; exit point of _isalnum
	.ENDFUNC _isalnum
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:Lisalnum.isblank$c$65536$13({2}SI:S),E,0,0
	;--cdb--S:Lisalnum.isdigit$c$65536$15({2}SI:S),E,0,0
	;--cdb--S:Lisalnum.islower$c$65536$17({2}SI:S),E,0,0
	;--cdb--S:Lisalnum.isupper$c$65536$19({2}SI:S),E,0,0
	;--cdb--S:Lisalnum.isalnum$c$65536$21({2}SI:S),R,0,0,[_isalnum_STK00,r0x1160]
	;--cdb--S:Lisalnum.isalnum$__1310720001$131072$22({2}SI:S),R,0,0,[]
	;--cdb--S:Lisalnum.isalnum$__1310720002$131072$23({2}SI:S),R,0,0,[]
	;--cdb--S:Lisalnum.isalnum$c$196608$24({2}SI:S),R,0,0,[]
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_isalpha

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_isalnum
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_isalnum_0	udata
r0x1160:	.ds	1
r0x1161:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_isalnum_STK00:	.ds	1
	.globl _isalnum_STK00
	.globl _isalpha_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x1160:NULL+0:4451:0
	;--cdb--W:r0x1161:NULL+0:4451:0
	;--cdb--W:r0x1162:NULL+0:-1:1
	end
