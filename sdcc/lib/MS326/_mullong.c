/* ---------------------------------------------------------------------------
   _mullong.c - routine for 32 bit multiplication

   Copyright (C) 2005, Raphael Neider <rneider AT web.de>

   This library is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any
   later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License 
   along with this library; see the file COPYING. If not, write to the
   Free Software Foundation, 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.

   As a special exception, if you link this library with other files,
   some of which are compiled with SDCC, to produce an executable,
   this library does not by itself cause the resulting executable to
   be covered by the GNU General Public License. This exception does
   not however invalidate any other reasons why the executable file
   might be covered by the GNU General Public License.
-------------------------------------------------------------------------*/

#pragma save
#pragma disable_warning 126 /* unreachable code */
#pragma disable_warning 116 /* left shifting more than size of object */

#include <ms326.h>
typedef unsigned char BYTE;
typedef unsigned long ULONG;
#define U(x) ((ULONG)x)
long
_mullong (long a, long b) __critical
{
 	BYTE a0,a1,a2,a3;
	BYTE b0,b1,b2,b3;

	unsigned long result;
	a0=U(a);
	a1=U(a)>>8;
	a2=U(a)>>16;
	a3=U(a)>>24;

	b0=U(b);
	b1=U(b)>>8;
	b2=U(b)>>16;
	b3=U(b)>>24;

	MULSHIFT=1;
	MULAL=0;
	MULBH=0;
	
	//part 0
	MULAH=a0;
	MULBL=b0;
	result=MULO1;

	// part 1
	// 10,01
	MULAH=a1;
	result+=(unsigned long)MULO1<<8;
	MULAH=a0;
	MULBL=b1;
	result+=(unsigned long)MULO1<<8;

	// part 2
	// 20 11 02
	MULAH=a1;
	result+=(unsigned long)MULO1<<16;
	MULAH=a0;
	MULBL=b2;
	result+=(unsigned long)MULO1<<16;
	MULAH=a2;
	MULBL=b0;
	result+=(unsigned long)MULO1<<16;
	
	// part 3
	// 30 21 12 03
	MULAH=a3;
	result+=(unsigned long)MULO1<<24;
	MULAH=a2;
	MULBL=b1;
	result+=(unsigned long)MULO1<<24;
	MULAH=a1;
	MULBL=b2;
	result+=(unsigned long)MULO1<<24;
	MULAH=a0;
	MULBL=b3;
	result+=(unsigned long)MULO1<<24;
	
	
	return (long)result;	
}
#pragma restore
