;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.1 #3de0c6772 (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"C:\work\ms326sphlib"
;;	.file	"rec_job2.c"
	.module rec_job2
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Frec_job2$spiastru[({0}S:S$addrhm$0$0({2}SI:U),Z,0,0)({2}S:S$addrl$0$0({1}SC:U),Z,0,0)]
	;--cdb--T:Frec_job2$adps[({0}S:S$predict$0$0({2}SI:S),Z,0,0)({2}S:S$index$0$0({1}SC:U),Z,0,0)]
	;--cdb--T:Frec_job2$touchen[({0}S:S$toff$0$0({1}SC:U),Z,0,0)({1}S:S$nmossw$0$0({1}SC:U),Z,0,0)({2}S:S$period$0$0({2}SI:U),Z,0,0)({4}S:S$threshold$0$0({2}SI:U),Z,0,0)({6}S:S$count$0$0({2}SI:U),Z,0,0)]
	;--cdb--T:Frec_job2$pwmleds[({0}S:S$period$0$0({1}SC:U),Z,0,0)({1}S:S$counter$0$0({1}SC:U),Z,0,0)({2}S:S$threshold$0$0({1}SC:U),Z,0,0)]
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_pre_erase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start_no_erase$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop_noerase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_noer$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_chspick$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_init$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_tk_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_job$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; rec_job2-code 
.globl _api_play_start_with_state

;--------------------------------------------------------
	.FUNC _api_play_start_with_state:$PNUM 10:$C:_api_clear_filter_mem\
:$L:r0x11CF:$L:_api_play_start_with_state_STK00:$L:_api_play_start_with_state_STK01:$L:_api_play_start_with_state_STK02:$L:_api_play_start_with_state_STK03\
:$L:_api_play_start_with_state_STK04:$L:_api_play_start_with_state_STK05:$L:_api_play_start_with_state_STK06:$L:_api_play_start_with_state_STK07:$L:_api_play_start_with_state_STK08\

;--------------------------------------------------------
;	.line	100; "rec_job2.c"	BYTE api_play_start_with_state(USHORT start_page, USHORT end_page, USHORT period, BYTE ulaw, BYTE dacosr, adpcm_state * the_state)
_api_play_start_with_state:	;Function start
	STA	r0x11CF
;	;.line	102; "rec_job2.c"	api_clear_filter_mem(0);
	CLRA	
	CALL	_api_clear_filter_mem
;	;.line	103; "rec_job2.c"	SPIH=start_page>>8;
	LDA	r0x11CF
	STA	_SPIH
;	;.line	104; "rec_job2.c"	SPIM=start_page&0xff;
	LDA	_api_play_start_with_state_STK00
	STA	_SPIM
;	;.line	105; "rec_job2.c"	SPIL=0;
	CLRA	
	STA	_SPIL
;	;.line	106; "rec_job2.c"	api_endpage=end_page;
	LDA	_api_play_start_with_state_STK02
	STA	_api_endpage
	LDA	_api_play_start_with_state_STK01
	STA	(_api_endpage + 1)
;	;.line	107; "rec_job2.c"	api_mode=ulaw;
	LDA	_api_play_start_with_state_STK05
	STA	_api_mode
;	;.line	109; "rec_job2.c"	SPIOP=0x10; // clear end-code
	LDA	#0x10
	STA	_SPIOP
;	;.line	110; "rec_job2.c"	PDMAH=0x80;
	LDA	#0x80
	STA	_PDMAH
;	;.line	111; "rec_job2.c"	ULAWC&=0x1f;
	LDA	#0x1f
	AND	_ULAWC
	STA	_ULAWC
;	;.line	112; "rec_job2.c"	ADCON=0x00;
	CLRA	
	STA	_ADCON
;	;.line	113; "rec_job2.c"	SYSC2 &=0x7f;
	LDA	#0x7f
	AND	_SYSC2
	STA	_SYSC2
;	;.line	114; "rec_job2.c"	DAC_PH=period>>8;
	LDA	_api_play_start_with_state_STK03
	STA	_DAC_PH
;	;.line	115; "rec_job2.c"	DAC_PL=period&0xff;
	LDA	_api_play_start_with_state_STK04
	STA	_DAC_PL
;	;.line	116; "rec_job2.c"	if(ulaw & 1) // ulaw need no state
	LDA	_api_play_start_with_state_STK05
	SHR	
	JNC	_00185_DS_
;	;.line	118; "rec_job2.c"	SPIOP=0x48; // read to 0x1xx
	LDA	#0x48
	STA	_SPIOP
;	;.line	119; "rec_job2.c"	SPIOP=0x20;
	LDA	#0x20
	STA	_SPIOP
;	;.line	120; "rec_job2.c"	if(SPIOP&0x80)
	LDA	_SPIOP
	JPL	_00181_DS_
;	;.line	123; "rec_job2.c"	return 0; // fail
	CLRA	
	JMP	_00190_DS_
_00181_DS_:
;	;.line	125; "rec_job2.c"	ULAWC=0xa0;
	LDA	#0xa0
	STA	_ULAWC
;	;.line	127; "rec_job2.c"	DACON=dacosr|0x1a;
	LDA	_api_play_start_with_state_STK06
	ORA	#0x1a
	STA	_DACON
	JMP	_00186_DS_
_00185_DS_:
;	;.line	131; "rec_job2.c"	ADP_VPLH=the_state->predict;
	LDA	_api_play_start_with_state_STK08
	STA	_ROMPL
	LDA	_api_play_start_with_state_STK07
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_ADP_VPLH
	LDA	@_ROMPINC
	STA	(_ADP_VPLH + 1)
;	;.line	132; "rec_job2.c"	ADP_IND=the_state->index;
	LDA	#0x02
	ADD	_api_play_start_with_state_STK08
	STA	_api_play_start_with_state_STK08
	CLRA	
	ADDC	_api_play_start_with_state_STK07
	STA	_api_play_start_with_state_STK07
	LDA	_api_play_start_with_state_STK08
	STA	_ROMPL
	LDA	_api_play_start_with_state_STK07
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_ADP_IND
;	;.line	133; "rec_job2.c"	SPIOP=0x88; // read page to 0x2xx
	LDA	#0x88
	STA	_SPIOP
;	;.line	134; "rec_job2.c"	SPIOP=0x20; // address increase a page
	LDA	#0x20
	STA	_SPIOP
;	;.line	135; "rec_job2.c"	SPIOP=0xc8; // read page to 0x3xx
	LDA	#0xc8
	STA	_SPIOP
;	;.line	136; "rec_job2.c"	if(SPIOP&0x80)
	LDA	_SPIOP
	JPL	_00183_DS_
;	;.line	138; "rec_job2.c"	return 0;
	CLRA	
	JMP	_00190_DS_
_00183_DS_:
;	;.line	140; "rec_job2.c"	DACON=dacosr|0x7a; // load state
	LDA	_api_play_start_with_state_STK06
	ORA	#0x7a
	STA	_DACON
;	;.line	141; "rec_job2.c"	DACON=dacosr|0x3a;
	LDA	_api_play_start_with_state_STK06
	ORA	#0x3a
	STA	_DACON
_00186_DS_:
;	;.line	144; "rec_job2.c"	SPIOP=0x20;// post increase
	LDA	#0x20
	STA	_SPIOP
_00187_DS_:
;	;.line	145; "rec_job2.c"	while(PDMAL==0);
	LDA	_PDMAL
	JZ	_00187_DS_
;	;.line	146; "rec_job2.c"	DACON|=1; // enable PA finally
	LDA	_DACON
	ORA	#0x01
	STA	_DACON
;	;.line	147; "rec_job2.c"	api_rampage=2; // 2xx~3xx
	LDA	#0x02
	STA	_api_rampage
;	;.line	148; "rec_job2.c"	return 1;
	LDA	#0x01
_00190_DS_:
;	;.line	149; "rec_job2.c"	}
	RET	
; exit point of _api_play_start_with_state
	.ENDFUNC _api_play_start_with_state
.globl _api_rec_job_do_write

;--------------------------------------------------------
	.FUNC _api_rec_job_do_write:$PNUM 4:$L:r0x11B9:$L:_api_rec_job_do_write_STK00:$L:_api_rec_job_do_write_STK01:$L:_api_rec_job_do_write_STK02:$L:r0x11BC\
:$L:r0x11BD
;--------------------------------------------------------
;	.line	61; "rec_job2.c"	BYTE api_rec_job_do_write( USHORT * pwr, USHORT *endpage)
_api_rec_job_do_write:	;Function start
	STA	r0x11B9
;	;.line	64; "rec_job2.c"	if(RDMAH!=api_rampage)
	LDA	_api_rampage
	XOR	_RDMAH
	JZ	_00174_DS_
_00163_DS_:
;	;.line	67; "rec_job2.c"	SPIOPRAH=api_rampage>>3;
	LDA	_api_rampage
	SHR	
	SHR	
	SHR	
	STA	_SPIOPRAH
;	;.line	68; "rec_job2.c"	SPIOP=1;
	LDA	#0x01
	STA	_SPIOP
;	;.line	69; "rec_job2.c"	SPIOP=(api_rampage<<6)|0x4;// write this page
	LDA	_api_rampage
	STA	r0x11BC
	SWA	
	AND	#0xf0
	SHL	
	SHL	
	ORA	#0x04
	STA	_SPIOP
;	;.line	70; "rec_job2.c"	SPIOP=0x20; // increase a page
	LDA	#0x20
	STA	_SPIOP
;	;.line	71; "rec_job2.c"	halt_page=api_rampage>>4;
	LDA	_api_rampage
	SWA	
	AND	#0x0f
	STA	r0x11BD
;	;.line	74; "rec_job2.c"	api_rampage+=0x10;
	LDA	#0x10
	ADD	r0x11BC
;	;.line	75; "rec_job2.c"	if(api_rampage>=api_fifoend)
	STA	_api_rampage
	SETB	_C
	SUBB	_api_fifoend
	JNC	_00165_DS_
;	;.line	76; "rec_job2.c"	api_rampage=api_fifostart;
	LDA	_api_fifostart
	STA	_api_rampage
_00165_DS_:
;	;.line	78; "rec_job2.c"	if(SPIM==(BYTE)(api_endpage&0xff) && SPIH==(BYTE)(api_endpage>>8))
	LDA	_api_endpage
	XOR	_SPIM
	JNZ	_00167_DS_
	LDA	(_api_endpage + 1)
	XOR	_SPIH
	JNZ	_00167_DS_
;	;.line	80; "rec_job2.c"	*endpage=api_endpage;
	LDA	_api_rec_job_do_write_STK01
	STA	_ROMPH
	LDA	_api_rec_job_do_write_STK02
	STA	_ROMPL
	LDA	_api_endpage
	STA	@_ROMPINC
	LDA	(_api_endpage + 1)
	STA	@_ROMPINC
;	;.line	81; "rec_job2.c"	return 0;
	CLRA	
	JMP	_00175_DS_
_00167_DS_:
;	;.line	83; "rec_job2.c"	if(!(SPIM&0xf))
	LDA	_SPIM
	AND	#0x0f
	JNZ	_00170_DS_
;	;.line	85; "rec_job2.c"	ADCON|=halt_page<<3;
	LDA	r0x11BD
	SHL	
	SHL	
	SHL	
	ORA	_ADCON
	STA	_ADCON
;	;.line	86; "rec_job2.c"	SPIOP=1;
	LDA	#0x01
	STA	_SPIOP
;	;.line	87; "rec_job2.c"	SPIOP=2;
	LDA	#0x02
	STA	_SPIOP
;	;.line	88; "rec_job2.c"	ADCON|=0x38;
	LDA	_ADCON
	ORA	#0x38
	STA	_ADCON
_00170_DS_:
;	;.line	90; "rec_job2.c"	if(RDMAH!=api_rampage)
	LDA	_api_rampage
	XOR	_RDMAH
	JNZ	_00163_DS_
;	;.line	92; "rec_job2.c"	*pwr=PWRHL; // 2 bytes !!
	LDA	r0x11B9
	STA	_ROMPH
	LDA	_api_rec_job_do_write_STK00
	STA	_ROMPL
	LDA	_PWRHL
	STA	@_ROMPINC
	LDA	(_PWRHL + 1)
	STA	@_ROMPINC
;	;.line	93; "rec_job2.c"	*endpage=SPIMH;
	LDA	_api_rec_job_do_write_STK01
	STA	_ROMPH
	LDA	_api_rec_job_do_write_STK02
	STA	_ROMPL
	LDA	_SPIMH
	STA	@_ROMPINC
	LDA	(_SPIMH + 1)
	STA	@_ROMPINC
;	;.line	94; "rec_job2.c"	return 2;
	LDA	#0x02
	JMP	_00175_DS_
_00174_DS_:
;	;.line	96; "rec_job2.c"	return 1;
	LDA	#0x01
_00175_DS_:
;	;.line	97; "rec_job2.c"	}
	RET	
; exit point of _api_rec_job_do_write
	.ENDFUNC _api_rec_job_do_write
.globl _api_rec_write_prev

;--------------------------------------------------------
	.FUNC _api_rec_write_prev:$PNUM 1:$L:r0x11B5:$L:r0x11B6
;--------------------------------------------------------
;	.line	33; "rec_job2.c"	void api_rec_write_prev(BYTE pagenum)
_api_rec_write_prev:	;Function start
	STA	r0x11B5
;	;.line	38; "rec_job2.c"	BYTE page_to_write=api_rampage-0x10;
	LDA	_api_rampage
	ADD	#0xf0
;	;.line	39; "rec_job2.c"	if(page_to_write<api_fifostart)
	STA	r0x11B6
	SETB	_C
	SUBB	_api_fifostart
	JC	_00117_DS_
;	;.line	40; "rec_job2.c"	page_to_write=api_fifoend;
	LDA	_api_fifoend
	STA	r0x11B6
_00117_DS_:
;	;.line	41; "rec_job2.c"	if(pagenum==1)
	LDA	r0x11B5
	XOR	#0x01
	JNZ	_00124_DS_
;	;.line	43; "rec_job2.c"	page_to_write-=0x10;
	LDA	r0x11B6
	ADD	#0xf0
;	;.line	44; "rec_job2.c"	if(page_to_write<api_fifostart)
	STA	r0x11B6
	SETB	_C
	SUBB	_api_fifostart
	JC	_00124_DS_
;	;.line	45; "rec_job2.c"	page_to_write=api_fifoend;
	LDA	_api_fifoend
	STA	r0x11B6
_00124_DS_:
;	;.line	47; "rec_job2.c"	while(page_to_write!=api_rampage)
	LDA	_api_rampage
	XOR	r0x11B6
	JZ	_00127_DS_
;	;.line	49; "rec_job2.c"	SPIOPRAH=page_to_write>>3;
	LDA	r0x11B6
	SHR	
	SHR	
	SHR	
	STA	_SPIOPRAH
;	;.line	50; "rec_job2.c"	SPIOP=1;
	LDA	#0x01
	STA	_SPIOP
;	;.line	51; "rec_job2.c"	SPIOP=4;
	LDA	#0x04
	STA	_SPIOP
;	;.line	52; "rec_job2.c"	SPIOP=0x20;
	LDA	#0x20
	STA	_SPIOP
;	;.line	53; "rec_job2.c"	page_to_write+=0x10;
	LDA	r0x11B6
	ADD	#0x10
;	;.line	54; "rec_job2.c"	if(page_to_write>=api_fifoend)
	STA	r0x11B6
	SETB	_C
	SUBB	_api_fifoend
	JNC	_00124_DS_
;	;.line	55; "rec_job2.c"	page_to_write=api_fifostart;
	LDA	_api_fifostart
	STA	r0x11B6
	JMP	_00124_DS_
_00127_DS_:
;	;.line	60; "rec_job2.c"	}
	RET	
; exit point of _api_rec_write_prev
	.ENDFUNC _api_rec_write_prev
.globl _api_rec_job_no_write

;--------------------------------------------------------
	.FUNC _api_rec_job_no_write:$PNUM 4:$L:r0x11A5:$L:_api_rec_job_no_write_STK00:$L:_api_rec_job_no_write_STK01:$L:_api_rec_job_no_write_STK02:$L:r0x11A8\

;--------------------------------------------------------
;	.line	14; "rec_job2.c"	BYTE api_rec_job_no_write(adpcm_state *the_state, USHORT *pwr)
_api_rec_job_no_write:	;Function start
	STA	r0x11A5
;	;.line	18; "rec_job2.c"	if(RDMAH==(api_rampage>>4))
	LDA	_api_rampage
	SWA	
	AND	#0x0f
	XOR	_RDMAH
	JNZ	_00106_DS_
;	;.line	19; "rec_job2.c"	return 0;
	CLRA	
	JMP	_00111_DS_
_00106_DS_:
;	;.line	20; "rec_job2.c"	api_rampage+=0x10;
	LDA	_api_rampage
	ADD	#0x10
;	;.line	21; "rec_job2.c"	if(api_rampage>=api_fifoend)
	STA	_api_rampage
	SETB	_C
	SUBB	_api_fifoend
	JNC	_00108_DS_
;	;.line	22; "rec_job2.c"	api_rampage=api_fifostart;
	LDA	_api_fifostart
	STA	_api_rampage
_00108_DS_:
;	;.line	23; "rec_job2.c"	if(!(api_mode&1))
	LDA	_api_mode
	SHR	
	JC	_00110_DS_
;	;.line	25; "rec_job2.c"	the_state->index=ADP_IND;
	LDA	#0x02
	ADD	_api_rec_job_no_write_STK00
	STA	r0x11A8
	CLRA	
	ADDC	r0x11A5
	STA	_ROMPH
	LDA	r0x11A8
	STA	_ROMPL
	LDA	_ADP_IND
	STA	@_ROMPINC
;	;.line	26; "rec_job2.c"	the_state->predict=ADP_VPLH;
	LDA	r0x11A5
	STA	_ROMPH
	LDA	_api_rec_job_no_write_STK00
	STA	_ROMPL
	LDA	_ADP_VPLH
	STA	@_ROMPINC
	LDA	(_ADP_VPLH + 1)
	STA	@_ROMPINC
_00110_DS_:
;	;.line	28; "rec_job2.c"	*pwr=PWRHL;
	LDA	_api_rec_job_no_write_STK01
	STA	_ROMPH
	LDA	_api_rec_job_no_write_STK02
	STA	_ROMPL
	LDA	_PWRHL
	STA	@_ROMPINC
	LDA	(_PWRHL + 1)
	STA	@_ROMPINC
;	;.line	29; "rec_job2.c"	return 1;
	LDA	#0x01
_00111_DS_:
;	;.line	30; "rec_job2.c"	}
	RET	
; exit point of _api_rec_job_no_write
	.ENDFUNC _api_rec_job_no_write
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_pre_erase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start_no_erase$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop_noerase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_noer$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_chspick$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_init$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_tk_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$PAR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PADIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOB$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMA_IL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIDAT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCLKDIV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CMPCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTVC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMICON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PRG_RAM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIDMAC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECOAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECLEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECMODE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIOPRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMALEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULB$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULBL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULBH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULO$0$0({4}SL:U),E,0,0
	;--cdb--S:G$MULO1$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULSHIFT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUF$0$0({2}SI:S),E,0,0
	;--cdb--S:G$L2UBUFL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUFH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2USH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$U2LSH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTPRI$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRA$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$LPWMRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMINV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPICK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPSEL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$HWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWDINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PATEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TRAMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMBUFP$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$PASKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBSKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABSKIP$0$0({2}SI:U),E,0,0
	;--cdb--S:G$PATR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTR$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TOUCHC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DUMMYC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IRCD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMDUTY$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMPER$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACGCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECPWR$0$0({4}SL:U),E,0,0
	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$HWPALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0UW$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$EA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_rampage$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_endpage$0$0({2}SI:U),E,0,0
	;--cdb--S:G$api_mode$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_fifostart$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_fifoend$0$0({1}SC:U),E,0,0
	;--cdb--S:Lrec_job2.api_rec_job_no_write$pwr$65536$37({2}DG,SI:U),R,0,0,[_api_rec_job_no_write_STK02,_api_rec_job_no_write_STK01]
	;--cdb--S:Lrec_job2.api_rec_job_no_write$the_state$65536$37({2}DG,STadps:S),R,0,0,[_api_rec_job_no_write_STK00,r0x11A5]
	;--cdb--S:Lrec_job2.api_rec_write_prev$pagenum$65536$40({1}SC:U),R,0,0,[r0x11B5]
	;--cdb--S:Lrec_job2.api_rec_write_prev$page_to_write$65536$41({1}SC:U),R,0,0,[r0x11B6]
	;--cdb--S:Lrec_job2.api_rec_job_do_write$endpage$65536$44({2}DG,SI:U),R,0,0,[_api_rec_job_do_write_STK02,_api_rec_job_do_write_STK01]
	;--cdb--S:Lrec_job2.api_rec_job_do_write$pwr$65536$44({2}DG,SI:U),R,0,0,[_api_rec_job_do_write_STK00,r0x11B9]
	;--cdb--S:Lrec_job2.api_rec_job_do_write$halt_page$65536$45({1}SC:U),R,0,0,[r0x11BD]
	;--cdb--S:Lrec_job2.api_play_start_with_state$the_state$65536$49({2}DG,STadps:S),R,0,0,[_api_play_start_with_state_STK08,_api_play_start_with_state_STK07]
	;--cdb--S:Lrec_job2.api_play_start_with_state$dacosr$65536$49({1}SC:U),R,0,0,[_api_play_start_with_state_STK06]
	;--cdb--S:Lrec_job2.api_play_start_with_state$ulaw$65536$49({1}SC:U),R,0,0,[_api_play_start_with_state_STK05]
	;--cdb--S:Lrec_job2.api_play_start_with_state$period$65536$49({2}SI:U),R,0,0,[_api_play_start_with_state_STK04,_api_play_start_with_state_STK03]
	;--cdb--S:Lrec_job2.api_play_start_with_state$end_page$65536$49({2}SI:U),R,0,0,[_api_play_start_with_state_STK02,_api_play_start_with_state_STK01]
	;--cdb--S:Lrec_job2.api_play_start_with_state$start_page$65536$49({2}SI:U),R,0,0,[_api_play_start_with_state_STK00,r0x11CF]
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_api_clear_filter_mem
	.globl	_PAR
	.globl	_PADIR
	.globl	_PIOA
	.globl	_PAWK
	.globl	_PAWKDR
	.globl	_TIMERC
	.globl	_THRLD
	.globl	_RAMP0L
	.globl	_RAMP0H
	.globl	_RAMP1L
	.globl	_RAMP1H
	.globl	_PTRCL
	.globl	_PTRCH
	.globl	_ROMPL
	.globl	_ROMPH
	.globl	_BEEPC
	.globl	_FILTERGR
	.globl	_ULAWC
	.globl	_STACKL
	.globl	_STACKH
	.globl	_ADCON
	.globl	_DACON
	.globl	_SYSC
	.globl	_SPIM
	.globl	_SPIH
	.globl	_SPIMH
	.globl	_SPIOP
	.globl	_SPI_BANK
	.globl	_ADP_IND
	.globl	_ADP_VPL
	.globl	_ADP_VPH
	.globl	_ADL
	.globl	_ADH
	.globl	_ZC
	.globl	_ADCG
	.globl	_DAC_PL
	.globl	_DAC_PH
	.globl	_PAG
	.globl	_RDMAL
	.globl	_RDMAH
	.globl	_SPIL
	.globl	_IOMASK
	.globl	_IOCMP
	.globl	_IOCNT
	.globl	_LVDCON
	.globl	_LVDCTH
	.globl	_LVRCON
	.globl	_OFFSETL
	.globl	_OFFSETH
	.globl	_RCCON
	.globl	_BGCON
	.globl	_PWRL
	.globl	_CRYPT
	.globl	_PWRH
	.globl	_PWRHL
	.globl	_IROMDL
	.globl	_IROMDH
	.globl	_RECMUTE
	.globl	_SPKC
	.globl	_DCLAMP
	.globl	_SSPIC
	.globl	_SSPIL
	.globl	_SSPIM
	.globl	_SSPIH
	.globl	_PBR
	.globl	_PBDIR
	.globl	_PIOB
	.globl	_PBWK
	.globl	_PBWKDR
	.globl	_PAIE
	.globl	_PBIE
	.globl	_PAIF
	.globl	_PBIF
	.globl	_GIE
	.globl	_GIF
	.globl	_WDTL
	.globl	_WDTH
	.globl	_RPAGES
	.globl	_PPAGES
	.globl	_DMA_IL
	.globl	_FILTERGP
	.globl	_SPIDAT
	.globl	_RSPIC
	.globl	_RCLKDIV
	.globl	_PCR
	.globl	_PCDIR
	.globl	_PIOC
	.globl	_CMPCON
	.globl	_INTVC
	.globl	_INTV
	.globl	_DMICON
	.globl	_PRG_RAM
	.globl	_PDMAL
	.globl	_PDMAH
	.globl	_PDMALH
	.globl	_SPIDMAC
	.globl	_SDMAAL
	.globl	_SDMAAH
	.globl	_SDMAALH
	.globl	_ECRAL
	.globl	_ECRAH
	.globl	_ECRALH
	.globl	_ECOAL
	.globl	_ECOAH
	.globl	_ECOALH
	.globl	_ECLEN
	.globl	_ECCON
	.globl	_ECMODE
	.globl	_SPIOPRAH
	.globl	_SDMALEN
	.globl	_MULA
	.globl	_MULAL
	.globl	_MULAH
	.globl	_MULB
	.globl	_MULBL
	.globl	_MULBH
	.globl	_MULO
	.globl	_MULO1
	.globl	_MULSHIFT
	.globl	_L2UBUF
	.globl	_L2UBUFL
	.globl	_L2UBUFH
	.globl	_ULAWD
	.globl	_L2USH
	.globl	_U2LSH
	.globl	_INTPRI
	.globl	_LPWMRA
	.globl	_LPWMRAL
	.globl	_LPWMRAH
	.globl	_LPWMEN
	.globl	_LPWMINV
	.globl	_SPICK
	.globl	_SYSC2
	.globl	_HWPSEL
	.globl	_HWPAL
	.globl	_HWPAH
	.globl	_HWPA
	.globl	_HWD
	.globl	_HWDINC
	.globl	_PATEN
	.globl	_PBTEN
	.globl	_PABTEN
	.globl	_TRAMAL
	.globl	_TRAMAH
	.globl	_TRAMBUFP
	.globl	_PASKIP
	.globl	_PBSKIP
	.globl	_PABSKIP
	.globl	_PATR
	.globl	_PBTR
	.globl	_PABTR
	.globl	_TOUCHC
	.globl	_DUMMYC
	.globl	_IRCD
	.globl	_FPWMEN
	.globl	_FPWMDUTY
	.globl	_FPWMPER
	.globl	_DACGCL
	.globl	_RECPWR
	.globl	_ICE0
	.globl	_ICE1
	.globl	_ICE2
	.globl	_ICE3
	.globl	_ICE4
	.globl	_RAMP0
	.globl	_RAMP0INC
	.globl	_RAMP1
	.globl	_RDMALH
	.globl	_HWPALH
	.globl	_RAMP1INC
	.globl	_RAMP1INC2
	.globl	_ROMP
	.globl	_ROMPINC
	.globl	_ROMPINC2
	.globl	_ACC
	.globl	_RAMP0UW
	.globl	_RAMP1UW
	.globl	_ROMPLH
	.globl	_ROMPUW
	.globl	_OFFSETLH
	.globl	_ADP_VPLH
	.globl	_TOV
	.globl	_EA
	.globl	_OV
	.globl	_api_rampage
	.globl	_api_endpage
	.globl	_api_mode
	.globl	_api_fifostart
	.globl	_api_fifoend

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_api_play_start_with_state
	.globl	_api_rec_job_do_write
	.globl	_api_rec_write_prev
	.globl	_api_rec_job_no_write
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_rec_job2_0	udata
r0x11A5:	.ds	1
r0x11A8:	.ds	1
r0x11B5:	.ds	1
r0x11B6:	.ds	1
r0x11B9:	.ds	1
r0x11BC:	.ds	1
r0x11BD:	.ds	1
r0x11CF:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_api_rec_job_no_write_STK00:	.ds	1
	.globl _api_rec_job_no_write_STK00
_api_rec_job_no_write_STK01:	.ds	1
	.globl _api_rec_job_no_write_STK01
_api_rec_job_no_write_STK02:	.ds	1
	.globl _api_rec_job_no_write_STK02
_api_rec_job_do_write_STK00:	.ds	1
	.globl _api_rec_job_do_write_STK00
_api_rec_job_do_write_STK01:	.ds	1
	.globl _api_rec_job_do_write_STK01
_api_rec_job_do_write_STK02:	.ds	1
	.globl _api_rec_job_do_write_STK02
_api_play_start_with_state_STK00:	.ds	1
	.globl _api_play_start_with_state_STK00
_api_play_start_with_state_STK01:	.ds	1
	.globl _api_play_start_with_state_STK01
_api_play_start_with_state_STK02:	.ds	1
	.globl _api_play_start_with_state_STK02
_api_play_start_with_state_STK03:	.ds	1
	.globl _api_play_start_with_state_STK03
_api_play_start_with_state_STK04:	.ds	1
	.globl _api_play_start_with_state_STK04
_api_play_start_with_state_STK05:	.ds	1
	.globl _api_play_start_with_state_STK05
_api_play_start_with_state_STK06:	.ds	1
	.globl _api_play_start_with_state_STK06
_api_play_start_with_state_STK07:	.ds	1
	.globl _api_play_start_with_state_STK07
_api_play_start_with_state_STK08:	.ds	1
	.globl _api_play_start_with_state_STK08
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x11D9:NULL+0:-1:1
	;--cdb--W:r0x11CF:NULL+0:-1:1
	;--cdb--W:_api_play_start_with_state_STK00:NULL+0:-1:1
	;--cdb--W:_api_play_start_with_state_STK00:NULL+0:4573:0
	;--cdb--W:r0x11D8:NULL+0:4559:0
	;--cdb--W:r0x11BD:NULL+0:-1:1
	;--cdb--W:r0x11BE:NULL+0:-1:1
	;--cdb--W:r0x11BC:NULL+0:-1:1
	;--cdb--W:r0x11BF:NULL+0:-1:1
	;--cdb--W:r0x11B6:NULL+0:-1:1
	;--cdb--W:r0x11B5:NULL+0:-1:1
	;--cdb--W:r0x11A8:NULL+0:-1:1
	;--cdb--W:r0x11A9:NULL+0:-1:1
	end
