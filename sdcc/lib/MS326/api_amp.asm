;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.1 #3de0c6772 (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"C:\work\ms326sphlib"
;;	.file	"api_amp.c"
	.module api_amp
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fapi_amp$spiastru[({0}S:S$addrhm$0$0({2}SI:U),Z,0,0)({2}S:S$addrl$0$0({1}SC:U),Z,0,0)]
	;--cdb--T:Fapi_amp$adps[({0}S:S$predict$0$0({2}SI:S),Z,0,0)({2}S:S$index$0$0({1}SC:U),Z,0,0)]
	;--cdb--T:Fapi_amp$touchen[({0}S:S$toff$0$0({1}SC:U),Z,0,0)({1}S:S$nmossw$0$0({1}SC:U),Z,0,0)({2}S:S$period$0$0({2}SI:U),Z,0,0)({4}S:S$threshold$0$0({2}SI:U),Z,0,0)({6}S:S$count$0$0({2}SI:U),Z,0,0)]
	;--cdb--T:Fapi_amp$pwmleds[({0}S:S$period$0$0({1}SC:U),Z,0,0)({1}S:S$counter$0$0({1}SC:U),Z,0,0)({2}S:S$threshold$0$0({1}SC:U),Z,0,0)]
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$api_amp_start$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$api_amp_prepare$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_pre_erase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start_no_erase$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop_noerase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_noer$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_chspick$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_init$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_tk_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_job$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; api_amp-code 
.globl _api_amp_stop

;--------------------------------------------------------
	.FUNC _api_amp_stop:$PNUM 0
;--------------------------------------------------------
;	.line	75; "api_amp.c"	DACON=0;
_api_amp_stop:	;Function start
	CLRA	
	STA	_DACON
;	;.line	76; "api_amp.c"	ULAWC&=0xEF;
	LDA	#0xef
	AND	_ULAWC
	STA	_ULAWC
;	;.line	77; "api_amp.c"	SPKC&=~0x48;
	LDA	#0xb7
	AND	_SPKC
	STA	_SPKC
;	;.line	78; "api_amp.c"	SYSC2&=0x7f; // mbias off
	LDA	#0x7f
	AND	_SYSC2
	STA	_SYSC2
;	;.line	79; "api_amp.c"	ADCON=0;
	CLRA	
	STA	_ADCON
;	;.line	80; "api_amp.c"	}
	RET	
; exit point of _api_amp_stop
	.ENDFUNC _api_amp_stop
.globl _api_amp_start

;--------------------------------------------------------
	.FUNC _api_amp_start:$PNUM 1:$L:r0x11CF
;--------------------------------------------------------
;	.line	46; "api_amp.c"	void api_amp_start(unsigned char mode) // mode=1 means use PA7, mode=2 means CEXT-out
_api_amp_start:	;Function start
	STA	r0x11CF
;	;.line	48; "api_amp.c"	RCLKDIV=0x3; // OSR HIGH!!
	LDA	#0x03
	STA	_RCLKDIV
;	;.line	49; "api_amp.c"	SYSC2|=0x80; // mbias+OSR128
	LDA	_SYSC2
	ORA	#0x80
	STA	_SYSC2
;	;.line	50; "api_amp.c"	ULAWC=0;
	CLRA	
	STA	_ULAWC
;	;.line	51; "api_amp.c"	ADCON=0x43; // enable dma, no xxx
	LDA	#0x43
	STA	_ADCON
_00109_DS_:
;	;.line	55; "api_amp.c"	while(RDMAL!=0); // minimize the delay
	LDA	_RDMAL
	JNZ	_00109_DS_
;	;.line	56; "api_amp.c"	PDMAH=0x80;
	LDA	#0x80
	STA	_PDMAH
;	;.line	58; "api_amp.c"	if(mode==1)
	LDA	r0x11CF
	XOR	#0x01
	JNZ	_00116_DS_
;	;.line	60; "api_amp.c"	DACON=0x9a;
	LDA	#0x9a
	STA	_DACON
;	;.line	61; "api_amp.c"	SPKC|=0x48;
	LDA	_SPKC
	ORA	#0x48
	STA	_SPKC
	JMP	_00118_DS_
_00116_DS_:
;	;.line	63; "api_amp.c"	else if(mode==2)
	LDA	r0x11CF
	XOR	#0x02
	JNZ	_00113_DS_
;	;.line	65; "api_amp.c"	ULAWC|=0x10;
	LDA	_ULAWC
	ORA	#0x10
	STA	_ULAWC
	JMP	_00118_DS_
_00113_DS_:
;	;.line	68; "api_amp.c"	DACON=0x9b; // just enable it
	LDA	#0x9b
	STA	_DACON
_00118_DS_:
;	;.line	71; "api_amp.c"	}
	RET	
; exit point of _api_amp_start
	.ENDFUNC _api_amp_start
.globl _api_amp_prepare

;--------------------------------------------------------
	.FUNC _api_amp_prepare:$PNUM 10:$L:r0x11B8:$L:_api_amp_prepare_STK00:$L:_api_amp_prepare_STK01:$L:_api_amp_prepare_STK02:$L:_api_amp_prepare_STK03\
:$L:_api_amp_prepare_STK04:$L:_api_amp_prepare_STK05:$L:_api_amp_prepare_STK06:$L:_api_amp_prepare_STK07:$L:_api_amp_prepare_STK08\
:$L:r0x11C2
;--------------------------------------------------------
;	.line	14; "api_amp.c"	void api_amp_prepare(BYTE pagv, BYTE fgp, BYTE fgr, BYTE dacgcl, BYTE adcgv, BYTE en5k,BYTE spkcv, BYTE *fifo, BYTE fifolen)
_api_amp_prepare:	;Function start
	STA	r0x11B8
;	;.line	16; "api_amp.c"	api_fifostart=(((USHORT)(fifo))&0x7ff)>>4;
	LDA	#0x07
	AND	_api_amp_prepare_STK06
	STA	_api_amp_prepare_STK06
	LDA	_api_amp_prepare_STK07
	SWA	
	AND	#0x0f
	STA	r0x11C2
	LDA	_api_amp_prepare_STK06
	SWA	
	PUSH	
	AND	#0xf0
	ORA	r0x11C2
	STA	r0x11C2
	POP	
	AND	#0x0f
	LDA	r0x11C2
;	;.line	17; "api_amp.c"	api_fifoend=api_fifostart+fifolen-1;
	STA	_api_fifostart
	ADD	_api_amp_prepare_STK08
	ADD	#0xff
	STA	_api_fifoend
;	;.line	19; "api_amp.c"	ADP_IND=0x80;
	LDA	#0x80
	STA	_ADP_IND
;	;.line	20; "api_amp.c"	PPAGES=RPAGES=(api_fifostart&0xf)|(((BYTE)(api_fifoend&0xf))<<4);
	LDA	_api_fifostart
	AND	#0x0f
	STA	_api_amp_prepare_STK07
	LDA	_api_fifoend
	AND	#0x0f
	SWA	
	AND	#0xf0
	ORA	_api_amp_prepare_STK07
	STA	_api_amp_prepare_STK07
	STA	_RPAGES
	LDA	_api_amp_prepare_STK07
	STA	_PPAGES
;	;.line	21; "api_amp.c"	ADP_IND=0;
	CLRA	
	STA	_ADP_IND
;	;.line	22; "api_amp.c"	PPAGES=RPAGES=(api_fifostart>>4)|(((BYTE)(api_fifoend&0xf0))>>1);
	LDA	_api_fifostart
	SWA	
	AND	#0x0f
	STA	_api_amp_prepare_STK07
	LDA	_api_fifoend
	AND	#0xf0
	SHR	
	ORA	_api_amp_prepare_STK07
	STA	_api_amp_prepare_STK07
	STA	_RPAGES
	LDA	_api_amp_prepare_STK07
	STA	_PPAGES
;	;.line	25; "api_amp.c"	PAG=pagv;
	LDA	r0x11B8
	STA	_PAG
;	;.line	26; "api_amp.c"	DAC_PL=0xff;
	LDA	#0xff
	STA	_DAC_PL
;	;.line	27; "api_amp.c"	DAC_PH=0x0; // dac period 0x1ff!!
	CLRA	
	STA	_DAC_PH
;	;.line	28; "api_amp.c"	FILTERGP=fgp;
	LDA	_api_amp_prepare_STK00
	STA	_FILTERGP
;	;.line	29; "api_amp.c"	FILTERGR=fgr;
	LDA	_api_amp_prepare_STK01
	STA	_FILTERGR
;	;.line	30; "api_amp.c"	DACGCL=dacgcl;
	LDA	_api_amp_prepare_STK02
	STA	_DACGCL
;	;.line	31; "api_amp.c"	ADCG=adcgv;
	LDA	_api_amp_prepare_STK03
	STA	_ADCG
;	;.line	32; "api_amp.c"	SPKC=(SPKC&0xcf)|spkcv;
	LDA	_SPKC
	AND	#0xcf
	ORA	_api_amp_prepare_STK05
	STA	_SPKC
;	;.line	34; "api_amp.c"	RDMAH=0x80;
	LDA	#0x80
;	;.line	35; "api_amp.c"	PDMAH=0x80; // reset to recording place
	STA	_RDMAH
	STA	_PDMAH
;	;.line	36; "api_amp.c"	OFFSETLH =0x0000; // we skip DC offset, because cap outside
	CLRA	
	STA	_OFFSETLH
	STA	(_OFFSETLH + 1)
;	;.line	37; "api_amp.c"	LVDCON=(LVDCON&0xEF)|en5k;
	LDA	_LVDCON
	AND	#0xef
	ORA	_api_amp_prepare_STK04
	STA	_LVDCON
;	;.line	39; "api_amp.c"	SYSC2|=0xA0; // mbias
	LDA	_SYSC2
	ORA	#0xa0
	STA	_SYSC2
;	;.line	40; "api_amp.c"	RCLKDIV=3; // OSR HIGH!!!
	LDA	#0x03
	STA	_RCLKDIV
;	;.line	41; "api_amp.c"	ADCON=0x41; // OSR, dma0 wakeup
	LDA	#0x41
	STA	_ADCON
;	;.line	42; "api_amp.c"	DCLAMP=0xF8;
	LDA	#0xf8
	STA	_DCLAMP
;	;.line	44; "api_amp.c"	}
	RET	
; exit point of _api_amp_prepare
	.ENDFUNC _api_amp_prepare
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_pre_erase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start_no_erase$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop_noerase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_noer$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_chspick$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_init$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_tk_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$PAR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PADIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOB$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMA_IL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIDAT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCLKDIV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CMPCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTVC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMICON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PRG_RAM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIDMAC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECOAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECLEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECMODE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIOPRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMALEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULB$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULBL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULBH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULO$0$0({4}SL:U),E,0,0
	;--cdb--S:G$MULO1$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULSHIFT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUF$0$0({2}SI:S),E,0,0
	;--cdb--S:G$L2UBUFL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUFH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2USH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$U2LSH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTPRI$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRA$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$LPWMRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMINV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPICK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPSEL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$HWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWDINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PATEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TRAMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMBUFP$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$PASKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBSKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABSKIP$0$0({2}SI:U),E,0,0
	;--cdb--S:G$PATR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTR$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TOUCHC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DUMMYC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IRCD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMDUTY$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMPER$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACGCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECPWR$0$0({4}SL:U),E,0,0
	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$HWPALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0UW$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$EA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_endpage$0$0({2}SI:U),E,0,0
	;--cdb--S:G$api_mode$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_rampage$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_fifostart$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_fifoend$0$0({1}SC:U),E,0,0
	;--cdb--S:Lapi_amp.api_amp_prepare$fifolen$65536$37({1}SC:U),R,0,0,[_api_amp_prepare_STK08]
	;--cdb--S:Lapi_amp.api_amp_prepare$fifo$65536$37({2}DG,SC:U),R,0,0,[_api_amp_prepare_STK07,_api_amp_prepare_STK06]
	;--cdb--S:Lapi_amp.api_amp_prepare$spkcv$65536$37({1}SC:U),R,0,0,[_api_amp_prepare_STK05]
	;--cdb--S:Lapi_amp.api_amp_prepare$en5k$65536$37({1}SC:U),R,0,0,[_api_amp_prepare_STK04]
	;--cdb--S:Lapi_amp.api_amp_prepare$adcgv$65536$37({1}SC:U),R,0,0,[_api_amp_prepare_STK03]
	;--cdb--S:Lapi_amp.api_amp_prepare$dacgcl$65536$37({1}SC:U),R,0,0,[_api_amp_prepare_STK02]
	;--cdb--S:Lapi_amp.api_amp_prepare$fgr$65536$37({1}SC:U),R,0,0,[_api_amp_prepare_STK01]
	;--cdb--S:Lapi_amp.api_amp_prepare$fgp$65536$37({1}SC:U),R,0,0,[_api_amp_prepare_STK00]
	;--cdb--S:Lapi_amp.api_amp_prepare$pagv$65536$37({1}SC:U),R,0,0,[r0x11B8]
	;--cdb--S:Lapi_amp.api_amp_start$mode$65536$39({1}SC:U),R,0,0,[r0x11CF]
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_PAR
	.globl	_PADIR
	.globl	_PIOA
	.globl	_PAWK
	.globl	_PAWKDR
	.globl	_TIMERC
	.globl	_THRLD
	.globl	_RAMP0L
	.globl	_RAMP0H
	.globl	_RAMP1L
	.globl	_RAMP1H
	.globl	_PTRCL
	.globl	_PTRCH
	.globl	_ROMPL
	.globl	_ROMPH
	.globl	_BEEPC
	.globl	_FILTERGR
	.globl	_ULAWC
	.globl	_STACKL
	.globl	_STACKH
	.globl	_ADCON
	.globl	_DACON
	.globl	_SYSC
	.globl	_SPIM
	.globl	_SPIH
	.globl	_SPIMH
	.globl	_SPIOP
	.globl	_SPI_BANK
	.globl	_ADP_IND
	.globl	_ADP_VPL
	.globl	_ADP_VPH
	.globl	_ADL
	.globl	_ADH
	.globl	_ZC
	.globl	_ADCG
	.globl	_DAC_PL
	.globl	_DAC_PH
	.globl	_PAG
	.globl	_RDMAL
	.globl	_RDMAH
	.globl	_SPIL
	.globl	_IOMASK
	.globl	_IOCMP
	.globl	_IOCNT
	.globl	_LVDCON
	.globl	_LVDCTH
	.globl	_LVRCON
	.globl	_OFFSETL
	.globl	_OFFSETH
	.globl	_RCCON
	.globl	_BGCON
	.globl	_PWRL
	.globl	_CRYPT
	.globl	_PWRH
	.globl	_PWRHL
	.globl	_IROMDL
	.globl	_IROMDH
	.globl	_RECMUTE
	.globl	_SPKC
	.globl	_DCLAMP
	.globl	_SSPIC
	.globl	_SSPIL
	.globl	_SSPIM
	.globl	_SSPIH
	.globl	_PBR
	.globl	_PBDIR
	.globl	_PIOB
	.globl	_PBWK
	.globl	_PBWKDR
	.globl	_PAIE
	.globl	_PBIE
	.globl	_PAIF
	.globl	_PBIF
	.globl	_GIE
	.globl	_GIF
	.globl	_WDTL
	.globl	_WDTH
	.globl	_RPAGES
	.globl	_PPAGES
	.globl	_DMA_IL
	.globl	_FILTERGP
	.globl	_SPIDAT
	.globl	_RSPIC
	.globl	_RCLKDIV
	.globl	_PCR
	.globl	_PCDIR
	.globl	_PIOC
	.globl	_CMPCON
	.globl	_INTVC
	.globl	_INTV
	.globl	_DMICON
	.globl	_PRG_RAM
	.globl	_PDMAL
	.globl	_PDMAH
	.globl	_PDMALH
	.globl	_SPIDMAC
	.globl	_SDMAAL
	.globl	_SDMAAH
	.globl	_SDMAALH
	.globl	_ECRAL
	.globl	_ECRAH
	.globl	_ECRALH
	.globl	_ECOAL
	.globl	_ECOAH
	.globl	_ECOALH
	.globl	_ECLEN
	.globl	_ECCON
	.globl	_ECMODE
	.globl	_SPIOPRAH
	.globl	_SDMALEN
	.globl	_MULA
	.globl	_MULAL
	.globl	_MULAH
	.globl	_MULB
	.globl	_MULBL
	.globl	_MULBH
	.globl	_MULO
	.globl	_MULO1
	.globl	_MULSHIFT
	.globl	_L2UBUF
	.globl	_L2UBUFL
	.globl	_L2UBUFH
	.globl	_ULAWD
	.globl	_L2USH
	.globl	_U2LSH
	.globl	_INTPRI
	.globl	_LPWMRA
	.globl	_LPWMRAL
	.globl	_LPWMRAH
	.globl	_LPWMEN
	.globl	_LPWMINV
	.globl	_SPICK
	.globl	_SYSC2
	.globl	_HWPSEL
	.globl	_HWPAL
	.globl	_HWPAH
	.globl	_HWPA
	.globl	_HWD
	.globl	_HWDINC
	.globl	_PATEN
	.globl	_PBTEN
	.globl	_PABTEN
	.globl	_TRAMAL
	.globl	_TRAMAH
	.globl	_TRAMBUFP
	.globl	_PASKIP
	.globl	_PBSKIP
	.globl	_PABSKIP
	.globl	_PATR
	.globl	_PBTR
	.globl	_PABTR
	.globl	_TOUCHC
	.globl	_DUMMYC
	.globl	_IRCD
	.globl	_FPWMEN
	.globl	_FPWMDUTY
	.globl	_FPWMPER
	.globl	_DACGCL
	.globl	_RECPWR
	.globl	_ICE0
	.globl	_ICE1
	.globl	_ICE2
	.globl	_ICE3
	.globl	_ICE4
	.globl	_RAMP0
	.globl	_RAMP0INC
	.globl	_RAMP1
	.globl	_RDMALH
	.globl	_HWPALH
	.globl	_RAMP1INC
	.globl	_RAMP1INC2
	.globl	_ROMP
	.globl	_ROMPINC
	.globl	_ROMPINC2
	.globl	_ACC
	.globl	_RAMP0UW
	.globl	_RAMP1UW
	.globl	_ROMPLH
	.globl	_ROMPUW
	.globl	_OFFSETLH
	.globl	_ADP_VPLH
	.globl	_TOV
	.globl	_EA
	.globl	_OV
	.globl	_api_endpage
	.globl	_api_mode
	.globl	_api_rampage
	.globl	_api_fifostart
	.globl	_api_fifoend

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_api_amp_start
	.globl	_api_amp_prepare
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_api_amp_0	udata
r0x11B8:	.ds	1
r0x11C2:	.ds	1
r0x11CF:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_api_amp_prepare_STK00:	.ds	1
	.globl _api_amp_prepare_STK00
_api_amp_prepare_STK01:	.ds	1
	.globl _api_amp_prepare_STK01
_api_amp_prepare_STK02:	.ds	1
	.globl _api_amp_prepare_STK02
_api_amp_prepare_STK03:	.ds	1
	.globl _api_amp_prepare_STK03
_api_amp_prepare_STK04:	.ds	1
	.globl _api_amp_prepare_STK04
_api_amp_prepare_STK05:	.ds	1
	.globl _api_amp_prepare_STK05
_api_amp_prepare_STK06:	.ds	1
	.globl _api_amp_prepare_STK06
_api_amp_prepare_STK07:	.ds	1
	.globl _api_amp_prepare_STK07
_api_amp_prepare_STK08:	.ds	1
	.globl _api_amp_prepare_STK08
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x11C3:NULL+0:-1:1
	;--cdb--W:_api_amp_prepare_STK08:NULL+0:-1:1
	;--cdb--W:_api_amp_prepare_STK07:NULL+0:-1:1
	;--cdb--W:_api_amp_prepare_STK06:NULL+0:-1:1
	;--cdb--W:r0x11B8:NULL+0:-1:1
	;--cdb--W:_api_amp_prepare_STK07:NULL+0:4546:0
	;--cdb--W:r0x11C2:NULL+0:4555:0
	;--cdb--W:r0x11C3:NULL+0:4554:0
	end
