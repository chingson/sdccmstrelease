;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_fsdiv.c"
	.module _fsdiv
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:F_fsdiv$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$__fsdiv$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:F_fsdiv$__fsdiv_org$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:F_fsdiv$__fsdiv_org$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _fsdiv-code 
.globl ___fsdiv

;--------------------------------------------------------
	.FUNC ___fsdiv:$PNUM 8:$C:___fslt:$C:___fsdiv_org\
:$L:r0x1189:$L:___fsdiv_STK00:$L:___fsdiv_STK01:$L:___fsdiv_STK02:$L:___fsdiv_STK03\
:$L:___fsdiv_STK04:$L:___fsdiv_STK05:$L:___fsdiv_STK06:$L:___fsdiv_f_65536_27
;--------------------------------------------------------
;	.line	349; "_fsdiv.c"	float __fsdiv (float a1, float a2)
___fsdiv:	;Function start
	STA	r0x1189
;	;.line	354; "_fsdiv.c"	if (a2 == 0.0f && a1 > 0.0f)
	LDA	___fsdiv_STK06
	ORA	___fsdiv_STK05
	ORA	___fsdiv_STK04
	ORA	___fsdiv_STK03
	JNZ	_00201_DS_
	LDA	___fsdiv_STK02
	STA	___fslt_STK06
	LDA	___fsdiv_STK01
	STA	___fslt_STK05
	LDA	___fsdiv_STK00
	STA	___fslt_STK04
	LDA	r0x1189
	STA	___fslt_STK03
	CLRA	
	STA	___fslt_STK02
	STA	___fslt_STK01
	STA	___fslt_STK00
	CALL	___fslt
	JZ	_00201_DS_
;	;.line	355; "_fsdiv.c"	*p = 0x7f800000; // inf
	LDA	#high (___fsdiv_f_65536_27 + 0)
	STA	_ROMPH
	LDA	#(___fsdiv_f_65536_27 + 0)
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
	STA	@_ROMPINC
	LDA	#0x80
	STA	@_ROMPINC
	DECA	
	STA	@_ROMPINC
	JMP	_00202_DS_
_00201_DS_:
;	;.line	356; "_fsdiv.c"	else if (a2 == 0.0f && a1 < 0.0f)
	LDA	___fsdiv_STK06
	ORA	___fsdiv_STK05
	ORA	___fsdiv_STK04
	ORA	___fsdiv_STK03
	JNZ	_00197_DS_
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	STA	___fslt_STK04
	STA	___fslt_STK03
	LDA	___fsdiv_STK02
	STA	___fslt_STK02
	LDA	___fsdiv_STK01
	STA	___fslt_STK01
	LDA	___fsdiv_STK00
	STA	___fslt_STK00
	LDA	r0x1189
	CALL	___fslt
	JZ	_00197_DS_
;	;.line	357; "_fsdiv.c"	*p = 0xff800000; // -inf
	LDA	#high (___fsdiv_f_65536_27 + 0)
	STA	_ROMPH
	LDA	#(___fsdiv_f_65536_27 + 0)
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
	STA	@_ROMPINC
	LDA	#0x80
	STA	@_ROMPINC
	LDA	#0xff
	STA	@_ROMPINC
	JMP	_00202_DS_
_00197_DS_:
;	;.line	358; "_fsdiv.c"	else if (a2 == 0.0f && a1 == 0.0f)
	LDA	___fsdiv_STK06
	ORA	___fsdiv_STK05
	ORA	___fsdiv_STK04
	ORA	___fsdiv_STK03
	JNZ	_00193_DS_
	LDA	___fsdiv_STK02
	ORA	___fsdiv_STK01
	ORA	___fsdiv_STK00
	ORA	r0x1189
	JNZ	_00193_DS_
;	;.line	359; "_fsdiv.c"	*p = 0xffc00000; // nan
	LDA	#high (___fsdiv_f_65536_27 + 0)
	STA	_ROMPH
	LDA	#(___fsdiv_f_65536_27 + 0)
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
	STA	@_ROMPINC
	LDA	#0xc0
	STA	@_ROMPINC
	LDA	#0xff
	STA	@_ROMPINC
	JMP	_00202_DS_
_00193_DS_:
;	;.line	361; "_fsdiv.c"	f = __fsdiv_org (a1, a2);
	LDA	___fsdiv_STK06
	STA	___fsdiv_org_STK06
	LDA	___fsdiv_STK05
	STA	___fsdiv_org_STK05
	LDA	___fsdiv_STK04
	STA	___fsdiv_org_STK04
	LDA	___fsdiv_STK03
	STA	___fsdiv_org_STK03
	LDA	___fsdiv_STK02
	STA	___fsdiv_org_STK02
	LDA	___fsdiv_STK01
	STA	___fsdiv_org_STK01
	LDA	___fsdiv_STK00
	STA	___fsdiv_org_STK00
	LDA	r0x1189
	CALL	___fsdiv_org
	STA	(___fsdiv_f_65536_27 + 3)
	LDA	STK00
	STA	(___fsdiv_f_65536_27 + 2)
	LDA	STK01
	STA	(___fsdiv_f_65536_27 + 1)
	LDA	STK02
	STA	___fsdiv_f_65536_27
_00202_DS_:
;	;.line	363; "_fsdiv.c"	return f; 
	LDA	___fsdiv_f_65536_27
	STA	STK02
	LDA	(___fsdiv_f_65536_27 + 1)
	STA	STK01
	LDA	(___fsdiv_f_65536_27 + 2)
	STA	STK00
	LDA	(___fsdiv_f_65536_27 + 3)
;	;.line	364; "_fsdiv.c"	}
	RET	
; exit point of ___fsdiv
	.ENDFUNC ___fsdiv

;--------------------------------------------------------
	.FUNC ___fsdiv_org:$PNUM 8:$L:r0x1167:$L:___fsdiv_org_STK00:$L:___fsdiv_org_STK01:$L:___fsdiv_org_STK02:$L:___fsdiv_org_STK03\
:$L:___fsdiv_org_STK04:$L:___fsdiv_org_STK05:$L:___fsdiv_org_STK06:$L:r0x116E:$L:r0x1172\
:$L:r0x1173:$L:r0x1174:$L:r0x1175:$L:r0x1176:$L:r0x1177\
:$L:r0x117B
;--------------------------------------------------------
;	.line	274; "_fsdiv.c"	static float __fsdiv_org (float a1, float a2)
___fsdiv_org:	;Function start
	STA	r0x1167
	LDA	___fsdiv_org_STK06
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	283; "_fsdiv.c"	fl1.f = a1;
	LDA	___fsdiv_org_STK02
	STA	___fsdiv_org_fl1_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	___fsdiv_org_STK01
	STA	(___fsdiv_org_fl1_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	___fsdiv_org_STK00
	STA	(___fsdiv_org_fl1_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1167
	STA	(___fsdiv_org_fl1_65536_21 + 3)
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	284; "_fsdiv.c"	fl2.f = a2;
	LDA	___fsdiv_org_STK06
	STA	___fsdiv_org_fl2_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	___fsdiv_org_STK05
	STA	(___fsdiv_org_fl2_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	___fsdiv_org_STK04
	STA	(___fsdiv_org_fl2_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	___fsdiv_org_STK03
	STA	(___fsdiv_org_fl2_65536_21 + 3)
;	;.line	287; "_fsdiv.c"	exp = EXP (fl1.l) ;
	LDA	(___fsdiv_org_fl1_65536_21 + 2)
	ROL	
	LDA	(___fsdiv_org_fl1_65536_21 + 3)
	ROL	
	CLRB	_C
	STA	___fsdiv_org_exp_65536_21
	CLRA	
	STA	(___fsdiv_org_exp_65536_21 + 1)
;	;.line	288; "_fsdiv.c"	exp -= EXP (fl2.l);
	LDA	(___fsdiv_org_fl2_65536_21 + 2)
	ROL	
	LDA	(___fsdiv_org_fl2_65536_21 + 3)
	ROL	
	STA	___fsdiv_org_STK02
	CLRA	
	ROL	
	SETB	_C
	LDA	___fsdiv_org_exp_65536_21
	SUBB	___fsdiv_org_STK02
	STA	___fsdiv_org_exp_65536_21
	LDA	(___fsdiv_org_exp_65536_21 + 1)
	SUBB	#0x00
	STA	(___fsdiv_org_exp_65536_21 + 1)
;	;.line	289; "_fsdiv.c"	exp += EXCESS;
	LDA	#0x7e
	ADD	___fsdiv_org_exp_65536_21
	STA	___fsdiv_org_exp_65536_21
	CLRA	
	ADDC	(___fsdiv_org_exp_65536_21 + 1)
	STA	(___fsdiv_org_exp_65536_21 + 1)
;	;.line	292; "_fsdiv.c"	sign = SIGN (fl1.l) ^ SIGN (fl2.l);
	LDA	(___fsdiv_org_fl1_65536_21 + 3)
	AND	#0x80
	DECA	
	CLRA	
	ROL	
	STA	___fsdiv_org_STK02
	LDA	(___fsdiv_org_fl2_65536_21 + 3)
	AND	#0x80
	DECA	
	CLRA	
	ROL	
	XOR	___fsdiv_org_STK02
	STA	___fsdiv_org_STK02
;	;.line	295; "_fsdiv.c"	if (!fl2.l)
	LDA	___fsdiv_org_fl2_65536_21
	ORA	(___fsdiv_org_fl2_65536_21 + 1)
	ORA	(___fsdiv_org_fl2_65536_21 + 2)
	ORA	(___fsdiv_org_fl2_65536_21 + 3)
	JNZ	_00106_DS_
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	297; "_fsdiv.c"	fl2.l = 0x7FC00000;
	CLRA	
	STA	___fsdiv_org_fl2_65536_21
	STA	(___fsdiv_org_fl2_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	#0xc0
	STA	(___fsdiv_org_fl2_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	#0x7f
	STA	(___fsdiv_org_fl2_65536_21 + 3)
;	;.line	298; "_fsdiv.c"	return (fl2.f);
	LDA	___fsdiv_org_fl2_65536_21
	STA	STK02
	LDA	(___fsdiv_org_fl2_65536_21 + 1)
	STA	STK01
	LDA	(___fsdiv_org_fl2_65536_21 + 2)
	STA	STK00
	LDA	(___fsdiv_org_fl2_65536_21 + 3)
	JMP	_00122_DS_
_00106_DS_:
;	;.line	302; "_fsdiv.c"	if (!fl1.l)
	LDA	___fsdiv_org_fl1_65536_21
	ORA	(___fsdiv_org_fl1_65536_21 + 1)
	ORA	(___fsdiv_org_fl1_65536_21 + 2)
	ORA	(___fsdiv_org_fl1_65536_21 + 3)
	JNZ	_00108_DS_
;	;.line	303; "_fsdiv.c"	return (0);
	CLRA	
	STA	STK02
	STA	STK01
	STA	STK00
	JMP	_00122_DS_
_00108_DS_:
;	;.line	306; "_fsdiv.c"	mant1 = MANT (fl1.l);
	LDA	#0x7f
	AND	(___fsdiv_org_fl1_65536_21 + 2)
	STA	r0x1173
	LDA	___fsdiv_org_fl1_65536_21
	STA	___fsdiv_org_mant1_65536_21
	LDA	(___fsdiv_org_fl1_65536_21 + 1)
	STA	(___fsdiv_org_mant1_65536_21 + 1)
	LDA	r0x1173
	ORA	#0x80
	STA	(___fsdiv_org_mant1_65536_21 + 2)
	CLRA	
	STA	(___fsdiv_org_mant1_65536_21 + 3)
;	;.line	307; "_fsdiv.c"	mant2 = MANT (fl2.l);
	LDA	#0x7f
	AND	(___fsdiv_org_fl2_65536_21 + 2)
	STA	r0x1173
	LDA	___fsdiv_org_fl2_65536_21
	STA	___fsdiv_org_mant2_65536_21
	LDA	(___fsdiv_org_fl2_65536_21 + 1)
	STA	(___fsdiv_org_mant2_65536_21 + 1)
	LDA	r0x1173
	ORA	#0x80
	STA	(___fsdiv_org_mant2_65536_21 + 2)
	CLRA	
	STA	(___fsdiv_org_mant2_65536_21 + 3)
;	;.line	310; "_fsdiv.c"	if (mant1 < mant2)
	SETB	_C
	LDA	___fsdiv_org_mant1_65536_21
	SUBB	___fsdiv_org_mant2_65536_21
	LDA	(___fsdiv_org_mant1_65536_21 + 1)
	SUBB	(___fsdiv_org_mant2_65536_21 + 1)
	LDA	(___fsdiv_org_mant1_65536_21 + 2)
	SUBB	(___fsdiv_org_mant2_65536_21 + 2)
	LDA	(___fsdiv_org_mant1_65536_21 + 3)
	SUBSI	
	SUBB	(___fsdiv_org_mant2_65536_21 + 3)
	JC	_00110_DS_
;	;.line	312; "_fsdiv.c"	mant1 <<= 1;
	LDA	___fsdiv_org_mant1_65536_21
	SHL	
	STA	___fsdiv_org_mant1_65536_21
	LDA	(___fsdiv_org_mant1_65536_21 + 1)
	ROL	
	STA	(___fsdiv_org_mant1_65536_21 + 1)
	LDA	(___fsdiv_org_mant1_65536_21 + 2)
	ROL	
	STA	(___fsdiv_org_mant1_65536_21 + 2)
	LDA	(___fsdiv_org_mant1_65536_21 + 3)
	ROL	
	STA	(___fsdiv_org_mant1_65536_21 + 3)
;	;.line	313; "_fsdiv.c"	exp--;
	LDA	___fsdiv_org_exp_65536_21
	DECA	
	STA	___fsdiv_org_exp_65536_21
	LDA	#0xff
	ADDC	(___fsdiv_org_exp_65536_21 + 1)
	STA	(___fsdiv_org_exp_65536_21 + 1)
_00110_DS_:
;	;.line	317; "_fsdiv.c"	mask = 0x1000000;
	CLRA	
	STA	___fsdiv_org_mask_65536_21
	STA	(___fsdiv_org_mask_65536_21 + 1)
	STA	(___fsdiv_org_mask_65536_21 + 2)
	INCA	
	STA	(___fsdiv_org_mask_65536_21 + 3)
;	;.line	318; "_fsdiv.c"	result = 0;
	CLRA	
	STA	___fsdiv_org_result_65536_21
	STA	(___fsdiv_org_result_65536_21 + 1)
	STA	(___fsdiv_org_result_65536_21 + 2)
	STA	(___fsdiv_org_result_65536_21 + 3)
_00113_DS_:
;	;.line	319; "_fsdiv.c"	while (mask)
	LDA	___fsdiv_org_mask_65536_21
	ORA	(___fsdiv_org_mask_65536_21 + 1)
	ORA	(___fsdiv_org_mask_65536_21 + 2)
	ORA	(___fsdiv_org_mask_65536_21 + 3)
	JZ	_00115_DS_
;	;.line	321; "_fsdiv.c"	if (mant1 >= mant2)
	SETB	_C
	LDA	___fsdiv_org_mant1_65536_21
	SUBB	___fsdiv_org_mant2_65536_21
	LDA	(___fsdiv_org_mant1_65536_21 + 1)
	SUBB	(___fsdiv_org_mant2_65536_21 + 1)
	LDA	(___fsdiv_org_mant1_65536_21 + 2)
	SUBB	(___fsdiv_org_mant2_65536_21 + 2)
	LDA	(___fsdiv_org_mant1_65536_21 + 3)
	SUBSI	
	SUBB	(___fsdiv_org_mant2_65536_21 + 3)
	JNC	_00112_DS_
;	;.line	323; "_fsdiv.c"	result |= mask;
	LDA	___fsdiv_org_result_65536_21
	STA	r0x116E
	LDA	(___fsdiv_org_result_65536_21 + 1)
	STA	r0x1172
	LDA	(___fsdiv_org_result_65536_21 + 2)
	STA	r0x1173
	LDA	(___fsdiv_org_result_65536_21 + 3)
	STA	r0x1174
	LDA	___fsdiv_org_mask_65536_21
	ORA	r0x116E
	STA	___fsdiv_org_result_65536_21
	LDA	(___fsdiv_org_mask_65536_21 + 1)
	ORA	r0x1172
	STA	(___fsdiv_org_result_65536_21 + 1)
	LDA	(___fsdiv_org_mask_65536_21 + 2)
	ORA	r0x1173
	STA	(___fsdiv_org_result_65536_21 + 2)
	LDA	(___fsdiv_org_mask_65536_21 + 3)
	ORA	r0x1174
	STA	(___fsdiv_org_result_65536_21 + 3)
;	;.line	324; "_fsdiv.c"	mant1 -= mant2;
	SETB	_C
	LDA	___fsdiv_org_mant1_65536_21
	SUBB	___fsdiv_org_mant2_65536_21
	STA	___fsdiv_org_mant1_65536_21
	LDA	(___fsdiv_org_mant1_65536_21 + 1)
	SUBB	(___fsdiv_org_mant2_65536_21 + 1)
	STA	(___fsdiv_org_mant1_65536_21 + 1)
	LDA	(___fsdiv_org_mant1_65536_21 + 2)
	SUBB	(___fsdiv_org_mant2_65536_21 + 2)
	STA	(___fsdiv_org_mant1_65536_21 + 2)
	LDA	(___fsdiv_org_mant1_65536_21 + 3)
	SUBB	(___fsdiv_org_mant2_65536_21 + 3)
	STA	(___fsdiv_org_mant1_65536_21 + 3)
_00112_DS_:
;	;.line	326; "_fsdiv.c"	mant1 <<= 1;
	LDA	___fsdiv_org_mant1_65536_21
	SHL	
	STA	___fsdiv_org_mant1_65536_21
	LDA	(___fsdiv_org_mant1_65536_21 + 1)
	ROL	
	STA	(___fsdiv_org_mant1_65536_21 + 1)
	LDA	(___fsdiv_org_mant1_65536_21 + 2)
	ROL	
	STA	(___fsdiv_org_mant1_65536_21 + 2)
	LDA	(___fsdiv_org_mant1_65536_21 + 3)
	ROL	
	STA	(___fsdiv_org_mant1_65536_21 + 3)
;	;.line	327; "_fsdiv.c"	mask >>= 1;
	LDA	(___fsdiv_org_mask_65536_21 + 3)
	SHR	
	STA	(___fsdiv_org_mask_65536_21 + 3)
	LDA	(___fsdiv_org_mask_65536_21 + 2)
	ROR	
	STA	(___fsdiv_org_mask_65536_21 + 2)
	LDA	(___fsdiv_org_mask_65536_21 + 1)
	ROR	
	STA	(___fsdiv_org_mask_65536_21 + 1)
	LDA	___fsdiv_org_mask_65536_21
	ROR	
	STA	___fsdiv_org_mask_65536_21
	JMP	_00113_DS_
_00115_DS_:
;	;.line	331; "_fsdiv.c"	result += 1;
	LDA	___fsdiv_org_result_65536_21
	INCA	
	STA	___fsdiv_org_result_65536_21
	CLRA	
	ADDC	(___fsdiv_org_result_65536_21 + 1)
	STA	(___fsdiv_org_result_65536_21 + 1)
	CLRA	
	ADDC	(___fsdiv_org_result_65536_21 + 2)
	STA	(___fsdiv_org_result_65536_21 + 2)
	CLRA	
	ADDC	(___fsdiv_org_result_65536_21 + 3)
	STA	(___fsdiv_org_result_65536_21 + 3)
;	;.line	334; "_fsdiv.c"	exp++;
	LDA	___fsdiv_org_exp_65536_21
	INCA	
	STA	___fsdiv_org_exp_65536_21
	CLRA	
	ADDC	(___fsdiv_org_exp_65536_21 + 1)
	STA	(___fsdiv_org_exp_65536_21 + 1)
;	;.line	335; "_fsdiv.c"	result >>= 1;
	LDA	(___fsdiv_org_result_65536_21 + 3)
	SHRS	
	STA	(___fsdiv_org_result_65536_21 + 3)
	LDA	(___fsdiv_org_result_65536_21 + 2)
	ROR	
	STA	(___fsdiv_org_result_65536_21 + 2)
	LDA	(___fsdiv_org_result_65536_21 + 1)
	ROR	
	STA	(___fsdiv_org_result_65536_21 + 1)
	LDA	___fsdiv_org_result_65536_21
	ROR	
	STA	___fsdiv_org_result_65536_21
;	;.line	337; "_fsdiv.c"	result &= ~HIDDEN;
	LDA	#0x7f
	AND	(___fsdiv_org_result_65536_21 + 2)
	STA	(___fsdiv_org_result_65536_21 + 2)
;	;.line	340; "_fsdiv.c"	if (exp >= 0x100)
	CLRB	_C
	LDA	(___fsdiv_org_exp_65536_21 + 1)
	XOR	#0x80
	ADDC	#0x7f
	JNC	_00120_DS_
;	;.line	341; "_fsdiv.c"	fl1.l = (sign ? SIGNBIT : 0) | __INFINITY;
	LDA	___fsdiv_org_STK02
	JZ	_00124_DS_
	CLRA	
	STA	r0x116E
	STA	r0x1172
	STA	r0x1173
	LDA	#0x80
	STA	r0x1174
	JMP	_00125_DS_
_00124_DS_:
	CLRA	
	STA	r0x116E
	STA	r0x1172
	STA	r0x1173
	STA	r0x1174
_00125_DS_:
	LDA	r0x1173
	ORA	#0x80
	STA	r0x1173
	LDA	r0x1174
	ORA	#0x7f
	STA	r0x1174
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
	LDA	r0x116E
	STA	___fsdiv_org_fl1_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	r0x1172
	STA	(___fsdiv_org_fl1_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	r0x1173
	STA	(___fsdiv_org_fl1_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1174
	STA	(___fsdiv_org_fl1_65536_21 + 3)
	JMP	_00121_DS_
_00120_DS_:
;	;.line	342; "_fsdiv.c"	else if (exp < 0)
	LDA	(___fsdiv_org_exp_65536_21 + 1)
	JMI	_00185_DS_
	JMP	_00117_DS_
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
_00185_DS_:
;	;.line	343; "_fsdiv.c"	fl1.l = 0;
	CLRA	
	STA	___fsdiv_org_fl1_65536_21
	STA	(___fsdiv_org_fl1_65536_21 + 1)
	STA	(___fsdiv_org_fl1_65536_21 + 2)
	STA	(___fsdiv_org_fl1_65536_21 + 3)
	JMP	_00121_DS_
_00117_DS_:
;	;.line	345; "_fsdiv.c"	fl1.l = PACK (sign ? SIGNBIT : 0 , exp, result);
	LDA	___fsdiv_org_STK02
	JZ	_00126_DS_
	CLRA	
	STA	___fsdiv_org_STK02
	STA	r0x116E
	STA	r0x1172
	LDA	#0x80
	STA	r0x1173
	JMP	_00127_DS_
_00126_DS_:
	CLRA	
	STA	___fsdiv_org_STK02
	STA	r0x116E
	STA	r0x1172
	STA	r0x1173
_00127_DS_:
	LDA	___fsdiv_org_exp_65536_21
	STA	r0x1174
	LDA	(___fsdiv_org_exp_65536_21 + 1)
	ROR	
	LDA	r0x1174
	ROR	
	STA	r0x117B
	CLRA	
	ROR	
	ORA	r0x1172
	STA	r0x1172
	LDA	r0x117B
	ORA	r0x1173
	STA	r0x1173
	LDA	___fsdiv_org_result_65536_21
	STA	r0x1174
	LDA	(___fsdiv_org_result_65536_21 + 1)
	STA	r0x1175
	LDA	(___fsdiv_org_result_65536_21 + 2)
	STA	r0x1176
	LDA	(___fsdiv_org_result_65536_21 + 3)
	STA	r0x1177
	LDA	r0x1174
	ORA	___fsdiv_org_STK02
	STA	___fsdiv_org_STK02
	LDA	r0x1175
	ORA	r0x116E
	STA	r0x116E
	LDA	r0x1176
	ORA	r0x1172
	STA	r0x1172
	LDA	r0x1177
	ORA	r0x1173
	STA	r0x1173
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
	LDA	___fsdiv_org_STK02
	STA	___fsdiv_org_fl1_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	r0x116E
	STA	(___fsdiv_org_fl1_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	r0x1172
	STA	(___fsdiv_org_fl1_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1173
	STA	(___fsdiv_org_fl1_65536_21 + 3)
_00121_DS_:
;	;.line	346; "_fsdiv.c"	return (fl1.f);
	LDA	___fsdiv_org_fl1_65536_21
	STA	STK02
	LDA	(___fsdiv_org_fl1_65536_21 + 1)
	STA	STK01
	LDA	(___fsdiv_org_fl1_65536_21 + 2)
	STA	STK00
	LDA	(___fsdiv_org_fl1_65536_21 + 3)
_00122_DS_:
;	;.line	347; "_fsdiv.c"	}
	RET	
; exit point of ___fsdiv_org
	.ENDFUNC ___fsdiv_org
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:F_fsdiv$__fsdiv_org$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:L_fsdiv.__fsdiv_org$a2$65536$20({4}SF:S),R,0,0,[___fsdiv_org_STK06,___fsdiv_org_STK05,___fsdiv_org_STK04,___fsdiv_org_STK03]
	;--cdb--S:L_fsdiv.__fsdiv_org$a1$65536$20({4}SF:S),R,0,0,[___fsdiv_org_STK02,___fsdiv_org_STK01,___fsdiv_org_STK00,r0x1167]
	;--cdb--S:L_fsdiv.__fsdiv_org$fl1$65536$21({4}STfloat_long:S),E,0,0
	;--cdb--S:L_fsdiv.__fsdiv_org$fl2$65536$21({4}STfloat_long:S),E,0,0
	;--cdb--S:L_fsdiv.__fsdiv_org$result$65536$21({4}SL:S),E,0,0
	;--cdb--S:L_fsdiv.__fsdiv_org$mask$65536$21({4}SL:U),E,0,0
	;--cdb--S:L_fsdiv.__fsdiv_org$mant1$65536$21({4}SL:S),E,0,0
	;--cdb--S:L_fsdiv.__fsdiv_org$mant2$65536$21({4}SL:S),E,0,0
	;--cdb--S:L_fsdiv.__fsdiv_org$exp$65536$21({2}SI:S),E,0,0
	;--cdb--S:L_fsdiv.__fsdiv_org$sign$65536$21({1}SC:U),R,0,0,[___fsdiv_org_STK02]
	;--cdb--S:L_fsdiv.__fsdiv$a2$65536$26({4}SF:S),R,0,0,[___fsdiv_STK06,___fsdiv_STK05,___fsdiv_STK04,___fsdiv_STK03]
	;--cdb--S:L_fsdiv.__fsdiv$a1$65536$26({4}SF:S),R,0,0,[___fsdiv_STK02,___fsdiv_STK01,___fsdiv_STK00,r0x1189]
	;--cdb--S:L_fsdiv.__fsdiv$f$65536$27({4}SF:S),E,0,0
	;--cdb--S:L_fsdiv.__fsdiv$p$65536$27({2}DG,SL:U),R,0,0,[r0x1190,r0x1191]
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	___fslt

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___fsdiv
	.globl	___fsdiv_org_fl1_65536_21
	.globl	___fsdiv_org_fl2_65536_21
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
___fsdiv_org_fl1_65536_21:	.ds	4

	.area DSEG(DATA)
___fsdiv_org_fl2_65536_21:	.ds	4

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__fsdiv_0	udata
r0x1167:	.ds	1
r0x116E:	.ds	1
r0x1172:	.ds	1
r0x1173:	.ds	1
r0x1174:	.ds	1
r0x1175:	.ds	1
r0x1176:	.ds	1
r0x1177:	.ds	1
r0x117B:	.ds	1
r0x1189:	.ds	1
___fsdiv_org_exp_65536_21:	.ds	2
___fsdiv_org_mant1_65536_21:	.ds	4
___fsdiv_org_mant2_65536_21:	.ds	4
___fsdiv_org_mask_65536_21:	.ds	4
___fsdiv_org_result_65536_21:	.ds	4
	.area DSEG (DATA); (local stack unassigned) 
___fsdiv_org_STK00:	.ds	1
___fsdiv_org_STK01:	.ds	1
___fsdiv_org_STK02:	.ds	1
___fsdiv_org_STK03:	.ds	1
___fsdiv_org_STK04:	.ds	1
___fsdiv_org_STK05:	.ds	1
___fsdiv_org_STK06:	.ds	1
___fsdiv_f_65536_27:	.ds	4
	.globl ___fsdiv_f_65536_27
___fsdiv_STK00:	.ds	1
	.globl ___fsdiv_STK00
___fsdiv_STK01:	.ds	1
	.globl ___fsdiv_STK01
___fsdiv_STK02:	.ds	1
	.globl ___fsdiv_STK02
___fsdiv_STK03:	.ds	1
	.globl ___fsdiv_STK03
___fsdiv_STK04:	.ds	1
	.globl ___fsdiv_STK04
___fsdiv_STK05:	.ds	1
	.globl ___fsdiv_STK05
___fsdiv_STK06:	.ds	1
	.globl ___fsdiv_STK06
	.globl ___fslt_STK06
	.globl ___fslt_STK05
	.globl ___fslt_STK04
	.globl ___fslt_STK03
	.globl ___fslt_STK02
	.globl ___fslt_STK01
	.globl ___fslt_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x118F:NULL+0:-1:1
	;--cdb--W:r0x118E:NULL+0:-1:1
	;--cdb--W:r0x1191:NULL+0:-1:1
	;--cdb--W:r0x1190:NULL+0:-1:1
	;--cdb--W:___fsdiv_org_STK06:NULL+0:-1:1
	;--cdb--W:___fsdiv_org_STK05:NULL+0:-1:1
	;--cdb--W:___fsdiv_org_STK00:NULL+0:-1:1
	;--cdb--W:r0x1167:NULL+0:-1:1
	;--cdb--W:r0x116E:NULL+0:-1:1
	;--cdb--W:r0x116F:NULL+0:-1:1
	;--cdb--W:r0x1170:NULL+0:-1:1
	;--cdb--W:r0x1172:NULL+0:-1:1
	;--cdb--W:r0x1173:NULL+0:-1:1
	;--cdb--W:r0x1174:NULL+0:-1:1
	;--cdb--W:r0x1176:NULL+0:-1:1
	;--cdb--W:r0x1177:NULL+0:-1:1
	;--cdb--W:___fsdiv_org_STK01:NULL+0:0:0
	;--cdb--W:___fsdiv_org_STK02:NULL+0:4482:0
	;--cdb--W:___fsdiv_org_STK06:NULL+0:4478:0
	;--cdb--W:r0x1174:NULL+0:0:0
	;--cdb--W:r0x1179:NULL+0:0:0
	;--cdb--W:r0x1178:NULL+0:0:0
	;--cdb--W:___fsdiv_org_STK01:NULL+0:-1:1
	;--cdb--W:r0x1179:NULL+0:-1:1
	;--cdb--W:r0x1178:NULL+0:-1:1
	;--cdb--W:___fsdiv_org_STK02:NULL+0:-1:1
	;--cdb--W:r0x1175:NULL+0:-1:1
	;--cdb--W:r0x117A:NULL+0:-1:1
	end
