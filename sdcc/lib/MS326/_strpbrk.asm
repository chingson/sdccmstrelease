;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_strpbrk.c"
	.module _strpbrk
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--F:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _strpbrk-code 
.globl _strpbrk

;--------------------------------------------------------
	.FUNC _strpbrk:$PNUM 4:$C:_strchr\
:$L:r0x1160:$L:_strpbrk_STK00:$L:_strpbrk_STK01:$L:_strpbrk_STK02:$L:r0x1163\
:$L:r0x1164:$L:r0x1165:$L:r0x1166
;--------------------------------------------------------
;	.line	31; "_strpbrk.c"	char * strpbrk ( char * string, char * control )
_strpbrk:	;Function start
	STA	r0x1160
;	;.line	33; "_strpbrk.c"	char *ret = NULL;
	CLRA	
	STA	r0x1163
	STA	r0x1164
_00109_DS_:
;	;.line	36; "_strpbrk.c"	while (ch = *control) {
	LDA	_strpbrk_STK02
	STA	_ROMPL
	LDA	_strpbrk_STK01
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1165
	STA	r0x1166
	LDA	r0x1165
	JZ	_00111_DS_
;	;.line	37; "_strpbrk.c"	char * p = strchr(string, ch);
	LDA	r0x1166
	STA	_strchr_STK01
	LDA	_strpbrk_STK00
	STA	_strchr_STK00
	LDA	r0x1160
	CALL	_strchr
;	;.line	38; "_strpbrk.c"	if (p != NULL && (ret == NULL || p < ret)) {
	STA	r0x1166
	ORA	STK00
	JZ	_00106_DS_
	LDA	r0x1163
	ORA	r0x1164
	JZ	_00105_DS_
	SETB	_C
	LDA	STK00
	SUBB	r0x1163
	LDA	r0x1166
	SUBB	r0x1164
	JC	_00106_DS_
_00105_DS_:
;	;.line	39; "_strpbrk.c"	ret = p;
	LDA	STK00
	STA	r0x1163
	LDA	r0x1166
	STA	r0x1164
_00106_DS_:
;	;.line	41; "_strpbrk.c"	control++;
	LDA	_strpbrk_STK02
	INCA	
	STA	_strpbrk_STK02
	CLRA	
	ADDC	_strpbrk_STK01
	STA	_strpbrk_STK01
	JMP	_00109_DS_
_00111_DS_:
;	;.line	44; "_strpbrk.c"	return (ret);
	LDA	r0x1163
	STA	STK00
	LDA	r0x1164
;	;.line	45; "_strpbrk.c"	}
	RET	
; exit point of _strpbrk
	.ENDFUNC _strpbrk
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_strpbrk.strpbrk$control$65536$21({2}DG,SC:U),R,0,0,[_strpbrk_STK02,_strpbrk_STK01]
	;--cdb--S:L_strpbrk.strpbrk$string$65536$21({2}DG,SC:U),R,0,0,[_strpbrk_STK00,r0x1160]
	;--cdb--S:L_strpbrk.strpbrk$ret$65536$22({2}DG,SC:U),R,0,0,[r0x1163,r0x1164]
	;--cdb--S:L_strpbrk.strpbrk$ch$65536$22({1}SC:U),R,0,0,[r0x1166]
	;--cdb--S:L_strpbrk.strpbrk$p$131072$23({2}DG,SC:U),R,0,0,[r0x1165,r0x1166]
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_strchr

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_strpbrk
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__strpbrk_0	udata
r0x1160:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_strpbrk_STK00:	.ds	1
	.globl _strpbrk_STK00
_strpbrk_STK01:	.ds	1
	.globl _strpbrk_STK01
_strpbrk_STK02:	.ds	1
	.globl _strpbrk_STK02
	.globl _strchr_STK01
	.globl _strchr_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1165:NULL+0:14:0
	end
