;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_free.c"
	.module _free
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:F_free$MAH[({0}S:S$next$0$0({2}DX,STMAH:S),Z,0,0)({2}S:S$len$0$0({2}SI:U),Z,0,0)({4}S:S$mem$0$0({0}DA0d,SC:U),Z,0,0)]
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$free$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--F:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0,0,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_sdcc_find_memheader$0$0({2}DF,DX,STMAH:S),C,0,0
	;--cdb--F:G$_sdcc_find_memheader$0$0({2}DF,DX,STMAH:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _free-code 
.globl _free

;--------------------------------------------------------
	.FUNC _free:$PNUM 2:$C:__sdcc_find_memheader\
:$L:r0x1167:$L:_free_STK00:$L:r0x1169:$L:r0x1168:$L:r0x116A\
:$L:r0x116B
;--------------------------------------------------------
;	.line	148; "_free.c"	void free (void * p)
_free:	;Function start
	STA	r0x1167
;	;.line	154; "_free.c"	pthis = _sdcc_find_memheader(p);
	LDA	_free_STK00
	STA	__sdcc_find_memheader_STK00
	LDA	r0x1167
	CALL	__sdcc_find_memheader
;	;.line	155; "_free.c"	if (pthis) //For allocated pointers only!
	STA	r0x1167
	ORA	STK00
	JZ	_00147_DS_
;	;.line	157; "_free.c"	if (!_sdcc_prev_memheader)
	LDA	__sdcc_prev_memheader
	ORA	(__sdcc_prev_memheader + 1)
	JNZ	_00143_DS_
;	;.line	159; "_free.c"	pthis->len = 0;
	LDA	#0x02
	ADD	STK00
	STA	r0x1168
	CLRA	
	ADDC	r0x1167
	STA	r0x1169
	LDA	r0x1168
	STA	_ROMPL
	LDA	r0x1169
	STA	_ROMPH
	CLRA	
	STA	@_ROMPINC
	STA	@_ROMPINC
	JMP	_00147_DS_
_00143_DS_:
;	;.line	163; "_free.c"	_sdcc_prev_memheader->next = pthis->next;
	LDA	STK00
	STA	_ROMPL
	LDA	r0x1167
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x116A
	LDA	@_ROMPINC
	STA	r0x116B
	LDA	__sdcc_prev_memheader
	STA	_ROMPL
	LDA	(__sdcc_prev_memheader + 1)
	STA	_ROMPH
	LDA	r0x116A
	STA	@_ROMPINC
	LDA	r0x116B
	STA	@_ROMP
_00147_DS_:
;	;.line	167; "_free.c"	}
	RET	
; exit point of _free
	.ENDFUNC _free
.globl __sdcc_find_memheader

;--------------------------------------------------------
	.FUNC __sdcc_find_memheader:$PNUM 2:$L:r0x1161:$L:__sdcc_find_memheader_STK00:$L:r0x1163:$L:r0x1162
;--------------------------------------------------------
;	.line	129; "_free.c"	MEMHEADER __xdata * _sdcc_find_memheader(void __xdata * p)
__sdcc_find_memheader:	;Function start
	STA	r0x1161
;	;.line	134; "_free.c"	if (!p)
	ORA	__sdcc_find_memheader_STK00
	JNZ	_00106_DS_
;	;.line	135; "_free.c"	return NULL;
	CLRA	
	STA	STK00
	JMP	_00111_DS_
_00106_DS_:
;	;.line	136; "_free.c"	pthis = (MEMHEADER __xdata *) p;
	LDA	__sdcc_find_memheader_STK00
;	;.line	137; "_free.c"	pthis -= 1; //to start of header
	ADD	#0xfc
	STA	__sdcc_find_memheader_STK00
	LDA	#0xff
	ADDC	r0x1161
	STA	r0x1161
;	;.line	138; "_free.c"	cur_header = _sdcc_first_memheader;
	LDA	__sdcc_first_memheader
	STA	r0x1162
	LDA	(__sdcc_first_memheader + 1)
	STA	r0x1163
;	;.line	139; "_free.c"	_sdcc_prev_memheader = NULL;
	CLRA	
	STA	__sdcc_prev_memheader
	STA	(__sdcc_prev_memheader + 1)
_00108_DS_:
;	;.line	140; "_free.c"	while (cur_header && pthis != cur_header)
	LDA	r0x1162
	ORA	r0x1163
	JZ	_00110_DS_
	LDA	r0x1162
	XOR	__sdcc_find_memheader_STK00
	JNZ	_00137_DS_
	LDA	r0x1163
	XOR	r0x1161
_00137_DS_:
	JZ	_00110_DS_
;	;.line	142; "_free.c"	_sdcc_prev_memheader = cur_header;
	LDA	r0x1162
	STA	__sdcc_prev_memheader
	LDA	r0x1163
	STA	(__sdcc_prev_memheader + 1)
;	;.line	143; "_free.c"	cur_header = cur_header->next;
	LDA	r0x1162
	STA	_ROMPL
	LDA	r0x1163
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1162
	LDA	@_ROMPINC
	STA	r0x1163
	JMP	_00108_DS_
_00110_DS_:
;	;.line	145; "_free.c"	return (cur_header);
	LDA	r0x1162
	STA	STK00
	LDA	r0x1163
_00111_DS_:
;	;.line	146; "_free.c"	}
	RET	
; exit point of __sdcc_find_memheader
	.ENDFUNC __sdcc_find_memheader
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_sdcc_find_memheader$0$0({2}DF,DX,STMAH:S),C,0,0
	;--cdb--S:L_free.aligned_alloc$size$65536$15({2}SI:U),E,0,0
	;--cdb--S:L_free.aligned_alloc$alignment$65536$15({2}SI:U),E,0,0
	;--cdb--S:G$_sdcc_first_memheader$0$0({2}DX,STMAH:S),E,0,0
	;--cdb--S:G$_sdcc_prev_memheader$0$0({2}DX,STMAH:S),E,0,0
	;--cdb--S:L_free._sdcc_find_memheader$p$65536$29({2}DX,SV:S),R,0,0,[__sdcc_find_memheader_STK00,r0x1161]
	;--cdb--S:L_free._sdcc_find_memheader$pthis$65536$30({2}DX,STMAH:S),R,0,0,[__sdcc_find_memheader_STK00,r0x1161]
	;--cdb--S:L_free._sdcc_find_memheader$cur_header$65536$30({2}DX,STMAH:S),R,0,0,[r0x1162,r0x1163]
	;--cdb--S:L_free.free$p$65536$32({2}DG,SV:S),R,0,0,[_free_STK00,r0x1167]
	;--cdb--S:L_free.free$pthis$65536$33({2}DX,STMAH:S),R,0,0,[_free_STK00,r0x1167]
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__sdcc_first_memheader

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_free
	.globl	__sdcc_find_memheader
	.globl	__sdcc_prev_memheader
	.globl	__sdcc_find_memheader_pthis_65536_30
	.globl	__sdcc_find_memheader_cur_header_65536_30
	.globl	_free_pthis_65536_33
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
__sdcc_prev_memheader:	.ds	2

	.area DSEG(DATA)
__sdcc_find_memheader_pthis_65536_30:	.ds	2

	.area DSEG(DATA)
__sdcc_find_memheader_cur_header_65536_30:	.ds	2

	.area DSEG(DATA)
_free_pthis_65536_33:	.ds	2

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__free_0	udata
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__sdcc_find_memheader_STK00:	.ds	1
	.globl __sdcc_find_memheader_STK00
_free_STK00:	.ds	1
	.globl _free_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1168:NULL+0:-1:1
	;--cdb--W:_free_STK00:NULL+0:14:0
	;--cdb--W:r0x1169:NULL+0:4455:0
	;--cdb--W:r0x1162:NULL+0:-1:1
	;--cdb--W:r0x1163:NULL+0:4449:0
	end
