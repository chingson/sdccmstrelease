#include<stdio.h>
#include "ms326.h"

void putchar(char a)
{
	unsigned char mask;
	for(mask=0x80;mask;mask>>=1)
	{
		if(a&mask) PIOA|=0x40;
		else PIOA&=0xbf;
		PIOA|=0x80;
		PIOA&=0x7f;
	}
}
