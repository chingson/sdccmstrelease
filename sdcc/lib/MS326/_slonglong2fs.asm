;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_slonglong2fs.c"
	.module _slonglong2fs
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$__slonglong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$__slonglong2fs$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__ulonglong2fs$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _slonglong2fs-code 
.globl ___slonglong2fs

;--------------------------------------------------------
	.FUNC ___slonglong2fs:$PNUM 8:$C:___ulonglong2fs\
:$L:r0x1166:$L:___slonglong2fs_STK00:$L:___slonglong2fs_STK01:$L:___slonglong2fs_STK02:$L:___slonglong2fs_STK03\
:$L:___slonglong2fs_STK04:$L:___slonglong2fs_STK05:$L:___slonglong2fs_STK06:$L:r0x1167:$L:r0x1168\
:$L:r0x1169:$L:r0x116A:$L:r0x116B:$L:r0x116C:$L:r0x116D\
:$L:r0x116E
;--------------------------------------------------------
;	.line	80; "_slonglong2fs.c"	float __slonglong2fs (signed long long sl) {
___slonglong2fs:	;Function start
	STA	r0x1166
;	;.line	81; "_slonglong2fs.c"	if (sl<0) 
	JPL	_00106_DS_
;	;.line	82; "_slonglong2fs.c"	return -__ulonglong2fs(-sl);
	SETB	_C
	CLRA	
	SUBB	___slonglong2fs_STK06
	STA	r0x1167
	CLRA	
	SUBB	___slonglong2fs_STK05
	STA	r0x1168
	CLRA	
	SUBB	___slonglong2fs_STK04
	STA	r0x1169
	CLRA	
	SUBB	___slonglong2fs_STK03
	STA	r0x116A
	CLRA	
	SUBB	___slonglong2fs_STK02
	STA	r0x116B
	CLRA	
	SUBB	___slonglong2fs_STK01
	STA	r0x116C
	CLRA	
	SUBB	___slonglong2fs_STK00
	STA	r0x116D
	CLRA	
	SUBB	r0x1166
	STA	r0x116E
	LDA	r0x1167
	STA	___ulonglong2fs_STK06
	LDA	r0x1168
	STA	___ulonglong2fs_STK05
	LDA	r0x1169
	STA	___ulonglong2fs_STK04
	LDA	r0x116A
	STA	___ulonglong2fs_STK03
	LDA	r0x116B
	STA	___ulonglong2fs_STK02
	LDA	r0x116C
	STA	___ulonglong2fs_STK01
	LDA	r0x116D
	STA	___ulonglong2fs_STK00
	LDA	r0x116E
	CALL	___ulonglong2fs
	XOR	#0x80
	JMP	_00108_DS_
_00106_DS_:
;	;.line	84; "_slonglong2fs.c"	return __ulonglong2fs(sl);
	LDA	___slonglong2fs_STK06
	STA	___ulonglong2fs_STK06
	LDA	___slonglong2fs_STK05
	STA	___ulonglong2fs_STK05
	LDA	___slonglong2fs_STK04
	STA	___ulonglong2fs_STK04
	LDA	___slonglong2fs_STK03
	STA	___ulonglong2fs_STK03
	LDA	___slonglong2fs_STK02
	STA	___ulonglong2fs_STK02
	LDA	___slonglong2fs_STK01
	STA	___ulonglong2fs_STK01
	LDA	___slonglong2fs_STK00
	STA	___ulonglong2fs_STK00
	LDA	r0x1166
	CALL	___ulonglong2fs
_00108_DS_:
;	;.line	85; "_slonglong2fs.c"	}
	RET	
; exit point of ___slonglong2fs
	.ENDFUNC ___slonglong2fs
	;--cdb--S:G$__slonglong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__ulonglong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:L_slonglong2fs.__slonglong2fs$sl$65536$21({8}SI:S),R,0,0,[___slonglong2fs_STK06,___slonglong2fs_STK05,___slonglong2fs_STK04,___slonglong2fs_STK03___slonglong2fs_STK02___slonglong2fs_STK01___slonglong2fs_STK00r0x1166]
	;--cdb--S:G$__slonglong2fs$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	___ulonglong2fs

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___slonglong2fs
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__slonglong2fs_0	udata
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116C:	.ds	1
r0x116D:	.ds	1
r0x116E:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
___slonglong2fs_STK00:	.ds	1
	.globl ___slonglong2fs_STK00
___slonglong2fs_STK01:	.ds	1
	.globl ___slonglong2fs_STK01
___slonglong2fs_STK02:	.ds	1
	.globl ___slonglong2fs_STK02
___slonglong2fs_STK03:	.ds	1
	.globl ___slonglong2fs_STK03
___slonglong2fs_STK04:	.ds	1
	.globl ___slonglong2fs_STK04
___slonglong2fs_STK05:	.ds	1
	.globl ___slonglong2fs_STK05
___slonglong2fs_STK06:	.ds	1
	.globl ___slonglong2fs_STK06
	.globl ___ulonglong2fs_STK06
	.globl ___ulonglong2fs_STK05
	.globl ___ulonglong2fs_STK04
	.globl ___ulonglong2fs_STK03
	.globl ___ulonglong2fs_STK02
	.globl ___ulonglong2fs_STK01
	.globl ___ulonglong2fs_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:___slonglong2fs_STK04:NULL+0:14:0
	;--cdb--W:___slonglong2fs_STK05:NULL+0:13:0
	;--cdb--W:___slonglong2fs_STK06:NULL+0:12:0
	;--cdb--W:r0x1167:NULL+0:12:0
	;--cdb--W:r0x1168:NULL+0:13:0
	;--cdb--W:r0x1169:NULL+0:14:0
	;--cdb--W:r0x116A:NULL+0:-1:1
	;--cdb--W:___slonglong2fs_STK03:NULL+0:-1:1
	end
