;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_divulonglong.c"
	.module _divulonglong
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$_divulonglong$0$0({2}DF,SI:U),C,0,0
	;--cdb--F:G$_divulonglong$0$0({2}DF,SI:U),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _divulonglong-code 
.globl __divulonglong

;--------------------------------------------------------
	.FUNC __divulonglong:$PNUM 16:$L:r0x1166:$L:__divulonglong_STK00:$L:__divulonglong_STK01:$L:__divulonglong_STK02:$L:__divulonglong_STK03\
:$L:__divulonglong_STK04:$L:__divulonglong_STK05:$L:__divulonglong_STK06:$L:__divulonglong_STK07:$L:__divulonglong_STK08\
:$L:__divulonglong_STK09:$L:__divulonglong_STK10:$L:__divulonglong_STK11:$L:__divulonglong_STK12:$L:__divulonglong_STK13\
:$L:__divulonglong_STK14:$L:r0x116F:$L:r0x1170:$L:r0x1171:$L:r0x1172\
:$L:r0x1173:$L:r0x1174:$L:r0x1175:$L:r0x1176:$L:r0x1177\
:$L:r0x1178
;--------------------------------------------------------
;	.line	39; "_divulonglong.c"	_divulonglong (unsigned long long x, unsigned long long y)
__divulonglong:	;Function start
	STA	r0x1166
;	;.line	41; "_divulonglong.c"	unsigned long long reste = 0L;
	CLRA	
	STA	r0x116F
	STA	r0x1170
	STA	r0x1171
	STA	r0x1172
	STA	r0x1173
	STA	r0x1174
	STA	r0x1175
	STA	r0x1176
;	;.line	42; "_divulonglong.c"	unsigned char count = 64;
	LDA	#0x40
	STA	r0x1177
_00109_DS_:
;	;.line	48; "_divulonglong.c"	c = MSB_SET(x);
	LDA	r0x1166
	AND	#0x80
	DECA	
	CLRA	
	ROL	
	STA	r0x1178
;	;.line	49; "_divulonglong.c"	x <<= 1;
	LDA	__divulonglong_STK06
	SHL	
	STA	__divulonglong_STK06
	LDA	__divulonglong_STK05
	ROL	
	STA	__divulonglong_STK05
	LDA	__divulonglong_STK04
	ROL	
	STA	__divulonglong_STK04
	LDA	__divulonglong_STK03
	ROL	
	STA	__divulonglong_STK03
	LDA	__divulonglong_STK02
	ROL	
	STA	__divulonglong_STK02
	LDA	__divulonglong_STK01
	ROL	
	STA	__divulonglong_STK01
	LDA	__divulonglong_STK00
	ROL	
	STA	__divulonglong_STK00
	LDA	r0x1166
	ROL	
	STA	r0x1166
;	;.line	50; "_divulonglong.c"	reste <<= 1;
	LDA	r0x116F
	SHL	
	STA	r0x116F
	LDA	r0x1170
	ROL	
	STA	r0x1170
	LDA	r0x1171
	ROL	
	STA	r0x1171
	LDA	r0x1172
	ROL	
	STA	r0x1172
	LDA	r0x1173
	ROL	
	STA	r0x1173
	LDA	r0x1174
	ROL	
	STA	r0x1174
	LDA	r0x1175
	ROL	
	STA	r0x1175
	LDA	r0x1176
	ROL	
	STA	r0x1176
;	;.line	51; "_divulonglong.c"	if (c)
	LDA	r0x1178
	JZ	_00106_DS_
;	;.line	52; "_divulonglong.c"	reste |= 1L;
	LDA	r0x116F
	ORA	#0x01
	STA	r0x116F
_00106_DS_:
;	;.line	54; "_divulonglong.c"	if (reste >= y)
	SETB	_C
	LDA	r0x116F
	SUBB	__divulonglong_STK14
	LDA	r0x1170
	SUBB	__divulonglong_STK13
	LDA	r0x1171
	SUBB	__divulonglong_STK12
	LDA	r0x1172
	SUBB	__divulonglong_STK11
	LDA	r0x1173
	SUBB	__divulonglong_STK10
	LDA	r0x1174
	SUBB	__divulonglong_STK09
	LDA	r0x1175
	SUBB	__divulonglong_STK08
	LDA	r0x1176
	SUBB	__divulonglong_STK07
	JNC	_00110_DS_
;	;.line	56; "_divulonglong.c"	reste -= y;
	SETB	_C
	LDA	r0x116F
	SUBB	__divulonglong_STK14
	STA	r0x116F
	LDA	r0x1170
	SUBB	__divulonglong_STK13
	STA	r0x1170
	LDA	r0x1171
	SUBB	__divulonglong_STK12
	STA	r0x1171
	LDA	r0x1172
	SUBB	__divulonglong_STK11
	STA	r0x1172
	LDA	r0x1173
	SUBB	__divulonglong_STK10
	STA	r0x1173
	LDA	r0x1174
	SUBB	__divulonglong_STK09
	STA	r0x1174
	LDA	r0x1175
	SUBB	__divulonglong_STK08
	STA	r0x1175
	LDA	r0x1176
	SUBB	__divulonglong_STK07
	STA	r0x1176
;	;.line	58; "_divulonglong.c"	x |= 1L;
	LDA	__divulonglong_STK06
	ORA	#0x01
	STA	__divulonglong_STK06
_00110_DS_:
;	;.line	61; "_divulonglong.c"	while (--count);
	LDA	r0x1177
	DECA	
	STA	r0x1178
	STA	r0x1177
	LDA	r0x1178
	JNZ	_00109_DS_
;	;.line	62; "_divulonglong.c"	return x;
	LDA	__divulonglong_STK06
	STA	STK06
	LDA	__divulonglong_STK05
	STA	STK05
	LDA	__divulonglong_STK04
	STA	STK04
	LDA	__divulonglong_STK03
	STA	STK03
	LDA	__divulonglong_STK02
	STA	STK02
	LDA	__divulonglong_STK01
	STA	STK01
	LDA	__divulonglong_STK00
	STA	STK00
	LDA	r0x1166
;	;.line	63; "_divulonglong.c"	}
	RET	
; exit point of __divulonglong
	.ENDFUNC __divulonglong
	;--cdb--S:G$_divulonglong$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_divulonglong._divulonglong$y$65536$1({8}SI:U),R,0,0,[__divulonglong_STK14,__divulonglong_STK13,__divulonglong_STK12,__divulonglong_STK11__divulonglong_STK10__divulonglong_STK09__divulonglong_STK08__divulonglong_STK07]
	;--cdb--S:L_divulonglong._divulonglong$x$65536$1({8}SI:U),R,0,0,[__divulonglong_STK06,__divulonglong_STK05,__divulonglong_STK04,__divulonglong_STK03__divulonglong_STK02__divulonglong_STK01__divulonglong_STK00r0x1166]
	;--cdb--S:L_divulonglong._divulonglong$reste$65536$2({8}SI:U),R,0,0,[r0x116F,r0x1170,r0x1171,r0x1172r0x1173r0x1174r0x1175r0x1176]
	;--cdb--S:L_divulonglong._divulonglong$count$65536$2({1}SC:U),R,0,0,[r0x1177]
	;--cdb--S:L_divulonglong._divulonglong$c$65536$2({1}:S),R,0,0,[r0x1178]
	;--cdb--S:G$_divulonglong$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__divulonglong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__divulonglong_0	udata
r0x1166:	.ds	1
r0x116F:	.ds	1
r0x1170:	.ds	1
r0x1171:	.ds	1
r0x1172:	.ds	1
r0x1173:	.ds	1
r0x1174:	.ds	1
r0x1175:	.ds	1
r0x1176:	.ds	1
r0x1177:	.ds	1
r0x1178:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__divulonglong_STK00:	.ds	1
	.globl __divulonglong_STK00
__divulonglong_STK01:	.ds	1
	.globl __divulonglong_STK01
__divulonglong_STK02:	.ds	1
	.globl __divulonglong_STK02
__divulonglong_STK03:	.ds	1
	.globl __divulonglong_STK03
__divulonglong_STK04:	.ds	1
	.globl __divulonglong_STK04
__divulonglong_STK05:	.ds	1
	.globl __divulonglong_STK05
__divulonglong_STK06:	.ds	1
	.globl __divulonglong_STK06
__divulonglong_STK07:	.ds	1
	.globl __divulonglong_STK07
__divulonglong_STK08:	.ds	1
	.globl __divulonglong_STK08
__divulonglong_STK09:	.ds	1
	.globl __divulonglong_STK09
__divulonglong_STK10:	.ds	1
	.globl __divulonglong_STK10
__divulonglong_STK11:	.ds	1
	.globl __divulonglong_STK11
__divulonglong_STK12:	.ds	1
	.globl __divulonglong_STK12
__divulonglong_STK13:	.ds	1
	.globl __divulonglong_STK13
__divulonglong_STK14:	.ds	1
	.globl __divulonglong_STK14
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	end
