;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"asincosf.c"
	.module asincosf
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fasincosf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$asincosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$asincosf$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; asincosf-code 
.globl _asincosf

;--------------------------------------------------------
	.FUNC _asincosf:$PNUM 5:$C:_fabsf:$C:___fslt:$C:___fssub:$C:_ldexpf\
:$C:_sqrtf:$C:___fsadd:$C:___fsmul:$C:___fsdiv\
:$L:r0x1163:$L:_asincosf_STK00:$L:_asincosf_STK01:$L:_asincosf_STK02:$L:_asincosf_STK03\
:$L:r0x1165:$L:r0x1169:$L:r0x1168:$L:r0x1167:$L:r0x1166\
:$L:r0x116A:$L:r0x116B:$L:r0x116C:$L:r0x116D:$L:r0x116E\
:$L:r0x1171:$L:r0x1170:$L:r0x116F:$L:r0x1175:$L:r0x1174\
:$L:r0x1173:$L:r0x1172:$L:r0x1179
;--------------------------------------------------------
;	.line	47; "asincosf.c"	float asincosf(float x, bool isacos)
_asincosf:	;Function start
	STA	r0x1163
;	;.line	51; "asincosf.c"	bool quartPI = isacos;
	LDA	_asincosf_STK03
	STA	r0x1165
;	;.line	56; "asincosf.c"	y = fabsf(x);
	LDA	_asincosf_STK02
	STA	_fabsf_STK02
	LDA	_asincosf_STK01
	STA	_fabsf_STK01
	LDA	_asincosf_STK00
	STA	_fabsf_STK00
	LDA	r0x1163
	CALL	_fabsf
	STA	r0x1169
	LDA	STK00
	STA	r0x1168
	LDA	STK01
	STA	r0x1167
	LDA	STK02
	STA	r0x1166
;	;.line	57; "asincosf.c"	if (y < EPS)
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	LDA	#0x80
	STA	___fslt_STK04
	LDA	#0x39
	STA	___fslt_STK03
	LDA	r0x1166
	STA	___fslt_STK02
	LDA	r0x1167
	STA	___fslt_STK01
	LDA	r0x1168
	STA	___fslt_STK00
	LDA	r0x1169
	CALL	___fslt
	JZ	_00111_DS_
;	;.line	59; "asincosf.c"	r = y;
	LDA	r0x1166
	STA	r0x116A
	LDA	r0x1167
	STA	r0x116B
	LDA	r0x1168
	STA	r0x116C
	LDA	r0x1169
	STA	r0x116D
	JMP	_00112_DS_
_00111_DS_:
;	;.line	63; "asincosf.c"	if (y > 0.5)
	LDA	r0x1166
	STA	___fslt_STK06
	LDA	r0x1167
	STA	___fslt_STK05
	LDA	r0x1168
	STA	___fslt_STK04
	LDA	r0x1169
	STA	___fslt_STK03
	CLRA	
	STA	___fslt_STK02
	STA	___fslt_STK01
	STA	___fslt_STK00
	LDA	#0x3f
	CALL	___fslt
	JZ	_00108_DS_
;	;.line	65; "asincosf.c"	quartPI = !isacos;
	LDA	_asincosf_STK03
	LDC	_Z
	CLRA	
	ROL	
	STA	r0x1165
;	;.line	66; "asincosf.c"	if (y > 1.0)
	LDA	r0x1166
	STA	___fslt_STK06
	LDA	r0x1167
	STA	___fslt_STK05
	LDA	r0x1168
	STA	___fslt_STK04
	LDA	r0x1169
	STA	___fslt_STK03
	CLRA	
	STA	___fslt_STK02
	STA	___fslt_STK01
	LDA	#0x80
	STA	___fslt_STK00
	LDA	#0x3f
	CALL	___fslt
	JZ	_00106_DS_
;	;.line	68; "asincosf.c"	errno = EDOM;
	LDA	#0x21
	STA	_errno
;	;.line	69; "asincosf.c"	return 0.0;
	CLRA	
	STA	(_errno + 1)
	STA	STK02
	STA	STK01
	STA	STK00
	CLRA	
	JMP	_00121_DS_
_00106_DS_:
;	;.line	71; "asincosf.c"	g = (0.5 - y) + 0.5;
	LDA	r0x1166
	STA	___fssub_STK06
	LDA	r0x1167
	STA	___fssub_STK05
	LDA	r0x1168
	STA	___fssub_STK04
	LDA	r0x1169
	STA	___fssub_STK03
	CLRA	
	STA	___fssub_STK02
	STA	___fssub_STK01
	LDA	#0x80
	STA	___fssub_STK00
	LDA	#0x3f
	CALL	___fssub
	STA	r0x1171
;	;.line	72; "asincosf.c"	g = ldexpf(g, -1);
	LDA	#0xff
	STA	_ldexpf_STK04
	STA	_ldexpf_STK03
	LDA	STK02
	STA	_ldexpf_STK02
	LDA	STK01
	STA	_ldexpf_STK01
	LDA	STK00
	STA	_ldexpf_STK00
	LDA	r0x1171
	CALL	_ldexpf
	STA	r0x1171
	LDA	STK00
	STA	r0x1170
	LDA	STK01
	STA	r0x116F
	LDA	STK02
	STA	r0x116E
;	;.line	73; "asincosf.c"	y = sqrtf(g);
	STA	_sqrtf_STK02
	LDA	r0x116F
	STA	_sqrtf_STK01
	LDA	r0x1170
	STA	_sqrtf_STK00
	LDA	r0x1171
	CALL	_sqrtf
	STA	r0x1175
;	;.line	74; "asincosf.c"	y = -(y + y);
	LDA	STK02
	STA	___fsadd_STK06
	LDA	STK01
	STA	___fsadd_STK05
	LDA	STK00
	STA	___fsadd_STK04
	LDA	r0x1175
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1175
	CALL	___fsadd
	XOR	#0x80
	STA	r0x1169
	LDA	STK00
	STA	r0x1168
	LDA	STK01
	STA	r0x1167
	LDA	STK02
	STA	r0x1166
	JMP	_00109_DS_
_00108_DS_:
;	;.line	78; "asincosf.c"	g = y * y;
	LDA	r0x1166
	STA	___fsmul_STK06
	LDA	r0x1167
	STA	___fsmul_STK05
	LDA	r0x1168
	STA	___fsmul_STK04
	LDA	r0x1169
	STA	___fsmul_STK03
	LDA	r0x1166
	STA	___fsmul_STK02
	LDA	r0x1167
	STA	___fsmul_STK01
	LDA	r0x1168
	STA	___fsmul_STK00
	LDA	r0x1169
	CALL	___fsmul
	STA	r0x1171
	LDA	STK00
	STA	r0x1170
	LDA	STK01
	STA	r0x116F
	LDA	STK02
	STA	r0x116E
_00109_DS_:
;	;.line	80; "asincosf.c"	r = y + y * ((P(g) * g) / Q(g));
	LDA	r0x116E
	STA	___fsmul_STK06
	LDA	r0x116F
	STA	___fsmul_STK05
	LDA	r0x1170
	STA	___fsmul_STK04
	LDA	r0x1171
	STA	___fsmul_STK03
	LDA	#0x65
	STA	___fsmul_STK02
	LDA	#0x20
	STA	___fsmul_STK01
	LDA	#0x01
	STA	___fsmul_STK00
	LDA	#0xbf
	CALL	___fsmul
	STA	r0x1175
	LDA	#0x6b
	STA	___fsadd_STK06
	LDA	#0x16
	STA	___fsadd_STK05
	LDA	#0x6f
	STA	___fsadd_STK04
	LDA	#0x3f
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1175
	CALL	___fsadd
	STA	r0x1175
	LDA	r0x116E
	STA	___fsmul_STK06
	LDA	r0x116F
	STA	___fsmul_STK05
	LDA	r0x1170
	STA	___fsmul_STK04
	LDA	r0x1171
	STA	___fsmul_STK03
	LDA	STK02
	STA	___fsmul_STK02
	LDA	STK01
	STA	___fsmul_STK01
	LDA	STK00
	STA	___fsmul_STK00
	LDA	r0x1175
	CALL	___fsmul
	STA	r0x1175
	LDA	STK00
	STA	r0x1174
	LDA	STK01
	STA	r0x1173
	LDA	STK02
	STA	r0x1172
	LDA	#0x0b
	STA	___fsadd_STK06
	LDA	#0x8d
	STA	___fsadd_STK05
	LDA	#0xb1
	STA	___fsadd_STK04
	LDA	#0xc0
	STA	___fsadd_STK03
	LDA	r0x116E
	STA	___fsadd_STK02
	LDA	r0x116F
	STA	___fsadd_STK01
	LDA	r0x1170
	STA	___fsadd_STK00
	LDA	r0x1171
	CALL	___fsadd
	STA	r0x1179
	LDA	r0x116E
	STA	___fsmul_STK06
	LDA	r0x116F
	STA	___fsmul_STK05
	LDA	r0x1170
	STA	___fsmul_STK04
	LDA	r0x1171
	STA	___fsmul_STK03
	LDA	STK02
	STA	___fsmul_STK02
	LDA	STK01
	STA	___fsmul_STK01
	LDA	STK00
	STA	___fsmul_STK00
	LDA	r0x1179
	CALL	___fsmul
	STA	r0x1171
	LDA	#0xf0
	STA	___fsadd_STK06
	LDA	#0x50
	STA	___fsadd_STK05
	LDA	#0xb3
	STA	___fsadd_STK04
	LDA	#0x40
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1171
	CALL	___fsadd
	STA	r0x1171
	LDA	STK02
	STA	___fsdiv_STK06
	LDA	STK01
	STA	___fsdiv_STK05
	LDA	STK00
	STA	___fsdiv_STK04
	LDA	r0x1171
	STA	___fsdiv_STK03
	LDA	r0x1172
	STA	___fsdiv_STK02
	LDA	r0x1173
	STA	___fsdiv_STK01
	LDA	r0x1174
	STA	___fsdiv_STK00
	LDA	r0x1175
	CALL	___fsdiv
	STA	r0x1171
	LDA	STK02
	STA	___fsmul_STK06
	LDA	STK01
	STA	___fsmul_STK05
	LDA	STK00
	STA	___fsmul_STK04
	LDA	r0x1171
	STA	___fsmul_STK03
	LDA	r0x1166
	STA	___fsmul_STK02
	LDA	r0x1167
	STA	___fsmul_STK01
	LDA	r0x1168
	STA	___fsmul_STK00
	LDA	r0x1169
	CALL	___fsmul
	STA	r0x1171
	LDA	STK02
	STA	___fsadd_STK06
	LDA	STK01
	STA	___fsadd_STK05
	LDA	STK00
	STA	___fsadd_STK04
	LDA	r0x1171
	STA	___fsadd_STK03
	LDA	r0x1166
	STA	___fsadd_STK02
	LDA	r0x1167
	STA	___fsadd_STK01
	LDA	r0x1168
	STA	___fsadd_STK00
	LDA	r0x1169
	CALL	___fsadd
	STA	r0x116D
	LDA	STK00
	STA	r0x116C
	LDA	STK01
	STA	r0x116B
	LDA	STK02
	STA	r0x116A
_00112_DS_:
;	;.line	83; "asincosf.c"	if (isacos)
	LDA	_asincosf_STK03
	JZ	_00119_DS_
;	;.line	85; "asincosf.c"	if (x < 0.0)
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	STA	___fslt_STK04
	STA	___fslt_STK03
	LDA	_asincosf_STK02
	STA	___fslt_STK02
	LDA	_asincosf_STK01
	STA	___fslt_STK01
	LDA	_asincosf_STK00
	STA	___fslt_STK00
	LDA	r0x1163
	CALL	___fslt
	JZ	_00114_DS_
;	;.line	86; "asincosf.c"	r = (b[i] + r) + b[i];
	CLRA	
	STA	_MULAL
	STA	_MULBH
	LDA	r0x1165
	STA	_MULAH
	LDA	#0x04
	STA	_MULBL
	LDA	_MULO2A
	STA	_asincosf_STK03
	LDA	_MULO2B
	LDA	_asincosf_STK03
	STA	_PTRCL
	LDAT	#(_asincosf_b_65536_26 + 0)
	STA	r0x1167
	LDA	_PTRCL
	LDAT	#(_asincosf_b_65536_26 + 1)
	STA	r0x1168
	LDA	_PTRCL
	LDAT	#(_asincosf_b_65536_26 + 2)
	STA	r0x1169
	LDA	_PTRCL
	LDAT	#(_asincosf_b_65536_26 + 3)
	STA	r0x116E
	LDA	r0x116A
	STA	___fsadd_STK06
	LDA	r0x116B
	STA	___fsadd_STK05
	LDA	r0x116C
	STA	___fsadd_STK04
	LDA	r0x116D
	STA	___fsadd_STK03
	LDA	r0x1167
	STA	___fsadd_STK02
	LDA	r0x1168
	STA	___fsadd_STK01
	LDA	r0x1169
	STA	___fsadd_STK00
	LDA	r0x116E
	CALL	___fsadd
	STA	r0x1170
	LDA	r0x1167
	STA	___fsadd_STK06
	LDA	r0x1168
	STA	___fsadd_STK05
	LDA	r0x1169
	STA	___fsadd_STK04
	LDA	r0x116E
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1170
	CALL	___fsadd
	STA	r0x1168
	LDA	STK00
	STA	r0x1167
	LDA	STK01
	STA	r0x1165
	LDA	STK02
	STA	_asincosf_STK03
	JMP	_00120_DS_
_00114_DS_:
;	;.line	88; "asincosf.c"	r = (a[i] - r) + a[i];
	CLRA	
	STA	_MULAL
	STA	_MULBH
	LDA	r0x1165
	STA	_MULAH
	LDA	#0x04
	STA	_MULBL
	LDA	_MULO2A
	STA	r0x1169
	LDA	_MULO2B
	LDA	r0x1169
	STA	_PTRCL
	LDAT	#(_asincosf_a_65536_26 + 0)
	STA	r0x116F
	LDA	_PTRCL
	LDAT	#(_asincosf_a_65536_26 + 1)
	STA	r0x1170
	LDA	_PTRCL
	LDAT	#(_asincosf_a_65536_26 + 2)
	STA	r0x1171
	LDA	_PTRCL
	LDAT	#(_asincosf_a_65536_26 + 3)
	STA	r0x1172
	LDA	r0x116A
	STA	___fssub_STK06
	LDA	r0x116B
	STA	___fssub_STK05
	LDA	r0x116C
	STA	___fssub_STK04
	LDA	r0x116D
	STA	___fssub_STK03
	LDA	r0x116F
	STA	___fssub_STK02
	LDA	r0x1170
	STA	___fssub_STK01
	LDA	r0x1171
	STA	___fssub_STK00
	LDA	r0x1172
	CALL	___fssub
	STA	r0x1174
	LDA	r0x116F
	STA	___fsadd_STK06
	LDA	r0x1170
	STA	___fsadd_STK05
	LDA	r0x1171
	STA	___fsadd_STK04
	LDA	r0x1172
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1174
	CALL	___fsadd
	STA	r0x1168
	LDA	STK00
	STA	r0x1167
	LDA	STK01
	STA	r0x1165
	LDA	STK02
	STA	_asincosf_STK03
	JMP	_00120_DS_
_00119_DS_:
;	;.line	92; "asincosf.c"	r = (a[i] + r) + a[i];
	CLRA	
	STA	_MULAL
	STA	_MULBH
	LDA	r0x1165
	STA	_MULAH
	LDA	#0x04
	STA	_MULBL
	LDA	_MULO2A
	STA	r0x1169
	LDA	_MULO2B
	LDA	r0x1169
	STA	_PTRCL
	LDAT	#(_asincosf_a_65536_26 + 0)
	STA	r0x1169
	LDA	_PTRCL
	LDAT	#(_asincosf_a_65536_26 + 1)
	STA	r0x116E
	LDA	_PTRCL
	LDAT	#(_asincosf_a_65536_26 + 2)
	STA	r0x1170
	LDA	_PTRCL
	LDAT	#(_asincosf_a_65536_26 + 3)
	STA	r0x1171
	LDA	r0x116A
	STA	___fsadd_STK06
	LDA	r0x116B
	STA	___fsadd_STK05
	LDA	r0x116C
	STA	___fsadd_STK04
	LDA	r0x116D
	STA	___fsadd_STK03
	LDA	r0x1169
	STA	___fsadd_STK02
	LDA	r0x116E
	STA	___fsadd_STK01
	LDA	r0x1170
	STA	___fsadd_STK00
	LDA	r0x1171
	CALL	___fsadd
	STA	r0x116C
	LDA	r0x1169
	STA	___fsadd_STK06
	LDA	r0x116E
	STA	___fsadd_STK05
	LDA	r0x1170
	STA	___fsadd_STK04
	LDA	r0x1171
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x116C
	CALL	___fsadd
	STA	r0x1168
	LDA	STK00
	STA	r0x1167
	LDA	STK01
	STA	r0x1165
	LDA	STK02
	STA	_asincosf_STK03
;	;.line	93; "asincosf.c"	if (x < 0.0)
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	STA	___fslt_STK04
	STA	___fslt_STK03
	LDA	_asincosf_STK02
	STA	___fslt_STK02
	LDA	_asincosf_STK01
	STA	___fslt_STK01
	LDA	_asincosf_STK00
	STA	___fslt_STK00
	LDA	r0x1163
	CALL	___fslt
	JZ	_00120_DS_
;	;.line	94; "asincosf.c"	r = -r;
	LDA	r0x1168
	XOR	#0x80
	STA	r0x1168
_00120_DS_:
;	;.line	96; "asincosf.c"	return r;
	LDA	_asincosf_STK03
	STA	STK02
	LDA	r0x1165
	STA	STK01
	LDA	r0x1167
	STA	STK00
	LDA	r0x1168
_00121_DS_:
;	;.line	97; "asincosf.c"	}
	RET	
; exit point of _asincosf
	.ENDFUNC _asincosf
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$asincosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$errno$0$0({2}SI:S),E,0,0
	;--cdb--S:Lasincosf.asincosf$isacos$65536$25({1}:S),R,0,0,[_asincosf_STK03]
	;--cdb--S:Lasincosf.asincosf$x$65536$25({4}SF:S),R,0,0,[_asincosf_STK02,_asincosf_STK01,_asincosf_STK00,r0x1163]
	;--cdb--S:Lasincosf.asincosf$y$65536$26({4}SF:S),R,0,0,[r0x1172,r0x1173,r0x1174,r0x1175]
	;--cdb--S:Lasincosf.asincosf$g$65536$26({4}SF:S),R,0,0,[r0x116E,r0x116F,r0x1170,r0x1171]
	;--cdb--S:Lasincosf.asincosf$r$65536$26({4}SF:S),R,0,0,[_asincosf_STK03,r0x1165,r0x1167,r0x1168]
	;--cdb--S:Lasincosf.asincosf$i$65536$26({1}SC:U),R,0,0,[r0x1166]
	;--cdb--S:Lasincosf.asincosf$quartPI$65536$26({1}:S),R,0,0,[r0x1165]
	;--cdb--S:Lasincosf.asincosf$a$65536$26({8}DA2d,SF:S),D,0,0
	;--cdb--S:Lasincosf.asincosf$b$65536$26({8}DA2d,SF:S),D,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_sqrtf
	.globl	_fabsf
	.globl	_ldexpf
	.globl	___fslt
	.globl	___fssub
	.globl	___fsadd
	.globl	___fsmul
	.globl	___fsdiv
	.globl	_errno

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_asincosf
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_asincosf_0	udata
r0x1163:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116C:	.ds	1
r0x116D:	.ds	1
r0x116E:	.ds	1
r0x116F:	.ds	1
r0x1170:	.ds	1
r0x1171:	.ds	1
r0x1172:	.ds	1
r0x1173:	.ds	1
r0x1174:	.ds	1
r0x1175:	.ds	1
r0x1179:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_asincosf_STK00:	.ds	1
	.globl _asincosf_STK00
_asincosf_STK01:	.ds	1
	.globl _asincosf_STK01
_asincosf_STK02:	.ds	1
	.globl _asincosf_STK02
_asincosf_STK03:	.ds	1
	.globl _asincosf_STK03
	.globl _fabsf_STK02
	.globl _fabsf_STK01
	.globl _fabsf_STK00
	.globl ___fslt_STK06
	.globl ___fslt_STK05
	.globl ___fslt_STK04
	.globl ___fslt_STK03
	.globl ___fslt_STK02
	.globl ___fslt_STK01
	.globl ___fslt_STK00
	.globl ___fssub_STK06
	.globl ___fssub_STK05
	.globl ___fssub_STK04
	.globl ___fssub_STK03
	.globl ___fssub_STK02
	.globl ___fssub_STK01
	.globl ___fssub_STK00
	.globl _ldexpf_STK04
	.globl _ldexpf_STK03
	.globl _ldexpf_STK02
	.globl _ldexpf_STK01
	.globl _ldexpf_STK00
	.globl _sqrtf_STK02
	.globl _sqrtf_STK01
	.globl _sqrtf_STK00
	.globl ___fsadd_STK06
	.globl ___fsadd_STK05
	.globl ___fsadd_STK04
	.globl ___fsadd_STK03
	.globl ___fsadd_STK02
	.globl ___fsadd_STK01
	.globl ___fsadd_STK00
	.globl ___fsmul_STK06
	.globl ___fsmul_STK05
	.globl ___fsmul_STK04
	.globl ___fsmul_STK03
	.globl ___fsmul_STK02
	.globl ___fsmul_STK01
	.globl ___fsmul_STK00
	.globl ___fsdiv_STK06
	.globl ___fsdiv_STK05
	.globl ___fsdiv_STK04
	.globl ___fsdiv_STK03
	.globl ___fsdiv_STK02
	.globl ___fsdiv_STK01
	.globl ___fsdiv_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------

	.area	SEGC	(CODE) ;asincosf-0-code

_asincosf_a_65536_26:
	.byte #0x00,#0x00,#0x00,#0x00	;  0.000000e+00
	.byte #0xdb,#0x0f,#0x49,#0x3f	;  7.853982e-01


	.area	SEGC	(CODE) ;asincosf-1-code

_asincosf_b_65536_26:
	.byte #0xdb,#0x0f,#0xc9,#0x3f	;  1.570796e+00
	.byte #0xdb,#0x0f,#0x49,#0x3f	;  7.853982e-01

;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------


	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1165:NULL+0:-1:1
	;--cdb--W:r0x116E:NULL+0:-1:1
	;--cdb--W:_asincosf_STK03:NULL+0:12:0
	;--cdb--W:r0x1165:NULL+0:13:0
	;--cdb--W:r0x1169:NULL+0:12:0
	;--cdb--W:r0x1166:NULL+0:4453:0
	;--cdb--W:r0x1166:NULL+0:12:0
	;--cdb--W:r0x116A:NULL+0:13:0
	;--cdb--W:r0x116B:NULL+0:14:0
	;--cdb--W:r0x116E:NULL+0:12:0
	;--cdb--W:r0x116E:NULL+0:13:0
	;--cdb--W:r0x1170:NULL+0:14:0
	;--cdb--W:r0x116F:NULL+0:13:0
	;--cdb--W:r0x116F:NULL+0:14:0
	;--cdb--W:r0x1174:NULL+0:14:0
	;--cdb--W:r0x1173:NULL+0:13:0
	;--cdb--W:r0x1173:NULL+0:14:0
	;--cdb--W:r0x1172:NULL+0:12:0
	;--cdb--W:r0x1178:NULL+0:14:0
	;--cdb--W:r0x1177:NULL+0:13:0
	;--cdb--W:r0x1176:NULL+0:12:0
	;--cdb--W:r0x116A:NULL+0:-1:1
	;--cdb--W:r0x1175:NULL+0:-1:1
	;--cdb--W:_asincosf_STK03:NULL+0:-1:1
	;--cdb--W:_asincosf_STK02:NULL+0:-1:1
	end
