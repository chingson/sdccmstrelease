;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"sincoshf.c"
	.module sincoshf
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fsincoshf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$sincoshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$sincoshf$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; sincoshf-code 
.globl _sincoshf

;--------------------------------------------------------
	.FUNC _sincoshf:$PNUM 5:$C:___fslt:$C:___fssub:$C:_expf:$C:___fsmul\
:$C:___fsadd:$C:___fsdiv\
:$L:r0x1163:$L:_sincoshf_STK00:$L:_sincoshf_STK01:$L:_sincoshf_STK02:$L:_sincoshf_STK03\
:$L:r0x1165:$L:r0x1168:$L:r0x1167:$L:r0x1166:$L:r0x1169\
:$L:r0x116A:$L:r0x116D:$L:r0x116C:$L:r0x116B:$L:r0x116E\
:$L:r0x116F:$L:r0x1170:$L:r0x1171:$L:r0x1175:$L:r0x1173\

;--------------------------------------------------------
;	.line	56; "sincoshf.c"	float sincoshf(float x, bool iscosh)
_sincoshf:	;Function start
	STA	r0x1163
;	;.line	61; "sincoshf.c"	if (x<0.0) { y=-x; sign=1; }
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	STA	___fslt_STK04
	STA	___fslt_STK03
	LDA	_sincoshf_STK02
	STA	___fslt_STK02
	LDA	_sincoshf_STK01
	STA	___fslt_STK01
	LDA	_sincoshf_STK00
	STA	___fslt_STK00
	LDA	r0x1163
	CALL	___fslt
	JZ	_00106_DS_
	LDA	r0x1163
	XOR	#0x80
	STA	r0x1168
	LDA	_sincoshf_STK00
	STA	r0x1167
	LDA	_sincoshf_STK01
	STA	r0x1166
	LDA	_sincoshf_STK02
	STA	r0x1165
	LDA	#0x01
	STA	r0x1169
	JMP	_00107_DS_
_00106_DS_:
;	;.line	62; "sincoshf.c"	else { y=x;  sign=0; }
	LDA	_sincoshf_STK02
	STA	r0x1165
	LDA	_sincoshf_STK01
	STA	r0x1166
	LDA	_sincoshf_STK00
	STA	r0x1167
	LDA	r0x1163
	STA	r0x1168
	CLRA	
	STA	r0x1169
_00107_DS_:
;	;.line	64; "sincoshf.c"	if ((y>1.0) || iscosh)
	LDA	r0x1165
	STA	___fslt_STK06
	LDA	r0x1166
	STA	___fslt_STK05
	LDA	r0x1167
	STA	___fslt_STK04
	LDA	r0x1168
	STA	___fslt_STK03
	CLRA	
	STA	___fslt_STK02
	STA	___fslt_STK01
	LDA	#0x80
	STA	___fslt_STK00
	LDA	#0x3f
	CALL	___fslt
	JNZ	_00121_DS_
	LDA	_sincoshf_STK03
	JZ	_00122_DS_
_00121_DS_:
;	;.line	66; "sincoshf.c"	if(y>YBAR)
	LDA	r0x1165
	STA	___fslt_STK06
	LDA	r0x1166
	STA	___fslt_STK05
	LDA	r0x1167
	STA	___fslt_STK04
	LDA	r0x1168
	STA	___fslt_STK03
	CLRA	
	STA	___fslt_STK02
	STA	___fslt_STK01
	LDA	#0x10
	STA	___fslt_STK00
	LDA	#0x41
	CALL	___fslt
	JZ	_00114_DS_
;	;.line	68; "sincoshf.c"	w=y-K1;
	CLRA	
	STA	___fssub_STK06
	LDA	#0x73
	STA	___fssub_STK05
	LDA	#0x31
	STA	___fssub_STK04
	LDA	#0x3f
	STA	___fssub_STK03
	LDA	r0x1165
	STA	___fssub_STK02
	LDA	r0x1166
	STA	___fssub_STK01
	LDA	r0x1167
	STA	___fssub_STK00
	LDA	r0x1168
	CALL	___fssub
	STA	r0x116D
	LDA	STK00
	STA	r0x116C
	LDA	STK01
	STA	r0x116B
	LDA	STK02
	STA	r0x116A
;	;.line	69; "sincoshf.c"	if (w>WMAX)
	STA	___fslt_STK06
	LDA	r0x116B
	STA	___fslt_STK05
	LDA	r0x116C
	STA	___fslt_STK04
	LDA	r0x116D
	STA	___fslt_STK03
	LDA	#0xcf
	STA	___fslt_STK02
	LDA	#0xbd
	STA	___fslt_STK01
	LDA	#0x33
	STA	___fslt_STK00
	LDA	#0x42
	CALL	___fslt
	JZ	_00109_DS_
;	;.line	71; "sincoshf.c"	errno=ERANGE;
	LDA	#0x22
	STA	_errno
	CLRA	
	STA	(_errno + 1)
;	;.line	72; "sincoshf.c"	z=HUGE_VALF;
	DECA	
	STA	r0x116E
	STA	r0x116F
	LDA	#0x7f
	STA	r0x1170
	STA	r0x1171
	JMP	_00115_DS_
_00109_DS_:
;	;.line	76; "sincoshf.c"	z=expf(w);
	LDA	r0x116A
	STA	_expf_STK02
	LDA	r0x116B
	STA	_expf_STK01
	LDA	r0x116C
	STA	_expf_STK00
	LDA	r0x116D
	CALL	_expf
	STA	r0x116D
	LDA	STK00
	STA	r0x116C
	LDA	STK01
	STA	r0x116B
	LDA	STK02
	STA	r0x116A
;	;.line	77; "sincoshf.c"	z+=K3*z;
	STA	___fsmul_STK06
	LDA	r0x116B
	STA	___fsmul_STK05
	LDA	r0x116C
	STA	___fsmul_STK04
	LDA	r0x116D
	STA	___fsmul_STK03
	LDA	#0x97
	STA	___fsmul_STK02
	LDA	#0x08
	STA	___fsmul_STK01
	LDA	#0x68
	STA	___fsmul_STK00
	LDA	#0x37
	CALL	___fsmul
	STA	r0x1175
	LDA	STK02
	STA	___fsadd_STK06
	LDA	STK01
	STA	___fsadd_STK05
	LDA	STK00
	STA	___fsadd_STK04
	LDA	r0x1175
	STA	___fsadd_STK03
	LDA	r0x116A
	STA	___fsadd_STK02
	LDA	r0x116B
	STA	___fsadd_STK01
	LDA	r0x116C
	STA	___fsadd_STK00
	LDA	r0x116D
	CALL	___fsadd
	STA	r0x1171
	LDA	STK00
	STA	r0x1170
	LDA	STK01
	STA	r0x116F
	LDA	STK02
	STA	r0x116E
	JMP	_00115_DS_
_00114_DS_:
;	;.line	82; "sincoshf.c"	z=expf(y);
	LDA	r0x1165
	STA	_expf_STK02
	LDA	r0x1166
	STA	_expf_STK01
	LDA	r0x1167
	STA	_expf_STK00
	LDA	r0x1168
	CALL	_expf
	STA	r0x116D
	LDA	STK00
	STA	r0x116C
	LDA	STK01
	STA	r0x116B
	LDA	STK02
	STA	r0x116A
;	;.line	83; "sincoshf.c"	w=1.0/z;
	STA	___fsdiv_STK06
	LDA	r0x116B
	STA	___fsdiv_STK05
	LDA	r0x116C
	STA	___fsdiv_STK04
	LDA	r0x116D
	STA	___fsdiv_STK03
	CLRA	
	STA	___fsdiv_STK02
	STA	___fsdiv_STK01
	LDA	#0x80
	STA	___fsdiv_STK00
	LDA	#0x3f
	CALL	___fsdiv
	STA	r0x1175
;	;.line	84; "sincoshf.c"	if(!iscosh) w=-w;
	LDA	_sincoshf_STK03
	JNZ	_00112_DS_
	LDA	r0x1175
	XOR	#0x80
	STA	r0x1175
_00112_DS_:
;	;.line	85; "sincoshf.c"	z=(z+w)*0.5;
	LDA	STK02
	STA	___fsadd_STK06
	LDA	STK01
	STA	___fsadd_STK05
	LDA	STK00
	STA	___fsadd_STK04
	LDA	r0x1175
	STA	___fsadd_STK03
	LDA	r0x116A
	STA	___fsadd_STK02
	LDA	r0x116B
	STA	___fsadd_STK01
	LDA	r0x116C
	STA	___fsadd_STK00
	LDA	r0x116D
	CALL	___fsadd
	STA	r0x116C
	LDA	STK02
	STA	___fsmul_STK06
	LDA	STK01
	STA	___fsmul_STK05
	LDA	STK00
	STA	___fsmul_STK04
	LDA	r0x116C
	STA	___fsmul_STK03
	CLRA	
	STA	___fsmul_STK02
	STA	___fsmul_STK01
	STA	___fsmul_STK00
	LDA	#0x3f
	CALL	___fsmul
	STA	r0x1171
	LDA	STK00
	STA	r0x1170
	LDA	STK01
	STA	r0x116F
	LDA	STK02
	STA	r0x116E
_00115_DS_:
;	;.line	87; "sincoshf.c"	if(sign) z=-z;
	LDA	r0x1169
	JZ	_00123_DS_
	LDA	r0x1171
	XOR	#0x80
	STA	r0x1171
	LDA	r0x116E
	JMP	_00123_DS_
_00122_DS_:
;	;.line	91; "sincoshf.c"	if (y<EPS)
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	LDA	#0x80
	STA	___fslt_STK04
	LDA	#0x39
	STA	___fslt_STK03
	LDA	r0x1165
	STA	___fslt_STK02
	LDA	r0x1166
	STA	___fslt_STK01
	LDA	r0x1167
	STA	___fslt_STK00
	LDA	r0x1168
	CALL	___fslt
	JZ	_00119_DS_
;	;.line	92; "sincoshf.c"	z=x;
	LDA	_sincoshf_STK02
	STA	r0x116E
	LDA	_sincoshf_STK01
	STA	r0x116F
	LDA	_sincoshf_STK00
	STA	r0x1170
	LDA	r0x1163
	STA	r0x1171
	JMP	_00123_DS_
_00119_DS_:
;	;.line	95; "sincoshf.c"	z=x*x;
	LDA	_sincoshf_STK02
	STA	___fsmul_STK06
	LDA	_sincoshf_STK01
	STA	___fsmul_STK05
	LDA	_sincoshf_STK00
	STA	___fsmul_STK04
	LDA	r0x1163
	STA	___fsmul_STK03
	LDA	_sincoshf_STK02
	STA	___fsmul_STK02
	LDA	_sincoshf_STK01
	STA	___fsmul_STK01
	LDA	_sincoshf_STK00
	STA	___fsmul_STK00
	LDA	r0x1163
	CALL	___fsmul
	STA	r0x1167
	LDA	STK00
	STA	r0x1166
	LDA	STK01
	STA	r0x1165
	LDA	STK02
	STA	_sincoshf_STK03
;	;.line	96; "sincoshf.c"	z=x+x*z*P(z)/Q(z);
	STA	___fsmul_STK06
	LDA	r0x1165
	STA	___fsmul_STK05
	LDA	r0x1166
	STA	___fsmul_STK04
	LDA	r0x1167
	STA	___fsmul_STK03
	LDA	_sincoshf_STK02
	STA	___fsmul_STK02
	LDA	_sincoshf_STK01
	STA	___fsmul_STK01
	LDA	_sincoshf_STK00
	STA	___fsmul_STK00
	LDA	r0x1163
	CALL	___fsmul
	STA	r0x116B
	LDA	STK00
	STA	r0x116A
	LDA	STK01
	STA	r0x1169
	LDA	STK02
	STA	r0x1168
	LDA	_sincoshf_STK03
	STA	___fsmul_STK06
	LDA	r0x1165
	STA	___fsmul_STK05
	LDA	r0x1166
	STA	___fsmul_STK04
	LDA	r0x1167
	STA	___fsmul_STK03
	LDA	#0xea
	STA	___fsmul_STK02
	LDA	#0xe6
	STA	___fsmul_STK01
	LDA	#0x42
	STA	___fsmul_STK00
	LDA	#0xbe
	CALL	___fsmul
	STA	r0x1173
	LDA	#0xf0
	STA	___fsadd_STK06
	LDA	#0x69
	STA	___fsadd_STK05
	LDA	#0xe4
	STA	___fsadd_STK04
	LDA	#0xc0
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1173
	CALL	___fsadd
	STA	r0x1173
	LDA	STK02
	STA	___fsmul_STK06
	LDA	STK01
	STA	___fsmul_STK05
	LDA	STK00
	STA	___fsmul_STK04
	LDA	r0x1173
	STA	___fsmul_STK03
	LDA	r0x1168
	STA	___fsmul_STK02
	LDA	r0x1169
	STA	___fsmul_STK01
	LDA	r0x116A
	STA	___fsmul_STK00
	LDA	r0x116B
	CALL	___fsmul
	STA	r0x116B
	LDA	STK00
	STA	r0x116A
	LDA	STK01
	STA	r0x1169
	LDA	STK02
	STA	r0x1168
	LDA	#0x93
	STA	___fsadd_STK06
	LDA	#0x4f
	STA	___fsadd_STK05
	LDA	#0x2b
	STA	___fsadd_STK04
	LDA	#0xc2
	STA	___fsadd_STK03
	LDA	_sincoshf_STK03
	STA	___fsadd_STK02
	LDA	r0x1165
	STA	___fsadd_STK01
	LDA	r0x1166
	STA	___fsadd_STK00
	LDA	r0x1167
	CALL	___fsadd
	STA	r0x1167
	LDA	STK02
	STA	___fsdiv_STK06
	LDA	STK01
	STA	___fsdiv_STK05
	LDA	STK00
	STA	___fsdiv_STK04
	LDA	r0x1167
	STA	___fsdiv_STK03
	LDA	r0x1168
	STA	___fsdiv_STK02
	LDA	r0x1169
	STA	___fsdiv_STK01
	LDA	r0x116A
	STA	___fsdiv_STK00
	LDA	r0x116B
	CALL	___fsdiv
	STA	r0x1167
	LDA	STK02
	STA	___fsadd_STK06
	LDA	STK01
	STA	___fsadd_STK05
	LDA	STK00
	STA	___fsadd_STK04
	LDA	r0x1167
	STA	___fsadd_STK03
	LDA	_sincoshf_STK02
	STA	___fsadd_STK02
	LDA	_sincoshf_STK01
	STA	___fsadd_STK01
	LDA	_sincoshf_STK00
	STA	___fsadd_STK00
	LDA	r0x1163
	CALL	___fsadd
	STA	r0x1171
	LDA	STK00
	STA	r0x1170
	LDA	STK01
	STA	r0x116F
	LDA	STK02
	STA	r0x116E
_00123_DS_:
;	;.line	99; "sincoshf.c"	return z;
	LDA	r0x116E
	STA	STK02
	LDA	r0x116F
	STA	STK01
	LDA	r0x1170
	STA	STK00
	LDA	r0x1171
;	;.line	100; "sincoshf.c"	}
	RET	
; exit point of _sincoshf
	.ENDFUNC _sincoshf
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$sincoshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$errno$0$0({2}SI:S),E,0,0
	;--cdb--S:Lsincoshf.sincoshf$iscosh$65536$25({1}:S),R,0,0,[_sincoshf_STK03]
	;--cdb--S:Lsincoshf.sincoshf$x$65536$25({4}SF:S),R,0,0,[_sincoshf_STK02,_sincoshf_STK01,_sincoshf_STK00,r0x1163]
	;--cdb--S:Lsincoshf.sincoshf$y$65536$26({4}SF:S),R,0,0,[r0x1165,r0x1166,r0x1167,r0x1168]
	;--cdb--S:Lsincoshf.sincoshf$w$65536$26({4}SF:S),R,0,0,[r0x1172,r0x1173,r0x1174,r0x1175]
	;--cdb--S:Lsincoshf.sincoshf$z$65536$26({4}SF:S),R,0,0,[r0x116A,r0x116B,r0x116C,r0x116D]
	;--cdb--S:Lsincoshf.sincoshf$sign$65536$26({1}:S),R,0,0,[r0x1169]
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_expf
	.globl	___fslt
	.globl	___fssub
	.globl	___fsmul
	.globl	___fsadd
	.globl	___fsdiv
	.globl	_errno

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_sincoshf
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_sincoshf_0	udata
r0x1163:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116C:	.ds	1
r0x116D:	.ds	1
r0x116E:	.ds	1
r0x116F:	.ds	1
r0x1170:	.ds	1
r0x1171:	.ds	1
r0x1173:	.ds	1
r0x1175:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_sincoshf_STK00:	.ds	1
	.globl _sincoshf_STK00
_sincoshf_STK01:	.ds	1
	.globl _sincoshf_STK01
_sincoshf_STK02:	.ds	1
	.globl _sincoshf_STK02
_sincoshf_STK03:	.ds	1
	.globl _sincoshf_STK03
	.globl ___fslt_STK06
	.globl ___fslt_STK05
	.globl ___fslt_STK04
	.globl ___fslt_STK03
	.globl ___fslt_STK02
	.globl ___fslt_STK01
	.globl ___fslt_STK00
	.globl ___fssub_STK06
	.globl ___fssub_STK05
	.globl ___fssub_STK04
	.globl ___fssub_STK03
	.globl ___fssub_STK02
	.globl ___fssub_STK01
	.globl ___fssub_STK00
	.globl _expf_STK02
	.globl _expf_STK01
	.globl _expf_STK00
	.globl ___fsmul_STK06
	.globl ___fsmul_STK05
	.globl ___fsmul_STK04
	.globl ___fsmul_STK03
	.globl ___fsmul_STK02
	.globl ___fsmul_STK01
	.globl ___fsmul_STK00
	.globl ___fsadd_STK06
	.globl ___fsadd_STK05
	.globl ___fsadd_STK04
	.globl ___fsadd_STK03
	.globl ___fsadd_STK02
	.globl ___fsadd_STK01
	.globl ___fsadd_STK00
	.globl ___fsdiv_STK06
	.globl ___fsdiv_STK05
	.globl ___fsdiv_STK04
	.globl ___fsdiv_STK03
	.globl ___fsdiv_STK02
	.globl ___fsdiv_STK01
	.globl ___fsdiv_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:_sincoshf_STK03:NULL+0:12:0
	;--cdb--W:r0x1165:NULL+0:13:0
	;--cdb--W:r0x1166:NULL+0:14:0
	;--cdb--W:r0x116A:NULL+0:13:0
	;--cdb--W:r0x116D:NULL+0:13:0
	;--cdb--W:r0x116C:NULL+0:12:0
	;--cdb--W:r0x116B:NULL+0:14:0
	;--cdb--W:r0x1174:NULL+0:14:0
	;--cdb--W:r0x1173:NULL+0:13:0
	;--cdb--W:r0x1172:NULL+0:12:0
	;--cdb--W:r0x1172:NULL+0:14:0
	;--cdb--W:r0x1165:NULL+0:-1:1
	;--cdb--W:r0x116A:NULL+0:-1:1
	;--cdb--W:r0x116E:NULL+0:-1:1
	;--cdb--W:_sincoshf_STK03:NULL+0:-1:1
	end
