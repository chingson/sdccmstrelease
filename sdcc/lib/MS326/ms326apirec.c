
// MS326 recording library function
#include"ms326.h"
#include"ms326typedef.h"
#include"ms326sphlib.h"

extern USHORT api_endpage;
extern BYTE api_mode;
extern BYTE api_rampage;
extern BYTE api_fifostart;// unit in 16 bytes
extern BYTE api_fifoend;
extern BYTE api_maxrecopg;
extern api_spi_addr_t api_rec_spi_addr;

//extern signed short api_offset0dbg;
// prepare for recording
// rec fifo is the fifo address
// if SPI recording, this address must be 512 byte aliged
// FIFOLEN unit is in 16 byte

void api_rec_prepare(BYTE rate, BYTE opag, BYTE fg, BYTE en5k, BYTE spkcv, BYTE *fifostart, BYTE fifolen)
{
    //BYTE api_rec_end_16;
    unsigned char ratediv=rate>>2;
    rate=rate&3;
    api_clear_filter_mem(0); // recording alreays

    // skcmd must be 0!!!

    FILTERGR=fg;
    DMA_IL=0xff;

    api_fifostart=(((USHORT)fifostart)&0x7ff)>>4; // unit is 16byte , usually this is 0x50, msb skip!!
    api_fifoend=api_fifostart+fifolen-1; // we use XF as the page addr 0x6f
    ADP_IND=0x80;// following is 8 bit wide
    RPAGES=(api_fifostart&0xf)|((api_fifoend&0xf)<<4); // f0
    ADP_IND=0;// following is 6 bits only
    RPAGES=(api_fifostart>>4)|((api_fifoend&0xf0)>>1);// 35
    RDMAH=0x80; // clear dma address here!!


    //FILTERGR=0x80; // for DC cancel
    LVDCON|=en5k;
    SPKC=(SPKC&0xcf)|spkcv;
    ADCG=opag;
    api_maxrecopg=opag;
    SYSC2|=SYSC2_BIT_MBIAS; // mbias ON
    if(rate==API_ADC_OSR128) // osr high
    {
        if(ratediv)
            RCLKDIV=ratediv;
        else
            RCLKDIV=7;
        ADCON=0x41;
    } else if(rate==API_ADC_OSR256)
    {
        if(ratediv)
            RCLKDIV=ratediv;
        else
            RCLKDIV=3;
        SYSC2|=0x20;
        ADCON=0x41;
    } else
    {
        if(ratediv)
            RCLKDIV=ratediv;
        ADCON=1;
    }

    ADCON|=0x3a; // give DMA should be OK, for full duplex, halt at 7fe
    if(SYSC&SYSC_BIT_SKCMD)
        SYSC&=~SYSC_BIT_SKCMD;

}


// fifop is the fifo pointer
// default we use

BYTE api_rec_start( BYTE recmode, USHORT page_start, USHORT page_end ,BYTE (*callback)(void))
{
    SYSC2|=SYSC2_BIT_DMAI7; // match 7 bit is ok
    if(recmode&API_RECMODE_DMI)
    {
        // off the mic switch
        BGCON|=0x10;
        DMICON|=0x80;
    } else
    {
        BGCON&=0xef;
        L2USH=0;
    }


    DCLAMP=(DCLAMP&0xFE)|0xf8;



    if(recmode&1)
    {
        ULAWC|=0xC0;


    } else if (!(recmode&API_RECMODE_16B))
    {
        ULAWC&=0x3f;
        ADCON|=0x04;
    }





    SPIH=page_start>>8;
    SPIM=page_start&0xff;
    api_endpage=page_end;
    api_mode=recmode;
    SPIL=0;
    SPIOP=1; // erase it
    api_rec_spi_addr.addrhm=SPIMH;
    api_rec_spi_addr.addrl=SPIL;
    if(api_mode&API_RECMODE_ERASE_SUSP)
    {
        SPIOP=0x82; // erase and recording at the same time
        while(SPIOP&1) // sus now, we re-check all back
        {
            if(callback!=NULL)
                callback();
        }
    }
    else
        SPIOP=2; // no suspend, for first sector, because no play yet

    RDMAH=0x80;
    ADCON|=0x2; // DMA enable now, wakeup enable, too

    api_rampage=api_fifostart; // 16 byte align
    GIF=0x7f;

    return 1;

}

void api_rec_stop(BYTE add_end_code)
{
    ADCON=0;
    BGCON=0;
    SYSC2&=0x7f ; // mbias off
    GIF=0x7f;
    while(SPIOP&1) // now in suspend mode
    {

        api_enter_stdby_mode(0,0,0,0,0);// use timer wakeup
    }

    ULAWC&=0x1f;
    // erase more sector if at final sector
    if(!add_end_code)
        return;
    if((SPIM&0xf)!=0x0f)
        return;
    SPIOP=0x20;
    if(SPIM==(BYTE)(api_endpage&0xff) && SPIH==(BYTE)(api_endpage>>8))
        return;

    SPIOP=1;
    if(api_mode & API_RECMODE_ERASE_SUSP)
    {
        SPIOP=0x82;
        while(SPIOP&1)
            api_enter_stdby_mode(0,0,0,0,0);
        return;
    }
    SPIOP=2;
}


