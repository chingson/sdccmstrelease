;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.3.0 #8604 (Sep 15 2014) (MSVC)
; This file was generated Sat Oct 11 21:41:17 2014
;--------------------------------------------------------
; MST port for the MS322 series simple CPU
;--------------------------------------------------------
	.module _shr_schar
	;.list	p=MS205
	;.radix dec
	.include "ms326sfr.def"
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
;--------------------------------------------------------
; Code Head 
;--------------------------------------------------------
	.area CSEG	 (CODE) 
;--------------------------------------------------------
; code
;--------------------------------------------------------
; sh schar, assembly is the source!!
; input: _PTRCL is the data byte
; input: ACC is the shift num
.FUNC __shr_schar

__shr_schar:	;Function start
; 2 exit points
	sta 	_ROMPL ; temp var
	add	#0xf8
	jc	shift_too_large
	lda	_ROMPL
loop_shr:
	jnz	cont_shift
	ret
cont_shift:
	lda	_PTRCL
	shrs
	sta	_PTRCL
	lda	_ROMPL
	deca
	sta	_ROMPL
	jmp	loop_shr

	
shift_too_large:
	lda	_PTRCL
	jmi	givem1
	clra
done:
	sta	_PTRCL
	ret
givem1:
	lda	#0xff
	jmp	done
	

; exit point of _shr_schar


;	code size estimation:
;	   21+   10 =    31 instructions (   31 byte)

;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__shr_schar

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	;end
