;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"tancotf.c"
	.module tancotf
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Ftancotf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tancotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$tancotf$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; tancotf-code 
.globl _tancotf

;--------------------------------------------------------
	.FUNC _tancotf:$PNUM 5:$C:_fabsf:$C:___fslt:$C:___fsmul:$C:___fsadd\
:$C:___fs2sint:$C:___sint2fs:$C:___fssub:$C:___fsdiv\
:$L:r0x1163:$L:_tancotf_STK00:$L:_tancotf_STK01:$L:_tancotf_STK02:$L:_tancotf_STK03\
:$L:r0x1168:$L:r0x1167:$L:r0x1166:$L:r0x1165:$L:r0x1169\
:$L:r0x116A:$L:r0x116B:$L:r0x116C:$L:r0x116E:$L:r0x116D\
:$L:r0x1172:$L:r0x1171:$L:r0x1170:$L:r0x116F:$L:r0x1176\

;--------------------------------------------------------
;	.line	53; "tancotf.c"	float tancotf(float x, bool iscotan)
_tancotf:	;Function start
	STA	r0x1163
;	;.line	58; "tancotf.c"	if (fabsf(x) > YMAX)
	LDA	_tancotf_STK02
	STA	_fabsf_STK02
	LDA	_tancotf_STK01
	STA	_fabsf_STK01
	LDA	_tancotf_STK00
	STA	_fabsf_STK00
	LDA	r0x1163
	CALL	_fabsf
	STA	r0x1168
	LDA	STK02
	STA	___fslt_STK06
	LDA	STK01
	STA	___fslt_STK05
	LDA	STK00
	STA	___fslt_STK04
	LDA	r0x1168
	STA	___fslt_STK03
	CLRA	
	STA	___fslt_STK02
	LDA	#0x08
	STA	___fslt_STK01
	LDA	#0xc9
	STA	___fslt_STK00
	LDA	#0x45
	CALL	___fslt
	JZ	_00106_DS_
;	;.line	60; "tancotf.c"	errno = ERANGE;
	LDA	#0x22
	STA	_errno
;	;.line	61; "tancotf.c"	return 0.0;
	CLRA	
	STA	(_errno + 1)
	STA	STK02
	STA	STK01
	STA	STK00
	CLRA	
	JMP	_00119_DS_
_00106_DS_:
;	;.line	65; "tancotf.c"	n=(x*TWO_O_PI+(x>0.0?0.5:-0.5)); /*works for +-x*/
	LDA	_tancotf_STK02
	STA	___fsmul_STK06
	LDA	_tancotf_STK01
	STA	___fsmul_STK05
	LDA	_tancotf_STK00
	STA	___fsmul_STK04
	LDA	r0x1163
	STA	___fsmul_STK03
	LDA	#0x83
	STA	___fsmul_STK02
	LDA	#0xf9
	STA	___fsmul_STK01
	LDA	#0x22
	STA	___fsmul_STK00
	LDA	#0x3f
	CALL	___fsmul
	STA	r0x1168
	LDA	STK00
	STA	r0x1167
	LDA	STK01
	STA	r0x1166
	LDA	STK02
	STA	r0x1165
	LDA	_tancotf_STK02
	STA	___fslt_STK06
	LDA	_tancotf_STK01
	STA	___fslt_STK05
	LDA	_tancotf_STK00
	STA	___fslt_STK04
	LDA	r0x1163
	STA	___fslt_STK03
	CLRA	
	STA	___fslt_STK02
	STA	___fslt_STK01
	STA	___fslt_STK00
	CALL	___fslt
	JZ	_00121_DS_
	CLRA	
	STA	r0x1169
	STA	r0x116A
	STA	r0x116B
	LDA	#0x3f
	STA	r0x116C
	JMP	_00122_DS_
_00121_DS_:
	CLRA	
	STA	r0x1169
	STA	r0x116A
	STA	r0x116B
	LDA	#0xbf
	STA	r0x116C
_00122_DS_:
	LDA	r0x1169
	STA	___fsadd_STK06
	LDA	r0x116A
	STA	___fsadd_STK05
	LDA	r0x116B
	STA	___fsadd_STK04
	LDA	r0x116C
	STA	___fsadd_STK03
	LDA	r0x1165
	STA	___fsadd_STK02
	LDA	r0x1166
	STA	___fsadd_STK01
	LDA	r0x1167
	STA	___fsadd_STK00
	LDA	r0x1168
	CALL	___fsadd
	STA	r0x1168
	LDA	STK02
	STA	___fs2sint_STK02
	LDA	STK01
	STA	___fs2sint_STK01
	LDA	STK00
	STA	___fs2sint_STK00
	LDA	r0x1168
	CALL	___fs2sint
	STA	r0x1166
	LDA	STK00
	STA	r0x1165
;	;.line	66; "tancotf.c"	xn=n;
	STA	___sint2fs_STK00
	LDA	r0x1166
	CALL	___sint2fs
	STA	r0x116A
	LDA	STK00
	STA	r0x1169
	LDA	STK01
	STA	r0x1168
	LDA	STK02
	STA	r0x1167
;	;.line	68; "tancotf.c"	xnum=(int)x;
	LDA	_tancotf_STK02
	STA	___fs2sint_STK02
	LDA	_tancotf_STK01
	STA	___fs2sint_STK01
	LDA	_tancotf_STK00
	STA	___fs2sint_STK00
	LDA	r0x1163
	CALL	___fs2sint
	STA	r0x116C
	LDA	STK00
	STA	___sint2fs_STK00
	LDA	r0x116C
	CALL	___sint2fs
	STA	r0x116E
	LDA	STK00
	STA	r0x116D
	LDA	STK01
	STA	r0x116C
	LDA	STK02
	STA	r0x116B
;	;.line	69; "tancotf.c"	xden=x-xnum;
	STA	___fssub_STK06
	LDA	r0x116C
	STA	___fssub_STK05
	LDA	r0x116D
	STA	___fssub_STK04
	LDA	r0x116E
	STA	___fssub_STK03
	LDA	_tancotf_STK02
	STA	___fssub_STK02
	LDA	_tancotf_STK01
	STA	___fssub_STK01
	LDA	_tancotf_STK00
	STA	___fssub_STK00
	LDA	r0x1163
	CALL	___fssub
	STA	r0x1163
	LDA	STK00
	STA	_tancotf_STK00
	LDA	STK01
	STA	_tancotf_STK01
	LDA	STK02
	STA	_tancotf_STK02
;	;.line	70; "tancotf.c"	f=((xnum-xn*C1)+xden)-xn*C2;
	LDA	r0x1167
	STA	___fsmul_STK06
	LDA	r0x1168
	STA	___fsmul_STK05
	LDA	r0x1169
	STA	___fsmul_STK04
	LDA	r0x116A
	STA	___fsmul_STK03
	CLRA	
	STA	___fsmul_STK02
	STA	___fsmul_STK01
	LDA	#0xc9
	STA	___fsmul_STK00
	LDA	#0x3f
	CALL	___fsmul
	STA	r0x1172
	LDA	STK02
	STA	___fssub_STK06
	LDA	STK01
	STA	___fssub_STK05
	LDA	STK00
	STA	___fssub_STK04
	LDA	r0x1172
	STA	___fssub_STK03
	LDA	r0x116B
	STA	___fssub_STK02
	LDA	r0x116C
	STA	___fssub_STK01
	LDA	r0x116D
	STA	___fssub_STK00
	LDA	r0x116E
	CALL	___fssub
	STA	r0x116E
	LDA	_tancotf_STK02
	STA	___fsadd_STK06
	LDA	_tancotf_STK01
	STA	___fsadd_STK05
	LDA	_tancotf_STK00
	STA	___fsadd_STK04
	LDA	r0x1163
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x116E
	CALL	___fsadd
	STA	r0x1163
	LDA	STK00
	STA	_tancotf_STK00
	LDA	STK01
	STA	_tancotf_STK01
	LDA	STK02
	STA	_tancotf_STK02
	LDA	r0x1167
	STA	___fsmul_STK06
	LDA	r0x1168
	STA	___fsmul_STK05
	LDA	r0x1169
	STA	___fsmul_STK04
	LDA	r0x116A
	STA	___fsmul_STK03
	LDA	#0x22
	STA	___fsmul_STK02
	LDA	#0xaa
	STA	___fsmul_STK01
	LDA	#0xfd
	STA	___fsmul_STK00
	LDA	#0x39
	CALL	___fsmul
	STA	r0x116A
	LDA	STK02
	STA	___fssub_STK06
	LDA	STK01
	STA	___fssub_STK05
	LDA	STK00
	STA	___fssub_STK04
	LDA	r0x116A
	STA	___fssub_STK03
	LDA	_tancotf_STK02
	STA	___fssub_STK02
	LDA	_tancotf_STK01
	STA	___fssub_STK01
	LDA	_tancotf_STK00
	STA	___fssub_STK00
	LDA	r0x1163
	CALL	___fssub
	STA	r0x1163
	LDA	STK00
	STA	_tancotf_STK00
	LDA	STK01
	STA	_tancotf_STK01
	LDA	STK02
	STA	_tancotf_STK02
;	;.line	72; "tancotf.c"	if (fabsf(f) < EPS)
	STA	_fabsf_STK02
	LDA	_tancotf_STK01
	STA	_fabsf_STK01
	LDA	_tancotf_STK00
	STA	_fabsf_STK00
	LDA	r0x1163
	CALL	_fabsf
	STA	r0x116A
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	LDA	#0x80
	STA	___fslt_STK04
	LDA	#0x39
	STA	___fslt_STK03
	LDA	STK02
	STA	___fslt_STK02
	LDA	STK01
	STA	___fslt_STK01
	LDA	STK00
	STA	___fslt_STK00
	LDA	r0x116A
	CALL	___fslt
	JZ	_00108_DS_
;	;.line	74; "tancotf.c"	xnum = f;
	LDA	_tancotf_STK02
	STA	r0x1167
	LDA	_tancotf_STK01
	STA	r0x1168
	LDA	_tancotf_STK00
	STA	r0x1169
	LDA	r0x1163
	STA	r0x116A
;	;.line	75; "tancotf.c"	xden = 1.0;
	CLRA	
	STA	r0x116B
	STA	r0x116C
	LDA	#0x80
	STA	r0x116D
	LDA	#0x3f
	STA	r0x116E
	JMP	_00109_DS_
_00108_DS_:
;	;.line	79; "tancotf.c"	g = f*f;
	LDA	_tancotf_STK02
	STA	___fsmul_STK06
	LDA	_tancotf_STK01
	STA	___fsmul_STK05
	LDA	_tancotf_STK00
	STA	___fsmul_STK04
	LDA	r0x1163
	STA	___fsmul_STK03
	LDA	_tancotf_STK02
	STA	___fsmul_STK02
	LDA	_tancotf_STK01
	STA	___fsmul_STK01
	LDA	_tancotf_STK00
	STA	___fsmul_STK00
	LDA	r0x1163
	CALL	___fsmul
	STA	r0x1172
	LDA	STK00
	STA	r0x1171
	LDA	STK01
	STA	r0x1170
	LDA	STK02
	STA	r0x116F
;	;.line	80; "tancotf.c"	xnum = P(f,g);
	STA	___fsmul_STK06
	LDA	r0x1170
	STA	___fsmul_STK05
	LDA	r0x1171
	STA	___fsmul_STK04
	LDA	r0x1172
	STA	___fsmul_STK03
	LDA	#0xb8
	STA	___fsmul_STK02
	LDA	#0x33
	STA	___fsmul_STK01
	LDA	#0xc4
	STA	___fsmul_STK00
	LDA	#0xbd
	CALL	___fsmul
	STA	r0x1176
	LDA	_tancotf_STK02
	STA	___fsmul_STK06
	LDA	_tancotf_STK01
	STA	___fsmul_STK05
	LDA	_tancotf_STK00
	STA	___fsmul_STK04
	LDA	r0x1163
	STA	___fsmul_STK03
	LDA	STK02
	STA	___fsmul_STK02
	LDA	STK01
	STA	___fsmul_STK01
	LDA	STK00
	STA	___fsmul_STK00
	LDA	r0x1176
	CALL	___fsmul
	STA	r0x1176
	LDA	_tancotf_STK02
	STA	___fsadd_STK06
	LDA	_tancotf_STK01
	STA	___fsadd_STK05
	LDA	_tancotf_STK00
	STA	___fsadd_STK04
	LDA	r0x1163
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1176
	CALL	___fsadd
	STA	r0x116A
	LDA	STK00
	STA	r0x1169
	LDA	STK01
	STA	r0x1168
	LDA	STK02
	STA	r0x1167
;	;.line	81; "tancotf.c"	xden = Q(g);
	LDA	r0x116F
	STA	___fsmul_STK06
	LDA	r0x1170
	STA	___fsmul_STK05
	LDA	r0x1171
	STA	___fsmul_STK04
	LDA	r0x1172
	STA	___fsmul_STK03
	LDA	#0x75
	STA	___fsmul_STK02
	LDA	#0x33
	STA	___fsmul_STK01
	LDA	#0x1f
	STA	___fsmul_STK00
	LDA	#0x3c
	CALL	___fsmul
	STA	r0x1163
	LDA	#0xaf
	STA	___fsadd_STK06
	LDA	#0xb7
	STA	___fsadd_STK05
	LDA	#0xdb
	STA	___fsadd_STK04
	LDA	#0xbe
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1163
	CALL	___fsadd
	STA	r0x1163
	LDA	r0x116F
	STA	___fsmul_STK06
	LDA	r0x1170
	STA	___fsmul_STK05
	LDA	r0x1171
	STA	___fsmul_STK04
	LDA	r0x1172
	STA	___fsmul_STK03
	LDA	STK02
	STA	___fsmul_STK02
	LDA	STK01
	STA	___fsmul_STK01
	LDA	STK00
	STA	___fsmul_STK00
	LDA	r0x1163
	CALL	___fsmul
	STA	r0x1163
	CLRA	
	STA	___fsadd_STK06
	STA	___fsadd_STK05
	LDA	#0x80
	STA	___fsadd_STK04
	LDA	#0x3f
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1163
	CALL	___fsadd
	STA	r0x116E
	LDA	STK00
	STA	r0x116D
	LDA	STK01
	STA	r0x116C
	LDA	STK02
	STA	r0x116B
_00109_DS_:
;	;.line	84; "tancotf.c"	if(n&1)
	LDA	r0x1165
	SHR	
	JNC	_00117_DS_
;	;.line	87; "tancotf.c"	if(iscotan) return (-xnum/xden);
	LDA	_tancotf_STK03
	JZ	_00111_DS_
	LDA	r0x116A
	XOR	#0x80
	STA	r0x1163
	LDA	r0x116B
	STA	___fsdiv_STK06
	LDA	r0x116C
	STA	___fsdiv_STK05
	LDA	r0x116D
	STA	___fsdiv_STK04
	LDA	r0x116E
	STA	___fsdiv_STK03
	LDA	r0x1167
	STA	___fsdiv_STK02
	LDA	r0x1168
	STA	___fsdiv_STK01
	LDA	r0x1169
	STA	___fsdiv_STK00
	LDA	r0x1163
	CALL	___fsdiv
	JMP	_00119_DS_
_00111_DS_:
;	;.line	88; "tancotf.c"	else return (-xden/xnum);
	LDA	r0x116E
	XOR	#0x80
	STA	r0x1163
	LDA	r0x1167
	STA	___fsdiv_STK06
	LDA	r0x1168
	STA	___fsdiv_STK05
	LDA	r0x1169
	STA	___fsdiv_STK04
	LDA	r0x116A
	STA	___fsdiv_STK03
	LDA	r0x116B
	STA	___fsdiv_STK02
	LDA	r0x116C
	STA	___fsdiv_STK01
	LDA	r0x116D
	STA	___fsdiv_STK00
	LDA	r0x1163
	CALL	___fsdiv
	JMP	_00119_DS_
_00117_DS_:
;	;.line	92; "tancotf.c"	if(iscotan) return (xden/xnum);
	LDA	_tancotf_STK03
	JZ	_00114_DS_
	LDA	r0x1167
	STA	___fsdiv_STK06
	LDA	r0x1168
	STA	___fsdiv_STK05
	LDA	r0x1169
	STA	___fsdiv_STK04
	LDA	r0x116A
	STA	___fsdiv_STK03
	LDA	r0x116B
	STA	___fsdiv_STK02
	LDA	r0x116C
	STA	___fsdiv_STK01
	LDA	r0x116D
	STA	___fsdiv_STK00
	LDA	r0x116E
	CALL	___fsdiv
	JMP	_00119_DS_
_00114_DS_:
;	;.line	93; "tancotf.c"	else return (xnum/xden);
	LDA	r0x116B
	STA	___fsdiv_STK06
	LDA	r0x116C
	STA	___fsdiv_STK05
	LDA	r0x116D
	STA	___fsdiv_STK04
	LDA	r0x116E
	STA	___fsdiv_STK03
	LDA	r0x1167
	STA	___fsdiv_STK02
	LDA	r0x1168
	STA	___fsdiv_STK01
	LDA	r0x1169
	STA	___fsdiv_STK00
	LDA	r0x116A
	CALL	___fsdiv
_00119_DS_:
;	;.line	95; "tancotf.c"	}
	RET	
; exit point of _tancotf
	.ENDFUNC _tancotf
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tancotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$errno$0$0({2}SI:S),E,0,0
	;--cdb--S:Ltancotf.tancotf$iscotan$65536$25({1}:S),R,0,0,[_tancotf_STK03]
	;--cdb--S:Ltancotf.tancotf$x$65536$25({4}SF:S),R,0,0,[_tancotf_STK02,_tancotf_STK01,_tancotf_STK00,r0x1163]
	;--cdb--S:Ltancotf.tancotf$f$65536$26({4}SF:S),R,0,0,[_tancotf_STK02,_tancotf_STK01,_tancotf_STK00,r0x1163]
	;--cdb--S:Ltancotf.tancotf$g$65536$26({4}SF:S),R,0,0,[r0x116F,r0x1170,r0x1171,r0x1172]
	;--cdb--S:Ltancotf.tancotf$xn$65536$26({4}SF:S),R,0,0,[r0x1167,r0x1168,r0x1169,r0x116A]
	;--cdb--S:Ltancotf.tancotf$xnum$65536$26({4}SF:S),R,0,0,[r0x1167,r0x1168,r0x1169,r0x116A]
	;--cdb--S:Ltancotf.tancotf$xden$65536$26({4}SF:S),R,0,0,[r0x116B,r0x116C,r0x116D,r0x116E]
	;--cdb--S:Ltancotf.tancotf$n$65536$26({2}SI:S),R,0,0,[r0x1165,r0x1166]
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_fabsf
	.globl	___fslt
	.globl	___fsmul
	.globl	___fsadd
	.globl	___fs2sint
	.globl	___sint2fs
	.globl	___fssub
	.globl	___fsdiv
	.globl	_errno

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_tancotf
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_tancotf_0	udata
r0x1163:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116C:	.ds	1
r0x116D:	.ds	1
r0x116E:	.ds	1
r0x116F:	.ds	1
r0x1170:	.ds	1
r0x1171:	.ds	1
r0x1172:	.ds	1
r0x1176:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_tancotf_STK00:	.ds	1
	.globl _tancotf_STK00
_tancotf_STK01:	.ds	1
	.globl _tancotf_STK01
_tancotf_STK02:	.ds	1
	.globl _tancotf_STK02
_tancotf_STK03:	.ds	1
	.globl _tancotf_STK03
	.globl _fabsf_STK02
	.globl _fabsf_STK01
	.globl _fabsf_STK00
	.globl ___fslt_STK06
	.globl ___fslt_STK05
	.globl ___fslt_STK04
	.globl ___fslt_STK03
	.globl ___fslt_STK02
	.globl ___fslt_STK01
	.globl ___fslt_STK00
	.globl ___fsmul_STK06
	.globl ___fsmul_STK05
	.globl ___fsmul_STK04
	.globl ___fsmul_STK03
	.globl ___fsmul_STK02
	.globl ___fsmul_STK01
	.globl ___fsmul_STK00
	.globl ___fsadd_STK06
	.globl ___fsadd_STK05
	.globl ___fsadd_STK04
	.globl ___fsadd_STK03
	.globl ___fsadd_STK02
	.globl ___fsadd_STK01
	.globl ___fsadd_STK00
	.globl ___fs2sint_STK02
	.globl ___fs2sint_STK01
	.globl ___fs2sint_STK00
	.globl ___sint2fs_STK00
	.globl ___fssub_STK06
	.globl ___fssub_STK05
	.globl ___fssub_STK04
	.globl ___fssub_STK03
	.globl ___fssub_STK02
	.globl ___fssub_STK01
	.globl ___fssub_STK00
	.globl ___fsdiv_STK06
	.globl ___fsdiv_STK05
	.globl ___fsdiv_STK04
	.globl ___fsdiv_STK03
	.globl ___fsdiv_STK02
	.globl ___fsdiv_STK01
	.globl ___fsdiv_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1165:NULL+0:-1:1
	;--cdb--W:_tancotf_STK00:NULL+0:14:0
	;--cdb--W:_tancotf_STK00:NULL+0:4457:0
	;--cdb--W:_tancotf_STK00:NULL+0:4461:0
	;--cdb--W:_tancotf_STK01:NULL+0:13:0
	;--cdb--W:_tancotf_STK01:NULL+0:4456:0
	;--cdb--W:_tancotf_STK01:NULL+0:4460:0
	;--cdb--W:_tancotf_STK02:NULL+0:12:0
	;--cdb--W:_tancotf_STK02:NULL+0:4455:0
	;--cdb--W:_tancotf_STK02:NULL+0:4459:0
	;--cdb--W:r0x1168:NULL+0:13:0
	;--cdb--W:r0x1167:NULL+0:14:0
	;--cdb--W:r0x1167:NULL+0:12:0
	;--cdb--W:r0x1166:NULL+0:13:0
	;--cdb--W:r0x1165:NULL+0:12:0
	;--cdb--W:r0x1169:NULL+0:14:0
	;--cdb--W:r0x116B:NULL+0:14:0
	;--cdb--W:r0x116B:NULL+0:12:0
	;--cdb--W:r0x116C:NULL+0:13:0
	;--cdb--W:r0x116D:NULL+0:14:0
	;--cdb--W:r0x1171:NULL+0:14:0
	;--cdb--W:r0x1170:NULL+0:13:0
	;--cdb--W:r0x116F:NULL+0:12:0
	;--cdb--W:r0x1175:NULL+0:14:0
	;--cdb--W:r0x1174:NULL+0:13:0
	;--cdb--W:r0x1173:NULL+0:12:0
	;--cdb--W:r0x1169:NULL+0:-1:1
	;--cdb--W:r0x1167:NULL+0:-1:1
	;--cdb--W:r0x1163:NULL+0:-1:1
	end
