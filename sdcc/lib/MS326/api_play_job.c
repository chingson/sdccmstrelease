
#include"ms326.h"
#include"ms326typedef.h"
#include"ms326sphlib.h"

extern USHORT api_play_endpage;
extern USHORT api_play_startpage;
extern BYTE api_play_mode;
extern BYTE api_play_rampage;
extern BYTE api_target_vol;
extern BYTE api_play_fifostart;
extern BYTE api_play_fifoend;
extern api_spi_addr_t api_play_spi_addr;


void api_play_stop(void)
{
    if(api_play_mode&API_PLAYMODE_PA7)
    {
        PADIR&=0x7f;
        SPKC&=~0x48;
    }
    DACON=0;
}
extern BYTE api_decode_12bit(BYTE page);

BYTE api_play_job(void)
{
    GIF=0xbf;

    SPIMH=api_play_spi_addr.addrhm;
    SPIL=api_play_spi_addr.addrl;
    if((PDMALH>>7)!=(api_play_rampage>>3))
    {
        if(api_play_mode&0x80) // prevent sat
        {
            if(DCLAMP)
            {
                DCLAMP=1;
                FILTERGP-=8;
            }

            else
            {
                if(FILTERGP!=api_target_vol)
                    FILTERGP++;
            }

        }
        if(api_play_mode & API_PLAYMODE_12BIT)
        {
            if(!api_decode_12bit(api_play_rampage))
                return 0;

        } else
        {
again:
            SPIOPRAH=api_play_rampage>>3;
            SPIOP=0x48;// read a half page
            if(SPIL&0x80)
            {
                SPIOP=0x20;
                SPIL=0;
            }
            else
            {
                SPIL=0x80;
            }
            // OK, we have an issue, non reversed have 80 problem



            if((SPIOP&0x80)||((SPIH==(BYTE)(api_play_endpage>>8))
                              && SPIM==(BYTE)(api_play_endpage&0xff)))
            {
                if(api_play_mode & API_PLAYMODE_REPEAT)
                {
                    api_play_spi_addr.addrhm=api_play_startpage;
                    api_play_spi_addr.addrl=0;
                    SPIM=api_play_startpage&0xff;
                    SPIH=api_play_startpage>>8;
                    SPIOP=0x10; // reset ENDCODECNT

                    // there must be zero for adpcm to start
                    goto again;
                }
                return 0;
            }
        }
        api_play_rampage+=8; // page by page now
        if(api_play_rampage>=api_play_fifoend)
            api_play_rampage=api_play_fifostart;

        api_play_spi_addr.addrhm=SPIMH;
        api_play_spi_addr.addrl=SPIL;
        return 2;
    }
    return 1;
}

