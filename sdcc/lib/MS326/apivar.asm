;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.1 #3de0c6772 (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"C:\work\ms326sphlib"
;;	.file	"apivar.c"
	.module apivar
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fapivar$spiastru[({0}S:S$addrhm$0$0({2}SI:U),Z,0,0)({2}S:S$addrl$0$0({1}SC:U),Z,0,0)]
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; apivar-code 
	;--cdb--S:G$api_rampage$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_endpage$0$0({2}SI:U),E,0,0
	;--cdb--S:G$api_mode$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_target_vol$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_fifostart$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_fifoend$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_play_mode$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_play_rampage$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_play_fifostart$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_play_fifoend$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_play_startpage$0$0({2}SI:U),E,0,0
	;--cdb--S:G$api_play_endpage$0$0({2}SI:U),E,0,0
	;--cdb--S:G$api_play_spibuf$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_play_spiind$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_maxrecopg$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_play_spi_addr$0$0({3}STspiastru:S),E,0,0
	;--cdb--S:G$api_rec_spi_addr$0$0({3}STspiastru:S),E,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_api_rampage
	.globl	_api_endpage
	.globl	_api_mode
	.globl	_api_target_vol
	.globl	_api_fifostart
	.globl	_api_fifoend
	.globl	_api_play_mode
	.globl	_api_play_rampage
	.globl	_api_play_fifostart
	.globl	_api_play_fifoend
	.globl	_api_play_startpage
	.globl	_api_play_endpage
	.globl	_api_play_spibuf
	.globl	_api_play_spiind
	.globl	_api_maxrecopg
	.globl	_api_play_spi_addr
	.globl	_api_rec_spi_addr
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
_api_rampage:	.ds	1

	.area DSEG(DATA)
_api_endpage:	.ds	2

	.area DSEG(DATA)
_api_mode:	.ds	1

	.area DSEG(DATA)
_api_target_vol:	.ds	1

	.area DSEG(DATA)
_api_fifostart:	.ds	1

	.area DSEG(DATA)
_api_fifoend:	.ds	1

	.area DSEG(DATA)
_api_play_mode:	.ds	1

	.area DSEG(DATA)
_api_play_rampage:	.ds	1

	.area DSEG(DATA)
_api_play_fifostart:	.ds	1

	.area DSEG(DATA)
_api_play_fifoend:	.ds	1

	.area DSEG(DATA)
_api_play_startpage:	.ds	2

	.area DSEG(DATA)
_api_play_endpage:	.ds	2

	.area DSEG(DATA)
_api_play_spibuf:	.ds	1

	.area DSEG(DATA)
_api_play_spiind:	.ds	1

	.area DSEG(DATA)
_api_maxrecopg:	.ds	1

	.area DSEG(DATA)
_api_play_spi_addr:	.ds	3

	.area DSEG(DATA)
_api_rec_spi_addr:	.ds	3

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	end
