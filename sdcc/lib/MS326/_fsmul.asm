;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_fsmul.c"
	.module _fsmul
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:F_fsmul$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:U),Z,0,0)]
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$__fsmul$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _fsmul-code 
.globl ___fsmul

;--------------------------------------------------------
	.FUNC ___fsmul:$PNUM 8:$C:__mullong\
:$L:r0x1162:$L:___fsmul_STK00:$L:___fsmul_STK01:$L:___fsmul_STK02:$L:___fsmul_STK03\
:$L:___fsmul_STK04:$L:___fsmul_STK05:$L:___fsmul_STK06:$L:___fsmul_fl1_65536_21:$L:___fsmul_fl2_65536_21\
:$L:r0x116A:$L:r0x116B:$L:r0x116E:$L:r0x116F:$L:r0x1170\
:$L:r0x1171:$L:r0x1172:$L:r0x1173:$L:r0x1174:$L:r0x1175\
:$L:r0x1176:$L:r0x117D:$L:r0x117C:$L:r0x117B:$L:r0x117A\

;--------------------------------------------------------
;	.line	241; "_fsmul.c"	float __fsmul (float a1, float a2) {
___fsmul:	;Function start
	STA	r0x1162
	LDA	___fsmul_STK06
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	247; "_fsmul.c"	fl1.f = a1;
	LDA	___fsmul_STK02
	STA	___fsmul_fl1_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	___fsmul_STK01
	STA	(___fsmul_fl1_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	___fsmul_STK00
	STA	(___fsmul_fl1_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1162
	STA	(___fsmul_fl1_65536_21 + 3)
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	248; "_fsmul.c"	fl2.f = a2;
	LDA	___fsmul_STK06
	STA	___fsmul_fl2_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	___fsmul_STK05
	STA	(___fsmul_fl2_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	___fsmul_STK04
	STA	(___fsmul_fl2_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	___fsmul_STK03
	STA	(___fsmul_fl2_65536_21 + 3)
;	;.line	250; "_fsmul.c"	if (!fl1.l || !fl2.l)
	LDA	___fsmul_fl1_65536_21
	ORA	(___fsmul_fl1_65536_21 + 1)
	ORA	(___fsmul_fl1_65536_21 + 2)
	ORA	(___fsmul_fl1_65536_21 + 3)
	JZ	_00105_DS_
	LDA	___fsmul_fl2_65536_21
	ORA	(___fsmul_fl2_65536_21 + 1)
	ORA	(___fsmul_fl2_65536_21 + 2)
	ORA	(___fsmul_fl2_65536_21 + 3)
	JNZ	_00106_DS_
_00105_DS_:
;	;.line	251; "_fsmul.c"	return (0);
	CLRA	
	STA	STK02
	STA	STK01
	STA	STK00
	JMP	_00117_DS_
_00106_DS_:
;	;.line	254; "_fsmul.c"	sign = SIGN (fl1.l) ^ SIGN (fl2.l);
	LDA	(___fsmul_fl1_65536_21 + 3)
	AND	#0x80
	DECA	
	CLRA	
	ROL	
	STA	___fsmul_STK06
	LDA	(___fsmul_fl2_65536_21 + 3)
	AND	#0x80
	DECA	
	CLRA	
	ROL	
	XOR	___fsmul_STK06
	STA	___fsmul_STK02
;	;.line	255; "_fsmul.c"	exp = EXP (fl1.l) - EXCESS;
	LDA	(___fsmul_fl1_65536_21 + 2)
	ROL	
	LDA	(___fsmul_fl1_65536_21 + 3)
	ROL	
	STA	___fsmul_STK05
	CLRB	_C
	LDA	#0x82
	ADD	___fsmul_STK05
	STA	___fsmul_STK05
	CLRA	
	ADDC	#0xff
	STA	___fsmul_STK04
;	;.line	256; "_fsmul.c"	exp += EXP (fl2.l);
	LDA	(___fsmul_fl2_65536_21 + 2)
	ROL	
	LDA	(___fsmul_fl2_65536_21 + 3)
	ROL	
	STA	r0x116A
	CLRB	_C
	LDA	___fsmul_STK05
	ADD	r0x116A
	STA	___fsmul_STK01
	CLRA	
	ADDC	___fsmul_STK04
	STA	___fsmul_STK00
;	;.line	258; "_fsmul.c"	fl1.l = MANT (fl1.l);
	LDA	___fsmul_fl1_65536_21
	STA	___fsmul_STK05
	LDA	(___fsmul_fl1_65536_21 + 1)
	STA	___fsmul_STK04
	LDA	#0x7f
	AND	(___fsmul_fl1_65536_21 + 2)
	ORA	#0x80
	STA	r0x116A
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
	LDA	___fsmul_STK05
	STA	___fsmul_fl1_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	___fsmul_STK04
	STA	(___fsmul_fl1_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	r0x116A
	STA	(___fsmul_fl1_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	CLRA	
	STA	(___fsmul_fl1_65536_21 + 3)
;	;.line	259; "_fsmul.c"	fl2.l = MANT (fl2.l);
	LDA	___fsmul_fl2_65536_21
	STA	r0x116E
	LDA	(___fsmul_fl2_65536_21 + 1)
	STA	r0x116F
	LDA	#0x7f
	AND	(___fsmul_fl2_65536_21 + 2)
	ORA	#0x80
	STA	r0x1170
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
	LDA	r0x116E
	STA	___fsmul_fl2_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	r0x116F
	STA	(___fsmul_fl2_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	r0x1170
	STA	(___fsmul_fl2_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	CLRA	
	STA	(___fsmul_fl2_65536_21 + 3)
;	;.line	262; "_fsmul.c"	result = (fl1.l >> 8) * (fl2.l >> 8);
	LDA	___fsmul_STK04
	STA	r0x1172
	LDA	r0x116A
	STA	r0x1173
	LDA	r0x116F
	STA	__mullong_STK06
	LDA	r0x1170
	STA	__mullong_STK05
	CLRA	
	STA	__mullong_STK04
	STA	__mullong_STK03
	LDA	r0x1172
	STA	__mullong_STK02
	LDA	r0x1173
	STA	__mullong_STK01
	CLRA	
	STA	__mullong_STK00
	CALL	__mullong
	STA	r0x117D
	LDA	STK00
	STA	r0x117C
	LDA	STK01
	STA	r0x117B
	LDA	STK02
	STA	r0x117A
;	;.line	263; "_fsmul.c"	result += ((fl1.l & (unsigned long) 0xFF) * (fl2.l >> 8)) >> 8;
	LDA	r0x116F
	STA	__mullong_STK06
	LDA	r0x1170
	STA	__mullong_STK05
	CLRA	
	STA	__mullong_STK04
	STA	__mullong_STK03
	LDA	___fsmul_STK05
	STA	__mullong_STK02
	CLRA	
	STA	__mullong_STK01
	STA	__mullong_STK00
	CALL	__mullong
	STA	r0x116B
	LDA	r0x117A
	ADD	STK01
	STA	___fsmul_STK05
	LDA	r0x117B
	ADDC	STK00
	STA	___fsmul_STK04
	LDA	r0x117C
	ADDC	r0x116B
	STA	r0x116A
	CLRA	
	ADDC	r0x117D
	STA	r0x116B
;	;.line	264; "_fsmul.c"	result += ((fl2.l & (unsigned long) 0xFF) * (fl1.l >> 8)) >> 8;
	LDA	r0x1172
	STA	__mullong_STK06
	LDA	r0x1173
	STA	__mullong_STK05
	CLRA	
	STA	__mullong_STK04
	STA	__mullong_STK03
	LDA	r0x116E
	STA	__mullong_STK02
	CLRA	
	STA	__mullong_STK01
	STA	__mullong_STK00
	CALL	__mullong
	STA	r0x1171
	LDA	___fsmul_STK05
	ADD	STK01
	STA	r0x116E
	LDA	___fsmul_STK04
	ADDC	STK00
	STA	r0x116F
	LDA	r0x116A
	ADDC	r0x1171
	STA	r0x1170
	CLRA	
	ADDC	r0x116B
	STA	r0x1171
;	;.line	267; "_fsmul.c"	result += 0x40;
	LDA	#0x40
	ADD	r0x116E
	STA	___fsmul_STK05
	CLRA	
	ADDC	r0x116F
	STA	___fsmul_STK04
	CLRA	
	ADDC	r0x1170
	STA	r0x116A
	CLRA	
	ADDC	r0x1171
	STA	r0x116B
;	;.line	269; "_fsmul.c"	if (result & SIGNBIT)
	JPL	_00109_DS_
;	;.line	272; "_fsmul.c"	result += 0x40;
	LDA	#0x40
	ADD	___fsmul_STK05
	CLRA	
	ADDC	___fsmul_STK04
	STA	r0x116F
	CLRA	
	ADDC	r0x116A
	STA	r0x1170
	CLRA	
	ADDC	r0x116B
	STA	r0x1171
;	;.line	273; "_fsmul.c"	result >>= 8;
	LDA	r0x116F
	STA	r0x1172
	LDA	r0x1170
	STA	r0x1173
	LDA	r0x1171
	STA	r0x1174
	CLRA	
	STA	r0x1175
	JMP	_00110_DS_
_00109_DS_:
;	;.line	277; "_fsmul.c"	result >>= 7;
	LDA	___fsmul_STK05
	ROL	
	LDA	___fsmul_STK04
	ROL	
	STA	r0x1172
	LDA	r0x116A
	ROL	
	STA	r0x1173
	LDA	r0x116B
	ROL	
	STA	r0x1174
	CLRA	
	ROL	
	STA	r0x1175
;	;.line	278; "_fsmul.c"	exp--;
	LDA	___fsmul_STK01
	DECA	
	STA	___fsmul_STK01
	LDA	#0xff
	ADDC	___fsmul_STK00
	STA	___fsmul_STK00
_00110_DS_:
;	;.line	281; "_fsmul.c"	result &= ~HIDDEN;
	LDA	#0x7f
	AND	r0x1174
	STA	r0x116A
;	;.line	284; "_fsmul.c"	if (exp >= 0x100)
	CLRB	_C
	LDA	___fsmul_STK00
	XOR	#0x80
	ADDC	#0x7f
	JNC	_00115_DS_
;	;.line	285; "_fsmul.c"	fl1.l = (sign ? SIGNBIT : 0) | __INFINITY;
	LDA	___fsmul_STK02
	JZ	_00119_DS_
	CLRA	
	STA	r0x116E
	STA	r0x116F
	STA	r0x1170
	LDA	#0x80
	STA	r0x1171
	JMP	_00120_DS_
_00119_DS_:
	CLRA	
	STA	r0x116E
	STA	r0x116F
	STA	r0x1170
	STA	r0x1171
_00120_DS_:
	LDA	r0x1170
	ORA	#0x80
	STA	r0x1170
	LDA	r0x1171
	ORA	#0x7f
	STA	r0x1171
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
	LDA	r0x116E
	STA	___fsmul_fl1_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	r0x116F
	STA	(___fsmul_fl1_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	r0x1170
	STA	(___fsmul_fl1_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1171
	STA	(___fsmul_fl1_65536_21 + 3)
	JMP	_00116_DS_
_00115_DS_:
;	;.line	286; "_fsmul.c"	else if (exp < 0)
	LDA	___fsmul_STK00
	JMI	_00184_DS_
	JMP	_00112_DS_
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
_00184_DS_:
;	;.line	287; "_fsmul.c"	fl1.l = 0;
	CLRA	
	STA	___fsmul_fl1_65536_21
	STA	(___fsmul_fl1_65536_21 + 1)
	STA	(___fsmul_fl1_65536_21 + 2)
	STA	(___fsmul_fl1_65536_21 + 3)
	JMP	_00116_DS_
_00112_DS_:
;	;.line	289; "_fsmul.c"	fl1.l = PACK (sign ? SIGNBIT : 0 , exp, result);
	LDA	___fsmul_STK02
	JZ	_00121_DS_
	CLRA	
	STA	___fsmul_STK02
	STA	r0x116E
	STA	r0x116F
	LDA	#0x80
	STA	r0x1170
	JMP	_00122_DS_
_00121_DS_:
	CLRA	
	STA	___fsmul_STK02
	STA	r0x116E
	STA	r0x116F
	STA	r0x1170
_00122_DS_:
	LDA	___fsmul_STK00
	ROR	
	LDA	___fsmul_STK01
	ROR	
	STA	r0x1176
	CLRA	
	ROR	
	ORA	r0x116F
	STA	r0x116F
	LDA	r0x1176
	ORA	r0x1170
	STA	r0x1170
	LDA	r0x1172
	ORA	___fsmul_STK02
	STA	___fsmul_STK02
	LDA	r0x1173
	ORA	r0x116E
	STA	r0x116E
	LDA	r0x116A
	ORA	r0x116F
	STA	r0x116F
	LDA	r0x1175
	ORA	r0x1170
	STA	r0x1170
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
	LDA	___fsmul_STK02
	STA	___fsmul_fl1_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	r0x116E
	STA	(___fsmul_fl1_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	r0x116F
	STA	(___fsmul_fl1_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1170
	STA	(___fsmul_fl1_65536_21 + 3)
_00116_DS_:
;	;.line	290; "_fsmul.c"	return (fl1.f);
	LDA	___fsmul_fl1_65536_21
	STA	STK02
	LDA	(___fsmul_fl1_65536_21 + 1)
	STA	STK01
	LDA	(___fsmul_fl1_65536_21 + 2)
	STA	STK00
	LDA	(___fsmul_fl1_65536_21 + 3)
_00117_DS_:
;	;.line	291; "_fsmul.c"	}
	RET	
; exit point of ___fsmul
	.ENDFUNC ___fsmul
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:L_fsmul.__fsmul$a2$65536$20({4}SF:S),R,0,0,[___fsmul_STK06,___fsmul_STK05,___fsmul_STK04,___fsmul_STK03]
	;--cdb--S:L_fsmul.__fsmul$a1$65536$20({4}SF:S),R,0,0,[___fsmul_STK02,___fsmul_STK01,___fsmul_STK00,r0x1162]
	;--cdb--S:L_fsmul.__fsmul$fl1$65536$21({4}STfloat_long:S),E,0,0
	;--cdb--S:L_fsmul.__fsmul$fl2$65536$21({4}STfloat_long:S),E,0,0
	;--cdb--S:L_fsmul.__fsmul$result$65536$21({4}SL:U),R,0,0,[r0x116E,r0x116F,r0x1170,r0x1171]
	;--cdb--S:L_fsmul.__fsmul$exp$65536$21({2}SI:S),R,0,0,[___fsmul_STK01,___fsmul_STK00]
	;--cdb--S:L_fsmul.__fsmul$sign$65536$21({1}SC:U),R,0,0,[___fsmul_STK02]
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__mullong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___fsmul
	.globl	___fsmul_fl1_65536_21
	.globl	___fsmul_fl2_65536_21
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
___fsmul_fl1_65536_21:	.ds	4

	.area DSEG(DATA)
___fsmul_fl2_65536_21:	.ds	4

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__fsmul_0	udata
r0x1162:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116E:	.ds	1
r0x116F:	.ds	1
r0x1170:	.ds	1
r0x1171:	.ds	1
r0x1172:	.ds	1
r0x1173:	.ds	1
r0x1174:	.ds	1
r0x1175:	.ds	1
r0x1176:	.ds	1
r0x117A:	.ds	1
r0x117B:	.ds	1
r0x117C:	.ds	1
r0x117D:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
___fsmul_STK00:	.ds	1
	.globl ___fsmul_STK00
___fsmul_STK01:	.ds	1
	.globl ___fsmul_STK01
___fsmul_STK02:	.ds	1
	.globl ___fsmul_STK02
___fsmul_STK03:	.ds	1
	.globl ___fsmul_STK03
___fsmul_STK04:	.ds	1
	.globl ___fsmul_STK04
___fsmul_STK05:	.ds	1
	.globl ___fsmul_STK05
___fsmul_STK06:	.ds	1
	.globl ___fsmul_STK06
	.globl __mullong_STK06
	.globl __mullong_STK05
	.globl __mullong_STK04
	.globl __mullong_STK03
	.globl __mullong_STK02
	.globl __mullong_STK01
	.globl __mullong_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:___fsmul_STK02:NULL+0:-1:1
	;--cdb--W:___fsmul_STK01:NULL+0:-1:1
	;--cdb--W:___fsmul_STK00:NULL+0:-1:1
	;--cdb--W:___fsmul_STK03:NULL+0:-1:1
	;--cdb--W:r0x1167:NULL+0:-1:1
	;--cdb--W:r0x116C:NULL+0:-1:1
	;--cdb--W:r0x116D:NULL+0:-1:1
	;--cdb--W:r0x116B:NULL+0:-1:1
	;--cdb--W:r0x1171:NULL+0:-1:1
	;--cdb--W:___fsmul_STK05:NULL+0:-1:1
	;--cdb--W:r0x116E:NULL+0:-1:1
	;--cdb--W:r0x1173:NULL+0:-1:1
	;--cdb--W:r0x1174:NULL+0:-1:1
	;--cdb--W:___fsmul_STK00:NULL+0:0:0
	;--cdb--W:___fsmul_STK01:NULL+0:4483:0
	;--cdb--W:___fsmul_STK01:NULL+0:4458:0
	;--cdb--W:___fsmul_STK01:NULL+0:0:0
	;--cdb--W:___fsmul_STK04:NULL+0:0:0
	;--cdb--W:___fsmul_STK04:NULL+0:13:0
	;--cdb--W:___fsmul_STK04:NULL+0:4467:0
	;--cdb--W:r0x116A:NULL+0:14:0
	;--cdb--W:r0x116B:NULL+0:0:0
	;--cdb--W:r0x116F:NULL+0:13:0
	;--cdb--W:r0x1170:NULL+0:14:0
	;--cdb--W:r0x1171:NULL+0:0:0
	;--cdb--W:r0x1171:NULL+0:4479:0
	;--cdb--W:r0x1172:NULL+0:13:0
	;--cdb--W:r0x1172:NULL+0:4478:0
	;--cdb--W:r0x1173:NULL+0:14:0
	;--cdb--W:r0x1174:NULL+0:4465:0
	;--cdb--W:r0x1175:NULL+0:0:0
	;--cdb--W:r0x1176:NULL+0:4463:0
	;--cdb--W:r0x1176:NULL+0:13:0
	;--cdb--W:r0x1177:NULL+0:4464:0
	;--cdb--W:r0x1177:NULL+0:14:0
	;--cdb--W:r0x1178:NULL+0:4465:0
	;--cdb--W:r0x1178:NULL+0:4459:0
	;--cdb--W:r0x1179:NULL+0:0:0
	;--cdb--W:___fsmul_STK04:NULL+0:-1:1
	;--cdb--W:r0x1179:NULL+0:-1:1
	;--cdb--W:r0x1175:NULL+0:-1:1
	;--cdb--W:___fsmul_STK05:NULL+0:4466:0
	;--cdb--W:r0x116A:NULL+0:-1:1
	;--cdb--W:r0x1170:NULL+0:-1:1
	;--cdb--W:r0x116A:NULL+0:0:0
	;--cdb--W:r0x116F:NULL+0:0:0
	;--cdb--W:r0x1174:NULL+0:0:0
	;--cdb--W:r0x116F:NULL+0:-1:1
	;--cdb--W:r0x1170:NULL+0:0:0
	;--cdb--W:r0x116B:NULL+0:4469:0
	end
