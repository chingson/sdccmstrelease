;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"atoi.c"
	.module atoi
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$atoi$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--F:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0,0,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isblank$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$islower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isupper$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$_mulint$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; atoi-code 
.globl _atoi

;--------------------------------------------------------
	.FUNC _atoi:$PNUM 2:$C:__mulint\
:$L:r0x1160:$L:_atoi_STK00:$L:r0x1161:$L:r0x1162:$L:r0x1163\
:$L:r0x1164:$L:r0x1165:$L:r0x1166:$L:r0x1169
;--------------------------------------------------------
;	.line	34; "atoi.c"	int atoi(const char *nptr)
_atoi:	;Function start
	STA	r0x1160
;	;.line	36; "atoi.c"	int ret = 0;
	CLRA	
	STA	r0x1161
	STA	r0x1162
_00105_DS_:
;	;.line	39; "atoi.c"	while (isblank (*nptr))
	LDA	_atoi_STK00
	STA	_ROMPL
	LDA	r0x1160
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1163
;	;.line	53; "/home/chingson/sdccofficial/sdcc/device/include/ctype.h"	return ((unsigned char)c == ' ' || (unsigned char)c == '\t');
	ADD	#0xe0
	JZ	_00119_DS_
	LDA	r0x1163
	XOR	#0x09
	JNZ	_00135_DS_
_00119_DS_:
;	;.line	40; "atoi.c"	nptr++;
	LDA	_atoi_STK00
	INCA	
	STA	_atoi_STK00
	CLRA	
	ADDC	r0x1160
	STA	r0x1160
	JMP	_00105_DS_
_00135_DS_:
	LDA	_atoi_STK00
	STA	r0x1163
	LDA	r0x1160
	STA	r0x1164
;	;.line	42; "atoi.c"	neg = (*nptr == '-');
	LDA	_atoi_STK00
	STA	_ROMPL
	LDA	r0x1160
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1165
	XOR	#0x2d
	LDC	_Z
	CLRA	
	ROL	
	STA	_atoi_STK00
;	;.line	44; "atoi.c"	if (*nptr == '-' || *nptr == '+')
	LDA	r0x1165
	ADD	#0xd3
	JZ	_00108_DS_
	LDA	r0x1165
	XOR	#0x2b
	JNZ	_00133_DS_
_00108_DS_:
;	;.line	45; "atoi.c"	nptr++;
	LDA	r0x1163
	INCA	
	STA	r0x1163
	CLRA	
	ADDC	r0x1164
	STA	r0x1164
_00133_DS_:
;	;.line	47; "atoi.c"	while (isdigit (*nptr))
	LDA	r0x1163
	STA	r0x1160
	LDA	r0x1164
	STA	r0x1163
_00111_DS_:
	LDA	r0x1160
	STA	_ROMPL
	LDA	r0x1163
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1164
;	;.line	62; "/home/chingson/sdccofficial/sdcc/device/include/ctype.h"	return ((unsigned char)c >= '0' && (unsigned char)c <= '9');
	ADD	#0xd0
	JNC	_00113_DS_
	SETB	_C
	LDA	#0x39
	SUBB	r0x1164
	JNC	_00113_DS_
;	;.line	48; "atoi.c"	ret = ret * 10 + (*(nptr++) - '0');
	LDA	r0x1161
	STA	__mulint_STK02
	LDA	r0x1162
	STA	__mulint_STK01
	LDA	#0x0a
	STA	__mulint_STK00
	CLRA	
	CALL	__mulint
	STA	r0x1165
	LDA	r0x1160
	STA	_ROMPL
	LDA	r0x1163
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1166
	LDA	r0x1160
	INCA	
	STA	r0x1160
	CLRA	
	ADDC	r0x1163
	STA	r0x1163
	LDA	#0xd0
	ADD	r0x1166
	STA	r0x1166
	CLRA	
	ADDC	#0xff
	STA	r0x1169
	LDA	STK00
	ADD	r0x1166
	STA	r0x1161
	LDA	r0x1165
	ADDC	r0x1169
	STA	r0x1162
	JMP	_00111_DS_
_00113_DS_:
;	;.line	50; "atoi.c"	return (neg ? -ret : ret); // Since -INT_MIN is INT_MIN in sdcc, the result value always turns out ok.
	LDA	_atoi_STK00
	JZ	_00124_DS_
	SETB	_C
	CLRA	
	SUBB	r0x1161
	STA	_atoi_STK00
	CLRA	
	SUBB	r0x1162
	STA	r0x1160
	JMP	_00125_DS_
_00124_DS_:
	LDA	r0x1161
	STA	_atoi_STK00
	LDA	r0x1162
	STA	r0x1160
_00125_DS_:
	LDA	_atoi_STK00
	STA	STK00
	LDA	r0x1160
;	;.line	51; "atoi.c"	}
	RET	
; exit point of _atoi
	.ENDFUNC _atoi
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_mulint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:Latoi.aligned_alloc$size$65536$15({2}SI:U),E,0,0
	;--cdb--S:Latoi.aligned_alloc$alignment$65536$15({2}SI:U),E,0,0
	;--cdb--S:Latoi.isblank$c$65536$41({2}SI:S),E,0,0
	;--cdb--S:Latoi.isdigit$c$65536$43({2}SI:S),E,0,0
	;--cdb--S:Latoi.islower$c$65536$45({2}SI:S),E,0,0
	;--cdb--S:Latoi.isupper$c$65536$47({2}SI:S),E,0,0
	;--cdb--S:Latoi.atoi$nptr$65536$49({2}DG,SC:U),R,0,0,[_atoi_STK00,r0x1164]
	;--cdb--S:Latoi.atoi$__1310720004$131072$50({2}SI:S),R,0,0,[]
	;--cdb--S:Latoi.atoi$__1310720001$131072$50({2}SI:S),R,0,0,[]
	;--cdb--S:Latoi.atoi$ret$65536$50({2}SI:S),R,0,0,[r0x1161,r0x1162]
	;--cdb--S:Latoi.atoi$neg$65536$50({1}:S),R,0,0,[_atoi_STK00]
	;--cdb--S:Latoi.atoi$__1310720002$131072$51({2}SI:S),R,0,0,[r0x1164,r0x1165]
	;--cdb--S:Latoi.atoi$c$196608$52({2}SI:S),R,0,0,[]
	;--cdb--S:Latoi.atoi$__1310720005$131072$54({2}SI:S),R,0,0,[r0x1165,r0x1166]
	;--cdb--S:Latoi.atoi$c$196608$55({2}SI:S),R,0,0,[]
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__mulint

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_atoi
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_atoi_0	udata
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1169:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_atoi_STK00:	.ds	1
	.globl _atoi_STK00
	.globl __mulint_STK02
	.globl __mulint_STK01
	.globl __mulint_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1165:NULL+0:-1:1
	;--cdb--W:r0x1166:NULL+0:-1:1
	;--cdb--W:r0x1163:NULL+0:4452:0
	;--cdb--W:r0x1164:NULL+0:4451:0
	;--cdb--W:r0x1164:NULL+0:4453:0
	;--cdb--W:r0x1164:NULL+0:14:0
	;--cdb--W:r0x1165:NULL+0:4452:0
	;--cdb--W:r0x1167:NULL+0:4454:0
	;--cdb--W:r0x1168:NULL+0:0:0
	;--cdb--W:r0x1168:NULL+0:-1:1
	end
