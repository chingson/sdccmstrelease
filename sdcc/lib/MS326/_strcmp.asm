;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_strcmp.c"
	.module _strcmp
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$strcmp$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _strcmp-code 
.globl _strcmp

;--------------------------------------------------------
	.FUNC _strcmp:$PNUM 4:$L:r0x1160:$L:_strcmp_STK00:$L:_strcmp_STK01:$L:_strcmp_STK02:$L:r0x1163\
:$L:r0x1164:$L:r0x1165:$L:r0x1166:$L:r0x1167
;--------------------------------------------------------
;	.line	34; "_strcmp.c"	int strcmp ( char * asrc, char * adst )
_strcmp:	;Function start
	STA	r0x1160
_00106_DS_:
;	;.line	50; "_strcmp.c"	while( ! (ret = *(unsigned char *)asrc - *(unsigned char *)adst) && *adst)
	LDA	_strcmp_STK00
	STA	_ROMPL
	LDA	r0x1160
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1164
	LDA	_strcmp_STK02
	STA	_ROMPL
	LDA	_strcmp_STK01
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1163
	CLRA	
	SETB	_C
	LDA	r0x1164
	SUBB	r0x1163
	STA	r0x1164
	CLRA	
	SUBB	#0x00
	STA	r0x1165
	LDA	r0x1164
	STA	r0x1166
	LDA	r0x1165
	STA	r0x1167
	LDA	r0x1164
	ORA	r0x1165
	JNZ	_00108_DS_
	LDA	r0x1163
	JZ	_00108_DS_
;	;.line	51; "_strcmp.c"	++asrc, ++adst;
	LDA	_strcmp_STK00
	INCA	
	STA	_strcmp_STK00
	CLRA	
	ADDC	r0x1160
	STA	r0x1160
	LDA	_strcmp_STK02
	INCA	
	STA	_strcmp_STK02
	CLRA	
	ADDC	_strcmp_STK01
	STA	_strcmp_STK01
	JMP	_00106_DS_
_00108_DS_:
;	;.line	53; "_strcmp.c"	return( ret );
	LDA	r0x1166
	STA	STK00
	LDA	r0x1167
;	;.line	55; "_strcmp.c"	}
	RET	
; exit point of _strcmp
	.ENDFUNC _strcmp
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_strcmp.strcmp$adst$65536$21({2}DG,SC:U),R,0,0,[_strcmp_STK02,_strcmp_STK01]
	;--cdb--S:L_strcmp.strcmp$asrc$65536$21({2}DG,SC:U),R,0,0,[_strcmp_STK00]
	;--cdb--S:L_strcmp.strcmp$ret$65536$22({2}SI:S),R,0,0,[r0x1166,r0x1167]
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_strcmp
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__strcmp_0	udata
r0x1160:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_strcmp_STK00:	.ds	1
	.globl _strcmp_STK00
_strcmp_STK01:	.ds	1
	.globl _strcmp_STK01
_strcmp_STK02:	.ds	1
	.globl _strcmp_STK02
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x1166:NULL+0:4451:0
	;--cdb--W:r0x1167:NULL+0:0:0
	;--cdb--W:r0x1167:NULL+0:-1:1
	;--cdb--W:r0x1165:NULL+0:0:0
	;--cdb--W:r0x1165:NULL+0:-1:1
	end
