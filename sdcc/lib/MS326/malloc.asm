;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"malloc.c"
	.module malloc
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fmalloc$header[({0}S:S$next$0$0({2}DX,STheader:S),Z,0,0)({2}S:S$next_free$0$0({2}DX,STheader:S),Z,0,0)]
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--F:G$malloc$0$0({2}DF,DX,SV:S),C,0,0,0,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--F:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0,0,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__sdcc_heap_init$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$__sdcc_heap_init$0$0({2}DF,SV:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; malloc-code 
.globl _malloc

;--------------------------------------------------------
	.FUNC _malloc:$PNUM 2:$L:r0x1167:$L:_malloc_STK00:$L:r0x1168:$L:r0x1169:$L:r0x116A\
:$L:r0x116B:$L:r0x116C:$L:r0x116D:$L:r0x116F:$L:r0x116E\
:$L:r0x1171:$L:r0x1170:$L:r0x1172:$L:r0x1173
;--------------------------------------------------------
;	.line	60; "malloc.c"	void XDATA *malloc(size_t size)
_malloc:	;Function start
	STA	r0x1167
;	;.line	70; "malloc.c"	if(!size || size + offsetof(struct header, next_free) < size)
	ORA	_malloc_STK00
	JZ	_00109_DS_
	LDA	#0x02
	ADD	_malloc_STK00
	STA	r0x1168
	CLRA	
	ADDC	r0x1167
	STA	r0x1169
	SETB	_C
	LDA	r0x1168
	SUBB	_malloc_STK00
	LDA	r0x1169
	SUBB	r0x1167
	JC	_00110_DS_
_00109_DS_:
;	;.line	71; "malloc.c"	return(0);
	CLRA	
	STA	STK00
	JMP	_00123_DS_
_00110_DS_:
;	;.line	72; "malloc.c"	size += offsetof(struct header, next_free);
	LDA	r0x1168
	STA	_malloc_STK00
	LDA	r0x1169
	STA	r0x1167
;	;.line	73; "malloc.c"	if(size < sizeof(struct header)) // Requiring a minimum size makes it easier to implement free(), and avoid memory leaks.
	LDA	_malloc_STK00
	ADD	#0xfc
	LDA	r0x1167
	ADDC	#0xff
	JC	_00113_DS_
;	;.line	74; "malloc.c"	size = sizeof(struct header);
	LDA	#0x04
	STA	_malloc_STK00
	CLRA	
	STA	r0x1167
_00113_DS_:
;	;.line	76; "malloc.c"	for(h = __sdcc_heap_free, f = &__sdcc_heap_free; h; f = &(h->next_free), h = h->next_free)
	SETPTR	#(___sdcc_heap_free + 0)
	LDA	@_ROMPINC
	STA	r0x1168
	LDA	@_ROMPINC
	STA	r0x1169
	LDA	#(___sdcc_heap_free + 0)
	STA	r0x116A
	LDA	#high (___sdcc_heap_free + 0)
	STA	r0x116B
_00121_DS_:
	LDA	r0x1168
	ORA	r0x1169
	JZ	_00119_DS_
;	;.line	78; "malloc.c"	size_t blocksize = (char XDATA *)(h->next) - (char XDATA *)h;
	LDA	r0x1168
	STA	_ROMPL
	LDA	r0x1169
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x116C
	LDA	@_ROMPINC
	STA	r0x116D
	SETB	_C
	LDA	r0x116C
	SUBB	r0x1168
	STA	r0x116E
	LDA	r0x116D
	SUBB	r0x1169
	STA	r0x116F
;	;.line	79; "malloc.c"	if(blocksize >= size) // Found free block of sufficient size.
	SETB	_C
	LDA	r0x116E
	SUBB	_malloc_STK00
	LDA	r0x116F
	SUBB	r0x1167
	JNC	_00122_DS_
;	;.line	81; "malloc.c"	if(blocksize >= size + sizeof(struct header)) // It is worth creating a new free block
	LDA	#0x04
	ADD	_malloc_STK00
	STA	r0x1172
	CLRA	
	ADDC	r0x1167
	STA	r0x1173
	SETB	_C
	LDA	r0x116E
	SUBB	r0x1172
	LDA	r0x116F
	SUBB	r0x1173
	JNC	_00115_DS_
;	;.line	83; "malloc.c"	header_t *const newheader = (header_t *const)((char XDATA*)h + size);
	LDA	r0x1168
	ADD	_malloc_STK00
	STA	r0x1170
	LDA	r0x1169
	ADDC	r0x1167
	STA	r0x116F
	LDA	r0x1170
	STA	r0x116E
;	;.line	84; "malloc.c"	newheader->next = h->next;
	STA	_ROMPL
	LDA	r0x116F
	STA	_ROMPH
	LDA	r0x116C
	STA	@_ROMPINC
	LDA	r0x116D
	STA	@_ROMP
;	;.line	85; "malloc.c"	newheader->next_free = h->next_free;
	LDA	#0x02
	ADD	r0x116E
	STA	r0x116C
	CLRA	
	ADDC	r0x116F
	STA	r0x116D
	LDA	#0x02
	ADD	r0x1168
	STA	r0x1170
	CLRA	
	ADDC	r0x1169
	STA	r0x1171
	LDA	r0x1170
	STA	_ROMPL
	LDA	r0x1171
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1172
	LDA	@_ROMPINC
	STA	r0x1173
	LDA	r0x116C
	STA	_ROMPL
	LDA	r0x116D
	STA	_ROMPH
	LDA	r0x1172
	STA	@_ROMPINC
	LDA	r0x1173
	STA	@_ROMP
;	;.line	86; "malloc.c"	*f = newheader;
	LDA	r0x116A
	STA	_ROMPL
	LDA	r0x116B
	STA	_ROMPH
	LDA	r0x116E
	STA	@_ROMPINC
	LDA	r0x116F
	STA	@_ROMP
;	;.line	87; "malloc.c"	h->next = newheader;
	LDA	r0x1168
	STA	_ROMPL
	LDA	r0x1169
	STA	_ROMPH
	LDA	r0x116E
	STA	@_ROMPINC
	LDA	r0x116F
	STA	@_ROMP
	JMP	_00116_DS_
_00115_DS_:
;	;.line	90; "malloc.c"	*f = h->next_free;
	LDA	#0x02
	ADD	r0x1168
	STA	r0x116C
	CLRA	
	ADDC	r0x1169
	STA	r0x116D
	LDA	r0x116C
	STA	_ROMPL
	LDA	r0x116D
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x116E
	LDA	@_ROMPINC
	STA	r0x116F
	LDA	r0x116A
	STA	_ROMPL
	LDA	r0x116B
	STA	_ROMPH
	LDA	r0x116E
	STA	@_ROMPINC
	LDA	r0x116F
	STA	@_ROMP
_00116_DS_:
;	;.line	92; "malloc.c"	return(&(h->next_free));
	LDA	#0x02
	ADD	r0x1168
	STA	r0x116C
	CLRA	
	ADDC	r0x1169
	STA	r0x116D
	LDA	r0x116C
	STA	STK00
	LDA	r0x116D
	JMP	_00123_DS_
_00122_DS_:
;	;.line	76; "malloc.c"	for(h = __sdcc_heap_free, f = &__sdcc_heap_free; h; f = &(h->next_free), h = h->next_free)
	LDA	#0x02
	ADD	r0x1168
	STA	r0x116C
	CLRA	
	ADDC	r0x1169
	STA	r0x116D
	LDA	r0x116C
	STA	r0x116A
	LDA	r0x116D
	STA	r0x116B
	LDA	r0x116C
	STA	_ROMPL
	LDA	r0x116D
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1168
	LDA	@_ROMPINC
	STA	r0x1169
	JMP	_00121_DS_
_00119_DS_:
;	;.line	96; "malloc.c"	return(0);
	CLRA	
	STA	STK00
_00123_DS_:
;	;.line	97; "malloc.c"	}
	RET	
; exit point of _malloc
	.ENDFUNC _malloc
.globl ___sdcc_heap_init

;--------------------------------------------------------
	.FUNC ___sdcc_heap_init:$PNUM 0:$L:r0x1160:$L:r0x1162:$L:r0x1163
;--------------------------------------------------------
;	.line	55; "malloc.c"	__sdcc_heap_free = HEAP_START;
___sdcc_heap_init:	;Function start
	SETPTR	#(___sdcc_heap_free + 0)
	LDA	#(___sdcc_heap + 0)
	STA	@_ROMPINC
	LDA	#high (___sdcc_heap + 0)
	STA	@_ROMPINC
;	;.line	56; "malloc.c"	__sdcc_heap_free->next = HEAP_END;
	LDA	#(___sdcc_heap + 0)
;;genAssign from CODESPACE
	LDA	#low (___sdcc_heap_size + 0)
	STA	_ROMPL
	LDA	#high (___sdcc_heap_size + 0)
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1162
	LDA	@_ROMPINC
	STA	r0x1163
	LDA	r0x1162
	DECA	
	STA	r0x1162
	LDA	#0xff
	ADDC	r0x1163
	STA	r0x1163
	LDA	#(___sdcc_heap + 0)
	ADD	r0x1162
	STA	r0x1160
	LDA	#high (___sdcc_heap + 0)
	ADDC	r0x1163
	STA	r0x1163
	LDA	r0x1160
	SETPTR	#(___sdcc_heap + 0)
	LDA	r0x1160
	STA	@_ROMPINC
	LDA	r0x1163
	STA	@_ROMP
;	;.line	57; "malloc.c"	__sdcc_heap_free->next_free = 0;
	SETPTR	#(___sdcc_heap + 2)
	CLRA	
	STA	@_ROMPINC
	STA	@_ROMPINC
;	;.line	58; "malloc.c"	}
	RET	
; exit point of ___sdcc_heap_init
	.ENDFUNC ___sdcc_heap_init
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__sdcc_heap_init$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:Lmalloc.aligned_alloc$size$65536$15({2}SI:U),E,0,0
	;--cdb--S:Lmalloc.aligned_alloc$alignment$65536$15({2}SI:U),E,0,0
	;--cdb--S:Lmalloc.malloc$size$65536$31({2}SI:U),R,0,0,[_malloc_STK00,r0x1167]
	;--cdb--S:Lmalloc.malloc$h$65536$32({2}DX,STheader:S),R,0,0,[r0x1168,r0x1169]
	;--cdb--S:Lmalloc.malloc$f$65536$32({2}DX,DX,STheader:S),R,0,0,[r0x116A,r0x116B]
	;--cdb--S:Lmalloc.malloc$blocksize$196608$34({2}SI:U),R,0,0,[r0x116E,r0x116F]
	;--cdb--S:Lmalloc.malloc$newheader$327680$36({2}DX,STheader:S),R,0,0,[r0x116E,r0x116F]
	;--cdb--S:G$__sdcc_heap_free$0$0({2}DX,STheader:S),F,0,0
	;--cdb--S:G$__sdcc_heap$0$0({4}STheader:S),F,0,0
	;--cdb--S:G$__sdcc_heap_size$0$0({2}SI:U),D,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	___sdcc_heap
	.globl	___sdcc_heap_size

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_malloc
	.globl	_malloc_h_65536_32
	.globl	_malloc_f_65536_32
	.globl	_malloc_newheader_327680_36
	.globl	___sdcc_heap_free
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
_malloc_h_65536_32:	.ds	2

	.area DSEG(DATA)
_malloc_f_65536_32:	.ds	2

	.area DSEG(DATA)
_malloc_newheader_327680_36:	.ds	2

	.area XSEG(XDATA)
___sdcc_heap_free:	.ds	2

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_malloc_0	udata
r0x1160:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116C:	.ds	1
r0x116D:	.ds	1
r0x116E:	.ds	1
r0x116F:	.ds	1
r0x1170:	.ds	1
r0x1171:	.ds	1
r0x1172:	.ds	1
r0x1173:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_malloc_STK00:	.ds	1
	.globl _malloc_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x116F:NULL+0:4461:0
	;--cdb--W:r0x116E:NULL+0:4460:0
	;--cdb--W:r0x1171:NULL+0:4457:0
	;--cdb--W:r0x1170:NULL+0:4456:0
	;--cdb--W:r0x1171:NULL+0:-1:1
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x1162:NULL+0:4448:0
	;--cdb--W:r0x1160:NULL+0:-1:1
	end
