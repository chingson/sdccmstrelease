#include <ms326.h>
#include "ms326sphlib.h"

// MS326 uses differet BEEP,
// that beep can set frequency!!
void api_beep_start(BYTE bv, USHORT period )
{
    DMA_IL=0xff; // prevent wake up
    PDMAH=0x80; // for simulation purpose..

    PAG=0x3F; // beep uses loudest
    //SPKC=0x10;
    DAC_PH=period>>8;
    DAC_PL=period&0xff;
    BEEPC=bv|1;
    DACON|=3;
}
void api_beep_stop(void)
{

    BEEPC=0;
    DACON=0;
    PAG=0;
}



