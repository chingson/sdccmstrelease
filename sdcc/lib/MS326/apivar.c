#include"ms326typedef.h"

BYTE api_rampage;
USHORT api_endpage;
BYTE api_mode;
BYTE api_target_vol;
BYTE api_fifostart;
BYTE api_fifoend;
BYTE api_play_mode;
BYTE api_play_rampage;
BYTE api_play_fifostart;
BYTE api_play_fifoend;
USHORT api_play_startpage; // seperate play & rec
USHORT api_play_endpage; // seperate play & rec
BYTE api_play_spibuf; // used in 12bit mode
BYTE api_play_spiind;
BYTE api_maxrecopg;


// for full duplex current spi address
api_spi_addr_t api_play_spi_addr;
api_spi_addr_t api_rec_spi_addr;
//signed short api_offset0dbg;

