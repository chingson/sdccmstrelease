;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_fs2uint.c"
	.module _fs2uint
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--F:G$__fs2uint$0$0({2}DF,SI:U),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _fs2uint-code 
.globl ___fs2uint

;--------------------------------------------------------
	.FUNC ___fs2uint:$PNUM 4:$C:___fs2ulong\
:$L:r0x1162:$L:___fs2uint_STK00:$L:___fs2uint_STK01:$L:___fs2uint_STK02
;--------------------------------------------------------
;	.line	54; "_fs2uint.c"	unsigned int __fs2uint (float f)
___fs2uint:	;Function start
	STA	r0x1162
;	;.line	56; "_fs2uint.c"	unsigned long ul=__fs2ulong(f);
	LDA	___fs2uint_STK02
	STA	___fs2ulong_STK02
	LDA	___fs2uint_STK01
	STA	___fs2ulong_STK01
	LDA	___fs2uint_STK00
	STA	___fs2ulong_STK00
	LDA	r0x1162
	CALL	___fs2ulong
	STA	r0x1162
	LDA	STK02
;	;.line	57; "_fs2uint.c"	if (ul>=UINT_MAX) return UINT_MAX;
	INCA	
	CLRA	
	ADDC	STK01
	LDA	STK00
	ADDC	#0xff
	LDA	r0x1162
	ADDC	#0xff
	JNC	_00106_DS_
	LDA	#0xff
	STA	STK00
	JMP	_00107_DS_
_00106_DS_:
;	;.line	58; "_fs2uint.c"	return ul;
	LDA	STK02
	STA	STK00
	LDA	STK01
_00107_DS_:
;	;.line	59; "_fs2uint.c"	}
	RET	
; exit point of ___fs2uint
	.ENDFUNC ___fs2uint
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:L_fs2uint.__fs2uint$f$65536$21({4}SF:S),R,0,0,[___fs2uint_STK02,___fs2uint_STK01,___fs2uint_STK00,r0x1162]
	;--cdb--S:L_fs2uint.__fs2uint$ul$65536$22({4}SL:U),R,0,0,[___fs2uint_STK02,___fs2uint_STK01,___fs2uint_STK00,r0x1162]
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	___fs2ulong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___fs2uint
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__fs2uint_0	udata
r0x1162:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
___fs2uint_STK00:	.ds	1
	.globl ___fs2uint_STK00
___fs2uint_STK01:	.ds	1
	.globl ___fs2uint_STK01
___fs2uint_STK02:	.ds	1
	.globl ___fs2uint_STK02
	.globl ___fs2ulong_STK02
	.globl ___fs2ulong_STK01
	.globl ___fs2ulong_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:___fs2uint_STK00:NULL+0:14:0
	;--cdb--W:___fs2uint_STK01:NULL+0:13:0
	;--cdb--W:___fs2uint_STK02:NULL+0:12:0
	;--cdb--W:r0x1163:NULL+0:12:0
	;--cdb--W:r0x1164:NULL+0:13:0
	end
