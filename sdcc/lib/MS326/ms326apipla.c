#include"ms326.h"
#include"ms326typedef.h"
#include"ms326sphlib.h"

extern USHORT api_play_endpage;
extern USHORT api_play_startpage;
extern BYTE api_play_mode;
extern BYTE api_play_rampage;
extern BYTE api_play_fifostart;
extern BYTE api_play_fifoend;
extern BYTE api_play_spibuf;
extern BYTE api_play_spiind;
extern api_spi_addr_t api_play_spi_addr;
#define USEROMP12 0



//static unsigned short exp_lut[8] = { 0, 132, 396, 924, 1980, 4092, 8316, 16764 };
// 2015, using half page as unit, 128 bytes aligned
// for playing, 128 bytes is enough,
static BYTE get12halfpage(void)
{
again:
    SPIOPRAH=api_play_spibuf>>3;
    SPIOP=0x48;// read a half page
    if(SPIL&0x80)
    {
        SPIOP=0x20;
        SPIL=0;
    }
    else
    {
        SPIL=0x80;
    }
    if((SPIOP&0x80)||((SPIH==(BYTE)(api_play_endpage>>8))
                      && SPIM==(BYTE)(api_play_endpage&0xff)))
    {
        if(api_play_mode & API_PLAYMODE_REPEAT)
        {
            SPIH=api_play_startpage>>8;
            SPIM=api_play_startpage&0xff;
            SPIL=0; // from 0
            SPIOP=0x10; // reset endcode count
            // read again
            goto again;
        }
        else
            return 0;
    }
#if USEROMP12
    ROMPUW=0x8000|(api_play_spibuf<<4);
#else
    HWPA=0x8000|(api_play_spibuf<<4);
#endif
    api_play_spiind=0;
    return 1;

}
BYTE api_decode_12bit(BYTE page) // by api_play_rampage, we decode a page of 12 bit data
{
    USHORT voiceptr=(page<<4)|0x8000;
    USHORT dataptr=(api_play_spibuf<<4)|api_play_spiind|0x8000;
    BYTE byte0,byte1,byte2;
#if USEROMP12
    do {
        ROMPUW=dataptr;
        // ROMPTR will be update in get12halfpage
        byte0=ROMPINC;
        if(!(ROMPL&0x7f)) {
            if(!get12halfpage()) return 0;
        }
        byte1=ROMPINC;
        if(!(ROMPL&0x7f)) {
            if(!get12halfpage()) return 0;
        }
        byte2=ROMPINC;
        if(!(ROMPL&0x7f)) {
            if(!get12halfpage()) return 0;
        }
        dataptr=ROMPUW;
        ROMPUW=voiceptr;
        ROMPINC=(BYTE)(byte2<<4);
        ROMPINC=byte0;
        ROMPINC=(byte2&0xf0);
        ROMPINC=byte1;
        voiceptr=ROMPUW;
    } while(ROMPL&0x7f);
    api_play_spiind=dataptr&0x7f;
#else
    HWPSEL=0;
    HWPA=dataptr;
    HWPSEL=1;
    HWPA=voiceptr;
    do {
        HWPSEL=0;
        // ROMPTR will be update in get12halfpage
        byte0=HWDINC;
        if(!(HWPAL&0x7f)) {
            if(!get12halfpage()) return 0;
        }
        byte1=HWDINC;
        if(!(HWPAL&0x7f)) {
            if(!get12halfpage()) return 0;
        }
        byte2=HWDINC;
        if(!(HWPAL&0x7f)) {
            if(!get12halfpage()) return 0;
        }
        HWPSEL=1;


        HWDINC=(BYTE)(byte2<<4);
        HWDINC=byte0;
        HWDINC=(byte2&0xf0);
        HWDINC=byte1;

    } while(HWPAL&0x7f);
    HWPSEL=0;
    api_play_spiind=HWPAL&0x7f;

#endif

    return 1;

}
BYTE api_play_start(USHORT start_page, USHORT end_page, USHORT period, BYTE playmode, BYTE dacosr, BYTE dacgcl, BYTE *fifo)
{
    if(ADCON&1) // if ADC already enabled
        api_clear_filter_mem(1); // clear 700~77F
    else
        api_clear_filter_mem(0);
    SYSC2|=0x08; // 128 byte wake up a time!!
    api_play_startpage = start_page;
    SPIH=start_page>>8;
    SPIM=start_page&0xff;
    SPIL=0;
    api_play_endpage=end_page;
    api_play_mode=playmode;
    DMA_IL=0; // use 1 as wakeup address
    DACGCL=dacgcl;

    // for 12 bit mode, we need another 128 byte for decode

    if(playmode&API_PLAYMODE_12BIT)
    {

        api_play_spibuf=(((USHORT)fifo)&0x7ff)>>4; // unit is 16byte , usually this is 0x50
        api_play_fifostart=api_play_spibuf+8; //16*8=128
        api_play_fifoend=api_play_fifostart+15; // we use XF as the page addr 0x6f
        api_play_spiind=0; // start from 0;

    }
    else
    {

        api_play_fifostart=(((USHORT)fifo)&0x7ff)>>4; // unit is 16byte , usually this is 0x50
        api_play_fifoend=api_play_fifostart+15; // we use XF as the page addr 0x6f
    }
    ADP_IND=0x80;// following is 8 bit wide
    PPAGES=(api_play_fifostart&0xf)|((api_play_fifoend&0xf)<<4); // f0
    ADP_IND=0;// following is 6 bits only
    PPAGES=(api_play_fifostart>>4)|((api_play_fifoend&0xf0)>>1);// 35

    if(!dacosr)
        RCLKDIV|=0x80;
    else
        RCLKDIV&=0x7f;

    SPIOP=0x10; // clear end-code
    PDMAH=0x80;
    ULAWC&=0xdf; // no touch record settings!!
    if(playmode&API_PLAYMODE_NOF)
        period<<=2;
    DAC_PH=period>>8;
    if(!(period&0x10))
        period-=32;

    DAC_PL=(period&0xff)|0x1F; // sync to pwm freq

    api_play_rampage=api_play_fifostart;

    if(!(playmode&API_PLAYMODE_12BIT))
    {
        SPIOPRAH=api_play_rampage>>3; // initially, read 256 byes
        SPIOP=0x08; // read page to 0x2xx
        SPIOP=0x20; // address increase a page
    }
    else
    {
        SPIOPRAH=api_play_spibuf>>3;
        SPIOP=0x48; // initially, read half page
        SPIL=0x80;
    }
    if(SPIOP&0x80)
    {
        return 0;
    }
    if(playmode&API_PLAYMODE_ULAW)
    {
        U2LSH=0;
        ULAWC|=0xA0;
    } else if(playmode & API_PLAYMODE_12BIT)
    {
        api_decode_12bit(api_play_rampage);// unit is 16
        api_decode_12bit(api_play_rampage+8); // decode 2 half pages
    } else
        DACON=0x20; // bit 5 is adpcm enable
    if(playmode&API_PLAYMODE_NOF) // skip filter
        DACON|=0x1E;
    else
        DACON|=0x1a;
    api_play_rampage=api_play_fifostart; // change it back

    while(PDMAL==0);
    GIF=0xbf; // clear all pending flags
    if(playmode&API_PLAYMODE_PA7)
    {

        SPKC|=0x48; ;// PA7 out
        PADIR|=0x80;
    }
    else
        DACON|=1; // enable PA finally
    // save
    api_play_spi_addr.addrhm=SPIMH;
    api_play_spi_addr.addrl=SPIL;
    return 1;

}

