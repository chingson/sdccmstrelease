;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"sqrtf.c"
	.module sqrtf
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fsqrtf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,2
	;--cdb--F:G$sqrtf$0$0({2}DF,SF:S),C,0,2,0,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; sqrtf-code 
.globl _sqrtf

;--------------------------------------------------------
	.FUNC _sqrtf:$PNUM 4:$C:___fseq:$C:___fslt:$C:_frexpf:$C:___fsmul\
:$C:___fsadd:$C:___fsdiv:$C:_ldexpf\
:$L:r0x1164:$L:_sqrtf_STK00:$L:_sqrtf_STK01:$L:_sqrtf_STK02:$L:r0x1165\
:$L:r0x1166:$L:r0x1168:$L:r0x1167:$L:r0x116C:$L:r0x116B\
:$L:r0x116A:$L:r0x1169
;--------------------------------------------------------
;	.line	37; "sqrtf.c"	float sqrtf(float x) _FLOAT_FUNC_REENTRANT
_sqrtf:	;Function start
	STA	r0x1164
;	;.line	42; "sqrtf.c"	if (x==0.0) return x;
	LDA	_sqrtf_STK02
	ORA	_sqrtf_STK01
	ORA	_sqrtf_STK00
	ORA	r0x1164
	JNZ	_00111_DS_
	LDA	_sqrtf_STK02
	STA	STK02
	LDA	_sqrtf_STK01
	STA	STK01
	LDA	_sqrtf_STK00
	STA	STK00
	LDA	r0x1164
	JMP	_00115_DS_
_00111_DS_:
;	;.line	43; "sqrtf.c"	else if (x==1.0) return 1.0;
	CLRA	
	STA	___fseq_STK06
	STA	___fseq_STK05
	LDA	#0x80
	STA	___fseq_STK04
	LDA	#0x3f
	STA	___fseq_STK03
	LDA	_sqrtf_STK02
	STA	___fseq_STK02
	LDA	_sqrtf_STK01
	STA	___fseq_STK01
	LDA	_sqrtf_STK00
	STA	___fseq_STK00
	LDA	r0x1164
	CALL	___fseq
	JZ	_00108_DS_
	CLRA	
	STA	STK02
	STA	STK01
	LDA	#0x80
	STA	STK00
	LDA	#0x3f
	JMP	_00115_DS_
_00108_DS_:
;	;.line	44; "sqrtf.c"	else if (x<0.0)
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	STA	___fslt_STK04
	STA	___fslt_STK03
	LDA	_sqrtf_STK02
	STA	___fslt_STK02
	LDA	_sqrtf_STK01
	STA	___fslt_STK01
	LDA	_sqrtf_STK00
	STA	___fslt_STK00
	LDA	r0x1164
	CALL	___fslt
	JZ	_00112_DS_
;	;.line	46; "sqrtf.c"	errno=EDOM;
	LDA	#0x21
	STA	_errno
;	;.line	47; "sqrtf.c"	return 0.0;
	CLRA	
	STA	(_errno + 1)
	STA	STK02
	STA	STK01
	STA	STK00
	CLRA	
	JMP	_00115_DS_
_00112_DS_:
;	;.line	49; "sqrtf.c"	f=frexpf(x, &n);
	LDA	#(_sqrtf_n_65536_26 + 0)
	STA	_frexpf_STK04
	LDA	#high (_sqrtf_n_65536_26 + 0)
	STA	_frexpf_STK03
	LDA	_sqrtf_STK02
	STA	_frexpf_STK02
	LDA	_sqrtf_STK01
	STA	_frexpf_STK01
	LDA	_sqrtf_STK00
	STA	_frexpf_STK00
	LDA	r0x1164
	CALL	_frexpf
	STA	r0x1164
	LDA	STK00
	STA	_sqrtf_STK00
	LDA	STK01
	STA	_sqrtf_STK01
	LDA	STK02
	STA	_sqrtf_STK02
;	;.line	50; "sqrtf.c"	y=0.41731+0.59016*f; /*Educated guess*/
	STA	___fsmul_STK06
	LDA	_sqrtf_STK01
	STA	___fsmul_STK05
	LDA	_sqrtf_STK00
	STA	___fsmul_STK04
	LDA	r0x1164
	STA	___fsmul_STK03
	LDA	#0xba
	STA	___fsmul_STK02
	LDA	#0x14
	STA	___fsmul_STK01
	LDA	#0x17
	STA	___fsmul_STK00
	LDA	#0x3f
	CALL	___fsmul
	STA	r0x1168
	LDA	#0xa8
	STA	___fsadd_STK06
	INCA	
	STA	___fsadd_STK05
	LDA	#0xd5
	STA	___fsadd_STK04
	LDA	#0x3e
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1168
	CALL	___fsadd
	STA	r0x1168
	LDA	STK00
	STA	r0x1167
	LDA	STK01
	STA	r0x1166
	LDA	STK02
	STA	r0x1165
;	;.line	52; "sqrtf.c"	y+=f/y;
	STA	___fsdiv_STK06
	LDA	r0x1166
	STA	___fsdiv_STK05
	LDA	r0x1167
	STA	___fsdiv_STK04
	LDA	r0x1168
	STA	___fsdiv_STK03
	LDA	_sqrtf_STK02
	STA	___fsdiv_STK02
	LDA	_sqrtf_STK01
	STA	___fsdiv_STK01
	LDA	_sqrtf_STK00
	STA	___fsdiv_STK00
	LDA	r0x1164
	CALL	___fsdiv
	STA	r0x116C
	LDA	STK02
	STA	___fsadd_STK06
	LDA	STK01
	STA	___fsadd_STK05
	LDA	STK00
	STA	___fsadd_STK04
	LDA	r0x116C
	STA	___fsadd_STK03
	LDA	r0x1165
	STA	___fsadd_STK02
	LDA	r0x1166
	STA	___fsadd_STK01
	LDA	r0x1167
	STA	___fsadd_STK00
	LDA	r0x1168
	CALL	___fsadd
	STA	r0x1168
	LDA	STK00
	STA	r0x1167
	LDA	STK01
	STA	r0x1166
	LDA	STK02
	STA	r0x1165
;	;.line	53; "sqrtf.c"	y=ldexpf(y, -2) + f/y; /*Faster version of 0.25 * y + f/y*/
	LDA	#0xfe
	STA	_ldexpf_STK04
	INCA	
	STA	_ldexpf_STK03
	LDA	r0x1165
	STA	_ldexpf_STK02
	LDA	r0x1166
	STA	_ldexpf_STK01
	LDA	r0x1167
	STA	_ldexpf_STK00
	LDA	r0x1168
	CALL	_ldexpf
	STA	r0x116C
	LDA	STK00
	STA	r0x116B
	LDA	STK01
	STA	r0x116A
	LDA	STK02
	STA	r0x1169
	LDA	r0x1165
	STA	___fsdiv_STK06
	LDA	r0x1166
	STA	___fsdiv_STK05
	LDA	r0x1167
	STA	___fsdiv_STK04
	LDA	r0x1168
	STA	___fsdiv_STK03
	LDA	_sqrtf_STK02
	STA	___fsdiv_STK02
	LDA	_sqrtf_STK01
	STA	___fsdiv_STK01
	LDA	_sqrtf_STK00
	STA	___fsdiv_STK00
	LDA	r0x1164
	CALL	___fsdiv
	STA	r0x1164
	LDA	STK02
	STA	___fsadd_STK06
	LDA	STK01
	STA	___fsadd_STK05
	LDA	STK00
	STA	___fsadd_STK04
	LDA	r0x1164
	STA	___fsadd_STK03
	LDA	r0x1169
	STA	___fsadd_STK02
	LDA	r0x116A
	STA	___fsadd_STK01
	LDA	r0x116B
	STA	___fsadd_STK00
	LDA	r0x116C
	CALL	___fsadd
	STA	r0x1164
	LDA	STK00
	STA	_sqrtf_STK00
	LDA	STK01
	STA	_sqrtf_STK01
	LDA	STK02
	STA	_sqrtf_STK02
;	;.line	55; "sqrtf.c"	if (n&1)
	LDA	_sqrtf_n_65536_26
	SHR	
	JNC	_00114_DS_
;	;.line	57; "sqrtf.c"	y*=0.7071067812;
	LDA	_sqrtf_STK02
	STA	___fsmul_STK06
	LDA	_sqrtf_STK01
	STA	___fsmul_STK05
	LDA	_sqrtf_STK00
	STA	___fsmul_STK04
	LDA	r0x1164
	STA	___fsmul_STK03
	LDA	#0xf3
	STA	___fsmul_STK02
	LDA	#0x04
	STA	___fsmul_STK01
	LDA	#0x35
	STA	___fsmul_STK00
	LDA	#0x3f
	CALL	___fsmul
	STA	r0x1164
	LDA	STK00
	STA	_sqrtf_STK00
	LDA	STK01
	STA	_sqrtf_STK01
	LDA	STK02
	STA	_sqrtf_STK02
;	;.line	58; "sqrtf.c"	++n;
	LDA	_sqrtf_n_65536_26
	INCA	
	STA	_sqrtf_n_65536_26
	CLRA	
	ADDC	(_sqrtf_n_65536_26 + 1)
	STA	(_sqrtf_n_65536_26 + 1)
_00114_DS_:
;	;.line	60; "sqrtf.c"	return ldexpf(y, n>>1);
	LDA	(_sqrtf_n_65536_26 + 1)
	SHRS	
	STA	r0x1166
	LDA	_sqrtf_n_65536_26
	ROR	
	STA	_ldexpf_STK04
	LDA	r0x1166
	STA	_ldexpf_STK03
	LDA	_sqrtf_STK02
	STA	_ldexpf_STK02
	LDA	_sqrtf_STK01
	STA	_ldexpf_STK01
	LDA	_sqrtf_STK00
	STA	_ldexpf_STK00
	LDA	r0x1164
	CALL	_ldexpf
_00115_DS_:
;	;.line	61; "sqrtf.c"	}
	RET	
; exit point of _sqrtf
	.ENDFUNC _sqrtf
	;--cdb--S:Lsqrtf.sqrtf$x$65536$25({4}SF:S),R,0,0,[_sqrtf_STK02,_sqrtf_STK01,_sqrtf_STK00,r0x1164]
	;--cdb--S:Lsqrtf.sqrtf$f$65536$26({4}SF:S),R,0,0,[r0x1161,r0x1162,r0x1163,r0x1164]
	;--cdb--S:Lsqrtf.sqrtf$y$65536$26({4}SF:S),R,0,0,[r0x1165,r0x1166,r0x1167,r0x1168]
	;--cdb--S:Lsqrtf.sqrtf$n$65536$26({2}SI:S),B,1,1
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,2
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$errno$0$0({2}SI:S),E,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,2
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_frexpf
	.globl	_ldexpf
	.globl	___fseq
	.globl	___fslt
	.globl	___fsmul
	.globl	___fsadd
	.globl	___fsdiv
	.globl	_errno

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_sqrtf
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_sqrtf_0	udata
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116C:	.ds	1
_sqrtf_n_65536_26:	.ds	2
	.area DSEG (DATA); (local stack unassigned) 
_sqrtf_STK00:	.ds	1
	.globl _sqrtf_STK00
_sqrtf_STK01:	.ds	1
	.globl _sqrtf_STK01
_sqrtf_STK02:	.ds	1
	.globl _sqrtf_STK02
	.globl ___fseq_STK06
	.globl ___fseq_STK05
	.globl ___fseq_STK04
	.globl ___fseq_STK03
	.globl ___fseq_STK02
	.globl ___fseq_STK01
	.globl ___fseq_STK00
	.globl ___fslt_STK06
	.globl ___fslt_STK05
	.globl ___fslt_STK04
	.globl ___fslt_STK03
	.globl ___fslt_STK02
	.globl ___fslt_STK01
	.globl ___fslt_STK00
	.globl _frexpf_STK04
	.globl _frexpf_STK03
	.globl _frexpf_STK02
	.globl _frexpf_STK01
	.globl _frexpf_STK00
	.globl ___fsmul_STK06
	.globl ___fsmul_STK05
	.globl ___fsmul_STK04
	.globl ___fsmul_STK03
	.globl ___fsmul_STK02
	.globl ___fsmul_STK01
	.globl ___fsmul_STK00
	.globl ___fsadd_STK06
	.globl ___fsadd_STK05
	.globl ___fsadd_STK04
	.globl ___fsadd_STK03
	.globl ___fsadd_STK02
	.globl ___fsadd_STK01
	.globl ___fsadd_STK00
	.globl ___fsdiv_STK06
	.globl ___fsdiv_STK05
	.globl ___fsdiv_STK04
	.globl ___fsdiv_STK03
	.globl ___fsdiv_STK02
	.globl ___fsdiv_STK01
	.globl ___fsdiv_STK00
	.globl _ldexpf_STK04
	.globl _ldexpf_STK03
	.globl _ldexpf_STK02
	.globl _ldexpf_STK01
	.globl _ldexpf_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:_sqrtf_STK00:NULL+0:14:0
	;--cdb--W:_sqrtf_STK01:NULL+0:13:0
	;--cdb--W:_sqrtf_STK02:NULL+0:12:0
	;--cdb--W:r0x1165:NULL+0:12:0
	;--cdb--W:r0x1166:NULL+0:13:0
	;--cdb--W:r0x1167:NULL+0:14:0
	;--cdb--W:r0x116B:NULL+0:14:0
	;--cdb--W:r0x116A:NULL+0:13:0
	;--cdb--W:r0x1169:NULL+0:12:0
	;--cdb--W:r0x1165:NULL+0:-1:1
	;--cdb--W:r0x1166:NULL+0:-1:1
	;--cdb--W:r0x1168:NULL+0:-1:1
	;--cdb--W:r0x1167:NULL+0:-1:1
	;--cdb--W:r0x1164:NULL+0:-1:1
	end
