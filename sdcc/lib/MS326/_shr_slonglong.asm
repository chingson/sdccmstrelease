;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.3.0 #8604 (Sep 15 2014) (MSVC)
; This file was generated Sat Oct 11 21:41:17 2014
;--------------------------------------------------------
; MST port for the MS322 series simple CPU
;--------------------------------------------------------
	.module _shr_slonglong
	;.list	p=MS205
	;.radix dec
	.include "ms326sfr.def"
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
;--------------------------------------------------------
; Code Head 
;--------------------------------------------------------
	.area CSEG	 (CODE) 
;--------------------------------------------------------
; code
;--------------------------------------------------------
; sh_slonglong, assembly is the source!!
; input: _PTRCL is the data byte
; input: ACC is the shift num
; input: STK0 is high byte
.FUNC	__shr_slonglong
__shr_slonglong:	;Function start
; 2 exit points
	sta 	_ROMPL ; temp var
	add	#0xc0
	jc	shift_too_large
	lda	_ROMPL
loop_shr:
	jnz	cont_shift
	ret
cont_shift:
	lda	STK00
	shrs
	sta	STK00
	lda	STK01
	ror
	sta	STK01
	lda	STK02
	ror
	sta	STK02
	lda	STK03
	ror
	sta	STK03
	lda	STK04
	ror
	sta	STK04
	lda	STK05
	ror
	sta	STK05
	lda	STK06
	ror
	sta	STK06
	lda	_PTRCL
	ror
	sta	_PTRCL
	lda	_ROMPL
	deca
	sta	_ROMPL
	jmp	loop_shr

	
shift_too_large:
	lda	STK00
	jmi	givem1
	clra
done:
	sta	STK00
	sta	STK01
	sta	STK02
	sta	STK03
	sta	STK04
	sta	STK05
	sta	STK06
	sta	_PTRCL
	ret
givem1:
	lda	#0xff
	jmp	done
	

; exit point of _shr_slonglong


;	code size estimation:
;	   21+   10 =    31 instructions (   31 byte)

;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__shr_slonglong
	.globl	STK06
	.globl	STK05
	.globl	STK04
	.globl	STK03
	.globl	STK00
	.globl	STK01
	.globl	STK02


;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	;end
