;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_strrchr.c"
	.module _strrchr
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--F:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _strrchr-code 
.globl _strrchr

;--------------------------------------------------------
	.FUNC _strrchr:$PNUM 3:$L:r0x1160:$L:_strrchr_STK00:$L:_strrchr_STK01:$L:r0x1162:$L:r0x1163\
:$L:r0x1164
;--------------------------------------------------------
;	.line	31; "_strrchr.c"	char * strrchr ( char * string, char ch )
_strrchr:	;Function start
	STA	r0x1160
;	;.line	33; "_strrchr.c"	char * start = string;
	LDA	_strrchr_STK00
	STA	r0x1162
	LDA	r0x1160
	STA	r0x1163
_00105_DS_:
;	;.line	35; "_strrchr.c"	while (*string++)                       /* find end of string */
	LDA	_strrchr_STK00
	STA	_ROMPL
	LDA	r0x1160
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1164
	LDA	_strrchr_STK00
	INCA	
	STA	_strrchr_STK00
	CLRA	
	ADDC	r0x1160
	STA	r0x1160
	LDA	r0x1164
	JNZ	_00105_DS_
_00109_DS_:
;	;.line	38; "_strrchr.c"	while (--string != start && *string != ch)
	LDA	_strrchr_STK00
	DECA	
	STA	_strrchr_STK00
	LDA	#0xff
	ADDC	r0x1160
	STA	r0x1160
	LDA	r0x1162
	XOR	_strrchr_STK00
	JNZ	_00141_DS_
	LDA	r0x1163
	XOR	r0x1160
_00141_DS_:
	JZ	_00111_DS_
	LDA	_strrchr_STK00
	STA	_ROMPL
	LDA	r0x1160
	STA	_ROMPH
	LDA	@_ROMPINC
	XOR	_strrchr_STK01
	JNZ	_00109_DS_
_00111_DS_:
;	;.line	41; "_strrchr.c"	if (*string == ch)                      /* char found ? */
	LDA	_strrchr_STK00
	STA	_ROMPL
	LDA	r0x1160
	STA	_ROMPH
	LDA	@_ROMPINC
	XOR	_strrchr_STK01
	JNZ	_00113_DS_
;	;.line	42; "_strrchr.c"	return( (char *)string );
	LDA	_strrchr_STK00
	STA	STK00
	LDA	r0x1160
	JMP	_00114_DS_
_00113_DS_:
;	;.line	44; "_strrchr.c"	return (NULL) ;
	CLRA	
	STA	STK00
_00114_DS_:
;	;.line	45; "_strrchr.c"	}
	RET	
; exit point of _strrchr
	.ENDFUNC _strrchr
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_strrchr.strrchr$ch$65536$21({1}SC:U),R,0,0,[_strrchr_STK01]
	;--cdb--S:L_strrchr.strrchr$string$65536$21({2}DG,SC:U),R,0,0,[_strrchr_STK00,r0x1160]
	;--cdb--S:L_strrchr.strrchr$start$65536$22({2}DG,SC:U),R,0,0,[r0x1162,r0x1163]
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_strrchr
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__strrchr_0	udata
r0x1160:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_strrchr_STK00:	.ds	1
	.globl _strrchr_STK00
_strrchr_STK01:	.ds	1
	.globl _strrchr_STK01
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1164:NULL+0:-1:1
	;--cdb--W:r0x1162:NULL+0:-1:1
	end
