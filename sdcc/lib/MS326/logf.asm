;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"logf.c"
	.module logf
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Flogf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,2
	;--cdb--F:G$logf$0$0({2}DF,SF:S),C,0,2,0,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; logf-code 
.globl _logf

;--------------------------------------------------------
	.FUNC _logf:$PNUM 4:$C:___fslt:$C:_frexpf:$C:___fssub:$C:___fsmul\
:$C:___fsadd:$C:___fsdiv:$C:___sint2fs\
:$L:r0x1164:$L:_logf_STK00:$L:_logf_STK01:$L:_logf_STK02:$L:r0x1165\
:$L:r0x1166:$L:r0x1168:$L:r0x1167:$L:r0x1169:$L:r0x116C\
:$L:r0x116B:$L:r0x116A
;--------------------------------------------------------
;	.line	216; "logf.c"	float logf(float x) _FLOAT_FUNC_REENTRANT
_logf:	;Function start
	STA	r0x1164
;	;.line	222; "logf.c"	if (x<=0.0)
	LDA	_logf_STK02
	STA	___fslt_STK06
	LDA	_logf_STK01
	STA	___fslt_STK05
	LDA	_logf_STK00
	STA	___fslt_STK04
	LDA	r0x1164
	STA	___fslt_STK03
	CLRA	
	STA	___fslt_STK02
	STA	___fslt_STK01
	STA	___fslt_STK00
	CALL	___fslt
	JNZ	_00106_DS_
;	;.line	224; "logf.c"	errno=EDOM;
	LDA	#0x21
	STA	_errno
;	;.line	225; "logf.c"	return 0.0;
	CLRA	
	STA	(_errno + 1)
	STA	STK02
	STA	STK01
	STA	STK00
	CLRA	
	JMP	_00110_DS_
_00106_DS_:
;	;.line	227; "logf.c"	f=frexpf(x, &n);
	LDA	#(_logf_n_65536_26 + 0)
	STA	_frexpf_STK04
	LDA	#high (_logf_n_65536_26 + 0)
	STA	_frexpf_STK03
	LDA	_logf_STK02
	STA	_frexpf_STK02
	LDA	_logf_STK01
	STA	_frexpf_STK01
	LDA	_logf_STK00
	STA	_frexpf_STK00
	LDA	r0x1164
	CALL	_frexpf
	STA	r0x1164
	LDA	STK00
	STA	_logf_STK00
	LDA	STK01
	STA	_logf_STK01
	LDA	STK02
	STA	_logf_STK02
;	;.line	228; "logf.c"	znum=f-0.5;
	CLRA	
	STA	___fssub_STK06
	STA	___fssub_STK05
	STA	___fssub_STK04
	LDA	#0x3f
	STA	___fssub_STK03
	LDA	_logf_STK02
	STA	___fssub_STK02
	LDA	_logf_STK01
	STA	___fssub_STK01
	LDA	_logf_STK00
	STA	___fssub_STK00
	LDA	r0x1164
	CALL	___fssub
	STA	r0x1168
	LDA	STK00
	STA	r0x1167
	LDA	STK01
	STA	r0x1166
	LDA	STK02
	STA	r0x1165
;	;.line	229; "logf.c"	if (f>C0)
	LDA	_logf_STK02
	STA	___fslt_STK06
	LDA	_logf_STK01
	STA	___fslt_STK05
	LDA	_logf_STK00
	STA	___fslt_STK04
	LDA	r0x1164
	STA	___fslt_STK03
	LDA	#0xf3
	STA	___fslt_STK02
	LDA	#0x04
	STA	___fslt_STK01
	LDA	#0x35
	STA	___fslt_STK00
	LDA	#0x3f
	CALL	___fslt
	JZ	_00108_DS_
;	;.line	231; "logf.c"	znum-=0.5;
	CLRA	
	STA	___fssub_STK06
	STA	___fssub_STK05
	STA	___fssub_STK04
	LDA	#0x3f
	STA	___fssub_STK03
	LDA	r0x1165
	STA	___fssub_STK02
	LDA	r0x1166
	STA	___fssub_STK01
	LDA	r0x1167
	STA	___fssub_STK00
	LDA	r0x1168
	CALL	___fssub
	STA	r0x116C
	LDA	STK02
	STA	r0x1165
	LDA	STK01
	STA	r0x1166
	LDA	STK00
	STA	r0x1167
	LDA	r0x116C
	STA	r0x1168
;	;.line	232; "logf.c"	zden=(f*0.5)+0.5;
	LDA	_logf_STK02
	STA	___fsmul_STK06
	LDA	_logf_STK01
	STA	___fsmul_STK05
	LDA	_logf_STK00
	STA	___fsmul_STK04
	LDA	r0x1164
	STA	___fsmul_STK03
	CLRA	
	STA	___fsmul_STK02
	STA	___fsmul_STK01
	STA	___fsmul_STK00
	LDA	#0x3f
	CALL	___fsmul
	STA	r0x1164
	CLRA	
	STA	___fsadd_STK06
	STA	___fsadd_STK05
	STA	___fsadd_STK04
	LDA	#0x3f
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1164
	CALL	___fsadd
	STA	r0x1164
	LDA	STK00
	STA	_logf_STK00
	LDA	STK01
	STA	_logf_STK01
	LDA	STK02
	STA	_logf_STK02
	JMP	_00109_DS_
_00108_DS_:
;	;.line	236; "logf.c"	n--;
	LDA	_logf_n_65536_26
	DECA	
	STA	_logf_n_65536_26
	LDA	#0xff
	ADDC	(_logf_n_65536_26 + 1)
	STA	(_logf_n_65536_26 + 1)
;	;.line	237; "logf.c"	zden=znum*0.5+0.5;
	LDA	r0x1165
	STA	___fsmul_STK06
	LDA	r0x1166
	STA	___fsmul_STK05
	LDA	r0x1167
	STA	___fsmul_STK04
	LDA	r0x1168
	STA	___fsmul_STK03
	CLRA	
	STA	___fsmul_STK02
	STA	___fsmul_STK01
	STA	___fsmul_STK00
	LDA	#0x3f
	CALL	___fsmul
	STA	r0x116C
	CLRA	
	STA	___fsadd_STK06
	STA	___fsadd_STK05
	STA	___fsadd_STK04
	LDA	#0x3f
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x116C
	CALL	___fsadd
	STA	r0x116C
	LDA	STK02
	STA	_logf_STK02
	LDA	STK01
	STA	_logf_STK01
	LDA	STK00
	STA	_logf_STK00
	LDA	r0x116C
	STA	r0x1164
_00109_DS_:
;	;.line	239; "logf.c"	z=znum/zden;
	LDA	_logf_STK02
	STA	___fsdiv_STK06
	LDA	_logf_STK01
	STA	___fsdiv_STK05
	LDA	_logf_STK00
	STA	___fsdiv_STK04
	LDA	r0x1164
	STA	___fsdiv_STK03
	LDA	r0x1165
	STA	___fsdiv_STK02
	LDA	r0x1166
	STA	___fsdiv_STK01
	LDA	r0x1167
	STA	___fsdiv_STK00
	LDA	r0x1168
	CALL	___fsdiv
	STA	r0x1164
	LDA	STK00
	STA	_logf_STK00
	LDA	STK01
	STA	_logf_STK01
	LDA	STK02
	STA	_logf_STK02
;	;.line	240; "logf.c"	w=z*z;
	STA	___fsmul_STK06
	LDA	_logf_STK01
	STA	___fsmul_STK05
	LDA	_logf_STK00
	STA	___fsmul_STK04
	LDA	r0x1164
	STA	___fsmul_STK03
	LDA	_logf_STK02
	STA	___fsmul_STK02
	LDA	_logf_STK01
	STA	___fsmul_STK01
	LDA	_logf_STK00
	STA	___fsmul_STK00
	LDA	r0x1164
	CALL	___fsmul
	STA	r0x1168
	LDA	STK00
	STA	r0x1167
	LDA	STK01
	STA	r0x1166
	LDA	STK02
	STA	r0x1165
;	;.line	242; "logf.c"	Rz=z+z*(w*A(w)/B(w));
	STA	___fsmul_STK06
	LDA	r0x1166
	STA	___fsmul_STK05
	LDA	r0x1167
	STA	___fsmul_STK04
	LDA	r0x1168
	STA	___fsmul_STK03
	LDA	#0x3d
	STA	___fsmul_STK02
	LDA	#0x7e
	STA	___fsmul_STK01
	LDA	#0x0d
	STA	___fsmul_STK00
	LDA	#0xbf
	CALL	___fsmul
	STA	r0x116C
	LDA	STK00
	STA	r0x116B
	LDA	STK01
	STA	r0x116A
	LDA	STK02
	STA	r0x1169
	LDA	#0x3a
	STA	___fsadd_STK06
	LDA	#0x3f
	STA	___fsadd_STK05
	LDA	#0xd4
	STA	___fsadd_STK04
	LDA	#0xc0
	STA	___fsadd_STK03
	LDA	r0x1165
	STA	___fsadd_STK02
	LDA	r0x1166
	STA	___fsadd_STK01
	LDA	r0x1167
	STA	___fsadd_STK00
	LDA	r0x1168
	CALL	___fsadd
	STA	r0x1168
	LDA	STK02
	STA	___fsdiv_STK06
	LDA	STK01
	STA	___fsdiv_STK05
	LDA	STK00
	STA	___fsdiv_STK04
	LDA	r0x1168
	STA	___fsdiv_STK03
	LDA	r0x1169
	STA	___fsdiv_STK02
	LDA	r0x116A
	STA	___fsdiv_STK01
	LDA	r0x116B
	STA	___fsdiv_STK00
	LDA	r0x116C
	CALL	___fsdiv
	STA	r0x1168
	LDA	STK02
	STA	___fsmul_STK06
	LDA	STK01
	STA	___fsmul_STK05
	LDA	STK00
	STA	___fsmul_STK04
	LDA	r0x1168
	STA	___fsmul_STK03
	LDA	_logf_STK02
	STA	___fsmul_STK02
	LDA	_logf_STK01
	STA	___fsmul_STK01
	LDA	_logf_STK00
	STA	___fsmul_STK00
	LDA	r0x1164
	CALL	___fsmul
	STA	r0x1168
	LDA	STK02
	STA	___fsadd_STK06
	LDA	STK01
	STA	___fsadd_STK05
	LDA	STK00
	STA	___fsadd_STK04
	LDA	r0x1168
	STA	___fsadd_STK03
	LDA	_logf_STK02
	STA	___fsadd_STK02
	LDA	_logf_STK01
	STA	___fsadd_STK01
	LDA	_logf_STK00
	STA	___fsadd_STK00
	LDA	r0x1164
	CALL	___fsadd
	STA	r0x1164
	LDA	STK00
	STA	_logf_STK00
	LDA	STK01
	STA	_logf_STK01
	LDA	STK02
	STA	_logf_STK02
;	;.line	243; "logf.c"	xn=n;
	LDA	_logf_n_65536_26
	STA	___sint2fs_STK00
	LDA	(_logf_n_65536_26 + 1)
	CALL	___sint2fs
	STA	r0x1168
	LDA	STK00
	STA	r0x1167
	LDA	STK01
	STA	r0x1166
	LDA	STK02
	STA	r0x1165
;	;.line	244; "logf.c"	return ((xn*C2+Rz)+xn*C1);
	STA	___fsmul_STK06
	LDA	r0x1166
	STA	___fsmul_STK05
	LDA	r0x1167
	STA	___fsmul_STK04
	LDA	r0x1168
	STA	___fsmul_STK03
	LDA	#0x83
	STA	___fsmul_STK02
	LDA	#0x80
	STA	___fsmul_STK01
	LDA	#0x5e
	STA	___fsmul_STK00
	LDA	#0xb9
	CALL	___fsmul
	STA	r0x116C
	LDA	_logf_STK02
	STA	___fsadd_STK06
	LDA	_logf_STK01
	STA	___fsadd_STK05
	LDA	_logf_STK00
	STA	___fsadd_STK04
	LDA	r0x1164
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x116C
	CALL	___fsadd
	STA	r0x1164
	LDA	STK00
	STA	_logf_STK00
	LDA	STK01
	STA	_logf_STK01
	LDA	STK02
	STA	_logf_STK02
	LDA	r0x1165
	STA	___fsmul_STK06
	LDA	r0x1166
	STA	___fsmul_STK05
	LDA	r0x1167
	STA	___fsmul_STK04
	LDA	r0x1168
	STA	___fsmul_STK03
	CLRA	
	STA	___fsmul_STK02
	LDA	#0x80
	STA	___fsmul_STK01
	LDA	#0x31
	STA	___fsmul_STK00
	LDA	#0x3f
	CALL	___fsmul
	STA	r0x1168
	LDA	STK02
	STA	___fsadd_STK06
	LDA	STK01
	STA	___fsadd_STK05
	LDA	STK00
	STA	___fsadd_STK04
	LDA	r0x1168
	STA	___fsadd_STK03
	LDA	_logf_STK02
	STA	___fsadd_STK02
	LDA	_logf_STK01
	STA	___fsadd_STK01
	LDA	_logf_STK00
	STA	___fsadd_STK00
	LDA	r0x1164
	CALL	___fsadd
_00110_DS_:
;	;.line	245; "logf.c"	}
	RET	
; exit point of _logf
	.ENDFUNC _logf
	;--cdb--S:Llogf.logf$x$65536$25({4}SF:S),R,0,0,[_logf_STK02,_logf_STK01,_logf_STK00,r0x1164]
	;--cdb--S:Llogf.logf$Rz$65536$26({4}SF:S),R,0,0,[r0x1161,r0x1162,r0x1163,r0x1164]
	;--cdb--S:Llogf.logf$f$65536$26({4}SF:S),R,0,0,[r0x1161,r0x1162,r0x1163,r0x1164]
	;--cdb--S:Llogf.logf$z$65536$26({4}SF:S),R,0,0,[r0x1161,r0x1162,r0x1163,r0x1164]
	;--cdb--S:Llogf.logf$w$65536$26({4}SF:S),R,0,0,[r0x1165,r0x1166,r0x1167,r0x1168]
	;--cdb--S:Llogf.logf$znum$65536$26({4}SF:S),R,0,0,[r0x1165,r0x1166,r0x1167,r0x1168]
	;--cdb--S:Llogf.logf$zden$65536$26({4}SF:S),R,0,0,[r0x1161,r0x1162,r0x1163,r0x1164]
	;--cdb--S:Llogf.logf$xn$65536$26({4}SF:S),R,0,0,[r0x1165,r0x1166,r0x1167,r0x1168]
	;--cdb--S:Llogf.logf$n$65536$26({2}SI:S),B,1,1
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,2
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$errno$0$0({2}SI:S),E,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,2
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_frexpf
	.globl	___fslt
	.globl	___fssub
	.globl	___fsmul
	.globl	___fsadd
	.globl	___fsdiv
	.globl	___sint2fs
	.globl	_errno

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_logf
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_logf_0	udata
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116C:	.ds	1
_logf_n_65536_26:	.ds	2
	.area DSEG (DATA); (local stack unassigned) 
_logf_STK00:	.ds	1
	.globl _logf_STK00
_logf_STK01:	.ds	1
	.globl _logf_STK01
_logf_STK02:	.ds	1
	.globl _logf_STK02
	.globl ___fslt_STK06
	.globl ___fslt_STK05
	.globl ___fslt_STK04
	.globl ___fslt_STK03
	.globl ___fslt_STK02
	.globl ___fslt_STK01
	.globl ___fslt_STK00
	.globl _frexpf_STK04
	.globl _frexpf_STK03
	.globl _frexpf_STK02
	.globl _frexpf_STK01
	.globl _frexpf_STK00
	.globl ___fssub_STK06
	.globl ___fssub_STK05
	.globl ___fssub_STK04
	.globl ___fssub_STK03
	.globl ___fssub_STK02
	.globl ___fssub_STK01
	.globl ___fssub_STK00
	.globl ___fsmul_STK06
	.globl ___fsmul_STK05
	.globl ___fsmul_STK04
	.globl ___fsmul_STK03
	.globl ___fsmul_STK02
	.globl ___fsmul_STK01
	.globl ___fsmul_STK00
	.globl ___fsadd_STK06
	.globl ___fsadd_STK05
	.globl ___fsadd_STK04
	.globl ___fsadd_STK03
	.globl ___fsadd_STK02
	.globl ___fsadd_STK01
	.globl ___fsadd_STK00
	.globl ___fsdiv_STK06
	.globl ___fsdiv_STK05
	.globl ___fsdiv_STK04
	.globl ___fsdiv_STK03
	.globl ___fsdiv_STK02
	.globl ___fsdiv_STK01
	.globl ___fsdiv_STK00
	.globl ___sint2fs_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:_logf_STK00:NULL+0:14:0
	;--cdb--W:_logf_STK01:NULL+0:13:0
	;--cdb--W:_logf_STK02:NULL+0:12:0
	;--cdb--W:r0x1165:NULL+0:12:0
	;--cdb--W:r0x1166:NULL+0:13:0
	;--cdb--W:r0x1167:NULL+0:14:0
	;--cdb--W:r0x1169:NULL+0:12:0
	;--cdb--W:r0x116B:NULL+0:14:0
	;--cdb--W:r0x116A:NULL+0:13:0
	;--cdb--W:r0x1165:NULL+0:-1:1
	;--cdb--W:r0x1166:NULL+0:-1:1
	;--cdb--W:r0x1168:NULL+0:-1:1
	;--cdb--W:r0x1167:NULL+0:-1:1
	;--cdb--W:r0x1169:NULL+0:-1:1
	;--cdb--W:r0x1164:NULL+0:-1:1
	end
