;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"iscntrl.c"
	.module iscntrl
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$iscntrl$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isblank$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$islower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isupper$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; iscntrl-code 
.globl _iscntrl

;--------------------------------------------------------
	.FUNC _iscntrl:$PNUM 2:$L:r0x1160:$L:_iscntrl_STK00:$L:r0x1161
;--------------------------------------------------------
;	.line	33; "iscntrl.c"	int iscntrl (int c)
_iscntrl:	;Function start
	STA	r0x1160
;	;.line	35; "iscntrl.c"	return (c < ' ' || c == 0x7f);
	LDA	_iscntrl_STK00
	ADD	#0xe0
	LDA	r0x1160
	XOR	#0x80
	ADDC	#0x7f
	JNC	_00108_DS_
	LDA	r0x1160
	JNZ	_00118_DS_
	LDA	_iscntrl_STK00
	ADD	#0x81
_00118_DS_:
	JZ	_00108_DS_
	CLRA	
	STA	_iscntrl_STK00
	JMP	_00109_DS_
_00108_DS_:
	LDA	#0x01
	STA	_iscntrl_STK00
_00109_DS_:
	LDA	_iscntrl_STK00
	JPL	_00119_DS_
	LDA	#0xff
	JMP	_00120_DS_
_00119_DS_:
	CLRA	
_00120_DS_:
	STA	r0x1161
	LDA	_iscntrl_STK00
	STA	STK00
	LDA	r0x1161
;	;.line	36; "iscntrl.c"	}
	RET	
; exit point of _iscntrl
	.ENDFUNC _iscntrl
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:Liscntrl.isblank$c$65536$13({2}SI:S),E,0,0
	;--cdb--S:Liscntrl.isdigit$c$65536$15({2}SI:S),E,0,0
	;--cdb--S:Liscntrl.islower$c$65536$17({2}SI:S),E,0,0
	;--cdb--S:Liscntrl.isupper$c$65536$19({2}SI:S),E,0,0
	;--cdb--S:Liscntrl.iscntrl$c$65536$21({2}SI:S),R,0,0,[_iscntrl_STK00,r0x1160]
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_iscntrl
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_iscntrl_0	udata
r0x1160:	.ds	1
r0x1161:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_iscntrl_STK00:	.ds	1
	.globl _iscntrl_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1160:NULL+0:4450:0
	end
