;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"isxdigit.c"
	.module isxdigit
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isxdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isblank$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$islower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isupper$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; isxdigit-code 
.globl _isxdigit

;--------------------------------------------------------
	.FUNC _isxdigit:$PNUM 2:$L:r0x1160:$L:_isxdigit_STK00:$L:r0x1161
;--------------------------------------------------------
;	.line	33; "isxdigit.c"	int isxdigit (int c)
_isxdigit:	;Function start
	STA	r0x1160
;	;.line	35; "isxdigit.c"	return (c >= '0' && c <= '9' || c >= 'a' && c <= 'f' || c >= 'A' && c <= 'F');
	LDA	_isxdigit_STK00
	ADD	#0xd0
	LDA	r0x1160
	XOR	#0x80
	ADDC	#0x7f
	JNC	_00115_DS_
	SETB	_C
	LDA	#0x39
	SUBB	_isxdigit_STK00
	CLRA	
	SUBSI	
	SUBB	r0x1160
	JC	_00108_DS_
_00115_DS_:
	LDA	_isxdigit_STK00
	ADD	#0x9f
	LDA	r0x1160
	XOR	#0x80
	ADDC	#0x7f
	JNC	_00112_DS_
	SETB	_C
	LDA	#0x66
	SUBB	_isxdigit_STK00
	CLRA	
	SUBSI	
	SUBB	r0x1160
	JC	_00108_DS_
_00112_DS_:
	LDA	_isxdigit_STK00
	ADD	#0xbf
	LDA	r0x1160
	XOR	#0x80
	ADDC	#0x7f
	JNC	_00107_DS_
	SETB	_C
	LDA	#0x46
	SUBB	_isxdigit_STK00
	CLRA	
	SUBSI	
	SUBB	r0x1160
	JC	_00108_DS_
_00107_DS_:
	CLRA	
	STA	_isxdigit_STK00
	JMP	_00109_DS_
_00108_DS_:
	LDA	#0x01
	STA	_isxdigit_STK00
_00109_DS_:
	LDA	_isxdigit_STK00
	JPL	_00137_DS_
	LDA	#0xff
	JMP	_00138_DS_
_00137_DS_:
	CLRA	
_00138_DS_:
	STA	r0x1161
	LDA	_isxdigit_STK00
	STA	STK00
	LDA	r0x1161
;	;.line	36; "isxdigit.c"	}
	RET	
; exit point of _isxdigit
	.ENDFUNC _isxdigit
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:Lisxdigit.isblank$c$65536$13({2}SI:S),E,0,0
	;--cdb--S:Lisxdigit.isdigit$c$65536$15({2}SI:S),E,0,0
	;--cdb--S:Lisxdigit.islower$c$65536$17({2}SI:S),E,0,0
	;--cdb--S:Lisxdigit.isupper$c$65536$19({2}SI:S),E,0,0
	;--cdb--S:Lisxdigit.isxdigit$c$65536$21({2}SI:S),R,0,0,[_isxdigit_STK00,r0x1160]
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_isxdigit
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_isxdigit_0	udata
r0x1160:	.ds	1
r0x1161:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_isxdigit_STK00:	.ds	1
	.globl _isxdigit_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1160:NULL+0:4450:0
	end
