;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"printf.c"
	.module printf
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$putcp$0$0({2}DF,SV:S),C,0,-4
	;--cdb--F:G$putcp$0$0({2}DF,SV:S),C,0,-4,0,0,0
	;--cdb--S:G$tfp_format$0$0({2}DF,SV:S),C,0,-2
	;--cdb--F:G$tfp_format$0$0({2}DF,SV:S),C,0,-2,0,0,0
	;--cdb--S:G$putchar$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$putchar$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$sprintf$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$sprintf$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:Fprintf$putch$0$0({2}DF,SV:S),C,0,-2
	;--cdb--F:Fprintf$putch$0$0({2}DF,SV:S),C,0,-2,0,0,0
	;--cdb--S:Fprintf$uli2a$0$0({2}DF,SV:S),C,0,-2
	;--cdb--F:Fprintf$uli2a$0$0({2}DF,SV:S),C,0,-2,0,0,0
	;--cdb--S:G$_divulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$_modulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:Fprintf$li2a$0$0({2}DF,SV:S),C,0,-2
	;--cdb--F:Fprintf$li2a$0$0({2}DF,SV:S),C,0,-2,0,0,0
	;--cdb--S:Fprintf$ui2a$0$0({2}DF,SV:S),C,0,-2
	;--cdb--F:Fprintf$ui2a$0$0({2}DF,SV:S),C,0,-2,0,0,0
	;--cdb--S:G$_divuint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_mulint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_moduint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:Fprintf$i2a$0$0({2}DF,SV:S),C,0,-2
	;--cdb--F:Fprintf$i2a$0$0({2}DF,SV:S),C,0,-2,0,0,0
	;--cdb--S:Fprintf$a2d$0$0({2}DF,SI:S),C,0,-2
	;--cdb--F:Fprintf$a2d$0$0({2}DF,SI:S),C,0,-2,0,0,0
	;--cdb--S:Fprintf$a2i$0$0({2}DF,SC:U),C,0,-2
	;--cdb--F:Fprintf$a2i$0$0({2}DF,SC:U),C,0,-2,0,0,0
	;--cdb--S:Fprintf$putchw$0$0({2}DF,SV:S),C,0,-2
	;--cdb--F:Fprintf$putchw$0$0({2}DF,SV:S),C,0,-2,0,0,0
	;--cdb--S:G$printf$0$0({2}DF,SV:S),C,0,-2
	;--cdb--F:G$printf$0$0({2}DF,SV:S),C,0,-2,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; printf-code 
.globl _putcp

;--------------------------------------------------------
	.FUNC _putcp:$PNUM 3:$L:r0x122A:$L:_putcp_STK00:$L:_putcp_STK01:$L:r0x122D:$L:r0x122C\
:$L:r0x122E:$L:r0x122F
;--------------------------------------------------------
;	.line	229; "printf.c"	*(*((char**)p))++ = c;
_putcp:	;Function start
	STA	r0x122D
	LDA	_putcp_STK00
	STA	r0x122C
	STA	_ROMPL
	LDA	r0x122D
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_putcp_STK00
	LDA	@_ROMPINC
	STA	r0x122A
	LDA	_putcp_STK00
	INCA	
	STA	r0x122E
	CLRA	
	ADDC	r0x122A
	STA	r0x122F
	LDA	r0x122D
	STA	_ROMPH
	LDA	r0x122C
	STA	_ROMPL
	LDA	r0x122E
	STA	@_ROMPINC
	LDA	r0x122F
	STA	@_ROMPINC
	LDA	r0x122A
	STA	_ROMPH
	LDA	_putcp_STK00
	STA	_ROMPL
	LDA	_putcp_STK01
	STA	@_ROMPINC
;	;.line	230; "printf.c"	}
	RET	
; exit point of _putcp
	.ENDFUNC _putcp
.globl _printf

;--------------------------------------------------------
	.FUNC_PSTK _printf:$C:_tfp_format\
:$L:r0x1222:$L:r0x1225
;--------------------------------------------------------
;	.line	220; "printf.c"	if(stdout_putf==NULL)
_printf:	;Function start
	LDA	_stdout_putf
	ORA	(_stdout_putf + 1)
	JNZ	_00541_DS_
;	;.line	221; "printf.c"	stdout_putf=putch;
	LDA	#low (_putch + 0)
	STA	_stdout_putf
	LDA	#high (_putch + 0)
	STA	(_stdout_putf + 1)
_00541_DS_:
;	;.line	222; "printf.c"	va_start(va,fmt);
	LDA	#0xfc
	ADD	_RAMP1L
	STA	r0x1222
	LDA	#0xff
	ADDC	_RAMP1H
	STA	r0x1225
	LDA	r0x1222
;	;.line	223; "printf.c"	tfp_format(stdout_putp,stdout_putf,fmt,va);
	STA	_tfp_format_STK06
	LDA	r0x1225
	STA	_tfp_format_STK05
	LDA	@P1,-4
	STA	_tfp_format_STK04
	LDA	@P1,-3
	STA	_tfp_format_STK03
	LDA	_stdout_putf
	STA	_tfp_format_STK02
	LDA	(_stdout_putf + 1)
	STA	_tfp_format_STK01
	LDA	_stdout_putp
	STA	_tfp_format_STK00
	LDA	(_stdout_putp + 1)
	CALL	_tfp_format
;	;.line	225; "printf.c"	}
	RET	
; exit point of _printf
	.ENDFUNC _printf
.globl _tfp_format

;--------------------------------------------------------
	.FUNC _tfp_format:$PNUM 8:$C:_a2i:$C:_uli2a:$C:_ui2a:$C:_putchw\
:$C:_li2a:$C:_i2a\
:$L:r0x1210:$L:_tfp_format_STK00:$L:_tfp_format_STK01:$L:_tfp_format_STK02:$L:_tfp_format_STK03\
:$L:_tfp_format_STK04:$L:_tfp_format_STK05:$L:r0x1214:$L:_tfp_format_STK06:$L:r0x1213\
:$L:r0x1215:$L:r0x1216:$L:r0x1217:$L:r0x1218:$L:r0x1219\
:$L:_tfp_format_fmt_65536_32:$L:_tfp_format_w_196608_35:$L:r0x121A:$L:r0x121C:$L:r0x121D\
:$L:r0x121E:$L:_tfp_format_bf_65536_33
;--------------------------------------------------------
;	.line	131; "printf.c"	void tfp_format(void* putp,putcf putf,char *fmt, va_list va)
_tfp_format:	;Function start
	STA	r0x1210
	LDA	_tfp_format_STK03
	STA	(_tfp_format_fmt_65536_32 + 1)
	LDA	_tfp_format_STK04
	STA	_tfp_format_fmt_65536_32
	LDA	_tfp_format_STK05
	STA	r0x1214
	LDA	_tfp_format_STK06
	STA	r0x1213
_00385_DS_:
;	;.line	138; "printf.c"	while ((ch=*(fmt++))) {
	LDA	_tfp_format_fmt_65536_32
	STA	r0x1215
	LDA	(_tfp_format_fmt_65536_32 + 1)
	STA	r0x1216
	LDA	r0x1215
	STA	_ROMPL
	LDA	r0x1216
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1217
	LDA	r0x1215
	INCA	
	STA	_tfp_format_fmt_65536_32
	CLRA	
	ADDC	r0x1216
	STA	(_tfp_format_fmt_65536_32 + 1)
	LDA	r0x1217
	STA	r0x1215
	LDA	r0x1217
	JZ	_00389_DS_
;	;.line	139; "printf.c"	if (ch!='%') 
	LDA	r0x1215
	ADD	#0xdb
	JZ	_00383_DS_
;	;.line	140; "printf.c"	putf(putp,ch);
	CALL	_00483_DS_
	JMP	_00484_DS_
_00483_DS_:
	CALL	_00485_DS_
_00485_DS_:
	LDA	_tfp_format_STK01
	STA	_STACKH
	LDA	_tfp_format_STK02
	STA	_STACKL
	LDA	r0x1215
	STA	@_RAMP0INC
	LDA	_tfp_format_STK00
	STA	@_RAMP0INC
	LDA	r0x1210
	RET	
_00484_DS_:
	JMP	_00385_DS_
_00383_DS_:
;	;.line	142; "printf.c"	char lz=0;
	CLRA	
;	;.line	144; "printf.c"	char lng=0;
	STA	r0x1215
;	;.line	146; "printf.c"	int w=0;
	STA	r0x1216
	STA	_tfp_format_w_196608_35
	STA	(_tfp_format_w_196608_35 + 1)
;	;.line	147; "printf.c"	ch=*(fmt++);
	LDA	_tfp_format_fmt_65536_32
	STA	r0x1217
	LDA	(_tfp_format_fmt_65536_32 + 1)
	STA	r0x1218
	LDA	r0x1217
	STA	_ROMPL
	LDA	r0x1218
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1219
	LDA	r0x1217
	INCA	
	STA	_tfp_format_fmt_65536_32
	CLRA	
	ADDC	r0x1218
	STA	(_tfp_format_fmt_65536_32 + 1)
;	;.line	148; "printf.c"	if (ch=='0') {
	LDA	r0x1219
	XOR	#0x30
	JNZ	_00357_DS_
;	;.line	149; "printf.c"	ch=*(fmt++);
	LDA	_tfp_format_fmt_65536_32
	STA	r0x1217
	LDA	(_tfp_format_fmt_65536_32 + 1)
	STA	r0x1218
	LDA	r0x1217
	STA	_ROMPL
	LDA	r0x1218
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1219
	LDA	r0x1217
	INCA	
	STA	_tfp_format_fmt_65536_32
	CLRA	
	ADDC	r0x1218
	STA	(_tfp_format_fmt_65536_32 + 1)
;	;.line	150; "printf.c"	lz=1;
	LDA	#0x01
	STA	r0x1215
_00357_DS_:
;	;.line	152; "printf.c"	if (ch>='0' && ch<='9') {
	LDA	r0x1219
	ADD	#0xd0
	JNC	_00359_DS_
	SETB	_C
	LDA	#0x39
	SUBB	r0x1219
	JNC	_00359_DS_
;	;.line	153; "printf.c"	ch=a2i(ch,&fmt,10,&w);
	LDA	#(_tfp_format_w_196608_35 + 0)
	STA	_a2i_STK05
	LDA	#high (_tfp_format_w_196608_35 + 0)
	STA	_a2i_STK04
	LDA	#0x0a
	STA	_a2i_STK03
	CLRA	
	STA	_a2i_STK02
	LDA	#(_tfp_format_fmt_65536_32 + 0)
	STA	_a2i_STK01
	LDA	#high (_tfp_format_fmt_65536_32 + 0)
	STA	_a2i_STK00
	LDA	r0x1219
	CALL	_a2i
	STA	r0x1219
_00359_DS_:
;	;.line	156; "printf.c"	if (ch=='l') {
	LDA	r0x1219
	XOR	#0x6c
	JNZ	_00362_DS_
;	;.line	157; "printf.c"	ch=*(fmt++);
	LDA	_tfp_format_fmt_65536_32
	STA	r0x1217
	LDA	(_tfp_format_fmt_65536_32 + 1)
	STA	r0x1218
	LDA	r0x1217
	STA	_ROMPL
	LDA	r0x1218
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1219
	LDA	r0x1217
	INCA	
	STA	_tfp_format_fmt_65536_32
	CLRA	
	ADDC	r0x1218
	STA	(_tfp_format_fmt_65536_32 + 1)
;	;.line	158; "printf.c"	lng=1;
	LDA	#0x01
	STA	r0x1216
_00362_DS_:
;	;.line	161; "printf.c"	switch (ch) {
	LDA	r0x1219
	JZ	_00389_DS_
	XOR	#0x25
	JZ	_00379_DS_
	LDA	r0x1219
	XOR	#0x58
	LDC	_Z
	CLRA	
	ROL	
	STA	r0x1217
	JNZ	_00373_DS_
	LDA	r0x1219
	ADD	#0x9d
	JZ	_00377_DS_
	DECA	
	JZ	_00368_DS_
	ADD	#0xf1
	JZ	_00378_DS_
	ADD	#0xfe
	JZ	_00364_DS_
	ADD	#0xfd
	JZ	_00373_DS_
	JMP	_00385_DS_
_00364_DS_:
;	;.line	166; "printf.c"	if (lng)
	LDA	r0x1216
	JZ	_00366_DS_
;	;.line	167; "printf.c"	uli2a(va_arg(va, unsigned long int),10,0,bf);
	LDA	#0xfc
	ADD	r0x1213
	STA	r0x1218
	LDA	#0xff
	ADDC	r0x1214
	STA	r0x121A
	LDA	r0x1218
	STA	r0x1213
	LDA	r0x121A
	STA	r0x1214
	LDA	r0x1218
	STA	_ROMPL
	LDA	r0x121A
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1218
	LDA	@_ROMPINC
	STA	r0x121A
	LDA	@_ROMPINC
	STA	r0x121D
	LDA	@_ROMPINC
	STA	r0x121E
	LDA	#(_tfp_format_bf_65536_33 + 0)
	STA	_uli2a_STK08
	LDA	#high (_tfp_format_bf_65536_33 + 0)
	STA	_uli2a_STK07
	CLRA	
	STA	_uli2a_STK06
	STA	_uli2a_STK05
	LDA	#0x0a
	STA	_uli2a_STK04
	CLRA	
	STA	_uli2a_STK03
	LDA	r0x1218
	STA	_uli2a_STK02
	LDA	r0x121A
	STA	_uli2a_STK01
	LDA	r0x121D
	STA	_uli2a_STK00
	LDA	r0x121E
	CALL	_uli2a
	JMP	_00367_DS_
_00366_DS_:
;	;.line	170; "printf.c"	ui2a(va_arg(va, unsigned int),10,0,bf);
	LDA	#0xfe
	ADD	r0x1213
	STA	r0x1218
	LDA	#0xff
	ADDC	r0x1214
	STA	r0x121A
	LDA	r0x1218
	STA	r0x1213
	LDA	r0x121A
	STA	r0x1214
	LDA	r0x1218
	STA	_ROMPL
	LDA	r0x121A
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1218
	LDA	@_ROMPINC
	STA	r0x121A
	LDA	#(_tfp_format_bf_65536_33 + 0)
	STA	_ui2a_STK06
	LDA	#high (_tfp_format_bf_65536_33 + 0)
	STA	_ui2a_STK05
	CLRA	
	STA	_ui2a_STK04
	STA	_ui2a_STK03
	LDA	#0x0a
	STA	_ui2a_STK02
	CLRA	
	STA	_ui2a_STK01
	LDA	r0x1218
	STA	_ui2a_STK00
	LDA	r0x121A
	CALL	_ui2a
_00367_DS_:
;	;.line	171; "printf.c"	putchw(putp,putf,w,lz,bf);
	LDA	#(_tfp_format_bf_65536_33 + 0)
	STA	_putchw_STK07
	LDA	#high (_tfp_format_bf_65536_33 + 0)
	STA	_putchw_STK06
	LDA	r0x1215
	STA	_putchw_STK05
	LDA	_tfp_format_w_196608_35
	STA	_putchw_STK04
	LDA	(_tfp_format_w_196608_35 + 1)
	STA	_putchw_STK03
	LDA	_tfp_format_STK02
	STA	_putchw_STK02
	LDA	_tfp_format_STK01
	STA	_putchw_STK01
	LDA	_tfp_format_STK00
	STA	_putchw_STK00
	LDA	r0x1210
	CALL	_putchw
;	;.line	172; "printf.c"	break;
	JMP	_00385_DS_
_00368_DS_:
;	;.line	176; "printf.c"	if (lng)
	LDA	r0x1216
	JZ	_00370_DS_
;	;.line	177; "printf.c"	li2a(va_arg(va, unsigned long int),bf);
	LDA	#0xfc
	ADD	r0x1213
	STA	r0x1218
	LDA	#0xff
	ADDC	r0x1214
	STA	r0x121A
	LDA	r0x1218
	STA	r0x1213
	LDA	r0x121A
	STA	r0x1214
	LDA	r0x1218
	STA	_ROMPL
	LDA	r0x121A
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1218
	LDA	@_ROMPINC
	STA	r0x121A
	LDA	@_ROMPINC
	STA	r0x121D
	LDA	@_ROMPINC
	STA	r0x121E
	LDA	#(_tfp_format_bf_65536_33 + 0)
	STA	_li2a_STK04
	LDA	#high (_tfp_format_bf_65536_33 + 0)
	STA	_li2a_STK03
	LDA	r0x1218
	STA	_li2a_STK02
	LDA	r0x121A
	STA	_li2a_STK01
	LDA	r0x121D
	STA	_li2a_STK00
	LDA	r0x121E
	CALL	_li2a
	JMP	_00371_DS_
_00370_DS_:
;	;.line	180; "printf.c"	i2a(va_arg(va, int),bf);
	LDA	#0xfe
	ADD	r0x1213
	STA	r0x1218
	LDA	#0xff
	ADDC	r0x1214
	STA	r0x121A
	LDA	r0x1218
	STA	r0x1213
	LDA	r0x121A
	STA	r0x1214
	LDA	r0x1218
	STA	_ROMPL
	LDA	r0x121A
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1218
	LDA	@_ROMPINC
	STA	r0x121A
	LDA	#(_tfp_format_bf_65536_33 + 0)
	STA	_i2a_STK02
	LDA	#high (_tfp_format_bf_65536_33 + 0)
	STA	_i2a_STK01
	LDA	r0x1218
	STA	_i2a_STK00
	LDA	r0x121A
	CALL	_i2a
_00371_DS_:
;	;.line	181; "printf.c"	putchw(putp,putf,w,lz,bf);
	LDA	#(_tfp_format_bf_65536_33 + 0)
	STA	_putchw_STK07
	LDA	#high (_tfp_format_bf_65536_33 + 0)
	STA	_putchw_STK06
	LDA	r0x1215
	STA	_putchw_STK05
	LDA	_tfp_format_w_196608_35
	STA	_putchw_STK04
	LDA	(_tfp_format_w_196608_35 + 1)
	STA	_putchw_STK03
	LDA	_tfp_format_STK02
	STA	_putchw_STK02
	LDA	_tfp_format_STK01
	STA	_putchw_STK01
	LDA	_tfp_format_STK00
	STA	_putchw_STK00
	LDA	r0x1210
	CALL	_putchw
;	;.line	182; "printf.c"	break;
	JMP	_00385_DS_
_00373_DS_:
;	;.line	186; "printf.c"	if (lng)
	LDA	r0x1216
	JZ	_00375_DS_
;	;.line	187; "printf.c"	uli2a(va_arg(va, unsigned long int),16,(ch=='X'),bf);
	LDA	#0xfc
	ADD	r0x1213
	STA	r0x1216
	LDA	#0xff
	ADDC	r0x1214
	STA	r0x1218
	LDA	r0x1216
	STA	r0x1213
	LDA	r0x1218
	STA	r0x1214
	LDA	r0x1216
	STA	_ROMPL
	LDA	r0x1218
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1216
	LDA	@_ROMPINC
	STA	r0x1218
	LDA	@_ROMPINC
	STA	r0x121C
	LDA	@_ROMPINC
	STA	r0x121D
	LDA	#(_tfp_format_bf_65536_33 + 0)
	STA	_uli2a_STK08
	LDA	#high (_tfp_format_bf_65536_33 + 0)
	STA	_uli2a_STK07
	LDA	r0x1217
	STA	_uli2a_STK06
	CLRA	
	STA	_uli2a_STK05
	LDA	#0x10
	STA	_uli2a_STK04
	CLRA	
	STA	_uli2a_STK03
	LDA	r0x1216
	STA	_uli2a_STK02
	LDA	r0x1218
	STA	_uli2a_STK01
	LDA	r0x121C
	STA	_uli2a_STK00
	LDA	r0x121D
	CALL	_uli2a
	JMP	_00376_DS_
_00375_DS_:
;	;.line	190; "printf.c"	ui2a(va_arg(va, unsigned int),16,(ch=='X'),bf);
	LDA	#0xfe
	ADD	r0x1213
	STA	r0x1216
	LDA	#0xff
	ADDC	r0x1214
	STA	r0x1218
	LDA	r0x1216
	STA	r0x1213
	LDA	r0x1218
	STA	r0x1214
	LDA	r0x1216
	STA	_ROMPL
	LDA	r0x1218
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1216
	LDA	@_ROMPINC
	STA	r0x1218
	LDA	#(_tfp_format_bf_65536_33 + 0)
	STA	_ui2a_STK06
	LDA	#high (_tfp_format_bf_65536_33 + 0)
	STA	_ui2a_STK05
	LDA	r0x1217
	STA	_ui2a_STK04
	CLRA	
	STA	_ui2a_STK03
	LDA	#0x10
	STA	_ui2a_STK02
	CLRA	
	STA	_ui2a_STK01
	LDA	r0x1216
	STA	_ui2a_STK00
	LDA	r0x1218
	CALL	_ui2a
_00376_DS_:
;	;.line	191; "printf.c"	putchw(putp,putf,w,lz,bf);
	LDA	#(_tfp_format_bf_65536_33 + 0)
	STA	_putchw_STK07
	LDA	#high (_tfp_format_bf_65536_33 + 0)
	STA	_putchw_STK06
	LDA	r0x1215
	STA	_putchw_STK05
	LDA	_tfp_format_w_196608_35
	STA	_putchw_STK04
	LDA	(_tfp_format_w_196608_35 + 1)
	STA	_putchw_STK03
	LDA	_tfp_format_STK02
	STA	_putchw_STK02
	LDA	_tfp_format_STK01
	STA	_putchw_STK01
	LDA	_tfp_format_STK00
	STA	_putchw_STK00
	LDA	r0x1210
	CALL	_putchw
;	;.line	192; "printf.c"	break;
	JMP	_00385_DS_
_00377_DS_:
;	;.line	194; "printf.c"	putf(putp,(char)(va_arg(va, int)));
	LDA	#0xfe
	ADD	r0x1213
	STA	r0x1215
	LDA	#0xff
	ADDC	r0x1214
	STA	r0x1216
	LDA	r0x1215
	STA	r0x1213
	LDA	r0x1216
	STA	r0x1214
	LDA	r0x1215
	STA	_ROMPL
	LDA	r0x1216
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1215
	CALL	_00526_DS_
	JMP	_00527_DS_
_00526_DS_:
	CALL	_00528_DS_
_00528_DS_:
	LDA	_tfp_format_STK01
	STA	_STACKH
	LDA	_tfp_format_STK02
	STA	_STACKL
	LDA	r0x1215
	STA	@_RAMP0INC
	LDA	_tfp_format_STK00
	STA	@_RAMP0INC
	LDA	r0x1210
	RET	
_00527_DS_:
;	;.line	195; "printf.c"	break;
	JMP	_00385_DS_
_00378_DS_:
;	;.line	197; "printf.c"	putchw(putp,putf,w,0,va_arg(va, char*));
	LDA	#0xfe
	ADD	r0x1213
	STA	r0x1215
	LDA	#0xff
	ADDC	r0x1214
	STA	r0x1217
	LDA	r0x1215
	STA	r0x1213
	LDA	r0x1217
	STA	r0x1214
	LDA	r0x1215
	STA	_ROMPL
	LDA	r0x1217
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1215
	LDA	@_ROMPINC
	STA	r0x1217
	LDA	r0x1215
	STA	_putchw_STK07
	LDA	r0x1217
	STA	_putchw_STK06
	CLRA	
	STA	_putchw_STK05
	LDA	_tfp_format_w_196608_35
	STA	_putchw_STK04
	LDA	(_tfp_format_w_196608_35 + 1)
	STA	_putchw_STK03
	LDA	_tfp_format_STK02
	STA	_putchw_STK02
	LDA	_tfp_format_STK01
	STA	_putchw_STK01
	LDA	_tfp_format_STK00
	STA	_putchw_STK00
	LDA	r0x1210
	CALL	_putchw
;	;.line	198; "printf.c"	break;
	JMP	_00385_DS_
_00379_DS_:
;	;.line	200; "printf.c"	putf(putp,ch);
	CALL	_00533_DS_
	JMP	_00534_DS_
_00533_DS_:
	CALL	_00535_DS_
_00535_DS_:
	LDA	_tfp_format_STK01
	STA	_STACKH
	LDA	_tfp_format_STK02
	STA	_STACKL
	LDA	r0x1219
	STA	@_RAMP0INC
	LDA	_tfp_format_STK00
	STA	@_RAMP0INC
	LDA	r0x1210
	RET	
_00534_DS_:
;	;.line	203; "printf.c"	}
	JMP	_00385_DS_
_00389_DS_:
;	;.line	207; "printf.c"	}
	RET	
; exit point of _tfp_format
	.ENDFUNC _tfp_format

;--------------------------------------------------------
	.FUNC _putchw:$PNUM 9:$L:r0x11FA:$L:_putchw_STK00:$L:_putchw_STK01:$L:_putchw_STK02:$L:_putchw_STK03\
:$L:_putchw_STK04:$L:_putchw_STK05:$L:_putchw_STK06:$L:_putchw_STK07:$L:r0x1202\
:$L:r0x1203:$L:r0x1204
;--------------------------------------------------------
;	.line	118; "printf.c"	static void putchw(void* putp,putcf putf,int n, char z, char* bf)
_putchw:	;Function start
	STA	r0x11FA
;	;.line	120; "printf.c"	char fc=z? '0' : ' ';
	LDA	_putchw_STK05
	JZ	_00299_DS_
	LDA	#0x30
	STA	_putchw_STK05
	CLRA	
	JMP	_00300_DS_
_00299_DS_:
	LDA	#0x20
	STA	_putchw_STK05
_00300_DS_:
	LDA	_putchw_STK05
	STA	r0x1203
;	;.line	123; "printf.c"	while (*p++ && n > 0)
	LDA	_putchw_STK07
	STA	_putchw_STK05
	LDA	_putchw_STK06
	STA	r0x1202
_00288_DS_:
	LDA	_putchw_STK05
	STA	_ROMPL
	LDA	r0x1202
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1204
	LDA	_putchw_STK05
	INCA	
	STA	_putchw_STK05
	CLRA	
	ADDC	r0x1202
	STA	r0x1202
	LDA	r0x1204
	JZ	_00291_DS_
	SETB	_C
	CLRA	
	SUBB	_putchw_STK04
	CLRA	
	SUBSI	
	SUBB	_putchw_STK03
	JC	_00291_DS_
;	;.line	124; "printf.c"	n--;
	LDA	_putchw_STK04
	DECA	
	STA	_putchw_STK04
	LDA	#0xff
	ADDC	_putchw_STK03
	STA	_putchw_STK03
	JMP	_00288_DS_
_00291_DS_:
;	;.line	125; "printf.c"	while (n-- > 0) 
	SETB	_C
	CLRA	
	SUBB	_putchw_STK04
	CLRA	
	SUBSI	
	SUBB	_putchw_STK03
	JC	_00308_DS_
	LDA	_putchw_STK04
	DECA	
	STA	_putchw_STK04
	LDA	#0xff
	ADDC	_putchw_STK03
	STA	_putchw_STK03
;	;.line	126; "printf.c"	putf(putp,fc);
	CALL	_00344_DS_
	JMP	_00345_DS_
_00344_DS_:
	CALL	_00346_DS_
_00346_DS_:
	LDA	_putchw_STK01
	STA	_STACKH
	LDA	_putchw_STK02
	STA	_STACKL
	LDA	r0x1203
	STA	@_RAMP0INC
	LDA	_putchw_STK00
	STA	@_RAMP0INC
	LDA	r0x11FA
	RET	
_00345_DS_:
	JMP	_00291_DS_
_00308_DS_:
;	;.line	127; "printf.c"	while ((ch= *bf++))
	LDA	_putchw_STK07
	STA	_putchw_STK04
	LDA	_putchw_STK06
	STA	_putchw_STK03
_00294_DS_:
	LDA	_putchw_STK04
	STA	_ROMPL
	LDA	_putchw_STK03
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_putchw_STK05
	LDA	_putchw_STK04
	INCA	
	STA	_putchw_STK04
	CLRA	
	ADDC	_putchw_STK03
	STA	_putchw_STK03
	LDA	_putchw_STK05
	STA	_putchw_STK07
	LDA	_putchw_STK05
	JZ	_00297_DS_
;	;.line	128; "printf.c"	putf(putp,ch);
	CALL	_00349_DS_
	JMP	_00350_DS_
_00349_DS_:
	CALL	_00351_DS_
_00351_DS_:
	LDA	_putchw_STK01
	STA	_STACKH
	LDA	_putchw_STK02
	STA	_STACKL
	LDA	_putchw_STK07
	STA	@_RAMP0INC
	LDA	_putchw_STK00
	STA	@_RAMP0INC
	LDA	r0x11FA
	RET	
_00350_DS_:
	JMP	_00294_DS_
_00297_DS_:
;	;.line	129; "printf.c"	}
	RET	
; exit point of _putchw
	.ENDFUNC _putchw

;--------------------------------------------------------
	.FUNC _a2i:$PNUM 7:$C:_a2d:$C:__mulint\
:$L:r0x11E4:$L:_a2i_STK00:$L:_a2i_STK01:$L:_a2i_STK02:$L:_a2i_STK03\
:$L:_a2i_STK04:$L:_a2i_STK05:$L:r0x11EB:$L:r0x11EC:$L:r0x11ED\
:$L:r0x11EE:$L:r0x11F0:$L:r0x11F1:$L:r0x11F2
;--------------------------------------------------------
;	.line	103; "printf.c"	static char a2i(char ch, char** src,int base,int* nump)
_a2i:	;Function start
	STA	r0x11E4
;	;.line	105; "printf.c"	char* p= *src;
	LDA	_a2i_STK01
	STA	_ROMPL
	LDA	_a2i_STK00
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x11EB
	LDA	@_ROMPINC
	STA	r0x11EC
;	;.line	106; "printf.c"	int num=0;
	CLRA	
	STA	r0x11ED
	STA	r0x11EE
_00280_DS_:
;	;.line	108; "printf.c"	while ((digit=a2d(ch))>=0) {
	LDA	r0x11E4
	CALL	_a2d
	STA	r0x11F0
	LDA	STK00
	STA	r0x11F1
	LDA	r0x11F0
	STA	r0x11F2
	LDA	r0x11F0
	JMI	_00282_DS_
;	;.line	109; "printf.c"	if (digit>base) break;
	SETB	_C
	LDA	_a2i_STK03
	SUBB	r0x11F1
	LDA	_a2i_STK02
	SUBSI	
	SUBB	r0x11F2
	JNC	_00282_DS_
;	;.line	110; "printf.c"	num=num*base+digit;
	LDA	_a2i_STK03
	STA	__mulint_STK02
	LDA	_a2i_STK02
	STA	__mulint_STK01
	LDA	r0x11ED
	STA	__mulint_STK00
	LDA	r0x11EE
	CALL	__mulint
	STA	r0x11F0
	LDA	STK00
	ADD	r0x11F1
	STA	r0x11ED
	LDA	r0x11F0
	ADDC	r0x11F2
	STA	r0x11EE
;	;.line	111; "printf.c"	ch=*p++;
	LDA	r0x11EB
	STA	_ROMPL
	LDA	r0x11EC
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x11E4
	LDA	r0x11EB
	INCA	
	STA	r0x11EB
	CLRA	
	ADDC	r0x11EC
	STA	r0x11EC
	JMP	_00280_DS_
_00282_DS_:
;	;.line	113; "printf.c"	*src=p;
	LDA	_a2i_STK00
	STA	_ROMPH
	LDA	_a2i_STK01
	STA	_ROMPL
	LDA	r0x11EB
	STA	@_ROMPINC
	LDA	r0x11EC
	STA	@_ROMPINC
;	;.line	114; "printf.c"	*nump=num;
	LDA	_a2i_STK04
	STA	_ROMPH
	LDA	_a2i_STK05
	STA	_ROMPL
	LDA	r0x11ED
	STA	@_ROMPINC
	LDA	r0x11EE
	STA	@_ROMPINC
;	;.line	115; "printf.c"	return ch;
	LDA	r0x11E4
;	;.line	116; "printf.c"	}
	RET	
; exit point of _a2i
	.ENDFUNC _a2i

;--------------------------------------------------------
	.FUNC _a2d:$PNUM 1:$L:r0x11E0:$L:r0x11E1:$L:r0x11E2:$L:r0x11E3
;--------------------------------------------------------
;	.line	92; "printf.c"	static int a2d(char ch)
_a2d:	;Function start
	STA	r0x11E0
;	;.line	94; "printf.c"	if (ch>='0' && ch<='9') 
	ADD	#0xd0
	JNC	_00270_DS_
	SETB	_C
	LDA	#0x39
	SUBB	r0x11E0
	JNC	_00270_DS_
;	;.line	95; "printf.c"	return ch-'0';
	LDA	#0xd0
	ADD	r0x11E0
	STA	r0x11E1
	CLRA	
	ADDC	#0xff
	STA	r0x11E2
	LDA	r0x11E1
	STA	STK00
	LDA	r0x11E2
	JMP	_00273_DS_
_00270_DS_:
;	;.line	96; "printf.c"	else if (ch>='a' && ch<='f')
	LDA	r0x11E0
	ADD	#0x9f
	JNC	_00266_DS_
	SETB	_C
	LDA	#0x66
	SUBB	r0x11E0
	JNC	_00266_DS_
;	;.line	97; "printf.c"	return ch-'a'+10;
	LDA	#0xa9
	ADD	r0x11E0
	STA	r0x11E1
	CLRA	
	ADDC	#0xff
	STA	r0x11E2
	LDA	r0x11E1
	STA	STK00
	LDA	r0x11E2
	JMP	_00273_DS_
_00266_DS_:
;	;.line	98; "printf.c"	else if (ch>='A' && ch<='F')
	LDA	r0x11E0
	ADD	#0xbf
	JNC	_00262_DS_
	SETB	_C
	LDA	#0x46
	SUBB	r0x11E0
	JNC	_00262_DS_
;	;.line	99; "printf.c"	return ch-'A'+10;
	LDA	#0xc9
	ADD	r0x11E0
	STA	r0x11E0
	CLRA	
	ADDC	#0xff
	STA	r0x11E3
	LDA	r0x11E0
	STA	STK00
	LDA	r0x11E3
	JMP	_00273_DS_
_00262_DS_:
;	;.line	100; "printf.c"	else return -1;
	LDA	#0xff
	STA	STK00
_00273_DS_:
;	;.line	101; "printf.c"	}
	RET	
; exit point of _a2d
	.ENDFUNC _a2d

;--------------------------------------------------------
	.FUNC _i2a:$PNUM 4:$C:_ui2a\
:$L:r0x11DA:$L:_i2a_STK00:$L:_i2a_STK01:$L:_i2a_STK02
;--------------------------------------------------------
;	.line	83; "printf.c"	static void i2a (int num, char * bf)
_i2a:	;Function start
	STA	r0x11DA
;	;.line	85; "printf.c"	if (num<0) {
	JPL	_00249_DS_
;	;.line	86; "printf.c"	num=-num;
	SETB	_C
	CLRA	
	SUBB	_i2a_STK00
	STA	_i2a_STK00
	CLRA	
	SUBB	r0x11DA
	STA	r0x11DA
;	;.line	87; "printf.c"	*bf++ = '-';
	LDA	_i2a_STK01
	STA	_ROMPH
	LDA	_i2a_STK02
	STA	_ROMPL
	LDA	#0x2d
	STA	@_ROMPINC
	LDA	_i2a_STK02
	INCA	
	STA	_i2a_STK02
	CLRA	
	ADDC	_i2a_STK01
	STA	_i2a_STK01
_00249_DS_:
;	;.line	89; "printf.c"	ui2a(num,10,0,bf);
	LDA	_i2a_STK02
	STA	_ui2a_STK06
	LDA	_i2a_STK01
	STA	_ui2a_STK05
	CLRA	
	STA	_ui2a_STK04
	STA	_ui2a_STK03
	LDA	#0x0a
	STA	_ui2a_STK02
	CLRA	
	STA	_ui2a_STK01
	LDA	_i2a_STK00
	STA	_ui2a_STK00
	LDA	r0x11DA
	CALL	_ui2a
;	;.line	90; "printf.c"	}
	RET	
; exit point of _i2a
	.ENDFUNC _i2a

;--------------------------------------------------------
	.FUNC _ui2a:$PNUM 8:$C:__divuint:$C:__mulint:$C:__moduint\
:$L:r0x11B6:$L:_ui2a_STK00:$L:_ui2a_STK01:$L:_ui2a_STK02:$L:_ui2a_STK03\
:$L:_ui2a_STK04:$L:_ui2a_STK05:$L:_ui2a_STK06:$L:r0x11BD:$L:r0x11BE\
:$L:r0x11C0:$L:r0x11BF:$L:r0x11C2:$L:r0x11C1:$L:r0x11C3\
:$L:r0x11C4:$L:r0x11C5:$L:r0x11C6
;--------------------------------------------------------
;	.line	65; "printf.c"	static void ui2a(unsigned int num, unsigned int base, int uc,char * bf)
_ui2a:	;Function start
	STA	r0x11B6
;	;.line	68; "printf.c"	unsigned int d=1;
	LDA	#0x01
	STA	r0x11BD
	CLRA	
	STA	r0x11BE
_00190_DS_:
;	;.line	69; "printf.c"	while (num/d >= base)
	LDA	r0x11BD
	STA	__divuint_STK02
	LDA	r0x11BE
	STA	__divuint_STK01
	LDA	_ui2a_STK00
	STA	__divuint_STK00
	LDA	r0x11B6
	CALL	__divuint
	STA	r0x11C0
	LDA	STK00
	SETB	_C
	SUBB	_ui2a_STK02
	LDA	r0x11C0
	SUBB	_ui2a_STK01
	JNC	_00211_DS_
;	;.line	70; "printf.c"	d*=base;        
	LDA	_ui2a_STK02
	STA	__mulint_STK02
	LDA	_ui2a_STK01
	STA	__mulint_STK01
	LDA	r0x11BD
	STA	__mulint_STK00
	LDA	r0x11BE
	CALL	__mulint
	STA	r0x11BE
	LDA	STK00
	STA	r0x11BD
	JMP	_00190_DS_
_00211_DS_:
;	;.line	71; "printf.c"	while (d!=0) {
	CLRA	
	STA	r0x11BF
	STA	r0x11C0
_00197_DS_:
	LDA	r0x11BD
	ORA	r0x11BE
	JZ	_00199_DS_
;	;.line	72; "printf.c"	int dgt = num / d;
	LDA	r0x11BD
	STA	__divuint_STK02
	LDA	r0x11BE
	STA	__divuint_STK01
	LDA	_ui2a_STK00
	STA	__divuint_STK00
	LDA	r0x11B6
	CALL	__divuint
	STA	r0x11C2
	LDA	STK00
	STA	r0x11C1
;	;.line	73; "printf.c"	num%= d;
	LDA	r0x11BD
	STA	__moduint_STK02
	LDA	r0x11BE
	STA	__moduint_STK01
	LDA	_ui2a_STK00
	STA	__moduint_STK00
	LDA	r0x11B6
	CALL	__moduint
	STA	r0x11B6
	LDA	STK00
	STA	_ui2a_STK00
;	;.line	74; "printf.c"	d/=base;
	LDA	_ui2a_STK02
	STA	__divuint_STK02
	LDA	_ui2a_STK01
	STA	__divuint_STK01
	LDA	r0x11BD
	STA	__divuint_STK00
	LDA	r0x11BE
	CALL	__divuint
	STA	r0x11BE
	LDA	STK00
	STA	r0x11BD
;	;.line	75; "printf.c"	if (n || dgt>0 || d==0) {
	LDA	r0x11BF
	ORA	r0x11C0
	JNZ	_00193_DS_
	SETB	_C
	CLRA	
	SUBB	r0x11C1
	CLRA	
	SUBSI	
	SUBB	r0x11C2
	JNC	_00193_DS_
	LDA	r0x11BD
	ORA	r0x11BE
	JNZ	_00197_DS_
_00193_DS_:
;	;.line	76; "printf.c"	*bf++ = dgt+(dgt<10 ? '0' : (uc ? 'A' : 'a')-10);
	LDA	_ui2a_STK06
	STA	r0x11C3
	LDA	_ui2a_STK05
	STA	r0x11C4
	LDA	_ui2a_STK06
	INCA	
	STA	_ui2a_STK06
	CLRA	
	ADDC	_ui2a_STK05
	STA	_ui2a_STK05
	LDA	r0x11C1
	STA	r0x11C5
	LDA	r0x11C1
	ADD	#0xf6
	LDA	r0x11C2
	XOR	#0x80
	ADDC	#0x7f
	JC	_00202_DS_
	LDA	#0x30
	STA	r0x11C1
	CLRA	
	JMP	_00242_DS_
_00202_DS_:
	LDA	_ui2a_STK04
	ORA	_ui2a_STK03
	JZ	_00204_DS_
	LDA	#0x41
	STA	r0x11C6
	CLRA	
	JMP	_00205_DS_
_00204_DS_:
	LDA	#0x61
	STA	r0x11C6
_00205_DS_:
	LDA	r0x11C6
	ADD	#0xf6
	STA	r0x11C1
_00242_DS_:
	LDA	r0x11C1
	ADD	r0x11C5
	STA	r0x11C5
	LDA	r0x11C4
	STA	_ROMPH
	LDA	r0x11C3
	STA	_ROMPL
	LDA	r0x11C5
	STA	@_ROMPINC
;	;.line	77; "printf.c"	++n;
	LDA	r0x11BF
	INCA	
	STA	r0x11BF
	CLRA	
	ADDC	r0x11C0
	STA	r0x11C0
	JMP	_00197_DS_
_00199_DS_:
;	;.line	80; "printf.c"	*bf=0;
	LDA	_ui2a_STK05
	STA	_ROMPH
	LDA	_ui2a_STK06
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
;	;.line	81; "printf.c"	}
	RET	
; exit point of _ui2a
	.ENDFUNC _ui2a

;--------------------------------------------------------
	.FUNC _li2a:$PNUM 6:$C:_uli2a\
:$L:r0x11AD:$L:_li2a_STK00:$L:_li2a_STK01:$L:_li2a_STK02:$L:_li2a_STK03\
:$L:_li2a_STK04
;--------------------------------------------------------
;	.line	54; "printf.c"	static void li2a (long num, char * bf)
_li2a:	;Function start
	STA	r0x11AD
;	;.line	56; "printf.c"	if (num<0) {
	JPL	_00178_DS_
;	;.line	57; "printf.c"	num=-num;
	SETB	_C
	CLRA	
	SUBB	_li2a_STK02
	STA	_li2a_STK02
	CLRA	
	SUBB	_li2a_STK01
	STA	_li2a_STK01
	CLRA	
	SUBB	_li2a_STK00
	STA	_li2a_STK00
	CLRA	
	SUBB	r0x11AD
	STA	r0x11AD
;	;.line	58; "printf.c"	*bf++ = '-';
	LDA	_li2a_STK03
	STA	_ROMPH
	LDA	_li2a_STK04
	STA	_ROMPL
	LDA	#0x2d
	STA	@_ROMPINC
	LDA	_li2a_STK04
	INCA	
	STA	_li2a_STK04
	CLRA	
	ADDC	_li2a_STK03
	STA	_li2a_STK03
_00178_DS_:
;	;.line	60; "printf.c"	uli2a(num,10,0,bf);
	LDA	_li2a_STK04
	STA	_uli2a_STK08
	LDA	_li2a_STK03
	STA	_uli2a_STK07
	CLRA	
	STA	_uli2a_STK06
	STA	_uli2a_STK05
	LDA	#0x0a
	STA	_uli2a_STK04
	CLRA	
	STA	_uli2a_STK03
	LDA	_li2a_STK02
	STA	_uli2a_STK02
	LDA	_li2a_STK01
	STA	_uli2a_STK01
	LDA	_li2a_STK00
	STA	_uli2a_STK00
	LDA	r0x11AD
	CALL	_uli2a
;	;.line	61; "printf.c"	}
	RET	
; exit point of _li2a
	.ENDFUNC _li2a

;--------------------------------------------------------
	.FUNC _uli2a:$PNUM 10:$C:__divulong:$C:__mullong:$C:__modulong\
:$L:r0x1175:$L:_uli2a_STK00:$L:_uli2a_STK01:$L:_uli2a_STK02:$L:_uli2a_STK03\
:$L:_uli2a_STK04:$L:_uli2a_STK05:$L:_uli2a_STK06:$L:_uli2a_STK07:$L:_uli2a_STK08\
:$L:r0x117C:$L:r0x117D:$L:r0x117E:$L:r0x117F:$L:r0x1183\
:$L:r0x1182:$L:r0x1181:$L:r0x1180:$L:r0x1184:$L:r0x1185\
:$L:r0x1187:$L:r0x1188:$L:r0x1189
;--------------------------------------------------------
;	.line	36; "printf.c"	static void uli2a(unsigned long int num, unsigned int base, int uc,char * bf)
_uli2a:	;Function start
	STA	r0x1175
;	;.line	39; "printf.c"	unsigned long d=1;
	LDA	#0x01
	STA	r0x117C
	CLRA	
	STA	r0x117D
	STA	r0x117E
	STA	r0x117F
_00117_DS_:
;	;.line	40; "printf.c"	while (num/d >= base)
	LDA	r0x117C
	STA	__divulong_STK06
	LDA	r0x117D
	STA	__divulong_STK05
	LDA	r0x117E
	STA	__divulong_STK04
	LDA	r0x117F
	STA	__divulong_STK03
	LDA	_uli2a_STK02
	STA	__divulong_STK02
	LDA	_uli2a_STK01
	STA	__divulong_STK01
	LDA	_uli2a_STK00
	STA	__divulong_STK00
	LDA	r0x1175
	CALL	__divulong
	STA	r0x1183
	LDA	_uli2a_STK04
	STA	r0x1184
	LDA	_uli2a_STK03
	STA	r0x1185
	CLRA	
	STA	r0x1187
	SETB	_C
	LDA	STK02
	SUBB	r0x1184
	LDA	STK01
	SUBB	r0x1185
	LDA	STK00
	SUBB	#0x00
	LDA	r0x1183
	SUBB	r0x1187
	JNC	_00138_DS_
;	;.line	41; "printf.c"	d*=base;         
	LDA	r0x1184
	STA	__mullong_STK06
	LDA	r0x1185
	STA	__mullong_STK05
	CLRA	
	STA	__mullong_STK04
	LDA	r0x1187
	STA	__mullong_STK03
	LDA	r0x117C
	STA	__mullong_STK02
	LDA	r0x117D
	STA	__mullong_STK01
	LDA	r0x117E
	STA	__mullong_STK00
	LDA	r0x117F
	CALL	__mullong
	STA	r0x117F
	LDA	STK00
	STA	r0x117E
	LDA	STK01
	STA	r0x117D
	LDA	STK02
	STA	r0x117C
	JMP	_00117_DS_
_00138_DS_:
;	;.line	42; "printf.c"	while (d!=0) {
	LDA	_uli2a_STK08
	STA	_uli2a_STK04
	LDA	_uli2a_STK07
	STA	_uli2a_STK03
	CLRA	
	STA	_uli2a_STK08
	STA	_uli2a_STK07
_00124_DS_:
	LDA	r0x117C
	ORA	r0x117D
	ORA	r0x117E
	ORA	r0x117F
	JZ	_00126_DS_
;	;.line	43; "printf.c"	int dgt = num / d;
	LDA	r0x117C
	STA	__divulong_STK06
	LDA	r0x117D
	STA	__divulong_STK05
	LDA	r0x117E
	STA	__divulong_STK04
	LDA	r0x117F
	STA	__divulong_STK03
	LDA	_uli2a_STK02
	STA	__divulong_STK02
	LDA	_uli2a_STK01
	STA	__divulong_STK01
	LDA	_uli2a_STK00
	STA	__divulong_STK00
	LDA	r0x1175
	CALL	__divulong
	LDA	STK02
	STA	r0x1188
	LDA	STK01
	STA	r0x1189
;	;.line	44; "printf.c"	num%=d;
	LDA	r0x117C
	STA	__modulong_STK06
	LDA	r0x117D
	STA	__modulong_STK05
	LDA	r0x117E
	STA	__modulong_STK04
	LDA	r0x117F
	STA	__modulong_STK03
	LDA	_uli2a_STK02
	STA	__modulong_STK02
	LDA	_uli2a_STK01
	STA	__modulong_STK01
	LDA	_uli2a_STK00
	STA	__modulong_STK00
	LDA	r0x1175
	CALL	__modulong
	STA	r0x1175
	LDA	STK00
	STA	_uli2a_STK00
	LDA	STK01
	STA	_uli2a_STK01
	LDA	STK02
	STA	_uli2a_STK02
;	;.line	45; "printf.c"	d/=base;
	LDA	r0x1184
	STA	__divulong_STK06
	LDA	r0x1185
	STA	__divulong_STK05
	CLRA	
	STA	__divulong_STK04
	LDA	r0x1187
	STA	__divulong_STK03
	LDA	r0x117C
	STA	__divulong_STK02
	LDA	r0x117D
	STA	__divulong_STK01
	LDA	r0x117E
	STA	__divulong_STK00
	LDA	r0x117F
	CALL	__divulong
	STA	r0x117F
	LDA	STK00
	STA	r0x117E
	LDA	STK01
	STA	r0x117D
	LDA	STK02
	STA	r0x117C
;	;.line	46; "printf.c"	if (n || dgt>0|| d==0) {
	LDA	_uli2a_STK08
	ORA	_uli2a_STK07
	JNZ	_00120_DS_
	SETB	_C
	CLRA	
	SUBB	r0x1188
	CLRA	
	SUBSI	
	SUBB	r0x1189
	JNC	_00120_DS_
	LDA	r0x117C
	ORA	r0x117D
	ORA	r0x117E
	ORA	r0x117F
	JNZ	_00124_DS_
_00120_DS_:
;	;.line	47; "printf.c"	*bf++ = dgt+(dgt<10 ? '0' : (uc ? 'A' : 'a')-10);
	LDA	_uli2a_STK04
	STA	r0x1180
	LDA	_uli2a_STK03
	STA	r0x1181
	LDA	_uli2a_STK04
	INCA	
	STA	_uli2a_STK04
	CLRA	
	ADDC	_uli2a_STK03
	STA	_uli2a_STK03
	LDA	r0x1188
	STA	r0x1182
	LDA	r0x1188
	ADD	#0xf6
	LDA	r0x1189
	XOR	#0x80
	ADDC	#0x7f
	JC	_00129_DS_
	LDA	#0x30
	STA	r0x1183
	CLRA	
	JMP	_00171_DS_
_00129_DS_:
	LDA	_uli2a_STK06
	ORA	_uli2a_STK05
	JZ	_00131_DS_
	LDA	#0x41
	STA	r0x1189
	CLRA	
	JMP	_00132_DS_
_00131_DS_:
	LDA	#0x61
	STA	r0x1189
_00132_DS_:
	LDA	r0x1189
	ADD	#0xf6
	STA	r0x1183
_00171_DS_:
	LDA	r0x1183
	ADD	r0x1182
	STA	r0x1182
	LDA	r0x1181
	STA	_ROMPH
	LDA	r0x1180
	STA	_ROMPL
	LDA	r0x1182
	STA	@_ROMPINC
;	;.line	48; "printf.c"	++n;
	LDA	_uli2a_STK08
	INCA	
	STA	_uli2a_STK08
	CLRA	
	ADDC	_uli2a_STK07
	STA	_uli2a_STK07
	JMP	_00124_DS_
_00126_DS_:
;	;.line	51; "printf.c"	*bf=0;
	LDA	_uli2a_STK03
	STA	_ROMPH
	LDA	_uli2a_STK04
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
;	;.line	52; "printf.c"	}
	RET	
; exit point of _uli2a
	.ENDFUNC _uli2a

;--------------------------------------------------------
	.FUNC _putch:$PNUM 3:$C:_putchar\
:$L:_putch_STK01
;--------------------------------------------------------
;	.line	31; "printf.c"	putchar(c);
_putch:	;Function start
	LDA	_putch_STK01
	CALL	_putchar
;	;.line	32; "printf.c"	}
	RET	
; exit point of _putch
	.ENDFUNC _putch
.globl _sprintf

;--------------------------------------------------------
	.FUNC_PSTK _sprintf:$C:_tfp_format:$C:_putcp\
:$L:r0x115F:$L:r0x1160:$L:r0x1162:$L:r0x1161:$L:r0x1164\

;--------------------------------------------------------
;	.line	23; "printf.c"	va_start(va,fmt);
_sprintf:	;Function start
	LDA	#0xfa
	ADD	_RAMP1L
	STA	r0x115F
	LDA	#0xff
	ADDC	_RAMP1H
	STA	r0x1162
	LDA	r0x115F
	STA	r0x1161
;	;.line	24; "printf.c"	tfp_format(&s,putcp,fmt,va);
	LDA	#0xfc
	ADD	_RAMP1L
	STA	r0x115F
	LDA	#0xff
	ADDC	_RAMP1H
	STA	r0x1160
	STA	r0x1164
	LDA	r0x1161
	STA	_tfp_format_STK06
	LDA	r0x1162
	STA	_tfp_format_STK05
	LDA	@P1,-6
	STA	_tfp_format_STK04
	LDA	@P1,-5
	STA	_tfp_format_STK03
	LDA	#low (_putcp + 0)
	STA	_tfp_format_STK02
	LDA	#high (_putcp + 0)
	STA	_tfp_format_STK01
	LDA	r0x115F
	STA	_tfp_format_STK00
	LDA	r0x1164
	CALL	_tfp_format
;	;.line	25; "printf.c"	putcp(&s,0);
	CLRA	
	STA	_putcp_STK01
	LDA	r0x115F
	STA	_putcp_STK00
	LDA	r0x1160
	CALL	_putcp
;	;.line	27; "printf.c"	}
	RET	
; exit point of _sprintf
	.ENDFUNC _sprintf
.globl _putchar

;--------------------------------------------------------
	.FUNC _putchar:$PNUM 1
;--------------------------------------------------------
;	.line	15; "printf.c"	void putchar(char c)
_putchar:	;Function start
	STA	_SPIDAT
;	;.line	18; "printf.c"	}
	RET	
; exit point of _putchar
	.ENDFUNC _putchar
	;--cdb--S:Lprintf.sprintf$s$65536$7({2}DD,SC:U),B,1,-4
	;--cdb--S:Lprintf.sprintf$fmt$65536$7({2}DG,SC:U),B,1,-6
	;--cdb--S:Lprintf.sprintf$va$65536$8({2}DD,SC:U),R,0,0,[r0x1161,r0x1162]
	;--cdb--S:Lprintf.printf$fmt$65536$42({2}DG,SC:U),B,1,-4
	;--cdb--S:Lprintf.printf$va$65536$43({2}DD,SC:U),R,0,0,[r0x1224,r0x1225]
	;--cdb--S:G$putcp$0$0({2}DF,SV:S),C,0,-4
	;--cdb--S:G$tfp_format$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:G$putchar$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$sprintf$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:Fprintf$putch$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:Fprintf$uli2a$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:G$_divulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$_modulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:Fprintf$li2a$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:Fprintf$ui2a$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:G$_divuint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_mulint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_moduint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:Fprintf$i2a$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:Fprintf$a2d$0$0({2}DF,SI:S),C,0,-2
	;--cdb--S:Fprintf$a2i$0$0({2}DF,SC:U),C,0,-2
	;--cdb--S:Fprintf$putchw$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:G$printf$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:Fprintf$stdout_putf$0$0({2}DC,DF,SV:S),E,0,0
	;--cdb--S:Fprintf$stdout_putp$0$0({2}DG,SV:S),E,0,0
	;--cdb--S:G$SPIDAT$0$0({1}SC:U),E,0,0
	;--cdb--S:Lprintf.putchar$c$65536$5({1}SC:U),R,0,0,[]
	;--cdb--S:Lprintf.putch$c$65536$9({1}SC:U),R,0,0,[_putch_STK01]
	;--cdb--S:Lprintf.putch$p$65536$9({2}DG,SV:S),R,0,0,[]
	;--cdb--S:Lprintf.uli2a$bf$65536$11({2}DG,SC:U),R,0,0,[_uli2a_STK08,_uli2a_STK07]
	;--cdb--S:Lprintf.uli2a$uc$65536$11({2}SI:S),R,0,0,[_uli2a_STK06,_uli2a_STK05]
	;--cdb--S:Lprintf.uli2a$base$65536$11({2}SI:U),R,0,0,[_uli2a_STK04,_uli2a_STK03]
	;--cdb--S:Lprintf.uli2a$num$65536$11({4}SL:U),R,0,0,[_uli2a_STK02,_uli2a_STK01,_uli2a_STK00,r0x1175]
	;--cdb--S:Lprintf.uli2a$n$65536$12({2}SI:S),R,0,0,[_uli2a_STK08,_uli2a_STK07]
	;--cdb--S:Lprintf.uli2a$d$65536$12({4}SL:U),R,0,0,[r0x117C,r0x117D,r0x117E,r0x117F]
	;--cdb--S:Lprintf.uli2a$dgt$131072$13({2}SI:S),R,0,0,[r0x1188,r0x1189]
	;--cdb--S:Lprintf.li2a$bf$65536$15({2}DG,SC:U),R,0,0,[_li2a_STK04,_li2a_STK03]
	;--cdb--S:Lprintf.li2a$num$65536$15({4}SL:S),R,0,0,[_li2a_STK02,_li2a_STK01,_li2a_STK00,r0x11AD]
	;--cdb--S:Lprintf.ui2a$bf$65536$18({2}DG,SC:U),R,0,0,[_ui2a_STK06,_ui2a_STK05]
	;--cdb--S:Lprintf.ui2a$uc$65536$18({2}SI:S),R,0,0,[_ui2a_STK04,_ui2a_STK03]
	;--cdb--S:Lprintf.ui2a$base$65536$18({2}SI:U),R,0,0,[_ui2a_STK02,_ui2a_STK01]
	;--cdb--S:Lprintf.ui2a$num$65536$18({2}SI:U),R,0,0,[_ui2a_STK00,r0x11B6]
	;--cdb--S:Lprintf.ui2a$n$65536$19({2}SI:S),R,0,0,[r0x11BF,r0x11C0]
	;--cdb--S:Lprintf.ui2a$d$65536$19({2}SI:U),R,0,0,[r0x11BD,r0x11BE]
	;--cdb--S:Lprintf.ui2a$dgt$131072$20({2}SI:S),R,0,0,[r0x11C1,r0x11C2]
	;--cdb--S:Lprintf.i2a$bf$65536$22({2}DG,SC:U),R,0,0,[_i2a_STK02,_i2a_STK01]
	;--cdb--S:Lprintf.i2a$num$65536$22({2}SI:S),R,0,0,[_i2a_STK00,r0x11DA]
	;--cdb--S:Lprintf.a2d$ch$65536$25({1}SC:U),R,0,0,[r0x11E0]
	;--cdb--S:Lprintf.a2i$nump$65536$27({2}DG,SI:S),R,0,0,[_a2i_STK05,_a2i_STK04]
	;--cdb--S:Lprintf.a2i$base$65536$27({2}SI:S),R,0,0,[_a2i_STK03,_a2i_STK02]
	;--cdb--S:Lprintf.a2i$src$65536$27({2}DG,DG,SC:U),R,0,0,[_a2i_STK01,_a2i_STK00]
	;--cdb--S:Lprintf.a2i$ch$65536$27({1}SC:U),R,0,0,[r0x11E4]
	;--cdb--S:Lprintf.a2i$p$65536$28({2}DG,SC:U),R,0,0,[]
	;--cdb--S:Lprintf.a2i$num$65536$28({2}SI:S),R,0,0,[r0x11ED,r0x11EE]
	;--cdb--S:Lprintf.a2i$digit$65536$28({2}SI:S),R,0,0,[r0x11F1,r0x11F2]
	;--cdb--S:Lprintf.putchw$bf$65536$30({2}DG,SC:U),R,0,0,[_putchw_STK07,_putchw_STK06]
	;--cdb--S:Lprintf.putchw$z$65536$30({1}SC:U),R,0,0,[_putchw_STK05]
	;--cdb--S:Lprintf.putchw$n$65536$30({2}SI:S),R,0,0,[_putchw_STK04,_putchw_STK03]
	;--cdb--S:Lprintf.putchw$putf$65536$30({2}DC,DF,SV:S),R,0,0,[_putchw_STK02,_putchw_STK01]
	;--cdb--S:Lprintf.putchw$putp$65536$30({2}DG,SV:S),R,0,0,[_putchw_STK00,r0x11FA]
	;--cdb--S:Lprintf.putchw$fc$65536$31({1}SC:U),R,0,0,[r0x1203]
	;--cdb--S:Lprintf.putchw$ch$65536$31({1}SC:U),R,0,0,[_putchw_STK07]
	;--cdb--S:Lprintf.putchw$p$65536$31({2}DG,SC:U),R,0,0,[_putchw_STK05,r0x1202]
	;--cdb--S:Lprintf.tfp_format$va$65536$32({2}DD,SC:U),R,0,0,[r0x1213,r0x1214]
	;--cdb--S:Lprintf.tfp_format$fmt$65536$32({2}DG,SC:U),E,0,0
	;--cdb--S:Lprintf.tfp_format$putf$65536$32({2}DC,DF,SV:S),R,0,0,[_tfp_format_STK02,_tfp_format_STK01]
	;--cdb--S:Lprintf.tfp_format$putp$65536$32({2}DG,SV:S),R,0,0,[_tfp_format_STK00,r0x1210]
	;--cdb--S:Lprintf.tfp_format$bf$65536$33({12}DA12d,SC:U),E,0,0
	;--cdb--S:Lprintf.tfp_format$ch$65536$33({1}SC:U),R,0,0,[r0x1219]
	;--cdb--S:Lprintf.tfp_format$lz$196608$35({1}SC:U),R,0,0,[r0x1215]
	;--cdb--S:Lprintf.tfp_format$lng$196608$35({1}SC:U),R,0,0,[r0x1216]
	;--cdb--S:Lprintf.tfp_format$w$196608$35({2}SI:S),E,0,0
	;--cdb--S:Lprintf.putcp$c$65536$44({1}SC:U),R,0,0,[_putcp_STK01]
	;--cdb--S:Lprintf.putcp$p$65536$44({2}DG,SV:S),R,0,0,[_putcp_STK00,r0x122A]
	;--cdb--S:G$putchar$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$tfp_format$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:G$putcp$0$0({2}DF,SV:S),C,0,-4
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__divulong
	.globl	__mullong
	.globl	__modulong
	.globl	__divuint
	.globl	__mulint
	.globl	__moduint
	.globl	_SPIDAT

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_putcp
	.globl	_tfp_format
	.globl	_putchar
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_printf_0	udata
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1164:	.ds	1
r0x1175:	.ds	1
r0x117C:	.ds	1
r0x117D:	.ds	1
r0x117E:	.ds	1
r0x117F:	.ds	1
r0x1180:	.ds	1
r0x1181:	.ds	1
r0x1182:	.ds	1
r0x1183:	.ds	1
r0x1184:	.ds	1
r0x1185:	.ds	1
r0x1187:	.ds	1
r0x1188:	.ds	1
r0x1189:	.ds	1
r0x11AD:	.ds	1
r0x11B6:	.ds	1
r0x11BD:	.ds	1
r0x11BE:	.ds	1
r0x11BF:	.ds	1
r0x11C0:	.ds	1
r0x11C1:	.ds	1
r0x11C2:	.ds	1
r0x11C3:	.ds	1
r0x11C4:	.ds	1
r0x11C5:	.ds	1
r0x11C6:	.ds	1
r0x11DA:	.ds	1
r0x11E0:	.ds	1
r0x11E1:	.ds	1
r0x11E2:	.ds	1
r0x11E3:	.ds	1
r0x11E4:	.ds	1
r0x11EB:	.ds	1
r0x11EC:	.ds	1
r0x11ED:	.ds	1
r0x11EE:	.ds	1
r0x11F0:	.ds	1
r0x11F1:	.ds	1
r0x11F2:	.ds	1
r0x11FA:	.ds	1
r0x1202:	.ds	1
r0x1203:	.ds	1
r0x1204:	.ds	1
r0x1210:	.ds	1
r0x1213:	.ds	1
r0x1214:	.ds	1
r0x1215:	.ds	1
r0x1216:	.ds	1
r0x1217:	.ds	1
r0x1218:	.ds	1
r0x1219:	.ds	1
r0x121A:	.ds	1
r0x121C:	.ds	1
r0x121D:	.ds	1
r0x121E:	.ds	1
r0x1222:	.ds	1
r0x1225:	.ds	1
r0x122A:	.ds	1
r0x122C:	.ds	1
r0x122D:	.ds	1
r0x122E:	.ds	1
r0x122F:	.ds	1
_stdout_putf:	.ds	2
_stdout_putp:	.ds	2
	.area DSEG (DATA); (local stack unassigned) 
_tfp_format_STK06:	.ds	1
	.globl _tfp_format_STK06
_tfp_format_STK05:	.ds	1
	.globl _tfp_format_STK05
_tfp_format_STK04:	.ds	1
	.globl _tfp_format_STK04
_tfp_format_STK03:	.ds	1
	.globl _tfp_format_STK03
_tfp_format_STK02:	.ds	1
	.globl _tfp_format_STK02
_tfp_format_STK01:	.ds	1
	.globl _tfp_format_STK01
_tfp_format_STK00:	.ds	1
	.globl _tfp_format_STK00
_putcp_STK01:	.ds	1
	.globl _putcp_STK01
_putcp_STK00:	.ds	1
	.globl _putcp_STK00
_putch_STK01:	.ds	1
_uli2a_STK00:	.ds	1
_uli2a_STK01:	.ds	1
_uli2a_STK02:	.ds	1
_uli2a_STK03:	.ds	1
_uli2a_STK04:	.ds	1
_uli2a_STK05:	.ds	1
_uli2a_STK06:	.ds	1
_uli2a_STK07:	.ds	1
_uli2a_STK08:	.ds	1
	.globl __divulong_STK06
	.globl __divulong_STK05
	.globl __divulong_STK04
	.globl __divulong_STK03
	.globl __divulong_STK02
	.globl __divulong_STK01
	.globl __divulong_STK00
	.globl __mullong_STK06
	.globl __mullong_STK05
	.globl __mullong_STK04
	.globl __mullong_STK03
	.globl __mullong_STK02
	.globl __mullong_STK01
	.globl __mullong_STK00
	.globl __modulong_STK06
	.globl __modulong_STK05
	.globl __modulong_STK04
	.globl __modulong_STK03
	.globl __modulong_STK02
	.globl __modulong_STK01
	.globl __modulong_STK00
_li2a_STK00:	.ds	1
_li2a_STK01:	.ds	1
_li2a_STK02:	.ds	1
_li2a_STK03:	.ds	1
_li2a_STK04:	.ds	1
_ui2a_STK00:	.ds	1
_ui2a_STK01:	.ds	1
_ui2a_STK02:	.ds	1
_ui2a_STK03:	.ds	1
_ui2a_STK04:	.ds	1
_ui2a_STK05:	.ds	1
_ui2a_STK06:	.ds	1
	.globl __divuint_STK02
	.globl __divuint_STK01
	.globl __divuint_STK00
	.globl __mulint_STK02
	.globl __mulint_STK01
	.globl __mulint_STK00
	.globl __moduint_STK02
	.globl __moduint_STK01
	.globl __moduint_STK00
_i2a_STK00:	.ds	1
_i2a_STK01:	.ds	1
_i2a_STK02:	.ds	1
_a2i_STK00:	.ds	1
_a2i_STK01:	.ds	1
_a2i_STK02:	.ds	1
_a2i_STK03:	.ds	1
_a2i_STK04:	.ds	1
_a2i_STK05:	.ds	1
_putchw_STK00:	.ds	1
_putchw_STK01:	.ds	1
_putchw_STK02:	.ds	1
_putchw_STK03:	.ds	1
_putchw_STK04:	.ds	1
_putchw_STK05:	.ds	1
_putchw_STK06:	.ds	1
_putchw_STK07:	.ds	1
_tfp_format_fmt_65536_32:	.ds	2
	.globl _tfp_format_fmt_65536_32
_tfp_format_w_196608_35:	.ds	2
	.globl _tfp_format_w_196608_35
_tfp_format_bf_65536_33:	.ds	12
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x122A:NULL+0:-1:1
	;--cdb--W:r0x1223:NULL+0:-1:1
	;--cdb--W:r0x1224:NULL+0:-1:1
	;--cdb--W:r0x1217:NULL+0:4629:0
	;--cdb--W:r0x1218:NULL+0:4630:0
	;--cdb--W:r0x1218:NULL+0:4629:0
	;--cdb--W:r0x121B:NULL+0:4632:0
	;--cdb--W:r0x121B:NULL+0:0:0
	;--cdb--W:r0x121A:NULL+0:4630:0
	;--cdb--W:r0x121A:NULL+0:4631:0
	;--cdb--W:r0x121C:NULL+0:4634:0
	;--cdb--W:r0x1218:NULL+0:-1:1
	;--cdb--W:r0x1217:NULL+0:-1:1
	;--cdb--W:r0x121B:NULL+0:-1:1
	;--cdb--W:r0x121A:NULL+0:-1:1
	;--cdb--W:r0x121C:NULL+0:-1:1
	;--cdb--W:r0x121F:NULL+0:-1:1
	;--cdb--W:r0x121E:NULL+0:-1:1
	;--cdb--W:r0x1216:NULL+0:-1:1
	;--cdb--W:r0x1202:NULL+0:-1:1
	;--cdb--W:r0x11EF:NULL+0:14:0
	;--cdb--W:r0x11E1:NULL+0:4576:0
	;--cdb--W:r0x11E2:NULL+0:0:0
	;--cdb--W:r0x11E2:NULL+0:-1:1
	;--cdb--W:r0x11BF:NULL+0:-1:1
	;--cdb--W:r0x11C2:NULL+0:-1:1
	;--cdb--W:r0x11C7:NULL+0:-1:1
	;--cdb--W:r0x11C8:NULL+0:-1:1
	;--cdb--W:r0x11C6:NULL+0:-1:1
	;--cdb--W:r0x1183:NULL+0:-1:1
	;--cdb--W:r0x1182:NULL+0:-1:1
	;--cdb--W:r0x1188:NULL+0:-1:1
	;--cdb--W:r0x118A:NULL+0:-1:1
	;--cdb--W:r0x118B:NULL+0:-1:1
	;--cdb--W:r0x1189:NULL+0:-1:1
	;--cdb--W:r0x1182:NULL+0:14:0
	;--cdb--W:r0x1181:NULL+0:13:0
	;--cdb--W:r0x1180:NULL+0:12:0
	;--cdb--W:r0x1186:NULL+0:0:0
	;--cdb--W:r0x1186:NULL+0:-1:1
	;--cdb--W:r0x1160:NULL+0:-1:1
	;--cdb--W:r0x1162:NULL+0:4448:0
	;--cdb--W:r0x1161:NULL+0:4447:0
	;--cdb--W:r0x1163:NULL+0:4447:0
	end
