;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_strspn.c"
	.module _strspn
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--F:G$strspn$0$0({2}DF,SI:U),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _strspn-code 
.globl _strspn

;--------------------------------------------------------
	.FUNC _strspn:$PNUM 4:$C:_strchr\
:$L:r0x1160:$L:_strspn_STK00:$L:_strspn_STK01:$L:_strspn_STK02:$L:r0x1163\
:$L:r0x1164:$L:r0x1165:$L:r0x1166
;--------------------------------------------------------
;	.line	31; "_strspn.c"	size_t strspn ( char * string, char * control )
_strspn:	;Function start
	STA	r0x1160
;	;.line	36; "_strspn.c"	while (ch = *string) {
	CLRA	
	STA	r0x1163
	STA	r0x1164
_00108_DS_:
	LDA	_strspn_STK00
	STA	_ROMPL
	LDA	r0x1160
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1165
	STA	r0x1166
	LDA	r0x1165
	JZ	_00110_DS_
;	;.line	37; "_strspn.c"	if ( strchr(control,ch) )
	LDA	r0x1166
	STA	_strchr_STK01
	LDA	_strspn_STK02
	STA	_strchr_STK00
	LDA	_strspn_STK01
	CALL	_strchr
	ORA	STK00
	JZ	_00110_DS_
;	;.line	38; "_strspn.c"	count++ ;
	LDA	r0x1163
	INCA	
	STA	r0x1163
	CLRA	
	ADDC	r0x1164
	STA	r0x1164
;	;.line	41; "_strspn.c"	string++ ;
	LDA	_strspn_STK00
	INCA	
	STA	_strspn_STK00
	CLRA	
	ADDC	r0x1160
	STA	r0x1160
	JMP	_00108_DS_
_00110_DS_:
;	;.line	44; "_strspn.c"	return count ;
	LDA	r0x1163
	STA	STK00
	LDA	r0x1164
;	;.line	45; "_strspn.c"	}
	RET	
; exit point of _strspn
	.ENDFUNC _strspn
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_strspn.strspn$control$65536$21({2}DG,SC:U),R,0,0,[_strspn_STK02,_strspn_STK01]
	;--cdb--S:L_strspn.strspn$string$65536$21({2}DG,SC:U),R,0,0,[_strspn_STK00]
	;--cdb--S:L_strspn.strspn$count$65536$22({2}SI:U),R,0,0,[r0x1163,r0x1164]
	;--cdb--S:L_strspn.strspn$ch$65536$22({1}SC:U),R,0,0,[r0x1166]
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_strchr

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_strspn
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__strspn_0	udata
r0x1160:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_strspn_STK00:	.ds	1
	.globl _strspn_STK00
_strspn_STK01:	.ds	1
	.globl _strspn_STK01
_strspn_STK02:	.ds	1
	.globl _strspn_STK02
	.globl _strchr_STK01
	.globl _strchr_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1165:NULL+0:14:0
	;--cdb--W:r0x1166:NULL+0:-1:1
	end
