;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_setjmp.c"
	.module _setjmp
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$longjmp$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$longjmp$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$__setjmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$__setjmp$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _setjmp-code 
.globl _longjmp

;--------------------------------------------------------
	.FUNC _longjmp:$PNUM 4:$L:r0x1173:$L:_longjmp_STK00:$L:_longjmp_STK01:$L:_longjmp_STK02
;--------------------------------------------------------
;	.line	60; "_setjmp.c"	_Noreturn void longjmp (jmp_buf buf, int rv)
_longjmp:	;Function start
	STA	r0x1173
;	;.line	62; "_setjmp.c"	rv0=rv?rv:1;
	LDA	_longjmp_STK02
	ORA	_longjmp_STK01
	JNZ	_00112_DS_
	LDA	#0x01
	STA	_longjmp_STK02
	CLRA	
	STA	_longjmp_STK01
_00112_DS_:
	LDA	_longjmp_STK02
	STA	_rv0
	LDA	_longjmp_STK01
	STA	(_rv0 + 1)
;	;.line	63; "_setjmp.c"	stkchk=0;
	CLRA	
	STA	_stkchk
	STA	(_stkchk + 1)
;	;.line	64; "_setjmp.c"	ROMPLH = buf;
	LDA	_longjmp_STK00
	STA	_ROMPLH
	LDA	r0x1173
	STA	(_ROMPLH + 1)
;	;.line	66; "_setjmp.c"	..asm
	.globl	STK00
;	;.line	67; "_setjmp.c"	..asm
	LDA	@_ROMPINC
;	;.line	68; "_setjmp.c"	..asm
	STA	_stkchk
;	;.line	69; "_setjmp.c"	..asm
	LDA	@_ROMPINC
;	;.line	70; "_setjmp.c"	..asm
	STA	(_stkchk+1)
;	;.line	71; "_setjmp.c"	 ..asm
back000:
	LDA	_STACKL
;	;.line	72; "_setjmp.c"	..asm
	XOR	_stkchk
;	;.line	73; "_setjmp.c"	..asm
	JNZ	next1;
;	;.line	74; "_setjmp.c"	..asm
	LDA	_STACKH
;	;.line	75; "_setjmp.c"	..asm
	XOR	(_stkchk+1)
;	;.line	76; "_setjmp.c"	..asm
	JZ	cont1;
;	;.line	78; "_setjmp.c"	 ..asm
next1:
	LDA	#low(back000)
;	;.line	79; "_setjmp.c"	..asm
	STA	_STACKL
;	;.line	80; "_setjmp.c"	..asm
	LDA	#HIGH( back000)
;	;.line	81; "_setjmp.c"	..asm
	STA	_STACKH
;	;.line	82; "_setjmp.c"	..asm
	RET
;	;.line	84; "_setjmp.c"	 ..asm
cont1:
	call	cont2
;	;.line	85; "_setjmp.c"	 ..asm
cont2:
	
;	;.line	90; "_setjmp.c"	STACKL=ROMPINC;
	LDA	@_ROMPINC
	STA	_STACKL
;	;.line	91; "_setjmp.c"	STACKH=ROMPINC;
	LDA	@_ROMPINC
	STA	_STACKH
;	;.line	92; "_setjmp.c"	RAMP0L=ROMPINC;
	LDA	@_ROMPINC
	STA	_RAMP0L
;	;.line	93; "_setjmp.c"	RAMP0H=ROMPINC;
	LDA	@_ROMPINC
	STA	_RAMP0H
;	;.line	94; "_setjmp.c"	RAMP1L=ROMPINC;
	LDA	@_ROMPINC
	STA	_RAMP1L
;	;.line	95; "_setjmp.c"	RAMP1H=ROMPINC;
	LDA	@_ROMPINC
	STA	_RAMP1H
;	;.line	97; "_setjmp.c"	..asm
	LDA	_rv0
;	;.line	98; "_setjmp.c"	..asm
	STA	STK00
;	;.line	99; "_setjmp.c"	..asm
	LDA	_rv0+1
;	;.line	100; "_setjmp.c"	..asm
	ret
;	;.line	101; "_setjmp.c"	 ..asm
	
;	;.line	102; "_setjmp.c"	}
	RET	
; exit point of _longjmp
	.ENDFUNC _longjmp
.globl ___setjmp

;--------------------------------------------------------
	.FUNC ___setjmp:$PNUM 4:$L:r0x1165:$L:___setjmp_STK00:$L:___setjmp_STK01:$L:___setjmp_STK02
;--------------------------------------------------------
;	.line	35; "_setjmp.c"	int __setjmp (jmp_buf buf, unsigned char lowstk, unsigned char highstk)
___setjmp:	;Function start
	STA	r0x1165
;	;.line	40; "_setjmp.c"	ROMPUW=(unsigned short)RAMP1UW;
	LDA	_RAMP1UW
	STA	_ROMPUW
	LDA	(_RAMP1UW + 1)
	STA	(_ROMPUW + 1)
;	;.line	41; "_setjmp.c"	ROMPUW-=2;
	LDA	#0xfe
	ADD	_ROMPUW
	STA	_ROMPUW
	LDA	#0xff
	ADDC	(_ROMPUW + 1)
	STA	(_ROMPUW + 1)
;	;.line	43; "_setjmp.c"	__ramp1save=ROMPINC;
	LDA	@_ROMPINC
	STA	___ramp1save
	CLRA	
	STA	(___ramp1save + 1)
;	;.line	44; "_setjmp.c"	__ramp1save|=(ROMPINC<<8);
	LDA	@_ROMPINC
	ORA	(___ramp1save + 1)
	STA	(___ramp1save + 1)
;	;.line	45; "_setjmp.c"	__ramp0save=ROMPUW-5;
	LDA	#0xfb
	ADD	_ROMPUW
	STA	___ramp0save
	LDA	#0xff
	ADDC	(_ROMPUW + 1)
	STA	(___ramp0save + 1)
;	;.line	47; "_setjmp.c"	ROMPLH = buf;
	LDA	___setjmp_STK00
	STA	_ROMPLH
	LDA	r0x1165
	STA	(_ROMPLH + 1)
;	;.line	48; "_setjmp.c"	ROMPINC=lowstk;
	LDA	___setjmp_STK01
	STA	@_ROMPINC
;	;.line	49; "_setjmp.c"	ROMPINC=highstk;
	LDA	___setjmp_STK02
	STA	@_ROMPINC
;	;.line	50; "_setjmp.c"	ROMPINC=STACKL;
	LDA	_STACKL
	STA	@_ROMPINC
;	;.line	51; "_setjmp.c"	ROMPINC=STACKH;
	LDA	_STACKH
	STA	@_ROMPINC
;	;.line	52; "_setjmp.c"	ROMPINC=__ramp0save;
	LDA	___ramp0save
	STA	@_ROMPINC
;	;.line	53; "_setjmp.c"	ROMPINC=__ramp0save>>8;
	LDA	(___ramp0save + 1)
	STA	@_ROMPINC
;	;.line	54; "_setjmp.c"	ROMPINC=__ramp1save;
	LDA	___ramp1save
	STA	@_ROMPINC
;	;.line	55; "_setjmp.c"	ROMPINC=__ramp1save>>8;
	LDA	(___ramp1save + 1)
	STA	@_ROMPINC
;	;.line	56; "_setjmp.c"	return 0;
	CLRA	
	STA	STK00
;	;.line	57; "_setjmp.c"	}
	RET	
; exit point of ___setjmp
	.ENDFUNC ___setjmp
	;--cdb--S:G$longjmp$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$__setjmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$PAR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PADIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOB$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMA_IL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIDAT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCLKDIV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CMPCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTVC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMICON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PRG_RAM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIDMAC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECOAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECLEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECMODE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIOPRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMALEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULB$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULBL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULBH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULO$0$0({4}SL:U),E,0,0
	;--cdb--S:G$MULO1$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULSHIFT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUF$0$0({2}SI:S),E,0,0
	;--cdb--S:G$L2UBUFL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUFH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2USH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$U2LSH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTPRI$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRA$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$LPWMRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMINV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPICK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPSEL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$HWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWDINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PATEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TRAMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMBUFP$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$PASKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBSKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABSKIP$0$0({2}SI:U),E,0,0
	;--cdb--S:G$PATR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTR$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TOUCHC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DUMMYC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IRCD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMDUTY$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMPER$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACGCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECPWR$0$0({4}SL:U),E,0,0
	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$HWPALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0UW$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$EA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
	;--cdb--S:F_setjmp$__ramp0save$0$0({2}SI:U),E,0,0
	;--cdb--S:F_setjmp$__ramp1save$0$0({2}SI:U),E,0,0
	;--cdb--S:L_setjmp.__setjmp$highstk$65536$3({1}SC:U),R,0,0,[___setjmp_STK02]
	;--cdb--S:L_setjmp.__setjmp$lowstk$65536$3({1}SC:U),R,0,0,[___setjmp_STK01]
	;--cdb--S:L_setjmp.__setjmp$buf$65536$3({2}DG,SC:U),R,0,0,[___setjmp_STK00,r0x1165]
	;--cdb--S:F_setjmp$rv0$0$0({2}SI:S),E,0,0
	;--cdb--S:F_setjmp$stkchk$0$0({2}SI:U),E,0,0
	;--cdb--S:L_setjmp.longjmp$rv$65536$5({2}SI:S),R,0,0,[_longjmp_STK02,_longjmp_STK01]
	;--cdb--S:L_setjmp.longjmp$buf$65536$5({2}DG,SC:U),R,0,0,[_longjmp_STK00,r0x1173]
	;--cdb--S:G$__setjmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$longjmp$0$0({2}DF,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_PAR
	.globl	_PADIR
	.globl	_PIOA
	.globl	_PAWK
	.globl	_PAWKDR
	.globl	_TIMERC
	.globl	_THRLD
	.globl	_RAMP0L
	.globl	_RAMP0H
	.globl	_RAMP1L
	.globl	_RAMP1H
	.globl	_PTRCL
	.globl	_PTRCH
	.globl	_ROMPL
	.globl	_ROMPH
	.globl	_BEEPC
	.globl	_FILTERGR
	.globl	_ULAWC
	.globl	_ADCON
	.globl	_DACON
	.globl	_SYSC
	.globl	_SPIM
	.globl	_SPIH
	.globl	_SPIMH
	.globl	_SPIOP
	.globl	_SPI_BANK
	.globl	_ADP_IND
	.globl	_ADP_VPL
	.globl	_ADP_VPH
	.globl	_ADL
	.globl	_ADH
	.globl	_ZC
	.globl	_ADCG
	.globl	_DAC_PL
	.globl	_DAC_PH
	.globl	_PAG
	.globl	_RDMAL
	.globl	_RDMAH
	.globl	_SPIL
	.globl	_IOMASK
	.globl	_IOCMP
	.globl	_IOCNT
	.globl	_LVDCON
	.globl	_LVDCTH
	.globl	_LVRCON
	.globl	_OFFSETL
	.globl	_OFFSETH
	.globl	_RCCON
	.globl	_BGCON
	.globl	_PWRL
	.globl	_CRYPT
	.globl	_PWRH
	.globl	_PWRHL
	.globl	_IROMDL
	.globl	_IROMDH
	.globl	_RECMUTE
	.globl	_SPKC
	.globl	_DCLAMP
	.globl	_SSPIC
	.globl	_SSPIL
	.globl	_SSPIM
	.globl	_SSPIH
	.globl	_PBR
	.globl	_PBDIR
	.globl	_PIOB
	.globl	_PBWK
	.globl	_PBWKDR
	.globl	_PAIE
	.globl	_PBIE
	.globl	_PAIF
	.globl	_PBIF
	.globl	_GIE
	.globl	_GIF
	.globl	_WDTL
	.globl	_WDTH
	.globl	_RPAGES
	.globl	_PPAGES
	.globl	_DMA_IL
	.globl	_FILTERGP
	.globl	_SPIDAT
	.globl	_RSPIC
	.globl	_RCLKDIV
	.globl	_PCR
	.globl	_PCDIR
	.globl	_PIOC
	.globl	_CMPCON
	.globl	_INTVC
	.globl	_INTV
	.globl	_DMICON
	.globl	_PRG_RAM
	.globl	_PDMAL
	.globl	_PDMAH
	.globl	_PDMALH
	.globl	_SPIDMAC
	.globl	_SDMAAL
	.globl	_SDMAAH
	.globl	_SDMAALH
	.globl	_ECRAL
	.globl	_ECRAH
	.globl	_ECRALH
	.globl	_ECOAL
	.globl	_ECOAH
	.globl	_ECOALH
	.globl	_ECLEN
	.globl	_ECCON
	.globl	_ECMODE
	.globl	_SPIOPRAH
	.globl	_SDMALEN
	.globl	_MULA
	.globl	_MULAL
	.globl	_MULAH
	.globl	_MULB
	.globl	_MULBL
	.globl	_MULBH
	.globl	_MULO
	.globl	_MULO1
	.globl	_MULSHIFT
	.globl	_L2UBUF
	.globl	_L2UBUFL
	.globl	_L2UBUFH
	.globl	_ULAWD
	.globl	_L2USH
	.globl	_U2LSH
	.globl	_INTPRI
	.globl	_LPWMRA
	.globl	_LPWMRAL
	.globl	_LPWMRAH
	.globl	_LPWMEN
	.globl	_LPWMINV
	.globl	_SPICK
	.globl	_SYSC2
	.globl	_HWPSEL
	.globl	_HWPAL
	.globl	_HWPAH
	.globl	_HWPA
	.globl	_HWD
	.globl	_HWDINC
	.globl	_PATEN
	.globl	_PBTEN
	.globl	_PABTEN
	.globl	_TRAMAL
	.globl	_TRAMAH
	.globl	_TRAMBUFP
	.globl	_PASKIP
	.globl	_PBSKIP
	.globl	_PABSKIP
	.globl	_PATR
	.globl	_PBTR
	.globl	_PABTR
	.globl	_TOUCHC
	.globl	_DUMMYC
	.globl	_IRCD
	.globl	_FPWMEN
	.globl	_FPWMDUTY
	.globl	_FPWMPER
	.globl	_DACGCL
	.globl	_RECPWR
	.globl	_ICE0
	.globl	_ICE1
	.globl	_ICE2
	.globl	_ICE3
	.globl	_ICE4
	.globl	_RAMP0
	.globl	_RAMP0INC
	.globl	_RAMP1
	.globl	_RDMALH
	.globl	_HWPALH
	.globl	_RAMP1INC
	.globl	_RAMP1INC2
	.globl	_ROMP
	.globl	_ROMPINC
	.globl	_ROMPINC2
	.globl	_ACC
	.globl	_RAMP0UW
	.globl	_RAMP1UW
	.globl	_ROMPLH
	.globl	_ROMPUW
	.globl	_OFFSETLH
	.globl	_ADP_VPLH
	.globl	_TOV
	.globl	_EA
	.globl	_OV
	.globl	_STACKL
	.globl	_STACKH

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_longjmp
	.globl	___setjmp
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__setjmp_0	udata
r0x1165:	.ds	1
r0x1173:	.ds	1
___ramp1save:	.ds	2
___ramp0save:	.ds	2
_rv0:	.ds	2
_stkchk:	.ds	2
	.area DSEG (DATA); (local stack unassigned) 
___setjmp_STK00:	.ds	1
	.globl ___setjmp_STK00
___setjmp_STK01:	.ds	1
	.globl ___setjmp_STK01
___setjmp_STK02:	.ds	1
	.globl ___setjmp_STK02
_longjmp_STK00:	.ds	1
	.globl _longjmp_STK00
_longjmp_STK01:	.ds	1
	.globl _longjmp_STK01
_longjmp_STK02:	.ds	1
	.globl _longjmp_STK02
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1169:NULL+0:-1:1
	;--cdb--W:r0x1165:NULL+0:-1:1
	;--cdb--W:r0x1168:NULL+0:4458:0
	;--cdb--W:r0x1169:NULL+0:4459:0
	;--cdb--W:r0x116B:NULL+0:4456:0
	;--cdb--W:r0x116A:NULL+0:0:0
	;--cdb--W:r0x116A:NULL+0:-1:1
	;--cdb--W:r0x1168:NULL+0:-1:1
	end
