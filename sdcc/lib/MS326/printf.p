pcode dump


	New pBlock

CSEG	 (CODE), dbName =C
;; Starting pCode block
_sprintf:	;Function start
; 0 exit points
;	.line	218; "printf.c"	void sprintf(char* s,char *fmt, ...)
	LDA	_RAMP1L
	PUSH	
	LDA	_RAMP1H
	PUSH	
	P02P1	
	PUSH	
	PUSH	
	PUSH	
	PUSH	
;	.line	221; "printf.c"	va_start(va,fmt);
	LDA	#0xfa
	ADD	_RAMP1L
	STA	@_RAMP1
	LDA	#0x7f
	ADDC	_RAMP1H
	STA	@P1,1
	LDA	@_RAMP1
	STA	@P1,2
;	.line	222; "printf.c"	tfp_format(&s,putcp,fmt,va);
	LDA	#0xfc
	ADD	_RAMP1L
	STA	@_RAMP1
	LDA	#0x7f
	ADDC	_RAMP1H
	STA	@P1,3
	LDA	@P1,2
	PUSH	
	LDA	@P1,1
	PUSH	
	LDA	@P1,-6
	PUSH	
	LDA	@P1,-5
	PUSH	
	LDA	_putcp
	PUSH	
	PUSH	
	LDA	@_RAMP1
	PUSH	
	LDA	@P1,3
	PUSH	
	BANK	_tfp_format
	CALL	_tfp_format
;	.line	223; "printf.c"	putcp(&s,0);
	CLRA	
	PUSH	
	LDA	@_RAMP1
	PUSH	
	LDA	@P1,3
	PUSH	
	BANK	_putcp
	CALL	_putcp
	POP	
	POP	
	POP	
	POP	
	POP	
	STA	_RAMP1H
	POP	
	STA	_RAMP1L
	RET	

	New pBlock

CSEG	 (CODE), dbName =C
;; Starting pCode block
_putcp:	;Function start
; 0 exit points
;	.line	209; "printf.c"	void putcp(void* p,char c)
	LDA	_RAMP1L
	PUSH	
	LDA	_RAMP1H
	PUSH	
	P02P1	
	PUSH	
	PUSH	
	PUSH	
	PUSH	
;	.line	211; "printf.c"	*(*((char**)p))++ = c;
	LDA	@P1,-4
	STA	_ROMPL
	LDA	@P1,-3
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@_RAMP1
	LDA	@_ROMPINC
	STA	@P1,1
	LDA	@_RAMP1
	INCA	
	STA	@P1,2
	CLRA	
	ADDC	@P1,1
	STA	@P1,3
	LDA	@P1,-3
	STA	_ROMPH
	LDA	@P1,-4
	STA	_ROMPL
	LDA	@P1,2
	STA	@_ROMPINC
	LDA	@P1,3
	STA	@_ROMPINC
	LDA	@P1,1
	STA	_ROMPH
	LDA	@_RAMP1
	STA	_ROMPL
	LDA	@P1,-5
	STA	@_ROMPINC
	POP	
	POP	
	POP	
	POP	
	POP	
	STA	_RAMP1H
	POP	
	STA	_RAMP1L
	LDA	#0xFD
	BANK	__sp_dec
	JMP	__sp_dec

	New pBlock

CSEG	 (CODE), dbName =C
;; Starting pCode block
_printf:	;Function start
; 0 exit points
;	.line	199; "printf.c"	void printf(char *fmt, ...)
	LDA	_RAMP1L
	PUSH	
	LDA	_RAMP1H
	PUSH	
	P02P1	
	PUSH	
	PUSH	
;	.line	202; "printf.c"	if(stdout_putf==NULL)
	LDA	_stdout_putf
	ORA	(_stdout_putf + 1)
	JNZ	_00444_DS_
;	.line	203; "printf.c"	stdout_putf=putch;
	LDA	#(_putch + 0)
	STA	_ROMPL
	LDA	#> (_putch + 0)
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_stdout_putf
	STA	(_stdout_putf + 1)
_00444_DS_:
;	.line	204; "printf.c"	va_start(va,fmt);
	LDA	#0xfc
	ADD	_RAMP1L
	STA	@_RAMP1
	LDA	#0x7f
	ADDC	_RAMP1H
	STA	@P1,1
;	.line	205; "printf.c"	tfp_format(stdout_putp,stdout_putf,fmt,va);
	LDA	@_RAMP1
	PUSH	
	LDA	@P1,1
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	LDA	_stdout_putf
	PUSH	
	LDA	(_stdout_putf + 1)
	PUSH	
	LDA	_stdout_putp
	PUSH	
	LDA	(_stdout_putp + 1)
	PUSH	
	BANK	_tfp_format
	CALL	_tfp_format
	POP	
	POP	
	POP	
	STA	_RAMP1H
	POP	
	STA	_RAMP1L
	RET	

	New pBlock

CSEG	 (CODE), dbName =C
;; Starting pCode block
_tfp_format:	;Function start
; 0 exit points
;	.line	113; "printf.c"	void tfp_format(void* putp,putcf putf,char *fmt, va_list va)
	LDA	_RAMP1L
	PUSH	
	LDA	_RAMP1H
	PUSH	
	P02P1	
	LDA	#0x0A
	BANK	__sp_inc
	CALL	__sp_inc
	LDA	@P1,-7
	STA	(_tfp_format_fmt_1_26 + 1)
	LDA	@P1,-8
	STA	_tfp_format_fmt_1_26
_00358_DS_:
;	.line	120; "printf.c"	while ((ch=*(fmt++))) {
	LDA	_tfp_format_fmt_1_26
	STA	_ROMPL
	LDA	(_tfp_format_fmt_1_26 + 1)
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@_RAMP1
	LDA	_tfp_format_fmt_1_26
	INCA	
	STA	_tfp_format_fmt_1_26
	CLRA	
	ADDC	(_tfp_format_fmt_1_26 + 1)
	STA	(_tfp_format_fmt_1_26 + 1)
	LDA	@_RAMP1
	STA	@P1,1
	JNZ	_00009_DS_
	BANK	_00362_DS_
	JMP	_00362_DS_
_00009_DS_:
;	.line	121; "printf.c"	if (ch!='%') 
	LDA	@P1,1
	XOR	#0x25
;	.line	122; "printf.c"	putf(putp,ch);
	JZ	_00356_DS_
	LDA	@P1,1
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	_00420_DS_
	JMP	_00420_DS_
_00419_DS_:
	BANK	_00421_DS_
	CALL	_00421_DS_
_00421_DS_:
	LDA	@P1,-6
	STA	_STACKL
	LDA	@P1,-5
	STA	_STACKH
	RET	
_00420_DS_:
	BANK	_00419_DS_
	CALL	_00419_DS_
	BANK	_00358_DS_
	JMP	_00358_DS_
_00356_DS_:
;	.line	124; "printf.c"	char lz=0;
	CLRA	
;	.line	126; "printf.c"	char lng=0;
	STA	@P1,2
;	.line	128; "printf.c"	int w=0;
	STA	@_RAMP1
	STA	_tfp_format_w_3_29
	STA	(_tfp_format_w_3_29 + 1)
;	.line	129; "printf.c"	ch=*(fmt++);
	LDA	_tfp_format_fmt_1_26
	STA	_ROMPL
	LDA	(_tfp_format_fmt_1_26 + 1)
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@P1,1
	LDA	_tfp_format_fmt_1_26
	INCA	
	STA	_tfp_format_fmt_1_26
	CLRA	
	ADDC	(_tfp_format_fmt_1_26 + 1)
	STA	(_tfp_format_fmt_1_26 + 1)
;	.line	130; "printf.c"	if (ch=='0') {
	LDA	@P1,1
	XOR	#0x30
	JNZ	_00330_DS_
;	.line	131; "printf.c"	ch=*(fmt++);
	LDA	_tfp_format_fmt_1_26
	STA	_ROMPL
	LDA	(_tfp_format_fmt_1_26 + 1)
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@P1,1
	LDA	_tfp_format_fmt_1_26
	INCA	
	STA	_tfp_format_fmt_1_26
	CLRA	
	ADDC	(_tfp_format_fmt_1_26 + 1)
	STA	(_tfp_format_fmt_1_26 + 1)
;	.line	132; "printf.c"	lz=1;
	LDA	#0x01
	STA	@P1,2
_00330_DS_:
;	.line	134; "printf.c"	if (ch>='0' && ch<='9') {
	LDA	@P1,1
	XOR	#0x80
	ADD	#0x50
	JNC	_00332_DS_
	SETB	_C
	LDA	#0x39
	SUBSI	
	SUBB	@P1,1
	JNC	_00332_DS_
;	.line	135; "printf.c"	ch=a2i(ch,&fmt,10,&w);
	LDA	#(_tfp_format_w_3_29 + 0)
	PUSH	
	LDA	#> (_tfp_format_w_3_29 + 0)
	PUSH	
	LDA	#0x0a
	PUSH	
	CLRA	
	PUSH	
	LDA	#(_tfp_format_fmt_1_26 + 0)
	PUSH	
	LDA	#> (_tfp_format_fmt_1_26 + 0)
	PUSH	
	LDA	@P1,1
	PUSH	
	BANK	_a2i
	CALL	_a2i
	STA	@P1,1
_00332_DS_:
;	.line	138; "printf.c"	if (ch=='l') {
	LDA	@P1,1
	XOR	#0x6c
	JNZ	_00335_DS_
;	.line	139; "printf.c"	ch=*(fmt++);
	LDA	_tfp_format_fmt_1_26
	STA	_ROMPL
	LDA	(_tfp_format_fmt_1_26 + 1)
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@P1,1
	LDA	_tfp_format_fmt_1_26
	INCA	
	STA	_tfp_format_fmt_1_26
	CLRA	
	ADDC	(_tfp_format_fmt_1_26 + 1)
	STA	(_tfp_format_fmt_1_26 + 1)
;	.line	140; "printf.c"	lng=1;
	LDA	#0x01
	STA	@_RAMP1
_00335_DS_:
;	.line	143; "printf.c"	switch (ch) {
	LDA	@P1,1
	JNZ	_00010_DS_
	BANK	_00362_DS_
	JMP	_00362_DS_
_00010_DS_:
	LDA	@P1,1
	XOR	#0x25
	JNZ	_00008_DS_
	BANK	_00352_DS_
	JMP	_00352_DS_
_00008_DS_:
	LDA	@P1,1
	XOR	#0x58
	LDC	_Z
	CLRA	
	ROL	
	STA	@P1,3
	ROR	
	LDA	@P1,3
	JZ	_00004_DS_
	BANK	_00346_DS_
	JMP	_00346_DS_
_00004_DS_:
	LDA	@P1,1
	XOR	#0x63
	JNZ	_00007_DS_
	BANK	_00350_DS_
	JMP	_00350_DS_
_00007_DS_:
	LDA	@P1,1
	XOR	#0x64
	JZ	_00341_DS_
	LDA	@P1,1
	XOR	#0x73
	JNZ	_00001_DS_
	BANK	_00351_DS_
	JMP	_00351_DS_
_00001_DS_:
	LDA	@P1,1
	XOR	#0x75
	JZ	_00337_DS_
	LDA	@P1,1
	XOR	#0x78
	JNZ	_00002_DS_
	BANK	_00346_DS_
	JMP	_00346_DS_
_00002_DS_:
	BANK	_00358_DS_
	JMP	_00358_DS_
_00337_DS_:
;	.line	148; "printf.c"	if (lng)
	LDA	@_RAMP1
	JZ	_00339_DS_
;	.line	149; "printf.c"	uli2a(va_arg(va, unsigned long int),10,0,bf);
	LDA	#0xfc
	ADD	@P1,-10
	STA	@P1,4
	LDA	#0xff
	ADDC	@P1,-9
	STA	@P1,5
	LDA	@P1,4
	STA	@P1,-10
	LDA	@P1,5
	STA	@P1,-9
	LDA	@P1,4
	STA	_ROMPL
	LDA	@P1,5
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@P1,4
	LDA	@_ROMPINC
	STA	@P1,5
	LDA	@_ROMPINC
	STA	@P1,6
	LDA	@_ROMPINC
	STA	@P1,7
	LDA	#(_tfp_format_bf_1_27 + 0)
	PUSH	
	LDA	#> (_tfp_format_bf_1_27 + 0)
	PUSH	
	CLRA	
	PUSH	
	PUSH	
	LDA	#0x0a
	PUSH	
	CLRA	
	PUSH	
	LDA	@P1,4
	PUSH	
	LDA	@P1,5
	PUSH	
	LDA	@P1,6
	PUSH	
	LDA	@P1,7
	PUSH	
	BANK	_uli2a
	CALL	_uli2a
	BANK	_00340_DS_
	JMP	_00340_DS_
_00339_DS_:
;	.line	152; "printf.c"	ui2a(va_arg(va, unsigned int),10,0,bf);
	LDA	#0xfe
	ADD	@P1,-10
	STA	@P1,4
	LDA	#0xff
	ADDC	@P1,-9
	STA	@P1,5
	LDA	@P1,4
	STA	@P1,-10
	LDA	@P1,5
	STA	@P1,-9
	LDA	@P1,4
	STA	_ROMPL
	LDA	@P1,5
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@P1,4
	LDA	@_ROMPINC
	STA	@P1,5
	LDA	#(_tfp_format_bf_1_27 + 0)
	PUSH	
	LDA	#> (_tfp_format_bf_1_27 + 0)
	PUSH	
	CLRA	
	PUSH	
	PUSH	
	LDA	#0x0a
	PUSH	
	CLRA	
	PUSH	
	LDA	@P1,4
	PUSH	
	LDA	@P1,5
	PUSH	
	BANK	_ui2a
	CALL	_ui2a
_00340_DS_:
;	.line	153; "printf.c"	putchw(putp,putf,w,lz,bf);
	LDA	#(_tfp_format_bf_1_27 + 0)
	PUSH	
	LDA	#> (_tfp_format_bf_1_27 + 0)
	PUSH	
	LDA	@P1,2
	PUSH	
	LDA	_tfp_format_w_3_29
	PUSH	
	LDA	(_tfp_format_w_3_29 + 1)
	PUSH	
	LDA	@P1,-6
	PUSH	
	LDA	@P1,-5
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	_putchw
	CALL	_putchw
	BANK	_00358_DS_
;	.line	154; "printf.c"	break;
	JMP	_00358_DS_
_00341_DS_:
;	.line	158; "printf.c"	if (lng)
	LDA	@_RAMP1
	JNZ	_00003_DS_
	BANK	_00343_DS_
	JMP	_00343_DS_
_00003_DS_:
;	.line	159; "printf.c"	li2a(va_arg(va, unsigned long int),bf);
	LDA	#0xfc
	ADD	@P1,-10
	STA	@P1,4
	LDA	#0xff
	ADDC	@P1,-9
	STA	@P1,5
	LDA	@P1,4
	STA	@P1,-10
	LDA	@P1,5
	STA	@P1,-9
	LDA	@P1,4
	STA	_ROMPL
	LDA	@P1,5
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@P1,4
	LDA	@_ROMPINC
	STA	@P1,5
	LDA	@_ROMPINC
	STA	@P1,6
	LDA	@_ROMPINC
	STA	@P1,7
	LDA	#(_tfp_format_bf_1_27 + 0)
	PUSH	
	LDA	#> (_tfp_format_bf_1_27 + 0)
	PUSH	
	LDA	@P1,4
	PUSH	
	LDA	@P1,5
	PUSH	
	LDA	@P1,6
	PUSH	
	LDA	@P1,7
	PUSH	
	BANK	_li2a
	CALL	_li2a
	BANK	_00344_DS_
	JMP	_00344_DS_
_00343_DS_:
;	.line	162; "printf.c"	i2a(va_arg(va, int),bf);
	LDA	#0xfe
	ADD	@P1,-10
	STA	@P1,4
	LDA	#0xff
	ADDC	@P1,-9
	STA	@P1,5
	LDA	@P1,4
	STA	@P1,-10
	LDA	@P1,5
	STA	@P1,-9
	LDA	@P1,4
	STA	_ROMPL
	LDA	@P1,5
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@P1,4
	LDA	@_ROMPINC
	STA	@P1,5
	LDA	#(_tfp_format_bf_1_27 + 0)
	PUSH	
	LDA	#> (_tfp_format_bf_1_27 + 0)
	PUSH	
	LDA	@P1,4
	PUSH	
	LDA	@P1,5
	PUSH	
	BANK	_i2a
	CALL	_i2a
_00344_DS_:
;	.line	163; "printf.c"	putchw(putp,putf,w,lz,bf);
	LDA	#(_tfp_format_bf_1_27 + 0)
	PUSH	
	LDA	#> (_tfp_format_bf_1_27 + 0)
	PUSH	
	LDA	@P1,2
	PUSH	
	LDA	_tfp_format_w_3_29
	PUSH	
	LDA	(_tfp_format_w_3_29 + 1)
	PUSH	
	LDA	@P1,-6
	PUSH	
	LDA	@P1,-5
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	_putchw
	CALL	_putchw
	BANK	_00358_DS_
;	.line	164; "printf.c"	break;
	JMP	_00358_DS_
_00346_DS_:
;	.line	168; "printf.c"	if (lng)
	LDA	@_RAMP1
	JNZ	_00005_DS_
	BANK	_00348_DS_
	JMP	_00348_DS_
_00005_DS_:
;	.line	169; "printf.c"	uli2a(va_arg(va, unsigned long int),16,(ch=='X'),bf);
	LDA	#0xfc
	ADD	@P1,-10
	STA	@_RAMP1
	LDA	#0xff
	ADDC	@P1,-9
	STA	@P1,4
	LDA	@_RAMP1
	STA	@P1,-10
	LDA	@P1,4
	STA	@P1,-9
	LDA	@_RAMP1
	STA	_ROMPL
	LDA	@P1,4
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@_RAMP1
	LDA	@_ROMPINC
	STA	@P1,4
	LDA	@_ROMPINC
	STA	@P1,8
	LDA	@_ROMPINC
	STA	@P1,6
	LDA	@P1,3
	STA	@P1,5
	JPL	_00429_DS_
	LDA	#0xff
	BANK	_00430_DS_
	JMP	_00430_DS_
_00429_DS_:
	CLRA	
_00430_DS_:
	STA	@P1,9
	LDA	#(_tfp_format_bf_1_27 + 0)
	PUSH	
	LDA	#> (_tfp_format_bf_1_27 + 0)
	PUSH	
	LDA	@P1,5
	PUSH	
	LDA	@P1,9
	PUSH	
	LDA	#0x10
	PUSH	
	CLRA	
	PUSH	
	LDA	@_RAMP1
	PUSH	
	LDA	@P1,4
	PUSH	
	LDA	@P1,8
	PUSH	
	LDA	@P1,6
	PUSH	
	BANK	_uli2a
	CALL	_uli2a
	BANK	_00349_DS_
	JMP	_00349_DS_
_00348_DS_:
;	.line	172; "printf.c"	ui2a(va_arg(va, unsigned int),16,(ch=='X'),bf);
	LDA	#0xfe
	ADD	@P1,-10
	STA	@_RAMP1
	LDA	#0xff
	ADDC	@P1,-9
	STA	@P1,4
	LDA	@_RAMP1
	STA	@P1,-10
	LDA	@P1,4
	STA	@P1,-9
	LDA	@_RAMP1
	STA	_ROMPL
	LDA	@P1,4
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@_RAMP1
	LDA	@_ROMPINC
	STA	@P1,4
	LDA	@P1,3
	STA	@P1,5
	JMI	_00006_DS_
	BANK	_00431_DS_
	JMP	_00431_DS_
_00006_DS_:
	LDA	#0xff
	BANK	_00432_DS_
	JMP	_00432_DS_
_00431_DS_:
	CLRA	
_00432_DS_:
	STA	@P1,9
	LDA	#(_tfp_format_bf_1_27 + 0)
	PUSH	
	LDA	#> (_tfp_format_bf_1_27 + 0)
	PUSH	
	LDA	@P1,5
	PUSH	
	LDA	@P1,9
	PUSH	
	LDA	#0x10
	PUSH	
	CLRA	
	PUSH	
	LDA	@_RAMP1
	PUSH	
	LDA	@P1,4
	PUSH	
	BANK	_ui2a
	CALL	_ui2a
_00349_DS_:
;	.line	173; "printf.c"	putchw(putp,putf,w,lz,bf);
	LDA	#(_tfp_format_bf_1_27 + 0)
	PUSH	
	LDA	#> (_tfp_format_bf_1_27 + 0)
	PUSH	
	LDA	@P1,2
	PUSH	
	LDA	_tfp_format_w_3_29
	PUSH	
	LDA	(_tfp_format_w_3_29 + 1)
	PUSH	
	LDA	@P1,-6
	PUSH	
	LDA	@P1,-5
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	_putchw
	CALL	_putchw
	BANK	_00358_DS_
;	.line	174; "printf.c"	break;
	JMP	_00358_DS_
_00350_DS_:
;	.line	176; "printf.c"	putf(putp,(char)(va_arg(va, int)));
	LDA	#0xfe
	ADD	@P1,-10
	STA	@P1,2
	LDA	#0xff
	ADDC	@P1,-9
	STA	@_RAMP1
	LDA	@P1,2
	STA	@P1,-10
	LDA	@_RAMP1
	STA	@P1,-9
	LDA	@P1,2
	STA	_ROMPL
	LDA	@_RAMP1
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@P1,2
	LDA	@_ROMPINC
	LDA	@P1,2
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	_00434_DS_
	JMP	_00434_DS_
_00433_DS_:
	BANK	_00435_DS_
	CALL	_00435_DS_
_00435_DS_:
	LDA	@P1,-6
	STA	_STACKL
	LDA	@P1,-5
	STA	_STACKH
	RET	
_00434_DS_:
	BANK	_00433_DS_
	CALL	_00433_DS_
	BANK	_00358_DS_
;	.line	177; "printf.c"	break;
	JMP	_00358_DS_
_00351_DS_:
;	.line	179; "printf.c"	putchw(putp,putf,w,0,va_arg(va, char*));
	LDA	#0xfe
	ADD	@P1,-10
	STA	@P1,2
	LDA	#0xff
	ADDC	@P1,-9
	STA	@_RAMP1
	LDA	@P1,2
	STA	@P1,-10
	LDA	@_RAMP1
	STA	@P1,-9
	LDA	@P1,2
	STA	_ROMPL
	LDA	@_RAMP1
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@P1,2
	LDA	@_ROMPINC
	STA	@_RAMP1
	LDA	@P1,2
	PUSH	
	LDA	@_RAMP1
	PUSH	
	CLRA	
	PUSH	
	LDA	_tfp_format_w_3_29
	PUSH	
	LDA	(_tfp_format_w_3_29 + 1)
	PUSH	
	LDA	@P1,-6
	PUSH	
	LDA	@P1,-5
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	_putchw
	CALL	_putchw
	BANK	_00358_DS_
;	.line	180; "printf.c"	break;
	JMP	_00358_DS_
_00352_DS_:
;	.line	182; "printf.c"	putf(putp,ch);
	LDA	@P1,1
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	_00437_DS_
	JMP	_00437_DS_
_00436_DS_:
	BANK	_00438_DS_
	CALL	_00438_DS_
_00438_DS_:
	LDA	@P1,-6
	STA	_STACKL
	LDA	@P1,-5
	STA	_STACKH
	RET	
_00437_DS_:
	BANK	_00436_DS_
	CALL	_00436_DS_
	BANK	_00358_DS_
;	.line	185; "printf.c"	}
	JMP	_00358_DS_
_00362_DS_:
;	.line	188; "printf.c"	abort:;
	LDA	#0xF6
	BANK	__sp_dec
	CALL	__sp_dec
	POP	
	STA	_RAMP1H
	POP	
	STA	_RAMP1L
	LDA	#0xF8
	BANK	__sp_dec
	JMP	__sp_dec

	New pBlock

CSEG	 (CODE), dbName =C
;; Starting pCode block
_putchw:	;Function start
; 0 exit points
;	.line	100; "printf.c"	static void putchw(void* putp,putcf putf,int n, char z, char* bf)
	LDA	_RAMP1L
	PUSH	
	LDA	_RAMP1H
	PUSH	
	P02P1	
	PUSH	
	PUSH	
	PUSH	
	PUSH	
;	.line	102; "printf.c"	char fc=z? '0' : ' ';
	LDA	@P1,-9
	JZ	_00288_DS_
	LDA	#0x30
	STA	@_RAMP1
	BANK	_00289_DS_
	JMP	_00289_DS_
_00288_DS_:
	LDA	#0x20
	STA	@_RAMP1
_00289_DS_:
;	.line	105; "printf.c"	while (*p++ && n > 0)
	LDA	@P1,-11
	STA	@P1,1
	LDA	@P1,-10
	STA	@P1,2
_00277_DS_:
	LDA	@P1,1
	STA	_ROMPL
	LDA	@P1,2
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@P1,3
	LDA	@P1,1
	INCA	
	STA	@P1,1
	CLRA	
	ADDC	@P1,2
	STA	@P1,2
	LDA	@P1,3
	JZ	_00280_DS_
	SETB	_C
	CLRA	
	SUBB	@P1,-8
	CLRA	
	SUBSI	
	SUBB	@P1,-7
	JC	_00280_DS_
;	.line	106; "printf.c"	n--;
	LDA	@P1,-8
	DECA	
	STA	@P1,-8
	LDA	#0xff
	ADDC	@P1,-7
	STA	@P1,-7
	BANK	_00277_DS_
	JMP	_00277_DS_
_00280_DS_:
;	.line	107; "printf.c"	while (n-- > 0) 
	LDA	@P1,-8
	STA	@P1,1
	LDA	@P1,-7
	STA	@P1,2
	LDA	@P1,-8
	DECA	
	STA	@P1,-8
	LDA	#0xff
	ADDC	@P1,-7
	STA	@P1,-7
	SETB	_C
	CLRA	
	SUBB	@P1,1
	CLRA	
	SUBSI	
	SUBB	@P1,2
	JC	_00297_DS_
;	.line	108; "printf.c"	putf(putp,fc);
	LDA	@_RAMP1
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	_00320_DS_
	JMP	_00320_DS_
_00319_DS_:
	BANK	_00321_DS_
	CALL	_00321_DS_
_00321_DS_:
	LDA	@P1,-6
	STA	_STACKL
	LDA	@P1,-5
	STA	_STACKH
	RET	
_00320_DS_:
	BANK	_00319_DS_
	CALL	_00319_DS_
	BANK	_00280_DS_
	JMP	_00280_DS_
_00297_DS_:
;	.line	109; "printf.c"	while ((ch= *bf++))
	LDA	@P1,-11
	STA	@P1,-8
	LDA	@P1,-10
	STA	@P1,-7
_00283_DS_:
	LDA	@P1,-8
	STA	_ROMPL
	LDA	@P1,-7
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@_RAMP1
	LDA	@P1,-8
	INCA	
	STA	@P1,-8
	CLRA	
	ADDC	@P1,-7
	STA	@P1,-7
	LDA	@_RAMP1
	STA	@P1,-11
	JZ	_00286_DS_
;	.line	110; "printf.c"	putf(putp,ch);
	LDA	@P1,-11
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	_00323_DS_
	JMP	_00323_DS_
_00322_DS_:
	BANK	_00324_DS_
	CALL	_00324_DS_
_00324_DS_:
	LDA	@P1,-6
	STA	_STACKL
	LDA	@P1,-5
	STA	_STACKH
	RET	
_00323_DS_:
	BANK	_00322_DS_
	CALL	_00322_DS_
	BANK	_00283_DS_
	JMP	_00283_DS_
_00286_DS_:
	POP	
	POP	
	POP	
	POP	
	POP	
	STA	_RAMP1H
	POP	
	STA	_RAMP1L
	LDA	#0xF7
	BANK	__sp_dec
	JMP	__sp_dec

	New pBlock

CSEG	 (CODE), dbName =C
;; Starting pCode block
_a2i:	;Function start
; 2 exit points
;	.line	85; "printf.c"	static char a2i(char ch, char** src,int base,int* nump)
	LDA	_RAMP1L
	PUSH	
	LDA	_RAMP1H
	PUSH	
	P02P1	
	LDA	#0x07
	BANK	__sp_inc
	CALL	__sp_inc
;	.line	87; "printf.c"	char* p= *src;
	LDA	@P1,-5
	STA	_ROMPL
	LDA	@P1,-4
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@_RAMP1
	LDA	@_ROMPINC
	STA	@P1,1
;	.line	88; "printf.c"	int num=0;
	CLRA	
	STA	@P1,2
	STA	@P1,3
_00269_DS_:
;	.line	90; "printf.c"	while ((digit=a2d(ch))>=0) {
	LDA	@P1,-3
	PUSH	
	BANK	_a2d
	CALL	_a2d
	STA	@P1,4
	LDA	STK00
	STA	@P1,5
	LDA	@P1,4
	STA	@P1,6
	XOR	#0x80
	ROL	
	JNC	_00271_DS_
;	.line	91; "printf.c"	if (digit>base) break;
	SETB	_C
	LDA	@P1,-7
	SUBB	@P1,5
	LDA	@P1,-6
	SUBSI	
	SUBB	@P1,6
	JNC	_00271_DS_
;	.line	92; "printf.c"	num=num*base+digit;
	LDA	@P1,-7
	PUSH	
	LDA	@P1,-6
	PUSH	
	LDA	@P1,2
	PUSH	
	LDA	@P1,3
	PUSH	
	BANK	__mulint
	CALL	__mulint
	STA	@P1,4
	LDA	STK00
	ADD	@P1,5
	STA	@P1,2
	LDA	@P1,4
	ADDC	@P1,6
	STA	@P1,3
;	.line	93; "printf.c"	ch=*p++;
	LDA	@_RAMP1
	STA	_ROMPL
	LDA	@P1,1
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	@P1,-3
	LDA	@_RAMP1
	INCA	
	STA	@_RAMP1
	CLRA	
	ADDC	@P1,1
	STA	@P1,1
	BANK	_00269_DS_
	JMP	_00269_DS_
_00271_DS_:
;	.line	95; "printf.c"	*src=p;
	LDA	@P1,-4
	STA	_ROMPH
	LDA	@P1,-5
	STA	_ROMPL
	LDA	@_RAMP1
	STA	@_ROMPINC
	LDA	@P1,1
	STA	@_ROMPINC
;	.line	96; "printf.c"	*nump=num;
	LDA	@P1,-8
	STA	_ROMPH
	LDA	@P1,-9
	STA	_ROMPL
	LDA	@P1,2
	STA	@_ROMPINC
	LDA	@P1,3
	STA	@_ROMPINC
;	.line	97; "printf.c"	return ch;
	LDA	@P1,-3
	STA	_PTRCL
	LDA	#0xF9
	BANK	__sp_dec
	CALL	__sp_dec
	POP	
	STA	_RAMP1H
	POP	
	STA	_RAMP1L
	LDA	#0xF9
	BANK	__sp_dec
	CALL	__sp_dec
	LDA	_PTRCL
	RET	
; exit point of _a2i

	New pBlock

CSEG	 (CODE), dbName =C
;; Starting pCode block
_a2d:	;Function start
; 2 exit points
;	.line	74; "printf.c"	static int a2d(char ch)
	LDA	_RAMP1L
	PUSH	
	LDA	_RAMP1H
	PUSH	
	P02P1	
	PUSH	
	PUSH	
	PUSH	
	LDA	@P1,-3
	XOR	#0x80
	ADD	#0x50
	JNC	_00234_DS_
	SETB	_C
	LDA	#0x39
	SUBSI	
	SUBB	@P1,-3
	JNC	_00234_DS_
;	.line	77; "printf.c"	return ch-'0';
	LDA	@P1,-3
	STA	@_RAMP1
	JPL	_00257_DS_
	LDA	#0xff
	BANK	_00258_DS_
	JMP	_00258_DS_
_00257_DS_:
	CLRA	
_00258_DS_:
	STA	@P1,1
	LDA	#0xd0
	ADD	@_RAMP1
	STA	@_RAMP1
	LDA	#0xff
	ADDC	@P1,1
	STA	@P1,1
	LDA	@_RAMP1
	STA	STK00
	LDA	@P1,1
	BANK	_00237_DS_
	JMP	_00237_DS_
_00234_DS_:
;	.line	78; "printf.c"	else if (ch>='a' && ch<='f')
	LDA	@P1,-3
	XOR	#0x80
	ADD	#0x1f
	JNC	_00230_DS_
	SETB	_C
	LDA	#0x66
	SUBSI	
	SUBB	@P1,-3
	JNC	_00230_DS_
;	.line	79; "printf.c"	return ch-'a'+10;
	LDA	@P1,-3
	STA	@_RAMP1
	JPL	_00259_DS_
	LDA	#0xff
	BANK	_00260_DS_
	JMP	_00260_DS_
_00259_DS_:
	CLRA	
_00260_DS_:
	STA	@P1,1
	LDA	#0xa9
	ADD	@_RAMP1
	STA	@_RAMP1
	LDA	#0xff
	ADDC	@P1,1
	STA	@P1,1
	LDA	@_RAMP1
	STA	STK00
	LDA	@P1,1
	BANK	_00237_DS_
	JMP	_00237_DS_
_00230_DS_:
;	.line	80; "printf.c"	else if (ch>='A' && ch<='F')
	LDA	@P1,-3
	XOR	#0x80
	ADD	#0x3f
	JNC	_00226_DS_
	SETB	_C
	LDA	#0x46
	SUBSI	
	SUBB	@P1,-3
	JNC	_00226_DS_
;	.line	81; "printf.c"	return ch-'A'+10;
	LDA	@P1,-3
	STA	@_RAMP1
	JPL	_00261_DS_
	LDA	#0xff
	BANK	_00262_DS_
	JMP	_00262_DS_
_00261_DS_:
	CLRA	
_00262_DS_:
	STA	@P1,1
	LDA	#0xc9
	ADD	@_RAMP1
	STA	@P1,-3
	LDA	#0xff
	ADDC	@P1,1
	STA	@P1,2
	LDA	@P1,-3
	STA	STK00
	LDA	@P1,2
	BANK	_00237_DS_
	JMP	_00237_DS_
_00226_DS_:
;	.line	82; "printf.c"	else return -1;
	LDA	#0xff
	STA	STK00
_00237_DS_:
	STA	_PTRCL
	POP	
	POP	
	POP	
	POP	
	STA	_RAMP1H
	POP	
	STA	_RAMP1L
	POP	
	LDA	_PTRCL
	RET	
; exit point of _a2d

	New pBlock

CSEG	 (CODE), dbName =C
;; Starting pCode block
_i2a:	;Function start
; 0 exit points
;	.line	65; "printf.c"	static void i2a (int num, char * bf)
	LDA	_RAMP1L
	PUSH	
	LDA	_RAMP1H
	PUSH	
	P02P1	
;	.line	67; "printf.c"	if (num<0) {
	LDA	@P1,-3
	XOR	#0x80
	ROL	
	JC	_00214_DS_
;	.line	68; "printf.c"	num=-num;
	SETB	_C
	CLRA	
	SUBB	@P1,-4
	STA	@P1,-4
	CLRA	
	SUBB	@P1,-3
	STA	@P1,-3
;	.line	69; "printf.c"	*bf++ = '-';
	LDA	@P1,-5
	STA	_ROMPH
	LDA	@P1,-6
	STA	_ROMPL
	LDA	#0x2d
	STA	@_ROMPINC
	LDA	@P1,-6
	INCA	
	STA	@P1,-6
	CLRA	
	ADDC	@P1,-5
	STA	@P1,-5
_00214_DS_:
;	.line	71; "printf.c"	ui2a(num,10,0,bf);
	LDA	@P1,-6
	PUSH	
	LDA	@P1,-5
	PUSH	
	CLRA	
	PUSH	
	PUSH	
	LDA	#0x0a
	PUSH	
	CLRA	
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	_ui2a
	CALL	_ui2a
	POP	
	STA	_RAMP1H
	POP	
	STA	_RAMP1L
	LDA	#0xFC
	BANK	__sp_dec
	JMP	__sp_dec

	New pBlock

CSEG	 (CODE), dbName =C
;; Starting pCode block
_ui2a:	;Function start
; 0 exit points
;	.line	47; "printf.c"	static void ui2a(unsigned int num, unsigned int base, int uc,char * bf)
	LDA	_RAMP1L
	PUSH	
	LDA	_RAMP1H
	PUSH	
	P02P1	
	LDA	#0x09
	BANK	__sp_inc
	CALL	__sp_inc
;	.line	50; "printf.c"	unsigned int d=1;
	LDA	#0x01
	STA	@_RAMP1
	CLRA	
	STA	@P1,1
_00167_DS_:
;	.line	51; "printf.c"	while (num/d >= base)
	LDA	@_RAMP1
	PUSH	
	LDA	@P1,1
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	__divuint
	CALL	__divuint
	STA	@P1,2
	LDA	STK00
	SETB	_C
	LDA	STK00
	SUBB	@P1,-6
	LDA	@P1,2
	SUBB	@P1,-5
	JNC	_00188_DS_
;	.line	52; "printf.c"	d*=base;        
	LDA	@P1,-6
	PUSH	
	LDA	@P1,-5
	PUSH	
	LDA	@_RAMP1
	PUSH	
	LDA	@P1,1
	PUSH	
	BANK	__mulint
	CALL	__mulint
	STA	@P1,1
	LDA	STK00
	STA	@_RAMP1
	BANK	_00167_DS_
	JMP	_00167_DS_
_00188_DS_:
;	.line	53; "printf.c"	while (d!=0) {
	CLRA	
	STA	@P1,3
	STA	@P1,2
_00174_DS_:
	LDA	@_RAMP1
	ORA	@P1,1
	JZ	_00176_DS_
;	.line	54; "printf.c"	int dgt = num / d;
	LDA	@_RAMP1
	PUSH	
	LDA	@P1,1
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	__divuint
	CALL	__divuint
	STA	@P1,4
	LDA	STK00
	STA	@P1,5
;	.line	55; "printf.c"	num%= d;
	LDA	@_RAMP1
	PUSH	
	LDA	@P1,1
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	__moduint
	CALL	__moduint
	STA	@P1,-3
	LDA	STK00
	STA	@P1,-4
;	.line	56; "printf.c"	d/=base;
	LDA	@P1,-6
	PUSH	
	LDA	@P1,-5
	PUSH	
	LDA	@_RAMP1
	PUSH	
	LDA	@P1,1
	PUSH	
	BANK	__divuint
	CALL	__divuint
	STA	@P1,1
	LDA	STK00
	STA	@_RAMP1
;	.line	57; "printf.c"	if (n || dgt>0 || d==0) {
	LDA	@P1,3
	ORA	@P1,2
	JNZ	_00170_DS_
	SETB	_C
	CLRA	
	SUBB	@P1,5
	CLRA	
	SUBSI	
	SUBB	@P1,4
	JNC	_00170_DS_
	LDA	@_RAMP1
	ORA	@P1,1
	JNZ	_00174_DS_
_00170_DS_:
;	.line	58; "printf.c"	*bf++ = dgt+(dgt<10 ? '0' : (uc ? 'A' : 'a')-10);
	LDA	@P1,-10
	STA	@P1,6
	LDA	@P1,-9
	STA	@P1,7
	LDA	@P1,-10
	INCA	
	STA	@P1,-10
	CLRA	
	ADDC	@P1,-9
	STA	@P1,-9
	LDA	@P1,5
	STA	@P1,8
	ADD	#0xf6
	LDA	@P1,4
	XOR	#0x80
	ADDC	#0x7f
	JC	_00179_DS_
	LDA	#0x30
	STA	@P1,5
	BANK	_00180_DS_
	JMP	_00180_DS_
_00179_DS_:
	LDA	@P1,-8
	ORA	@P1,-7
	JZ	_00181_DS_
	LDA	#0x41
	STA	@P1,4
	BANK	_00182_DS_
	JMP	_00182_DS_
_00181_DS_:
	LDA	#0x61
	STA	@P1,4
_00182_DS_:
	LDA	#0xf6
	ADD	@P1,4
	STA	@P1,5
_00180_DS_:
	LDA	@P1,8
	ADD	@P1,5
	STA	@P1,8
	LDA	@P1,7
	STA	_ROMPH
	LDA	@P1,6
	STA	_ROMPL
	LDA	@P1,8
	STA	@_ROMPINC
;	.line	59; "printf.c"	++n;
	LDA	@P1,3
	INCA	
	STA	@P1,3
	CLRA	
	ADDC	@P1,2
	STA	@P1,2
	BANK	_00174_DS_
	JMP	_00174_DS_
_00176_DS_:
;	.line	62; "printf.c"	*bf=0;
	LDA	@P1,-9
	STA	_ROMPH
	LDA	@P1,-10
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
	LDA	#0xF7
	BANK	__sp_dec
	CALL	__sp_dec
	POP	
	STA	_RAMP1H
	POP	
	STA	_RAMP1L
	LDA	#0xF8
	BANK	__sp_dec
	JMP	__sp_dec

	New pBlock

CSEG	 (CODE), dbName =C
;; Starting pCode block
_li2a:	;Function start
; 0 exit points
;	.line	36; "printf.c"	static void li2a (long num, char * bf)
	LDA	_RAMP1L
	PUSH	
	LDA	_RAMP1H
	PUSH	
	P02P1	
;	.line	38; "printf.c"	if (num<0) {
	LDA	@P1,-3
	XOR	#0x80
	ROL	
	JC	_00156_DS_
;	.line	39; "printf.c"	num=-num;
	SETB	_C
	CLRA	
	SUBB	@P1,-6
	STA	@P1,-6
	CLRA	
	SUBB	@P1,-5
	STA	@P1,-5
	CLRA	
	SUBB	@P1,-4
	STA	@P1,-4
	CLRA	
	SUBB	@P1,-3
	STA	@P1,-3
;	.line	40; "printf.c"	*bf++ = '-';
	LDA	@P1,-7
	STA	_ROMPH
	LDA	@P1,-8
	STA	_ROMPL
	LDA	#0x2d
	STA	@_ROMPINC
	LDA	@P1,-8
	INCA	
	STA	@P1,-8
	CLRA	
	ADDC	@P1,-7
	STA	@P1,-7
_00156_DS_:
;	.line	42; "printf.c"	uli2a(num,10,0,bf);
	LDA	@P1,-8
	PUSH	
	LDA	@P1,-7
	PUSH	
	CLRA	
	PUSH	
	PUSH	
	LDA	#0x0a
	PUSH	
	CLRA	
	PUSH	
	LDA	@P1,-6
	PUSH	
	LDA	@P1,-5
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	_uli2a
	CALL	_uli2a
	POP	
	STA	_RAMP1H
	POP	
	STA	_RAMP1L
	LDA	#0xFA
	BANK	__sp_dec
	JMP	__sp_dec

	New pBlock

CSEG	 (CODE), dbName =C
;; Starting pCode block
_uli2a:	;Function start
; 0 exit points
;	.line	18; "printf.c"	static void uli2a(unsigned long int num, unsigned int base, int uc,char * bf)
	LDA	_RAMP1L
	PUSH	
	LDA	_RAMP1H
	PUSH	
	P02P1	
	LDA	#0x0E
	BANK	__sp_inc
	CALL	__sp_inc
;	.line	21; "printf.c"	unsigned long d=1;
	LDA	#0x01
	STA	@_RAMP1
	CLRA	
	STA	@P1,1
	STA	@P1,2
	STA	@P1,3
_00109_DS_:
;	.line	22; "printf.c"	while (num/d >= base)
	LDA	@_RAMP1
	PUSH	
	LDA	@P1,1
	PUSH	
	LDA	@P1,2
	PUSH	
	LDA	@P1,3
	PUSH	
	LDA	@P1,-6
	PUSH	
	LDA	@P1,-5
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	__divulong
	CALL	__divulong
	STA	@P1,4
	LDA	@P1,-8
	STA	@P1,5
	LDA	@P1,-7
	STA	@P1,6
	CLRA	
	STA	@P1,7
	STA	@P1,8
	SETB	_C
	LDA	STK02
	SUBB	@P1,5
	LDA	STK01
	SUBB	@P1,6
	LDA	STK00
	SUBB	@P1,7
	LDA	@P1,4
	SUBB	@P1,8
	JNC	_00130_DS_
;	.line	23; "printf.c"	d*=base;         
	LDA	@P1,5
	PUSH	
	LDA	@P1,6
	PUSH	
	LDA	@P1,7
	PUSH	
	LDA	@P1,8
	PUSH	
	LDA	@_RAMP1
	PUSH	
	LDA	@P1,1
	PUSH	
	LDA	@P1,2
	PUSH	
	LDA	@P1,3
	PUSH	
	BANK	__mullong
	CALL	__mullong
	STA	@P1,3
	LDA	STK00
	STA	@P1,2
	LDA	STK01
	STA	@P1,1
	LDA	STK02
	STA	@_RAMP1
	BANK	_00109_DS_
	JMP	_00109_DS_
_00130_DS_:
;	.line	24; "printf.c"	while (d!=0) {
	LDA	@P1,-12
	STA	@P1,-8
	LDA	@P1,-11
	STA	@P1,-7
	CLRA	
	STA	@P1,-12
	STA	@P1,-11
_00116_DS_:
	LDA	@_RAMP1
	ORA	@P1,1
	ORA	@P1,2
	ORA	@P1,3
	JZ	_00118_DS_
;	.line	25; "printf.c"	int dgt = num / d;
	LDA	@_RAMP1
	PUSH	
	LDA	@P1,1
	PUSH	
	LDA	@P1,2
	PUSH	
	LDA	@P1,3
	PUSH	
	LDA	@P1,-6
	PUSH	
	LDA	@P1,-5
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	__divulong
	CALL	__divulong
	LDA	STK02
	STA	@P1,9
	LDA	STK01
	STA	@P1,10
;	.line	26; "printf.c"	num%=d;
	LDA	@_RAMP1
	PUSH	
	LDA	@P1,1
	PUSH	
	LDA	@P1,2
	PUSH	
	LDA	@P1,3
	PUSH	
	LDA	@P1,-6
	PUSH	
	LDA	@P1,-5
	PUSH	
	LDA	@P1,-4
	PUSH	
	LDA	@P1,-3
	PUSH	
	BANK	__modulong
	CALL	__modulong
	STA	@P1,-3
	LDA	STK00
	STA	@P1,-4
	LDA	STK01
	STA	@P1,-5
	LDA	STK02
	STA	@P1,-6
;	.line	27; "printf.c"	d/=base;
	LDA	@P1,5
	PUSH	
	LDA	@P1,6
	PUSH	
	LDA	@P1,7
	PUSH	
	LDA	@P1,8
	PUSH	
	LDA	@_RAMP1
	PUSH	
	LDA	@P1,1
	PUSH	
	LDA	@P1,2
	PUSH	
	LDA	@P1,3
	PUSH	
	BANK	__divulong
	CALL	__divulong
	STA	@P1,3
	LDA	STK00
	STA	@P1,2
	LDA	STK01
	STA	@P1,1
	LDA	STK02
	STA	@_RAMP1
;	.line	28; "printf.c"	if (n || dgt>0|| d==0) {
	LDA	@P1,-12
	ORA	@P1,-11
	JNZ	_00112_DS_
	SETB	_C
	CLRA	
	SUBB	@P1,9
	CLRA	
	SUBSI	
	SUBB	@P1,10
	JNC	_00112_DS_
	LDA	@_RAMP1
	ORA	@P1,1
	ORA	@P1,2
	ORA	@P1,3
	JNZ	_00116_DS_
_00112_DS_:
;	.line	29; "printf.c"	*bf++ = dgt+(dgt<10 ? '0' : (uc ? 'A' : 'a')-10);
	LDA	@P1,-8
	STA	@P1,11
	LDA	@P1,-7
	STA	@P1,12
	LDA	@P1,-8
	INCA	
	STA	@P1,-8
	CLRA	
	ADDC	@P1,-7
	STA	@P1,-7
	LDA	@P1,9
	STA	@P1,13
	ADD	#0xf6
	LDA	@P1,10
	XOR	#0x80
	ADDC	#0x7f
	JNC	_00011_DS_
	BANK	_00121_DS_
	JMP	_00121_DS_
_00011_DS_:
	LDA	#0x30
	STA	@P1,4
	BANK	_00122_DS_
	JMP	_00122_DS_
_00121_DS_:
	LDA	@P1,-10
	ORA	@P1,-9
	JNZ	_00012_DS_
	BANK	_00123_DS_
	JMP	_00123_DS_
_00012_DS_:
	LDA	#0x41
	STA	@P1,9
	BANK	_00124_DS_
	JMP	_00124_DS_
_00123_DS_:
	LDA	#0x61
	STA	@P1,9
_00124_DS_:
	LDA	#0xf6
	ADD	@P1,9
	STA	@P1,4
_00122_DS_:
	LDA	@P1,13
	ADD	@P1,4
	STA	@P1,13
	LDA	@P1,12
	STA	_ROMPH
	LDA	@P1,11
	STA	_ROMPL
	LDA	@P1,13
	STA	@_ROMPINC
;	.line	30; "printf.c"	++n;
	LDA	@P1,-12
	INCA	
	STA	@P1,-12
	CLRA	
	ADDC	@P1,-11
	STA	@P1,-11
	BANK	_00116_DS_
	JMP	_00116_DS_
_00118_DS_:
;	.line	33; "printf.c"	*bf=0;
	LDA	@P1,-7
	STA	_ROMPH
	LDA	@P1,-8
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
	LDA	#0xF2
	BANK	__sp_dec
	CALL	__sp_dec
	POP	
	STA	_RAMP1H
	POP	
	STA	_RAMP1L
	LDA	#0xF6
	BANK	__sp_dec
	JMP	__sp_dec

	New pBlock

CSEG	 (CODE), dbName =C
;; Starting pCode block
_putch:	;Function start
; 0 exit points
;	.line	11; "printf.c"	static void putch(void* p,char c)
	LDA	_RAMP1L
	PUSH	
	LDA	_RAMP1H
	PUSH	
	P02P1	
;	.line	13; "printf.c"	putchar(c);
	LDA	@P1,-5
	PUSH	
	BANK	_putchar
	CALL	_putchar
	POP	
	STA	_RAMP1H
	POP	
	STA	_RAMP1L
	LDA	#0xFD
	BANK	__sp_dec
	JMP	__sp_dec
