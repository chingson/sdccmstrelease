;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"atan2f.c"
	.module atan2f
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fatan2f$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$atan2f$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; atan2f-code 
.globl _atan2f

;--------------------------------------------------------
	.FUNC _atan2f:$PNUM 8:$C:_fabsf:$C:___fslt:$C:___fsdiv:$C:_atanf\
:$C:___fsadd\
:$L:r0x1163:$L:_atan2f_STK00:$L:_atan2f_STK01:$L:_atan2f_STK02:$L:_atan2f_STK03\
:$L:_atan2f_STK04:$L:_atan2f_STK05:$L:_atan2f_STK06:$L:r0x116B:$L:r0x116A\
:$L:r0x1169:$L:r0x1168:$L:r0x116F:$L:r0x116E:$L:r0x116D\
:$L:r0x116C
;--------------------------------------------------------
;	.line	34; "atan2f.c"	float atan2f(float x, float y)
_atan2f:	;Function start
	STA	r0x1163
;	;.line	38; "atan2f.c"	if ((x==0.0) && (y==0.0))
	LDA	_atan2f_STK02
	ORA	_atan2f_STK01
	ORA	_atan2f_STK00
	ORA	r0x1163
	JNZ	_00106_DS_
	LDA	_atan2f_STK06
	ORA	_atan2f_STK05
	ORA	_atan2f_STK04
	ORA	_atan2f_STK03
	JNZ	_00106_DS_
;	;.line	40; "atan2f.c"	errno=EDOM;
	LDA	#0x21
	STA	_errno
;	;.line	41; "atan2f.c"	return 0.0;
	CLRA	
	STA	(_errno + 1)
	STA	STK02
	STA	STK01
	STA	STK00
	CLRA	
	JMP	_00113_DS_
_00106_DS_:
;	;.line	44; "atan2f.c"	if(fabsf(y)>=fabsf(x))
	LDA	_atan2f_STK06
	STA	_fabsf_STK02
	LDA	_atan2f_STK05
	STA	_fabsf_STK01
	LDA	_atan2f_STK04
	STA	_fabsf_STK00
	LDA	_atan2f_STK03
	CALL	_fabsf
	STA	r0x116B
	LDA	STK00
	STA	r0x116A
	LDA	STK01
	STA	r0x1169
	LDA	STK02
	STA	r0x1168
	LDA	_atan2f_STK02
	STA	_fabsf_STK02
	LDA	_atan2f_STK01
	STA	_fabsf_STK01
	LDA	_atan2f_STK00
	STA	_fabsf_STK00
	LDA	r0x1163
	CALL	_fabsf
	STA	r0x116F
	LDA	STK02
	STA	___fslt_STK06
	LDA	STK01
	STA	___fslt_STK05
	LDA	STK00
	STA	___fslt_STK04
	LDA	r0x116F
	STA	___fslt_STK03
	LDA	r0x1168
	STA	___fslt_STK02
	LDA	r0x1169
	STA	___fslt_STK01
	LDA	r0x116A
	STA	___fslt_STK00
	LDA	r0x116B
	CALL	___fslt
	JNZ	_00111_DS_
;	;.line	46; "atan2f.c"	r=atanf(x/y);
	LDA	_atan2f_STK06
	STA	___fsdiv_STK06
	LDA	_atan2f_STK05
	STA	___fsdiv_STK05
	LDA	_atan2f_STK04
	STA	___fsdiv_STK04
	LDA	_atan2f_STK03
	STA	___fsdiv_STK03
	LDA	_atan2f_STK02
	STA	___fsdiv_STK02
	LDA	_atan2f_STK01
	STA	___fsdiv_STK01
	LDA	_atan2f_STK00
	STA	___fsdiv_STK00
	LDA	r0x1163
	CALL	___fsdiv
	STA	r0x116B
	LDA	STK02
	STA	_atanf_STK02
	LDA	STK01
	STA	_atanf_STK01
	LDA	STK00
	STA	_atanf_STK00
	LDA	r0x116B
	CALL	_atanf
	STA	r0x116B
	LDA	STK00
	STA	r0x116A
	LDA	STK01
	STA	r0x1169
	LDA	STK02
	STA	r0x1168
;	;.line	47; "atan2f.c"	if(y<0.0) r+=(x>=0?PI:-PI);
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	STA	___fslt_STK04
	STA	___fslt_STK03
	LDA	_atan2f_STK06
	STA	___fslt_STK02
	LDA	_atan2f_STK05
	STA	___fslt_STK01
	LDA	_atan2f_STK04
	STA	___fslt_STK00
	LDA	_atan2f_STK03
	CALL	___fslt
	JZ	_00112_DS_
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	STA	___fslt_STK04
	STA	___fslt_STK03
	LDA	_atan2f_STK02
	STA	___fslt_STK02
	LDA	_atan2f_STK01
	STA	___fslt_STK01
	LDA	_atan2f_STK00
	STA	___fslt_STK00
	LDA	r0x1163
	CALL	___fslt
	JNZ	_00115_DS_
	LDA	#0xdb
	STA	r0x116C
	LDA	#0x0f
	STA	r0x116D
	LDA	#0x49
	STA	r0x116E
	LDA	#0x40
	STA	r0x116F
	JMP	_00116_DS_
_00115_DS_:
	LDA	#0xdb
	STA	r0x116C
	LDA	#0x0f
	STA	r0x116D
	LDA	#0x49
	STA	r0x116E
	LDA	#0xc0
	STA	r0x116F
_00116_DS_:
	LDA	r0x116C
	STA	___fsadd_STK06
	LDA	r0x116D
	STA	___fsadd_STK05
	LDA	r0x116E
	STA	___fsadd_STK04
	LDA	r0x116F
	STA	___fsadd_STK03
	LDA	r0x1168
	STA	___fsadd_STK02
	LDA	r0x1169
	STA	___fsadd_STK01
	LDA	r0x116A
	STA	___fsadd_STK00
	LDA	r0x116B
	CALL	___fsadd
	STA	r0x116B
	LDA	STK00
	STA	r0x116A
	LDA	STK01
	STA	r0x1169
	LDA	STK02
	STA	r0x1168
	JMP	_00112_DS_
_00111_DS_:
;	;.line	51; "atan2f.c"	r=-atanf(y/x);
	LDA	_atan2f_STK02
	STA	___fsdiv_STK06
	LDA	_atan2f_STK01
	STA	___fsdiv_STK05
	LDA	_atan2f_STK00
	STA	___fsdiv_STK04
	LDA	r0x1163
	STA	___fsdiv_STK03
	LDA	_atan2f_STK06
	STA	___fsdiv_STK02
	LDA	_atan2f_STK05
	STA	___fsdiv_STK01
	LDA	_atan2f_STK04
	STA	___fsdiv_STK00
	LDA	_atan2f_STK03
	CALL	___fsdiv
	STA	_atan2f_STK03
	LDA	STK02
	STA	_atanf_STK02
	LDA	STK01
	STA	_atanf_STK01
	LDA	STK00
	STA	_atanf_STK00
	LDA	_atan2f_STK03
	CALL	_atanf
	XOR	#0x80
	STA	_atan2f_STK03
	LDA	STK00
	STA	_atan2f_STK04
	LDA	STK01
	STA	_atan2f_STK05
	LDA	STK02
	STA	_atan2f_STK06
;	;.line	52; "atan2f.c"	r+=(x<0.0?-HALF_PI:HALF_PI);
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	STA	___fslt_STK04
	STA	___fslt_STK03
	LDA	_atan2f_STK02
	STA	___fslt_STK02
	LDA	_atan2f_STK01
	STA	___fslt_STK01
	LDA	_atan2f_STK00
	STA	___fslt_STK00
	LDA	r0x1163
	CALL	___fslt
	JZ	_00117_DS_
	LDA	#0xdb
	STA	_atan2f_STK02
	LDA	#0x0f
	STA	_atan2f_STK01
	LDA	#0xc9
	STA	_atan2f_STK00
	LDA	#0xbf
	STA	r0x1163
	JMP	_00118_DS_
_00117_DS_:
	LDA	#0xdb
	STA	_atan2f_STK02
	LDA	#0x0f
	STA	_atan2f_STK01
	LDA	#0xc9
	STA	_atan2f_STK00
	LDA	#0x3f
	STA	r0x1163
_00118_DS_:
	LDA	_atan2f_STK02
	STA	___fsadd_STK06
	LDA	_atan2f_STK01
	STA	___fsadd_STK05
	LDA	_atan2f_STK00
	STA	___fsadd_STK04
	LDA	r0x1163
	STA	___fsadd_STK03
	LDA	_atan2f_STK06
	STA	___fsadd_STK02
	LDA	_atan2f_STK05
	STA	___fsadd_STK01
	LDA	_atan2f_STK04
	STA	___fsadd_STK00
	LDA	_atan2f_STK03
	CALL	___fsadd
	STA	r0x116B
	LDA	STK00
	STA	r0x116A
	LDA	STK01
	STA	r0x1169
	LDA	STK02
	STA	r0x1168
_00112_DS_:
;	;.line	54; "atan2f.c"	return r;
	LDA	r0x1168
	STA	STK02
	LDA	r0x1169
	STA	STK01
	LDA	r0x116A
	STA	STK00
	LDA	r0x116B
_00113_DS_:
;	;.line	55; "atan2f.c"	}
	RET	
; exit point of _atan2f
	.ENDFUNC _atan2f
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$errno$0$0({2}SI:S),E,0,0
	;--cdb--S:Latan2f.atan2f$y$65536$25({4}SF:S),R,0,0,[_atan2f_STK06,_atan2f_STK05,_atan2f_STK04,_atan2f_STK03]
	;--cdb--S:Latan2f.atan2f$x$65536$25({4}SF:S),R,0,0,[_atan2f_STK02,_atan2f_STK01,_atan2f_STK00,r0x1163]
	;--cdb--S:Latan2f.atan2f$r$65536$26({4}SF:S),R,0,0,[_atan2f_STK06,_atan2f_STK05,_atan2f_STK04,_atan2f_STK03]
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_atanf
	.globl	_fabsf
	.globl	___fslt
	.globl	___fsdiv
	.globl	___fsadd
	.globl	_errno

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_atan2f
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_atan2f_0	udata
r0x1163:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116C:	.ds	1
r0x116D:	.ds	1
r0x116E:	.ds	1
r0x116F:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_atan2f_STK00:	.ds	1
	.globl _atan2f_STK00
_atan2f_STK01:	.ds	1
	.globl _atan2f_STK01
_atan2f_STK02:	.ds	1
	.globl _atan2f_STK02
_atan2f_STK03:	.ds	1
	.globl _atan2f_STK03
_atan2f_STK04:	.ds	1
	.globl _atan2f_STK04
_atan2f_STK05:	.ds	1
	.globl _atan2f_STK05
_atan2f_STK06:	.ds	1
	.globl _atan2f_STK06
	.globl _fabsf_STK02
	.globl _fabsf_STK01
	.globl _fabsf_STK00
	.globl ___fslt_STK06
	.globl ___fslt_STK05
	.globl ___fslt_STK04
	.globl ___fslt_STK03
	.globl ___fslt_STK02
	.globl ___fslt_STK01
	.globl ___fslt_STK00
	.globl ___fsdiv_STK06
	.globl ___fsdiv_STK05
	.globl ___fsdiv_STK04
	.globl ___fsdiv_STK03
	.globl ___fsdiv_STK02
	.globl ___fsdiv_STK01
	.globl ___fsdiv_STK00
	.globl _atanf_STK02
	.globl _atanf_STK01
	.globl _atanf_STK00
	.globl ___fsadd_STK06
	.globl ___fsadd_STK05
	.globl ___fsadd_STK04
	.globl ___fsadd_STK03
	.globl ___fsadd_STK02
	.globl ___fsadd_STK01
	.globl ___fsadd_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:_atan2f_STK04:NULL+0:14:0
	;--cdb--W:_atan2f_STK05:NULL+0:13:0
	;--cdb--W:_atan2f_STK06:NULL+0:12:0
	;--cdb--W:r0x116A:NULL+0:14:0
	;--cdb--W:r0x1169:NULL+0:13:0
	;--cdb--W:r0x1168:NULL+0:12:0
	;--cdb--W:r0x116E:NULL+0:14:0
	;--cdb--W:r0x116D:NULL+0:13:0
	;--cdb--W:r0x116C:NULL+0:12:0
	;--cdb--W:r0x1168:NULL+0:-1:1
	;--cdb--W:r0x116C:NULL+0:-1:1
	;--cdb--W:_atan2f_STK03:NULL+0:-1:1
	;--cdb--W:_atan2f_STK02:NULL+0:-1:1
	end
