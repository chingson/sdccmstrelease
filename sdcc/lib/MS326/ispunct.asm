;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"ispunct.c"
	.module ispunct
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$ispunct$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isblank$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$islower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isupper$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; ispunct-code 
.globl _ispunct

;--------------------------------------------------------
	.FUNC _ispunct:$PNUM 2:$C:_isprint:$C:_isspace:$C:_isalnum\
:$L:r0x1160:$L:_ispunct_STK00:$L:r0x1161
;--------------------------------------------------------
;	.line	33; "ispunct.c"	int ispunct (int c)
_ispunct:	;Function start
	STA	r0x1160
;	;.line	35; "ispunct.c"	return (isprint (c) && !isspace (c) && !isalnum (c));
	LDA	_ispunct_STK00
	STA	_isprint_STK00
	LDA	r0x1160
	CALL	_isprint
	ORA	STK00
	JZ	_00107_DS_
	LDA	_ispunct_STK00
	STA	_isspace_STK00
	LDA	r0x1160
	CALL	_isspace
	ORA	STK00
	JNZ	_00107_DS_
	LDA	_ispunct_STK00
	STA	_isalnum_STK00
	LDA	r0x1160
	CALL	_isalnum
	ORA	STK00
	JZ	_00108_DS_
_00107_DS_:
	CLRA	
	STA	_ispunct_STK00
	JMP	_00109_DS_
_00108_DS_:
	LDA	#0x01
	STA	_ispunct_STK00
_00109_DS_:
	LDA	_ispunct_STK00
	JPL	_00120_DS_
	LDA	#0xff
	JMP	_00121_DS_
_00120_DS_:
	CLRA	
_00121_DS_:
	STA	r0x1161
	LDA	_ispunct_STK00
	STA	STK00
	LDA	r0x1161
;	;.line	36; "ispunct.c"	}
	RET	
; exit point of _ispunct
	.ENDFUNC _ispunct
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:Lispunct.isblank$c$65536$13({2}SI:S),E,0,0
	;--cdb--S:Lispunct.isdigit$c$65536$15({2}SI:S),E,0,0
	;--cdb--S:Lispunct.islower$c$65536$17({2}SI:S),E,0,0
	;--cdb--S:Lispunct.isupper$c$65536$19({2}SI:S),E,0,0
	;--cdb--S:Lispunct.ispunct$c$65536$21({2}SI:S),R,0,0,[_ispunct_STK00,r0x1160]
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_isprint
	.globl	_isspace
	.globl	_isalnum

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_ispunct
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_ispunct_0	udata
r0x1160:	.ds	1
r0x1161:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_ispunct_STK00:	.ds	1
	.globl _ispunct_STK00
	.globl _isprint_STK00
	.globl _isspace_STK00
	.globl _isalnum_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:_ispunct_STK00:NULL+0:-1:1
	;--cdb--W:r0x1160:NULL+0:4451:0
	;--cdb--W:r0x1162:NULL+0:-1:1
	;--cdb--W:r0x1160:NULL+0:-1:1
	end
