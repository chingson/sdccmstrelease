;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.1 #3de0c6772 (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"C:\work\ms326sphlib"
;;	.file	"ms326apirec_preera.c"
	.module ms326apirec_preera
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fms326apirec_preera$spiastru[({0}S:S$addrhm$0$0({2}SI:U),Z,0,0)({2}S:S$addrl$0$0({1}SC:U),Z,0,0)]
	;--cdb--T:Fms326apirec_preera$adps[({0}S:S$predict$0$0({2}SI:S),Z,0,0)({2}S:S$index$0$0({1}SC:U),Z,0,0)]
	;--cdb--T:Fms326apirec_preera$touchen[({0}S:S$toff$0$0({1}SC:U),Z,0,0)({1}S:S$nmossw$0$0({1}SC:U),Z,0,0)({2}S:S$period$0$0({2}SI:U),Z,0,0)({4}S:S$threshold$0$0({2}SI:U),Z,0,0)({6}S:S$count$0$0({2}SI:U),Z,0,0)]
	;--cdb--T:Fms326apirec_preera$pwmleds[({0}S:S$period$0$0({1}SC:U),Z,0,0)({1}S:S$counter$0$0({1}SC:U),Z,0,0)({2}S:S$threshold$0$0({1}SC:U),Z,0,0)]
	;--cdb--S:G$api_rec_stop_noerase$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$api_rec_stop_noerase$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$api_rec_start_no_erase$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$api_rec_start_no_erase$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$api_rec_prepare_pre_erase$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$api_rec_prepare_pre_erase$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_noer$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_chspick$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_init$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_tk_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_job$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; ms326apirec_preera-code 
.globl _api_rec_stop_noerase

;--------------------------------------------------------
	.FUNC _api_rec_stop_noerase:$PNUM 1:$C:_api_enter_stdby_mode\
:$L:r0x11E8
;--------------------------------------------------------
;	.line	153; "ms326apirec_preera.c"	void api_rec_stop_noerase(BYTE add_end_code)
_api_rec_stop_noerase:	;Function start
	STA	r0x11E8
;	;.line	155; "ms326apirec_preera.c"	DMICON=0; // disable adc clock
	CLRA	
;	;.line	156; "ms326apirec_preera.c"	ADCON=0;
	STA	_DMICON
	STA	_ADCON
;	;.line	157; "ms326apirec_preera.c"	SYSC2&=0x7f ; // mbias off
	LDA	#0x7f
	AND	_SYSC2
	STA	_SYSC2
;	;.line	158; "ms326apirec_preera.c"	GIF=0x7f;
	LDA	#0x7f
	STA	_GIF
_00228_DS_:
;	;.line	159; "ms326apirec_preera.c"	while(SPIOP&1) // now in suspend mode
	LDA	_SPIOP
	SHR	
	JNC	_00230_DS_
;	;.line	162; "ms326apirec_preera.c"	api_enter_stdby_mode(0,0,0,0,0);// use timer wakeup
	CLRA	
	STA	_api_enter_stdby_mode_STK03
	STA	_api_enter_stdby_mode_STK02
	STA	_api_enter_stdby_mode_STK01
	STA	_api_enter_stdby_mode_STK00
	CLRA	
	CALL	_api_enter_stdby_mode
	JMP	_00228_DS_
_00230_DS_:
;	;.line	165; "ms326apirec_preera.c"	ULAWC&=0x1f;
	LDA	#0x1f
	AND	_ULAWC
	STA	_ULAWC
;	;.line	167; "ms326apirec_preera.c"	if(!add_end_code)
	LDA	r0x11E8
;	;.line	168; "ms326apirec_preera.c"	return;
	JZ	_00243_DS_
;	;.line	169; "ms326apirec_preera.c"	if((SPIM&0xf)!=0x0f)
	LDA	_SPIM
	AND	#0x0f
	XOR	#0x0f
;	;.line	170; "ms326apirec_preera.c"	return;
	JNZ	_00243_DS_
;	;.line	171; "ms326apirec_preera.c"	SPIOP=0x20;
	LDA	#0x20
	STA	_SPIOP
;	;.line	172; "ms326apirec_preera.c"	if(SPIM==(BYTE)(api_endpage&0xff) && SPIH==(BYTE)(api_endpage>>8))
	LDA	_api_endpage
	XOR	_SPIM
	JNZ	_00236_DS_
	LDA	(_api_endpage + 1)
	XOR	_SPIH
;	;.line	173; "ms326apirec_preera.c"	return;
	JZ	_00243_DS_
_00236_DS_:
;	;.line	175; "ms326apirec_preera.c"	SPIOP=1;
	LDA	#0x01
	STA	_SPIOP
;	;.line	176; "ms326apirec_preera.c"	if(api_mode & API_RECMODE_ERASE_SUSP)
	LDA	_api_mode
	AND	#0x02
	JZ	_00242_DS_
;	;.line	178; "ms326apirec_preera.c"	SPIOP=0x82;
	LDA	#0x82
	STA	_SPIOP
_00238_DS_:
;	;.line	179; "ms326apirec_preera.c"	while(SPIOP&1)
	LDA	_SPIOP
	SHR	
	JNC	_00240_DS_
;	;.line	180; "ms326apirec_preera.c"	api_enter_stdby_mode(0,0,0,0,0);
	CLRA	
	STA	_api_enter_stdby_mode_STK03
	STA	_api_enter_stdby_mode_STK02
	STA	_api_enter_stdby_mode_STK01
	STA	_api_enter_stdby_mode_STK00
	CLRA	
	CALL	_api_enter_stdby_mode
	JMP	_00238_DS_
_00240_DS_:
;	;.line	181; "ms326apirec_preera.c"	return;
	JMP	_00243_DS_
_00242_DS_:
;	;.line	183; "ms326apirec_preera.c"	SPIOP=2;
	LDA	#0x02
	STA	_SPIOP
_00243_DS_:
;	;.line	184; "ms326apirec_preera.c"	}
	RET	
; exit point of _api_rec_stop_noerase
	.ENDFUNC _api_rec_stop_noerase
.globl _api_rec_start_no_erase

;--------------------------------------------------------
	.FUNC _api_rec_start_no_erase:$PNUM 5:$L:r0x11DB:$L:_api_rec_start_no_erase_STK00:$L:_api_rec_start_no_erase_STK01:$L:_api_rec_start_no_erase_STK02:$L:_api_rec_start_no_erase_STK03\

;--------------------------------------------------------
;	.line	89; "ms326apirec_preera.c"	BYTE api_rec_start_no_erase( BYTE recmode, USHORT page_start, USHORT page_end)
_api_rec_start_no_erase:	;Function start
	STA	r0x11DB
;	;.line	91; "ms326apirec_preera.c"	SYSC2|=SYSC2_BIT_DMAI7; // match 7 bit is ok
	LDA	_SYSC2
	ORA	#0x08
	STA	_SYSC2
;	;.line	93; "ms326apirec_preera.c"	if(recmode&API_RECMODE_DMI)
	LDA	r0x11DB
	AND	#0x10
	JZ	_00201_DS_
;	;.line	96; "ms326apirec_preera.c"	BGCON|=0x10;
	LDA	_BGCON
	ORA	#0x10
	STA	_BGCON
;	;.line	97; "ms326apirec_preera.c"	DMICON|=0x80;
	LDA	_DMICON
	ORA	#0x80
	STA	_DMICON
	JMP	_00202_DS_
_00201_DS_:
;	;.line	100; "ms326apirec_preera.c"	BGCON&=0xef;
	LDA	#0xef
	AND	_BGCON
	STA	_BGCON
;	;.line	101; "ms326apirec_preera.c"	L2USH=0;
	CLRA	
	STA	_L2USH
_00202_DS_:
;	;.line	105; "ms326apirec_preera.c"	DCLAMP=(DCLAMP&0xFE)|0xf8;
	LDA	_DCLAMP
	AND	#0xfe
	ORA	#0xf8
	STA	_DCLAMP
;	;.line	109; "ms326apirec_preera.c"	if(recmode&1)
	LDA	r0x11DB
	SHR	
	JNC	_00206_DS_
;	;.line	111; "ms326apirec_preera.c"	ULAWC|=0xC0;
	LDA	_ULAWC
	ORA	#0xc0
	STA	_ULAWC
	JMP	_00207_DS_
_00206_DS_:
;	;.line	114; "ms326apirec_preera.c"	} else if (!(recmode&API_RECMODE_16B))
	LDA	r0x11DB
	AND	#0x08
	JNZ	_00207_DS_
;	;.line	116; "ms326apirec_preera.c"	ULAWC&=0x3f;
	LDA	#0x3f
	AND	_ULAWC
	STA	_ULAWC
;	;.line	117; "ms326apirec_preera.c"	ADCON|=0x04;
	LDA	_ADCON
	ORA	#0x04
	STA	_ADCON
_00207_DS_:
;	;.line	124; "ms326apirec_preera.c"	SPIH=page_start>>8;
	LDA	_api_rec_start_no_erase_STK00
	STA	_SPIH
;	;.line	125; "ms326apirec_preera.c"	SPIM=page_start&0xff;
	LDA	_api_rec_start_no_erase_STK01
	STA	_SPIM
;	;.line	126; "ms326apirec_preera.c"	api_endpage=page_end;
	LDA	_api_rec_start_no_erase_STK03
	STA	_api_endpage
	LDA	_api_rec_start_no_erase_STK02
	STA	(_api_endpage + 1)
;	;.line	127; "ms326apirec_preera.c"	api_mode=recmode;
	LDA	r0x11DB
	STA	_api_mode
;	;.line	128; "ms326apirec_preera.c"	SPIL=0;
	CLRA	
	STA	_SPIL
;;d:\common\sdccofficial\sdcc\src\ms322\gen.c:9244: size=1, offset=0, AOP_TYPE(res)=9
;	;.line	130; "ms326apirec_preera.c"	api_rec_spi_addr.addrhm=SPIMH;
	LDA	_SPIMH
	STA	_api_rec_spi_addr
;;d:\common\sdccofficial\sdcc\src\ms322\gen.c:9244: size=0, offset=1, AOP_TYPE(res)=9
	LDA	(_SPIMH + 1)
	STA	(_api_rec_spi_addr + 1)
;;d:\common\sdccofficial\sdcc\src\ms322\gen.c:9244: size=0, offset=0, AOP_TYPE(res)=9
;	;.line	131; "ms326apirec_preera.c"	api_rec_spi_addr.addrl=SPIL;
	LDA	_SPIL
	STA	(_api_rec_spi_addr + 2)
;	;.line	143; "ms326apirec_preera.c"	RDMAH=0x80;
	LDA	#0x80
	STA	_RDMAH
;	;.line	144; "ms326apirec_preera.c"	ADCON|=0x2; // DMA enable now, wakeup enable, too
	LDA	_ADCON
	ORA	#0x02
	STA	_ADCON
;	;.line	146; "ms326apirec_preera.c"	api_rampage=api_fifostart; // 16 byte align
	LDA	_api_fifostart
	STA	_api_rampage
;	;.line	147; "ms326apirec_preera.c"	GIF=0x7f;
	LDA	#0x7f
	STA	_GIF
;	;.line	149; "ms326apirec_preera.c"	return 1;
	LDA	#0x01
;	;.line	151; "ms326apirec_preera.c"	}
	RET	
; exit point of _api_rec_start_no_erase
	.ENDFUNC _api_rec_start_no_erase
.globl _api_rec_prepare_pre_erase

;--------------------------------------------------------
	.FUNC _api_rec_prepare_pre_erase:$PNUM 14:$C:_api_clear_filter_mem\
:$L:r0x11B5:$L:_api_rec_prepare_pre_erase_STK00:$L:_api_rec_prepare_pre_erase_STK01:$L:_api_rec_prepare_pre_erase_STK02:$L:_api_rec_prepare_pre_erase_STK03\
:$L:_api_rec_prepare_pre_erase_STK04:$L:_api_rec_prepare_pre_erase_STK05:$L:_api_rec_prepare_pre_erase_STK06:$L:_api_rec_prepare_pre_erase_STK07:$L:_api_rec_prepare_pre_erase_STK08\
:$L:_api_rec_prepare_pre_erase_STK09:$L:_api_rec_prepare_pre_erase_STK10:$L:_api_rec_prepare_pre_erase_STK11:$L:_api_rec_prepare_pre_erase_STK12:$L:r0x11C2\
:$L:r0x11C3
;--------------------------------------------------------
;	.line	21; "ms326apirec_preera.c"	void api_rec_prepare_pre_erase(BYTE rate, BYTE opag, BYTE fg, BYTE en5k, BYTE spkcv,
_api_rec_prepare_pre_erase:	;Function start
	STA	r0x11B5
	LDA	_api_rec_prepare_pre_erase_STK01
	STA	_FILTERGR
;	;.line	26; "ms326apirec_preera.c"	unsigned char ratediv=rate>>2;
	LDA	r0x11B5
	SHR	
	SHR	
	STA	r0x11C2
;	;.line	27; "ms326apirec_preera.c"	rate&=3;
	LDA	#0x03
	AND	r0x11B5
	STA	r0x11B5
;	;.line	28; "ms326apirec_preera.c"	api_clear_filter_mem(0); // recording alreays
	CLRA	
	CALL	_api_clear_filter_mem
;	;.line	33; "ms326apirec_preera.c"	DMA_IL=0xff;
	LDA	#0xff
	STA	_DMA_IL
;	;.line	35; "ms326apirec_preera.c"	api_fifostart=(((USHORT)fifostart)&0x7ff)>>4; // unit is 16byte , usually this is 0x50, msb skip!!
	LDA	#0x07
	AND	_api_rec_prepare_pre_erase_STK04
	STA	_api_rec_prepare_pre_erase_STK04
	LDA	_api_rec_prepare_pre_erase_STK05
	SWA	
	AND	#0x0f
	STA	r0x11C3
	LDA	_api_rec_prepare_pre_erase_STK04
	SWA	
	PUSH	
	AND	#0xf0
	ORA	r0x11C3
	STA	r0x11C3
	POP	
	AND	#0x0f
	LDA	r0x11C3
;	;.line	36; "ms326apirec_preera.c"	api_fifoend=api_fifostart+fifolen-1; // we use XF as the page addr 0x6f
	STA	_api_fifostart
	ADD	_api_rec_prepare_pre_erase_STK06
	ADD	#0xff
	STA	_api_fifoend
;	;.line	37; "ms326apirec_preera.c"	ADP_IND=0x80;// following is 8 bit wide
	LDA	#0x80
	STA	_ADP_IND
;	;.line	38; "ms326apirec_preera.c"	RPAGES=(api_fifostart&0xf)|((api_fifoend&0xf)<<4); // f0
	LDA	_api_fifostart
	AND	#0x0f
	STA	_api_rec_prepare_pre_erase_STK05
	LDA	_api_fifoend
	AND	#0x0f
	SWA	
	AND	#0xf0
	ORA	_api_rec_prepare_pre_erase_STK05
	STA	_RPAGES
;	;.line	39; "ms326apirec_preera.c"	ADP_IND=0;// following is 6 bits only
	CLRA	
	STA	_ADP_IND
;	;.line	40; "ms326apirec_preera.c"	RPAGES=(api_fifostart>>4)|((api_fifoend&0xf0)>>1);// 35
	LDA	_api_fifostart
	SWA	
	AND	#0x0f
	STA	_api_rec_prepare_pre_erase_STK05
	LDA	#0xf0
	AND	_api_fifoend
	STA	_api_rec_prepare_pre_erase_STK04
	CLRA	
	SHRS	
	LDA	_api_rec_prepare_pre_erase_STK04
	ROR	
	ORA	_api_rec_prepare_pre_erase_STK05
	STA	_RPAGES
;	;.line	41; "ms326apirec_preera.c"	RDMAH=0x80; // clear dma address here!!
	LDA	#0x80
	STA	_RDMAH
;	;.line	45; "ms326apirec_preera.c"	LVDCON|=en5k;
	LDA	_api_rec_prepare_pre_erase_STK02
	ORA	_LVDCON
	STA	_LVDCON
;	;.line	46; "ms326apirec_preera.c"	SPKC=(SPKC&0xcf)|spkcv;
	LDA	_SPKC
	AND	#0xcf
	ORA	_api_rec_prepare_pre_erase_STK03
	STA	_SPKC
;	;.line	47; "ms326apirec_preera.c"	ADCG=opag;
	LDA	_api_rec_prepare_pre_erase_STK00
;	;.line	48; "ms326apirec_preera.c"	api_maxrecopg=opag;
	STA	_ADCG
	STA	_api_maxrecopg
;	;.line	49; "ms326apirec_preera.c"	SYSC2|=(SYSC2_BIT_MBIAS|SYSC2_BIT_BZCKSLOW); // mbias ON
	LDA	_SYSC2
	ORA	#0x82
	STA	_SYSC2
;	;.line	51; "ms326apirec_preera.c"	if(SYSC&SYSC_BIT_SKCMD)
	LDA	_SYSC
	AND	#0x02
	JZ	_00131_DS_
;	;.line	52; "ms326apirec_preera.c"	SYSC&=~SYSC_BIT_SKCMD;
	LDA	#0xfd
	AND	_SYSC
	STA	_SYSC
_00131_DS_:
;	;.line	53; "ms326apirec_preera.c"	do {
	LDA	_api_rec_prepare_pre_erase_STK08
	STA	_api_rec_prepare_pre_erase_STK00
	LDA	_api_rec_prepare_pre_erase_STK07
	STA	_api_rec_prepare_pre_erase_STK02
_00110_DS_:
;	;.line	54; "ms326apirec_preera.c"	API_SPI_ERASE(start_spi_page);
	LDA	_api_rec_prepare_pre_erase_STK02
	STA	_SPIH
	LDA	_api_rec_prepare_pre_erase_STK00
	STA	_SPIM
	CLRA	
	STA	_SPIL
	LDA	#0x01
	STA	_SPIOP
	LDA	#0x02
	STA	_SPIOP
;	;.line	55; "ms326apirec_preera.c"	if(callback!=NULL && callback())
	LDA	_api_rec_prepare_pre_erase_STK12
	ORA	_api_rec_prepare_pre_erase_STK11
	JZ	_00108_DS_
	CALL	_00193_DS_
	JMP	_00194_DS_
_00193_DS_:
	CALL	_00195_DS_
_00195_DS_:
	LDA	_api_rec_prepare_pre_erase_STK11
	STA	_STACKH
	LDA	_api_rec_prepare_pre_erase_STK12
	STA	_STACKL
	RET	
_00194_DS_:
	JNZ	_00112_DS_
_00108_DS_:
;	;.line	57; "ms326apirec_preera.c"	start_spi_page+=0x10;
	LDA	#0x10
	ADD	_api_rec_prepare_pre_erase_STK00
	STA	_api_rec_prepare_pre_erase_STK00
	CLRA	
	ADDC	_api_rec_prepare_pre_erase_STK02
	STA	_api_rec_prepare_pre_erase_STK02
;	;.line	58; "ms326apirec_preera.c"	} while(start_spi_page<final_spi_page);
	SETB	_C
	LDA	_api_rec_prepare_pre_erase_STK00
	SUBB	_api_rec_prepare_pre_erase_STK10
	LDA	_api_rec_prepare_pre_erase_STK02
	SUBB	_api_rec_prepare_pre_erase_STK09
	JNC	_00110_DS_
_00112_DS_:
;	;.line	59; "ms326apirec_preera.c"	if(rate==API_ADC_OSR128) // osr high
	LDA	r0x11B5
	XOR	#0x01
	JNZ	_00125_DS_
;	;.line	61; "ms326apirec_preera.c"	if(ratediv)
	LDA	r0x11C2
;	;.line	62; "ms326apirec_preera.c"	RCLKDIV=ratediv;
	JZ	_00114_DS_
	STA	_RCLKDIV
	JMP	_00115_DS_
_00114_DS_:
;	;.line	64; "ms326apirec_preera.c"	RCLKDIV=7;
	LDA	#0x07
	STA	_RCLKDIV
_00115_DS_:
;	;.line	65; "ms326apirec_preera.c"	ADCON=0x41;
	LDA	#0x41
	STA	_ADCON
	JMP	_00126_DS_
_00125_DS_:
;	;.line	66; "ms326apirec_preera.c"	} else if(rate==API_ADC_OSR256)
	LDA	r0x11B5
	XOR	#0x02
	JNZ	_00122_DS_
;	;.line	68; "ms326apirec_preera.c"	if(ratediv)
	LDA	r0x11C2
;	;.line	69; "ms326apirec_preera.c"	RCLKDIV=ratediv;
	JZ	_00117_DS_
	STA	_RCLKDIV
	JMP	_00118_DS_
_00117_DS_:
;	;.line	71; "ms326apirec_preera.c"	RCLKDIV=3;
	LDA	#0x03
	STA	_RCLKDIV
_00118_DS_:
;	;.line	72; "ms326apirec_preera.c"	SYSC2|=0x20;
	LDA	_SYSC2
	ORA	#0x20
	STA	_SYSC2
;	;.line	73; "ms326apirec_preera.c"	ADCON=0x41;
	LDA	#0x41
	STA	_ADCON
	JMP	_00126_DS_
_00122_DS_:
;	;.line	76; "ms326apirec_preera.c"	if(ratediv)
	LDA	r0x11C2
;	;.line	77; "ms326apirec_preera.c"	RCLKDIV=ratediv;
	JZ	_00120_DS_
	STA	_RCLKDIV
_00120_DS_:
;	;.line	78; "ms326apirec_preera.c"	ADCON=1;
	LDA	#0x01
	STA	_ADCON
_00126_DS_:
;	;.line	80; "ms326apirec_preera.c"	ADCON|=0x3a; // give DMA should be OK, for full duplex, halt at 7fe
	LDA	_ADCON
	ORA	#0x3a
	STA	_ADCON
;	;.line	83; "ms326apirec_preera.c"	}
	RET	
; exit point of _api_rec_prepare_pre_erase
	.ENDFUNC _api_rec_prepare_pre_erase
	;--cdb--S:G$api_rec_stop_noerase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start_no_erase$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_prepare_pre_erase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_noer$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_chspick$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_init$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_tk_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$PAR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PADIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOB$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMA_IL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIDAT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCLKDIV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CMPCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTVC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMICON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PRG_RAM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIDMAC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECOAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECLEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECMODE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIOPRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMALEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULB$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULBL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULBH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULO$0$0({4}SL:U),E,0,0
	;--cdb--S:G$MULO1$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULSHIFT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUF$0$0({2}SI:S),E,0,0
	;--cdb--S:G$L2UBUFL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUFH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2USH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$U2LSH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTPRI$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRA$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$LPWMRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMINV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPICK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPSEL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$HWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWDINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PATEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TRAMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMBUFP$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$PASKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBSKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABSKIP$0$0({2}SI:U),E,0,0
	;--cdb--S:G$PATR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTR$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TOUCHC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DUMMYC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IRCD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMDUTY$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMPER$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACGCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECPWR$0$0({4}SL:U),E,0,0
	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$HWPALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0UW$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$EA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_endpage$0$0({2}SI:U),E,0,0
	;--cdb--S:G$api_mode$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_rampage$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_fifostart$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_fifoend$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_maxrecopg$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_rec_spi_addr$0$0({3}STspiastru:S),E,0,0
	;--cdb--S:Lms326apirec_preera.api_rec_prepare_pre_erase$callback$65536$37({2}DC,DF,SC:U),R,0,0,[_api_rec_prepare_pre_erase_STK12,_api_rec_prepare_pre_erase_STK11]
	;--cdb--S:Lms326apirec_preera.api_rec_prepare_pre_erase$final_spi_page$65536$37({2}SI:U),R,0,0,[_api_rec_prepare_pre_erase_STK10,_api_rec_prepare_pre_erase_STK09]
	;--cdb--S:Lms326apirec_preera.api_rec_prepare_pre_erase$start_spi_page$65536$37({2}SI:U),R,0,0,[_api_rec_prepare_pre_erase_STK08,_api_rec_prepare_pre_erase_STK07]
	;--cdb--S:Lms326apirec_preera.api_rec_prepare_pre_erase$fifolen$65536$37({1}SC:U),R,0,0,[_api_rec_prepare_pre_erase_STK06]
	;--cdb--S:Lms326apirec_preera.api_rec_prepare_pre_erase$fifostart$65536$37({2}DG,SC:U),R,0,0,[_api_rec_prepare_pre_erase_STK05,_api_rec_prepare_pre_erase_STK04]
	;--cdb--S:Lms326apirec_preera.api_rec_prepare_pre_erase$spkcv$65536$37({1}SC:U),R,0,0,[_api_rec_prepare_pre_erase_STK03]
	;--cdb--S:Lms326apirec_preera.api_rec_prepare_pre_erase$en5k$65536$37({1}SC:U),R,0,0,[_api_rec_prepare_pre_erase_STK02]
	;--cdb--S:Lms326apirec_preera.api_rec_prepare_pre_erase$fg$65536$37({1}SC:U),R,0,0,[]
	;--cdb--S:Lms326apirec_preera.api_rec_prepare_pre_erase$opag$65536$37({1}SC:U),R,0,0,[_api_rec_prepare_pre_erase_STK00]
	;--cdb--S:Lms326apirec_preera.api_rec_prepare_pre_erase$rate$65536$37({1}SC:U),R,0,0,[r0x11B5]
	;--cdb--S:Lms326apirec_preera.api_rec_prepare_pre_erase$ratediv$65536$39({1}SC:U),R,0,0,[r0x11C2]
	;--cdb--S:Lms326apirec_preera.api_rec_start_no_erase$page_end$65536$45({2}SI:U),R,0,0,[_api_rec_start_no_erase_STK03,_api_rec_start_no_erase_STK02]
	;--cdb--S:Lms326apirec_preera.api_rec_start_no_erase$page_start$65536$45({2}SI:U),R,0,0,[_api_rec_start_no_erase_STK01,_api_rec_start_no_erase_STK00]
	;--cdb--S:Lms326apirec_preera.api_rec_start_no_erase$recmode$65536$45({1}SC:U),R,0,0,[r0x11DB]
	;--cdb--S:Lms326apirec_preera.api_rec_stop_noerase$add_end_code$65536$51({1}SC:U),R,0,0,[r0x11E8]
	;--cdb--S:G$api_rec_prepare_pre_erase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start_no_erase$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop_noerase$0$0({2}DF,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_api_enter_stdby_mode
	.globl	_api_clear_filter_mem
	.globl	_PAR
	.globl	_PADIR
	.globl	_PIOA
	.globl	_PAWK
	.globl	_PAWKDR
	.globl	_TIMERC
	.globl	_THRLD
	.globl	_RAMP0L
	.globl	_RAMP0H
	.globl	_RAMP1L
	.globl	_RAMP1H
	.globl	_PTRCL
	.globl	_PTRCH
	.globl	_ROMPL
	.globl	_ROMPH
	.globl	_BEEPC
	.globl	_FILTERGR
	.globl	_ULAWC
	.globl	_STACKL
	.globl	_STACKH
	.globl	_ADCON
	.globl	_DACON
	.globl	_SYSC
	.globl	_SPIM
	.globl	_SPIH
	.globl	_SPIMH
	.globl	_SPIOP
	.globl	_SPI_BANK
	.globl	_ADP_IND
	.globl	_ADP_VPL
	.globl	_ADP_VPH
	.globl	_ADL
	.globl	_ADH
	.globl	_ZC
	.globl	_ADCG
	.globl	_DAC_PL
	.globl	_DAC_PH
	.globl	_PAG
	.globl	_RDMAL
	.globl	_RDMAH
	.globl	_SPIL
	.globl	_IOMASK
	.globl	_IOCMP
	.globl	_IOCNT
	.globl	_LVDCON
	.globl	_LVDCTH
	.globl	_LVRCON
	.globl	_OFFSETL
	.globl	_OFFSETH
	.globl	_RCCON
	.globl	_BGCON
	.globl	_PWRL
	.globl	_CRYPT
	.globl	_PWRH
	.globl	_PWRHL
	.globl	_IROMDL
	.globl	_IROMDH
	.globl	_RECMUTE
	.globl	_SPKC
	.globl	_DCLAMP
	.globl	_SSPIC
	.globl	_SSPIL
	.globl	_SSPIM
	.globl	_SSPIH
	.globl	_PBR
	.globl	_PBDIR
	.globl	_PIOB
	.globl	_PBWK
	.globl	_PBWKDR
	.globl	_PAIE
	.globl	_PBIE
	.globl	_PAIF
	.globl	_PBIF
	.globl	_GIE
	.globl	_GIF
	.globl	_WDTL
	.globl	_WDTH
	.globl	_RPAGES
	.globl	_PPAGES
	.globl	_DMA_IL
	.globl	_FILTERGP
	.globl	_SPIDAT
	.globl	_RSPIC
	.globl	_RCLKDIV
	.globl	_PCR
	.globl	_PCDIR
	.globl	_PIOC
	.globl	_CMPCON
	.globl	_INTVC
	.globl	_INTV
	.globl	_DMICON
	.globl	_PRG_RAM
	.globl	_PDMAL
	.globl	_PDMAH
	.globl	_PDMALH
	.globl	_SPIDMAC
	.globl	_SDMAAL
	.globl	_SDMAAH
	.globl	_SDMAALH
	.globl	_ECRAL
	.globl	_ECRAH
	.globl	_ECRALH
	.globl	_ECOAL
	.globl	_ECOAH
	.globl	_ECOALH
	.globl	_ECLEN
	.globl	_ECCON
	.globl	_ECMODE
	.globl	_SPIOPRAH
	.globl	_SDMALEN
	.globl	_MULA
	.globl	_MULAL
	.globl	_MULAH
	.globl	_MULB
	.globl	_MULBL
	.globl	_MULBH
	.globl	_MULO
	.globl	_MULO1
	.globl	_MULSHIFT
	.globl	_L2UBUF
	.globl	_L2UBUFL
	.globl	_L2UBUFH
	.globl	_ULAWD
	.globl	_L2USH
	.globl	_U2LSH
	.globl	_INTPRI
	.globl	_LPWMRA
	.globl	_LPWMRAL
	.globl	_LPWMRAH
	.globl	_LPWMEN
	.globl	_LPWMINV
	.globl	_SPICK
	.globl	_SYSC2
	.globl	_HWPSEL
	.globl	_HWPAL
	.globl	_HWPAH
	.globl	_HWPA
	.globl	_HWD
	.globl	_HWDINC
	.globl	_PATEN
	.globl	_PBTEN
	.globl	_PABTEN
	.globl	_TRAMAL
	.globl	_TRAMAH
	.globl	_TRAMBUFP
	.globl	_PASKIP
	.globl	_PBSKIP
	.globl	_PABSKIP
	.globl	_PATR
	.globl	_PBTR
	.globl	_PABTR
	.globl	_TOUCHC
	.globl	_DUMMYC
	.globl	_IRCD
	.globl	_FPWMEN
	.globl	_FPWMDUTY
	.globl	_FPWMPER
	.globl	_DACGCL
	.globl	_RECPWR
	.globl	_ICE0
	.globl	_ICE1
	.globl	_ICE2
	.globl	_ICE3
	.globl	_ICE4
	.globl	_RAMP0
	.globl	_RAMP0INC
	.globl	_RAMP1
	.globl	_RDMALH
	.globl	_HWPALH
	.globl	_RAMP1INC
	.globl	_RAMP1INC2
	.globl	_ROMP
	.globl	_ROMPINC
	.globl	_ROMPINC2
	.globl	_ACC
	.globl	_RAMP0UW
	.globl	_RAMP1UW
	.globl	_ROMPLH
	.globl	_ROMPUW
	.globl	_OFFSETLH
	.globl	_ADP_VPLH
	.globl	_TOV
	.globl	_EA
	.globl	_OV
	.globl	_api_endpage
	.globl	_api_mode
	.globl	_api_rampage
	.globl	_api_fifostart
	.globl	_api_fifoend
	.globl	_api_maxrecopg
	.globl	_api_rec_spi_addr

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_api_rec_stop_noerase
	.globl	_api_rec_start_no_erase
	.globl	_api_rec_prepare_pre_erase
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_ms326apirec_preera_0	udata
r0x11B5:	.ds	1
r0x11C2:	.ds	1
r0x11C3:	.ds	1
r0x11DB:	.ds	1
r0x11E8:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_api_rec_prepare_pre_erase_STK00:	.ds	1
	.globl _api_rec_prepare_pre_erase_STK00
_api_rec_prepare_pre_erase_STK01:	.ds	1
	.globl _api_rec_prepare_pre_erase_STK01
_api_rec_prepare_pre_erase_STK02:	.ds	1
	.globl _api_rec_prepare_pre_erase_STK02
_api_rec_prepare_pre_erase_STK03:	.ds	1
	.globl _api_rec_prepare_pre_erase_STK03
_api_rec_prepare_pre_erase_STK04:	.ds	1
	.globl _api_rec_prepare_pre_erase_STK04
_api_rec_prepare_pre_erase_STK05:	.ds	1
	.globl _api_rec_prepare_pre_erase_STK05
_api_rec_prepare_pre_erase_STK06:	.ds	1
	.globl _api_rec_prepare_pre_erase_STK06
_api_rec_prepare_pre_erase_STK07:	.ds	1
	.globl _api_rec_prepare_pre_erase_STK07
_api_rec_prepare_pre_erase_STK08:	.ds	1
	.globl _api_rec_prepare_pre_erase_STK08
_api_rec_prepare_pre_erase_STK09:	.ds	1
	.globl _api_rec_prepare_pre_erase_STK09
_api_rec_prepare_pre_erase_STK10:	.ds	1
	.globl _api_rec_prepare_pre_erase_STK10
_api_rec_prepare_pre_erase_STK11:	.ds	1
	.globl _api_rec_prepare_pre_erase_STK11
_api_rec_prepare_pre_erase_STK12:	.ds	1
	.globl _api_rec_prepare_pre_erase_STK12
_api_rec_start_no_erase_STK00:	.ds	1
	.globl _api_rec_start_no_erase_STK00
_api_rec_start_no_erase_STK01:	.ds	1
	.globl _api_rec_start_no_erase_STK01
_api_rec_start_no_erase_STK02:	.ds	1
	.globl _api_rec_start_no_erase_STK02
_api_rec_start_no_erase_STK03:	.ds	1
	.globl _api_rec_start_no_erase_STK03
	.globl _api_enter_stdby_mode_STK03
	.globl _api_enter_stdby_mode_STK02
	.globl _api_enter_stdby_mode_STK01
	.globl _api_enter_stdby_mode_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x11E9:NULL+0:-1:1
	;--cdb--W:r0x11E8:NULL+0:-1:1
	;--cdb--W:r0x11EA:NULL+0:-1:1
	;--cdb--W:r0x11E0:NULL+0:-1:1
	;--cdb--W:r0x11E1:NULL+0:-1:1
	;--cdb--W:r0x11E0:NULL+0:4578:0
	;--cdb--W:r0x11C4:NULL+0:-1:1
	;--cdb--W:_api_rec_prepare_pre_erase_STK06:NULL+0:-1:1
	;--cdb--W:_api_rec_prepare_pre_erase_STK05:NULL+0:-1:1
	;--cdb--W:_api_rec_prepare_pre_erase_STK04:NULL+0:-1:1
	;--cdb--W:r0x11C3:NULL+0:-1:1
	;--cdb--W:_api_rec_prepare_pre_erase_STK02:NULL+0:-1:1
	;--cdb--W:_api_rec_prepare_pre_erase_STK00:NULL+0:-1:1
	;--cdb--W:_api_rec_prepare_pre_erase_STK03:NULL+0:-1:1
	;--cdb--W:_api_rec_prepare_pre_erase_STK03:NULL+0:4551:0
	;--cdb--W:_api_rec_prepare_pre_erase_STK05:NULL+0:4547:0
	;--cdb--W:r0x11C3:NULL+0:4554:0
	;--cdb--W:r0x11C4:NULL+0:4553:0
	end
