;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"mbrlen.c"
	.module mbrlen
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fmbrlen$__00000000[({0}S:S$c$0$0({3}DA3d,SC:U),Z,0,0)]
	;--cdb--T:Fmbrlen$tm[]
	;--cdb--S:G$mbrlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--F:G$mbrlen$0$0({2}DF,SI:U),C,0,0,0,0,0
	;--cdb--S:G$wcscmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wcslen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$btowc$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$wctob$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbsinit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbrtowc$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcrtomb$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; mbrlen-code 
.globl _mbrlen

;--------------------------------------------------------
	.FUNC _mbrlen:$PNUM 6:$C:_mbrtowc\
:$L:r0x1160:$L:_mbrlen_STK00:$L:_mbrlen_STK01:$L:_mbrlen_STK02:$L:_mbrlen_STK03\
:$L:_mbrlen_STK04
;--------------------------------------------------------
;	.line	31; "mbrlen.c"	size_t mbrlen(const char *restrict s, size_t n, mbstate_t *restrict ps)
_mbrlen:	;Function start
	STA	r0x1160
;	;.line	35; "mbrlen.c"	return(mbrtowc(0, s, n, ps ? ps : &sps));
	LDA	_mbrlen_STK04
	ORA	_mbrlen_STK03
	JNZ	_00108_DS_
	LDA	#high (_mbrlen_sps_65536_10 + 0)
	STA	_mbrlen_STK03
	LDA	#(_mbrlen_sps_65536_10 + 0)
	STA	_mbrlen_STK04
_00108_DS_:
	LDA	_mbrlen_STK04
	STA	_mbrtowc_STK06
	LDA	_mbrlen_STK03
	STA	_mbrtowc_STK05
	LDA	_mbrlen_STK02
	STA	_mbrtowc_STK04
	LDA	_mbrlen_STK01
	STA	_mbrtowc_STK03
	LDA	_mbrlen_STK00
	STA	_mbrtowc_STK02
	LDA	r0x1160
	STA	_mbrtowc_STK01
	CLRA	
	STA	_mbrtowc_STK00
	CALL	_mbrtowc
;	;.line	36; "mbrlen.c"	}
	RET	
; exit point of _mbrlen
	.ENDFUNC _mbrlen
	;--cdb--S:G$mbrlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcscmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wcslen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$btowc$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$wctob$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbsinit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbrtowc$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcrtomb$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:Lmbrlen.mbrlen$ps$65536$9({2}DG,ST__00000000:S),R,0,0,[_mbrlen_STK04,_mbrlen_STK03]
	;--cdb--S:Lmbrlen.mbrlen$n$65536$9({2}SI:U),R,0,0,[_mbrlen_STK02,_mbrlen_STK01]
	;--cdb--S:Lmbrlen.mbrlen$s$65536$9({2}DG,SC:U),R,0,0,[_mbrlen_STK00,r0x1160]
	;--cdb--S:Lmbrlen.mbrlen$sps$65536$10({3}ST__00000000:S),E,0,0
	;--cdb--S:G$mbrlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_mbrtowc

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_mbrlen
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
_mbrlen_sps_65536_10:	.ds	3

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_mbrlen_0	udata
r0x1160:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_mbrlen_STK00:	.ds	1
	.globl _mbrlen_STK00
_mbrlen_STK01:	.ds	1
	.globl _mbrlen_STK01
_mbrlen_STK02:	.ds	1
	.globl _mbrlen_STK02
_mbrlen_STK03:	.ds	1
	.globl _mbrlen_STK03
_mbrlen_STK04:	.ds	1
	.globl _mbrlen_STK04
	.globl _mbrtowc_STK06
	.globl _mbrtowc_STK05
	.globl _mbrtowc_STK04
	.globl _mbrtowc_STK03
	.globl _mbrtowc_STK02
	.globl _mbrtowc_STK01
	.globl _mbrtowc_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:_mbrlen_STK00:NULL+0:14:0
	;--cdb--W:r0x1160:NULL+0:-1:1
	end
