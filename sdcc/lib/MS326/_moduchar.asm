;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_moduchar.c"
	.module _moduchar
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$_moduchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$_moduchar$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$_moduint$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _moduchar-code 
.globl __moduchar

;--------------------------------------------------------
	.FUNC __moduchar:$PNUM 2:$C:__moduint\
:$L:r0x115F:$L:__moduchar_STK00
;--------------------------------------------------------
;	.line	3; "_moduchar.c"	_moduchar (unsigned char x, unsigned char y)
__moduchar:	;Function start
	STA	r0x115F
;	;.line	5; "_moduchar.c"	return (unsigned char) ((unsigned int)x % (unsigned int)y);
	LDA	__moduchar_STK00
	STA	__moduint_STK02
	CLRA	
	STA	__moduint_STK01
	LDA	r0x115F
	STA	__moduint_STK00
	CLRA	
	CALL	__moduint
	LDA	STK00
;	;.line	6; "_moduchar.c"	}
	RET	
; exit point of __moduchar
	.ENDFUNC __moduchar
	;--cdb--S:G$_moduchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$_moduint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_moduchar._moduchar$y$65536$1({1}SC:U),R,0,0,[__moduchar_STK00]
	;--cdb--S:L_moduchar._moduchar$x$65536$1({1}SC:U),R,0,0,[r0x115F]
	;--cdb--S:G$_moduchar$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__moduint

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__moduchar
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__moduchar_0	udata
r0x115F:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__moduchar_STK00:	.ds	1
	.globl __moduchar_STK00
	.globl __moduint_STK02
	.globl __moduint_STK01
	.globl __moduint_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:__moduchar_STK00:NULL+0:-1:1
	;--cdb--W:r0x115F:NULL+0:4452:0
	;--cdb--W:r0x115F:NULL+0:14:0
	;--cdb--W:r0x1161:NULL+0:4447:0
	;--cdb--W:r0x1161:NULL+0:14:0
	;--cdb--W:r0x1163:NULL+0:0:0
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x1162:NULL+0:0:0
	;--cdb--W:r0x1162:NULL+0:-1:1
	end
