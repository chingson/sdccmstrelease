;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_strncat.c"
	.module _strncat
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--F:G$strncat$0$0({2}DF,DG,SC:U),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _strncat-code 
.globl _strncat

;--------------------------------------------------------
	.FUNC _strncat:$PNUM 6:$L:r0x1160:$L:_strncat_STK00:$L:_strncat_STK01:$L:_strncat_STK02:$L:_strncat_STK03\
:$L:_strncat_STK04:$L:r0x1165:$L:r0x1166:$L:r0x1167:$L:r0x1168\
:$L:r0x1169:$L:r0x116A
;--------------------------------------------------------
;	.line	31; "_strncat.c"	char * strncat (char * front, char * back, size_t count )
_strncat:	;Function start
	STA	r0x1160
;	;.line	33; "_strncat.c"	char *start = front;
	LDA	_strncat_STK00
	STA	r0x1165
	LDA	r0x1160
	STA	r0x1166
_00105_DS_:
;	;.line	35; "_strncat.c"	while (*front++);
	LDA	_strncat_STK00
	STA	_ROMPL
	LDA	r0x1160
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1167
	LDA	_strncat_STK00
	INCA	
	STA	_strncat_STK00
	CLRA	
	ADDC	r0x1160
	STA	r0x1160
	LDA	r0x1167
	JNZ	_00105_DS_
;	;.line	37; "_strncat.c"	front--;
	LDA	_strncat_STK00
	DECA	
	STA	_strncat_STK00
	LDA	#0xff
	ADDC	r0x1160
	STA	r0x1160
;	;.line	39; "_strncat.c"	while (count--)
	LDA	_strncat_STK00
	STA	r0x1167
	LDA	r0x1160
	STA	r0x1168
_00110_DS_:
	LDA	_strncat_STK04
	STA	r0x1169
	LDA	_strncat_STK03
	STA	r0x116A
	LDA	_strncat_STK04
	DECA	
	STA	_strncat_STK04
	LDA	#0xff
	ADDC	_strncat_STK03
	STA	_strncat_STK03
	LDA	r0x1169
	ORA	r0x116A
	JZ	_00112_DS_
;	;.line	40; "_strncat.c"	if (!(*front++ = *back++))
	LDA	_strncat_STK02
	STA	_ROMPL
	LDA	_strncat_STK01
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1169
	LDA	_strncat_STK02
	INCA	
	STA	_strncat_STK02
	CLRA	
	ADDC	_strncat_STK01
	STA	_strncat_STK01
	LDA	r0x1168
	STA	_ROMPH
	LDA	r0x1167
	STA	_ROMPL
	LDA	r0x1169
	STA	@_ROMPINC
	LDA	r0x1167
	INCA	
	STA	r0x1167
	CLRA	
	ADDC	r0x1168
	STA	r0x1168
	LDA	r0x1167
	STA	_strncat_STK00
	LDA	r0x1168
	STA	r0x1160
	LDA	r0x1169
	JNZ	_00110_DS_
;	;.line	41; "_strncat.c"	return(start);
	LDA	r0x1165
	STA	STK00
	LDA	r0x1166
	JMP	_00113_DS_
_00112_DS_:
;	;.line	43; "_strncat.c"	*front = '\0';
	LDA	r0x1160
	STA	_ROMPH
	LDA	_strncat_STK00
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
;	;.line	44; "_strncat.c"	return(start);
	LDA	r0x1165
	STA	STK00
	LDA	r0x1166
_00113_DS_:
;	;.line	45; "_strncat.c"	}
	RET	
; exit point of _strncat
	.ENDFUNC _strncat
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_strncat.strncat$count$65536$21({2}SI:U),R,0,0,[_strncat_STK04,_strncat_STK03]
	;--cdb--S:L_strncat.strncat$back$65536$21({2}DG,SC:U),R,0,0,[_strncat_STK02,_strncat_STK01]
	;--cdb--S:L_strncat.strncat$front$65536$21({2}DG,SC:U),R,0,0,[_strncat_STK00,r0x1160]
	;--cdb--S:L_strncat.strncat$start$65536$22({2}DG,SC:U),R,0,0,[r0x1165,r0x1166]
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_strncat
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__strncat_0	udata
r0x1160:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_strncat_STK00:	.ds	1
	.globl _strncat_STK00
_strncat_STK01:	.ds	1
	.globl _strncat_STK01
_strncat_STK02:	.ds	1
	.globl _strncat_STK02
_strncat_STK03:	.ds	1
	.globl _strncat_STK03
_strncat_STK04:	.ds	1
	.globl _strncat_STK04
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	end
