;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"frexpf.c"
	.module frexpf
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Ffrexpf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$frexpf$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; frexpf-code 
.globl _frexpf

;--------------------------------------------------------
	.FUNC _frexpf:$PNUM 6:$L:r0x1162:$L:_frexpf_STK00:$L:_frexpf_STK01:$L:_frexpf_STK02:$L:_frexpf_STK03\
:$L:_frexpf_STK04:$L:_frexpf_fl_65536_26
;--------------------------------------------------------
;	.line	34; "frexpf.c"	float frexpf(float x, __xdata int *pw2)
_frexpf:	;Function start
	STA	r0x1162
	LDA	_frexpf_STK04
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	39; "frexpf.c"	fl.f=x;
	LDA	_frexpf_STK02
	STA	_frexpf_fl_65536_26
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	_frexpf_STK01
	STA	(_frexpf_fl_65536_26 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	_frexpf_STK00
	STA	(_frexpf_fl_65536_26 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1162
	STA	(_frexpf_fl_65536_26 + 3)
;	;.line	41; "frexpf.c"	i  = ( fl.l >> 23) & 0x000000ff;
	LDA	(_frexpf_fl_65536_26 + 2)
	ROL	
	LDA	(_frexpf_fl_65536_26 + 3)
	ROL	
;	;.line	42; "frexpf.c"	i -= 0x7e;
	ADD	#0x82
	STA	_frexpf_STK02
	CLRA	
	ADDC	#0xff
	STA	_frexpf_STK01
	CLRA	
	ADDC	#0xff
	CLRA	
	ADDC	#0xff
;	;.line	43; "frexpf.c"	*pw2 = i;
	LDA	_frexpf_STK04
	STA	_ROMPL
	LDA	_frexpf_STK03
	STA	_ROMPH
	LDA	_frexpf_STK02
	STA	@_ROMPINC
	LDA	_frexpf_STK01
	STA	@_ROMP
;	;.line	44; "frexpf.c"	fl.l &= 0x807fffff; /* strip all exponent bits */
	LDA	_frexpf_fl_65536_26
	STA	_frexpf_STK02
	LDA	(_frexpf_fl_65536_26 + 1)
	STA	_frexpf_STK01
	LDA	#0x7f
	AND	(_frexpf_fl_65536_26 + 2)
	STA	_frexpf_STK00
	LDA	(_frexpf_fl_65536_26 + 3)
	AND	#0x80
	STA	r0x1162
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
	LDA	_frexpf_STK02
	STA	_frexpf_fl_65536_26
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	_frexpf_STK01
	STA	(_frexpf_fl_65536_26 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	_frexpf_STK00
	STA	(_frexpf_fl_65536_26 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1162
	STA	(_frexpf_fl_65536_26 + 3)
;	;.line	45; "frexpf.c"	fl.l |= 0x3f000000; /* mantissa between 0.5 and 1 */
	LDA	r0x1162
	ORA	#0x3f
	STA	r0x1162
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
	LDA	_frexpf_STK02
	STA	_frexpf_fl_65536_26
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	_frexpf_STK01
	STA	(_frexpf_fl_65536_26 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	_frexpf_STK00
	STA	(_frexpf_fl_65536_26 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1162
	STA	(_frexpf_fl_65536_26 + 3)
;	;.line	46; "frexpf.c"	return(fl.f);
	LDA	_frexpf_fl_65536_26
	STA	STK02
	LDA	(_frexpf_fl_65536_26 + 1)
	STA	STK01
	LDA	(_frexpf_fl_65536_26 + 2)
	STA	STK00
	LDA	(_frexpf_fl_65536_26 + 3)
;	;.line	47; "frexpf.c"	}
	RET	
; exit point of _frexpf
	.ENDFUNC _frexpf
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$errno$0$0({2}SI:S),E,0,0
	;--cdb--S:Lfrexpf.frexpf$pw2$65536$25({2}DX,SI:S),R,0,0,[_frexpf_STK04,_frexpf_STK03]
	;--cdb--S:Lfrexpf.frexpf$x$65536$25({4}SF:S),R,0,0,[_frexpf_STK02,_frexpf_STK01,_frexpf_STK00,r0x1162]
	;--cdb--S:Lfrexpf.frexpf$fl$65536$26({4}STfloat_long:S),E,0,0
	;--cdb--S:Lfrexpf.frexpf$i$65536$26({4}SL:S),R,0,0,[_frexpf_STK02,_frexpf_STK01,_frexpf_STK00,r0x1162]
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_errno

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_frexpf
	.globl	_frexpf_fl_65536_26
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
_frexpf_fl_65536_26:	.ds	4

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_frexpf_0	udata
r0x1162:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_frexpf_STK00:	.ds	1
	.globl _frexpf_STK00
_frexpf_STK01:	.ds	1
	.globl _frexpf_STK01
_frexpf_STK02:	.ds	1
	.globl _frexpf_STK02
_frexpf_STK03:	.ds	1
	.globl _frexpf_STK03
_frexpf_STK04:	.ds	1
	.globl _frexpf_STK04
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:_frexpf_STK02:NULL+0:-1:1
	;--cdb--W:_frexpf_STK01:NULL+0:-1:1
	;--cdb--W:r0x1166:NULL+0:-1:1
	;--cdb--W:r0x1167:NULL+0:-1:1
	;--cdb--W:r0x1168:NULL+0:-1:1
	;--cdb--W:_frexpf_STK00:NULL+0:-1:1
	;--cdb--W:r0x1162:NULL+0:-1:1
	;--cdb--W:r0x1162:NULL+0:0:0
	;--cdb--W:_frexpf_STK02:NULL+0:4453:0
	;--cdb--W:r0x1165:NULL+0:4459:0
	;--cdb--W:r0x1166:NULL+0:4458:0
	;--cdb--W:_frexpf_STK00:NULL+0:0:0
	;--cdb--W:_frexpf_STK01:NULL+0:0:0
	;--cdb--W:r0x1165:NULL+0:-1:1
	end
