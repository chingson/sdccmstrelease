#include "ms326sphlib.h"


void api_enter_stdby_mode(BYTE newpawk, BYTE newpawkdr, BYTE newpbwk, BYTE newpbwkdr, BYTE timeroff) __critical
{

    if(timeroff)
    {
        TIMERC=0; // save a stack!!
        TOV=0;
        goto label2;
label1:
        TIMERC=1;
        return;
    }
label2:        
    PAWKDR=newpawkdr; // DIR set first!!
    PBWKDR=newpbwkdr;

    PAWK=newpawk;
    PBWK=newpbwk;

    SYSC|=SYSC_BIT_STDBY; // following instructions should be enough to halt
    if(timeroff)
        goto label1;
}
