;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.1.6 #323e22ebf (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"C:\work\ms326sphlib"
;;	.file	"api_normalsleep.c"
	.module api_normalsleep
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fapi_normalsleep$spiastru[({0}S:S$addrhm$0$0({2}SI:U),Z,0,0)({2}S:S$addrl$0$0({1}SC:U),Z,0,0)]
	;--cdb--T:Fapi_normalsleep$adps[({0}S:S$predict$0$0({2}SI:S),Z,0,0)({2}S:S$index$0$0({1}SC:U),Z,0,0)]
	;--cdb--T:Fapi_normalsleep$touchen[({0}S:S$toff$0$0({1}SC:U),Z,0,0)({1}S:S$nmossw$0$0({1}SC:U),Z,0,0)({2}S:S$period$0$0({2}SI:U),Z,0,0)({4}S:S$threshold$0$0({2}SI:U),Z,0,0)({6}S:S$count$0$0({2}SI:U),Z,0,0)]
	;--cdb--T:Fapi_normalsleep$pwmleds[({0}S:S$period$0$0({1}SC:U),Z,0,0)({1}S:S$counter$0$0({1}SC:U),Z,0,0)({2}S:S$threshold$0$0({1}SC:U),Z,0,0)]
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_pre_erase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start_no_erase$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop_noerase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_noer$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start2$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_chspick$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_init$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_tk_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_job$0$0({2}DF,SC:U),C,0,0
; *** BIT REGION *** (pseudo)
	.area UDATA (DATA,REL,CON)
; *** BIT REGION ***END
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; api_normalsleep-code 
.globl _api_normal_sleep

;--------------------------------------------------------
	.FUNC _api_normal_sleep:$PNUM 6:$L:r0x1177:$L:_api_normal_sleep_STK00:$L:_api_normal_sleep_STK01:$L:_api_normal_sleep_STK02:$L:_api_normal_sleep_STK03\
:$L:_api_normal_sleep_STK04
;--------------------------------------------------------
;	.line	3; "api_normalsleep.c"	void api_normal_sleep(BYTE newpawk,BYTE newpawkdr, 
_api_normal_sleep:	;Function start
	CRIT	
	STA	r0x1177
	LDA	_api_normal_sleep_STK00
	STA	_PAWKDR
	LDA	_api_normal_sleep_STK02
	STA	_PBWKDR
;	;.line	9; "api_normalsleep.c"	PAWK=newpawk;
	LDA	r0x1177
	STA	_PAWK
;	;.line	10; "api_normalsleep.c"	PBWK=newpbwk;
	LDA	_api_normal_sleep_STK01
	STA	_PBWK
;	;.line	11; "api_normalsleep.c"	ADP_IND=0x80;// following is 8 bit wide
	LDA	#0x80
	STA	_ADP_IND
;	;.line	12; "api_normalsleep.c"	RPAGES=0xf0; // f0// just give a value
	LDA	#0xf0
	STA	_RPAGES
;	;.line	13; "api_normalsleep.c"	PPAGES=0xf0;
	STA	_PPAGES
;	;.line	14; "api_normalsleep.c"	ADP_IND=0;// following is 6 bits only
	CLRA	
	STA	_ADP_IND
;	;.line	15; "api_normalsleep.c"	RPAGES=0x35;// 35
	LDA	#0x35
	STA	_RPAGES
;	;.line	16; "api_normalsleep.c"	PPAGES=0x35;
	STA	_PPAGES
;	;.line	17; "api_normalsleep.c"	RDMAH=0x80;
	LDA	#0x80
	STA	_RDMAH
;	;.line	18; "api_normalsleep.c"	PDMAH=0x80;
	STA	_PDMAH
;	;.line	19; "api_normalsleep.c"	ADCON=1;
	LDA	#0x01
	STA	_ADCON
;	;.line	20; "api_normalsleep.c"	ADCON=0; // clear DMA wakeup bit
	CLRA	
	STA	_ADCON
;	;.line	21; "api_normalsleep.c"	SYSC2&=0x7f; // mbias off
	LDA	#0x7f
	AND	_SYSC2
	STA	_SYSC2
;	;.line	22; "api_normalsleep.c"	DACON=0;
	CLRA	
	STA	_DACON
;	;.line	26; "api_normalsleep.c"	DMA_IL=0x80; // set to middle to prevent wakeup
	LDA	#0x80
	STA	_DMA_IL
;	;.line	27; "api_normalsleep.c"	PAG=0; // this is important not zero will leak!!
	CLRA	
	STA	_PAG
;	;.line	28; "api_normalsleep.c"	SYSC&=~SYSC_BIT_SKCMD;
	LDA	#0xfd
	AND	_SYSC
	STA	_SYSC
_00105_DS_:
;	;.line	29; "api_normalsleep.c"	while(SYSC&SYSC_BIT_SKCMD);
	LDA	_SYSC
	AND	#0x02
	JNZ	_00105_DS_
;	;.line	31; "api_normalsleep.c"	giesave=GIE;
	LDA	_GIE
	STA	r0x1177
;	;.line	32; "api_normalsleep.c"	GIE=2; // WDT keep
	LDA	#0x02
	STA	_GIE
;	;.line	33; "api_normalsleep.c"	EA=0;
	CLRB	_EA
;	;.line	34; "api_normalsleep.c"	DUMMYC=0x20; //RD/PH/OE
	LDA	#0x20
	STA	_DUMMYC
;	;.line	35; "api_normalsleep.c"	TOUCHC=2; // TCIEN
	LDA	#0x02
	STA	_TOUCHC
;	;.line	37; "api_normalsleep.c"	if(lvrdis)
	LDA	_api_normal_sleep_STK03
	JZ	_00111_DS_
;	;.line	39; "api_normalsleep.c"	LVRCON|=0x10;
	LDA	_LVRCON
	ORA	#0x10
	STA	_LVRCON
;	;.line	40; "api_normalsleep.c"	LVRCON|=0x18; // disable later
	ORA	#0x18
	STA	_LVRCON
;	;.line	41; "api_normalsleep.c"	goto label2;
	JMP	_00111_DS_
_00108_DS_:
;	;.line	43; "api_normalsleep.c"	LVRCON&=0xF7;
	LDA	#0xf7
	AND	_LVRCON
;	;.line	44; "api_normalsleep.c"	LVRCON&=0xe7;
	STA	_LVRCON
	AND	#0xe7
	STA	_LVRCON
;	;.line	45; "api_normalsleep.c"	GIE=giesave;
	LDA	r0x1177
	STA	_GIE
;	;.line	46; "api_normalsleep.c"	return;
	JMP	_00114_DS_
_00111_DS_:
;	;.line	50; "api_normalsleep.c"	SYSC=0x05;
	LDA	#0x05
	STA	_SYSC
;	;.line	51; "api_normalsleep.c"	if(!wkrst)
	LDA	_api_normal_sleep_STK04
	JZ	_00108_DS_
;	;.line	54; "api_normalsleep.c"	..asm
	.dw	0x0000
;	;.line	55; "api_normalsleep.c"	..asm
	.dw	0x0000
;	;.line	56; "api_normalsleep.c"	..asm
	.dw	0xffff
;	;.line	57; "api_normalsleep.c"	 ..asm
	
_00114_DS_:
;	;.line	58; "api_normalsleep.c"	}
	NOCRIT	
	RET	
; exit point of _api_normal_sleep
	.ENDFUNC _api_normal_sleep
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_pre_erase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start_no_erase$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop_noerase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_noer$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start2$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_chspick$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_init$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_tk_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$PAR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PADIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOB$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMA_IL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIDAT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCLKDIV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CMPCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTVC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMICON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PRG_RAM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIDMAC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECOAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECLEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECMODE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIOPRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMALEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULB$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULBL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULBH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULO$0$0({4}SL:U),E,0,0
	;--cdb--S:G$MULO1$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULSHIFT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUF$0$0({2}SI:S),E,0,0
	;--cdb--S:G$L2UBUFL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUFH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2USH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$U2LSH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTPRI$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRA$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$LPWMRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMINV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPICK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPSEL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$HWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWDINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PATEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TRAMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMBUFP$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$PASKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBSKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABSKIP$0$0({2}SI:U),E,0,0
	;--cdb--S:G$PATR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTR$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TOUCHC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DUMMYC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IRCD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMDUTY$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMPER$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACGCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECPWR$0$0({4}SL:U),E,0,0
	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$HWPALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0UW$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$EA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OV$0$0({1}SC:U),E,0,0
	;--cdb--S:Lapi_normalsleep.api_normal_sleep$wkrst$65536$38({1}SC:U),R,0,0,[_api_normal_sleep_STK04]
	;--cdb--S:Lapi_normalsleep.api_normal_sleep$lvrdis$65536$38({1}SC:U),R,0,0,[_api_normal_sleep_STK03]
	;--cdb--S:Lapi_normalsleep.api_normal_sleep$newpbwkdr$65536$38({1}SC:U),R,0,0,[]
	;--cdb--S:Lapi_normalsleep.api_normal_sleep$newpbwk$65536$38({1}SC:U),R,0,0,[_api_normal_sleep_STK01]
	;--cdb--S:Lapi_normalsleep.api_normal_sleep$newpawkdr$65536$38({1}SC:U),R,0,0,[]
	;--cdb--S:Lapi_normalsleep.api_normal_sleep$newpawk$65536$38({1}SC:U),R,0,0,[r0x1177]
	;--cdb--S:Lapi_normalsleep.api_normal_sleep$giesave$65536$39({1}SC:U),R,0,0,[r0x1177]
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_PAR
	.globl	_PADIR
	.globl	_PIOA
	.globl	_PAWK
	.globl	_PAWKDR
	.globl	_TIMERC
	.globl	_THRLD
	.globl	_RAMP0L
	.globl	_RAMP0H
	.globl	_RAMP1L
	.globl	_RAMP1H
	.globl	_PTRCL
	.globl	_PTRCH
	.globl	_ROMPL
	.globl	_ROMPH
	.globl	_BEEPC
	.globl	_FILTERGR
	.globl	_ULAWC
	.globl	_STACKL
	.globl	_STACKH
	.globl	_ADCON
	.globl	_DACON
	.globl	_SYSC
	.globl	_SPIM
	.globl	_SPIH
	.globl	_SPIMH
	.globl	_SPIOP
	.globl	_SPI_BANK
	.globl	_ADP_IND
	.globl	_ADP_VPL
	.globl	_ADP_VPH
	.globl	_ADL
	.globl	_ADH
	.globl	_ZC
	.globl	_ADCG
	.globl	_DAC_PL
	.globl	_DAC_PH
	.globl	_PAG
	.globl	_RDMAL
	.globl	_RDMAH
	.globl	_SPIL
	.globl	_IOMASK
	.globl	_IOCMP
	.globl	_IOCNT
	.globl	_LVDCON
	.globl	_LVDCTH
	.globl	_LVRCON
	.globl	_OFFSETL
	.globl	_OFFSETH
	.globl	_RCCON
	.globl	_BGCON
	.globl	_PWRL
	.globl	_CRYPT
	.globl	_PWRH
	.globl	_PWRHL
	.globl	_IROMDL
	.globl	_IROMDH
	.globl	_RECMUTE
	.globl	_SPKC
	.globl	_DCLAMP
	.globl	_SSPIC
	.globl	_SSPIL
	.globl	_SSPIM
	.globl	_SSPIH
	.globl	_PBR
	.globl	_PBDIR
	.globl	_PIOB
	.globl	_PBWK
	.globl	_PBWKDR
	.globl	_PAIE
	.globl	_PBIE
	.globl	_PAIF
	.globl	_PBIF
	.globl	_GIE
	.globl	_GIF
	.globl	_WDTL
	.globl	_WDTH
	.globl	_RPAGES
	.globl	_PPAGES
	.globl	_DMA_IL
	.globl	_FILTERGP
	.globl	_SPIDAT
	.globl	_RSPIC
	.globl	_RCLKDIV
	.globl	_PCR
	.globl	_PCDIR
	.globl	_PIOC
	.globl	_CMPCON
	.globl	_INTVC
	.globl	_INTV
	.globl	_DMICON
	.globl	_PRG_RAM
	.globl	_PDMAL
	.globl	_PDMAH
	.globl	_PDMALH
	.globl	_SPIDMAC
	.globl	_SDMAAL
	.globl	_SDMAAH
	.globl	_SDMAALH
	.globl	_ECRAL
	.globl	_ECRAH
	.globl	_ECRALH
	.globl	_ECOAL
	.globl	_ECOAH
	.globl	_ECOALH
	.globl	_ECLEN
	.globl	_ECCON
	.globl	_ECMODE
	.globl	_SPIOPRAH
	.globl	_SDMALEN
	.globl	_MULA
	.globl	_MULAL
	.globl	_MULAH
	.globl	_MULB
	.globl	_MULBL
	.globl	_MULBH
	.globl	_MULO
	.globl	_MULO1
	.globl	_MULSHIFT
	.globl	_L2UBUF
	.globl	_L2UBUFL
	.globl	_L2UBUFH
	.globl	_ULAWD
	.globl	_L2USH
	.globl	_U2LSH
	.globl	_INTPRI
	.globl	_LPWMRA
	.globl	_LPWMRAL
	.globl	_LPWMRAH
	.globl	_LPWMEN
	.globl	_LPWMINV
	.globl	_SPICK
	.globl	_SYSC2
	.globl	_HWPSEL
	.globl	_HWPAL
	.globl	_HWPAH
	.globl	_HWPA
	.globl	_HWD
	.globl	_HWDINC
	.globl	_PATEN
	.globl	_PBTEN
	.globl	_PABTEN
	.globl	_TRAMAL
	.globl	_TRAMAH
	.globl	_TRAMBUFP
	.globl	_PASKIP
	.globl	_PBSKIP
	.globl	_PABSKIP
	.globl	_PATR
	.globl	_PBTR
	.globl	_PABTR
	.globl	_TOUCHC
	.globl	_DUMMYC
	.globl	_IRCD
	.globl	_FPWMEN
	.globl	_FPWMDUTY
	.globl	_FPWMPER
	.globl	_DACGCL
	.globl	_RECPWR
	.globl	_ICE0
	.globl	_ICE1
	.globl	_ICE2
	.globl	_ICE3
	.globl	_ICE4
	.globl	_RAMP0
	.globl	_RAMP0INC
	.globl	_RAMP1
	.globl	_RDMALH
	.globl	_HWPALH
	.globl	_RAMP1INC
	.globl	_RAMP1INC2
	.globl	_ROMP
	.globl	_ROMPINC
	.globl	_ROMPINC2
	.globl	_ACC
	.globl	_RAMP0UW
	.globl	_RAMP1UW
	.globl	_ROMPLH
	.globl	_ROMPUW
	.globl	_OFFSETLH
	.globl	_ADP_VPLH
	.globl	_TOV
	.globl	_EA
	.globl	_OV

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_api_normal_sleep
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_api_normalsleep_0	udata
r0x1177:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_api_normal_sleep_STK00:	.ds	1
	.globl _api_normal_sleep_STK00
_api_normal_sleep_STK01:	.ds	1
	.globl _api_normal_sleep_STK01
_api_normal_sleep_STK02:	.ds	1
	.globl _api_normal_sleep_STK02
_api_normal_sleep_STK03:	.ds	1
	.globl _api_normal_sleep_STK03
_api_normal_sleep_STK04:	.ds	1
	.globl _api_normal_sleep_STK04
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1177:NULL+0:-1:1
	end
