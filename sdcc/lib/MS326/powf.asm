;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"powf.c"
	.module powf
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fpowf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$powf$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; powf-code 
.globl _powf

;--------------------------------------------------------
	.FUNC _powf:$PNUM 8:$C:___fseq:$C:___fslt:$C:_logf:$C:___fsmul\
:$C:_expf\
:$L:r0x1162:$L:_powf_STK00:$L:_powf_STK01:$L:_powf_STK02:$L:_powf_STK03\
:$L:_powf_STK04:$L:_powf_STK05:$L:_powf_STK06
;--------------------------------------------------------
;	.line	35; "powf.c"	float powf(float x, float y)
_powf:	;Function start
	STA	r0x1162
;	;.line	37; "powf.c"	if(y == 0.0) return 1.0;
	LDA	_powf_STK06
	ORA	_powf_STK05
	ORA	_powf_STK04
	ORA	_powf_STK03
	JNZ	_00106_DS_
	CLRA	
	STA	STK02
	STA	STK01
	LDA	#0x80
	STA	STK00
	LDA	#0x3f
	JMP	_00111_DS_
_00106_DS_:
;	;.line	38; "powf.c"	if(y == 1.0) return x;
	CLRA	
	STA	___fseq_STK06
	STA	___fseq_STK05
	LDA	#0x80
	STA	___fseq_STK04
	LDA	#0x3f
	STA	___fseq_STK03
	LDA	_powf_STK06
	STA	___fseq_STK02
	LDA	_powf_STK05
	STA	___fseq_STK01
	LDA	_powf_STK04
	STA	___fseq_STK00
	LDA	_powf_STK03
	CALL	___fseq
	JZ	_00108_DS_
	LDA	_powf_STK02
	STA	STK02
	LDA	_powf_STK01
	STA	STK01
	LDA	_powf_STK00
	STA	STK00
	LDA	r0x1162
	JMP	_00111_DS_
_00108_DS_:
;	;.line	39; "powf.c"	if(x <= 0.0) return 0.0;
	LDA	_powf_STK02
	STA	___fslt_STK06
	LDA	_powf_STK01
	STA	___fslt_STK05
	LDA	_powf_STK00
	STA	___fslt_STK04
	LDA	r0x1162
	STA	___fslt_STK03
	CLRA	
	STA	___fslt_STK02
	STA	___fslt_STK01
	STA	___fslt_STK00
	CALL	___fslt
	JNZ	_00110_DS_
	CLRA	
	STA	STK02
	STA	STK01
	STA	STK00
	JMP	_00111_DS_
_00110_DS_:
;	;.line	40; "powf.c"	return expf(logf(x) * y);
	LDA	_powf_STK02
	STA	_logf_STK02
	LDA	_powf_STK01
	STA	_logf_STK01
	LDA	_powf_STK00
	STA	_logf_STK00
	LDA	r0x1162
	CALL	_logf
	STA	r0x1162
	LDA	_powf_STK06
	STA	___fsmul_STK06
	LDA	_powf_STK05
	STA	___fsmul_STK05
	LDA	_powf_STK04
	STA	___fsmul_STK04
	LDA	_powf_STK03
	STA	___fsmul_STK03
	LDA	STK02
	STA	___fsmul_STK02
	LDA	STK01
	STA	___fsmul_STK01
	LDA	STK00
	STA	___fsmul_STK00
	LDA	r0x1162
	CALL	___fsmul
	STA	r0x1162
	LDA	STK02
	STA	_expf_STK02
	LDA	STK01
	STA	_expf_STK01
	LDA	STK00
	STA	_expf_STK00
	LDA	r0x1162
	CALL	_expf
_00111_DS_:
;	;.line	41; "powf.c"	}
	RET	
; exit point of _powf
	.ENDFUNC _powf
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$errno$0$0({2}SI:S),E,0,0
	;--cdb--S:Lpowf.powf$y$65536$25({4}SF:S),R,0,0,[_powf_STK06,_powf_STK05,_powf_STK04,_powf_STK03]
	;--cdb--S:Lpowf.powf$x$65536$25({4}SF:S),R,0,0,[_powf_STK02,_powf_STK01,_powf_STK00,r0x1162]
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_expf
	.globl	_logf
	.globl	___fseq
	.globl	___fslt
	.globl	___fsmul
	.globl	_errno

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_powf
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_powf_0	udata
r0x1162:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_powf_STK00:	.ds	1
	.globl _powf_STK00
_powf_STK01:	.ds	1
	.globl _powf_STK01
_powf_STK02:	.ds	1
	.globl _powf_STK02
_powf_STK03:	.ds	1
	.globl _powf_STK03
_powf_STK04:	.ds	1
	.globl _powf_STK04
_powf_STK05:	.ds	1
	.globl _powf_STK05
_powf_STK06:	.ds	1
	.globl _powf_STK06
	.globl ___fseq_STK06
	.globl ___fseq_STK05
	.globl ___fseq_STK04
	.globl ___fseq_STK03
	.globl ___fseq_STK02
	.globl ___fseq_STK01
	.globl ___fseq_STK00
	.globl ___fslt_STK06
	.globl ___fslt_STK05
	.globl ___fslt_STK04
	.globl ___fslt_STK03
	.globl ___fslt_STK02
	.globl ___fslt_STK01
	.globl ___fslt_STK00
	.globl _logf_STK02
	.globl _logf_STK01
	.globl _logf_STK00
	.globl ___fsmul_STK06
	.globl ___fsmul_STK05
	.globl ___fsmul_STK04
	.globl ___fsmul_STK03
	.globl ___fsmul_STK02
	.globl ___fsmul_STK01
	.globl ___fsmul_STK00
	.globl _expf_STK02
	.globl _expf_STK01
	.globl _expf_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:_powf_STK00:NULL+0:14:0
	;--cdb--W:_powf_STK01:NULL+0:13:0
	;--cdb--W:_powf_STK02:NULL+0:12:0
	;--cdb--W:r0x1167:NULL+0:-1:1
	;--cdb--W:r0x1162:NULL+0:-1:1
	end
