;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"isblank.c"
	.module isblank
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isblank$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$_print_format$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$printf_small$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$printf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$vprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$sprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$vsprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$puts$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$getchar$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$putchar$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$islower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isupper$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; isblank-code 
.globl _isblank

;--------------------------------------------------------
	.FUNC _isblank:$PNUM 2:$L:_isblank_STK00:$L:r0x1161
;--------------------------------------------------------
;	.line	53; "/home/chingson/sdccofficial/sdcc/device/include/ctype.h"	return ((unsigned char)c == ' ' || (unsigned char)c == '\t');
_isblank:	;Function start
	LDA	_isblank_STK00
	ADD	#0xe0
	JZ	_00108_DS_
	LDA	_isblank_STK00
	ADD	#0xf7
	JZ	_00108_DS_
	CLRA	
	STA	_isblank_STK00
	JMP	_00109_DS_
_00108_DS_:
	LDA	#0x01
	STA	_isblank_STK00
_00109_DS_:
	LDA	_isblank_STK00
	JPL	_00118_DS_
	LDA	#0xff
	JMP	_00119_DS_
_00118_DS_:
	CLRA	
_00119_DS_:
	STA	r0x1161
	LDA	_isblank_STK00
	STA	STK00
	LDA	r0x1161
;	;.line	54; "/home/chingson/sdccofficial/sdcc/device/include/ctype.h"	}
	RET	
; exit point of _isblank
	.ENDFUNC _isblank
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_print_format$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$printf_small$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$printf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$vprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$sprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$vsprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$puts$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$getchar$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$putchar$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:Lisblank.isblank$c$65536$24({2}SI:S),R,0,0,[_isblank_STK00,r0x1160]
	;--cdb--S:Lisblank.isdigit$c$65536$26({2}SI:S),E,0,0
	;--cdb--S:Lisblank.islower$c$65536$28({2}SI:S),E,0,0
	;--cdb--S:Lisblank.isupper$c$65536$30({2}SI:S),E,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_isblank
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_isblank_0	udata
r0x1161:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_isblank_STK00:	.ds	1
	.globl _isblank_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1160:NULL+0:-1:1
	;--cdb--W:r0x1160:NULL+0:4450:0
	;--cdb--W:r0x1161:NULL+0:4450:0
	end
