;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"mbrtowc.c"
	.module mbrtowc
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fmbrtowc$__00000000[({0}S:S$c$0$0({3}DA3d,SC:U),Z,0,0)]
	;--cdb--T:Fmbrtowc$tm[]
	;--cdb--S:G$mbrtowc$0$0({2}DF,SI:U),C,0,0
	;--cdb--F:G$mbrtowc$0$0({2}DF,SI:U),C,0,0,0,0,0
	;--cdb--S:G$wcscmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wcslen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$btowc$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$wctob$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbsinit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbrlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcrtomb$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; mbrtowc-code 
.globl _mbrtowc

;--------------------------------------------------------
	.FUNC _mbrtowc:$PNUM 8:$C:_mbrtowc:$C:__shr_sshort\
:$L:r0x1161:$L:_mbrtowc_STK00:$L:_mbrtowc_STK01:$L:_mbrtowc_STK02:$L:_mbrtowc_STK03\
:$L:_mbrtowc_STK04:$L:_mbrtowc_STK05:$L:_mbrtowc_STK06:$L:r0x1169:$L:r0x1168\
:$L:r0x116A:$L:r0x116B:$L:_mbrtowc_mbseq_65536_10:$L:r0x116D:$L:r0x116C\
:$L:r0x116E:$L:r0x116F:$L:r0x1170:$L:r0x1171:$L:r0x1172\
:$L:r0x1173:$L:r0x1174
;--------------------------------------------------------
;	.line	32; "mbrtowc.c"	size_t mbrtowc(wchar_t *restrict pwc, const char *restrict s, size_t n, mbstate_t *restrict ps)
_mbrtowc:	;Function start
	STA	r0x1161
;	;.line	41; "mbrtowc.c"	if(!s)
	LDA	_mbrtowc_STK02
	ORA	_mbrtowc_STK01
	JNZ	_00106_DS_
;	;.line	42; "mbrtowc.c"	return(mbrtowc(0, "", 1, ps));
	LDA	_mbrtowc_STK06
	STA	@_RAMP0INC
	LDA	_mbrtowc_STK05
	STA	@_RAMP0INC
	LDA	#0x01
	STA	@_RAMP0INC
	CLRA	
	STA	@_RAMP0INC
	LDA	#(___str_0 + 0)
	STA	@_RAMP0INC
	LDA	#high (___str_0 + 0)
	STA	@_RAMP0INC
	CLRA	
	STA	@_RAMP0INC
	CALL	_mbrtowc
	JMP	_00147_DS_
_00106_DS_:
;	;.line	43; "mbrtowc.c"	if(!n)
	LDA	_mbrtowc_STK04
	ORA	_mbrtowc_STK03
	JZ	_00130_DS_
;	;.line	45; "mbrtowc.c"	if(!ps)
	LDA	_mbrtowc_STK06
	ORA	_mbrtowc_STK05
	JNZ	_00110_DS_
;	;.line	47; "mbrtowc.c"	ps = &sps;
	LDA	#high (_mbrtowc_sps_65536_10 + 0)
	STA	_mbrtowc_STK05
	LDA	#(_mbrtowc_sps_65536_10 + 0)
	STA	_mbrtowc_STK06
_00110_DS_:
;	;.line	50; "mbrtowc.c"	for(i = 0; ps->c[i] && i < 3; i++)
	CLRA	
	STA	r0x1168
_00133_DS_:
	LDA	r0x1168
	ADD	_mbrtowc_STK06
	STA	r0x1169
	CLRA	
	ADDC	_mbrtowc_STK05
	STA	r0x116A
	LDA	r0x1169
	STA	_ROMPL
	LDA	r0x116A
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x116B
	JZ	_00175_DS_
	LDA	r0x1168
	ADD	#0xfd
	JC	_00175_DS_
;	;.line	51; "mbrtowc.c"	mbseq[i] = ps->c[i];
	LDA	r0x1168
	ADD	#(_mbrtowc_mbseq_65536_10 + 0)
	STA	r0x1169
	CLRA	
	ADDC	#high (_mbrtowc_mbseq_65536_10 + 0)
	STA	r0x116A
	LDA	r0x1169
	STA	_ROMPL
	LDA	r0x116A
	STA	_ROMPH
	LDA	r0x116B
	STA	@_ROMP
;	;.line	50; "mbrtowc.c"	for(i = 0; ps->c[i] && i < 3; i++)
	LDA	r0x1168
	INCA	
	STA	r0x1168
	JMP	_00133_DS_
_00175_DS_:
;	;.line	53; "mbrtowc.c"	seqlen = 1;
	LDA	#0x01
	STA	r0x1169
;	;.line	54; "mbrtowc.c"	first_byte = ps->c[0] ? ps->c[0] : *s;
	LDA	_mbrtowc_STK06
	STA	_ROMPL
	LDA	_mbrtowc_STK05
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x116A
	JNZ	_00150_DS_
	LDA	_mbrtowc_STK02
	STA	_ROMPL
	LDA	_mbrtowc_STK01
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x116A
_00150_DS_:
;	;.line	56; "mbrtowc.c"	if(first_byte & 0x80)
	LDA	r0x116A
	JPL	_00116_DS_
;	;.line	58; "mbrtowc.c"	while (first_byte & (0x80 >> seqlen))
	LDA	#0x01
	STA	r0x116B
_00112_DS_:
	CLRA	
	STA	STK00
	LDA	#0x80
	STA	_PTRCL
	LDA	r0x116B
	CALL	__shr_sshort
	LDA	_PTRCL
	AND	r0x116A
	STA	r0x116C
	CLRA	
	AND	STK00
	ORA	r0x116C
	JZ	_00176_DS_
;	;.line	59; "mbrtowc.c"	seqlen++;
	LDA	r0x116B
	INCA	
	STA	r0x116B
	JMP	_00112_DS_
_00176_DS_:
	LDA	r0x116B
	STA	r0x1169
;	;.line	60; "mbrtowc.c"	first_byte &= (0xff >> (seqlen + 1));
	LDA	r0x116B
	INCA	
	STA	r0x116B
	CLRA	
	STA	STK00
	DECA	
	STA	_PTRCL
	LDA	r0x116B
	CALL	__shr_sshort
	LDA	_PTRCL
	AND	r0x116A
	STA	r0x116A
_00116_DS_:
;	;.line	63; "mbrtowc.c"	if(seqlen > 4)
	SETB	_C
	LDA	#0x04
	SUBB	r0x1169
	JNC	_00130_DS_
;	;.line	66; "mbrtowc.c"	if(i + n < seqlen) // Incomplete multibyte character
	LDA	r0x1168
	STA	r0x116B
	CLRA	
	STA	r0x116C
	LDA	r0x116B
	ADD	_mbrtowc_STK04
	STA	r0x116D
	LDA	r0x116C
	ADDC	_mbrtowc_STK03
	STA	r0x116E
	SETB	_C
	LDA	r0x116D
	SUBB	r0x1169
	LDA	r0x116E
	SUBB	#0x00
	JC	_00166_DS_
;	;.line	69; "mbrtowc.c"	ps->c[i] = *s++;
	LDA	_mbrtowc_STK02
	STA	r0x116D
	LDA	_mbrtowc_STK01
	STA	r0x116E
	LDA	r0x1168
	STA	r0x116F
_00136_DS_:
;	;.line	68; "mbrtowc.c"	for(;n-- ; i++)
	LDA	_mbrtowc_STK04
	STA	r0x1170
	LDA	_mbrtowc_STK03
	STA	r0x1171
	LDA	_mbrtowc_STK04
	DECA	
	STA	_mbrtowc_STK04
	LDA	#0xff
	ADDC	_mbrtowc_STK03
	STA	_mbrtowc_STK03
	LDA	r0x1170
	ORA	r0x1171
	JZ	_00119_DS_
;	;.line	69; "mbrtowc.c"	ps->c[i] = *s++;
	LDA	r0x116F
	ADD	_mbrtowc_STK06
	STA	r0x1170
	CLRA	
	ADDC	_mbrtowc_STK05
	STA	r0x1171
	LDA	r0x116D
	STA	_ROMPL
	LDA	r0x116E
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1172
	LDA	r0x116D
	INCA	
	STA	r0x116D
	CLRA	
	ADDC	r0x116E
	STA	r0x116E
	LDA	r0x1171
	STA	_ROMPH
	LDA	r0x1170
	STA	_ROMPL
	LDA	r0x1172
	STA	@_ROMPINC
;	;.line	68; "mbrtowc.c"	for(;n-- ; i++)
	LDA	r0x116F
	INCA	
	STA	r0x116F
	JMP	_00136_DS_
_00119_DS_:
;	;.line	70; "mbrtowc.c"	return(-2);
	LDA	#0xfe
	STA	STK00
	INCA	
	JMP	_00147_DS_
_00166_DS_:
;	;.line	73; "mbrtowc.c"	for(j = 0; j < i; j++)
	CLRA	
	STA	_mbrtowc_STK04
_00139_DS_:
	SETB	_C
	LDA	_mbrtowc_STK04
	SUBB	r0x1168
	JC	_00122_DS_
;	;.line	74; "mbrtowc.c"	ps->c[j] = 0;
	LDA	_mbrtowc_STK04
	ADD	_mbrtowc_STK06
	STA	_mbrtowc_STK03
	CLRA	
	ADDC	_mbrtowc_STK05
	STA	_ROMPH
	LDA	_mbrtowc_STK03
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
;	;.line	73; "mbrtowc.c"	for(j = 0; j < i; j++)
	LDA	_mbrtowc_STK04
	INCA	
	STA	_mbrtowc_STK04
	JMP	_00139_DS_
_00122_DS_:
;	;.line	76; "mbrtowc.c"	for(n = 1, i = i ? i : 1; i < seqlen; i++, n++)
	LDA	#0x01
	STA	_mbrtowc_STK04
	CLRA	
	STA	_mbrtowc_STK03
	LDA	r0x1168
	JZ	_00151_DS_
	LDA	r0x116B
	STA	_mbrtowc_STK06
	LDA	r0x116C
	JMP	_00152_DS_
_00151_DS_:
	LDA	#0x01
	STA	_mbrtowc_STK06
_00152_DS_:
	LDA	_mbrtowc_STK06
	STA	r0x1168
	LDA	#0x01
	STA	_mbrtowc_STK06
	CLRA	
	STA	_mbrtowc_STK05
_00142_DS_:
	SETB	_C
	LDA	r0x1168
	SUBB	r0x1169
	JC	_00125_DS_
;	;.line	78; "mbrtowc.c"	mbseq[i] = *s++;
	LDA	r0x1168
	ADD	#(_mbrtowc_mbseq_65536_10 + 0)
	STA	r0x116B
	CLRA	
	ADDC	#high (_mbrtowc_mbseq_65536_10 + 0)
	STA	r0x116C
	LDA	_mbrtowc_STK02
	STA	_ROMPL
	LDA	_mbrtowc_STK01
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x116D
	LDA	_mbrtowc_STK02
	INCA	
	STA	_mbrtowc_STK02
	CLRA	
	ADDC	_mbrtowc_STK01
	STA	_mbrtowc_STK01
	LDA	r0x116B
	STA	_ROMPL
	LDA	r0x116C
	STA	_ROMPH
	LDA	r0x116D
	STA	@_ROMP
;	;.line	79; "mbrtowc.c"	if((mbseq[i] & 0xc0) != 0x80)
	LDA	#0xc0
	AND	r0x116D
	XOR	#0x80
	JNZ	_00130_DS_
;	;.line	76; "mbrtowc.c"	for(n = 1, i = i ? i : 1; i < seqlen; i++, n++)
	LDA	r0x1168
	INCA	
	STA	r0x1168
	LDA	_mbrtowc_STK06
	INCA	
	STA	_mbrtowc_STK06
	CLRA	
	ADDC	_mbrtowc_STK05
	STA	_mbrtowc_STK05
	LDA	_mbrtowc_STK06
	STA	_mbrtowc_STK04
	LDA	_mbrtowc_STK05
	STA	_mbrtowc_STK03
	JMP	_00142_DS_
_00125_DS_:
;	;.line	83; "mbrtowc.c"	codepoint = first_byte;
	LDA	r0x116A
	STA	_mbrtowc_STK02
	CLRA	
	STA	_mbrtowc_STK01
	STA	_mbrtowc_STK06
	STA	_mbrtowc_STK05
;	;.line	85; "mbrtowc.c"	for(s = mbseq + 1, seqlen--; seqlen; seqlen--)
	LDA	#high (_mbrtowc_mbseq_65536_10 + 1)
	STA	r0x116A
	LDA	#(_mbrtowc_mbseq_65536_10 + 1)
	STA	r0x1168
	LDA	r0x1169
	DECA	
	STA	r0x1169
_00145_DS_:
	LDA	r0x1169
	JZ	_00126_DS_
;	;.line	87; "mbrtowc.c"	codepoint <<= 6;
	LDA	_mbrtowc_STK05
	SWA	
	AND	#0xf0
	STA	r0x116E
	LDA	_mbrtowc_STK06
	SWA	
	STA	_PTRCL
	AND	#0x0f
	ORA	r0x116E
	STA	r0x116E
	LDA	_PTRCL
	AND	#0xf0
	STA	r0x116D
	LDA	_mbrtowc_STK01
	SWA	
	STA	_PTRCL
	AND	#0x0f
	ORA	r0x116D
	STA	r0x116D
	LDA	_PTRCL
	AND	#0xf0
	STA	r0x116C
	LDA	_mbrtowc_STK02
	SWA	
	STA	_PTRCL
	AND	#0x0f
	ORA	r0x116C
	STA	r0x116C
	LDA	_PTRCL
	AND	#0xf0
	SHL	
	STA	r0x116B
	LDA	r0x116C
	ROL	
	STA	r0x116C
	LDA	r0x116D
	ROL	
	STA	r0x116D
	LDA	r0x116E
	ROL	
	STA	r0x116E
	LDA	r0x116B
	SHL	
	STA	r0x116B
	LDA	r0x116C
	ROL	
	STA	r0x116C
	LDA	r0x116D
	ROL	
	STA	r0x116D
	LDA	r0x116E
	ROL	
	STA	r0x116E
;	;.line	88; "mbrtowc.c"	codepoint |= (*s & 0x3f);
	LDA	r0x1168
	STA	_ROMPL
	LDA	r0x116A
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x3f
	STA	r0x116F
	CLRA	
	JPL	_00322_DS_
	LDA	#0xff
	JMP	_00323_DS_
_00322_DS_:
	CLRA	
_00323_DS_:
	STA	r0x1173
	STA	r0x1174
	LDA	r0x116F
	ORA	r0x116B
	STA	_mbrtowc_STK02
	LDA	r0x116C
	STA	_mbrtowc_STK01
	LDA	r0x1173
	ORA	r0x116D
	STA	_mbrtowc_STK06
	LDA	r0x1174
	ORA	r0x116E
	STA	_mbrtowc_STK05
;	;.line	89; "mbrtowc.c"	s++;
	LDA	r0x1168
	INCA	
	STA	r0x1168
	CLRA	
	ADDC	r0x116A
	STA	r0x116A
;	;.line	85; "mbrtowc.c"	for(s = mbseq + 1, seqlen--; seqlen; seqlen--)
	LDA	r0x1169
	DECA	
	STA	r0x1169
	JMP	_00145_DS_
_00126_DS_:
;	;.line	92; "mbrtowc.c"	if(codepoint >= 0xd800 && codepoint <= 0xdfff) // UTF-16 surrogate.
	CLRB	_C
	LDA	_mbrtowc_STK01
	ADDC	#0x28
	LDA	_mbrtowc_STK06
	ADDC	#0xff
	LDA	_mbrtowc_STK05
	ADDC	#0xff
	JNC	_00128_DS_
	SETB	_C
	LDA	#0xff
	SUBB	_mbrtowc_STK02
	LDA	#0xdf
	SUBB	_mbrtowc_STK01
	CLRA	
	SUBB	_mbrtowc_STK06
	CLRA	
	SUBB	_mbrtowc_STK05
	JNC	_00128_DS_
;	;.line	93; "mbrtowc.c"	return(-1);
	LDA	#0xff
	STA	STK00
	JMP	_00147_DS_
_00128_DS_:
;	;.line	95; "mbrtowc.c"	*pwc = codepoint;
	LDA	r0x1161
	STA	_ROMPH
	LDA	_mbrtowc_STK00
	STA	_ROMPL
	LDA	_mbrtowc_STK02
	STA	@_ROMPINC
	LDA	_mbrtowc_STK01
	STA	@_ROMPINC
	LDA	_mbrtowc_STK06
	STA	@_ROMPINC
	LDA	_mbrtowc_STK05
	STA	@_ROMPINC
;	;.line	96; "mbrtowc.c"	return(n);
	LDA	_mbrtowc_STK04
	STA	STK00
	LDA	_mbrtowc_STK03
	JMP	_00147_DS_
_00130_DS_:
;	;.line	99; "mbrtowc.c"	errno = EILSEQ;
	LDA	#0x54
	STA	_errno
	CLRA	
	STA	(_errno + 1)
;	;.line	100; "mbrtowc.c"	return(-1);
	DECA	
	STA	STK00
	LDA	#0xff
_00147_DS_:
;	;.line	101; "mbrtowc.c"	}
	RET	
; exit point of _mbrtowc
	.ENDFUNC _mbrtowc
	;--cdb--S:G$mbrtowc$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcscmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wcslen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$btowc$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$wctob$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbsinit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbrlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcrtomb$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$errno$0$0({2}SI:S),E,0,0
	;--cdb--S:Lmbrtowc.mbrtowc$ps$65536$9({2}DG,ST__00000000:S),R,0,0,[_mbrtowc_STK06,_mbrtowc_STK05]
	;--cdb--S:Lmbrtowc.mbrtowc$n$65536$9({2}SI:U),R,0,0,[_mbrtowc_STK04,_mbrtowc_STK03]
	;--cdb--S:Lmbrtowc.mbrtowc$s$65536$9({2}DG,SC:U),R,0,0,[_mbrtowc_STK02,_mbrtowc_STK01]
	;--cdb--S:Lmbrtowc.mbrtowc$pwc$65536$9({2}DG,SL:U),R,0,0,[_mbrtowc_STK00,r0x1161]
	;--cdb--S:Lmbrtowc.mbrtowc$first_byte$65536$10({1}SC:U),R,0,0,[r0x116A]
	;--cdb--S:Lmbrtowc.mbrtowc$seqlen$65536$10({1}SC:U),R,0,0,[]
	;--cdb--S:Lmbrtowc.mbrtowc$mbseq$65536$10({4}DA4d,SC:U),E,0,0
	;--cdb--S:Lmbrtowc.mbrtowc$codepoint$65536$10({4}SL:U),R,0,0,[r0x116B,r0x116C,r0x116D,r0x116E]
	;--cdb--S:Lmbrtowc.mbrtowc$i$65536$10({1}SC:U),R,0,0,[r0x1168]
	;--cdb--S:Lmbrtowc.mbrtowc$j$65536$10({1}SC:U),R,0,0,[_mbrtowc_STK04]
	;--cdb--S:Lmbrtowc.mbrtowc$sps$65536$10({3}ST__00000000:S),E,0,0
	;--cdb--S:Fmbrtowc$__str_0$0$0({1}DA1d,SC:U),D,0,0
	;--cdb--S:G$mbrtowc$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_errno
	.globl	__shr_sshort

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_mbrtowc
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
_mbrtowc_sps_65536_10:	.ds	3

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_mbrtowc_0	udata
r0x1161:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116C:	.ds	1
r0x116D:	.ds	1
r0x116E:	.ds	1
r0x116F:	.ds	1
r0x1170:	.ds	1
r0x1171:	.ds	1
r0x1172:	.ds	1
r0x1173:	.ds	1
r0x1174:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_mbrtowc_STK00:	.ds	1
	.globl _mbrtowc_STK00
_mbrtowc_STK01:	.ds	1
	.globl _mbrtowc_STK01
_mbrtowc_STK02:	.ds	1
	.globl _mbrtowc_STK02
_mbrtowc_STK03:	.ds	1
	.globl _mbrtowc_STK03
_mbrtowc_STK04:	.ds	1
	.globl _mbrtowc_STK04
_mbrtowc_STK05:	.ds	1
	.globl _mbrtowc_STK05
_mbrtowc_STK06:	.ds	1
	.globl _mbrtowc_STK06
_mbrtowc_mbseq_65536_10:	.ds	4
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------

	.area	SEGC	(CODE) ;mbrtowc-0-code

___str_0:
	.db 0x00 ; '.'
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x116D:NULL+0:-1:1
	;--cdb--W:_mbrtowc_STK05:NULL+0:-1:1
	;--cdb--W:r0x116C:NULL+0:-1:1
	;--cdb--W:r0x1171:NULL+0:-1:1
	;--cdb--W:r0x1168:NULL+0:14:0
	;--cdb--W:r0x116B:NULL+0:4460:0
	;--cdb--W:r0x116B:NULL+0:4461:0
	;--cdb--W:r0x116D:NULL+0:0:0
	;--cdb--W:r0x116D:NULL+0:14:0
	;--cdb--W:r0x116D:NULL+0:4459:0
	;--cdb--W:r0x116C:NULL+0:128:0
	;--cdb--W:r0x116C:NULL+0:255:0
	;--cdb--W:r0x116E:NULL+0:4458:0
	;--cdb--W:r0x116E:NULL+0:4460:0
	;--cdb--W:r0x116E:NULL+0:0:0
	;--cdb--W:r0x116F:NULL+0:0:0
	;--cdb--W:r0x116F:NULL+0:4457:0
	;--cdb--W:r0x1170:NULL+0:0:0
	;--cdb--W:r0x1170:NULL+0:4463:0
	;--cdb--W:r0x1171:NULL+0:4466:0
	;--cdb--W:r0x1172:NULL+0:0:0
	;--cdb--W:r0x1169:NULL+0:-1:1
	;--cdb--W:r0x1168:NULL+0:-1:1
	;--cdb--W:r0x116F:NULL+0:-1:1
	;--cdb--W:r0x1170:NULL+0:-1:1
	;--cdb--W:r0x116E:NULL+0:-1:1
	;--cdb--W:r0x1172:NULL+0:-1:1
	;--cdb--W:r0x116B:NULL+0:4456:0
	;--cdb--W:r0x116C:NULL+0:0:0
	end
