;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_rrslonglong.c"
	.module _rrslonglong
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$_rrslonglong$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$_rrslonglong$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _rrslonglong-code 
.globl __rrslonglong

;--------------------------------------------------------
	.FUNC __rrslonglong:$PNUM 9:$C:__shr_ulong:$C:__shr_ushort\
:$L:__rrslonglong_STK00:$L:__rrslonglong_STK01:$L:__rrslonglong_STK02:$L:__rrslonglong_STK03:$L:__rrslonglong_STK04\
:$L:__rrslonglong_STK05:$L:__rrslonglong_STK06:$L:__rrslonglong_STK07:$L:r0x115F:$L:__rrslonglong_l_65536_1\
:$L:r0x1161:$L:r0x1160:$L:r0x1163:$L:r0x1162:$L:r0x1165\
:$L:r0x1164:$L:r0x1167:$L:r0x1166:$L:r0x1168:$L:r0x116A\
:$L:r0x116B:$L:r0x116C:$L:r0x116D:$L:r0x116E:$L:r0x116F\

;--------------------------------------------------------
;	.line	35; "_rrslonglong.c"	long long _rrslonglong(long long l, unsigned char s)
__rrslonglong:	;Function start
	STA	(__rrslonglong_l_65536_1 + 7)
	LDA	__rrslonglong_STK00
	STA	(__rrslonglong_l_65536_1 + 6)
	LDA	__rrslonglong_STK01
	STA	(__rrslonglong_l_65536_1 + 5)
	LDA	__rrslonglong_STK02
	STA	(__rrslonglong_l_65536_1 + 4)
	LDA	__rrslonglong_STK03
	STA	(__rrslonglong_l_65536_1 + 3)
	LDA	__rrslonglong_STK04
	STA	(__rrslonglong_l_65536_1 + 2)
	LDA	__rrslonglong_STK05
	STA	(__rrslonglong_l_65536_1 + 1)
	LDA	__rrslonglong_STK06
	STA	__rrslonglong_l_65536_1
	LDA	__rrslonglong_STK07
	STA	r0x115F
;	;.line	37; "_rrslonglong.c"	int32_t *top = (uint32_t *)((char *)(&l) + 4);
	LDA	#(__rrslonglong_l_65536_1 + 0)
	ADD	#0x04
	STA	r0x1160
	CLRA	
	ADDC	#high (__rrslonglong_l_65536_1 + 0)
	STA	r0x1163
	LDA	r0x1160
	STA	r0x1162
;	;.line	38; "_rrslonglong.c"	uint16_t *middle = (uint16_t *)((char *)(&l) + 3);
	LDA	#(__rrslonglong_l_65536_1 + 0)
	ADD	#0x03
	STA	r0x1160
	CLRA	
	ADDC	#high (__rrslonglong_l_65536_1 + 0)
	STA	r0x1165
	LDA	r0x1160
	STA	r0x1164
;	;.line	39; "_rrslonglong.c"	uint32_t *bottom = (uint32_t *)(&l);
	LDA	#high (__rrslonglong_l_65536_1 + 0)
	STA	r0x1167
	LDA	#(__rrslonglong_l_65536_1 + 0)
	STA	r0x1166
;	;.line	40; "_rrslonglong.c"	uint16_t *b = (uint16_t *)(&l);
	ADD	#0x06
	STA	r0x1160
	CLRA	
	ADDC	#high (__rrslonglong_l_65536_1 + 0)
	STA	r0x1161
_00107_DS_:
;	;.line	42; "_rrslonglong.c"	for(;s >= 16; s-= 16)
	LDA	r0x115F
	ADD	#0xf0
	JNC	_00105_DS_
;	;.line	44; "_rrslonglong.c"	b[0] = b[1];
	LDA	#0x02
	ADD	#(__rrslonglong_l_65536_1 + 0)
	STA	r0x116A
	CLRA	
	ADDC	#high (__rrslonglong_l_65536_1 + 0)
	STA	r0x116B
	LDA	r0x116A
	STA	_ROMPL
	LDA	r0x116B
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x116C
	LDA	@_ROMPINC
	STA	r0x116D
	LDA	#high (__rrslonglong_l_65536_1 + 0)
	STA	_ROMPH
	LDA	#(__rrslonglong_l_65536_1 + 0)
	STA	_ROMPL
	LDA	r0x116C
	STA	@_ROMPINC
	LDA	r0x116D
	STA	@_ROMPINC
;	;.line	45; "_rrslonglong.c"	b[1] = b[2];
	LDA	#0x04
	ADD	#(__rrslonglong_l_65536_1 + 0)
	STA	r0x116C
	CLRA	
	ADDC	#high (__rrslonglong_l_65536_1 + 0)
	STA	r0x116D
	LDA	r0x116C
	STA	_ROMPL
	LDA	r0x116D
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x116E
	LDA	@_ROMPINC
	STA	r0x116F
	LDA	r0x116B
	STA	_ROMPH
	LDA	r0x116A
	STA	_ROMPL
	LDA	r0x116E
	STA	@_ROMPINC
	LDA	r0x116F
	STA	@_ROMPINC
;	;.line	46; "_rrslonglong.c"	b[2] = b[3];
	LDA	r0x1160
	STA	_ROMPL
	LDA	r0x1161
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x116A
	LDA	@_ROMPINC
	STA	r0x116B
	LDA	r0x116D
	STA	_ROMPH
	LDA	r0x116C
	STA	_ROMPL
	LDA	r0x116A
	STA	@_ROMPINC
	LDA	r0x116B
	STA	@_ROMPINC
;	;.line	47; "_rrslonglong.c"	b[3] = (b[3] & 0x8000) ? 0xffff : 0x000000;
	LDA	r0x1160
	STA	r0x116A
	LDA	r0x1161
	STA	r0x116B
	LDA	r0x1160
	STA	_ROMPL
	LDA	r0x1161
	STA	_ROMPH
	LDA	@_ROMPINC
	LDA	@_ROMPINC
	JPL	_00111_DS_
	LDA	#0xff
	STA	r0x116C
	STA	r0x116D
	JMP	_00112_DS_
_00111_DS_:
	CLRA	
	STA	r0x116C
	STA	r0x116D
_00112_DS_:
	LDA	r0x116B
	STA	_ROMPH
	LDA	r0x116A
	STA	_ROMPL
	LDA	r0x116C
	STA	@_ROMPINC
	LDA	r0x116D
	STA	@_ROMPINC
;	;.line	42; "_rrslonglong.c"	for(;s >= 16; s-= 16)
	LDA	r0x115F
	ADD	#0xf0
	STA	r0x115F
	JMP	_00107_DS_
_00105_DS_:
;	;.line	50; "_rrslonglong.c"	(*bottom) >>= s;
	LDA	r0x1166
	STA	_ROMPL
	LDA	r0x1167
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1160
	LDA	@_ROMPINC
	STA	r0x1161
	LDA	@_ROMPINC
	STA	r0x1168
	LDA	@_ROMPINC
	STA	STK00
	LDA	r0x1168
	STA	STK01
	LDA	r0x1161
	STA	STK02
	LDA	r0x1160
	STA	_PTRCL
	LDA	r0x115F
	CALL	__shr_ulong
	LDA	STK00
	STA	r0x116D
	LDA	STK01
	STA	r0x116C
	LDA	STK02
	STA	r0x116B
	LDA	_PTRCL
	STA	r0x116A
	LDA	r0x1167
	STA	_ROMPH
	LDA	r0x1166
	STA	_ROMPL
	LDA	r0x116A
	STA	@_ROMPINC
	LDA	r0x116B
	STA	@_ROMPINC
	LDA	r0x116C
	STA	@_ROMPINC
	LDA	r0x116D
	STA	@_ROMPINC
;	;.line	51; "_rrslonglong.c"	(*bottom) |= ((uint32_t)((*middle) >> s) << 16);
	LDA	r0x1164
	STA	_ROMPL
	LDA	r0x1165
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1160
	LDA	@_ROMPINC
	STA	STK00
	LDA	r0x1160
	STA	_PTRCL
	LDA	r0x115F
	CALL	__shr_ushort
	LDA	_PTRCL
	ORA	r0x116C
	STA	r0x116C
	LDA	STK00
	ORA	r0x116D
	STA	r0x116D
	LDA	r0x1167
	STA	_ROMPH
	LDA	r0x1166
	STA	_ROMPL
	LDA	r0x116A
	STA	@_ROMPINC
	LDA	r0x116B
	STA	@_ROMPINC
	LDA	r0x116C
	STA	@_ROMPINC
	LDA	r0x116D
	STA	@_ROMPINC
;	;.line	52; "_rrslonglong.c"	(*top) |= (((*middle) & 0xffff0000) >> s);
	LDA	r0x1162
	STA	_ROMPL
	LDA	r0x1163
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1160
	LDA	@_ROMPINC
	STA	r0x1161
	LDA	@_ROMPINC
	STA	r0x1166
	LDA	@_ROMPINC
	STA	r0x1167
	LDA	r0x1164
	STA	_ROMPL
	LDA	r0x1165
	STA	_ROMPH
	LDA	@_ROMPINC
	LDA	@_ROMPINC
	CLRA	
	STA	STK00
	STA	STK01
	STA	STK02
	STA	_PTRCL
	LDA	r0x115F
	CALL	__shr_ulong
	LDA	_PTRCL
	ORA	r0x1160
	STA	r0x1160
	LDA	STK02
	ORA	r0x1161
	STA	r0x1161
	LDA	STK01
	ORA	r0x1166
	STA	r0x1166
	LDA	STK00
	ORA	r0x1167
	STA	r0x1167
	LDA	r0x1163
	STA	_ROMPH
	LDA	r0x1162
	STA	_ROMPL
	LDA	r0x1160
	STA	@_ROMPINC
	LDA	r0x1161
	STA	@_ROMPINC
	LDA	r0x1166
	STA	@_ROMPINC
	LDA	r0x1167
	STA	@_ROMPINC
;	;.line	54; "_rrslonglong.c"	return(l);
	LDA	__rrslonglong_l_65536_1
	STA	STK06
	LDA	(__rrslonglong_l_65536_1 + 1)
	STA	STK05
	LDA	(__rrslonglong_l_65536_1 + 2)
	STA	STK04
	LDA	(__rrslonglong_l_65536_1 + 3)
	STA	STK03
	LDA	(__rrslonglong_l_65536_1 + 4)
	STA	STK02
	LDA	(__rrslonglong_l_65536_1 + 5)
	STA	STK01
	LDA	(__rrslonglong_l_65536_1 + 6)
	STA	STK00
	LDA	(__rrslonglong_l_65536_1 + 7)
;	;.line	55; "_rrslonglong.c"	}
	RET	
; exit point of __rrslonglong
	.ENDFUNC __rrslonglong
	;--cdb--S:G$_rrslonglong$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:L_rrslonglong._rrslonglong$s$65536$1({1}SC:U),R,0,0,[r0x115F]
	;--cdb--S:L_rrslonglong._rrslonglong$l$65536$1({8}SI:S),E,0,0
	;--cdb--S:L_rrslonglong._rrslonglong$top$65536$2({2}DG,SL:S),R,0,0,[r0x1162,r0x1163]
	;--cdb--S:L_rrslonglong._rrslonglong$middle$65536$2({2}DG,SI:U),R,0,0,[r0x1164,r0x1165]
	;--cdb--S:L_rrslonglong._rrslonglong$bottom$65536$2({2}DG,SL:U),R,0,0,[r0x1166,r0x1167]
	;--cdb--S:L_rrslonglong._rrslonglong$b$65536$2({2}DG,SI:U),R,0,0,[r0x1168,r0x1169]
	;--cdb--S:G$_rrslonglong$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__shr_ulong
	.globl	__shr_ushort

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__rrslonglong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__rrslonglong_0	udata
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116C:	.ds	1
r0x116D:	.ds	1
r0x116E:	.ds	1
r0x116F:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__rrslonglong_l_65536_1:	.ds	8
__rrslonglong_STK00:	.ds	1
	.globl __rrslonglong_STK00
__rrslonglong_STK01:	.ds	1
	.globl __rrslonglong_STK01
__rrslonglong_STK02:	.ds	1
	.globl __rrslonglong_STK02
__rrslonglong_STK03:	.ds	1
	.globl __rrslonglong_STK03
__rrslonglong_STK04:	.ds	1
	.globl __rrslonglong_STK04
__rrslonglong_STK05:	.ds	1
	.globl __rrslonglong_STK05
__rrslonglong_STK06:	.ds	1
	.globl __rrslonglong_STK06
__rrslonglong_STK07:	.ds	1
	.globl __rrslonglong_STK07
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1160:NULL+0:-1:1
	;--cdb--W:r0x116C:NULL+0:-1:1
	;--cdb--W:r0x116A:NULL+0:-1:1
	;--cdb--W:r0x116E:NULL+0:-1:1
	;--cdb--W:r0x116F:NULL+0:-1:1
	;--cdb--W:r0x1164:NULL+0:-1:1
	;--cdb--W:r0x1165:NULL+0:-1:1
	;--cdb--W:r0x115F:NULL+0:4448:0
	;--cdb--W:r0x1161:NULL+0:4457:0
	;--cdb--W:r0x1160:NULL+0:4456:0
	;--cdb--W:r0x1165:NULL+0:4457:0
	;--cdb--W:r0x1165:NULL+0:12:0
	;--cdb--W:r0x1164:NULL+0:4456:0
	;--cdb--W:r0x1169:NULL+0:4449:0
	;--cdb--W:r0x1169:NULL+0:14:0
	;--cdb--W:r0x1169:NULL+0:0:0
	;--cdb--W:r0x1168:NULL+0:4448:0
	;--cdb--W:r0x1168:NULL+0:0:0
	;--cdb--W:r0x116A:NULL+0:4448:0
	;--cdb--W:r0x116A:NULL+0:4460:0
	;--cdb--W:r0x116A:NULL+0:13:0
	;--cdb--W:r0x116B:NULL+0:4449:0
	;--cdb--W:r0x116B:NULL+0:4461:0
	;--cdb--W:r0x116B:NULL+0:14:0
	;--cdb--W:r0x116C:NULL+0:4456:0
	;--cdb--W:r0x116C:NULL+0:4458:0
	;--cdb--W:r0x116D:NULL+0:4457:0
	;--cdb--W:r0x116D:NULL+0:4459:0
	;--cdb--W:r0x116E:NULL+0:4449:0
	;--cdb--W:r0x116F:NULL+0:4454:0
	;--cdb--W:r0x1171:NULL+0:14:0
	;--cdb--W:r0x1170:NULL+0:4455:0
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x1169:NULL+0:-1:1
	;--cdb--W:r0x1168:NULL+0:-1:1
	;--cdb--W:r0x1170:NULL+0:4456:0
	;--cdb--W:r0x116D:NULL+0:-1:1
	;--cdb--W:r0x116A:NULL+0:0:0
	;--cdb--W:r0x116B:NULL+0:0:0
	;--cdb--W:r0x116B:NULL+0:-1:1
	end
