;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_divsint.c"
	.module _divsint
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$_divsint$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$_divsint$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$_divuint$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _divsint-code 
.globl __divsint

;--------------------------------------------------------
	.FUNC __divsint:$PNUM 4:$C:__divuint\
:$L:r0x1160:$L:__divsint_STK00:$L:__divsint_STK01:$L:__divsint_STK02:$L:r0x1163\
:$L:r0x1164:$L:r0x1165:$L:r0x1166
;--------------------------------------------------------
;	.line	207; "_divsint.c"	_divsint (int x, int y)
__divsint:	;Function start
	STA	r0x1160
;	;.line	211; "_divsint.c"	r = (unsigned int)(x < 0 ? -x : x) / (unsigned int)(y < 0 ? -y : y);
	XOR	#0x80
	ROL	
	CLRA	
	ROL	
	XOR	#0x01
	STA	r0x1163
	JZ	_00110_DS_
	SETB	_C
	CLRA	
	SUBB	__divsint_STK00
	STA	r0x1164
	CLRA	
	SUBB	r0x1160
	STA	r0x1165
	JMP	_00111_DS_
_00110_DS_:
	LDA	__divsint_STK00
	STA	r0x1164
	LDA	r0x1160
	STA	r0x1165
_00111_DS_:
	LDA	r0x1164
	STA	__divsint_STK00
	LDA	r0x1165
	STA	r0x1160
	LDA	__divsint_STK01
	XOR	#0x80
	ROL	
	CLRA	
	ROL	
	XOR	#0x01
	STA	r0x1164
	JZ	_00112_DS_
	SETB	_C
	CLRA	
	SUBB	__divsint_STK02
	STA	r0x1165
	CLRA	
	SUBB	__divsint_STK01
	STA	r0x1166
	JMP	_00113_DS_
_00112_DS_:
	LDA	__divsint_STK02
	STA	r0x1165
	LDA	__divsint_STK01
	STA	r0x1166
_00113_DS_:
	LDA	r0x1165
	STA	__divuint_STK02
	LDA	r0x1166
	STA	__divuint_STK01
	LDA	__divsint_STK00
	STA	__divuint_STK00
	LDA	r0x1160
	CALL	__divuint
	STA	r0x1160
;	;.line	212; "_divsint.c"	if ((x < 0) ^ (y < 0))
	LDA	r0x1164
	XOR	r0x1163
	JZ	_00106_DS_
;	;.line	213; "_divsint.c"	return -r;
	SETB	_C
	CLRA	
	SUBB	STK00
	STA	r0x1163
	CLRA	
	SUBB	r0x1160
	STA	__divsint_STK02
	LDA	r0x1163
	STA	STK00
	LDA	__divsint_STK02
	JMP	_00108_DS_
_00106_DS_:
;	;.line	215; "_divsint.c"	return r;
	LDA	r0x1160
_00108_DS_:
;	;.line	216; "_divsint.c"	}
	RET	
; exit point of __divsint
	.ENDFUNC __divsint
	;--cdb--S:G$_divsint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_divuint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_divsint._divsint$y$65536$1({2}SI:S),R,0,0,[__divsint_STK02,__divsint_STK01]
	;--cdb--S:L_divsint._divsint$x$65536$1({2}SI:S),R,0,0,[__divsint_STK00,r0x1160]
	;--cdb--S:L_divsint._divsint$r$65536$2({2}SI:S),R,0,0,[__divsint_STK00,r0x1160]
	;--cdb--S:G$_divsint$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__divuint

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__divsint
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__divsint_0	udata
r0x1160:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__divsint_STK00:	.ds	1
	.globl __divsint_STK00
__divsint_STK01:	.ds	1
	.globl __divsint_STK01
__divsint_STK02:	.ds	1
	.globl __divsint_STK02
	.globl __divuint_STK02
	.globl __divuint_STK01
	.globl __divuint_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:__divsint_STK00:NULL+0:14:0
	;--cdb--W:__divsint_STK01:NULL+0:4454:0
	;--cdb--W:__divsint_STK02:NULL+0:4453:0
	end
