;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"modff.c"
	.module modff
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fmodff$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$modff$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; modff-code 
.globl _modff

;--------------------------------------------------------
	.FUNC _modff:$PNUM 6:$C:___fs2slong:$C:___slong2fs:$C:___fssub\
:$L:r0x1162:$L:_modff_STK00:$L:_modff_STK01:$L:_modff_STK02:$L:_modff_STK03\
:$L:_modff_STK04:$L:r0x1168
;--------------------------------------------------------
;	.line	33; "modff.c"	float modff(float x, __xdata float * y)
_modff:	;Function start
	STA	r0x1162
;	;.line	35; "modff.c"	*y=(long)x;
	LDA	_modff_STK02
	STA	___fs2slong_STK02
	LDA	_modff_STK01
	STA	___fs2slong_STK01
	LDA	_modff_STK00
	STA	___fs2slong_STK00
	LDA	r0x1162
	CALL	___fs2slong
	STA	r0x1168
	LDA	STK02
	STA	___slong2fs_STK02
	LDA	STK01
	STA	___slong2fs_STK01
	LDA	STK00
	STA	___slong2fs_STK00
	LDA	r0x1168
	CALL	___slong2fs
	STA	r0x1168
	LDA	_modff_STK04
	STA	_ROMPL
	LDA	_modff_STK03
	STA	_ROMPH
	LDA	STK02
	STA	@_ROMPINC
	LDA	STK01
	STA	@_ROMPINC
	LDA	STK00
	STA	@_ROMPINC
	LDA	r0x1168
	STA	@_ROMP
;	;.line	36; "modff.c"	return (x-*y);
	LDA	STK02
	STA	___fssub_STK06
	LDA	STK01
	STA	___fssub_STK05
	LDA	STK00
	STA	___fssub_STK04
	LDA	r0x1168
	STA	___fssub_STK03
	LDA	_modff_STK02
	STA	___fssub_STK02
	LDA	_modff_STK01
	STA	___fssub_STK01
	LDA	_modff_STK00
	STA	___fssub_STK00
	LDA	r0x1162
	CALL	___fssub
;	;.line	37; "modff.c"	}
	RET	
; exit point of _modff
	.ENDFUNC _modff
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:Lmodff.modff$y$65536$25({2}DX,SF:S),R,0,0,[_modff_STK04,_modff_STK03]
	;--cdb--S:Lmodff.modff$x$65536$25({4}SF:S),R,0,0,[_modff_STK02,_modff_STK01,_modff_STK00,r0x1162]
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	___fs2slong
	.globl	___slong2fs
	.globl	___fssub

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_modff
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_modff_0	udata
r0x1162:	.ds	1
r0x1168:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_modff_STK00:	.ds	1
	.globl _modff_STK00
_modff_STK01:	.ds	1
	.globl _modff_STK01
_modff_STK02:	.ds	1
	.globl _modff_STK02
_modff_STK03:	.ds	1
	.globl _modff_STK03
_modff_STK04:	.ds	1
	.globl _modff_STK04
	.globl ___fs2slong_STK02
	.globl ___fs2slong_STK01
	.globl ___fs2slong_STK00
	.globl ___slong2fs_STK02
	.globl ___slong2fs_STK01
	.globl ___slong2fs_STK00
	.globl ___fssub_STK06
	.globl ___fssub_STK05
	.globl ___fssub_STK04
	.globl ___fssub_STK03
	.globl ___fssub_STK02
	.globl ___fssub_STK01
	.globl ___fssub_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1165:NULL+0:-1:1
	;--cdb--W:_modff_STK02:NULL+0:-1:1
	;--cdb--W:_modff_STK00:NULL+0:14:0
	;--cdb--W:_modff_STK01:NULL+0:13:0
	;--cdb--W:r0x1167:NULL+0:14:0
	;--cdb--W:r0x1166:NULL+0:13:0
	;--cdb--W:r0x1165:NULL+0:12:0
	;--cdb--W:r0x1162:NULL+0:-1:1
	end
