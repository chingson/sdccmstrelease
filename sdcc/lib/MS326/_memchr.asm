;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_memchr.c"
	.module _memchr
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--F:G$memchr$0$0({2}DF,DG,SV:S),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _memchr-code 
.globl _memchr

;--------------------------------------------------------
	.FUNC _memchr:$PNUM 6:$L:r0x1160:$L:_memchr_STK00:$L:_memchr_STK01:$L:_memchr_STK02:$L:_memchr_STK03\
:$L:_memchr_STK04:$L:r0x1166:$L:r0x1165
;--------------------------------------------------------
;	.line	33; "_memchr.c"	unsigned char *p = (unsigned char *)s;
_memchr:	;Function start
	STA	r0x1166
	LDA	_memchr_STK00
	STA	r0x1165
;	;.line	34; "_memchr.c"	unsigned char *end = p + n;
	ADD	_memchr_STK04
	STA	_memchr_STK00
	LDA	r0x1166
	ADDC	_memchr_STK03
	STA	r0x1160
;	;.line	36; "_memchr.c"	if(*p == c)
	LDA	r0x1165
	STA	_memchr_STK04
	LDA	r0x1166
	STA	_memchr_STK03
_00109_DS_:
;	;.line	35; "_memchr.c"	for(; p != end; p++)
	LDA	_memchr_STK00
	XOR	_memchr_STK04
	JNZ	_00125_DS_
	LDA	r0x1160
	XOR	_memchr_STK03
_00125_DS_:
	JZ	_00107_DS_
;	;.line	36; "_memchr.c"	if(*p == c)
	LDA	_memchr_STK04
	STA	_ROMPL
	LDA	_memchr_STK03
	STA	_ROMPH
	LDA	@_ROMPINC
	XOR	_memchr_STK02
	JNZ	_00128_DS_
	LDA	_memchr_STK01
_00128_DS_:
	JNZ	_00110_DS_
;	;.line	37; "_memchr.c"	return((void *)p);
	LDA	r0x1165
	STA	STK00
	LDA	r0x1166
	JMP	_00111_DS_
_00110_DS_:
;	;.line	35; "_memchr.c"	for(; p != end; p++)
	LDA	_memchr_STK04
	INCA	
	STA	_memchr_STK04
	CLRA	
	ADDC	_memchr_STK03
	STA	_memchr_STK03
	LDA	_memchr_STK04
	STA	r0x1165
	LDA	_memchr_STK03
	STA	r0x1166
	JMP	_00109_DS_
_00107_DS_:
;	;.line	38; "_memchr.c"	return(0);
	CLRA	
	STA	STK00
_00111_DS_:
;	;.line	39; "_memchr.c"	}
	RET	
; exit point of _memchr
	.ENDFUNC _memchr
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_memchr.memchr$n$65536$21({2}SI:U),R,0,0,[_memchr_STK04,_memchr_STK03]
	;--cdb--S:L_memchr.memchr$c$65536$21({2}SI:S),R,0,0,[_memchr_STK02,_memchr_STK01]
	;--cdb--S:L_memchr.memchr$s$65536$21({2}DG,SV:S),R,0,0,[_memchr_STK00,r0x1160]
	;--cdb--S:L_memchr.memchr$p$65536$22({2}DG,SC:U),R,0,0,[r0x1165,r0x1166]
	;--cdb--S:L_memchr.memchr$end$65536$22({2}DG,SC:U),R,0,0,[_memchr_STK00,r0x1160]
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_memchr
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__memchr_0	udata
r0x1160:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_memchr_STK00:	.ds	1
	.globl _memchr_STK00
_memchr_STK01:	.ds	1
	.globl _memchr_STK01
_memchr_STK02:	.ds	1
	.globl _memchr_STK02
_memchr_STK03:	.ds	1
	.globl _memchr_STK03
_memchr_STK04:	.ds	1
	.globl _memchr_STK04
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1167:NULL+0:-1:1
	;--cdb--W:r0x1167:NULL+0:4453:0
	;--cdb--W:r0x1168:NULL+0:4454:0
	;--cdb--W:r0x1169:NULL+0:0:0
	;--cdb--W:r0x1160:NULL+0:-1:1
	;--cdb--W:r0x1169:NULL+0:-1:1
	;--cdb--W:r0x1168:NULL+0:-1:1
	end
