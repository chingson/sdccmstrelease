;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"fabsf.c"
	.module fabsf
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$fabsf$0$0({2}DF,SL:U),C,0,0
	;--cdb--F:G$fabsf$0$0({2}DF,SL:U),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; fabsf-code 
.globl _fabsf

;--------------------------------------------------------
	.FUNC _fabsf:$PNUM 4:$L:r0x1162:$L:_fabsf_STK00:$L:_fabsf_STK01:$L:_fabsf_STK02
;--------------------------------------------------------
;	.line	34; "fabsf.c"	return x&0x7fffffff;
_fabsf:	;Function start
	AND	#0x7f
	STA	r0x1162
	LDA	_fabsf_STK02
	STA	STK02
	LDA	_fabsf_STK01
	STA	STK01
	LDA	_fabsf_STK00
	STA	STK00
	LDA	r0x1162
;	;.line	35; "fabsf.c"	}
	RET	
; exit point of _fabsf
	.ENDFUNC _fabsf
	;--cdb--S:G$fabsf$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:Lfabsf.fabsf$x$65536$1({4}SL:U),R,0,0,[_fabsf_STK02,_fabsf_STK01,_fabsf_STK00,r0x1162]
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_fabsf
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_fabsf_0	udata
r0x1162:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_fabsf_STK00:	.ds	1
	.globl _fabsf_STK00
_fabsf_STK01:	.ds	1
	.globl _fabsf_STK01
_fabsf_STK02:	.ds	1
	.globl _fabsf_STK02
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1162:NULL+0:-1:1
	end
