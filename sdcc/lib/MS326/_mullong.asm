;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_mullong.c"
	.module _mullong
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--F:G$_mullong$0$0({2}DF,SL:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _mullong-code 
.globl __mullong

;--------------------------------------------------------
	.FUNC __mullong:$PNUM 8:$L:r0x1163:$L:__mullong_STK00:$L:__mullong_STK01:$L:__mullong_STK02:$L:__mullong_STK03\
:$L:__mullong_STK04:$L:__mullong_STK05:$L:__mullong_STK06:$L:r0x1168:$L:r0x1169\
:$L:r0x116B:$L:r0x116F:$L:r0x1170:$L:r0x1175:$L:r0x1176\
:$L:r0x1177:$L:r0x1178:$L:r0x1179:$L:r0x117A:$L:r0x117E\
:$L:r0x117D:$L:r0x117C
;--------------------------------------------------------
;	.line	38; "_mullong.c"	_mullong (long a, long b) __critical
__mullong:	;Function start
	CRIT	
	STA	r0x1163
;	;.line	44; "_mullong.c"	a0=U(a);
	LDA	__mullong_STK02
	STA	r0x1168
;	;.line	49; "_mullong.c"	b0=U(b);
	LDA	__mullong_STK06
	STA	__mullong_STK02
;	;.line	50; "_mullong.c"	b1=U(b)>>8;
	LDA	__mullong_STK05
	STA	r0x1170
;	;.line	51; "_mullong.c"	b2=U(b)>>16;
	LDA	__mullong_STK04
	STA	r0x1176
;	;.line	52; "_mullong.c"	b3=U(b)>>24;
	LDA	__mullong_STK03
	STA	r0x1175
;	;.line	54; "_mullong.c"	MULSHIFT=1;
	LDA	#0x01
	STA	_MULSHIFT
;	;.line	55; "_mullong.c"	MULAL=0;
	CLRA	
	STA	_MULAL
;	;.line	56; "_mullong.c"	MULBH=0;
	STA	_MULBH
;	;.line	59; "_mullong.c"	MULAH=a0;
	LDA	r0x1168
	STA	_MULAH
;	;.line	60; "_mullong.c"	MULBL=b0;
	LDA	__mullong_STK02
	STA	_MULBL
;	;.line	61; "_mullong.c"	result=MULO1;
	LDA	_MULO1
	STA	__mullong_STK06
	LDA	(_MULO1 + 1)
	STA	__mullong_STK05
;	;.line	65; "_mullong.c"	MULAH=a1;
	LDA	__mullong_STK01
	STA	_MULAH
;	;.line	66; "_mullong.c"	result+=(unsigned long)MULO1<<8;
	LDA	_MULO1
	STA	r0x1177
	LDA	(_MULO1 + 1)
	STA	r0x117D
	LDA	r0x1177
	STA	r0x117C
	LDA	__mullong_STK06
	STA	r0x1177
	LDA	__mullong_STK05
	ADD	r0x117C
	STA	r0x1178
	CLRA	
	ADDC	r0x117D
	STA	r0x1179
	CLRA	
	ROL	
	STA	r0x117A
;	;.line	67; "_mullong.c"	MULAH=a0;
	LDA	r0x1168
	STA	_MULAH
;	;.line	68; "_mullong.c"	MULBL=b1;
	LDA	r0x1170
	STA	_MULBL
;	;.line	69; "_mullong.c"	result+=(unsigned long)MULO1<<8;
	LDA	_MULO1
	STA	__mullong_STK06
	LDA	(_MULO1 + 1)
	STA	r0x117D
	LDA	__mullong_STK06
	STA	r0x117C
	LDA	r0x1177
	STA	__mullong_STK06
	LDA	r0x1178
	ADD	r0x117C
	STA	__mullong_STK05
	LDA	r0x1179
	ADDC	r0x117D
	STA	__mullong_STK04
	CLRA	
	ADDC	r0x117A
	STA	__mullong_STK03
;	;.line	73; "_mullong.c"	MULAH=a1;
	LDA	__mullong_STK01
	STA	_MULAH
;	;.line	74; "_mullong.c"	result+=(unsigned long)MULO1<<16;
	LDA	_MULO1
	STA	r0x1177
	LDA	(_MULO1 + 1)
	STA	r0x117E
	LDA	r0x1177
	STA	r0x117D
	CLRA	
	ADD	__mullong_STK06
	STA	r0x1177
	CLRA	
	ADDC	__mullong_STK05
	STA	r0x1178
	LDA	__mullong_STK04
	ADDC	r0x117D
	STA	r0x1179
	LDA	__mullong_STK03
	ADDC	r0x117E
	STA	r0x117A
;	;.line	75; "_mullong.c"	MULAH=a0;
	LDA	r0x1168
	STA	_MULAH
;	;.line	76; "_mullong.c"	MULBL=b2;
	LDA	r0x1176
	STA	_MULBL
;	;.line	77; "_mullong.c"	result+=(unsigned long)MULO1<<16;
	LDA	_MULO1
	STA	__mullong_STK06
	LDA	(_MULO1 + 1)
	STA	r0x117E
	LDA	__mullong_STK06
	STA	r0x117D
	CLRA	
	ADD	r0x1177
	STA	__mullong_STK06
	CLRA	
	ADDC	r0x1178
	STA	__mullong_STK05
	LDA	r0x1179
	ADDC	r0x117D
	STA	__mullong_STK04
	LDA	r0x117A
	ADDC	r0x117E
	STA	__mullong_STK03
;	;.line	78; "_mullong.c"	MULAH=a2;
	LDA	__mullong_STK00
	STA	_MULAH
;	;.line	79; "_mullong.c"	MULBL=b0;
	LDA	__mullong_STK02
	STA	_MULBL
;	;.line	80; "_mullong.c"	result+=(unsigned long)MULO1<<16;
	LDA	_MULO1
	STA	__mullong_STK02
	LDA	(_MULO1 + 1)
	STA	r0x117D
	LDA	__mullong_STK02
	STA	r0x117C
	CLRA	
	ADD	__mullong_STK06
	STA	__mullong_STK02
	CLRA	
	ADDC	__mullong_STK05
	STA	r0x1177
	LDA	__mullong_STK04
	ADDC	r0x117C
	STA	r0x1178
	LDA	__mullong_STK03
	ADDC	r0x117D
	STA	r0x1179
;	;.line	84; "_mullong.c"	MULAH=a3;
	LDA	r0x1163
	STA	_MULAH
;	;.line	85; "_mullong.c"	result+=(unsigned long)MULO1<<24;
	LDA	_MULO1
	ADD	r0x1179
	STA	r0x1179
;	;.line	86; "_mullong.c"	MULAH=a2;
	LDA	__mullong_STK00
	STA	_MULAH
;	;.line	87; "_mullong.c"	MULBL=b1;
	LDA	r0x1170
	STA	_MULBL
;	;.line	88; "_mullong.c"	result+=(unsigned long)MULO1<<24;
	LDA	_MULO1
	STA	r0x117A
	CLRA	
	ADD	__mullong_STK02
	STA	__mullong_STK06
	CLRA	
	ADDC	r0x1177
	STA	__mullong_STK05
	CLRA	
	ADDC	r0x1178
	STA	__mullong_STK04
	LDA	r0x1179
	ADDC	r0x117A
	STA	__mullong_STK03
;	;.line	89; "_mullong.c"	MULAH=a1;
	LDA	__mullong_STK01
	STA	_MULAH
;	;.line	90; "_mullong.c"	MULBL=b2;
	LDA	r0x1176
	STA	_MULBL
;	;.line	91; "_mullong.c"	result+=(unsigned long)MULO1<<24;
	LDA	_MULO1
	STA	r0x1178
	CLRA	
	ADD	__mullong_STK06
	STA	__mullong_STK02
	CLRA	
	ADDC	__mullong_STK05
	STA	r0x1169
	CLRA	
	ADDC	__mullong_STK04
	STA	r0x116B
	LDA	__mullong_STK03
	ADDC	r0x1178
	STA	r0x116F
;	;.line	92; "_mullong.c"	MULAH=a0;
	LDA	r0x1168
	STA	_MULAH
;	;.line	93; "_mullong.c"	MULBL=b3;
	LDA	r0x1175
	STA	_MULBL
;	;.line	94; "_mullong.c"	result+=(unsigned long)MULO1<<24;
	LDA	_MULO1
	STA	r0x1176
	CLRA	
	ADD	__mullong_STK02
	STA	__mullong_STK06
	CLRA	
	ADDC	r0x1169
	STA	__mullong_STK05
	CLRA	
	ADDC	r0x116B
	STA	__mullong_STK04
	LDA	r0x116F
	ADDC	r0x1176
	STA	__mullong_STK03
;	;.line	97; "_mullong.c"	return (long)result;	
	LDA	__mullong_STK06
	STA	STK02
	LDA	__mullong_STK05
	STA	STK01
	LDA	__mullong_STK04
	STA	STK00
	LDA	__mullong_STK03
;	;.line	98; "_mullong.c"	}
	NOCRIT	
	RET	
; exit point of __mullong
	.ENDFUNC __mullong
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$PAR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PADIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOB$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMA_IL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIDAT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCLKDIV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CMPCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTVC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMICON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PRG_RAM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIDMAC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECOAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECLEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECMODE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIOPRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMALEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULB$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULBL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULBH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULO$0$0({4}SL:U),E,0,0
	;--cdb--S:G$MULO1$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULSHIFT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUF$0$0({2}SI:S),E,0,0
	;--cdb--S:G$L2UBUFL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUFH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2USH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$U2LSH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTPRI$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRA$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$LPWMRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMINV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPICK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPSEL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$HWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWDINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PATEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TRAMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMBUFP$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$PASKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBSKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABSKIP$0$0({2}SI:U),E,0,0
	;--cdb--S:G$PATR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTR$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TOUCHC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DUMMYC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IRCD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMDUTY$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMPER$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACGCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECPWR$0$0({4}SL:U),E,0,0
	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$HWPALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0UW$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$EA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OV$0$0({1}SC:U),E,0,0
	;--cdb--S:L_mullong._mullong$b$65536$1({4}SL:S),R,0,0,[__mullong_STK06,__mullong_STK05,__mullong_STK04,__mullong_STK03]
	;--cdb--S:L_mullong._mullong$a$65536$1({4}SL:S),R,0,0,[__mullong_STK02,__mullong_STK01,__mullong_STK00,r0x1163]
	;--cdb--S:L_mullong._mullong$a0$65536$2({1}SC:U),R,0,0,[r0x1168]
	;--cdb--S:L_mullong._mullong$a1$65536$2({1}SC:U),R,0,0,[r0x1169,r0x116A]
	;--cdb--S:L_mullong._mullong$a2$65536$2({1}SC:U),R,0,0,[r0x116F]
	;--cdb--S:L_mullong._mullong$a3$65536$2({1}SC:U),R,0,0,[r0x116B,r0x116C,r0x116D,r0x116E]
	;--cdb--S:L_mullong._mullong$b0$65536$2({1}SC:U),R,0,0,[__mullong_STK02,r0x1161,r0x1162,r0x1163]
	;--cdb--S:L_mullong._mullong$b1$65536$2({1}SC:U),R,0,0,[r0x1170,r0x1171]
	;--cdb--S:L_mullong._mullong$b2$65536$2({1}SC:U),R,0,0,[r0x1176]
	;--cdb--S:L_mullong._mullong$b3$65536$2({1}SC:U),R,0,0,[r0x1172,r0x1173,r0x1174,r0x1175]
	;--cdb--S:L_mullong._mullong$result$65536$2({4}SL:U),R,0,0,[__mullong_STK02,r0x1177,r0x1178,r0x1179]
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_PAR
	.globl	_PADIR
	.globl	_PIOA
	.globl	_PAWK
	.globl	_PAWKDR
	.globl	_TIMERC
	.globl	_THRLD
	.globl	_RAMP0L
	.globl	_RAMP0H
	.globl	_RAMP1L
	.globl	_RAMP1H
	.globl	_PTRCL
	.globl	_PTRCH
	.globl	_ROMPL
	.globl	_ROMPH
	.globl	_BEEPC
	.globl	_FILTERGR
	.globl	_ULAWC
	.globl	_STACKL
	.globl	_STACKH
	.globl	_ADCON
	.globl	_DACON
	.globl	_SYSC
	.globl	_SPIM
	.globl	_SPIH
	.globl	_SPIMH
	.globl	_SPIOP
	.globl	_SPI_BANK
	.globl	_ADP_IND
	.globl	_ADP_VPL
	.globl	_ADP_VPH
	.globl	_ADL
	.globl	_ADH
	.globl	_ZC
	.globl	_ADCG
	.globl	_DAC_PL
	.globl	_DAC_PH
	.globl	_PAG
	.globl	_RDMAL
	.globl	_RDMAH
	.globl	_SPIL
	.globl	_IOMASK
	.globl	_IOCMP
	.globl	_IOCNT
	.globl	_LVDCON
	.globl	_LVDCTH
	.globl	_LVRCON
	.globl	_OFFSETL
	.globl	_OFFSETH
	.globl	_RCCON
	.globl	_BGCON
	.globl	_PWRL
	.globl	_CRYPT
	.globl	_PWRH
	.globl	_PWRHL
	.globl	_IROMDL
	.globl	_IROMDH
	.globl	_RECMUTE
	.globl	_SPKC
	.globl	_DCLAMP
	.globl	_SSPIC
	.globl	_SSPIL
	.globl	_SSPIM
	.globl	_SSPIH
	.globl	_PBR
	.globl	_PBDIR
	.globl	_PIOB
	.globl	_PBWK
	.globl	_PBWKDR
	.globl	_PAIE
	.globl	_PBIE
	.globl	_PAIF
	.globl	_PBIF
	.globl	_GIE
	.globl	_GIF
	.globl	_WDTL
	.globl	_WDTH
	.globl	_RPAGES
	.globl	_PPAGES
	.globl	_DMA_IL
	.globl	_FILTERGP
	.globl	_SPIDAT
	.globl	_RSPIC
	.globl	_RCLKDIV
	.globl	_PCR
	.globl	_PCDIR
	.globl	_PIOC
	.globl	_CMPCON
	.globl	_INTVC
	.globl	_INTV
	.globl	_DMICON
	.globl	_PRG_RAM
	.globl	_PDMAL
	.globl	_PDMAH
	.globl	_PDMALH
	.globl	_SPIDMAC
	.globl	_SDMAAL
	.globl	_SDMAAH
	.globl	_SDMAALH
	.globl	_ECRAL
	.globl	_ECRAH
	.globl	_ECRALH
	.globl	_ECOAL
	.globl	_ECOAH
	.globl	_ECOALH
	.globl	_ECLEN
	.globl	_ECCON
	.globl	_ECMODE
	.globl	_SPIOPRAH
	.globl	_SDMALEN
	.globl	_MULA
	.globl	_MULAL
	.globl	_MULAH
	.globl	_MULB
	.globl	_MULBL
	.globl	_MULBH
	.globl	_MULO
	.globl	_MULO1
	.globl	_MULSHIFT
	.globl	_L2UBUF
	.globl	_L2UBUFL
	.globl	_L2UBUFH
	.globl	_ULAWD
	.globl	_L2USH
	.globl	_U2LSH
	.globl	_INTPRI
	.globl	_LPWMRA
	.globl	_LPWMRAL
	.globl	_LPWMRAH
	.globl	_LPWMEN
	.globl	_LPWMINV
	.globl	_SPICK
	.globl	_SYSC2
	.globl	_HWPSEL
	.globl	_HWPAL
	.globl	_HWPAH
	.globl	_HWPA
	.globl	_HWD
	.globl	_HWDINC
	.globl	_PATEN
	.globl	_PBTEN
	.globl	_PABTEN
	.globl	_TRAMAL
	.globl	_TRAMAH
	.globl	_TRAMBUFP
	.globl	_PASKIP
	.globl	_PBSKIP
	.globl	_PABSKIP
	.globl	_PATR
	.globl	_PBTR
	.globl	_PABTR
	.globl	_TOUCHC
	.globl	_DUMMYC
	.globl	_IRCD
	.globl	_FPWMEN
	.globl	_FPWMDUTY
	.globl	_FPWMPER
	.globl	_DACGCL
	.globl	_RECPWR
	.globl	_ICE0
	.globl	_ICE1
	.globl	_ICE2
	.globl	_ICE3
	.globl	_ICE4
	.globl	_RAMP0
	.globl	_RAMP0INC
	.globl	_RAMP1
	.globl	_RDMALH
	.globl	_HWPALH
	.globl	_RAMP1INC
	.globl	_RAMP1INC2
	.globl	_ROMP
	.globl	_ROMPINC
	.globl	_ROMPINC2
	.globl	_ACC
	.globl	_RAMP0UW
	.globl	_RAMP1UW
	.globl	_ROMPLH
	.globl	_ROMPUW
	.globl	_OFFSETLH
	.globl	_ADP_VPLH
	.globl	_TOV
	.globl	_EA
	.globl	_OV

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__mullong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__mullong_0	udata
r0x1163:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116B:	.ds	1
r0x116F:	.ds	1
r0x1170:	.ds	1
r0x1175:	.ds	1
r0x1176:	.ds	1
r0x1177:	.ds	1
r0x1178:	.ds	1
r0x1179:	.ds	1
r0x117A:	.ds	1
r0x117C:	.ds	1
r0x117D:	.ds	1
r0x117E:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__mullong_STK00:	.ds	1
	.globl __mullong_STK00
__mullong_STK01:	.ds	1
	.globl __mullong_STK01
__mullong_STK02:	.ds	1
	.globl __mullong_STK02
__mullong_STK03:	.ds	1
	.globl __mullong_STK03
__mullong_STK04:	.ds	1
	.globl __mullong_STK04
__mullong_STK05:	.ds	1
	.globl __mullong_STK05
__mullong_STK06:	.ds	1
	.globl __mullong_STK06
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1169:NULL+0:-1:1
	;--cdb--W:r0x116A:NULL+0:-1:1
	;--cdb--W:r0x116C:NULL+0:-1:1
	;--cdb--W:r0x116B:NULL+0:-1:1
	;--cdb--W:__mullong_STK01:NULL+0:-1:1
	;--cdb--W:__mullong_STK00:NULL+0:-1:1
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x1170:NULL+0:-1:1
	;--cdb--W:r0x1173:NULL+0:-1:1
	;--cdb--W:r0x1172:NULL+0:-1:1
	;--cdb--W:__mullong_STK05:NULL+0:-1:1
	;--cdb--W:__mullong_STK04:NULL+0:-1:1
	;--cdb--W:__mullong_STK03:NULL+0:-1:1
	;--cdb--W:r0x117A:NULL+0:-1:1
	;--cdb--W:r0x117B:NULL+0:-1:1
	;--cdb--W:r0x1179:NULL+0:-1:1
	;--cdb--W:r0x1178:NULL+0:-1:1
	;--cdb--W:r0x116F:NULL+0:-1:1
	;--cdb--W:r0x1168:NULL+0:-1:1
	;--cdb--W:__mullong_STK02:NULL+0:4461:0
	;--cdb--W:__mullong_STK02:NULL+0:4462:0
	;--cdb--W:__mullong_STK02:NULL+0:4485:0
	;--cdb--W:__mullong_STK04:NULL+0:0:0
	;--cdb--W:__mullong_STK06:NULL+0:4468:0
	;--cdb--W:__mullong_STK06:NULL+0:4469:0
	;--cdb--W:r0x1168:NULL+0:4484:0
	;--cdb--W:r0x1169:NULL+0:4483:0
	;--cdb--W:r0x116B:NULL+0:4480:0
	;--cdb--W:r0x116B:NULL+0:4462:0
	;--cdb--W:r0x116B:NULL+0:4482:0
	;--cdb--W:r0x116D:NULL+0:4479:0
	;--cdb--W:r0x116E:NULL+0:4451:0
	;--cdb--W:r0x116F:NULL+0:4479:0
	;--cdb--W:r0x116F:NULL+0:0:0
	;--cdb--W:r0x1170:NULL+0:0:0
	;--cdb--W:r0x1171:NULL+0:4484:0
	;--cdb--W:r0x1172:NULL+0:4484:0
	;--cdb--W:r0x1172:NULL+0:4469:0
	;--cdb--W:r0x1174:NULL+0:4483:0
	;--cdb--W:r0x1175:NULL+0:4482:0
	;--cdb--W:r0x1176:NULL+0:0:0
	;--cdb--W:r0x1179:NULL+0:0:0
	;--cdb--W:r0x117A:NULL+0:0:0
	;--cdb--W:r0x117C:NULL+0:0:0
	;--cdb--W:r0x117C:NULL+0:4485:0
	;--cdb--W:r0x117B:NULL+0:0:0
	;--cdb--W:r0x117C:NULL+0:-1:1
	;--cdb--W:r0x1176:NULL+0:-1:1
	;--cdb--W:r0x1169:NULL+0:4480:0
	;--cdb--W:r0x1172:NULL+0:0:0
	;--cdb--W:r0x1177:NULL+0:0:0
	;--cdb--W:r0x1177:NULL+0:-1:1
	;--cdb--W:__mullong_STK03:NULL+0:0:0
	;--cdb--W:r0x117E:NULL+0:0:0
	;--cdb--W:r0x117E:NULL+0:-1:1
	;--cdb--W:__mullong_STK06:NULL+0:-1:1
	;--cdb--W:__mullong_STK02:NULL+0:-1:1
	end
