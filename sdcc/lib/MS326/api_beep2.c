#include<ms326.h>
#include<ms326sphlib.h>



extern USHORT api_play_endpage;
extern USHORT api_play_startpage;
extern BYTE api_play_mode;
extern BYTE api_play_rampage;
extern BYTE api_play_fifostart;
extern BYTE api_play_fifoend;
extern BYTE api_play_spibuf;
extern BYTE api_play_spiind;
extern api_spi_addr_t api_play_spi_addr;
// 2021, use ULAW to generate beep, it should be fast
// 256 bytes
void api_beep_start2(BYTE bv,USHORT perioddac, BYTE periodwav, BYTE pagv, USHORT buframa)
{
    unsigned char i;
    HWPSEL=0;
    PAG = pagv;
    FILTERGP=0x3f; // don't too loud?
    HWPA=buframa;
    do {
        for(i=0;i<periodwav;i++)
            HWDINC=bv;
        for(i=0;i<periodwav;i++)
            HWDINC=bv^0x80;
    }while(HWPAH==(buframa>>8));
    DMA_IL=0;
    DAC_PH=perioddac>>8;
    DAC_PL=(perioddac&0xff)|0x1f;
    api_play_fifostart=(((USHORT)buframa)&0x7ff)>>4; // unit is 16byte , usually this is 0x50
    api_play_fifoend=api_play_fifostart+15; // we use XF as the page addr 0x6f
    ADP_IND=0x80;// following is 8 bit wide
    PPAGES=(api_play_fifostart&0xf)|((api_play_fifoend&0xf)<<4); // f0
    ADP_IND=0;// following is 6 bits only
    PPAGES=(api_play_fifostart>>4)|((api_play_fifoend&0xf0)>>1);// 35
    PDMAH=0x80;//reset dma address
    RCLKDIV&=0x7f;
    U2LSH=0;
    ULAWC|=0xA0;
    DACON|=0x1E; // no filter
    while(PDMAL==0);
    GIF=0xbf;
    DACON|=1;

}
