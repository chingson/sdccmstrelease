;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.1 #3de0c6772 (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"C:\work\ms326sphlib"
;;	.file	"tklib.c"
	.module tklib
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Ftklib$spiastru[({0}S:S$addrhm$0$0({2}SI:U),Z,0,0)({2}S:S$addrl$0$0({1}SC:U),Z,0,0)]
	;--cdb--T:Ftklib$adps[({0}S:S$predict$0$0({2}SI:S),Z,0,0)({2}S:S$index$0$0({1}SC:U),Z,0,0)]
	;--cdb--T:Ftklib$touchen[({0}S:S$toff$0$0({1}SC:U),Z,0,0)({1}S:S$nmossw$0$0({1}SC:U),Z,0,0)({2}S:S$period$0$0({2}SI:U),Z,0,0)({4}S:S$threshold$0$0({2}SI:U),Z,0,0)({6}S:S$count$0$0({2}SI:U),Z,0,0)]
	;--cdb--T:Ftklib$pwmleds[({0}S:S$period$0$0({1}SC:U),Z,0,0)({1}S:S$counter$0$0({1}SC:U),Z,0,0)({2}S:S$threshold$0$0({1}SC:U),Z,0,0)]
	;--cdb--S:G$api_tk_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$api_tk_stop$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$api_tk_init$0$0({2}DF,SI:U),C,0,0
	;--cdb--F:G$api_tk_init$0$0({2}DF,SI:U),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_pre_erase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start_no_erase$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop_noerase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_noer$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_chspick$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:Ftklib$wait_tk_int$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:Ftklib$wait_tk_int$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$_mulint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; tklib-code 
.globl _api_tk_stop

;--------------------------------------------------------
	.FUNC _api_tk_stop:$PNUM 0
;--------------------------------------------------------
;	.line	140; "tklib.c"	TOUCHC = 0;
_api_tk_stop:	;Function start
	CLRA	
;	;.line	141; "tklib.c"	PABTEN = 0;
	STA	_TOUCHC
	STA	_PABTEN
	STA	(_PABTEN + 1)
;	;.line	142; "tklib.c"	}
	RET	
; exit point of _api_tk_stop
	.ENDFUNC _api_tk_stop
.globl _api_tk_init

;--------------------------------------------------------
	.FUNC _api_tk_init:$PNUM 14:$C:_memset:$C:_wait_tk_int:$C:__mullong\
:$L:r0x11BA:$L:_api_tk_init_STK00:$L:_api_tk_init_STK01:$L:_api_tk_init_STK02:$L:_api_tk_init_STK03\
:$L:_api_tk_init_STK04:$L:_api_tk_init_STK05:$L:_api_tk_init_STK06:$L:_api_tk_init_STK07:$L:_api_tk_init_STK08\
:$L:_api_tk_init_STK09:$L:_api_tk_init_STK10:$L:_api_tk_init_STK11:$L:_api_tk_init_STK12:$L:r0x11C3\
:$L:r0x11C4:$L:r0x11C5:$L:r0x11C6:$L:r0x11C8:$L:r0x11C7\

;--------------------------------------------------------
;	.line	27; "tklib.c"	USHORT api_tk_init(USHORT ch_en, BYTE mon, BYTE totalch, BYTE *bufp, USHORT *finalp, unsigned long *baselinei, unsigned char *th_ratio, BYTE (*callback)(void))
_api_tk_init:	;Function start
	STA	r0x11BA
	LDA	_api_tk_init_STK05
	STA	(_procdatap + 1)
	LDA	_api_tk_init_STK06
	STA	_procdatap
	LDA	_api_tk_init_STK07
	STA	(_baseline + 1)
	LDA	_api_tk_init_STK08
	STA	_baseline
;	;.line	31; "tklib.c"	USHORT init_result = 0;
	CLRA	
	STA	r0x11C3
	STA	r0x11C4
;	;.line	33; "tklib.c"	BYTE dum = 1;
	LDA	#0x01
	STA	r0x11C5
;	;.line	34; "tklib.c"	tch_mon = mon;
	LDA	_api_tk_init_STK01
	STA	_tch_mon
;	;.line	36; "tklib.c"	tch_total_ch = totalch + 1;
	LDA	_api_tk_init_STK02
	INCA	
	STA	_tch_total_ch
;	;.line	37; "tklib.c"	rambufp1 = (touch_entry *)bufp;
	LDA	_api_tk_init_STK03
	STA	r0x11C6
	LDA	_api_tk_init_STK04
	STA	_api_tk_init_STK02
	STA	_rambufp1
	LDA	r0x11C6
	STA	(_rambufp1 + 1)
;	;.line	40; "tklib.c"	PABSKIP = 0xffff;
	LDA	#0xff
	STA	_PABSKIP
	STA	(_PABSKIP + 1)
;	;.line	41; "tklib.c"	TRAMBUFP = bufp;                   //msb missed
	LDA	_api_tk_init_STK04
	STA	_TRAMBUFP
;	;.line	42; "tklib.c"	memset(bufp, 0, tch_total_ch * 8); // maximum 16*8=128
	LDA	_api_tk_init_STK03
	STA	(_TRAMBUFP + 1)
	STA	r0x11C8
	LDA	_api_tk_init_STK04
	STA	r0x11C7
	CLRA	
	STA	_api_tk_init_STK03
	LDA	_tch_total_ch
	SHL	
	STA	_api_tk_init_STK04
	LDA	_api_tk_init_STK03
	ROL	
	STA	_api_tk_init_STK03
	LDA	_api_tk_init_STK04
	SHL	
	STA	_api_tk_init_STK04
	LDA	_api_tk_init_STK03
	ROL	
	STA	_api_tk_init_STK03
	LDA	_api_tk_init_STK04
	SHL	
	STA	_api_tk_init_STK04
	LDA	_api_tk_init_STK03
	ROL	
	STA	_api_tk_init_STK03
	LDA	_api_tk_init_STK04
	STA	_memset_STK03
	LDA	_api_tk_init_STK03
	STA	_memset_STK02
	CLRA	
	STA	_memset_STK01
	LDA	r0x11C7
	STA	_memset_STK00
	LDA	r0x11C8
	CALL	_memset
;	;.line	43; "tklib.c"	PABTEN = ch_en;
	LDA	_api_tk_init_STK00
	STA	_PABTEN
	LDA	r0x11BA
	STA	(_PABTEN + 1)
;	;.line	44; "tklib.c"	TOUCHC = TOUCHC_DEFAULT;
	LDA	#0x49
	STA	_TOUCHC
;	;.line	45; "tklib.c"	DUMMYC = 6; // dummy count enabled
	LDA	#0x06
	STA	_DUMMYC
;	;.line	46; "tklib.c"	threshold_ratio = th_ratio;
	LDA	_api_tk_init_STK10
	STA	_threshold_ratio
	LDA	_api_tk_init_STK09
	STA	(_threshold_ratio + 1)
;	;.line	48; "tklib.c"	tkramp = (touch_entry *)bufp;
	LDA	r0x11C6
	STA	_api_tk_init_STK04
;	;.line	49; "tklib.c"	for (chk_channel = 1; chk_channel || dum; chk_channel <<= 1)
	LDA	#0x01
	STA	_api_tk_init_STK03
	CLRA	
	STA	_api_tk_init_STK10
_00185_DS_:
	LDA	_api_tk_init_STK03
	ORA	_api_tk_init_STK10
	JNZ	_00184_DS_
	LDA	r0x11C5
	JZ	_00167_DS_
_00184_DS_:
;	;.line	52; "tklib.c"	if ((chk_channel != 0) && !(chk_channel & ch_en))
	LDA	_api_tk_init_STK03
	ORA	_api_tk_init_STK10
	JZ	_00147_DS_
	LDA	_api_tk_init_STK00
	AND	_api_tk_init_STK03
	STA	_api_tk_init_STK09
	LDA	r0x11BA
	AND	_api_tk_init_STK10
	ORA	_api_tk_init_STK09
	JZ	_00165_DS_
_00147_DS_:
;	;.line	56; "tklib.c"	tkramp->toff = 7; // smallest
	LDA	_api_tk_init_STK04
	STA	_ROMPH
	LDA	_api_tk_init_STK02
	STA	_ROMPL
	LDA	#0x07
	STA	@_ROMPINC
;	;.line	57; "tklib.c"	tkramp->nmossw = 4;
	LDA	_api_tk_init_STK02
	INCA	
	STA	_api_tk_init_STK09
	CLRA	
	ADDC	_api_tk_init_STK04
	STA	_ROMPH
	LDA	_api_tk_init_STK09
	STA	_ROMPL
	LDA	#0x04
	STA	@_ROMPINC
;	;.line	58; "tklib.c"	tkramp->period = 4096;    // 2048 clock seems not long enough?
	LDA	#0x02
	ADD	_api_tk_init_STK02
	STA	_api_tk_init_STK09
	CLRA	
	ADDC	_api_tk_init_STK04
	STA	_ROMPH
	LDA	_api_tk_init_STK09
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
	LDA	#0x10
	STA	@_ROMPINC
;	;.line	59; "tklib.c"	tkramp->threshold = 2048; // we take 0x200 as the threshold
	LDA	#0x04
	ADD	_api_tk_init_STK02
	STA	_api_tk_init_STK09
	CLRA	
	ADDC	_api_tk_init_STK04
	STA	_ROMPH
	LDA	_api_tk_init_STK09
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
	LDA	#0x08
	STA	@_ROMPINC
;	;.line	60; "tklib.c"	PABSKIP = chk_channel ^ 0xffff;
	LDA	#0xff
	XOR	_api_tk_init_STK03
	STA	_PABSKIP
	LDA	#0xff
	XOR	_api_tk_init_STK10
	STA	(_PABSKIP + 1)
;	;.line	61; "tklib.c"	if (chk_channel == 0)
	LDA	_api_tk_init_STK03
	ORA	_api_tk_init_STK10
	JNZ	_00199_DS_
;	;.line	63; "tklib.c"	DUMMYC = 2; // enable dummy
	LDA	#0x02
	STA	_DUMMYC
_00199_DS_:
;	;.line	65; "tklib.c"	for (sw = 6; sw >= 4; sw--) // small current, the count is large -> small
	LDA	#0x06
	STA	_api_tk_init_STK09
_00181_DS_:
;	;.line	67; "tklib.c"	tkramp->nmossw = sw;
	LDA	_api_tk_init_STK02
	INCA	
	STA	r0x11C6
	CLRA	
	ADDC	_api_tk_init_STK04
	STA	_ROMPH
	LDA	r0x11C6
	STA	_ROMPL
	LDA	_api_tk_init_STK09
	STA	@_ROMPINC
;	;.line	71; "tklib.c"	tkramp->count = 0;
	LDA	#0x06
	ADD	_api_tk_init_STK02
	STA	r0x11C6
	CLRA	
	ADDC	_api_tk_init_STK04
	STA	_ROMPH
	LDA	r0x11C6
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
	CLRA	
	STA	@_ROMPINC
;	;.line	72; "tklib.c"	TOUCHC = TOUCHC_DEFAULT;
	LDA	#0x49
	STA	_TOUCHC
_00157_DS_:
;	;.line	78; "tklib.c"	if (wait_tk_int(1, callback))
	LDA	_api_tk_init_STK12
	STA	_wait_tk_int_STK01
	LDA	_api_tk_init_STK11
	STA	_wait_tk_int_STK00
	LDA	#0x01
	CALL	_wait_tk_int
	JNZ	_00167_DS_
;	;.line	80; "tklib.c"	TOUCHC = TOUCHC_DEFAULT;
	LDA	#0x49
	STA	_TOUCHC
;	;.line	82; "tklib.c"	if (wait_tk_int(1, callback))
	LDA	_api_tk_init_STK12
	STA	_wait_tk_int_STK01
	LDA	_api_tk_init_STK11
	STA	_wait_tk_int_STK00
	LDA	#0x01
	CALL	_wait_tk_int
	JNZ	_00167_DS_
;	;.line	85; "tklib.c"	TOUCHC = TOUCHC_DEFAULT;
	LDA	#0x49
	STA	_TOUCHC
;	;.line	86; "tklib.c"	} while ((PABTR || (DUMMYC & 1)) && (++(tkramp->toff) < 29));
	LDA	_PABTR
	ORA	(_PABTR + 1)
	JNZ	_00156_DS_
	LDA	_DUMMYC
	SHR	
	JNC	_00159_DS_
_00156_DS_:
	LDA	_api_tk_init_STK02
	STA	_ROMPL
	LDA	_api_tk_init_STK04
	STA	_ROMPH
	LDA	@_ROMPINC
	ADD	#0x01
	STA	r0x11C6
	LDA	_api_tk_init_STK04
	STA	_ROMPH
	LDA	_api_tk_init_STK02
	STA	_ROMPL
	LDA	r0x11C6
	STA	@_ROMPINC
	LDA	r0x11C6
	ADD	#0xe3
	JNC	_00157_DS_
_00159_DS_:
;	;.line	87; "tklib.c"	if (!PABTR)
	LDA	_PABTR
	ORA	(_PABTR + 1)
	JNZ	_00182_DS_
;	;.line	89; "tklib.c"	init_result |= chk_channel;
	LDA	_api_tk_init_STK03
	ORA	r0x11C3
	STA	r0x11C3
	LDA	_api_tk_init_STK10
	ORA	r0x11C4
	STA	r0x11C4
;	;.line	90; "tklib.c"	break;
	JMP	_00162_DS_
_00182_DS_:
;	;.line	65; "tklib.c"	for (sw = 6; sw >= 4; sw--) // small current, the count is large -> small
	LDA	_api_tk_init_STK09
	DECA	
	STA	_api_tk_init_STK09
	ADD	#0xfc
	JC	_00181_DS_
_00162_DS_:
;	;.line	93; "tklib.c"	tkramp++;
	LDA	#0x08
	ADD	_api_tk_init_STK02
	STA	_api_tk_init_STK02
	CLRA	
	ADDC	_api_tk_init_STK04
	STA	_api_tk_init_STK04
;	;.line	94; "tklib.c"	if (chk_channel == 0)
	LDA	_api_tk_init_STK03
	ORA	_api_tk_init_STK10
	JNZ	_00165_DS_
;	;.line	95; "tklib.c"	dum = 0;
	CLRA	
	STA	r0x11C5
_00165_DS_:
;	;.line	49; "tklib.c"	for (chk_channel = 1; chk_channel || dum; chk_channel <<= 1)
	LDA	_api_tk_init_STK03
	SHL	
	STA	_api_tk_init_STK03
	LDA	_api_tk_init_STK10
	ROL	
	STA	_api_tk_init_STK10
	JMP	_00185_DS_
_00167_DS_:
;	;.line	100; "tklib.c"	PABSKIP = 0; // start touch monitor!!
	CLRA	
	STA	_PABSKIP
	STA	(_PABSKIP + 1)
;	;.line	101; "tklib.c"	DUMMYC = 2;
	LDA	#0x02
	STA	_DUMMYC
;	;.line	110; "tklib.c"	if (wait_tk_int(1, callback))
	LDA	_api_tk_init_STK12
	STA	_wait_tk_int_STK01
	LDA	_api_tk_init_STK11
	STA	_wait_tk_int_STK00
	LDA	#0x01
	CALL	_wait_tk_int
	JZ	_00169_DS_
;	;.line	111; "tklib.c"	return 0;
	CLRA	
	STA	STK00
	JMP	_00189_DS_
_00169_DS_:
;	;.line	112; "tklib.c"	if (wait_tk_int(1, callback))
	LDA	_api_tk_init_STK12
	STA	_wait_tk_int_STK01
	LDA	_api_tk_init_STK11
	STA	_wait_tk_int_STK00
	LDA	#0x01
	CALL	_wait_tk_int
	JZ	_00206_DS_
;	;.line	113; "tklib.c"	return 0;
	CLRA	
	STA	STK00
	JMP	_00189_DS_
_00206_DS_:
;	;.line	116; "tklib.c"	for (sw = 0; sw < tch_total_ch; sw++)
	CLRA	
	STA	_api_tk_init_STK00
_00187_DS_:
	SETB	_C
	LDA	_api_tk_init_STK00
	SUBB	_tch_total_ch
	JC	_00175_DS_
;	;.line	118; "tklib.c"	USHORT s = rambufp1[sw].count;
	CLRA	
	STA	_MULAL
	STA	_MULBH
	LDA	_api_tk_init_STK00
	STA	_MULAH
	LDA	#0x08
	STA	_MULBL
	LDA	_MULO2A
	STA	r0x11BA
	LDA	_MULO2B
	STA	_api_tk_init_STK02
	LDA	_rambufp1
	ADD	r0x11BA
	STA	_api_tk_init_STK04
	LDA	(_rambufp1 + 1)
	ADDC	_api_tk_init_STK02
	STA	_api_tk_init_STK03
	LDA	#0x06
	ADD	_api_tk_init_STK04
	STA	_api_tk_init_STK04
	CLRA	
	ADDC	_api_tk_init_STK03
	STA	_api_tk_init_STK03
	LDA	_api_tk_init_STK04
	STA	_ROMPL
	LDA	_api_tk_init_STK03
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_api_tk_init_STK10
	LDA	@_ROMPINC
	STA	_api_tk_init_STK09
;	;.line	119; "tklib.c"	baseline[sw] = ((unsigned long)s) << 8;
	CLRA	
	STA	_MULAL
	STA	_MULBH
	LDA	_api_tk_init_STK00
	STA	_MULAH
	LDA	#0x04
	STA	_MULBL
	LDA	_MULO2A
	STA	_api_tk_init_STK04
	LDA	_MULO2B
	STA	_api_tk_init_STK03
	LDA	_baseline
	ADD	_api_tk_init_STK04
	STA	_api_tk_init_STK04
	LDA	(_baseline + 1)
	ADDC	_api_tk_init_STK03
	STA	_api_tk_init_STK03
	CLRA	
	STA	r0x11C5
	STA	r0x11C7
	LDA	_api_tk_init_STK03
	STA	_ROMPH
	LDA	_api_tk_init_STK04
	STA	_ROMPL
	LDA	r0x11C7
	STA	@_ROMPINC
	LDA	_api_tk_init_STK10
	STA	@_ROMPINC
	LDA	_api_tk_init_STK09
	STA	@_ROMPINC
	LDA	r0x11C5
	STA	@_ROMPINC
;	;.line	121; "tklib.c"	if (threshold_ratio == NULL)
	LDA	_threshold_ratio
	ORA	(_threshold_ratio + 1)
	JNZ	_00173_DS_
;	;.line	122; "tklib.c"	rambufp1[sw].threshold = s + (s >> 5); // take 3 %
	LDA	_rambufp1
	ADD	r0x11BA
	STA	_api_tk_init_STK04
	LDA	(_rambufp1 + 1)
	ADDC	_api_tk_init_STK02
	STA	_api_tk_init_STK03
	LDA	#0x04
	ADD	_api_tk_init_STK04
	STA	_api_tk_init_STK04
	CLRA	
	ADDC	_api_tk_init_STK03
	STA	_api_tk_init_STK03
	LDA	_api_tk_init_STK10
	SWA	
	AND	#0x0f
	STA	_api_tk_init_STK12
	LDA	_api_tk_init_STK09
	SWA	
	PUSH	
	AND	#0xf0
	ORA	_api_tk_init_STK12
	STA	_api_tk_init_STK12
	POP	
	AND	#0x0f
	SHR	
	STA	_api_tk_init_STK11
	LDA	_api_tk_init_STK12
	ROR	
	ADD	_api_tk_init_STK10
	STA	_api_tk_init_STK12
	LDA	_api_tk_init_STK09
	ADDC	_api_tk_init_STK11
	STA	_api_tk_init_STK11
	LDA	_api_tk_init_STK03
	STA	_ROMPH
	LDA	_api_tk_init_STK04
	STA	_ROMPL
	LDA	_api_tk_init_STK12
	STA	@_ROMPINC
	LDA	_api_tk_init_STK11
	STA	@_ROMPINC
	JMP	_00188_DS_
_00173_DS_:
;	;.line	124; "tklib.c"	rambufp1[sw].threshold = ((unsigned long)s * ((unsigned long)threshold_ratio[sw])) >> 7;
	LDA	_rambufp1
	ADD	r0x11BA
	STA	r0x11BA
	LDA	(_rambufp1 + 1)
	ADDC	_api_tk_init_STK02
	STA	_api_tk_init_STK02
	LDA	#0x04
	ADD	r0x11BA
	STA	r0x11BA
	CLRA	
	ADDC	_api_tk_init_STK02
	STA	_api_tk_init_STK02
	LDA	_api_tk_init_STK10
	STA	_api_tk_init_STK04
	LDA	_api_tk_init_STK09
	STA	_api_tk_init_STK03
	CLRA	
	STA	_api_tk_init_STK12
	STA	_api_tk_init_STK11
	LDA	_api_tk_init_STK00
	ADD	_threshold_ratio
	STA	_api_tk_init_STK10
	CLRA	
	ADDC	(_threshold_ratio + 1)
	STA	_api_tk_init_STK09
	LDA	_api_tk_init_STK10
	STA	_ROMPL
	LDA	_api_tk_init_STK09
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_api_tk_init_STK10
	CLRA	
	STA	_api_tk_init_STK09
	STA	r0x11C6
	STA	r0x11C7
	LDA	_api_tk_init_STK10
	STA	__mullong_STK06
	LDA	_api_tk_init_STK09
	STA	__mullong_STK05
	LDA	r0x11C6
	STA	__mullong_STK04
	LDA	r0x11C7
	STA	__mullong_STK03
	LDA	_api_tk_init_STK04
	STA	__mullong_STK02
	LDA	_api_tk_init_STK03
	STA	__mullong_STK01
	LDA	_api_tk_init_STK12
	STA	__mullong_STK00
	LDA	_api_tk_init_STK11
	CALL	__mullong
	STA	_api_tk_init_STK09
	LDA	STK02
	ROL	
	LDA	STK01
	ROL	
	STA	_api_tk_init_STK12
	LDA	STK00
	ROL	
	STA	_api_tk_init_STK11
	LDA	_api_tk_init_STK09
	ROL	
	CLRB	_C
	LDA	_api_tk_init_STK02
	STA	_ROMPH
	LDA	r0x11BA
	STA	_ROMPL
	LDA	_api_tk_init_STK12
	STA	@_ROMPINC
	LDA	_api_tk_init_STK11
	STA	@_ROMPINC
_00188_DS_:
;	;.line	116; "tklib.c"	for (sw = 0; sw < tch_total_ch; sw++)
	LDA	_api_tk_init_STK00
	INCA	
	STA	_api_tk_init_STK00
	JMP	_00187_DS_
_00175_DS_:
;	;.line	127; "tklib.c"	if (mon == 1) // use PB SSPI
	LDA	_api_tk_init_STK01
	XOR	#0x01
	JNZ	_00179_DS_
;	;.line	129; "tklib.c"	SSPIC = 1;
	LDA	#0x01
	STA	_SSPIC
	JMP	_00180_DS_
_00179_DS_:
;	;.line	131; "tklib.c"	else if (mon == 2) // use PC SSPI
	LDA	_api_tk_init_STK01
	XOR	#0x02
	JNZ	_00180_DS_
;	;.line	133; "tklib.c"	SSPIC = 5;
	LDA	#0x05
	STA	_SSPIC
_00180_DS_:
;	;.line	135; "tklib.c"	TOUCHC = TOUCHC_DEFAULT;
	LDA	#0x49
	STA	_TOUCHC
;	;.line	136; "tklib.c"	return init_result;
	LDA	r0x11C3
	STA	STK00
	LDA	r0x11C4
_00189_DS_:
;	;.line	137; "tklib.c"	}
	RET	
; exit point of _api_tk_init
	.ENDFUNC _api_tk_init

;--------------------------------------------------------
	.FUNC _wait_tk_int:$PNUM 3:$C:_api_enter_stdby_mode\
:$L:r0x11A3:$L:_wait_tk_int_STK00:$L:_wait_tk_int_STK01
;--------------------------------------------------------
;	.line	13; "tklib.c"	static BYTE wait_tk_int(BYTE stdby, BYTE (*callback)(void))
_wait_tk_int:	;Function start
	STA	r0x11A3
	LDA	_wait_tk_int_STK01
_00112_DS_:
;	;.line	15; "tklib.c"	while (!(TOUCHC & 0x80))
	LDA	_TOUCHC
	JMI	_00114_DS_
;	;.line	17; "tklib.c"	if (callback != NULL)
	LDA	_wait_tk_int_STK01
	ORA	_wait_tk_int_STK00
	JZ	_00110_DS_
;	;.line	19; "tklib.c"	if (callback())
	CALL	_00139_DS_
	JMP	_00140_DS_
_00139_DS_:
	CALL	_00141_DS_
_00141_DS_:
	LDA	_wait_tk_int_STK00
	STA	_STACKH
	LDA	_wait_tk_int_STK01
	STA	_STACKL
	RET	
_00140_DS_:
	JZ	_00112_DS_
;	;.line	20; "tklib.c"	return 1;
	LDA	#0x01
	JMP	_00115_DS_
_00110_DS_:
;	;.line	22; "tklib.c"	else if (stdby)
	LDA	r0x11A3
	JZ	_00112_DS_
;	;.line	23; "tklib.c"	api_enter_stdby_mode(0, 0, 0, 0, 0);
	CLRA	
	STA	_api_enter_stdby_mode_STK03
	STA	_api_enter_stdby_mode_STK02
	STA	_api_enter_stdby_mode_STK01
	STA	_api_enter_stdby_mode_STK00
	CLRA	
	CALL	_api_enter_stdby_mode
	JMP	_00112_DS_
_00114_DS_:
;	;.line	25; "tklib.c"	return 0;
	CLRA	
_00115_DS_:
;	;.line	26; "tklib.c"	}
	RET	
; exit point of _wait_tk_int
	.ENDFUNC _wait_tk_int
	;--cdb--S:G$api_tk_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_init$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_pre_erase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start_no_erase$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop_noerase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_noer$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_chspick$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:Ftklib$wait_tk_int$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$_mulint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$PAR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PADIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOB$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMA_IL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIDAT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCLKDIV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CMPCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTVC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMICON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PRG_RAM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIDMAC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECOAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECLEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECMODE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIOPRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMALEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULB$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULBL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULBH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULO$0$0({4}SL:U),E,0,0
	;--cdb--S:G$MULO1$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULSHIFT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUF$0$0({2}SI:S),E,0,0
	;--cdb--S:G$L2UBUFL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUFH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2USH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$U2LSH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTPRI$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRA$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$LPWMRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMINV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPICK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPSEL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$HWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWDINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PATEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TRAMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMBUFP$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$PASKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBSKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABSKIP$0$0({2}SI:U),E,0,0
	;--cdb--S:G$PATR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTR$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TOUCHC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DUMMYC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IRCD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMDUTY$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMPER$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACGCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECPWR$0$0({4}SL:U),E,0,0
	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$HWPALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0UW$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$EA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$tch_total_ch$0$0({1}SC:U),E,0,0
	;--cdb--S:G$tch_mon$0$0({1}SC:U),E,0,0
	;--cdb--S:G$rambufp1$0$0({2}DG,STtouchen:S),E,0,0
	;--cdb--S:G$procdatap$0$0({2}DG,SI:U),E,0,0
	;--cdb--S:G$baseline$0$0({2}DG,SL:U),E,0,0
	;--cdb--S:G$threshold_ratio$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:Ltklib.wait_tk_int$callback$65536$57({2}DC,DF,SC:U),R,0,0,[_wait_tk_int_STK01,_wait_tk_int_STK00]
	;--cdb--S:Ltklib.wait_tk_int$stdby$65536$57({1}SC:U),R,0,0,[r0x11A3]
	;--cdb--S:Ltklib.api_tk_init$callback$65536$62({2}DC,DF,SC:U),R,0,0,[_api_tk_init_STK12,_api_tk_init_STK11]
	;--cdb--S:Ltklib.api_tk_init$th_ratio$65536$62({2}DG,SC:U),R,0,0,[_api_tk_init_STK10,_api_tk_init_STK09]
	;--cdb--S:Ltklib.api_tk_init$baselinei$65536$62({2}DG,SL:U),R,0,0,[]
	;--cdb--S:Ltklib.api_tk_init$finalp$65536$62({2}DG,SI:U),R,0,0,[]
	;--cdb--S:Ltklib.api_tk_init$bufp$65536$62({2}DG,SC:U),R,0,0,[_api_tk_init_STK04,_api_tk_init_STK03]
	;--cdb--S:Ltklib.api_tk_init$totalch$65536$62({1}SC:U),R,0,0,[_api_tk_init_STK02]
	;--cdb--S:Ltklib.api_tk_init$mon$65536$62({1}SC:U),R,0,0,[_api_tk_init_STK01]
	;--cdb--S:Ltklib.api_tk_init$ch_en$65536$62({2}SI:U),R,0,0,[_api_tk_init_STK00,r0x11BA]
	;--cdb--S:Ltklib.api_tk_init$tkramp$65536$64({2}DG,STtouchen:S),R,0,0,[]
	;--cdb--S:Ltklib.api_tk_init$chk_channel$65536$64({2}SI:U),R,0,0,[_api_tk_init_STK03,_api_tk_init_STK10]
	;--cdb--S:Ltklib.api_tk_init$init_result$65536$64({2}SI:U),R,0,0,[r0x11C3,r0x11C4]
	;--cdb--S:Ltklib.api_tk_init$sw$65536$64({1}SC:U),R,0,0,[_api_tk_init_STK09]
	;--cdb--S:Ltklib.api_tk_init$dum$65536$64({1}SC:U),R,0,0,[r0x11C5]
	;--cdb--S:Ltklib.api_tk_init$s$196608$73({2}SI:U),R,0,0,[_api_tk_init_STK10,_api_tk_init_STK09]
	;--cdb--S:G$api_tk_init$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_tk_stop$0$0({2}DF,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_memset
	.globl	_api_enter_stdby_mode
	.globl	__mulint
	.globl	__mullong
	.globl	_PAR
	.globl	_PADIR
	.globl	_PIOA
	.globl	_PAWK
	.globl	_PAWKDR
	.globl	_TIMERC
	.globl	_THRLD
	.globl	_RAMP0L
	.globl	_RAMP0H
	.globl	_RAMP1L
	.globl	_RAMP1H
	.globl	_PTRCL
	.globl	_PTRCH
	.globl	_ROMPL
	.globl	_ROMPH
	.globl	_BEEPC
	.globl	_FILTERGR
	.globl	_ULAWC
	.globl	_STACKL
	.globl	_STACKH
	.globl	_ADCON
	.globl	_DACON
	.globl	_SYSC
	.globl	_SPIM
	.globl	_SPIH
	.globl	_SPIMH
	.globl	_SPIOP
	.globl	_SPI_BANK
	.globl	_ADP_IND
	.globl	_ADP_VPL
	.globl	_ADP_VPH
	.globl	_ADL
	.globl	_ADH
	.globl	_ZC
	.globl	_ADCG
	.globl	_DAC_PL
	.globl	_DAC_PH
	.globl	_PAG
	.globl	_RDMAL
	.globl	_RDMAH
	.globl	_SPIL
	.globl	_IOMASK
	.globl	_IOCMP
	.globl	_IOCNT
	.globl	_LVDCON
	.globl	_LVDCTH
	.globl	_LVRCON
	.globl	_OFFSETL
	.globl	_OFFSETH
	.globl	_RCCON
	.globl	_BGCON
	.globl	_PWRL
	.globl	_CRYPT
	.globl	_PWRH
	.globl	_PWRHL
	.globl	_IROMDL
	.globl	_IROMDH
	.globl	_RECMUTE
	.globl	_SPKC
	.globl	_DCLAMP
	.globl	_SSPIC
	.globl	_SSPIL
	.globl	_SSPIM
	.globl	_SSPIH
	.globl	_PBR
	.globl	_PBDIR
	.globl	_PIOB
	.globl	_PBWK
	.globl	_PBWKDR
	.globl	_PAIE
	.globl	_PBIE
	.globl	_PAIF
	.globl	_PBIF
	.globl	_GIE
	.globl	_GIF
	.globl	_WDTL
	.globl	_WDTH
	.globl	_RPAGES
	.globl	_PPAGES
	.globl	_DMA_IL
	.globl	_FILTERGP
	.globl	_SPIDAT
	.globl	_RSPIC
	.globl	_RCLKDIV
	.globl	_PCR
	.globl	_PCDIR
	.globl	_PIOC
	.globl	_CMPCON
	.globl	_INTVC
	.globl	_INTV
	.globl	_DMICON
	.globl	_PRG_RAM
	.globl	_PDMAL
	.globl	_PDMAH
	.globl	_PDMALH
	.globl	_SPIDMAC
	.globl	_SDMAAL
	.globl	_SDMAAH
	.globl	_SDMAALH
	.globl	_ECRAL
	.globl	_ECRAH
	.globl	_ECRALH
	.globl	_ECOAL
	.globl	_ECOAH
	.globl	_ECOALH
	.globl	_ECLEN
	.globl	_ECCON
	.globl	_ECMODE
	.globl	_SPIOPRAH
	.globl	_SDMALEN
	.globl	_MULA
	.globl	_MULAL
	.globl	_MULAH
	.globl	_MULB
	.globl	_MULBL
	.globl	_MULBH
	.globl	_MULO
	.globl	_MULO1
	.globl	_MULSHIFT
	.globl	_L2UBUF
	.globl	_L2UBUFL
	.globl	_L2UBUFH
	.globl	_ULAWD
	.globl	_L2USH
	.globl	_U2LSH
	.globl	_INTPRI
	.globl	_LPWMRA
	.globl	_LPWMRAL
	.globl	_LPWMRAH
	.globl	_LPWMEN
	.globl	_LPWMINV
	.globl	_SPICK
	.globl	_SYSC2
	.globl	_HWPSEL
	.globl	_HWPAL
	.globl	_HWPAH
	.globl	_HWPA
	.globl	_HWD
	.globl	_HWDINC
	.globl	_PATEN
	.globl	_PBTEN
	.globl	_PABTEN
	.globl	_TRAMAL
	.globl	_TRAMAH
	.globl	_TRAMBUFP
	.globl	_PASKIP
	.globl	_PBSKIP
	.globl	_PABSKIP
	.globl	_PATR
	.globl	_PBTR
	.globl	_PABTR
	.globl	_TOUCHC
	.globl	_DUMMYC
	.globl	_IRCD
	.globl	_FPWMEN
	.globl	_FPWMDUTY
	.globl	_FPWMPER
	.globl	_DACGCL
	.globl	_RECPWR
	.globl	_ICE0
	.globl	_ICE1
	.globl	_ICE2
	.globl	_ICE3
	.globl	_ICE4
	.globl	_RAMP0
	.globl	_RAMP0INC
	.globl	_RAMP1
	.globl	_RDMALH
	.globl	_HWPALH
	.globl	_RAMP1INC
	.globl	_RAMP1INC2
	.globl	_ROMP
	.globl	_ROMPINC
	.globl	_ROMPINC2
	.globl	_ACC
	.globl	_RAMP0UW
	.globl	_RAMP1UW
	.globl	_ROMPLH
	.globl	_ROMPUW
	.globl	_OFFSETLH
	.globl	_ADP_VPLH
	.globl	_TOV
	.globl	_EA
	.globl	_OV

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_api_tk_init
	.globl	_tch_total_ch
	.globl	_tch_mon
	.globl	_rambufp1
	.globl	_procdatap
	.globl	_baseline
	.globl	_threshold_ratio
	.globl	_api_tk_init_tkramp_65536_64
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
_tch_total_ch:	.ds	1

	.area DSEG(DATA)
_tch_mon:	.ds	1

	.area DSEG(DATA)
_rambufp1:	.ds	2

	.area DSEG(DATA)
_procdatap:	.ds	2

	.area DSEG(DATA)
_baseline:	.ds	2

	.area DSEG(DATA)
_threshold_ratio:	.ds	2

	.area DSEG(DATA)
_api_tk_init_tkramp_65536_64:	.ds	2

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_tklib_0	udata
r0x11A3:	.ds	1
r0x11BA:	.ds	1
r0x11C3:	.ds	1
r0x11C4:	.ds	1
r0x11C5:	.ds	1
r0x11C6:	.ds	1
r0x11C7:	.ds	1
r0x11C8:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_wait_tk_int_STK00:	.ds	1
_wait_tk_int_STK01:	.ds	1
	.globl _api_enter_stdby_mode_STK03
	.globl _api_enter_stdby_mode_STK02
	.globl _api_enter_stdby_mode_STK01
	.globl _api_enter_stdby_mode_STK00
_api_tk_init_STK00:	.ds	1
	.globl _api_tk_init_STK00
_api_tk_init_STK01:	.ds	1
	.globl _api_tk_init_STK01
_api_tk_init_STK02:	.ds	1
	.globl _api_tk_init_STK02
_api_tk_init_STK03:	.ds	1
	.globl _api_tk_init_STK03
_api_tk_init_STK04:	.ds	1
	.globl _api_tk_init_STK04
_api_tk_init_STK05:	.ds	1
	.globl _api_tk_init_STK05
_api_tk_init_STK06:	.ds	1
	.globl _api_tk_init_STK06
_api_tk_init_STK07:	.ds	1
	.globl _api_tk_init_STK07
_api_tk_init_STK08:	.ds	1
	.globl _api_tk_init_STK08
_api_tk_init_STK09:	.ds	1
	.globl _api_tk_init_STK09
_api_tk_init_STK10:	.ds	1
	.globl _api_tk_init_STK10
_api_tk_init_STK11:	.ds	1
	.globl _api_tk_init_STK11
_api_tk_init_STK12:	.ds	1
	.globl _api_tk_init_STK12
	.globl _memset_STK03
	.globl _memset_STK02
	.globl _memset_STK01
	.globl _memset_STK00
	.globl __mullong_STK06
	.globl __mullong_STK05
	.globl __mullong_STK04
	.globl __mullong_STK03
	.globl __mullong_STK02
	.globl __mullong_STK01
	.globl __mullong_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x11C6:NULL+0:-1:1
	;--cdb--W:r0x11C7:NULL+0:-1:1
	;--cdb--W:_api_tk_init_STK00:NULL+0:-1:1
	;--cdb--W:_api_tk_init_STK12:NULL+0:-1:1
	;--cdb--W:r0x11C5:NULL+0:-1:1
	;--cdb--W:_api_tk_init_STK04:NULL+0:-1:1
	;--cdb--W:_api_tk_init_STK03:NULL+0:13:0
	;--cdb--W:_api_tk_init_STK03:NULL+0:4566:0
	;--cdb--W:_api_tk_init_STK04:NULL+0:4567:0
	;--cdb--W:_api_tk_init_STK10:NULL+0:14:0
	;--cdb--W:_api_tk_init_STK11:NULL+0:4564:0
	;--cdb--W:_api_tk_init_STK12:NULL+0:4565:0
	;--cdb--W:r0x11C8:NULL+0:4565:0
	;--cdb--W:r0x11CA:NULL+0:4549:0
	;--cdb--W:r0x11C9:NULL+0:4564:0
	;--cdb--W:r0x11A6:NULL+0:-1:1
	end
