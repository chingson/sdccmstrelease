;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_fssub.c"
	.module _fssub
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$__fssub$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _fssub-code 
.globl ___fssub

;--------------------------------------------------------
	.FUNC ___fssub:$PNUM 8:$C:___fsadd\
:$L:r0x1162:$L:___fssub_STK00:$L:___fssub_STK01:$L:___fssub_STK02:$L:___fssub_STK03\
:$L:___fssub_STK04:$L:___fssub_STK05:$L:___fssub_STK06
;--------------------------------------------------------
;	.line	75; "_fssub.c"	float neg = -a1;
___fssub:	;Function start
	XOR	#0x80
	STA	r0x1162
;	;.line	76; "_fssub.c"	return -(neg + a2);
	LDA	___fssub_STK06
	STA	___fsadd_STK06
	LDA	___fssub_STK05
	STA	___fsadd_STK05
	LDA	___fssub_STK04
	STA	___fsadd_STK04
	LDA	___fssub_STK03
	STA	___fsadd_STK03
	LDA	___fssub_STK02
	STA	___fsadd_STK02
	LDA	___fssub_STK01
	STA	___fsadd_STK01
	LDA	___fssub_STK00
	STA	___fsadd_STK00
	LDA	r0x1162
	CALL	___fsadd
	XOR	#0x80
;	;.line	77; "_fssub.c"	}
	RET	
; exit point of ___fssub
	.ENDFUNC ___fssub
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:L_fssub.__fssub$a2$65536$20({4}SF:S),R,0,0,[___fssub_STK06,___fssub_STK05,___fssub_STK04,___fssub_STK03]
	;--cdb--S:L_fssub.__fssub$a1$65536$20({4}SF:S),R,0,0,[___fssub_STK02,___fssub_STK01,___fssub_STK00,r0x1162]
	;--cdb--S:L_fssub.__fssub$neg$65536$21({4}SF:S),R,0,0,[___fssub_STK02,___fssub_STK01,___fssub_STK00,r0x1162]
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	___fsadd

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___fssub
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__fssub_0	udata
r0x1162:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
___fssub_STK00:	.ds	1
	.globl ___fssub_STK00
___fssub_STK01:	.ds	1
	.globl ___fssub_STK01
___fssub_STK02:	.ds	1
	.globl ___fssub_STK02
___fssub_STK03:	.ds	1
	.globl ___fssub_STK03
___fssub_STK04:	.ds	1
	.globl ___fssub_STK04
___fssub_STK05:	.ds	1
	.globl ___fssub_STK05
___fssub_STK06:	.ds	1
	.globl ___fssub_STK06
	.globl ___fsadd_STK06
	.globl ___fsadd_STK05
	.globl ___fsadd_STK04
	.globl ___fsadd_STK03
	.globl ___fsadd_STK02
	.globl ___fsadd_STK01
	.globl ___fsadd_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:___fssub_STK00:NULL+0:14:0
	;--cdb--W:___fssub_STK01:NULL+0:13:0
	;--cdb--W:___fssub_STK02:NULL+0:12:0
	;--cdb--W:r0x1162:NULL+0:-1:1
	end
