;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.3.0 #8604 (Sep 15 2014) (MSVC)
; This file was generated Sat Oct 11 21:41:17 2014
;--------------------------------------------------------
; MST port for the MS322 series simple CPU
;--------------------------------------------------------
	.module _shl_long
	;.list	p=MS205
	;.radix dec
	.include "ms326sfr.def"
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
;--------------------------------------------------------
; Code Head 
;--------------------------------------------------------
	.area CSEG	 (CODE) 
.FUNC	__shl_long
;--------------------------------------------------------
; code
;--------------------------------------------------------
; sh ulong, assembly is the source!!
; input: _PTRCL is the data byte
; input STK00 is MSB!!
; input: ACC is the shift num
__shl_long:	;Function start
; 2 exit points
	sta 	_ROMPL ; temp var
	add	#0xe0
	jc	shift_too_large
	lda	_ROMPL
loop_shl:
	jnz	cont_shift
	ret
cont_shift:
	lda	_PTRCL
	shl
	sta	_PTRCL
	lda	STK02
	rol
	sta	STK02
	lda	STK01
	rol
	sta	STK01
	lda	STK00
	rol
	sta	STK00
	lda	_ROMPL
	deca
	sta	_ROMPL
	jmp	loop_shl

	
shift_too_large:
	clra
	sta	_PTRCL
	sta	STK00
	sta	STK01
	sta	STK02
	ret

; exit point of _shl_long


;	code size estimation:
;	   21+   10 =    31 instructions (   31 byte)

;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__shl_long
	.globl  STK00
	.globl  STK01
	.globl  STK02

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	;end
