;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"mbtowc.c"
	.module mbtowc
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fmbtowc$__00000000[({0}S:S$c$0$0({3}DA3d,SC:U),Z,0,0)]
	;--cdb--T:Fmbtowc$tm[]
	;--cdb--S:G$wcscmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wcslen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$btowc$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$wctob$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbsinit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbrlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$mbrtowc$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcrtomb$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$mbtowc$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; mbtowc-code 
.globl _mbtowc

;--------------------------------------------------------
	.FUNC _mbtowc:$PNUM 6:$C:__shr_sshort\
:$L:r0x1160:$L:_mbtowc_STK00:$L:_mbtowc_STK01:$L:_mbtowc_STK02:$L:_mbtowc_STK03\
:$L:_mbtowc_STK04:$L:r0x1165:$L:r0x1166:$L:r0x1167:$L:r0x1169\
:$L:r0x1168:$L:r0x116A:$L:r0x116B:$L:r0x116C:$L:r0x116D\
:$L:r0x1171:$L:r0x1172
;--------------------------------------------------------
;	.line	31; "mbtowc.c"	int mbtowc(wchar_t *pwc, const char *restrict s, size_t n)
_mbtowc:	;Function start
	STA	r0x1160
;	;.line	37; "mbtowc.c"	if(!s)
	LDA	_mbtowc_STK02
	ORA	_mbtowc_STK01
	JNZ	_00106_DS_
;	;.line	38; "mbtowc.c"	return(0);
	CLRA	
	STA	STK00
	JMP	_00124_DS_
_00106_DS_:
;	;.line	40; "mbtowc.c"	seqlen = 1;
	LDA	#0x01
	STA	r0x1165
;	;.line	41; "mbtowc.c"	first_byte = *s;
	LDA	_mbtowc_STK02
	STA	_ROMPL
	LDA	_mbtowc_STK01
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1166
;	;.line	43; "mbtowc.c"	if(first_byte & 0x80)
	JPL	_00111_DS_
;	;.line	45; "mbtowc.c"	while (first_byte & (0x80 >> seqlen))
	LDA	#0x01
	STA	r0x1167
_00107_DS_:
	CLRA	
	CLRA	
	STA	STK00
	LDA	#0x80
	STA	_PTRCL
	LDA	r0x1167
	CALL	__shr_sshort
	LDA	_PTRCL
	AND	r0x1166
	STA	r0x1168
	CLRA	
	AND	STK00
	ORA	r0x1168
	JZ	_00135_DS_
;	;.line	46; "mbtowc.c"	seqlen++;
	LDA	r0x1167
	INCA	
	STA	r0x1167
	JMP	_00107_DS_
_00135_DS_:
	LDA	r0x1167
	STA	r0x1165
;	;.line	47; "mbtowc.c"	first_byte &= (0xff >> (seqlen + 1));
	LDA	r0x1167
	INCA	
	STA	r0x1167
	CLRA	
	STA	STK00
	DECA	
	STA	_PTRCL
	LDA	r0x1167
	CALL	__shr_sshort
	LDA	_PTRCL
	AND	r0x1166
	STA	r0x1166
_00111_DS_:
;	;.line	50; "mbtowc.c"	if(n < seqlen)
	SETB	_C
	LDA	_mbtowc_STK04
	SUBB	r0x1165
	LDA	_mbtowc_STK03
	SUBB	#0x00
	JC	_00132_DS_
;	;.line	51; "mbtowc.c"	return(-1);
	LDA	#0xff
	STA	STK00
	JMP	_00124_DS_
_00132_DS_:
;	;.line	53; "mbtowc.c"	for(i = 1; i < seqlen; i++)
	LDA	#0x01
	STA	_mbtowc_STK04
_00119_DS_:
	SETB	_C
	LDA	_mbtowc_STK04
	SUBB	r0x1165
	JC	_00116_DS_
;	;.line	54; "mbtowc.c"	if((s[i] & 0xc0) != 0x80)
	LDA	_mbtowc_STK04
	ADD	_mbtowc_STK02
	STA	_mbtowc_STK03
	CLRA	
	ADDC	_mbtowc_STK01
	STA	r0x1167
	LDA	_mbtowc_STK03
	STA	_ROMPL
	LDA	r0x1167
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0xc0
	XOR	#0x80
	JZ	_00120_DS_
;	;.line	55; "mbtowc.c"	return(-1);
	LDA	#0xff
	STA	STK00
	JMP	_00124_DS_
_00120_DS_:
;	;.line	53; "mbtowc.c"	for(i = 1; i < seqlen; i++)
	LDA	_mbtowc_STK04
	INCA	
	STA	_mbtowc_STK04
	JMP	_00119_DS_
_00116_DS_:
;	;.line	57; "mbtowc.c"	codepoint = first_byte;
	LDA	r0x1166
	STA	_mbtowc_STK04
	CLRA	
	STA	_mbtowc_STK03
	STA	r0x1167
	STA	r0x1168
;	;.line	59; "mbtowc.c"	for(s++, i = seqlen - 1; i; i--)
	LDA	_mbtowc_STK02
	INCA	
	STA	_mbtowc_STK02
	CLRA	
	ADDC	_mbtowc_STK01
	STA	_mbtowc_STK01
	LDA	r0x1165
	DECA	
	STA	r0x1166
_00122_DS_:
	LDA	r0x1166
	JZ	_00117_DS_
;	;.line	61; "mbtowc.c"	codepoint <<= 6;
	LDA	r0x1168
	SWA	
	AND	#0xf0
	STA	r0x116C
	LDA	r0x1167
	SWA	
	STA	_PTRCL
	AND	#0x0f
	ORA	r0x116C
	STA	r0x116C
	LDA	_PTRCL
	AND	#0xf0
	STA	r0x116B
	LDA	_mbtowc_STK03
	SWA	
	STA	_PTRCL
	AND	#0x0f
	ORA	r0x116B
	STA	r0x116B
	LDA	_PTRCL
	AND	#0xf0
	STA	r0x116A
	LDA	_mbtowc_STK04
	SWA	
	STA	_PTRCL
	AND	#0x0f
	ORA	r0x116A
	STA	r0x116A
	LDA	_PTRCL
	AND	#0xf0
	SHL	
	STA	r0x1169
	LDA	r0x116A
	ROL	
	STA	r0x116A
	LDA	r0x116B
	ROL	
	STA	r0x116B
	LDA	r0x116C
	ROL	
	STA	r0x116C
	LDA	r0x1169
	SHL	
	STA	r0x1169
	LDA	r0x116A
	ROL	
	STA	r0x116A
	LDA	r0x116B
	ROL	
	STA	r0x116B
	LDA	r0x116C
	ROL	
	STA	r0x116C
;	;.line	62; "mbtowc.c"	codepoint |= (*s & 0x3f);
	LDA	_mbtowc_STK02
	STA	_ROMPL
	LDA	_mbtowc_STK01
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x3f
	STA	r0x116D
	CLRA	
	JPL	_00207_DS_
	LDA	#0xff
	JMP	_00208_DS_
_00207_DS_:
	CLRA	
_00208_DS_:
	STA	r0x1171
	STA	r0x1172
	LDA	r0x116D
	ORA	r0x1169
	STA	_mbtowc_STK04
	LDA	r0x116A
	STA	_mbtowc_STK03
	LDA	r0x1171
	ORA	r0x116B
	STA	r0x1167
	LDA	r0x1172
	ORA	r0x116C
	STA	r0x1168
;	;.line	63; "mbtowc.c"	s++;
	LDA	_mbtowc_STK02
	INCA	
	STA	_mbtowc_STK02
	CLRA	
	ADDC	_mbtowc_STK01
	STA	_mbtowc_STK01
;	;.line	59; "mbtowc.c"	for(s++, i = seqlen - 1; i; i--)
	LDA	r0x1166
	DECA	
	STA	r0x1166
	JMP	_00122_DS_
_00117_DS_:
;	;.line	66; "mbtowc.c"	*pwc = codepoint;
	LDA	r0x1160
	STA	_ROMPH
	LDA	_mbtowc_STK00
	STA	_ROMPL
	LDA	_mbtowc_STK04
	STA	@_ROMPINC
	LDA	_mbtowc_STK03
	STA	@_ROMPINC
	LDA	r0x1167
	STA	@_ROMPINC
	LDA	r0x1168
	STA	@_ROMPINC
;	;.line	67; "mbtowc.c"	return(seqlen);
	LDA	r0x1165
	STA	STK00
	CLRA	
_00124_DS_:
;	;.line	68; "mbtowc.c"	}
	RET	
; exit point of _mbtowc
	.ENDFUNC _mbtowc
	;--cdb--S:G$wcscmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wcslen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$btowc$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$wctob$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbsinit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbrlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$mbrtowc$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcrtomb$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:Lmbtowc.mbtowc$n$65536$9({2}SI:U),R,0,0,[_mbtowc_STK04,_mbtowc_STK03]
	;--cdb--S:Lmbtowc.mbtowc$s$65536$9({2}DG,SC:U),R,0,0,[_mbtowc_STK02,_mbtowc_STK01]
	;--cdb--S:Lmbtowc.mbtowc$pwc$65536$9({2}DG,SL:U),R,0,0,[_mbtowc_STK00,r0x1160]
	;--cdb--S:Lmbtowc.mbtowc$codepoint$65536$10({4}SL:U),R,0,0,[r0x1169,r0x116A,r0x116B,r0x116C]
	;--cdb--S:Lmbtowc.mbtowc$seqlen$65536$10({1}SC:U),R,0,0,[r0x1165]
	;--cdb--S:Lmbtowc.mbtowc$i$65536$10({1}SC:U),R,0,0,[]
	;--cdb--S:Lmbtowc.mbtowc$first_byte$65536$10({1}SC:U),R,0,0,[r0x1166]
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__shr_sshort

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_mbtowc
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_mbtowc_0	udata
r0x1160:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116C:	.ds	1
r0x116D:	.ds	1
r0x1171:	.ds	1
r0x1172:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_mbtowc_STK00:	.ds	1
	.globl _mbtowc_STK00
_mbtowc_STK01:	.ds	1
	.globl _mbtowc_STK01
_mbtowc_STK02:	.ds	1
	.globl _mbtowc_STK02
_mbtowc_STK03:	.ds	1
	.globl _mbtowc_STK03
_mbtowc_STK04:	.ds	1
	.globl _mbtowc_STK04
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1169:NULL+0:-1:1
	;--cdb--W:r0x1167:NULL+0:-1:1
	;--cdb--W:r0x1166:NULL+0:-1:1
	;--cdb--W:r0x116F:NULL+0:-1:1
	;--cdb--W:r0x1160:NULL+0:0:0
	;--cdb--W:_mbtowc_STK00:NULL+0:4453:0
	;--cdb--W:_mbtowc_STK03:NULL+0:4456:0
	;--cdb--W:r0x1167:NULL+0:4456:0
	;--cdb--W:r0x1167:NULL+0:4453:0
	;--cdb--W:r0x1169:NULL+0:0:0
	;--cdb--W:r0x1169:NULL+0:14:0
	;--cdb--W:r0x1168:NULL+0:128:0
	;--cdb--W:r0x1168:NULL+0:255:0
	;--cdb--W:r0x1168:NULL+0:0:0
	;--cdb--W:r0x116A:NULL+0:4454:0
	;--cdb--W:r0x116B:NULL+0:0:0
	;--cdb--W:r0x116E:NULL+0:4461:0
	;--cdb--W:r0x116F:NULL+0:4464:0
	;--cdb--W:r0x1170:NULL+0:0:0
	;--cdb--W:r0x1168:NULL+0:-1:1
	;--cdb--W:r0x116B:NULL+0:-1:1
	;--cdb--W:r0x1170:NULL+0:-1:1
	;--cdb--W:r0x1160:NULL+0:-1:1
	;--cdb--W:r0x116D:NULL+0:-1:1
	end
