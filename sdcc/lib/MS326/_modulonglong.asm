;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_modulonglong.c"
	.module _modulonglong
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$_modulonglong$0$0({2}DF,SI:U),C,0,0
	;--cdb--F:G$_modulonglong$0$0({2}DF,SI:U),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _modulonglong-code 
.globl __modulonglong

;--------------------------------------------------------
	.FUNC __modulonglong:$PNUM 16:$L:r0x1166:$L:__modulonglong_STK00:$L:__modulonglong_STK01:$L:__modulonglong_STK02:$L:__modulonglong_STK03\
:$L:__modulonglong_STK04:$L:__modulonglong_STK05:$L:__modulonglong_STK06:$L:__modulonglong_STK07:$L:__modulonglong_STK08\
:$L:__modulonglong_STK09:$L:__modulonglong_STK10:$L:__modulonglong_STK11:$L:__modulonglong_STK12:$L:__modulonglong_STK13\
:$L:__modulonglong_STK14:$L:r0x116F:$L:r0x1170
;--------------------------------------------------------
;	.line	38; "_modulonglong.c"	_modulonglong (unsigned long long a, unsigned long long b)
__modulonglong:	;Function start
	STA	r0x1166
;	;.line	40; "_modulonglong.c"	unsigned char count = 0;
	CLRA	
	STA	r0x116F
;	;.line	42; "_modulonglong.c"	while (!MSB_SET(b))
	STA	r0x1170
_00107_DS_:
	LDA	__modulonglong_STK07
	AND	#0x80
	DECA	
	CLRA	
	ROL	
	JNZ	_00112_DS_
;	;.line	44; "_modulonglong.c"	b <<= 1;
	LDA	__modulonglong_STK14
	SHL	
	STA	__modulonglong_STK14
	LDA	__modulonglong_STK13
	ROL	
	STA	__modulonglong_STK13
	LDA	__modulonglong_STK12
	ROL	
	STA	__modulonglong_STK12
	LDA	__modulonglong_STK11
	ROL	
	STA	__modulonglong_STK11
	LDA	__modulonglong_STK10
	ROL	
	STA	__modulonglong_STK10
	LDA	__modulonglong_STK09
	ROL	
	STA	__modulonglong_STK09
	LDA	__modulonglong_STK08
	ROL	
	STA	__modulonglong_STK08
	LDA	__modulonglong_STK07
	ROL	
	STA	__modulonglong_STK07
;	;.line	45; "_modulonglong.c"	if (b > a)
	SETB	_C
	LDA	__modulonglong_STK06
	SUBB	__modulonglong_STK14
	LDA	__modulonglong_STK05
	SUBB	__modulonglong_STK13
	LDA	__modulonglong_STK04
	SUBB	__modulonglong_STK12
	LDA	__modulonglong_STK03
	SUBB	__modulonglong_STK11
	LDA	__modulonglong_STK02
	SUBB	__modulonglong_STK10
	LDA	__modulonglong_STK01
	SUBB	__modulonglong_STK09
	LDA	__modulonglong_STK00
	SUBB	__modulonglong_STK08
	LDA	r0x1166
	SUBB	__modulonglong_STK07
	JC	_00106_DS_
;	;.line	47; "_modulonglong.c"	b >>=1;
	LDA	__modulonglong_STK07
	SHR	
	STA	__modulonglong_STK07
	LDA	__modulonglong_STK08
	ROR	
	STA	__modulonglong_STK08
	LDA	__modulonglong_STK09
	ROR	
	STA	__modulonglong_STK09
	LDA	__modulonglong_STK10
	ROR	
	STA	__modulonglong_STK10
	LDA	__modulonglong_STK11
	ROR	
	STA	__modulonglong_STK11
	LDA	__modulonglong_STK12
	ROR	
	STA	__modulonglong_STK12
	LDA	__modulonglong_STK13
	ROR	
	STA	__modulonglong_STK13
	LDA	__modulonglong_STK14
	ROR	
	STA	__modulonglong_STK14
;	;.line	48; "_modulonglong.c"	break;
	JMP	_00112_DS_
_00106_DS_:
;	;.line	50; "_modulonglong.c"	count++;
	LDA	r0x1170
	INCA	
	STA	r0x1170
	STA	r0x116F
	JMP	_00107_DS_
_00112_DS_:
;	;.line	54; "_modulonglong.c"	if (a >= b)
	SETB	_C
	LDA	__modulonglong_STK06
	SUBB	__modulonglong_STK14
	LDA	__modulonglong_STK05
	SUBB	__modulonglong_STK13
	LDA	__modulonglong_STK04
	SUBB	__modulonglong_STK12
	LDA	__modulonglong_STK03
	SUBB	__modulonglong_STK11
	LDA	__modulonglong_STK02
	SUBB	__modulonglong_STK10
	LDA	__modulonglong_STK01
	SUBB	__modulonglong_STK09
	LDA	__modulonglong_STK00
	SUBB	__modulonglong_STK08
	LDA	r0x1166
	SUBB	__modulonglong_STK07
	JNC	_00111_DS_
;	;.line	55; "_modulonglong.c"	a -= b;
	SETB	_C
	LDA	__modulonglong_STK06
	SUBB	__modulonglong_STK14
	STA	__modulonglong_STK06
	LDA	__modulonglong_STK05
	SUBB	__modulonglong_STK13
	STA	__modulonglong_STK05
	LDA	__modulonglong_STK04
	SUBB	__modulonglong_STK12
	STA	__modulonglong_STK04
	LDA	__modulonglong_STK03
	SUBB	__modulonglong_STK11
	STA	__modulonglong_STK03
	LDA	__modulonglong_STK02
	SUBB	__modulonglong_STK10
	STA	__modulonglong_STK02
	LDA	__modulonglong_STK01
	SUBB	__modulonglong_STK09
	STA	__modulonglong_STK01
	LDA	__modulonglong_STK00
	SUBB	__modulonglong_STK08
	STA	__modulonglong_STK00
	LDA	r0x1166
	SUBB	__modulonglong_STK07
	STA	r0x1166
_00111_DS_:
;	;.line	56; "_modulonglong.c"	b >>= 1;
	LDA	__modulonglong_STK07
	SHR	
	STA	__modulonglong_STK07
	LDA	__modulonglong_STK08
	ROR	
	STA	__modulonglong_STK08
	LDA	__modulonglong_STK09
	ROR	
	STA	__modulonglong_STK09
	LDA	__modulonglong_STK10
	ROR	
	STA	__modulonglong_STK10
	LDA	__modulonglong_STK11
	ROR	
	STA	__modulonglong_STK11
	LDA	__modulonglong_STK12
	ROR	
	STA	__modulonglong_STK12
	LDA	__modulonglong_STK13
	ROR	
	STA	__modulonglong_STK13
	LDA	__modulonglong_STK14
	ROR	
	STA	__modulonglong_STK14
;	;.line	58; "_modulonglong.c"	while (count--);
	LDA	r0x116F
	STA	r0x1170
	LDA	r0x116F
	DECA	
	STA	r0x116F
	LDA	r0x1170
	JNZ	_00112_DS_
;	;.line	60; "_modulonglong.c"	return a;
	LDA	__modulonglong_STK06
	STA	STK06
	LDA	__modulonglong_STK05
	STA	STK05
	LDA	__modulonglong_STK04
	STA	STK04
	LDA	__modulonglong_STK03
	STA	STK03
	LDA	__modulonglong_STK02
	STA	STK02
	LDA	__modulonglong_STK01
	STA	STK01
	LDA	__modulonglong_STK00
	STA	STK00
	LDA	r0x1166
;	;.line	61; "_modulonglong.c"	}
	RET	
; exit point of __modulonglong
	.ENDFUNC __modulonglong
	;--cdb--S:G$_modulonglong$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_modulonglong._modulonglong$b$65536$1({8}SI:U),R,0,0,[__modulonglong_STK14,__modulonglong_STK13,__modulonglong_STK12,__modulonglong_STK11__modulonglong_STK10__modulonglong_STK09__modulonglong_STK08__modulonglong_STK07]
	;--cdb--S:L_modulonglong._modulonglong$a$65536$1({8}SI:U),R,0,0,[__modulonglong_STK06,__modulonglong_STK05,__modulonglong_STK04,__modulonglong_STK03__modulonglong_STK02__modulonglong_STK01__modulonglong_STK00r0x1166]
	;--cdb--S:L_modulonglong._modulonglong$count$65536$2({1}SC:U),R,0,0,[r0x116F]
	;--cdb--S:G$_modulonglong$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__modulonglong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__modulonglong_0	udata
r0x1166:	.ds	1
r0x116F:	.ds	1
r0x1170:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__modulonglong_STK00:	.ds	1
	.globl __modulonglong_STK00
__modulonglong_STK01:	.ds	1
	.globl __modulonglong_STK01
__modulonglong_STK02:	.ds	1
	.globl __modulonglong_STK02
__modulonglong_STK03:	.ds	1
	.globl __modulonglong_STK03
__modulonglong_STK04:	.ds	1
	.globl __modulonglong_STK04
__modulonglong_STK05:	.ds	1
	.globl __modulonglong_STK05
__modulonglong_STK06:	.ds	1
	.globl __modulonglong_STK06
__modulonglong_STK07:	.ds	1
	.globl __modulonglong_STK07
__modulonglong_STK08:	.ds	1
	.globl __modulonglong_STK08
__modulonglong_STK09:	.ds	1
	.globl __modulonglong_STK09
__modulonglong_STK10:	.ds	1
	.globl __modulonglong_STK10
__modulonglong_STK11:	.ds	1
	.globl __modulonglong_STK11
__modulonglong_STK12:	.ds	1
	.globl __modulonglong_STK12
__modulonglong_STK13:	.ds	1
	.globl __modulonglong_STK13
__modulonglong_STK14:	.ds	1
	.globl __modulonglong_STK14
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1171:NULL+0:-1:1
	end
