;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_modsint.c"
	.module _modsint
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$_modsint$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$_modsint$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$_moduint$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _modsint-code 
.globl __modsint

;--------------------------------------------------------
	.FUNC __modsint:$PNUM 4:$C:__moduint\
:$L:r0x1160:$L:__modsint_STK00:$L:__modsint_STK01:$L:__modsint_STK02:$L:r0x1163\

;--------------------------------------------------------
;	.line	203; "_modsint.c"	int _modsint (int a, int b)
__modsint:	;Function start
	STA	r0x1160
;	;.line	205; "_modsint.c"	unsigned char sign=0;
	CLRA	
	STA	r0x1163
;	;.line	207; "_modsint.c"	if(a<0)
	LDA	r0x1160
	JPL	_00106_DS_
;	;.line	209; "_modsint.c"	a=-a;
	SETB	_C
	CLRA	
	SUBB	__modsint_STK00
	STA	__modsint_STK00
	CLRA	
	SUBB	r0x1160
	STA	r0x1160
;	;.line	210; "_modsint.c"	sign=0xff;
	LDA	#0xff
	STA	r0x1163
_00106_DS_:
;	;.line	212; "_modsint.c"	if(b<0)
	LDA	__modsint_STK01
	JPL	_00108_DS_
;	;.line	213; "_modsint.c"	b=-b;
	SETB	_C
	CLRA	
	SUBB	__modsint_STK02
	STA	__modsint_STK02
	CLRA	
	SUBB	__modsint_STK01
	STA	__modsint_STK01
_00108_DS_:
;	;.line	215; "_modsint.c"	r= (unsigned)a % (unsigned)b;
	LDA	__modsint_STK02
	STA	__moduint_STK02
	LDA	__modsint_STK01
	STA	__moduint_STK01
	LDA	__modsint_STK00
	STA	__moduint_STK00
	LDA	r0x1160
	CALL	__moduint
	STA	r0x1160
;	;.line	216; "_modsint.c"	if (sign)
	LDA	r0x1163
	JZ	_00110_DS_
;	;.line	217; "_modsint.c"	return -r;
	SETB	_C
	CLRA	
	SUBB	STK00
	STA	r0x1163
	CLRA	
	SUBB	r0x1160
	STA	__modsint_STK02
	LDA	r0x1163
	STA	STK00
	LDA	__modsint_STK02
	JMP	_00111_DS_
_00110_DS_:
;	;.line	218; "_modsint.c"	return r;
	LDA	r0x1160
_00111_DS_:
;	;.line	219; "_modsint.c"	}
	RET	
; exit point of __modsint
	.ENDFUNC __modsint
	;--cdb--S:G$_modsint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_moduint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_modsint._modsint$b$65536$1({2}SI:S),R,0,0,[__modsint_STK02,__modsint_STK01]
	;--cdb--S:L_modsint._modsint$a$65536$1({2}SI:S),R,0,0,[__modsint_STK00,r0x1160]
	;--cdb--S:L_modsint._modsint$sign$65536$2({1}SC:U),R,0,0,[r0x1163]
	;--cdb--S:L_modsint._modsint$r$65536$2({2}SI:S),R,0,0,[__modsint_STK00,r0x1160]
	;--cdb--S:G$_modsint$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__moduint

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__modsint
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__modsint_0	udata
r0x1160:	.ds	1
r0x1163:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__modsint_STK00:	.ds	1
	.globl __modsint_STK00
__modsint_STK01:	.ds	1
	.globl __modsint_STK01
__modsint_STK02:	.ds	1
	.globl __modsint_STK02
	.globl __moduint_STK02
	.globl __moduint_STK01
	.globl __moduint_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1160:NULL+0:4455:0
	;--cdb--W:__modsint_STK00:NULL+0:4456:0
	;--cdb--W:__modsint_STK00:NULL+0:14:0
	;--cdb--W:r0x1164:NULL+0:4454:0
	;--cdb--W:r0x1165:NULL+0:4448:0
	end
