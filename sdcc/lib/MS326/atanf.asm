;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"atanf.c"
	.module atanf
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Fatanf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$atanf$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$_mulint$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; atanf-code 
.globl _atanf

;--------------------------------------------------------
	.FUNC _atanf:$PNUM 4:$C:_fabsf:$C:___fslt:$C:___fsdiv:$C:___fsmul\
:$C:___fssub:$C:___fsadd\
:$L:r0x1162:$L:_atanf_STK00:$L:_atanf_STK01:$L:_atanf_STK02:$L:r0x1163\
:$L:r0x1164:$L:r0x1168:$L:r0x1167:$L:r0x1166:$L:r0x1165\
:$L:r0x1169:$L:r0x116C:$L:r0x116B:$L:r0x116A:$L:r0x1170\
:$L:r0x116F:$L:r0x116E:$L:r0x116D:$L:r0x1174:$L:r0x1173\
:$L:r0x1172:$L:r0x1171
;--------------------------------------------------------
;	.line	55; "atanf.c"	float atanf(float x) _FLOAT_FUNC_REENTRANT
_atanf:	;Function start
	STA	r0x1162
;	;.line	58; "atanf.c"	int n=0;
	CLRA	
	STA	r0x1163
	STA	r0x1164
;	;.line	61; "atanf.c"	f=fabsf(x);
	LDA	_atanf_STK02
	STA	_fabsf_STK02
	LDA	_atanf_STK01
	STA	_fabsf_STK01
	LDA	_atanf_STK00
	STA	_fabsf_STK00
	LDA	r0x1162
	CALL	_fabsf
	STA	r0x1168
	LDA	STK00
	STA	r0x1167
	LDA	STK01
	STA	r0x1166
	LDA	STK02
	STA	r0x1165
;	;.line	62; "atanf.c"	if(f>1.0)
	STA	___fslt_STK06
	LDA	r0x1166
	STA	___fslt_STK05
	LDA	r0x1167
	STA	___fslt_STK04
	LDA	r0x1168
	STA	___fslt_STK03
	CLRA	
	STA	___fslt_STK02
	STA	___fslt_STK01
	LDA	#0x80
	STA	___fslt_STK00
	LDA	#0x3f
	CALL	___fslt
	JZ	_00106_DS_
;	;.line	64; "atanf.c"	f=1.0/f;
	LDA	r0x1165
	STA	___fsdiv_STK06
	LDA	r0x1166
	STA	___fsdiv_STK05
	LDA	r0x1167
	STA	___fsdiv_STK04
	LDA	r0x1168
	STA	___fsdiv_STK03
	CLRA	
	STA	___fsdiv_STK02
	STA	___fsdiv_STK01
	LDA	#0x80
	STA	___fsdiv_STK00
	LDA	#0x3f
	CALL	___fsdiv
	STA	r0x116C
	LDA	STK02
	STA	r0x1165
	LDA	STK01
	STA	r0x1166
	LDA	STK00
	STA	r0x1167
	LDA	r0x116C
	STA	r0x1168
;	;.line	65; "atanf.c"	n=2;
	LDA	#0x02
	STA	r0x1163
	CLRA	
	STA	r0x1164
_00106_DS_:
;	;.line	67; "atanf.c"	if(f>K1)
	LDA	r0x1165
	STA	___fslt_STK06
	LDA	r0x1166
	STA	___fslt_STK05
	LDA	r0x1167
	STA	___fslt_STK04
	LDA	r0x1168
	STA	___fslt_STK03
	LDA	#0xa3
	STA	___fslt_STK02
	LDA	#0x30
	STA	___fslt_STK01
	LDA	#0x89
	STA	___fslt_STK00
	LDA	#0x3e
	CALL	___fslt
	JZ	_00108_DS_
;	;.line	69; "atanf.c"	f=((K2*f-1.0)+f)/(K3+f);
	LDA	r0x1165
	STA	___fsmul_STK06
	LDA	r0x1166
	STA	___fsmul_STK05
	LDA	r0x1167
	STA	___fsmul_STK04
	LDA	r0x1168
	STA	___fsmul_STK03
	LDA	#0xaf
	STA	___fsmul_STK02
	LDA	#0x67
	STA	___fsmul_STK01
	LDA	#0x3b
	STA	___fsmul_STK00
	LDA	#0x3f
	CALL	___fsmul
	STA	r0x116C
	CLRA	
	STA	___fssub_STK06
	STA	___fssub_STK05
	LDA	#0x80
	STA	___fssub_STK04
	LDA	#0x3f
	STA	___fssub_STK03
	LDA	STK02
	STA	___fssub_STK02
	LDA	STK01
	STA	___fssub_STK01
	LDA	STK00
	STA	___fssub_STK00
	LDA	r0x116C
	CALL	___fssub
	STA	r0x116C
	LDA	r0x1165
	STA	___fsadd_STK06
	LDA	r0x1166
	STA	___fsadd_STK05
	LDA	r0x1167
	STA	___fsadd_STK04
	LDA	r0x1168
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x116C
	CALL	___fsadd
	STA	r0x116C
	LDA	STK00
	STA	r0x116B
	LDA	STK01
	STA	r0x116A
	LDA	STK02
	STA	r0x1169
	LDA	#0xd7
	STA	___fsadd_STK06
	LDA	#0xb3
	STA	___fsadd_STK05
	LDA	#0xdd
	STA	___fsadd_STK04
	LDA	#0x3f
	STA	___fsadd_STK03
	LDA	r0x1165
	STA	___fsadd_STK02
	LDA	r0x1166
	STA	___fsadd_STK01
	LDA	r0x1167
	STA	___fsadd_STK00
	LDA	r0x1168
	CALL	___fsadd
	STA	r0x1170
	LDA	STK02
	STA	___fsdiv_STK06
	LDA	STK01
	STA	___fsdiv_STK05
	LDA	STK00
	STA	___fsdiv_STK04
	LDA	r0x1170
	STA	___fsdiv_STK03
	LDA	r0x1169
	STA	___fsdiv_STK02
	LDA	r0x116A
	STA	___fsdiv_STK01
	LDA	r0x116B
	STA	___fsdiv_STK00
	LDA	r0x116C
	CALL	___fsdiv
	STA	r0x116C
	LDA	STK02
	STA	r0x1165
	LDA	STK01
	STA	r0x1166
	LDA	STK00
	STA	r0x1167
	LDA	r0x116C
	STA	r0x1168
;	;.line	73; "atanf.c"	n++;
	LDA	r0x1163
	INCA	
	STA	r0x1163
	CLRA	
	ADDC	r0x1164
	STA	r0x1164
_00108_DS_:
;	;.line	75; "atanf.c"	if(fabsf(f)<EPS) r=f;
	LDA	r0x1165
	STA	_fabsf_STK02
	LDA	r0x1166
	STA	_fabsf_STK01
	LDA	r0x1167
	STA	_fabsf_STK00
	LDA	r0x1168
	CALL	_fabsf
	STA	r0x116C
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	LDA	#0x80
	STA	___fslt_STK04
	LDA	#0x39
	STA	___fslt_STK03
	LDA	STK02
	STA	___fslt_STK02
	LDA	STK01
	STA	___fslt_STK01
	LDA	STK00
	STA	___fslt_STK00
	LDA	r0x116C
	CALL	___fslt
	JZ	_00110_DS_
	LDA	r0x1165
	STA	r0x1169
	LDA	r0x1166
	STA	r0x116A
	LDA	r0x1167
	STA	r0x116B
	LDA	r0x1168
	STA	r0x116C
	JMP	_00111_DS_
_00110_DS_:
;	;.line	78; "atanf.c"	g=f*f;
	LDA	r0x1165
	STA	___fsmul_STK06
	LDA	r0x1166
	STA	___fsmul_STK05
	LDA	r0x1167
	STA	___fsmul_STK04
	LDA	r0x1168
	STA	___fsmul_STK03
	LDA	r0x1165
	STA	___fsmul_STK02
	LDA	r0x1166
	STA	___fsmul_STK01
	LDA	r0x1167
	STA	___fsmul_STK00
	LDA	r0x1168
	CALL	___fsmul
	STA	r0x1170
	LDA	STK00
	STA	r0x116F
	LDA	STK01
	STA	r0x116E
	LDA	STK02
	STA	r0x116D
;	;.line	79; "atanf.c"	r=f+P(g,f)/Q(g);
	STA	___fsmul_STK06
	LDA	r0x116E
	STA	___fsmul_STK05
	LDA	r0x116F
	STA	___fsmul_STK04
	LDA	r0x1170
	STA	___fsmul_STK03
	LDA	#0x91
	STA	___fsmul_STK02
	LDA	#0x86
	STA	___fsmul_STK01
	LDA	#0x50
	STA	___fsmul_STK00
	LDA	#0xbd
	CALL	___fsmul
	STA	r0x1174
	LDA	#0xf6
	STA	___fsadd_STK06
	LDA	#0x10
	STA	___fsadd_STK05
	LDA	#0xf1
	STA	___fsadd_STK04
	LDA	#0xbe
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1174
	CALL	___fsadd
	STA	r0x1174
	LDA	r0x116D
	STA	___fsmul_STK06
	LDA	r0x116E
	STA	___fsmul_STK05
	LDA	r0x116F
	STA	___fsmul_STK04
	LDA	r0x1170
	STA	___fsmul_STK03
	LDA	STK02
	STA	___fsmul_STK02
	LDA	STK01
	STA	___fsmul_STK01
	LDA	STK00
	STA	___fsmul_STK00
	LDA	r0x1174
	CALL	___fsmul
	STA	r0x1174
	LDA	r0x1165
	STA	___fsmul_STK06
	LDA	r0x1166
	STA	___fsmul_STK05
	LDA	r0x1167
	STA	___fsmul_STK04
	LDA	r0x1168
	STA	___fsmul_STK03
	LDA	STK02
	STA	___fsmul_STK02
	LDA	STK01
	STA	___fsmul_STK01
	LDA	STK00
	STA	___fsmul_STK00
	LDA	r0x1174
	CALL	___fsmul
	STA	r0x1174
	LDA	STK00
	STA	r0x1173
	LDA	STK01
	STA	r0x1172
	LDA	STK02
	STA	r0x1171
	LDA	#0xd3
	STA	___fsadd_STK06
	LDA	#0xcc
	STA	___fsadd_STK05
	LDA	#0xb4
	STA	___fsadd_STK04
	LDA	#0x3f
	STA	___fsadd_STK03
	LDA	r0x116D
	STA	___fsadd_STK02
	LDA	r0x116E
	STA	___fsadd_STK01
	LDA	r0x116F
	STA	___fsadd_STK00
	LDA	r0x1170
	CALL	___fsadd
	STA	r0x1170
	LDA	STK02
	STA	___fsdiv_STK06
	LDA	STK01
	STA	___fsdiv_STK05
	LDA	STK00
	STA	___fsdiv_STK04
	LDA	r0x1170
	STA	___fsdiv_STK03
	LDA	r0x1171
	STA	___fsdiv_STK02
	LDA	r0x1172
	STA	___fsdiv_STK01
	LDA	r0x1173
	STA	___fsdiv_STK00
	LDA	r0x1174
	CALL	___fsdiv
	STA	r0x1170
	LDA	STK02
	STA	___fsadd_STK06
	LDA	STK01
	STA	___fsadd_STK05
	LDA	STK00
	STA	___fsadd_STK04
	LDA	r0x1170
	STA	___fsadd_STK03
	LDA	r0x1165
	STA	___fsadd_STK02
	LDA	r0x1166
	STA	___fsadd_STK01
	LDA	r0x1167
	STA	___fsadd_STK00
	LDA	r0x1168
	CALL	___fsadd
	STA	r0x116C
	LDA	STK00
	STA	r0x116B
	LDA	STK01
	STA	r0x116A
	LDA	STK02
	STA	r0x1169
_00111_DS_:
;	;.line	81; "atanf.c"	if(n>1) r=-r;
	SETB	_C
	LDA	#0x01
	SUBB	r0x1163
	CLRA	
	SUBSI	
	SUBB	r0x1164
	JC	_00113_DS_
	LDA	r0x116C
	XOR	#0x80
	STA	r0x116C
_00113_DS_:
;	;.line	82; "atanf.c"	r+=a[n];
	LDA	r0x1163
	SHL	
	STA	r0x1163
	LDA	r0x1164
	ROL	
	STA	r0x1164
	LDA	r0x1163
	SHL	
	STA	r0x1163
	LDA	r0x1164
	ROL	
	LDA	r0x1163
	STA	_PTRCL
	LDAT	#(_atanf_a_65536_26 + 0)
	STA	r0x1165
	LDA	_PTRCL
	LDAT	#(_atanf_a_65536_26 + 1)
	STA	r0x1166
	LDA	_PTRCL
	LDAT	#(_atanf_a_65536_26 + 2)
	STA	r0x1167
	LDA	_PTRCL
	LDAT	#(_atanf_a_65536_26 + 3)
	STA	r0x1168
	LDA	r0x1165
	STA	___fsadd_STK06
	LDA	r0x1166
	STA	___fsadd_STK05
	LDA	r0x1167
	STA	___fsadd_STK04
	LDA	r0x1168
	STA	___fsadd_STK03
	LDA	r0x1169
	STA	___fsadd_STK02
	LDA	r0x116A
	STA	___fsadd_STK01
	LDA	r0x116B
	STA	___fsadd_STK00
	LDA	r0x116C
	CALL	___fsadd
	STA	r0x1166
	LDA	STK00
	STA	r0x1165
	LDA	STK01
	STA	r0x1164
	LDA	STK02
	STA	r0x1163
;	;.line	83; "atanf.c"	if(x<0.0) r=-r;
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	STA	___fslt_STK04
	STA	___fslt_STK03
	LDA	_atanf_STK02
	STA	___fslt_STK02
	LDA	_atanf_STK01
	STA	___fslt_STK01
	LDA	_atanf_STK00
	STA	___fslt_STK00
	LDA	r0x1162
	CALL	___fslt
	JZ	_00115_DS_
	LDA	r0x1166
	XOR	#0x80
	STA	r0x1166
_00115_DS_:
;	;.line	84; "atanf.c"	return r;
	LDA	r0x1163
	STA	STK02
	LDA	r0x1164
	STA	STK01
	LDA	r0x1165
	STA	STK00
	LDA	r0x1166
;	;.line	85; "atanf.c"	}
	RET	
; exit point of _atanf
	.ENDFUNC _atanf
	;--cdb--S:Latanf.atanf$x$65536$25({4}SF:S),R,0,0,[_atanf_STK02,_atanf_STK01,_atanf_STK00,r0x1162]
	;--cdb--S:Latanf.atanf$f$65536$26({4}SF:S),R,0,0,[r0x1165,r0x1166,r0x1167,r0x1168]
	;--cdb--S:Latanf.atanf$r$65536$26({4}SF:S),R,0,0,[r0x1163,r0x1164,r0x1165,r0x1166]
	;--cdb--S:Latanf.atanf$g$65536$26({4}SF:S),R,0,0,[r0x116D,r0x116E,r0x116F,r0x1170]
	;--cdb--S:Latanf.atanf$n$65536$26({2}SI:S),R,0,0,[r0x1163,r0x1164]
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$_mulint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$errno$0$0({2}SI:S),E,0,0
	;--cdb--S:Latanf.atanf$a$65536$26({16}DA4d,SF:S),D,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_fabsf
	.globl	___fslt
	.globl	___fsdiv
	.globl	___fsmul
	.globl	___fssub
	.globl	___fsadd
	.globl	__mulint
	.globl	_errno

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_atanf
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_atanf_0	udata
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116C:	.ds	1
r0x116D:	.ds	1
r0x116E:	.ds	1
r0x116F:	.ds	1
r0x1170:	.ds	1
r0x1171:	.ds	1
r0x1172:	.ds	1
r0x1173:	.ds	1
r0x1174:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_atanf_STK00:	.ds	1
	.globl _atanf_STK00
_atanf_STK01:	.ds	1
	.globl _atanf_STK01
_atanf_STK02:	.ds	1
	.globl _atanf_STK02
	.globl _fabsf_STK02
	.globl _fabsf_STK01
	.globl _fabsf_STK00
	.globl ___fslt_STK06
	.globl ___fslt_STK05
	.globl ___fslt_STK04
	.globl ___fslt_STK03
	.globl ___fslt_STK02
	.globl ___fslt_STK01
	.globl ___fslt_STK00
	.globl ___fsdiv_STK06
	.globl ___fsdiv_STK05
	.globl ___fsdiv_STK04
	.globl ___fsdiv_STK03
	.globl ___fsdiv_STK02
	.globl ___fsdiv_STK01
	.globl ___fsdiv_STK00
	.globl ___fsmul_STK06
	.globl ___fsmul_STK05
	.globl ___fsmul_STK04
	.globl ___fsmul_STK03
	.globl ___fsmul_STK02
	.globl ___fsmul_STK01
	.globl ___fsmul_STK00
	.globl ___fssub_STK06
	.globl ___fssub_STK05
	.globl ___fssub_STK04
	.globl ___fssub_STK03
	.globl ___fssub_STK02
	.globl ___fssub_STK01
	.globl ___fssub_STK00
	.globl ___fsadd_STK06
	.globl ___fsadd_STK05
	.globl ___fsadd_STK04
	.globl ___fsadd_STK03
	.globl ___fsadd_STK02
	.globl ___fsadd_STK01
	.globl ___fsadd_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------

	.area	SEGC	(CODE) ;atanf-0-code

_atanf_a_65536_26:
	.byte #0x00,#0x00,#0x00,#0x00	;  0.000000e+00
	.byte #0x92,#0x0a,#0x06,#0x3f	;  5.235988e-01
	.byte #0xdb,#0x0f,#0xc9,#0x3f	;  1.570796e+00
	.byte #0x92,#0x0a,#0x86,#0x3f	;  1.047198e+00

;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------

	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1164:NULL+0:-1:1
	;--cdb--W:r0x1169:NULL+0:12:0
	;--cdb--W:r0x116B:NULL+0:14:0
	;--cdb--W:r0x116A:NULL+0:13:0
	;--cdb--W:r0x116F:NULL+0:14:0
	;--cdb--W:r0x116E:NULL+0:13:0
	;--cdb--W:r0x116D:NULL+0:12:0
	;--cdb--W:r0x1173:NULL+0:14:0
	;--cdb--W:r0x1172:NULL+0:13:0
	;--cdb--W:r0x1171:NULL+0:12:0
	;--cdb--W:r0x1169:NULL+0:-1:1
	;--cdb--W:_atanf_STK02:NULL+0:-1:1
	end
