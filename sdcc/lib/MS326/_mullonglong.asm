;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_mullonglong.c"
	.module _mullonglong
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$_mullonglong$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$_mullonglong$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _mullonglong-code 
.globl __mullonglong

;--------------------------------------------------------
	.FUNC __mullonglong:$PNUM 16:$C:__shr_ulonglong:$C:__mullong:$C:__shl_longlong\
:$L:r0x1166:$L:__mullonglong_STK00:$L:__mullonglong_STK01:$L:__mullonglong_STK02:$L:__mullonglong_STK03\
:$L:__mullonglong_STK04:$L:__mullonglong_STK05:$L:__mullonglong_STK06:$L:__mullonglong_STK07:$L:__mullonglong_STK08\
:$L:__mullonglong_STK09:$L:__mullonglong_STK10:$L:__mullonglong_STK11:$L:__mullonglong_STK12:$L:__mullonglong_STK13\
:$L:__mullonglong_STK14:$L:r0x116F:$L:r0x1170:$L:r0x1171:$L:r0x1172\
:$L:r0x1173:$L:r0x1174:$L:r0x1175:$L:r0x1176:$L:r0x1177\
:$L:r0x1178:$L:r0x1179:$L:r0x1180:$L:r0x1181:$L:r0x1184\
:$L:r0x1182:$L:r0x1189:$L:r0x118A:$L:r0x1198:$L:r0x1197\

;--------------------------------------------------------
;	.line	36; "_mullonglong.c"	long long _mullonglong(long long ll,long long lr)
__mullonglong:	;Function start
	STA	r0x1166
;	;.line	38; "_mullonglong.c"	unsigned long long ret = 0ull;
	CLRA	
	STA	r0x116F
	STA	r0x1170
	STA	r0x1171
	STA	r0x1172
	STA	r0x1173
	STA	r0x1174
	STA	r0x1175
	STA	r0x1176
;	;.line	41; "_mullonglong.c"	for (i = 0; i < sizeof (long long); i+=2)
	STA	r0x1177
_00110_DS_:
;	;.line	43; "_mullonglong.c"	unsigned long l = (((unsigned long long)ll) >> ((unsigned char)(i <<3 )))&0xffffL;
	LDA	r0x1177
	SHL	
	SHL	
	SHL	
	STA	r0x1181
	LDA	r0x1166
	STA	STK00
	LDA	__mullonglong_STK00
	STA	STK01
	LDA	__mullonglong_STK01
	STA	STK02
	LDA	__mullonglong_STK02
	STA	STK03
	LDA	__mullonglong_STK03
	STA	STK04
	LDA	__mullonglong_STK04
	STA	STK05
	LDA	__mullonglong_STK05
	STA	STK06
	LDA	__mullonglong_STK06
	STA	_PTRCL
	LDA	r0x1181
	CALL	__shr_ulonglong
	LDA	_PTRCL
	STA	r0x1178
	LDA	STK06
	STA	r0x1179
;	;.line	44; "_mullonglong.c"	for(j = 0; (i + j) < sizeof (long long); j+=2)
	CLRA	
	STA	r0x1180
_00108_DS_:
	LDA	r0x1177
	ADD	r0x1180
	STA	r0x1181
	CLRA	
	ROL	
	STA	r0x1182
	LDA	r0x1181
	ADD	#0xf8
	LDA	r0x1182
	XOR	#0x80
	ADDC	#0x7f
	JC	_00111_DS_
;	;.line	46; "_mullonglong.c"	unsigned long r = (((unsigned long long)lr) >> ((unsigned char)(j <<3)))&0xffffL;
	LDA	r0x1180
	STA	r0x1189
	SHL	
	SHL	
	SHL	
	STA	r0x118A
	LDA	__mullonglong_STK07
	STA	STK00
	LDA	__mullonglong_STK08
	STA	STK01
	LDA	__mullonglong_STK09
	STA	STK02
	LDA	__mullonglong_STK10
	STA	STK03
	LDA	__mullonglong_STK11
	STA	STK04
	LDA	__mullonglong_STK12
	STA	STK05
	LDA	__mullonglong_STK13
	STA	STK06
	LDA	__mullonglong_STK14
	STA	_PTRCL
	LDA	r0x118A
	CALL	__shr_ulonglong
	LDA	_PTRCL
;	;.line	47; "_mullonglong.c"	ret += ((unsigned long long)(l * r)) << ((unsigned char)((i + j) << 3 ));
	STA	__mullong_STK06
	LDA	STK06
	STA	__mullong_STK05
	CLRA	
	STA	__mullong_STK04
	STA	__mullong_STK03
	LDA	r0x1178
	STA	__mullong_STK02
	LDA	r0x1179
	STA	__mullong_STK01
	CLRA	
	STA	__mullong_STK00
	CALL	__mullong
	STA	r0x1184
	LDA	r0x1177
	ADD	r0x1180
	SHL	
	SHL	
	SHL	
	STA	r0x1181
	LDA	STK00
	STA	r0x1198
	LDA	STK01
	STA	r0x1197
	LDA	STK02
	STA	r0x118A
	CLRA	
	STA	STK00
	STA	STK01
	STA	STK02
	STA	STK03
	LDA	r0x1184
	STA	STK04
	LDA	r0x1198
	STA	STK05
	LDA	r0x1197
	STA	STK06
	LDA	r0x118A
	STA	_PTRCL
	LDA	r0x1181
	CALL	__shl_longlong
	LDA	_PTRCL
	ADD	r0x116F
	STA	r0x116F
	LDA	r0x1170
	ADDC	STK06
	STA	r0x1170
	LDA	r0x1171
	ADDC	STK05
	STA	r0x1171
	LDA	r0x1172
	ADDC	STK04
	STA	r0x1172
	LDA	r0x1173
	ADDC	STK03
	STA	r0x1173
	LDA	r0x1174
	ADDC	STK02
	STA	r0x1174
	LDA	r0x1175
	ADDC	STK01
	STA	r0x1175
	LDA	r0x1176
	ADDC	STK00
	STA	r0x1176
;	;.line	44; "_mullonglong.c"	for(j = 0; (i + j) < sizeof (long long); j+=2)
	LDA	#0x02
	ADD	r0x1189
	STA	r0x1180
	JMP	_00108_DS_
_00111_DS_:
;	;.line	41; "_mullonglong.c"	for (i = 0; i < sizeof (long long); i+=2)
	LDA	r0x1177
	ADD	#0x02
	STA	r0x1177
	ADD	#0xf8
	JNC	_00110_DS_
;	;.line	51; "_mullonglong.c"	return(ret);
	LDA	r0x116F
	STA	STK06
	LDA	r0x1170
	STA	STK05
	LDA	r0x1171
	STA	STK04
	LDA	r0x1172
	STA	STK03
	LDA	r0x1173
	STA	STK02
	LDA	r0x1174
	STA	STK01
	LDA	r0x1175
	STA	STK00
	LDA	r0x1176
;	;.line	52; "_mullonglong.c"	}
	RET	
; exit point of __mullonglong
	.ENDFUNC __mullonglong
	;--cdb--S:G$_mullonglong$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:L_mullonglong._mullonglong$lr$65536$1({8}SI:S),R,0,0,[__mullonglong_STK14,__mullonglong_STK13,__mullonglong_STK12,__mullonglong_STK11__mullonglong_STK10__mullonglong_STK09__mullonglong_STK08__mullonglong_STK07]
	;--cdb--S:L_mullonglong._mullonglong$ll$65536$1({8}SI:S),R,0,0,[__mullonglong_STK06,__mullonglong_STK05,__mullonglong_STK04,__mullonglong_STK03__mullonglong_STK02__mullonglong_STK01__mullonglong_STK00r0x1166]
	;--cdb--S:L_mullonglong._mullonglong$ret$65536$2({8}SI:U),R,0,0,[r0x116F,r0x1170,r0x1171,r0x1172r0x1173r0x1174r0x1175r0x1176]
	;--cdb--S:L_mullonglong._mullonglong$i$65536$2({1}SC:U),R,0,0,[r0x1177]
	;--cdb--S:L_mullonglong._mullonglong$j$65536$2({1}SC:U),R,0,0,[r0x1180]
	;--cdb--S:L_mullonglong._mullonglong$l$196608$4({4}SL:U),R,0,0,[r0x1178,r0x1179,r0x117A,r0x117B]
	;--cdb--S:L_mullonglong._mullonglong$r$327680$6({4}SL:U),R,0,0,[r0x118B,r0x118C,r0x118D,r0x118Er0x118Fr0x1190r0x1191r0x1192]
	;--cdb--S:G$_mullonglong$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__mullong
	.globl	__shr_ulonglong
	.globl	__shl_longlong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__mullonglong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__mullonglong_0	udata
r0x1166:	.ds	1
r0x116F:	.ds	1
r0x1170:	.ds	1
r0x1171:	.ds	1
r0x1172:	.ds	1
r0x1173:	.ds	1
r0x1174:	.ds	1
r0x1175:	.ds	1
r0x1176:	.ds	1
r0x1177:	.ds	1
r0x1178:	.ds	1
r0x1179:	.ds	1
r0x1180:	.ds	1
r0x1181:	.ds	1
r0x1182:	.ds	1
r0x1184:	.ds	1
r0x1189:	.ds	1
r0x118A:	.ds	1
r0x1197:	.ds	1
r0x1198:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__mullonglong_STK00:	.ds	1
	.globl __mullonglong_STK00
__mullonglong_STK01:	.ds	1
	.globl __mullonglong_STK01
__mullonglong_STK02:	.ds	1
	.globl __mullonglong_STK02
__mullonglong_STK03:	.ds	1
	.globl __mullonglong_STK03
__mullonglong_STK04:	.ds	1
	.globl __mullonglong_STK04
__mullonglong_STK05:	.ds	1
	.globl __mullonglong_STK05
__mullonglong_STK06:	.ds	1
	.globl __mullonglong_STK06
__mullonglong_STK07:	.ds	1
	.globl __mullonglong_STK07
__mullonglong_STK08:	.ds	1
	.globl __mullonglong_STK08
__mullonglong_STK09:	.ds	1
	.globl __mullonglong_STK09
__mullonglong_STK10:	.ds	1
	.globl __mullonglong_STK10
__mullonglong_STK11:	.ds	1
	.globl __mullonglong_STK11
__mullonglong_STK12:	.ds	1
	.globl __mullonglong_STK12
__mullonglong_STK13:	.ds	1
	.globl __mullonglong_STK13
__mullonglong_STK14:	.ds	1
	.globl __mullonglong_STK14
	.globl __mullong_STK06
	.globl __mullong_STK05
	.globl __mullong_STK04
	.globl __mullong_STK03
	.globl __mullong_STK02
	.globl __mullong_STK01
	.globl __mullong_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1188:NULL+0:-1:1
	;--cdb--W:r0x1187:NULL+0:-1:1
	;--cdb--W:r0x1186:NULL+0:-1:1
	;--cdb--W:r0x1185:NULL+0:-1:1
	;--cdb--W:r0x117A:NULL+0:-1:1
	;--cdb--W:r0x117B:NULL+0:-1:1
	;--cdb--W:r0x1192:NULL+0:-1:1
	;--cdb--W:r0x1191:NULL+0:-1:1
	;--cdb--W:r0x1190:NULL+0:-1:1
	;--cdb--W:r0x118F:NULL+0:-1:1
	;--cdb--W:r0x1183:NULL+0:-1:1
	;--cdb--W:r0x1184:NULL+0:-1:1
	;--cdb--W:r0x1181:NULL+0:-1:1
	;--cdb--W:r0x1178:NULL+0:-1:1
	;--cdb--W:r0x1178:NULL+0:4516:0
	;--cdb--W:r0x1179:NULL+0:4515:0
	;--cdb--W:r0x117A:NULL+0:4514:0
	;--cdb--W:r0x117B:NULL+0:4513:0
	;--cdb--W:r0x117C:NULL+0:4512:0
	;--cdb--W:r0x117D:NULL+0:4511:0
	;--cdb--W:r0x117E:NULL+0:4510:0
	;--cdb--W:r0x117F:NULL+0:4454:0
	;--cdb--W:r0x1180:NULL+0:4471:0
	;--cdb--W:r0x1180:NULL+0:4516:0
	;--cdb--W:r0x1181:NULL+0:4471:0
	;--cdb--W:r0x1181:NULL+0:4524:0
	;--cdb--W:r0x1181:NULL+0:4491:0
	;--cdb--W:r0x1181:NULL+0:12:0
	;--cdb--W:r0x1181:NULL+0:4490:0
	;--cdb--W:r0x1188:NULL+0:4454:0
	;--cdb--W:r0x1188:NULL+0:4517:0
	;--cdb--W:r0x1187:NULL+0:4510:0
	;--cdb--W:r0x1187:NULL+0:4518:0
	;--cdb--W:r0x1186:NULL+0:4511:0
	;--cdb--W:r0x1186:NULL+0:4519:0
	;--cdb--W:r0x1185:NULL+0:4512:0
	;--cdb--W:r0x1185:NULL+0:4520:0
	;--cdb--W:r0x1184:NULL+0:4513:0
	;--cdb--W:r0x1184:NULL+0:10:0
	;--cdb--W:r0x1184:NULL+0:0:0
	;--cdb--W:r0x1184:NULL+0:4521:0
	;--cdb--W:r0x1183:NULL+0:4514:0
	;--cdb--W:r0x1183:NULL+0:9:0
	;--cdb--W:r0x1183:NULL+0:4480:0
	;--cdb--W:r0x1183:NULL+0:4522:0
	;--cdb--W:r0x1183:NULL+0:14:0
	;--cdb--W:r0x1182:NULL+0:4515:0
	;--cdb--W:r0x1182:NULL+0:8:0
	;--cdb--W:r0x1182:NULL+0:4523:0
	;--cdb--W:r0x1182:NULL+0:4492:0
	;--cdb--W:r0x1182:NULL+0:13:0
	;--cdb--W:r0x118A:NULL+0:12:0
	;--cdb--W:r0x1192:NULL+0:4517:0
	;--cdb--W:r0x1191:NULL+0:4518:0
	;--cdb--W:r0x1190:NULL+0:4519:0
	;--cdb--W:r0x118F:NULL+0:4520:0
	;--cdb--W:r0x118E:NULL+0:4521:0
	;--cdb--W:r0x118E:NULL+0:10:0
	;--cdb--W:r0x118E:NULL+0:0:0
	;--cdb--W:r0x118D:NULL+0:4522:0
	;--cdb--W:r0x118D:NULL+0:9:0
	;--cdb--W:r0x118D:NULL+0:4484:0
	;--cdb--W:r0x118C:NULL+0:4523:0
	;--cdb--W:r0x118C:NULL+0:8:0
	;--cdb--W:r0x118C:NULL+0:14:0
	;--cdb--W:r0x118B:NULL+0:4524:0
	;--cdb--W:r0x118B:NULL+0:13:0
	;--cdb--W:r0x1193:NULL+0:0:0
	;--cdb--W:r0x1194:NULL+0:0:0
	;--cdb--W:r0x1195:NULL+0:0:0
	;--cdb--W:r0x1196:NULL+0:4481:0
	;--cdb--W:r0x119D:NULL+0:14:0
	;--cdb--W:r0x119C:NULL+0:13:0
	;--cdb--W:r0x119B:NULL+0:12:0
	;--cdb--W:r0x119A:NULL+0:0:0
	;--cdb--W:r0x119A:NULL+0:11:0
	;--cdb--W:r0x1199:NULL+0:4484:0
	;--cdb--W:r0x1199:NULL+0:10:0
	;--cdb--W:r0x1198:NULL+0:9:0
	;--cdb--W:r0x1197:NULL+0:8:0
	;--cdb--W:r0x1180:NULL+0:-1:1
	;--cdb--W:r0x118E:NULL+0:-1:1
	;--cdb--W:r0x1193:NULL+0:-1:1
	;--cdb--W:r0x1194:NULL+0:-1:1
	;--cdb--W:r0x1195:NULL+0:-1:1
	;--cdb--W:r0x119A:NULL+0:-1:1
	;--cdb--W:r0x1182:NULL+0:0:0
	;--cdb--W:r0x118D:NULL+0:0:0
	;--cdb--W:r0x1182:NULL+0:-1:1
	;--cdb--W:r0x118D:NULL+0:-1:1
	;--cdb--W:r0x118B:NULL+0:-1:1
	;--cdb--W:r0x119D:NULL+0:0:0
	;--cdb--W:r0x119D:NULL+0:-1:1
	;--cdb--W:r0x119C:NULL+0:0:0
	;--cdb--W:r0x119C:NULL+0:-1:1
	;--cdb--W:r0x119B:NULL+0:0:0
	;--cdb--W:r0x119B:NULL+0:-1:1
	;--cdb--W:r0x117A:NULL+0:0:0
	;--cdb--W:r0x117B:NULL+0:0:0
	end
