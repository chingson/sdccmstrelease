;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_fsgt.c"
	.module _fsgt
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:F_fsgt$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$__fsgt$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _fsgt-code 
.globl ___fsgt

;--------------------------------------------------------
	.FUNC ___fsgt:$PNUM 8:$L:r0x1162:$L:___fsgt_STK00:$L:___fsgt_STK01:$L:___fsgt_STK02:$L:___fsgt_STK03\
:$L:___fsgt_STK04:$L:___fsgt_STK05:$L:___fsgt_STK06:$L:___fsgt_fl1_65536_21:$L:___fsgt_fl2_65536_21\

;--------------------------------------------------------
;	.line	108; "_fsgt.c"	char __fsgt (float a1, float a2)
___fsgt:	;Function start
	STA	r0x1162
	LDA	___fsgt_STK06
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	112; "_fsgt.c"	fl1.f = a1;
	LDA	___fsgt_STK02
	STA	___fsgt_fl1_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	___fsgt_STK01
	STA	(___fsgt_fl1_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	___fsgt_STK00
	STA	(___fsgt_fl1_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1162
	STA	(___fsgt_fl1_65536_21 + 3)
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	113; "_fsgt.c"	fl2.f = a2;
	LDA	___fsgt_STK06
	STA	___fsgt_fl2_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	___fsgt_STK05
	STA	(___fsgt_fl2_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	___fsgt_STK04
	STA	(___fsgt_fl2_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	___fsgt_STK03
	STA	(___fsgt_fl2_65536_21 + 3)
;	;.line	115; "_fsgt.c"	if (((fl1.l | fl2.l) & 0x7FFFFFFF) == 0)
	LDA	___fsgt_fl2_65536_21
	ORA	___fsgt_fl1_65536_21
	STA	___fsgt_STK02
	LDA	(___fsgt_fl2_65536_21 + 1)
	ORA	(___fsgt_fl1_65536_21 + 1)
	STA	___fsgt_STK01
	LDA	(___fsgt_fl2_65536_21 + 2)
	ORA	(___fsgt_fl1_65536_21 + 2)
	STA	___fsgt_STK00
	LDA	(___fsgt_fl2_65536_21 + 3)
	ORA	(___fsgt_fl1_65536_21 + 3)
	STA	r0x1162
	LDA	___fsgt_STK02
	ORA	___fsgt_STK01
	ORA	___fsgt_STK00
	JNZ	_00106_DS_
	LDA	r0x1162
	AND	#0x7f
	JNZ	_00106_DS_
;	;.line	116; "_fsgt.c"	return (0);
	CLRA	
	JMP	_00114_DS_
_00106_DS_:
;	;.line	118; "_fsgt.c"	if (fl1.l<0 && fl2.l<0) {
	LDA	(___fsgt_fl1_65536_21 + 3)
	JPL	_00110_DS_
	LDA	(___fsgt_fl2_65536_21 + 3)
	JPL	_00110_DS_
;	;.line	119; "_fsgt.c"	if (fl2.l > fl1.l)
	SETB	_C
	LDA	___fsgt_fl1_65536_21
	SUBB	___fsgt_fl2_65536_21
	LDA	(___fsgt_fl1_65536_21 + 1)
	SUBB	(___fsgt_fl2_65536_21 + 1)
	LDA	(___fsgt_fl1_65536_21 + 2)
	SUBB	(___fsgt_fl2_65536_21 + 2)
	LDA	(___fsgt_fl1_65536_21 + 3)
	SUBSI	
	SUBB	(___fsgt_fl2_65536_21 + 3)
	JC	_00108_DS_
;	;.line	120; "_fsgt.c"	return (1);
	LDA	#0x01
	JMP	_00114_DS_
_00108_DS_:
;	;.line	121; "_fsgt.c"	return (0);
	CLRA	
	JMP	_00114_DS_
_00110_DS_:
;	;.line	124; "_fsgt.c"	if (fl1.l > fl2.l)
	SETB	_C
	LDA	___fsgt_fl2_65536_21
	SUBB	___fsgt_fl1_65536_21
	LDA	(___fsgt_fl2_65536_21 + 1)
	SUBB	(___fsgt_fl1_65536_21 + 1)
	LDA	(___fsgt_fl2_65536_21 + 2)
	SUBB	(___fsgt_fl1_65536_21 + 2)
	LDA	(___fsgt_fl2_65536_21 + 3)
	SUBSI	
	SUBB	(___fsgt_fl1_65536_21 + 3)
	JC	_00113_DS_
;	;.line	125; "_fsgt.c"	return (1);
	LDA	#0x01
	JMP	_00114_DS_
_00113_DS_:
;	;.line	126; "_fsgt.c"	return (0);
	CLRA	
_00114_DS_:
;	;.line	127; "_fsgt.c"	}
	RET	
; exit point of ___fsgt
	.ENDFUNC ___fsgt
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:L_fsgt.__fsgt$a2$65536$20({4}SF:S),R,0,0,[___fsgt_STK06,___fsgt_STK05,___fsgt_STK04,___fsgt_STK03]
	;--cdb--S:L_fsgt.__fsgt$a1$65536$20({4}SF:S),R,0,0,[___fsgt_STK02,___fsgt_STK01,___fsgt_STK00,r0x1162]
	;--cdb--S:L_fsgt.__fsgt$fl1$65536$21({4}STfloat_long:S),E,0,0
	;--cdb--S:L_fsgt.__fsgt$fl2$65536$21({4}STfloat_long:S),E,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___fsgt
	.globl	___fsgt_fl1_65536_21
	.globl	___fsgt_fl2_65536_21
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
___fsgt_fl1_65536_21:	.ds	4

	.area DSEG(DATA)
___fsgt_fl2_65536_21:	.ds	4

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__fsgt_0	udata
r0x1162:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
___fsgt_STK00:	.ds	1
	.globl ___fsgt_STK00
___fsgt_STK01:	.ds	1
	.globl ___fsgt_STK01
___fsgt_STK02:	.ds	1
	.globl ___fsgt_STK02
___fsgt_STK03:	.ds	1
	.globl ___fsgt_STK03
___fsgt_STK04:	.ds	1
	.globl ___fsgt_STK04
___fsgt_STK05:	.ds	1
	.globl ___fsgt_STK05
___fsgt_STK06:	.ds	1
	.globl ___fsgt_STK06
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:___fsgt_STK02:NULL+0:-1:1
	;--cdb--W:___fsgt_STK01:NULL+0:-1:1
	;--cdb--W:___fsgt_STK00:NULL+0:-1:1
	end
