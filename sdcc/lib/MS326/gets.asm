;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"gets.c"
	.module gets
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$_print_format$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$printf_small$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$printf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$vprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$sprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$vsprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$puts$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$getchar$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$putchar$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$gets$0$0({2}DF,DD,SC:U),C,0,0
	;--cdb--F:G$gets$0$0({2}DF,DD,SC:U),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; gets-code 
.globl _gets

;--------------------------------------------------------
	.FUNC _gets:$PNUM 2:$C:_getchar:$C:_putchar\
:$L:r0x1160:$L:_gets_STK00:$L:r0x1161:$L:r0x1162
;--------------------------------------------------------
;	.line	32; "gets.c"	gets (__data char *s)
_gets:	;Function start
	STA	r0x1160
;	;.line	35; "gets.c"	unsigned int count = 0;
	CLRA	
	STA	r0x1161
	STA	r0x1162
_00113_DS_:
;	;.line	39; "gets.c"	c = getchar ();
	CALL	_getchar
;	;.line	40; "gets.c"	switch(c)
	LDA	STK00
	ADD	#0xf8
	JZ	_00105_DS_
	ADD	#0xfe
	JZ	_00109_DS_
	ADD	#0xfd
	JZ	_00109_DS_
	JMP	_00110_DS_
_00105_DS_:
;	;.line	43; "gets.c"	if (count)
	LDA	r0x1161
	ORA	r0x1162
	JZ	_00113_DS_
;	;.line	45; "gets.c"	putchar ('\b');
	LDA	#0x08
	CALL	_putchar
;	;.line	46; "gets.c"	putchar (' ');
	LDA	#0x20
	CALL	_putchar
;	;.line	47; "gets.c"	putchar ('\b');
	LDA	#0x08
	CALL	_putchar
;	;.line	48; "gets.c"	--s;
	LDA	_gets_STK00
	DECA	
	STA	_gets_STK00
	LDA	#0xff
	ADDC	r0x1160
	STA	r0x1160
;	;.line	49; "gets.c"	--count;
	LDA	r0x1161
	DECA	
	STA	r0x1161
	LDA	#0xff
	ADDC	r0x1162
	STA	r0x1162
;	;.line	51; "gets.c"	break;
	JMP	_00113_DS_
_00109_DS_:
;	;.line	55; "gets.c"	putchar ('\r');
	LDA	#0x0d
	CALL	_putchar
;	;.line	56; "gets.c"	putchar ('\n');
	LDA	#0x0a
	CALL	_putchar
;	;.line	57; "gets.c"	*s = 0;
	LDA	_gets_STK00
	STA	_ROMPL
	LDA	r0x1160
	STA	_ROMPH
	CLRA	
	STA	@_ROMP
;	;.line	58; "gets.c"	return s;
	LDA	_gets_STK00
	STA	STK00
	LDA	r0x1160
	JMP	_00115_DS_
_00110_DS_:
;	;.line	61; "gets.c"	*s++ = c;
	LDA	_gets_STK00
	STA	_ROMPL
	LDA	r0x1160
	STA	_ROMPH
	LDA	STK00
	STA	@_ROMP
	LDA	_gets_STK00
	INCA	
	STA	_gets_STK00
	CLRA	
	ADDC	r0x1160
	STA	r0x1160
;	;.line	62; "gets.c"	++count;
	LDA	r0x1161
	INCA	
	STA	r0x1161
	CLRA	
	ADDC	r0x1162
	STA	r0x1162
;	;.line	63; "gets.c"	putchar (c);
	LDA	STK00
	CALL	_putchar
;	;.line	65; "gets.c"	}
	JMP	_00113_DS_
_00115_DS_:
;	;.line	67; "gets.c"	}
	RET	
; exit point of _gets
	.ENDFUNC _gets
	;--cdb--S:G$_print_format$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$printf_small$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$printf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$vprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$sprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$vsprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$puts$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$getchar$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$putchar$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$gets$0$0({2}DF,DD,SC:U),C,0,0
	;--cdb--S:Lgets.gets$s$65536$11({2}DD,SC:U),R,0,0,[_gets_STK00,r0x1160]
	;--cdb--S:Lgets.gets$c$65536$12({1}SC:U),R,0,0,[r0x1165]
	;--cdb--S:Lgets.gets$count$65536$12({2}SI:U),R,0,0,[r0x1161,r0x1162]
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_getchar
	.globl	_putchar

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_gets
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_gets_0	udata
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_gets_STK00:	.ds	1
	.globl _gets_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1164:NULL+0:-1:1
	;--cdb--W:r0x1163:NULL+0:14:0
	;--cdb--W:r0x1165:NULL+0:14:0
	end
