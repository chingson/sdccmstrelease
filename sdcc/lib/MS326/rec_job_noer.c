#include"ms326.h"
#include"ms326typedef.h"
#include"ms326sphlib.h"

extern BYTE api_rampage;
extern USHORT api_endpage;
extern BYTE api_fifostart;
extern BYTE api_fifoend;
extern api_spi_addr_t api_rec_spi_addr;
extern BYTE api_mode;
extern BYTE api_maxrecopg;
static  BYTE api_noscnt=0;

//extern __code const BYTE ms326_nextrecpage[4];
BYTE api_rec_job_noer(void)
{
    //BYTE halt_page;
    BYTE rampagediv8;
    BYTE nowh128;
//	BYTE chk;


//    if(api_mode & API_RECMODE_ALC)
//    {
//        if(DCLAMP&0x80)
//        {
//            ADCG--;
//            DCLAMP=(DCLAMP&0xfe)|0xf8;
//            api_noscnt=0;
//        } else
//        {
//            if(++api_noscnt>5)
//            {
//                api_noscnt=0;
//                if(ADCG<api_maxrecopg)
//                    ADCG++;
//            }
//        }
//    }

    // consider from 180~77f
    GIF=0x7f;
    if(SPIOP&1)
        return 1;// no operatoin
    nowh128=(((USHORT)RDMALH)>>7);
    rampagediv8=api_rampage>>3;
    // we consider there is a case
    // 400~5FF recording
    // rampage is 40 50 40 50
    // if dma is 5Xx rampage is 40, it will
    if(nowh128!=rampagediv8 && nowh128!=(rampagediv8+1)) // api_rampage is 16-byte aligh
    {
        SPIMH=api_rec_spi_addr.addrhm;
        SPIL=api_rec_spi_addr.addrl;
again:
        /*
        // only shuttle bug
        chk=0;
        ROMPUW=(0x8000|(api_rampage<<4));
        do{
        			if(ROMP==0x80) // 4 times to reduce branch load
        				ROMPINC=0x00;
        			else
        				ACC=ROMPINC;
        			if(ROMP==0x80)
        				ROMPINC=0x00;
        			else
        				ACC=ROMPINC;
        			if(ROMP==0x80)
        				ROMPINC=0x00;
        			else
        				ACC=ROMPINC;
        			if(ROMP==0x80)
        				ROMPINC=0x00;
        			else
        				ACC=ROMPINC;
        			chk+=4;
        }while(chk); // half page work
        */



        SPIOP=1;
        SPIOPRAH=(api_rampage>>3); // SPIOPRAH is 128 byte as a unit
        SPIOP=0x4;// write this page
        SPIOP=0x20; // increase a page
        //halt_page=api_rampage>>4; // this is 256 byte align, 128 bytes ... whatever


        //api_rampage=ms326_nextrecpage[api_rampage];
        api_rampage+=0x10;
        if(api_rampage>=api_fifoend)
        {
            api_rampage=api_fifostart;
        }

        if(SPIM==(BYTE)(api_endpage&0xff) && SPIH==(BYTE)(api_endpage>>8))
        {
            return 0;
        }
        //if(!(SPIM&0xf))
        //{
        //ADCON|=halt_page<<3;
        //SPIOP=1;

        //if(api_mode&API_RECMODE_ERASE_SUSP)
        //{
        //SPIOP=0x82;
        //goto fin1; // if there is wake up we
        //}
        //else
        //{
        //SPIOP=2;
        //ADCON|=0x38;// no halt now
        //}
        // }
        rampagediv8=api_rampage>>3;
        if(nowh128!=rampagediv8 && nowh128!=(rampagediv8+1))
            goto again;
fin1:
        ADCON|=0x38;// no halt now

        api_rec_spi_addr.addrhm=SPIMH;
        api_rec_spi_addr.addrl=SPIL; // need save when return 2



        return 2;
    }
    return 1;
}

