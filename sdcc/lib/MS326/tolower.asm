;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"tolower.c"
	.module tolower
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$tolower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isblank$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$islower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isupper$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; tolower-code 
.globl _tolower

;--------------------------------------------------------
	.FUNC _tolower:$PNUM 2:$L:r0x1160:$L:_tolower_STK00:$L:r0x1161:$L:r0x1162
;--------------------------------------------------------
;	.line	33; "tolower.c"	int tolower (int c)
_tolower:	;Function start
	STA	r0x1160
;	;.line	80; "/home/chingson/sdccofficial/sdcc/device/include/ctype.h"	return ((unsigned char)c >= 'A' && (unsigned char)c <= 'Z');
	LDA	_tolower_STK00
	ADD	#0xbf
	JNC	_00108_DS_
	SETB	_C
	LDA	#0x5a
	SUBB	_tolower_STK00
	JNC	_00108_DS_
;	;.line	35; "tolower.c"	return (isupper (c) ? c + ('a' - 'A') : c);
	LDA	#0x20
	ADD	_tolower_STK00
	STA	r0x1161
	CLRA	
	ADDC	r0x1160
	STA	r0x1162
	JMP	_00109_DS_
_00108_DS_:
	LDA	_tolower_STK00
	STA	r0x1161
	LDA	r0x1160
	STA	r0x1162
_00109_DS_:
	LDA	r0x1161
	STA	STK00
	LDA	r0x1162
;	;.line	36; "tolower.c"	}
	RET	
; exit point of _tolower
	.ENDFUNC _tolower
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:Ltolower.isblank$c$65536$13({2}SI:S),E,0,0
	;--cdb--S:Ltolower.isdigit$c$65536$15({2}SI:S),E,0,0
	;--cdb--S:Ltolower.islower$c$65536$17({2}SI:S),E,0,0
	;--cdb--S:Ltolower.isupper$c$65536$19({2}SI:S),E,0,0
	;--cdb--S:Ltolower.tolower$c$65536$21({2}SI:S),R,0,0,[_tolower_STK00,r0x1160]
	;--cdb--S:Ltolower.tolower$__1310720001$131072$22({2}SI:S),R,0,0,[]
	;--cdb--S:Ltolower.tolower$__1310720002$131072$23({2}SI:S),R,0,0,[]
	;--cdb--S:Ltolower.tolower$c$196608$24({2}SI:S),R,0,0,[]
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_tolower
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_tolower_0	udata
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_tolower_STK00:	.ds	1
	.globl _tolower_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1161:NULL+0:4451:0
	end
