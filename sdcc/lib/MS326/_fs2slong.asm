;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/MS326"
;;	.file	"_fs2slong.c"
	.module _fs2slong
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--F:G$__fs2slong$0$0({2}DF,SL:S),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _fs2slong-code 
.globl ___fs2slong

;--------------------------------------------------------
	.FUNC ___fs2slong:$PNUM 4:$C:___fslt:$C:___fs2ulong\
:$L:r0x1162:$L:___fs2slong_STK00:$L:___fs2slong_STK01:$L:___fs2slong_STK02:$L:r0x1163\
:$L:r0x1166:$L:r0x1165:$L:r0x1164
;--------------------------------------------------------
;	.line	108; "_fs2slong.c"	signed long __fs2slong (float f)
___fs2slong:	;Function start
	STA	r0x1162
;	;.line	111; "_fs2slong.c"	if (!f)
	LDA	___fs2slong_STK02
	ORA	___fs2slong_STK01
	ORA	___fs2slong_STK00
	ORA	r0x1162
	JNZ	_00106_DS_
;	;.line	112; "_fs2slong.c"	return 0;
	CLRA	
	STA	STK02
	STA	STK01
	STA	STK00
	JMP	_00110_DS_
_00106_DS_:
;	;.line	114; "_fs2slong.c"	if (f<0) {
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	STA	___fslt_STK04
	STA	___fslt_STK03
	LDA	___fs2slong_STK02
	STA	___fslt_STK02
	LDA	___fs2slong_STK01
	STA	___fslt_STK01
	LDA	___fs2slong_STK00
	STA	___fslt_STK00
	LDA	r0x1162
	CALL	___fslt
	JZ	_00108_DS_
;	;.line	115; "_fs2slong.c"	return -__fs2ulong(-f);
	LDA	r0x1162
	XOR	#0x80
	STA	r0x1166
	LDA	___fs2slong_STK02
	STA	___fs2ulong_STK02
	LDA	___fs2slong_STK01
	STA	___fs2ulong_STK01
	LDA	___fs2slong_STK00
	STA	___fs2ulong_STK00
	LDA	r0x1166
	CALL	___fs2ulong
	STA	r0x1166
	LDA	STK02
	SETB	_C
	CLRA	
	SUBB	STK02
	STA	r0x1163
	CLRA	
	SUBB	STK01
	STA	r0x1164
	CLRA	
	SUBB	STK00
	STA	r0x1165
	CLRA	
	SUBB	r0x1166
	STA	r0x1166
	LDA	r0x1163
	STA	STK02
	LDA	r0x1164
	STA	STK01
	LDA	r0x1165
	STA	STK00
	LDA	r0x1166
	JMP	_00110_DS_
_00108_DS_:
;	;.line	117; "_fs2slong.c"	return __fs2ulong(f);
	LDA	___fs2slong_STK02
	STA	___fs2ulong_STK02
	LDA	___fs2slong_STK01
	STA	___fs2ulong_STK01
	LDA	___fs2slong_STK00
	STA	___fs2ulong_STK00
	LDA	r0x1162
	CALL	___fs2ulong
_00110_DS_:
;	;.line	119; "_fs2slong.c"	}
	RET	
; exit point of ___fs2slong
	.ENDFUNC ___fs2slong
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:L_fs2slong.__fs2slong$f$65536$20({4}SF:S),R,0,0,[___fs2slong_STK02,___fs2slong_STK01,___fs2slong_STK00,r0x1162]
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	___fs2ulong
	.globl	___fslt

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___fs2slong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__fs2slong_0	udata
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
___fs2slong_STK00:	.ds	1
	.globl ___fs2slong_STK00
___fs2slong_STK01:	.ds	1
	.globl ___fs2slong_STK01
___fs2slong_STK02:	.ds	1
	.globl ___fs2slong_STK02
	.globl ___fslt_STK06
	.globl ___fslt_STK05
	.globl ___fslt_STK04
	.globl ___fslt_STK03
	.globl ___fslt_STK02
	.globl ___fslt_STK01
	.globl ___fslt_STK00
	.globl ___fs2ulong_STK02
	.globl ___fs2ulong_STK01
	.globl ___fs2ulong_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:___fs2slong_STK00:NULL+0:14:0
	;--cdb--W:___fs2slong_STK01:NULL+0:13:0
	;--cdb--W:___fs2slong_STK02:NULL+0:12:0
	;--cdb--W:r0x1163:NULL+0:4457:0
	;--cdb--W:r0x1163:NULL+0:12:0
	;--cdb--W:r0x1165:NULL+0:4455:0
	;--cdb--W:r0x1165:NULL+0:14:0
	;--cdb--W:r0x1164:NULL+0:4456:0
	;--cdb--W:r0x1164:NULL+0:13:0
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x1162:NULL+0:-1:1
	end
