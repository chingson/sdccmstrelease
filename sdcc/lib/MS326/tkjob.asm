;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.1 #3de0c6772 (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"C:\work\ms326sphlib"
;;	.file	"tkjob.c"
	.module tkjob
	;.list	p=MS326
	.include "ms326sfr.def"
	;--cdb--T:Ftkjob$spiastru[({0}S:S$addrhm$0$0({2}SI:U),Z,0,0)({2}S:S$addrl$0$0({1}SC:U),Z,0,0)]
	;--cdb--T:Ftkjob$adps[({0}S:S$predict$0$0({2}SI:S),Z,0,0)({2}S:S$index$0$0({1}SC:U),Z,0,0)]
	;--cdb--T:Ftkjob$touchen[({0}S:S$toff$0$0({1}SC:U),Z,0,0)({1}S:S$nmossw$0$0({1}SC:U),Z,0,0)({2}S:S$period$0$0({2}SI:U),Z,0,0)({4}S:S$threshold$0$0({2}SI:U),Z,0,0)({6}S:S$count$0$0({2}SI:U),Z,0,0)]
	;--cdb--T:Ftkjob$pwmleds[({0}S:S$period$0$0({1}SC:U),Z,0,0)({1}S:S$counter$0$0({1}SC:U),Z,0,0)({2}S:S$threshold$0$0({1}SC:U),Z,0,0)]
	;--cdb--S:G$api_tk_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$api_tk_job$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_pre_erase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start_no_erase$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop_noerase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_noer$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_chspick$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_init$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_tk_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; tkjob-code 
.globl _api_tk_job

;--------------------------------------------------------
	.FUNC _api_tk_job:$PNUM 0:$C:__mullong\
:$L:r0x11B0:$L:r0x11B1:$L:r0x11B2:$L:r0x11B3:$L:r0x11B4\
:$L:r0x11B5:$L:r0x11B6:$L:r0x11B7:$L:r0x11B8:$L:r0x11B9\
:$L:r0x11BA:$L:r0x11BB:$L:r0x11BC:$L:r0x11BD:$L:r0x11BF\
:$L:r0x11C0:$L:r0x11C1:$L:r0x11C2:$L:r0x11C3:$L:r0x11C4\
:$L:r0x11C5:$L:r0x11C6
;--------------------------------------------------------
;	.line	25; "tkjob.c"	if (TOUCHC & 0x80)
_api_tk_job:	;Function start
	LDA	_TOUCHC
	JPL	_00120_DS_
;	;.line	27; "tkjob.c"	if (rambufp1[0].count == 0)
	LDA	#0x06
	ADD	_rambufp1
	STA	r0x11B0
	CLRA	
	ADDC	(_rambufp1 + 1)
	STA	r0x11B1
	LDA	r0x11B0
	STA	_ROMPL
	LDA	r0x11B1
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x11B2
	LDA	@_ROMPINC
	ORA	r0x11B2
	JNZ	_00106_DS_
;	;.line	29; "tkjob.c"	TOUCHC = TOUCHC_DEFAULT;
	LDA	#0x49
	STA	_TOUCHC
;	;.line	30; "tkjob.c"	return 0; // for sync!!
	CLRA	
	JMP	_00171_DS_
_00106_DS_:
;	;.line	32; "tkjob.c"	if (extrd == 0)
	LDA	_extrd
	JNZ	_00109_DS_
;	;.line	34; "tkjob.c"	tkramp = rambufp1;
	LDA	_rambufp1
	STA	r0x11B0
	LDA	(_rambufp1 + 1)
	STA	r0x11B1
;	;.line	35; "tkjob.c"	usp = procdatap;
	LDA	_procdatap
	STA	r0x11B2
	LDA	(_procdatap + 1)
	STA	r0x11B3
;	;.line	36; "tkjob.c"	for (i = 0; i < tch_total_ch; i++)
	CLRA	
	STA	r0x11B4
_00166_DS_:
	SETB	_C
	LDA	r0x11B4
	SUBB	_tch_total_ch
	JC	_00107_DS_
;	;.line	38; "tkjob.c"	*usp = tkramp->count - (baseline[i] >> 8);
	LDA	#0x06
	ADD	r0x11B0
	STA	r0x11B5
	CLRA	
	ADDC	r0x11B1
	STA	r0x11B6
	LDA	r0x11B5
	STA	_ROMPL
	LDA	r0x11B6
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x11B7
	LDA	@_ROMPINC
	STA	r0x11B8
	CLRA	
	STA	_MULAL
	STA	_MULBH
	LDA	r0x11B4
	STA	_MULAH
	LDA	#0x04
	STA	_MULBL
	LDA	_MULO2A
	STA	r0x11B5
	LDA	_MULO2B
	STA	r0x11B6
	LDA	_baseline
	ADD	r0x11B5
	STA	r0x11B9
	LDA	(_baseline + 1)
	ADDC	r0x11B6
	STA	r0x11BA
	LDA	r0x11B9
	STA	_ROMPL
	LDA	r0x11BA
	STA	_ROMPH
	LDA	@_ROMPINC
	LDA	@_ROMPINC
	STA	r0x11BC
	LDA	@_ROMPINC
	STA	r0x11BD
	LDA	@_ROMPINC
	SETB	_C
	LDA	r0x11B7
	SUBB	r0x11BC
	STA	r0x11B7
	LDA	r0x11B8
	SUBB	r0x11BD
	STA	r0x11B8
	LDA	r0x11B3
	STA	_ROMPH
	LDA	r0x11B2
	STA	_ROMPL
	LDA	r0x11B7
	STA	@_ROMPINC
	LDA	r0x11B8
	STA	@_ROMPINC
;	;.line	39; "tkjob.c"	usp++;
	LDA	#0x02
	ADD	r0x11B2
	STA	r0x11B7
	CLRA	
	ADDC	r0x11B3
	STA	r0x11B8
;	;.line	40; "tkjob.c"	*usp = (USHORT)((baseline[i] >> 8) & 0xffff); // get the baseline
	LDA	_baseline
	ADD	r0x11B5
	STA	r0x11B5
	LDA	(_baseline + 1)
	ADDC	r0x11B6
	STA	r0x11B6
	LDA	r0x11B5
	STA	_ROMPL
	LDA	r0x11B6
	STA	_ROMPH
	LDA	@_ROMPINC
	LDA	@_ROMPINC
	STA	r0x11BA
	LDA	@_ROMPINC
	STA	r0x11BB
	LDA	@_ROMPINC
	LDA	r0x11B8
	STA	_ROMPH
	LDA	r0x11B7
	STA	_ROMPL
	LDA	r0x11BA
	STA	@_ROMPINC
	LDA	r0x11BB
	STA	@_ROMPINC
;	;.line	41; "tkjob.c"	usp++;
	LDA	#0x02
	ADD	r0x11B7
	STA	r0x11B2
	CLRA	
	ADDC	r0x11B8
	STA	r0x11B3
;	;.line	42; "tkjob.c"	tkramp++;
	LDA	#0x08
	ADD	r0x11B0
	STA	r0x11B0
	CLRA	
	ADDC	r0x11B1
	STA	r0x11B1
;	;.line	36; "tkjob.c"	for (i = 0; i < tch_total_ch; i++)
	LDA	r0x11B4
	INCA	
	STA	r0x11B4
	JMP	_00166_DS_
_00107_DS_:
;	;.line	44; "tkjob.c"	extrd = 1; // 1 when new data 0 by ext sspi
	LDA	#0x01
	STA	_extrd
_00109_DS_:
;	;.line	46; "tkjob.c"	modified = 1;
	LDA	#0x01
	STA	_modified
;	;.line	48; "tkjob.c"	tkramp = rambufp1;
	LDA	_rambufp1
	STA	r0x11B0
	LDA	(_rambufp1 + 1)
	STA	r0x11B1
;	;.line	49; "tkjob.c"	for (i = 0; i < tch_total_ch; i++)
	CLRA	
	STA	r0x11B2
_00169_DS_:
	SETB	_C
	LDA	r0x11B2
	SUBB	_tch_total_ch
	JC	_00118_DS_
;	;.line	52; "tkjob.c"	unsigned long baselinel = baseline[i];
	CLRA	
	STA	_MULAL
	STA	_MULBH
	LDA	r0x11B2
	STA	_MULAH
	LDA	#0x04
	STA	_MULBL
	LDA	_MULO2A
	STA	r0x11B3
	LDA	_MULO2B
	STA	r0x11B4
	LDA	_baseline
	ADD	r0x11B3
	STA	r0x11B3
	LDA	(_baseline + 1)
	ADDC	r0x11B4
	STA	r0x11B4
	LDA	r0x11B3
	STA	_ROMPL
	LDA	r0x11B4
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x11B5
	LDA	@_ROMPINC
	STA	r0x11B6
	LDA	@_ROMPINC
	STA	r0x11B7
	LDA	@_ROMPINC
	STA	r0x11B8
;	;.line	53; "tkjob.c"	unsigned long lowbase = (baselinel - (baselinel >> 5)) >> 8;
	LDA	r0x11B5
	SWA	
	AND	#0x0f
	STA	r0x11B3
	LDA	r0x11B6
	SWA	
	PUSH	
	AND	#0xf0
	ORA	r0x11B3
	STA	r0x11B3
	POP	
	AND	#0x0f
	STA	r0x11B4
	LDA	r0x11B7
	SWA	
	PUSH	
	AND	#0xf0
	ORA	r0x11B4
	STA	r0x11B4
	POP	
	AND	#0x0f
	STA	r0x11B9
	LDA	r0x11B8
	SWA	
	PUSH	
	AND	#0xf0
	ORA	r0x11B9
	STA	r0x11B9
	POP	
	AND	#0x0f
	SHR	
	STA	r0x11BA
	LDA	r0x11B9
	ROR	
	STA	r0x11B9
	LDA	r0x11B4
	ROR	
	STA	r0x11B4
	LDA	r0x11B3
	ROR	
	STA	r0x11B3
	SETB	_C
	LDA	r0x11B5
	SUBB	r0x11B3
	LDA	r0x11B6
	SUBB	r0x11B4
	STA	r0x11B4
	LDA	r0x11B7
	SUBB	r0x11B9
	STA	r0x11B9
	LDA	r0x11B8
	SUBB	r0x11BA
	LDA	r0x11B4
	STA	r0x11BF
;	;.line	56; "tkjob.c"	if (tkramp->count < tkramp->threshold)
	LDA	#0x06
	ADD	r0x11B0
	STA	r0x11B3
	CLRA	
	ADDC	r0x11B1
	STA	r0x11B4
	LDA	r0x11B3
	STA	_ROMPL
	LDA	r0x11B4
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x11C3
	LDA	@_ROMPINC
	STA	r0x11C4
	LDA	#0x04
	ADD	r0x11B0
	STA	r0x11B3
	CLRA	
	ADDC	r0x11B1
	STA	r0x11B4
	LDA	r0x11B3
	STA	_ROMPL
	LDA	r0x11B4
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x11C5
	LDA	@_ROMPINC
	STA	r0x11C6
	SETB	_C
	LDA	r0x11C3
	SUBB	r0x11C5
	LDA	r0x11C4
	SUBB	r0x11C6
	JC	_00117_DS_
;	;.line	58; "tkjob.c"	if (tkramp->count < (unsigned short)lowbase)
	SETB	_C
	LDA	r0x11C3
	SUBB	r0x11BF
	LDA	r0x11C4
	SUBB	r0x11B9
	JC	_00111_DS_
;	;.line	59; "tkjob.c"	baselinel = ((unsigned long)tkramp->count) << 8;
	CLRA	
	STA	r0x11C6
	LDA	r0x11C4
	STA	r0x11C5
	LDA	r0x11C3
	STA	r0x11C2
	CLRA	
	STA	r0x11C1
	JMP	_00112_DS_
_00111_DS_:
;	;.line	62; "tkjob.c"	baselinel = baselinel - (baselinel >> 8);
	LDA	r0x11B8
	CLRA	
	SETB	_C
	LDA	r0x11B5
	SUBB	r0x11B6
	STA	r0x11B3
	LDA	r0x11B6
	SUBB	r0x11B7
	STA	r0x11B4
	LDA	r0x11B7
	SUBB	r0x11B8
	STA	r0x11BF
	LDA	r0x11B8
	SUBB	#0x00
	STA	r0x11C0
;	;.line	63; "tkjob.c"	baselinel = baselinel + tkramp->count;
	LDA	r0x11B3
	ADD	r0x11C3
	STA	r0x11C1
	LDA	r0x11B4
	ADDC	r0x11C4
	STA	r0x11C2
	CLRA	
	ADDC	r0x11BF
	STA	r0x11C5
	CLRA	
	ADDC	r0x11C0
	STA	r0x11C6
_00112_DS_:
;	;.line	65; "tkjob.c"	if (threshold_ratio != NULL)
	LDA	_threshold_ratio
	ORA	(_threshold_ratio + 1)
	JZ	_00114_DS_
;	;.line	66; "tkjob.c"	ratio = threshold_ratio[i];
	LDA	r0x11B2
	ADD	_threshold_ratio
	STA	r0x11B3
	CLRA	
	ADDC	(_threshold_ratio + 1)
	STA	r0x11B4
	LDA	r0x11B3
	STA	_ROMPL
	LDA	r0x11B4
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x11B5
	JMP	_00115_DS_
_00114_DS_:
;	;.line	68; "tkjob.c"	ratio = 0x87; // we take 5%
	LDA	#0x87
	STA	r0x11B5
_00115_DS_:
;	;.line	70; "tkjob.c"	tkramp->threshold = (USHORT)((baselinel * ((unsigned long)ratio)) >> 15);
	LDA	#0x04
	ADD	r0x11B0
	STA	r0x11B3
	CLRA	
	ADDC	r0x11B1
	STA	r0x11B4
	CLRA	
	STA	r0x11B7
	STA	r0x11B8
	STA	r0x11BF
	LDA	r0x11B5
	STA	__mullong_STK06
	LDA	r0x11B7
	STA	__mullong_STK05
	LDA	r0x11B8
	STA	__mullong_STK04
	LDA	r0x11BF
	STA	__mullong_STK03
	LDA	r0x11C1
	STA	__mullong_STK02
	LDA	r0x11C2
	STA	__mullong_STK01
	LDA	r0x11C5
	STA	__mullong_STK00
	LDA	r0x11C6
	CALL	__mullong
	STA	r0x11B8
	LDA	STK01
	ROL	
	LDA	STK00
	ROL	
	STA	r0x11BF
	LDA	r0x11B8
	ROL	
	STA	r0x11C0
	CLRB	_C
	LDA	r0x11B4
	STA	_ROMPH
	LDA	r0x11B3
	STA	_ROMPL
	LDA	r0x11BF
	STA	@_ROMPINC
	LDA	r0x11C0
	STA	@_ROMPINC
;	;.line	71; "tkjob.c"	baseline[i] = baselinel;
	CLRA	
	STA	_MULAL
	STA	_MULBH
	LDA	r0x11B2
	STA	_MULAH
	LDA	#0x04
	STA	_MULBL
	LDA	_MULO2A
	STA	r0x11B3
	LDA	_MULO2B
	STA	r0x11B4
	LDA	_baseline
	ADD	r0x11B3
	STA	r0x11B3
	LDA	(_baseline + 1)
	ADDC	r0x11B4
	STA	_ROMPH
	LDA	r0x11B3
	STA	_ROMPL
	LDA	r0x11C1
	STA	@_ROMPINC
	LDA	r0x11C2
	STA	@_ROMPINC
	LDA	r0x11C5
	STA	@_ROMPINC
	LDA	r0x11C6
	STA	@_ROMPINC
_00117_DS_:
;	;.line	73; "tkjob.c"	tkramp++;
	LDA	#0x08
	ADD	r0x11B0
	STA	r0x11B0
	CLRA	
	ADDC	r0x11B1
	STA	r0x11B1
;	;.line	49; "tkjob.c"	for (i = 0; i < tch_total_ch; i++)
	LDA	r0x11B2
	INCA	
	STA	r0x11B2
	JMP	_00169_DS_
_00118_DS_:
;	;.line	76; "tkjob.c"	rambufp1[0].count = 0;
	LDA	#0x06
	ADD	_rambufp1
	STA	r0x11B0
	CLRA	
	ADDC	(_rambufp1 + 1)
	STA	_ROMPH
	LDA	r0x11B0
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
	CLRA	
	STA	@_ROMPINC
;	;.line	77; "tkjob.c"	TOUCHC = TOUCHC_DEFAULT;
	LDA	#0x49
	STA	_TOUCHC
_00120_DS_:
;	;.line	79; "tkjob.c"	if (SSPIH == lastcmd)
	LDA	_lastcmd
	XOR	_SSPIH
	JNZ	_00122_DS_
;	;.line	94; "tkjob.c"	return modified;
	LDA	_modified
	JMP	_00171_DS_
_00122_DS_:
;	;.line	96; "tkjob.c"	ROMPUW = rompsave;
	LDA	_rompsave
	STA	_ROMPUW
	LDA	(_rompsave + 1)
	STA	(_ROMPUW + 1)
;	;.line	97; "tkjob.c"	lastcmd = SSPIH;
	LDA	_SSPIH
;	;.line	98; "tkjob.c"	cmd_now = lastcmd & 0x1F;
	STA	_lastcmd
	AND	#0x1f
;	;.line	99; "tkjob.c"	switch (cmd_now)
	STA	_cmd_now
	JZ	_00163_DS_
	SETB	_C
	LDA	#0x07
	SUBB	_cmd_now
	JNC	_00163_DS_
	LDA	_cmd_now
	DECA	
	CALL	_00358_DS_
_00358_DS_:
	SHL	
	STA	_PTRCL
	LDAT	#_00359_DS_
	STA	_STACKL
	LDA	_PTRCL
	INCA	
	LDAT	#_00359_DS_
	STA	_STACKH
	RET	
_00359_DS_:
	.DW	#_00123_DS_
	.DW	#_00124_DS_
	.DW	#_00125_DS_
	.DW	#_00126_DS_
	.DW	#_00133_DS_
	.DW	#_00140_DS_
	.DW	#_00162_DS_
_00123_DS_:
;	;.line	102; "tkjob.c"	SSPIL = ROMPINC;
	LDA	@_ROMPINC
	STA	_SSPIL
;	;.line	103; "tkjob.c"	SSPIM = ROMPINC;
	LDA	@_ROMPINC
	STA	_SSPIM
;	;.line	105; "tkjob.c"	break;
	JMP	_00164_DS_
_00124_DS_:
;	;.line	107; "tkjob.c"	ROMPL = SSPIL;
	LDA	_SSPIL
	STA	_ROMPL
;	;.line	108; "tkjob.c"	ROMPH = SSPIM;
	LDA	_SSPIM
	STA	_ROMPH
;	;.line	109; "tkjob.c"	break;
	JMP	_00164_DS_
_00125_DS_:
;	;.line	111; "tkjob.c"	ROMPINC = SSPIM;
	LDA	_SSPIM
	STA	@_ROMPINC
;	;.line	112; "tkjob.c"	break;
	JMP	_00164_DS_
_00126_DS_:
;	;.line	120; "tkjob.c"	if (SSPIM == 0x7E) //
	LDA	_SSPIM
	XOR	#0x7e
	JNZ	_00128_DS_
;	;.line	122; "tkjob.c"	SSPIM = HWD;
	LDA	_HWD
	STA	_SSPIM
;	;.line	123; "tkjob.c"	break;
	JMP	_00164_DS_
_00128_DS_:
;	;.line	125; "tkjob.c"	if (SSPIM == 0x7f)
	LDA	_SSPIM
	XOR	#0x7f
	JNZ	_00130_DS_
;	;.line	127; "tkjob.c"	SSPIM = HWDINC;
	LDA	_HWDINC
	STA	_SSPIM
;	;.line	128; "tkjob.c"	break;
	JMP	_00164_DS_
_00130_DS_:
;	;.line	130; "tkjob.c"	if (SSPIM == 0x4E) // spidat
	LDA	_SSPIM
	XOR	#0x4e
	JNZ	_00132_DS_
;	;.line	132; "tkjob.c"	SPIM = SPIDAT;
	LDA	_SPIDAT
	STA	_SPIM
_00132_DS_:
;	;.line	134; "tkjob.c"	ROMPUW = 0x8100;
	CLRA	
	STA	_ROMPUW
	LDA	#0x81
	STA	(_ROMPUW + 1)
;	;.line	135; "tkjob.c"	ROMPINC = 0x02;
	LDA	#0x02
	STA	@_ROMPINC
;	;.line	136; "tkjob.c"	ROMPINC = SSPIM;
	LDA	_SSPIM
	STA	@_ROMPINC
;	;.line	137; "tkjob.c"	ROMPINC = 0xc0;
	LDA	#0xc0
	STA	@_ROMPINC
;	;.line	138; "tkjob.c"	PRG_RAM = 1;
	LDA	#0x01
	STA	_PRG_RAM
;	;.line	140; "tkjob.c"	..asm
	.DB	0xb0
;	;.line	141; "tkjob.c"	..asm
	.DB	0x00
;	;.line	142; "tkjob.c"	..asm
	.DB	0x12;
;	;.line	143; "tkjob.c"	..asm
	.DB	0x3b;
;	;.line	144; "tkjob.c"	 ..asm
	
;	;.line	145; "tkjob.c"	PRG_RAM = 0;
	CLRA	
	STA	_PRG_RAM
;	;.line	146; "tkjob.c"	break;
	JMP	_00164_DS_
_00133_DS_:
;	;.line	148; "tkjob.c"	if (SSPIM == 0x7E)
	LDA	_SSPIM
	XOR	#0x7e
	JNZ	_00135_DS_
;	;.line	150; "tkjob.c"	HWD = SSPIL;
	LDA	_SSPIL
	STA	_HWD
;	;.line	151; "tkjob.c"	break;
	JMP	_00164_DS_
_00135_DS_:
;	;.line	153; "tkjob.c"	if (SSPIM == 0x7F)
	LDA	_SSPIM
	XOR	#0x7f
	JNZ	_00137_DS_
;	;.line	155; "tkjob.c"	HWDINC = SSPIL;
	LDA	_SSPIL
	STA	_HWDINC
;	;.line	156; "tkjob.c"	break;
	JMP	_00164_DS_
_00137_DS_:
;	;.line	158; "tkjob.c"	if (SSPIM == 0x4e)
	LDA	_SSPIM
	XOR	#0x4e
	JNZ	_00139_DS_
;	;.line	160; "tkjob.c"	SPIDAT = SSPIL;
	LDA	_SSPIL
	STA	_SPIDAT
;	;.line	161; "tkjob.c"	break;
	JMP	_00164_DS_
_00139_DS_:
;	;.line	163; "tkjob.c"	ROMPUW = 0x8100;
	CLRA	
	STA	_ROMPUW
	LDA	#0x81
	STA	(_ROMPUW + 1)
;	;.line	164; "tkjob.c"	ROMPINC = 0x0;
	CLRA	
	STA	@_ROMPINC
;	;.line	165; "tkjob.c"	ROMPINC = SSPIL;
	LDA	_SSPIL
	STA	@_ROMPINC
;	;.line	166; "tkjob.c"	ROMPINC = 0x12; // STA
	LDA	#0x12
	STA	@_ROMPINC
;	;.line	167; "tkjob.c"	ROMPINC = SSPIM;
	LDA	_SSPIM
	STA	@_ROMPINC
;	;.line	168; "tkjob.c"	ROMPINC = 0xc0; // RET
	LDA	#0xc0
	STA	@_ROMPINC
;	;.line	169; "tkjob.c"	PRG_RAM = 1;
	LDA	#0x01
	STA	_PRG_RAM
;	;.line	171; "tkjob.c"	..asm
	.DB	0xb0
;	;.line	172; "tkjob.c"	 ..asm
	.DB	0x00 
;	;.line	173; "tkjob.c"	PRG_RAM = 0;
	CLRA	
	STA	_PRG_RAM
;	;.line	174; "tkjob.c"	break;
	JMP	_00164_DS_
_00140_DS_:
;	;.line	178; "tkjob.c"	if (SSPIM == 0)
	LDA	_SSPIM
	JNZ	_00160_DS_
;	;.line	180; "tkjob.c"	SSPIL = extrd;
	LDA	_extrd
	STA	_SSPIL
;	;.line	181; "tkjob.c"	SSPIM = 0x55;
	LDA	#0x55
	STA	_SSPIM
	JMP	_00164_DS_
_00160_DS_:
;	;.line	183; "tkjob.c"	else if (SSPIM == 1)
	LDA	_SSPIM
	XOR	#0x01
	JNZ	_00157_DS_
;	;.line	185; "tkjob.c"	SSPIM = ((USHORT)procdatap) >> 8;
	LDA	(_procdatap + 1)
	STA	_SSPIM
;	;.line	186; "tkjob.c"	SSPIL = ((USHORT)procdatap) & 0xff;
	LDA	_procdatap
	STA	_SSPIL
	JMP	_00164_DS_
_00157_DS_:
;	;.line	188; "tkjob.c"	else if (SSPIM == 2)
	LDA	_SSPIM
	XOR	#0x02
	JNZ	_00154_DS_
;	;.line	190; "tkjob.c"	extrd = 0;
	CLRA	
	STA	_extrd
;	;.line	191; "tkjob.c"	SSPIM = 0x66;
	LDA	#0x66
	STA	_SSPIM
;	;.line	192; "tkjob.c"	SSPIL = 0x33;
	LDA	#0x33
	STA	_SSPIL
	JMP	_00164_DS_
_00154_DS_:
;	;.line	194; "tkjob.c"	else if (SSPIM == 3)
	LDA	_SSPIM
	XOR	#0x03
	JNZ	_00151_DS_
;	;.line	196; "tkjob.c"	SSPIL = PATEN;
	LDA	_PATEN
	STA	_SSPIL
;	;.line	197; "tkjob.c"	SSPIM = PBTEN;
	LDA	_PBTEN
	STA	_SSPIM
	JMP	_00164_DS_
_00151_DS_:
;	;.line	199; "tkjob.c"	else if (SSPIM == 4)
	LDA	_SSPIM
	XOR	#0x04
	JNZ	_00148_DS_
;	;.line	201; "tkjob.c"	SSPIL = ((USHORT)rambufp1) & 0xff;
	LDA	_rambufp1
;	;.line	202; "tkjob.c"	SSPIM = ((USHORT)rambufp1) >> 8;
	STA	_SSPIL
	LDA	(_rambufp1 + 1)
	STA	_SSPIM
	JMP	_00164_DS_
_00148_DS_:
;	;.line	204; "tkjob.c"	else if (SSPIM == 5)
	LDA	_SSPIM
	XOR	#0x05
	JNZ	_00145_DS_
;	;.line	206; "tkjob.c"	SSPIL = ((USHORT)baseline) & 0xff;
	LDA	_baseline
;	;.line	207; "tkjob.c"	SSPIM = ((USHORT)baseline) >> 8;
	STA	_SSPIL
	LDA	(_baseline + 1)
	STA	_SSPIM
	JMP	_00164_DS_
_00145_DS_:
;	;.line	209; "tkjob.c"	else if (SSPIM == 6)
	LDA	_SSPIM
	XOR	#0x06
	JNZ	_00142_DS_
;	;.line	211; "tkjob.c"	SSPIL = ((USHORT)threshold_ratio) & 0xff;
	LDA	_threshold_ratio
;	;.line	212; "tkjob.c"	SSPIM = ((USHORT)threshold_ratio) >> 8;
	STA	_SSPIL
	LDA	(_threshold_ratio + 1)
	STA	_SSPIM
	JMP	_00164_DS_
_00142_DS_:
;	;.line	216; "tkjob.c"	SSPIL = 0xCC;
	LDA	#0xcc
	STA	_SSPIL
;	;.line	217; "tkjob.c"	SSPIM = 0xDD;
	LDA	#0xdd
	STA	_SSPIM
;	;.line	219; "tkjob.c"	break;
	JMP	_00164_DS_
_00162_DS_:
;	;.line	222; "tkjob.c"	SSPIL = 0xaa;
	LDA	#0xaa
	STA	_SSPIL
;	;.line	223; "tkjob.c"	SSPIM = 0xbb;
	LDA	#0xbb
	STA	_SSPIM
;	;.line	224; "tkjob.c"	break;
	JMP	_00164_DS_
_00163_DS_:
;	;.line	227; "tkjob.c"	SSPIL = 0x12;
	LDA	#0x12
	STA	_SSPIL
;	;.line	228; "tkjob.c"	SSPIM = 0x34;
	LDA	#0x34
	STA	_SSPIM
_00164_DS_:
;	;.line	231; "tkjob.c"	SSPIH = lastcmd;
	LDA	_lastcmd
	STA	_SSPIH
;	;.line	232; "tkjob.c"	rompsave = ROMPUW;
	LDA	_ROMPUW
	STA	_rompsave
	LDA	(_ROMPUW + 1)
	STA	(_rompsave + 1)
;	;.line	233; "tkjob.c"	return modified;
	LDA	_modified
_00171_DS_:
;	;.line	234; "tkjob.c"	}
	RET	
; exit point of _api_tk_job
	.ENDFUNC _api_tk_job
	;--cdb--S:G$api_tk_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_pre_erase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start_no_erase$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop_noerase$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_noer$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_chspick$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_tk_init$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$api_tk_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$PAR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PADIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOB$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$GIF$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$WDTH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PPAGES$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMA_IL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERGP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIDAT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCLKDIV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PCDIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PIOC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CMPCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTVC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMICON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PRG_RAM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PDMALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIDMAC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMAALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECRALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECOAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECOALH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ECLEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ECMODE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIOPRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SDMALEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULB$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULBL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULBH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$MULO$0$0({4}SL:U),E,0,0
	;--cdb--S:G$MULO1$0$0({2}SI:U),E,0,0
	;--cdb--S:G$MULSHIFT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUF$0$0({2}SI:S),E,0,0
	;--cdb--S:G$L2UBUFL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2UBUFH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$L2USH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$U2LSH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$INTPRI$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRA$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$LPWMRAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMRAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LPWMINV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPICK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPSEL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWPA$0$0({2}SI:U),E,0,0
	;--cdb--S:G$HWD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$HWDINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PATEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTEN$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TRAMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TRAMBUFP$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$PASKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBSKIP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABSKIP$0$0({2}SI:U),E,0,0
	;--cdb--S:G$PATR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PBTR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PABTR$0$0({2}SI:U),E,0,0
	;--cdb--S:G$TOUCHC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DUMMYC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IRCD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMEN$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMDUTY$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FPWMPER$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACGCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RECPWR$0$0({4}SL:U),E,0,0
	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RDMALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$HWPALH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0UW$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$EA$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$tch_total_ch$0$0({1}SC:U),E,0,0
	;--cdb--S:G$tch_mon$0$0({1}SC:U),E,0,0
	;--cdb--S:G$procdatap$0$0({2}DG,SI:U),E,0,0
	;--cdb--S:G$rambufp1$0$0({2}DG,STtouchen:S),E,0,0
	;--cdb--S:G$lastcmd$0$0({1}SC:U),E,0,0
	;--cdb--S:G$cmd_now$0$0({1}SC:U),E,0,0
	;--cdb--S:G$extrd$0$0({1}SC:U),E,0,0
	;--cdb--S:G$rompsave$0$0({2}SI:U),E,0,0
	;--cdb--S:G$modified$0$0({1}SC:U),E,0,0
	;--cdb--S:G$baseline$0$0({2}DG,SL:U),E,0,0
	;--cdb--S:G$threshold_ratio$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:Ltkjob.api_tk_job$tkramp$65536$58({2}DG,STtouchen:S),R,0,0,[]
	;--cdb--S:Ltkjob.api_tk_job$usp$65536$58({2}DG,SI:U),R,0,0,[r0x11B7,r0x11B8]
	;--cdb--S:Ltkjob.api_tk_job$i$65536$58({1}SC:U),R,0,0,[r0x11B4]
	;--cdb--S:Ltkjob.api_tk_job$baselinel$262144$65({4}SL:U),R,0,0,[r0x11B3,r0x11B4,r0x11BF,r0x11C0]
	;--cdb--S:Ltkjob.api_tk_job$lowbase$262144$65({4}SL:U),R,0,0,[r0x11BF,r0x11C0,r0x11C1,r0x11C2]
	;--cdb--S:Ltkjob.api_tk_job$ratio$262144$65({1}SC:U),R,0,0,[r0x11B5]
	;--cdb--S:G$api_tk_job$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__mullong
	.globl	_PAR
	.globl	_PADIR
	.globl	_PIOA
	.globl	_PAWK
	.globl	_PAWKDR
	.globl	_TIMERC
	.globl	_THRLD
	.globl	_RAMP0L
	.globl	_RAMP0H
	.globl	_RAMP1L
	.globl	_RAMP1H
	.globl	_PTRCL
	.globl	_PTRCH
	.globl	_ROMPL
	.globl	_ROMPH
	.globl	_BEEPC
	.globl	_FILTERGR
	.globl	_ULAWC
	.globl	_STACKL
	.globl	_STACKH
	.globl	_ADCON
	.globl	_DACON
	.globl	_SYSC
	.globl	_SPIM
	.globl	_SPIH
	.globl	_SPIMH
	.globl	_SPIOP
	.globl	_SPI_BANK
	.globl	_ADP_IND
	.globl	_ADP_VPL
	.globl	_ADP_VPH
	.globl	_ADL
	.globl	_ADH
	.globl	_ZC
	.globl	_ADCG
	.globl	_DAC_PL
	.globl	_DAC_PH
	.globl	_PAG
	.globl	_RDMAL
	.globl	_RDMAH
	.globl	_SPIL
	.globl	_IOMASK
	.globl	_IOCMP
	.globl	_IOCNT
	.globl	_LVDCON
	.globl	_LVDCTH
	.globl	_LVRCON
	.globl	_OFFSETL
	.globl	_OFFSETH
	.globl	_RCCON
	.globl	_BGCON
	.globl	_PWRL
	.globl	_CRYPT
	.globl	_PWRH
	.globl	_PWRHL
	.globl	_IROMDL
	.globl	_IROMDH
	.globl	_RECMUTE
	.globl	_SPKC
	.globl	_DCLAMP
	.globl	_SSPIC
	.globl	_SSPIL
	.globl	_SSPIM
	.globl	_SSPIH
	.globl	_PBR
	.globl	_PBDIR
	.globl	_PIOB
	.globl	_PBWK
	.globl	_PBWKDR
	.globl	_PAIE
	.globl	_PBIE
	.globl	_PAIF
	.globl	_PBIF
	.globl	_GIE
	.globl	_GIF
	.globl	_WDTL
	.globl	_WDTH
	.globl	_RPAGES
	.globl	_PPAGES
	.globl	_DMA_IL
	.globl	_FILTERGP
	.globl	_SPIDAT
	.globl	_RSPIC
	.globl	_RCLKDIV
	.globl	_PCR
	.globl	_PCDIR
	.globl	_PIOC
	.globl	_CMPCON
	.globl	_INTVC
	.globl	_INTV
	.globl	_DMICON
	.globl	_PRG_RAM
	.globl	_PDMAL
	.globl	_PDMAH
	.globl	_PDMALH
	.globl	_SPIDMAC
	.globl	_SDMAAL
	.globl	_SDMAAH
	.globl	_SDMAALH
	.globl	_ECRAL
	.globl	_ECRAH
	.globl	_ECRALH
	.globl	_ECOAL
	.globl	_ECOAH
	.globl	_ECOALH
	.globl	_ECLEN
	.globl	_ECCON
	.globl	_ECMODE
	.globl	_SPIOPRAH
	.globl	_SDMALEN
	.globl	_MULA
	.globl	_MULAL
	.globl	_MULAH
	.globl	_MULB
	.globl	_MULBL
	.globl	_MULBH
	.globl	_MULO
	.globl	_MULO1
	.globl	_MULSHIFT
	.globl	_L2UBUF
	.globl	_L2UBUFL
	.globl	_L2UBUFH
	.globl	_ULAWD
	.globl	_L2USH
	.globl	_U2LSH
	.globl	_INTPRI
	.globl	_LPWMRA
	.globl	_LPWMRAL
	.globl	_LPWMRAH
	.globl	_LPWMEN
	.globl	_LPWMINV
	.globl	_SPICK
	.globl	_SYSC2
	.globl	_HWPSEL
	.globl	_HWPAL
	.globl	_HWPAH
	.globl	_HWPA
	.globl	_HWD
	.globl	_HWDINC
	.globl	_PATEN
	.globl	_PBTEN
	.globl	_PABTEN
	.globl	_TRAMAL
	.globl	_TRAMAH
	.globl	_TRAMBUFP
	.globl	_PASKIP
	.globl	_PBSKIP
	.globl	_PABSKIP
	.globl	_PATR
	.globl	_PBTR
	.globl	_PABTR
	.globl	_TOUCHC
	.globl	_DUMMYC
	.globl	_IRCD
	.globl	_FPWMEN
	.globl	_FPWMDUTY
	.globl	_FPWMPER
	.globl	_DACGCL
	.globl	_RECPWR
	.globl	_ICE0
	.globl	_ICE1
	.globl	_ICE2
	.globl	_ICE3
	.globl	_ICE4
	.globl	_RAMP0
	.globl	_RAMP0INC
	.globl	_RAMP1
	.globl	_RDMALH
	.globl	_HWPALH
	.globl	_RAMP1INC
	.globl	_RAMP1INC2
	.globl	_ROMP
	.globl	_ROMPINC
	.globl	_ROMPINC2
	.globl	_ACC
	.globl	_RAMP0UW
	.globl	_RAMP1UW
	.globl	_ROMPLH
	.globl	_ROMPUW
	.globl	_OFFSETLH
	.globl	_ADP_VPLH
	.globl	_TOV
	.globl	_EA
	.globl	_OV
	.globl	_tch_total_ch
	.globl	_tch_mon
	.globl	_procdatap
	.globl	_rambufp1
	.globl	_baseline
	.globl	_threshold_ratio

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_lastcmd
	.globl	_cmd_now
	.globl	_extrd
	.globl	_rompsave
	.globl	_modified
	.globl	_api_tk_job_tkramp_65536_58
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
_extrd:	.ds	1

	.area DSEG(DATA)
_api_tk_job_tkramp_65536_58:	.ds	2

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_tkjob_0	udata
r0x11B0:	.ds	1
r0x11B1:	.ds	1
r0x11B2:	.ds	1
r0x11B3:	.ds	1
r0x11B4:	.ds	1
r0x11B5:	.ds	1
r0x11B6:	.ds	1
r0x11B7:	.ds	1
r0x11B8:	.ds	1
r0x11B9:	.ds	1
r0x11BA:	.ds	1
r0x11BB:	.ds	1
r0x11BC:	.ds	1
r0x11BD:	.ds	1
r0x11BF:	.ds	1
r0x11C0:	.ds	1
r0x11C1:	.ds	1
r0x11C2:	.ds	1
r0x11C3:	.ds	1
r0x11C4:	.ds	1
r0x11C5:	.ds	1
r0x11C6:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
	.globl __mullong_STK06
	.globl __mullong_STK05
	.globl __mullong_STK04
	.globl __mullong_STK03
	.globl __mullong_STK02
	.globl __mullong_STK01
	.globl __mullong_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------

	.area	IDATAROM	(CODE) ;tkjob-1-data
_lastcmd_shadow:
	.db #0x00	; 0


	.area	IDATAROM	(CODE) ;tkjob-3-data
_cmd_now_shadow:
	.db #0x00	; 0


	.area	IDATAROM	(CODE) ;tkjob-5-data
_rompsave_shadow:
	.byte #0x00,#0x00	; 0


	.area	IDATAROM	(CODE) ;tkjob-7-data
_modified_shadow:
	.db #0x00	; 0

;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------

	.area	IDATA	(DATA) ;tkjob-0-data
_lastcmd:
	.ds	1


	.area	IDATA	(DATA) ;tkjob-2-data
_cmd_now:
	.ds	1


	.area	IDATA	(DATA) ;tkjob-4-data
_rompsave:
	.ds	2


	.area	IDATA	(DATA) ;tkjob-6-data
_modified:
	.ds	1

	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x11B0:NULL+0:-1:1
	;--cdb--W:r0x11B3:NULL+0:-1:1
	;--cdb--W:r0x11BB:NULL+0:-1:1
	;--cdb--W:r0x11BF:NULL+0:-1:1
	;--cdb--W:r0x11C0:NULL+0:-1:1
	;--cdb--W:r0x11B9:NULL+0:-1:1
	;--cdb--W:r0x11C1:NULL+0:-1:1
	;--cdb--W:r0x11C2:NULL+0:-1:1
	;--cdb--W:r0x11B5:NULL+0:-1:1
	;--cdb--W:r0x11C3:NULL+0:-1:1
	;--cdb--W:r0x11C4:NULL+0:-1:1
	;--cdb--W:r0x11B4:NULL+0:-1:1
	;--cdb--W:r0x11B1:NULL+0:-1:1
	;--cdb--W:r0x11B3:NULL+0:4543:0
	;--cdb--W:r0x11B3:NULL+0:4547:0
	;--cdb--W:r0x11B3:NULL+0:4534:0
	;--cdb--W:r0x11B4:NULL+0:4544:0
	;--cdb--W:r0x11B4:NULL+0:4548:0
	;--cdb--W:r0x11B4:NULL+0:4535:0
	;--cdb--W:r0x11B5:NULL+0:4538:0
	;--cdb--W:r0x11B5:NULL+0:4547:0
	;--cdb--W:r0x11B5:NULL+0:4543:0
	;--cdb--W:r0x11B6:NULL+0:4539:0
	;--cdb--W:r0x11B6:NULL+0:4548:0
	;--cdb--W:r0x11B6:NULL+0:4533:0
	;--cdb--W:r0x11B6:NULL+0:13:0
	;--cdb--W:r0x11B6:NULL+0:4544:0
	;--cdb--W:r0x11B7:NULL+0:0:0
	;--cdb--W:r0x11B7:NULL+0:14:0
	;--cdb--W:r0x11B9:NULL+0:4540:0
	;--cdb--W:r0x11BA:NULL+0:4541:0
	;--cdb--W:r0x11BA:NULL+0:4539:0
	;--cdb--W:r0x11BC:NULL+0:4541:0
	;--cdb--W:r0x11BF:NULL+0:4536:0
	;--cdb--W:r0x11C0:NULL+0:4537:0
	;--cdb--W:r0x11C0:NULL+0:0:0
	;--cdb--W:r0x11BE:NULL+0:-1:1
	;--cdb--W:r0x11BC:NULL+0:-1:1
	;--cdb--W:r0x11BA:NULL+0:-1:1
	;--cdb--W:r0x11B7:NULL+0:-1:1
	;--cdb--W:r0x11B8:NULL+0:0:0
	;--cdb--W:r0x11B9:NULL+0:4538:0
	;--cdb--W:r0x11BB:NULL+0:4540:0
	;--cdb--W:r0x11B8:NULL+0:-1:1
	end
