
#define MSB_SET(x) (x&0x80000000L)
extern volatile unsigned char STATUS;

unsigned long
_divulong (unsigned long x, unsigned long y)
{
  unsigned long reste = 0L;
  unsigned char count = 32;

  do
  {
    // reste: x <- 0;
    if(MSB_SET(x))
    {
    x <<= 1;
    reste <<= 1;
      reste |= 1L;
    }else
    {
    x <<= 1;
    reste <<= 1;
    }

    if (reste >= y)
    {
      reste -= y;
      // x <- (result = 1)
      x |= 1L;
    }
  }
  while (--count);
  return x;
}

