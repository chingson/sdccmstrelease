;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_fsneq.c"
	.module _fsneq
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:F_fsneq$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$__fsneq$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$__fsneq$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _fsneq-code 
.globl ___fsneq

;--------------------------------------------------------
	.FUNC ___fsneq:$PNUM 8:$L:r0x1157:$L:___fsneq_STK00:$L:___fsneq_STK01:$L:___fsneq_STK02:$L:___fsneq_STK03\
:$L:___fsneq_STK04:$L:___fsneq_STK05:$L:___fsneq_STK06:$L:___fsneq_fl1_65536_21:$L:___fsneq_fl2_65536_21\

;--------------------------------------------------------
;	.line	55; "_fsneq.c"	char __fsneq (float a1, float a2) _FS_REENTRANT
___fsneq:	;Function start
	STA	r0x1157
	LDA	___fsneq_STK06
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	59; "_fsneq.c"	fl1.f = a1;
	LDA	___fsneq_STK02
	STA	___fsneq_fl1_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	___fsneq_STK01
	STA	(___fsneq_fl1_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	___fsneq_STK00
	STA	(___fsneq_fl1_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1157
	STA	(___fsneq_fl1_65536_21 + 3)
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	60; "_fsneq.c"	fl2.f = a2;
	LDA	___fsneq_STK06
	STA	___fsneq_fl2_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	___fsneq_STK05
	STA	(___fsneq_fl2_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	___fsneq_STK04
	STA	(___fsneq_fl2_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	___fsneq_STK03
	STA	(___fsneq_fl2_65536_21 + 3)
;	;.line	70; "_fsneq.c"	if (fl1.l == fl2.l)
	LDA	___fsneq_fl2_65536_21
	XOR	___fsneq_fl1_65536_21
	JNZ	_00113_DS_
	LDA	(___fsneq_fl2_65536_21 + 1)
	XOR	(___fsneq_fl1_65536_21 + 1)
	JNZ	_00113_DS_
	LDA	(___fsneq_fl2_65536_21 + 2)
	XOR	(___fsneq_fl1_65536_21 + 2)
	JNZ	_00113_DS_
	LDA	(___fsneq_fl2_65536_21 + 3)
	XOR	(___fsneq_fl1_65536_21 + 3)
_00113_DS_:
	JNZ	_00106_DS_
;	;.line	71; "_fsneq.c"	return (0);
	CLRA	
	JMP	_00107_DS_
_00106_DS_:
;	;.line	72; "_fsneq.c"	return (1);
	LDA	#0x01
_00107_DS_:
;	;.line	73; "_fsneq.c"	}
	RET	
; exit point of ___fsneq
	.ENDFUNC ___fsneq
	;--cdb--S:G$__fsneq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:L_fsneq.__fsneq$a2$65536$20({4}SF:S),R,0,0,[___fsneq_STK06,___fsneq_STK05,___fsneq_STK04,___fsneq_STK03]
	;--cdb--S:L_fsneq.__fsneq$a1$65536$20({4}SF:S),R,0,0,[___fsneq_STK02,___fsneq_STK01,___fsneq_STK00,r0x1157]
	;--cdb--S:L_fsneq.__fsneq$fl1$65536$21({4}STfloat_long:S),E,0,0
	;--cdb--S:L_fsneq.__fsneq$fl2$65536$21({4}STfloat_long:S),E,0,0
	;--cdb--S:G$__fsneq$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___fsneq
	.globl	___fsneq_fl1_65536_21
	.globl	___fsneq_fl2_65536_21
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
___fsneq_fl1_65536_21:	.ds	4

	.area DSEG(DATA)
___fsneq_fl2_65536_21:	.ds	4

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__fsneq_0	udata
r0x1157:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
___fsneq_STK00:	.ds	1
	.globl ___fsneq_STK00
___fsneq_STK01:	.ds	1
	.globl ___fsneq_STK01
___fsneq_STK02:	.ds	1
	.globl ___fsneq_STK02
___fsneq_STK03:	.ds	1
	.globl ___fsneq_STK03
___fsneq_STK04:	.ds	1
	.globl ___fsneq_STK04
___fsneq_STK05:	.ds	1
	.globl ___fsneq_STK05
___fsneq_STK06:	.ds	1
	.globl ___fsneq_STK06
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	end
