;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #bbc754a74 (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"D:\common\ms311sphlib"
;;	.file	"apivar.c"
	.module apivar
	;.list	p=MS311
	.include "ms311sfr.def"
; *** BIT REGION *** (pseudo)
	.area UDATA (DATA,REL,CON)
; *** BIT REGION ***END
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; apivar-code 
	;--cdb--S:G$api_rampage$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_endpage$0$0({2}SI:U),E,0,0
	;--cdb--S:G$api_ulaw$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_target_vol$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_spi_index$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ms311_nextrecpage$0$0({4}DA4d,SC:U),D,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_api_rampage
	.globl	_api_endpage
	.globl	_api_ulaw
	.globl	_api_target_vol
	.globl	_api_spi_index
	.globl	_ms311_nextrecpage
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
_api_rampage:	.ds	1

	.area DSEG(DATA)
_api_endpage:	.ds	2

	.area DSEG(DATA)
_api_ulaw:	.ds	1

	.area DSEG(DATA)
_api_target_vol:	.ds	1

	.area DSEG(DATA)
_api_spi_index:	.ds	1

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------

	.area	SEGC	(CODE) ;apivar-0-code

_ms311_nextrecpage:
	.db #0x01	; 1
	.db #0x02	; 2
	.db #0x03	; 3
	.db #0x01	; 1

;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------

	end
