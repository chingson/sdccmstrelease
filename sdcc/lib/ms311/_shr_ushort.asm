;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.3.0 #8604 (Sep 15 2014) (MSVC)
; This file was generated Sat Oct 11 21:41:17 2014
;--------------------------------------------------------
; MST port for the MS322 series simple CPU
;--------------------------------------------------------
	.module _shr_ushort
	;.list	p=MS205
	;.radix dec
	.include "ms326sfr.def"
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
;--------------------------------------------------------
; Code Head 
;--------------------------------------------------------
	.area CSEG	 (CODE) 
;--------------------------------------------------------
; code
;--------------------------------------------------------
; sh ushort, assembly is the source!!
; input: _PTRCL is the data byte
; input: ACC is the shift num
.FUNC	__shr_ushort
__shr_ushort:	;Function start
; 2 exit points
	sta 	_ROMPL ; temp var
	add	#0xf0
	jc	shift_too_large
	lda	_ROMPL
loop_shr:
	jnz	cont_shift
	ret
cont_shift:
	lda	STK00
	shr
	sta	STK00
	lda	_PTRCL
	ror
	sta	_PTRCL
	lda	_ROMPL
	deca
	sta	_ROMPL
	jmp	loop_shr

	
shift_too_large:
	clra
	sta	_PTRCL
	sta	STK00
	ret

; exit point of _shr_ushort


;	code size estimation:
;	   21+   10 =    31 instructions (   31 byte)

;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__shr_ushort
	.globl  STK00

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	;end
