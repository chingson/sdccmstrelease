;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_sint2fs.c"
	.module _sint2fs
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$__sint2fs$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _sint2fs-code 
.globl ___sint2fs

;--------------------------------------------------------
	.FUNC ___sint2fs:$PNUM 2:$C:___slong2fs\
:$L:r0x1155:$L:___sint2fs_STK00:$L:r0x1158:$L:r0x1159
;--------------------------------------------------------
;	.line	54; "_sint2fs.c"	float __sint2fs (signed int si) {
___sint2fs:	;Function start
	STA	r0x1155
;	;.line	55; "_sint2fs.c"	return __slong2fs(si);
	JPL	_00107_DS_
	LDA	#0xff
	JMP	_00108_DS_
_00107_DS_:
	CLRA	
_00108_DS_:
	STA	r0x1158
	STA	r0x1159
	LDA	___sint2fs_STK00
	STA	___slong2fs_STK02
	LDA	r0x1155
	STA	___slong2fs_STK01
	LDA	r0x1158
	STA	___slong2fs_STK00
	LDA	r0x1159
	CALL	___slong2fs
;	;.line	56; "_sint2fs.c"	}
	RET	
; exit point of ___sint2fs
	.ENDFUNC ___sint2fs
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:L_sint2fs.__sint2fs$si$65536$20({2}SI:S),R,0,0,[___sint2fs_STK00,r0x1155]
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	___slong2fs

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___sint2fs
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__sint2fs_0	udata
r0x1155:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
___sint2fs_STK00:	.ds	1
	.globl ___sint2fs_STK00
	.globl ___slong2fs_STK02
	.globl ___slong2fs_STK01
	.globl ___slong2fs_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1155:NULL+0:13:0
	;--cdb--W:___sint2fs_STK00:NULL+0:12:0
	;--cdb--W:r0x1156:NULL+0:4442:0
	;--cdb--W:r0x1156:NULL+0:14:0
	;--cdb--W:r0x1157:NULL+0:4437:0
	;--cdb--W:r0x1157:NULL+0:-1:1
	end
