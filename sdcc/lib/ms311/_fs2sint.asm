;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_fs2sint.c"
	.module _fs2sint
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$__fs2sint$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _fs2sint-code 
.globl ___fs2sint

;--------------------------------------------------------
	.FUNC ___fs2sint:$PNUM 4:$C:___fs2slong\
:$L:r0x1157:$L:___fs2sint_STK00:$L:___fs2sint_STK01:$L:___fs2sint_STK02
;--------------------------------------------------------
;	.line	81; "_fs2sint.c"	signed int __fs2sint (float f)
___fs2sint:	;Function start
	STA	r0x1157
;	;.line	83; "_fs2sint.c"	signed long sl=__fs2slong(f);
	LDA	___fs2sint_STK02
	STA	___fs2slong_STK02
	LDA	___fs2sint_STK01
	STA	___fs2slong_STK01
	LDA	___fs2sint_STK00
	STA	___fs2slong_STK00
	LDA	r0x1157
	CALL	___fs2slong
	STA	r0x1157
	LDA	STK02
;	;.line	84; "_fs2sint.c"	if (sl>=INT_MAX)
	INCA	
	LDA	STK01
	ADDC	#0x80
	LDA	STK00
	ADDC	#0xff
	LDA	r0x1157
	XOR	#0x80
	ADDC	#0x7f
	JNC	_00106_DS_
;	;.line	85; "_fs2sint.c"	return INT_MAX;
	LDA	#0xff
	STA	STK00
	LDA	#0x7f
	JMP	_00109_DS_
_00106_DS_:
;	;.line	86; "_fs2sint.c"	if (sl<=INT_MIN)
	SETB	_C
	CLRA	
	SUBB	STK02
	LDA	#0x80
	SUBB	STK01
	LDA	#0xff
	SUBB	STK00
	LDA	#0xff
	SUBSI	
	SUBB	r0x1157
	JNC	_00108_DS_
;	;.line	87; "_fs2sint.c"	return -INT_MIN;
	CLRA	
	STA	STK00
	LDA	#0x80
	JMP	_00109_DS_
_00108_DS_:
;	;.line	88; "_fs2sint.c"	return sl;
	LDA	STK02
	STA	STK00
	LDA	STK01
_00109_DS_:
;	;.line	89; "_fs2sint.c"	}
	RET	
; exit point of ___fs2sint
	.ENDFUNC ___fs2sint
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:L_fs2sint.__fs2sint$f$65536$20({4}SF:S),R,0,0,[___fs2sint_STK02,___fs2sint_STK01,___fs2sint_STK00,r0x1157]
	;--cdb--S:L_fs2sint.__fs2sint$sl$65536$21({4}SL:S),R,0,0,[___fs2sint_STK02,___fs2sint_STK01,___fs2sint_STK00,r0x1157]
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	___fs2slong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___fs2sint
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__fs2sint_0	udata
r0x1157:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
___fs2sint_STK00:	.ds	1
	.globl ___fs2sint_STK00
___fs2sint_STK01:	.ds	1
	.globl ___fs2sint_STK01
___fs2sint_STK02:	.ds	1
	.globl ___fs2sint_STK02
	.globl ___fs2slong_STK02
	.globl ___fs2slong_STK01
	.globl ___fs2slong_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:___fs2sint_STK00:NULL+0:14:0
	;--cdb--W:___fs2sint_STK01:NULL+0:13:0
	;--cdb--W:___fs2sint_STK02:NULL+0:12:0
	;--cdb--W:r0x1158:NULL+0:12:0
	;--cdb--W:r0x1159:NULL+0:13:0
	end
