#include <stdio.h>
#include "ms326.h"

// MS205 getchar .. dummy one
//
char getchar(void)
{
	char dat=0;
	unsigned char i;
	for(i=0;i!=8;i++)
	{
		while(!(PIOA&1));
		while(PIOA&1);
		dat=(dat<<1)|((PIOA>>1)&1);
	}
	return dat;
}
