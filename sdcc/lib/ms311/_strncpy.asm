;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_strncpy.c"
	.module _strncpy
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--F:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _strncpy-code 
.globl _strncpy

;--------------------------------------------------------
	.FUNC _strncpy:$PNUM 6:$L:r0x1155:$L:_strncpy_STK00:$L:_strncpy_STK01:$L:_strncpy_STK02:$L:_strncpy_STK03\
:$L:_strncpy_STK04:$L:r0x115A:$L:r0x115B:$L:r0x115C
;--------------------------------------------------------
;	.line	33; "_strncpy.c"	char *strncpy (char * d, char * s, size_t n )
_strncpy:	;Function start
	STA	r0x1155
;	;.line	35; "_strncpy.c"	char * d1 =  d;
	LDA	_strncpy_STK00
	STA	r0x115A
	LDA	r0x1155
	STA	r0x115B
_00106_DS_:
;	;.line	37; "_strncpy.c"	while ( n && *s )
	LDA	_strncpy_STK04
	ORA	_strncpy_STK03
	JZ	_00118_DS_
	LDA	_strncpy_STK02
	STA	_ROMPL
	LDA	_strncpy_STK01
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115C
	JZ	_00118_DS_
;	;.line	39; "_strncpy.c"	n-- ;
	LDA	_strncpy_STK04
	DECA	
	STA	_strncpy_STK04
	LDA	#0xff
	ADDC	_strncpy_STK03
	STA	_strncpy_STK03
;	;.line	40; "_strncpy.c"	*d++ = *s++ ;
	LDA	_strncpy_STK02
	INCA	
	STA	_strncpy_STK02
	CLRA	
	ADDC	_strncpy_STK01
	STA	_strncpy_STK01
	LDA	r0x1155
	STA	_ROMPH
	LDA	_strncpy_STK00
	STA	_ROMPL
	LDA	r0x115C
	STA	@_ROMPINC
	LDA	_strncpy_STK00
	INCA	
	STA	_strncpy_STK00
	CLRA	
	ADDC	r0x1155
	STA	r0x1155
	JMP	_00106_DS_
_00118_DS_:
;	;.line	42; "_strncpy.c"	while ( n-- )
	LDA	_strncpy_STK04
	STA	_strncpy_STK02
	LDA	_strncpy_STK03
	STA	_strncpy_STK01
_00109_DS_:
	LDA	_strncpy_STK02
	STA	_strncpy_STK04
	LDA	_strncpy_STK01
	STA	_strncpy_STK03
	LDA	_strncpy_STK02
	DECA	
	STA	_strncpy_STK02
	LDA	#0xff
	ADDC	_strncpy_STK01
	STA	_strncpy_STK01
	LDA	_strncpy_STK04
	ORA	_strncpy_STK03
	JZ	_00111_DS_
;	;.line	44; "_strncpy.c"	*d++ = '\0' ;
	LDA	r0x1155
	STA	_ROMPH
	LDA	_strncpy_STK00
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
	LDA	_strncpy_STK00
	INCA	
	STA	_strncpy_STK00
	CLRA	
	ADDC	r0x1155
	STA	r0x1155
	JMP	_00109_DS_
_00111_DS_:
;	;.line	46; "_strncpy.c"	return d1;
	LDA	r0x115A
	STA	STK00
	LDA	r0x115B
;	;.line	47; "_strncpy.c"	}
	RET	
; exit point of _strncpy
	.ENDFUNC _strncpy
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_strncpy.strncpy$n$65536$21({2}SI:U),R,0,0,[_strncpy_STK04,_strncpy_STK03]
	;--cdb--S:L_strncpy.strncpy$s$65536$21({2}DG,SC:U),R,0,0,[_strncpy_STK02,_strncpy_STK01]
	;--cdb--S:L_strncpy.strncpy$d$65536$21({2}DG,SC:U),R,0,0,[_strncpy_STK00,r0x1155]
	;--cdb--S:L_strncpy.strncpy$d1$65536$22({2}DG,SC:U),R,0,0,[r0x115A,r0x115B]
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_strncpy
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__strncpy_0	udata
r0x1155:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_strncpy_STK00:	.ds	1
	.globl _strncpy_STK00
_strncpy_STK01:	.ds	1
	.globl _strncpy_STK01
_strncpy_STK02:	.ds	1
	.globl _strncpy_STK02
_strncpy_STK03:	.ds	1
	.globl _strncpy_STK03
_strncpy_STK04:	.ds	1
	.globl _strncpy_STK04
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	end
