;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_divuchar.c"
	.module _divuchar
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_divuchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$_divuchar$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$_divuint$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _divuchar-code 
.globl __divuchar

;--------------------------------------------------------
	.FUNC __divuchar:$PNUM 2:$C:__divuint\
:$L:r0x1154:$L:__divuchar_STK00
;--------------------------------------------------------
;	.line	3; "_divuchar.c"	_divuchar (unsigned char x, unsigned char y)
__divuchar:	;Function start
	STA	r0x1154
;	;.line	5; "_divuchar.c"	return (unsigned char)((unsigned int)x / (unsigned int)y);
	LDA	__divuchar_STK00
	STA	__divuint_STK02
	CLRA	
	STA	__divuint_STK01
	LDA	r0x1154
	STA	__divuint_STK00
	CLRA	
	CALL	__divuint
	LDA	STK00
;	;.line	6; "_divuchar.c"	}
	RET	
; exit point of __divuchar
	.ENDFUNC __divuchar
	;--cdb--S:G$_divuchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$_divuint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_divuchar._divuchar$y$65536$1({1}SC:U),R,0,0,[__divuchar_STK00]
	;--cdb--S:L_divuchar._divuchar$x$65536$1({1}SC:U),R,0,0,[r0x1154]
	;--cdb--S:G$_divuchar$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__divuint

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__divuchar
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__divuchar_0	udata
r0x1154:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__divuchar_STK00:	.ds	1
	.globl __divuchar_STK00
	.globl __divuint_STK02
	.globl __divuint_STK01
	.globl __divuint_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:__divuchar_STK00:NULL+0:-1:1
	;--cdb--W:r0x1154:NULL+0:4441:0
	;--cdb--W:r0x1154:NULL+0:14:0
	;--cdb--W:r0x1156:NULL+0:4436:0
	;--cdb--W:r0x1156:NULL+0:14:0
	;--cdb--W:r0x1158:NULL+0:0:0
	;--cdb--W:r0x1158:NULL+0:-1:1
	;--cdb--W:r0x1157:NULL+0:0:0
	;--cdb--W:r0x1157:NULL+0:-1:1
	end
