;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_modulong.c"
	.module _modulong
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_modulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--F:G$_modulong$0$0({2}DF,SL:U),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _modulong-code 
.globl __modulong

;--------------------------------------------------------
	.FUNC __modulong:$PNUM 8:$L:r0x1157:$L:__modulong_STK00:$L:__modulong_STK01:$L:__modulong_STK02:$L:__modulong_STK03\
:$L:__modulong_STK04:$L:__modulong_STK05:$L:__modulong_STK06:$L:r0x115C:$L:r0x115D\

;--------------------------------------------------------
;	.line	5; "_modulong.c"	_modulong (unsigned long a, unsigned long b)
__modulong:	;Function start
	STA	r0x1157
;	;.line	7; "_modulong.c"	unsigned char count = 0;
	CLRA	
	STA	r0x115C
;	;.line	9; "_modulong.c"	while (!MSB_SET(b))
	STA	r0x115D
_00107_DS_:
	LDA	__modulong_STK03
	JMI	_00112_DS_
;	;.line	11; "_modulong.c"	b <<= 1;
	LDA	__modulong_STK06
	SHL	
	STA	__modulong_STK06
	LDA	__modulong_STK05
	ROL	
	STA	__modulong_STK05
	LDA	__modulong_STK04
	ROL	
	STA	__modulong_STK04
	LDA	__modulong_STK03
	ROL	
	STA	__modulong_STK03
;	;.line	12; "_modulong.c"	if (b > a)
	SETB	_C
	LDA	__modulong_STK02
	SUBB	__modulong_STK06
	LDA	__modulong_STK01
	SUBB	__modulong_STK05
	LDA	__modulong_STK00
	SUBB	__modulong_STK04
	LDA	r0x1157
	SUBB	__modulong_STK03
	JC	_00106_DS_
;	;.line	14; "_modulong.c"	b >>=1;
	LDA	__modulong_STK03
	SHR	
	STA	__modulong_STK03
	LDA	__modulong_STK04
	ROR	
	STA	__modulong_STK04
	LDA	__modulong_STK05
	ROR	
	STA	__modulong_STK05
	LDA	__modulong_STK06
	ROR	
	STA	__modulong_STK06
;	;.line	15; "_modulong.c"	break;
	JMP	_00112_DS_
_00106_DS_:
;	;.line	17; "_modulong.c"	count++;
	LDA	r0x115D
	INCA	
	STA	r0x115D
	STA	r0x115C
	JMP	_00107_DS_
_00112_DS_:
;	;.line	21; "_modulong.c"	if (a >= b)
	SETB	_C
	LDA	__modulong_STK02
	SUBB	__modulong_STK06
	LDA	__modulong_STK01
	SUBB	__modulong_STK05
	LDA	__modulong_STK00
	SUBB	__modulong_STK04
	LDA	r0x1157
	SUBB	__modulong_STK03
	JNC	_00111_DS_
;	;.line	22; "_modulong.c"	a -= b;
	SETB	_C
	LDA	__modulong_STK02
	SUBB	__modulong_STK06
	STA	__modulong_STK02
	LDA	__modulong_STK01
	SUBB	__modulong_STK05
	STA	__modulong_STK01
	LDA	__modulong_STK00
	SUBB	__modulong_STK04
	STA	__modulong_STK00
	LDA	r0x1157
	SUBB	__modulong_STK03
	STA	r0x1157
_00111_DS_:
;	;.line	23; "_modulong.c"	b >>= 1;
	LDA	__modulong_STK03
	SHR	
	STA	__modulong_STK03
	LDA	__modulong_STK04
	ROR	
	STA	__modulong_STK04
	LDA	__modulong_STK05
	ROR	
	STA	__modulong_STK05
	LDA	__modulong_STK06
	ROR	
	STA	__modulong_STK06
;	;.line	25; "_modulong.c"	while (count--);
	LDA	r0x115C
	STA	r0x115D
	LDA	r0x115C
	DECA	
	STA	r0x115C
	LDA	r0x115D
	JNZ	_00112_DS_
;	;.line	27; "_modulong.c"	return a;
	LDA	__modulong_STK02
	STA	STK02
	LDA	__modulong_STK01
	STA	STK01
	LDA	__modulong_STK00
	STA	STK00
	LDA	r0x1157
;	;.line	28; "_modulong.c"	}
	RET	
; exit point of __modulong
	.ENDFUNC __modulong
	;--cdb--S:G$_modulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:L_modulong._modulong$b$65536$1({4}SL:U),R,0,0,[__modulong_STK06,__modulong_STK05,__modulong_STK04,__modulong_STK03]
	;--cdb--S:L_modulong._modulong$a$65536$1({4}SL:U),R,0,0,[__modulong_STK02,__modulong_STK01,__modulong_STK00,r0x1157]
	;--cdb--S:L_modulong._modulong$count$65536$2({1}SC:U),R,0,0,[r0x115C]
	;--cdb--S:G$_modulong$0$0({2}DF,SL:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__modulong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__modulong_0	udata
r0x1157:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__modulong_STK00:	.ds	1
	.globl __modulong_STK00
__modulong_STK01:	.ds	1
	.globl __modulong_STK01
__modulong_STK02:	.ds	1
	.globl __modulong_STK02
__modulong_STK03:	.ds	1
	.globl __modulong_STK03
__modulong_STK04:	.ds	1
	.globl __modulong_STK04
__modulong_STK05:	.ds	1
	.globl __modulong_STK05
__modulong_STK06:	.ds	1
	.globl __modulong_STK06
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	end
