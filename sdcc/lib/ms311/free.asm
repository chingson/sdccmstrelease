;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"free.c"
	.module free
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Ffree$header[({0}S:S$next$0$0({2}DX,STheader:S),Z,0,0)({2}S:S$next_free$0$0({2}DX,STheader:S),Z,0,0)]
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$free$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--F:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0,0,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; free-code 
.globl _free

;--------------------------------------------------------
	.FUNC _free:$PNUM 2:$L:r0x1155:$L:_free_STK00:$L:r0x1156:$L:r0x1157:$L:r0x1158\
:$L:r0x1159:$L:r0x115A:$L:r0x115B:$L:r0x115D:$L:r0x115C\
:$XL:___sdcc_heap_free
;--------------------------------------------------------
;	.line	44; "free.c"	void free(void *ptr)
_free:	;Function start
	STA	r0x1155
;	;.line	49; "free.c"	if(!ptr)
	ORA	_free_STK00
;	;.line	50; "free.c"	return;
	JZ	_00117_DS_
;	;.line	52; "free.c"	prev_free = 0;
	CLRA	
	STA	r0x1156
	STA	r0x1157
;	;.line	53; "free.c"	for(h = __sdcc_heap_free, f = &__sdcc_heap_free; h && h < ptr; prev_free = h, f = &(h->next_free), h = h->next_free); // Find adjacent blocks in free list
	LDA	#(___sdcc_heap_free + 0)
	STA	_ROMPL
	LDA	#high (___sdcc_heap_free + 0)
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1158
	LDA	@_ROMPINC
	STA	r0x1159
	LDA	#(___sdcc_heap_free + 0)
	STA	r0x115A
	LDA	#high (___sdcc_heap_free + 0)
	STA	r0x115B
_00115_DS_:
	LDA	r0x1158
	ORA	r0x1159
	JZ	_00107_DS_
	SETB	_C
	LDA	r0x1158
	SUBB	_free_STK00
	LDA	r0x1159
	SUBB	r0x1155
	JC	_00107_DS_
	LDA	r0x1158
	STA	r0x1156
	LDA	r0x1159
	STA	r0x1157
	LDA	#0x02
	ADD	r0x1158
	STA	r0x115C
	CLRA	
	ADDC	r0x1159
	STA	r0x115D
	LDA	r0x115C
	STA	r0x115A
	LDA	r0x115D
	STA	r0x115B
	LDA	r0x115C
	STA	_ROMPL
	LDA	r0x115D
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1158
	LDA	@_ROMPINC
	STA	r0x1159
	JMP	_00115_DS_
_00107_DS_:
;	;.line	56; "free.c"	h = (void XDATA *)((char XDATA *)(ptr) - offsetof(struct header, next_free));
	LDA	_free_STK00
	ADD	#0xfe
	STA	_free_STK00
	LDA	#0xff
	ADDC	r0x1155
	STA	r0x1155
	LDA	_free_STK00
;	;.line	59; "free.c"	h->next_free = next_free;
	ADD	#0x02
	STA	r0x115C
	CLRA	
	ADDC	r0x1155
	STA	r0x115D
	LDA	r0x115C
	STA	_ROMPL
	LDA	r0x115D
	STA	_ROMPH
	LDA	r0x1158
	STA	@_ROMPINC
	LDA	r0x1159
	STA	@_ROMP
;	;.line	60; "free.c"	*f = h;
	LDA	r0x115A
	STA	_ROMPL
	LDA	r0x115B
	STA	_ROMPH
	LDA	_free_STK00
	STA	@_ROMPINC
	LDA	r0x1155
	STA	@_ROMP
;	;.line	62; "free.c"	if(next_free == h->next) // Merge with next block
	LDA	_free_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115A
	LDA	@_ROMPINC
	STA	r0x115B
	LDA	r0x115A
	XOR	r0x1158
	JNZ	_00169_DS_
	LDA	r0x115B
	XOR	r0x1159
_00169_DS_:
	JNZ	_00109_DS_
;	;.line	64; "free.c"	h->next_free = h->next->next_free;
	LDA	#0x02
	ADD	r0x115A
	STA	r0x115A
	CLRA	
	ADDC	r0x115B
	STA	r0x115B
	LDA	r0x115A
	STA	_ROMPL
	LDA	r0x115B
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1158
	LDA	@_ROMPINC
	STA	r0x1159
	LDA	r0x115C
	STA	_ROMPL
	LDA	r0x115D
	STA	_ROMPH
	LDA	r0x1158
	STA	@_ROMPINC
	LDA	r0x1159
	STA	@_ROMP
;	;.line	65; "free.c"	h->next = h->next->next;
	LDA	_free_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1158
	LDA	@_ROMPINC
	STA	r0x1159
	LDA	r0x1158
	STA	_ROMPL
	LDA	r0x1159
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115A
	LDA	@_ROMPINC
	STA	r0x115B
	LDA	_free_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	r0x115A
	STA	@_ROMPINC
	LDA	r0x115B
	STA	@_ROMP
_00109_DS_:
;	;.line	68; "free.c"	if (prev_free && prev_free->next == h) // Merge with previous block
	LDA	r0x1156
	ORA	r0x1157
	JZ	_00117_DS_
	LDA	r0x1156
	STA	_ROMPL
	LDA	r0x1157
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1158
	LDA	@_ROMPINC
	STA	r0x1159
	LDA	_free_STK00
	XOR	r0x1158
	JNZ	_00172_DS_
	LDA	r0x1155
	XOR	r0x1159
_00172_DS_:
	JNZ	_00117_DS_
;	;.line	70; "free.c"	prev_free->next = h->next;
	LDA	_free_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1158
	LDA	@_ROMPINC
	STA	r0x1159
	LDA	r0x1156
	STA	_ROMPL
	LDA	r0x1157
	STA	_ROMPH
	LDA	r0x1158
	STA	@_ROMPINC
	LDA	r0x1159
	STA	@_ROMP
;	;.line	71; "free.c"	prev_free->next_free = h->next_free;
	LDA	#0x02
	ADD	r0x1156
	STA	r0x1156
	CLRA	
	ADDC	r0x1157
	STA	r0x1157
	LDA	r0x115C
	STA	_ROMPL
	LDA	r0x115D
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_free_STK00
	LDA	@_ROMPINC
	STA	r0x1155
	LDA	r0x1156
	STA	_ROMPL
	LDA	r0x1157
	STA	_ROMPH
	LDA	_free_STK00
	STA	@_ROMPINC
	LDA	r0x1155
	STA	@_ROMP
_00117_DS_:
;	;.line	73; "free.c"	}
	RET	
; exit point of _free
	.ENDFUNC _free
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:Lfree.aligned_alloc$size$65536$15({2}SI:U),E,0,0
	;--cdb--S:Lfree.aligned_alloc$alignment$65536$15({2}SI:U),E,0,0
	;--cdb--S:Lfree.free$ptr$65536$29({2}DG,SV:S),R,0,0,[_free_STK00,r0x1155]
	;--cdb--S:Lfree.free$h$65536$30({2}DX,STheader:S),R,0,0,[_free_STK00,r0x1155]
	;--cdb--S:Lfree.free$next_free$65536$30({2}DX,STheader:S),R,0,0,[r0x1158,r0x1159]
	;--cdb--S:Lfree.free$prev_free$65536$30({2}DX,STheader:S),R,0,0,[r0x1156,r0x1157]
	;--cdb--S:Lfree.free$f$65536$30({2}DX,DX,STheader:S),R,0,0,[r0x115A,r0x115B]
	;--cdb--S:G$__sdcc_heap_free$0$0({2}DX,STheader:S),F,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	___sdcc_heap_free

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_free
	.globl	_free_h_65536_30
	.globl	_free_next_free_65536_30
	.globl	_free_prev_free_65536_30
	.globl	_free_f_65536_30
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
_free_h_65536_30:	.ds	2

	.area DSEG(DATA)
_free_next_free_65536_30:	.ds	2

	.area DSEG(DATA)
_free_prev_free_65536_30:	.ds	2

	.area DSEG(DATA)
_free_f_65536_30:	.ds	2

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_free_0	udata
r0x1155:	.ds	1
r0x1156:	.ds	1
r0x1157:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_free_STK00:	.ds	1
	.globl _free_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115C:NULL+0:-1:1
	;--cdb--W:r0x115D:NULL+0:4437:0
	;--cdb--W:r0x115C:NULL+0:4446:0
	end
