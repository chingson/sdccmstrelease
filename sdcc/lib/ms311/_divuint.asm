;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_divuint.c"
	.module _divuint
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_divuint$0$0({2}DF,SI:U),C,0,0
	;--cdb--F:G$_divuint$0$0({2}DF,SI:U),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _divuint-code 
.globl __divuint

;--------------------------------------------------------
	.FUNC __divuint:$PNUM 4:$L:r0x1155:$L:__divuint_STK00:$L:__divuint_STK01:$L:__divuint_STK02:$L:r0x1158\
:$L:r0x1159:$L:r0x115A:$L:r0x115B:$L:r0x115C
;--------------------------------------------------------
;	.line	8; "_divuint.c"	_divuint (unsigned int x, unsigned int y)
__divuint:	;Function start
	STA	r0x1155
;	;.line	10; "_divuint.c"	unsigned int reste = 0;
	CLRA	
	STA	r0x1158
	STA	r0x1159
;	;.line	11; "_divuint.c"	unsigned char count = 16;
	LDA	#0x10
	STA	r0x115A
_00110_DS_:
;	;.line	16; "_divuint.c"	if(x&0x8000)
	LDA	r0x1155
	JPL	_00106_DS_
;	;.line	18; "_divuint.c"	x <<= 1;
	LDA	__divuint_STK00
	SHL	
	STA	__divuint_STK00
	LDA	r0x1155
	ROL	
	STA	r0x1155
;	;.line	19; "_divuint.c"	reste <<= 1;
	LDA	r0x1158
	SHL	
	STA	r0x115B
	LDA	r0x1159
	ROL	
	STA	r0x115C
;	;.line	20; "_divuint.c"	reste |= 1;
	LDA	r0x115B
	ORA	#0x01
	STA	r0x1158
	LDA	r0x115C
	STA	r0x1159
	JMP	_00107_DS_
_00106_DS_:
;	;.line	24; "_divuint.c"	x <<= 1;
	LDA	__divuint_STK00
	SHL	
	STA	__divuint_STK00
	LDA	r0x1155
	ROL	
	STA	r0x1155
;	;.line	25; "_divuint.c"	reste <<= 1;
	LDA	r0x1158
	SHL	
	STA	r0x1158
	LDA	r0x1159
	ROL	
	STA	r0x1159
_00107_DS_:
;	;.line	28; "_divuint.c"	if (reste >= y)
	SETB	_C
	LDA	r0x1158
	SUBB	__divuint_STK02
	LDA	r0x1159
	SUBB	__divuint_STK01
	JNC	_00111_DS_
;	;.line	30; "_divuint.c"	reste -= y;
	SETB	_C
	LDA	r0x1158
	SUBB	__divuint_STK02
	STA	r0x1158
	LDA	r0x1159
	SUBB	__divuint_STK01
	STA	r0x1159
;	;.line	32; "_divuint.c"	x |= 1;
	LDA	__divuint_STK00
	ORA	#0x01
	STA	__divuint_STK00
_00111_DS_:
;	;.line	35; "_divuint.c"	while (--count);
	LDA	r0x115A
	DECA	
	STA	r0x115B
	STA	r0x115A
	LDA	r0x115B
	JNZ	_00110_DS_
;	;.line	36; "_divuint.c"	return x;
	LDA	__divuint_STK00
	STA	STK00
	LDA	r0x1155
;	;.line	37; "_divuint.c"	}
	RET	
; exit point of __divuint
	.ENDFUNC __divuint
	;--cdb--S:G$_divuint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$STATUS$0$0({1}SC:U),E,0,0
	;--cdb--S:L_divuint._divuint$y$65536$1({2}SI:U),R,0,0,[__divuint_STK02,__divuint_STK01]
	;--cdb--S:L_divuint._divuint$x$65536$1({2}SI:U),R,0,0,[__divuint_STK00,r0x1155]
	;--cdb--S:L_divuint._divuint$reste$65536$2({2}SI:U),R,0,0,[r0x115B,r0x115C]
	;--cdb--S:L_divuint._divuint$count$65536$2({1}SC:U),R,0,0,[r0x115A]
	;--cdb--S:G$_divuint$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_STATUS

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__divuint
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__divuint_0	udata
r0x1155:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__divuint_STK00:	.ds	1
	.globl __divuint_STK00
__divuint_STK01:	.ds	1
	.globl __divuint_STK01
__divuint_STK02:	.ds	1
	.globl __divuint_STK02
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	end
