;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_ltoa.c"
	.module _ltoa
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$_ultoa$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$_modulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_divulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$_ltoa$0$0({2}DF,SV:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _ltoa-code 
.globl __ltoa

;--------------------------------------------------------
	.FUNC __ltoa:$PNUM 7:$C:__ultoa\
:$L:r0x117E:$L:__ltoa_STK00:$L:__ltoa_STK01:$L:__ltoa_STK02:$L:__ltoa_STK03\
:$L:__ltoa_STK04:$L:__ltoa_STK05
;--------------------------------------------------------
;	.line	76; "_ltoa.c"	void _ltoa(long value, __xdata char* string, unsigned char radix)
__ltoa:	;Function start
	STA	r0x117E
;	;.line	78; "_ltoa.c"	if (value < 0 && radix == 10) {
	JPL	_00146_DS_
	LDA	__ltoa_STK05
	XOR	#0x0a
	JNZ	_00146_DS_
;	;.line	79; "_ltoa.c"	*string++ = '-';
	LDA	__ltoa_STK04
	STA	_ROMPL
	LDA	__ltoa_STK03
	STA	_ROMPH
	LDA	#0x2d
	STA	@_ROMP
	LDA	__ltoa_STK04
	INCA	
	STA	__ltoa_STK04
	CLRA	
	ADDC	__ltoa_STK03
	STA	__ltoa_STK03
;	;.line	80; "_ltoa.c"	value = -value;
	SETB	_C
	CLRA	
	SUBB	__ltoa_STK02
	STA	__ltoa_STK02
	CLRA	
	SUBB	__ltoa_STK01
	STA	__ltoa_STK01
	CLRA	
	SUBB	__ltoa_STK00
	STA	__ltoa_STK00
	CLRA	
	SUBB	r0x117E
	STA	r0x117E
_00146_DS_:
;	;.line	82; "_ltoa.c"	_ultoa(value, string, radix);
	LDA	__ltoa_STK05
	STA	__ultoa_STK05
	LDA	__ltoa_STK04
	STA	__ultoa_STK04
	LDA	__ltoa_STK03
	STA	__ultoa_STK03
	LDA	__ltoa_STK02
	STA	__ultoa_STK02
	LDA	__ltoa_STK01
	STA	__ultoa_STK01
	LDA	__ltoa_STK00
	STA	__ultoa_STK00
	LDA	r0x117E
	CALL	__ultoa
;	;.line	83; "_ltoa.c"	}
	RET	
; exit point of __ltoa
	.ENDFUNC __ltoa
.globl __ultoa

;--------------------------------------------------------
	.FUNC __ultoa:$PNUM 7:$C:__modulong:$C:__divulong\
:$L:r0x1157:$L:__ultoa_STK00:$L:__ultoa_STK01:$L:__ultoa_STK02:$L:__ultoa_STK03\
:$L:__ultoa_STK04:$L:__ultoa_STK05:$L:r0x115B:$L:r0x1160:$L:r0x1164\
:$L:__ultoa_buffer_65536_2:$L:r0x1165
;--------------------------------------------------------
;	.line	56; "_ltoa.c"	void _ultoa(unsigned long value, __xdata char* string, unsigned char radix)
__ultoa:	;Function start
	STA	r0x1157
;	;.line	61; "_ltoa.c"	do {
	LDA	#0x20
	STA	r0x115B
_00107_DS_:
;	;.line	62; "_ltoa.c"	unsigned char c = '0' + (value % radix);
	LDA	__ultoa_STK05
	STA	__modulong_STK06
	CLRA	
	STA	__modulong_STK05
	STA	__modulong_STK04
	STA	__modulong_STK03
	LDA	__ultoa_STK02
	STA	__modulong_STK02
	LDA	__ultoa_STK01
	STA	__modulong_STK01
	LDA	__ultoa_STK00
	STA	__modulong_STK00
	LDA	r0x1157
	CALL	__modulong
	LDA	STK02
	ADD	#0x30
	STA	r0x1160
;	;.line	63; "_ltoa.c"	if (c > (unsigned char)'9')
	SETB	_C
	LDA	#0x39
	SUBB	r0x1160
	JC	_00106_DS_
;	;.line	64; "_ltoa.c"	c += 'A' - '9' - 1;
	LDA	r0x1160
	ADD	#0x07
	STA	r0x1160
_00106_DS_:
;	;.line	65; "_ltoa.c"	buffer[--index] = c;
	LDA	r0x115B
	DECA	
	STA	r0x115B
	ADD	#(__ultoa_buffer_65536_2 + 0)
	STA	r0x1164
	CLRA	
	ADDC	#high (__ultoa_buffer_65536_2 + 0)
	STA	r0x1165
	LDA	r0x1164
	STA	_ROMPL
	LDA	r0x1165
	STA	_ROMPH
	LDA	r0x1160
	STA	@_ROMP
;	;.line	66; "_ltoa.c"	value /= radix;
	LDA	__ultoa_STK05
	STA	__divulong_STK06
	CLRA	
	STA	__divulong_STK05
	STA	__divulong_STK04
	STA	__divulong_STK03
	LDA	__ultoa_STK02
	STA	__divulong_STK02
	LDA	__ultoa_STK01
	STA	__divulong_STK01
	LDA	__ultoa_STK00
	STA	__divulong_STK00
	LDA	r0x1157
	CALL	__divulong
	STA	r0x1157
	LDA	STK00
	STA	__ultoa_STK00
	LDA	STK01
	STA	__ultoa_STK01
	LDA	STK02
	STA	__ultoa_STK02
;	;.line	67; "_ltoa.c"	} while (value);
	ORA	__ultoa_STK01
	ORA	__ultoa_STK00
	ORA	r0x1157
	JNZ	_00107_DS_
;	;.line	69; "_ltoa.c"	do {
	LDA	__ultoa_STK04
	STA	__ultoa_STK02
	LDA	__ultoa_STK03
	STA	__ultoa_STK01
	LDA	r0x115B
	STA	__ultoa_STK00
_00110_DS_:
;	;.line	70; "_ltoa.c"	*string++ = buffer[index];
	LDA	__ultoa_STK00
	ADD	#(__ultoa_buffer_65536_2 + 0)
	STA	r0x1157
	CLRA	
	ADDC	#high (__ultoa_buffer_65536_2 + 0)
	STA	__ultoa_STK04
	LDA	r0x1157
	STA	_ROMPL
	LDA	__ultoa_STK04
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	__ultoa_STK03
	LDA	__ultoa_STK02
	STA	_ROMPL
	LDA	__ultoa_STK01
	STA	_ROMPH
	LDA	__ultoa_STK03
	STA	@_ROMP
	LDA	__ultoa_STK02
	INCA	
	STA	__ultoa_STK02
	CLRA	
	ADDC	__ultoa_STK01
	STA	__ultoa_STK01
;	;.line	71; "_ltoa.c"	} while ( ++index != NUMBER_OF_DIGITS );
	LDA	__ultoa_STK00
	INCA	
	STA	__ultoa_STK00
	XOR	#0x20
	JNZ	_00110_DS_
;	;.line	73; "_ltoa.c"	*string = 0;  /* string terminator */
	LDA	__ultoa_STK02
	STA	_ROMPL
	LDA	__ultoa_STK01
	STA	_ROMPH
	CLRA	
	STA	@_ROMP
;	;.line	74; "_ltoa.c"	}
	RET	
; exit point of __ultoa
	.ENDFUNC __ultoa
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_modulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_divulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:L_ltoa._ultoa$radix$65536$1({1}SC:U),R,0,0,[__ultoa_STK05]
	;--cdb--S:L_ltoa._ultoa$string$65536$1({2}DX,SC:U),R,0,0,[__ultoa_STK04,__ultoa_STK03]
	;--cdb--S:L_ltoa._ultoa$value$65536$1({4}SL:U),R,0,0,[__ultoa_STK02,__ultoa_STK01,__ultoa_STK00,r0x1157]
	;--cdb--S:L_ltoa._ultoa$buffer$65536$2({32}DA32d,SC:U),E,0,0
	;--cdb--S:L_ltoa._ultoa$index$65536$2({1}SC:U),R,0,0,[r0x115B]
	;--cdb--S:L_ltoa._ultoa$c$131072$3({1}SC:U),R,0,0,[r0x1160]
	;--cdb--S:L_ltoa._ltoa$radix$65536$5({1}SC:U),R,0,0,[__ltoa_STK05]
	;--cdb--S:L_ltoa._ltoa$string$65536$5({2}DX,SC:U),R,0,0,[__ltoa_STK04,__ltoa_STK03]
	;--cdb--S:L_ltoa._ltoa$value$65536$5({4}SL:S),R,0,0,[__ltoa_STK02,__ltoa_STK01,__ltoa_STK00,r0x117E]
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__modulong
	.globl	__divulong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__ultoa
	.globl	__ltoa
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__ltoa_0	udata
r0x1157:	.ds	1
r0x115B:	.ds	1
r0x1160:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x117E:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__ultoa_STK00:	.ds	1
	.globl __ultoa_STK00
__ultoa_STK01:	.ds	1
	.globl __ultoa_STK01
__ultoa_STK02:	.ds	1
	.globl __ultoa_STK02
__ultoa_STK03:	.ds	1
	.globl __ultoa_STK03
__ultoa_STK04:	.ds	1
	.globl __ultoa_STK04
__ultoa_STK05:	.ds	1
	.globl __ultoa_STK05
	.globl __modulong_STK06
	.globl __modulong_STK05
	.globl __modulong_STK04
	.globl __modulong_STK03
	.globl __modulong_STK02
	.globl __modulong_STK01
	.globl __modulong_STK00
__ultoa_buffer_65536_2:	.ds	32
	.globl __divulong_STK06
	.globl __divulong_STK05
	.globl __divulong_STK04
	.globl __divulong_STK03
	.globl __divulong_STK02
	.globl __divulong_STK01
	.globl __divulong_STK00
__ltoa_STK00:	.ds	1
	.globl __ltoa_STK00
__ltoa_STK01:	.ds	1
	.globl __ltoa_STK01
__ltoa_STK02:	.ds	1
	.globl __ltoa_STK02
__ltoa_STK03:	.ds	1
	.globl __ltoa_STK03
__ltoa_STK04:	.ds	1
	.globl __ltoa_STK04
__ltoa_STK05:	.ds	1
	.globl __ltoa_STK05
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x1162:NULL+0:-1:1
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x1160:NULL+0:-1:1
	;--cdb--W:r0x1164:NULL+0:-1:1
	;--cdb--W:r0x115C:NULL+0:4459:0
	;--cdb--W:r0x1160:NULL+0:12:0
	;--cdb--W:r0x1164:NULL+0:12:0
	;--cdb--W:r0x115D:NULL+0:0:0
	;--cdb--W:r0x115D:NULL+0:-1:1
	;--cdb--W:r0x115E:NULL+0:0:0
	;--cdb--W:r0x115E:NULL+0:-1:1
	;--cdb--W:r0x115F:NULL+0:0:0
	;--cdb--W:r0x115F:NULL+0:-1:1
	end
