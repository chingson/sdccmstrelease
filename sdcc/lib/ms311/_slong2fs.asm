;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_slong2fs.c"
	.module _slong2fs
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$__slong2fs$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _slong2fs-code 
.globl ___slong2fs

;--------------------------------------------------------
	.FUNC ___slong2fs:$PNUM 4:$C:___ulong2fs\
:$L:r0x1157:$L:___slong2fs_STK00:$L:___slong2fs_STK01:$L:___slong2fs_STK02:$L:r0x1158\
:$L:r0x1159:$L:r0x115A:$L:r0x115B
;--------------------------------------------------------
;	.line	79; "_slong2fs.c"	float __slong2fs (signed long sl) {
___slong2fs:	;Function start
	STA	r0x1157
;	;.line	80; "_slong2fs.c"	if (sl<0) 
	JPL	_00106_DS_
;	;.line	81; "_slong2fs.c"	return -__ulong2fs(-sl);
	SETB	_C
	CLRA	
	SUBB	___slong2fs_STK02
	STA	r0x1158
	CLRA	
	SUBB	___slong2fs_STK01
	STA	r0x1159
	CLRA	
	SUBB	___slong2fs_STK00
	STA	r0x115A
	CLRA	
	SUBB	r0x1157
	STA	r0x115B
	LDA	r0x1158
	STA	___ulong2fs_STK02
	LDA	r0x1159
	STA	___ulong2fs_STK01
	LDA	r0x115A
	STA	___ulong2fs_STK00
	LDA	r0x115B
	CALL	___ulong2fs
	XOR	#0x80
	JMP	_00108_DS_
_00106_DS_:
;	;.line	83; "_slong2fs.c"	return __ulong2fs(sl);
	LDA	___slong2fs_STK02
	STA	___ulong2fs_STK02
	LDA	___slong2fs_STK01
	STA	___ulong2fs_STK01
	LDA	___slong2fs_STK00
	STA	___ulong2fs_STK00
	LDA	r0x1157
	CALL	___ulong2fs
_00108_DS_:
;	;.line	84; "_slong2fs.c"	}
	RET	
; exit point of ___slong2fs
	.ENDFUNC ___slong2fs
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:L_slong2fs.__slong2fs$sl$65536$20({4}SL:S),R,0,0,[___slong2fs_STK02,___slong2fs_STK01,___slong2fs_STK00,r0x1157]
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	___ulong2fs

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___slong2fs
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__slong2fs_0	udata
r0x1157:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
___slong2fs_STK00:	.ds	1
	.globl ___slong2fs_STK00
___slong2fs_STK01:	.ds	1
	.globl ___slong2fs_STK01
___slong2fs_STK02:	.ds	1
	.globl ___slong2fs_STK02
	.globl ___ulong2fs_STK02
	.globl ___ulong2fs_STK01
	.globl ___ulong2fs_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:___slong2fs_STK00:NULL+0:14:0
	;--cdb--W:___slong2fs_STK01:NULL+0:13:0
	;--cdb--W:___slong2fs_STK02:NULL+0:12:0
	;--cdb--W:r0x1158:NULL+0:12:0
	;--cdb--W:r0x1159:NULL+0:13:0
	;--cdb--W:r0x115A:NULL+0:14:0
	;--cdb--W:r0x115B:NULL+0:-1:1
	;--cdb--W:r0x1157:NULL+0:-1:1
	end
