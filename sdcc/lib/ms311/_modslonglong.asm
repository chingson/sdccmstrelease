;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_modslonglong.c"
	.module _modslonglong
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_modslonglong$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$_modslonglong$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$_modulonglong$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _modslonglong-code 
.globl __modslonglong

;--------------------------------------------------------
	.FUNC __modslonglong:$PNUM 16:$C:__modulonglong\
:$L:r0x115B:$L:__modslonglong_STK00:$L:__modslonglong_STK01:$L:__modslonglong_STK02:$L:__modslonglong_STK03\
:$L:__modslonglong_STK04:$L:__modslonglong_STK05:$L:__modslonglong_STK06:$L:__modslonglong_STK07:$L:__modslonglong_STK08\
:$L:__modslonglong_STK09:$L:__modslonglong_STK10:$L:__modslonglong_STK11:$L:__modslonglong_STK12:$L:__modslonglong_STK13\
:$L:__modslonglong_STK14:$L:r0x1164:$L:r0x1165
;--------------------------------------------------------
;	.line	36; "_modslonglong.c"	_modslonglong (long long numerator, long long denominator)
__modslonglong:	;Function start
	STA	r0x115B
;	;.line	38; "_modslonglong.c"	bool numeratorneg = (numerator < 0);
	XOR	#0x80
	ROL	
	CLRA	
	ROL	
	XOR	#0x01
	STA	r0x1164
;	;.line	39; "_modslonglong.c"	bool denominatorneg = (denominator < 0);
	LDA	__modslonglong_STK07
	XOR	#0x80
	ROL	
	CLRA	
	ROL	
	XOR	#0x01
	STA	r0x1165
;	;.line	42; "_modslonglong.c"	if (numeratorneg)
	LDA	r0x1164
	JZ	_00106_DS_
;	;.line	43; "_modslonglong.c"	numerator = -numerator;
	SETB	_C
	CLRA	
	SUBB	__modslonglong_STK06
	STA	__modslonglong_STK06
	CLRA	
	SUBB	__modslonglong_STK05
	STA	__modslonglong_STK05
	CLRA	
	SUBB	__modslonglong_STK04
	STA	__modslonglong_STK04
	CLRA	
	SUBB	__modslonglong_STK03
	STA	__modslonglong_STK03
	CLRA	
	SUBB	__modslonglong_STK02
	STA	__modslonglong_STK02
	CLRA	
	SUBB	__modslonglong_STK01
	STA	__modslonglong_STK01
	CLRA	
	SUBB	__modslonglong_STK00
	STA	__modslonglong_STK00
	CLRA	
	SUBB	r0x115B
	STA	r0x115B
_00106_DS_:
;	;.line	44; "_modslonglong.c"	if (denominatorneg)
	LDA	r0x1165
	JZ	_00108_DS_
;	;.line	45; "_modslonglong.c"	denominator = -denominator;
	SETB	_C
	CLRA	
	SUBB	__modslonglong_STK14
	STA	__modslonglong_STK14
	CLRA	
	SUBB	__modslonglong_STK13
	STA	__modslonglong_STK13
	CLRA	
	SUBB	__modslonglong_STK12
	STA	__modslonglong_STK12
	CLRA	
	SUBB	__modslonglong_STK11
	STA	__modslonglong_STK11
	CLRA	
	SUBB	__modslonglong_STK10
	STA	__modslonglong_STK10
	CLRA	
	SUBB	__modslonglong_STK09
	STA	__modslonglong_STK09
	CLRA	
	SUBB	__modslonglong_STK08
	STA	__modslonglong_STK08
	CLRA	
	SUBB	__modslonglong_STK07
	STA	__modslonglong_STK07
_00108_DS_:
;	;.line	47; "_modslonglong.c"	r = (unsigned long long)numerator % (unsigned long long)denominator;
	LDA	__modslonglong_STK14
	STA	__modulonglong_STK14
	LDA	__modslonglong_STK13
	STA	__modulonglong_STK13
	LDA	__modslonglong_STK12
	STA	__modulonglong_STK12
	LDA	__modslonglong_STK11
	STA	__modulonglong_STK11
	LDA	__modslonglong_STK10
	STA	__modulonglong_STK10
	LDA	__modslonglong_STK09
	STA	__modulonglong_STK09
	LDA	__modslonglong_STK08
	STA	__modulonglong_STK08
	LDA	__modslonglong_STK07
	STA	__modulonglong_STK07
	LDA	__modslonglong_STK06
	STA	__modulonglong_STK06
	LDA	__modslonglong_STK05
	STA	__modulonglong_STK05
	LDA	__modslonglong_STK04
	STA	__modulonglong_STK04
	LDA	__modslonglong_STK03
	STA	__modulonglong_STK03
	LDA	__modslonglong_STK02
	STA	__modulonglong_STK02
	LDA	__modslonglong_STK01
	STA	__modulonglong_STK01
	LDA	__modslonglong_STK00
	STA	__modulonglong_STK00
	LDA	r0x115B
	CALL	__modulonglong
	STA	r0x115B
;	;.line	49; "_modslonglong.c"	return ((numeratorneg ^ denominatorneg) ? -r : r);
	LDA	r0x1165
	XOR	r0x1164
	JZ	_00111_DS_
	SETB	_C
	CLRA	
	SUBB	STK06
	STA	__modslonglong_STK14
	CLRA	
	SUBB	STK05
	STA	__modslonglong_STK13
	CLRA	
	SUBB	STK04
	STA	__modslonglong_STK12
	CLRA	
	SUBB	STK03
	STA	__modslonglong_STK11
	CLRA	
	SUBB	STK02
	STA	__modslonglong_STK10
	CLRA	
	SUBB	STK01
	STA	__modslonglong_STK09
	CLRA	
	SUBB	STK00
	STA	__modslonglong_STK08
	CLRA	
	SUBB	r0x115B
	STA	__modslonglong_STK07
	JMP	_00112_DS_
_00111_DS_:
	LDA	STK06
	STA	__modslonglong_STK14
	LDA	STK05
	STA	__modslonglong_STK13
	LDA	STK04
	STA	__modslonglong_STK12
	LDA	STK03
	STA	__modslonglong_STK11
	LDA	STK02
	STA	__modslonglong_STK10
	LDA	STK01
	STA	__modslonglong_STK09
	LDA	STK00
	STA	__modslonglong_STK08
	LDA	r0x115B
	STA	__modslonglong_STK07
_00112_DS_:
	LDA	__modslonglong_STK14
	STA	STK06
	LDA	__modslonglong_STK13
	STA	STK05
	LDA	__modslonglong_STK12
	STA	STK04
	LDA	__modslonglong_STK11
	STA	STK03
	LDA	__modslonglong_STK10
	STA	STK02
	LDA	__modslonglong_STK09
	STA	STK01
	LDA	__modslonglong_STK08
	STA	STK00
	LDA	__modslonglong_STK07
;	;.line	50; "_modslonglong.c"	}
	RET	
; exit point of __modslonglong
	.ENDFUNC __modslonglong
	;--cdb--S:G$_modslonglong$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_modulonglong$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_modslonglong._modslonglong$denominator$65536$1({8}SI:S),R,0,0,[__modslonglong_STK14,__modslonglong_STK13,__modslonglong_STK12,__modslonglong_STK11__modslonglong_STK10__modslonglong_STK09__modslonglong_STK08__modslonglong_STK07]
	;--cdb--S:L_modslonglong._modslonglong$numerator$65536$1({8}SI:S),R,0,0,[__modslonglong_STK06,__modslonglong_STK05,__modslonglong_STK04,__modslonglong_STK03__modslonglong_STK02__modslonglong_STK01__modslonglong_STK00r0x115B]
	;--cdb--S:L_modslonglong._modslonglong$numeratorneg$65536$2({1}:S),R,0,0,[r0x1164]
	;--cdb--S:L_modslonglong._modslonglong$denominatorneg$65536$2({1}:S),R,0,0,[r0x1165]
	;--cdb--S:L_modslonglong._modslonglong$r$65536$2({8}SI:S),R,0,0,[__modslonglong_STK06,__modslonglong_STK05,__modslonglong_STK04,__modslonglong_STK03__modslonglong_STK02__modslonglong_STK01__modslonglong_STK00r0x115B]
	;--cdb--S:G$_modslonglong$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__modulonglong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__modslonglong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__modslonglong_0	udata
r0x115B:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__modslonglong_STK00:	.ds	1
	.globl __modslonglong_STK00
__modslonglong_STK01:	.ds	1
	.globl __modslonglong_STK01
__modslonglong_STK02:	.ds	1
	.globl __modslonglong_STK02
__modslonglong_STK03:	.ds	1
	.globl __modslonglong_STK03
__modslonglong_STK04:	.ds	1
	.globl __modslonglong_STK04
__modslonglong_STK05:	.ds	1
	.globl __modslonglong_STK05
__modslonglong_STK06:	.ds	1
	.globl __modslonglong_STK06
__modslonglong_STK07:	.ds	1
	.globl __modslonglong_STK07
__modslonglong_STK08:	.ds	1
	.globl __modslonglong_STK08
__modslonglong_STK09:	.ds	1
	.globl __modslonglong_STK09
__modslonglong_STK10:	.ds	1
	.globl __modslonglong_STK10
__modslonglong_STK11:	.ds	1
	.globl __modslonglong_STK11
__modslonglong_STK12:	.ds	1
	.globl __modslonglong_STK12
__modslonglong_STK13:	.ds	1
	.globl __modslonglong_STK13
__modslonglong_STK14:	.ds	1
	.globl __modslonglong_STK14
	.globl __modulonglong_STK14
	.globl __modulonglong_STK13
	.globl __modulonglong_STK12
	.globl __modulonglong_STK11
	.globl __modulonglong_STK10
	.globl __modulonglong_STK09
	.globl __modulonglong_STK08
	.globl __modulonglong_STK07
	.globl __modulonglong_STK06
	.globl __modulonglong_STK05
	.globl __modulonglong_STK04
	.globl __modulonglong_STK03
	.globl __modulonglong_STK02
	.globl __modulonglong_STK01
	.globl __modulonglong_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1164:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:4469:0
	;--cdb--W:__modslonglong_STK00:NULL+0:4470:0
	;--cdb--W:__modslonglong_STK00:NULL+0:14:0
	;--cdb--W:__modslonglong_STK01:NULL+0:4471:0
	;--cdb--W:__modslonglong_STK01:NULL+0:13:0
	;--cdb--W:__modslonglong_STK02:NULL+0:4472:0
	;--cdb--W:__modslonglong_STK02:NULL+0:12:0
	;--cdb--W:__modslonglong_STK03:NULL+0:4473:0
	;--cdb--W:__modslonglong_STK03:NULL+0:11:0
	;--cdb--W:__modslonglong_STK04:NULL+0:4474:0
	;--cdb--W:__modslonglong_STK04:NULL+0:10:0
	;--cdb--W:__modslonglong_STK05:NULL+0:4475:0
	;--cdb--W:__modslonglong_STK05:NULL+0:9:0
	;--cdb--W:__modslonglong_STK06:NULL+0:4476:0
	;--cdb--W:__modslonglong_STK06:NULL+0:8:0
	;--cdb--W:r0x1166:NULL+0:4468:0
	;--cdb--W:r0x1167:NULL+0:4467:0
	;--cdb--W:r0x1168:NULL+0:4466:0
	;--cdb--W:r0x1169:NULL+0:4465:0
	;--cdb--W:r0x116A:NULL+0:4464:0
	;--cdb--W:r0x116B:NULL+0:4463:0
	;--cdb--W:r0x116C:NULL+0:4462:0
	;--cdb--W:r0x116D:NULL+0:4443:0
	end
