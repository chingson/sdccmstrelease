;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"tancotf.c"
	.module tancotf
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Ftancotf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tancotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$tancotf$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; tancotf-code 
.globl _tancotf

;--------------------------------------------------------
	.FUNC _tancotf:$PNUM 5:$C:_fabsf:$C:___fslt:$C:___fsmul:$C:___fsadd\
:$C:___fs2sint:$C:___sint2fs:$C:___fssub:$C:___fsdiv\
:$L:r0x1158:$L:_tancotf_STK00:$L:_tancotf_STK01:$L:_tancotf_STK02:$L:_tancotf_STK03\
:$L:r0x115D:$L:r0x115C:$L:r0x115B:$L:r0x115A:$L:r0x115E\
:$L:r0x115F:$L:r0x1160:$L:r0x1161:$L:r0x1163:$L:r0x1162\
:$L:r0x1167:$L:r0x1166:$L:r0x1165:$L:r0x1164:$L:r0x116B\

;--------------------------------------------------------
;	.line	53; "tancotf.c"	float tancotf(float x, bool iscotan)
_tancotf:	;Function start
	STA	r0x1158
;	;.line	58; "tancotf.c"	if (fabsf(x) > YMAX)
	LDA	_tancotf_STK02
	STA	_fabsf_STK02
	LDA	_tancotf_STK01
	STA	_fabsf_STK01
	LDA	_tancotf_STK00
	STA	_fabsf_STK00
	LDA	r0x1158
	CALL	_fabsf
	STA	r0x115D
	LDA	STK02
	STA	___fslt_STK06
	LDA	STK01
	STA	___fslt_STK05
	LDA	STK00
	STA	___fslt_STK04
	LDA	r0x115D
	STA	___fslt_STK03
	CLRA	
	STA	___fslt_STK02
	LDA	#0x08
	STA	___fslt_STK01
	LDA	#0xc9
	STA	___fslt_STK00
	LDA	#0x45
	CALL	___fslt
	JZ	_00106_DS_
;	;.line	60; "tancotf.c"	errno = ERANGE;
	LDA	#0x22
	STA	_errno
;	;.line	61; "tancotf.c"	return 0.0;
	CLRA	
	STA	(_errno + 1)
	STA	STK02
	STA	STK01
	STA	STK00
	CLRA	
	JMP	_00119_DS_
_00106_DS_:
;	;.line	65; "tancotf.c"	n=(x*TWO_O_PI+(x>0.0?0.5:-0.5)); /*works for +-x*/
	LDA	_tancotf_STK02
	STA	___fsmul_STK06
	LDA	_tancotf_STK01
	STA	___fsmul_STK05
	LDA	_tancotf_STK00
	STA	___fsmul_STK04
	LDA	r0x1158
	STA	___fsmul_STK03
	LDA	#0x83
	STA	___fsmul_STK02
	LDA	#0xf9
	STA	___fsmul_STK01
	LDA	#0x22
	STA	___fsmul_STK00
	LDA	#0x3f
	CALL	___fsmul
	STA	r0x115D
	LDA	STK00
	STA	r0x115C
	LDA	STK01
	STA	r0x115B
	LDA	STK02
	STA	r0x115A
	LDA	_tancotf_STK02
	STA	___fslt_STK06
	LDA	_tancotf_STK01
	STA	___fslt_STK05
	LDA	_tancotf_STK00
	STA	___fslt_STK04
	LDA	r0x1158
	STA	___fslt_STK03
	CLRA	
	STA	___fslt_STK02
	STA	___fslt_STK01
	STA	___fslt_STK00
	CALL	___fslt
	JZ	_00121_DS_
	CLRA	
	STA	r0x115E
	STA	r0x115F
	STA	r0x1160
	LDA	#0x3f
	STA	r0x1161
	JMP	_00122_DS_
_00121_DS_:
	CLRA	
	STA	r0x115E
	STA	r0x115F
	STA	r0x1160
	LDA	#0xbf
	STA	r0x1161
_00122_DS_:
	LDA	r0x115E
	STA	___fsadd_STK06
	LDA	r0x115F
	STA	___fsadd_STK05
	LDA	r0x1160
	STA	___fsadd_STK04
	LDA	r0x1161
	STA	___fsadd_STK03
	LDA	r0x115A
	STA	___fsadd_STK02
	LDA	r0x115B
	STA	___fsadd_STK01
	LDA	r0x115C
	STA	___fsadd_STK00
	LDA	r0x115D
	CALL	___fsadd
	STA	r0x115D
	LDA	STK02
	STA	___fs2sint_STK02
	LDA	STK01
	STA	___fs2sint_STK01
	LDA	STK00
	STA	___fs2sint_STK00
	LDA	r0x115D
	CALL	___fs2sint
	STA	r0x115B
	LDA	STK00
	STA	r0x115A
;	;.line	66; "tancotf.c"	xn=n;
	STA	___sint2fs_STK00
	LDA	r0x115B
	CALL	___sint2fs
	STA	r0x115F
	LDA	STK00
	STA	r0x115E
	LDA	STK01
	STA	r0x115D
	LDA	STK02
	STA	r0x115C
;	;.line	68; "tancotf.c"	xnum=(int)x;
	LDA	_tancotf_STK02
	STA	___fs2sint_STK02
	LDA	_tancotf_STK01
	STA	___fs2sint_STK01
	LDA	_tancotf_STK00
	STA	___fs2sint_STK00
	LDA	r0x1158
	CALL	___fs2sint
	STA	r0x1161
	LDA	STK00
	STA	___sint2fs_STK00
	LDA	r0x1161
	CALL	___sint2fs
	STA	r0x1163
	LDA	STK00
	STA	r0x1162
	LDA	STK01
	STA	r0x1161
	LDA	STK02
	STA	r0x1160
;	;.line	69; "tancotf.c"	xden=x-xnum;
	STA	___fssub_STK06
	LDA	r0x1161
	STA	___fssub_STK05
	LDA	r0x1162
	STA	___fssub_STK04
	LDA	r0x1163
	STA	___fssub_STK03
	LDA	_tancotf_STK02
	STA	___fssub_STK02
	LDA	_tancotf_STK01
	STA	___fssub_STK01
	LDA	_tancotf_STK00
	STA	___fssub_STK00
	LDA	r0x1158
	CALL	___fssub
	STA	r0x1158
	LDA	STK00
	STA	_tancotf_STK00
	LDA	STK01
	STA	_tancotf_STK01
	LDA	STK02
	STA	_tancotf_STK02
;	;.line	70; "tancotf.c"	f=((xnum-xn*C1)+xden)-xn*C2;
	LDA	r0x115C
	STA	___fsmul_STK06
	LDA	r0x115D
	STA	___fsmul_STK05
	LDA	r0x115E
	STA	___fsmul_STK04
	LDA	r0x115F
	STA	___fsmul_STK03
	CLRA	
	STA	___fsmul_STK02
	STA	___fsmul_STK01
	LDA	#0xc9
	STA	___fsmul_STK00
	LDA	#0x3f
	CALL	___fsmul
	STA	r0x1167
	LDA	STK02
	STA	___fssub_STK06
	LDA	STK01
	STA	___fssub_STK05
	LDA	STK00
	STA	___fssub_STK04
	LDA	r0x1167
	STA	___fssub_STK03
	LDA	r0x1160
	STA	___fssub_STK02
	LDA	r0x1161
	STA	___fssub_STK01
	LDA	r0x1162
	STA	___fssub_STK00
	LDA	r0x1163
	CALL	___fssub
	STA	r0x1163
	LDA	_tancotf_STK02
	STA	___fsadd_STK06
	LDA	_tancotf_STK01
	STA	___fsadd_STK05
	LDA	_tancotf_STK00
	STA	___fsadd_STK04
	LDA	r0x1158
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1163
	CALL	___fsadd
	STA	r0x1158
	LDA	STK00
	STA	_tancotf_STK00
	LDA	STK01
	STA	_tancotf_STK01
	LDA	STK02
	STA	_tancotf_STK02
	LDA	r0x115C
	STA	___fsmul_STK06
	LDA	r0x115D
	STA	___fsmul_STK05
	LDA	r0x115E
	STA	___fsmul_STK04
	LDA	r0x115F
	STA	___fsmul_STK03
	LDA	#0x22
	STA	___fsmul_STK02
	LDA	#0xaa
	STA	___fsmul_STK01
	LDA	#0xfd
	STA	___fsmul_STK00
	LDA	#0x39
	CALL	___fsmul
	STA	r0x115F
	LDA	STK02
	STA	___fssub_STK06
	LDA	STK01
	STA	___fssub_STK05
	LDA	STK00
	STA	___fssub_STK04
	LDA	r0x115F
	STA	___fssub_STK03
	LDA	_tancotf_STK02
	STA	___fssub_STK02
	LDA	_tancotf_STK01
	STA	___fssub_STK01
	LDA	_tancotf_STK00
	STA	___fssub_STK00
	LDA	r0x1158
	CALL	___fssub
	STA	r0x1158
	LDA	STK00
	STA	_tancotf_STK00
	LDA	STK01
	STA	_tancotf_STK01
	LDA	STK02
	STA	_tancotf_STK02
;	;.line	72; "tancotf.c"	if (fabsf(f) < EPS)
	STA	_fabsf_STK02
	LDA	_tancotf_STK01
	STA	_fabsf_STK01
	LDA	_tancotf_STK00
	STA	_fabsf_STK00
	LDA	r0x1158
	CALL	_fabsf
	STA	r0x115F
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	LDA	#0x80
	STA	___fslt_STK04
	LDA	#0x39
	STA	___fslt_STK03
	LDA	STK02
	STA	___fslt_STK02
	LDA	STK01
	STA	___fslt_STK01
	LDA	STK00
	STA	___fslt_STK00
	LDA	r0x115F
	CALL	___fslt
	JZ	_00108_DS_
;	;.line	74; "tancotf.c"	xnum = f;
	LDA	_tancotf_STK02
	STA	r0x115C
	LDA	_tancotf_STK01
	STA	r0x115D
	LDA	_tancotf_STK00
	STA	r0x115E
	LDA	r0x1158
	STA	r0x115F
;	;.line	75; "tancotf.c"	xden = 1.0;
	CLRA	
	STA	r0x1160
	STA	r0x1161
	LDA	#0x80
	STA	r0x1162
	LDA	#0x3f
	STA	r0x1163
	JMP	_00109_DS_
_00108_DS_:
;	;.line	79; "tancotf.c"	g = f*f;
	LDA	_tancotf_STK02
	STA	___fsmul_STK06
	LDA	_tancotf_STK01
	STA	___fsmul_STK05
	LDA	_tancotf_STK00
	STA	___fsmul_STK04
	LDA	r0x1158
	STA	___fsmul_STK03
	LDA	_tancotf_STK02
	STA	___fsmul_STK02
	LDA	_tancotf_STK01
	STA	___fsmul_STK01
	LDA	_tancotf_STK00
	STA	___fsmul_STK00
	LDA	r0x1158
	CALL	___fsmul
	STA	r0x1167
	LDA	STK00
	STA	r0x1166
	LDA	STK01
	STA	r0x1165
	LDA	STK02
	STA	r0x1164
;	;.line	80; "tancotf.c"	xnum = P(f,g);
	STA	___fsmul_STK06
	LDA	r0x1165
	STA	___fsmul_STK05
	LDA	r0x1166
	STA	___fsmul_STK04
	LDA	r0x1167
	STA	___fsmul_STK03
	LDA	#0xb8
	STA	___fsmul_STK02
	LDA	#0x33
	STA	___fsmul_STK01
	LDA	#0xc4
	STA	___fsmul_STK00
	LDA	#0xbd
	CALL	___fsmul
	STA	r0x116B
	LDA	_tancotf_STK02
	STA	___fsmul_STK06
	LDA	_tancotf_STK01
	STA	___fsmul_STK05
	LDA	_tancotf_STK00
	STA	___fsmul_STK04
	LDA	r0x1158
	STA	___fsmul_STK03
	LDA	STK02
	STA	___fsmul_STK02
	LDA	STK01
	STA	___fsmul_STK01
	LDA	STK00
	STA	___fsmul_STK00
	LDA	r0x116B
	CALL	___fsmul
	STA	r0x116B
	LDA	_tancotf_STK02
	STA	___fsadd_STK06
	LDA	_tancotf_STK01
	STA	___fsadd_STK05
	LDA	_tancotf_STK00
	STA	___fsadd_STK04
	LDA	r0x1158
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x116B
	CALL	___fsadd
	STA	r0x115F
	LDA	STK00
	STA	r0x115E
	LDA	STK01
	STA	r0x115D
	LDA	STK02
	STA	r0x115C
;	;.line	81; "tancotf.c"	xden = Q(g);
	LDA	r0x1164
	STA	___fsmul_STK06
	LDA	r0x1165
	STA	___fsmul_STK05
	LDA	r0x1166
	STA	___fsmul_STK04
	LDA	r0x1167
	STA	___fsmul_STK03
	LDA	#0x75
	STA	___fsmul_STK02
	LDA	#0x33
	STA	___fsmul_STK01
	LDA	#0x1f
	STA	___fsmul_STK00
	LDA	#0x3c
	CALL	___fsmul
	STA	r0x1158
	LDA	#0xaf
	STA	___fsadd_STK06
	LDA	#0xb7
	STA	___fsadd_STK05
	LDA	#0xdb
	STA	___fsadd_STK04
	LDA	#0xbe
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1158
	CALL	___fsadd
	STA	r0x1158
	LDA	r0x1164
	STA	___fsmul_STK06
	LDA	r0x1165
	STA	___fsmul_STK05
	LDA	r0x1166
	STA	___fsmul_STK04
	LDA	r0x1167
	STA	___fsmul_STK03
	LDA	STK02
	STA	___fsmul_STK02
	LDA	STK01
	STA	___fsmul_STK01
	LDA	STK00
	STA	___fsmul_STK00
	LDA	r0x1158
	CALL	___fsmul
	STA	r0x1158
	CLRA	
	STA	___fsadd_STK06
	STA	___fsadd_STK05
	LDA	#0x80
	STA	___fsadd_STK04
	LDA	#0x3f
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1158
	CALL	___fsadd
	STA	r0x1163
	LDA	STK00
	STA	r0x1162
	LDA	STK01
	STA	r0x1161
	LDA	STK02
	STA	r0x1160
_00109_DS_:
;	;.line	84; "tancotf.c"	if(n&1)
	LDA	r0x115A
	SHR	
	JNC	_00117_DS_
;	;.line	87; "tancotf.c"	if(iscotan) return (-xnum/xden);
	LDA	_tancotf_STK03
	JZ	_00111_DS_
	LDA	r0x115F
	XOR	#0x80
	STA	r0x1158
	LDA	r0x1160
	STA	___fsdiv_STK06
	LDA	r0x1161
	STA	___fsdiv_STK05
	LDA	r0x1162
	STA	___fsdiv_STK04
	LDA	r0x1163
	STA	___fsdiv_STK03
	LDA	r0x115C
	STA	___fsdiv_STK02
	LDA	r0x115D
	STA	___fsdiv_STK01
	LDA	r0x115E
	STA	___fsdiv_STK00
	LDA	r0x1158
	CALL	___fsdiv
	JMP	_00119_DS_
_00111_DS_:
;	;.line	88; "tancotf.c"	else return (-xden/xnum);
	LDA	r0x1163
	XOR	#0x80
	STA	r0x1158
	LDA	r0x115C
	STA	___fsdiv_STK06
	LDA	r0x115D
	STA	___fsdiv_STK05
	LDA	r0x115E
	STA	___fsdiv_STK04
	LDA	r0x115F
	STA	___fsdiv_STK03
	LDA	r0x1160
	STA	___fsdiv_STK02
	LDA	r0x1161
	STA	___fsdiv_STK01
	LDA	r0x1162
	STA	___fsdiv_STK00
	LDA	r0x1158
	CALL	___fsdiv
	JMP	_00119_DS_
_00117_DS_:
;	;.line	92; "tancotf.c"	if(iscotan) return (xden/xnum);
	LDA	_tancotf_STK03
	JZ	_00114_DS_
	LDA	r0x115C
	STA	___fsdiv_STK06
	LDA	r0x115D
	STA	___fsdiv_STK05
	LDA	r0x115E
	STA	___fsdiv_STK04
	LDA	r0x115F
	STA	___fsdiv_STK03
	LDA	r0x1160
	STA	___fsdiv_STK02
	LDA	r0x1161
	STA	___fsdiv_STK01
	LDA	r0x1162
	STA	___fsdiv_STK00
	LDA	r0x1163
	CALL	___fsdiv
	JMP	_00119_DS_
_00114_DS_:
;	;.line	93; "tancotf.c"	else return (xnum/xden);
	LDA	r0x1160
	STA	___fsdiv_STK06
	LDA	r0x1161
	STA	___fsdiv_STK05
	LDA	r0x1162
	STA	___fsdiv_STK04
	LDA	r0x1163
	STA	___fsdiv_STK03
	LDA	r0x115C
	STA	___fsdiv_STK02
	LDA	r0x115D
	STA	___fsdiv_STK01
	LDA	r0x115E
	STA	___fsdiv_STK00
	LDA	r0x115F
	CALL	___fsdiv
_00119_DS_:
;	;.line	95; "tancotf.c"	}
	RET	
; exit point of _tancotf
	.ENDFUNC _tancotf
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tancotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$errno$0$0({2}SI:S),E,0,0
	;--cdb--S:Ltancotf.tancotf$iscotan$65536$25({1}:S),R,0,0,[_tancotf_STK03]
	;--cdb--S:Ltancotf.tancotf$x$65536$25({4}SF:S),R,0,0,[_tancotf_STK02,_tancotf_STK01,_tancotf_STK00,r0x1158]
	;--cdb--S:Ltancotf.tancotf$f$65536$26({4}SF:S),R,0,0,[_tancotf_STK02,_tancotf_STK01,_tancotf_STK00,r0x1158]
	;--cdb--S:Ltancotf.tancotf$g$65536$26({4}SF:S),R,0,0,[r0x1164,r0x1165,r0x1166,r0x1167]
	;--cdb--S:Ltancotf.tancotf$xn$65536$26({4}SF:S),R,0,0,[r0x115C,r0x115D,r0x115E,r0x115F]
	;--cdb--S:Ltancotf.tancotf$xnum$65536$26({4}SF:S),R,0,0,[r0x115C,r0x115D,r0x115E,r0x115F]
	;--cdb--S:Ltancotf.tancotf$xden$65536$26({4}SF:S),R,0,0,[r0x1160,r0x1161,r0x1162,r0x1163]
	;--cdb--S:Ltancotf.tancotf$n$65536$26({2}SI:S),R,0,0,[r0x115A,r0x115B]
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_fabsf
	.globl	___fslt
	.globl	___fsmul
	.globl	___fsadd
	.globl	___fs2sint
	.globl	___sint2fs
	.globl	___fssub
	.globl	___fsdiv
	.globl	_errno

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_tancotf
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_tancotf_0	udata
r0x1158:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x116B:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_tancotf_STK00:	.ds	1
	.globl _tancotf_STK00
_tancotf_STK01:	.ds	1
	.globl _tancotf_STK01
_tancotf_STK02:	.ds	1
	.globl _tancotf_STK02
_tancotf_STK03:	.ds	1
	.globl _tancotf_STK03
	.globl _fabsf_STK02
	.globl _fabsf_STK01
	.globl _fabsf_STK00
	.globl ___fslt_STK06
	.globl ___fslt_STK05
	.globl ___fslt_STK04
	.globl ___fslt_STK03
	.globl ___fslt_STK02
	.globl ___fslt_STK01
	.globl ___fslt_STK00
	.globl ___fsmul_STK06
	.globl ___fsmul_STK05
	.globl ___fsmul_STK04
	.globl ___fsmul_STK03
	.globl ___fsmul_STK02
	.globl ___fsmul_STK01
	.globl ___fsmul_STK00
	.globl ___fsadd_STK06
	.globl ___fsadd_STK05
	.globl ___fsadd_STK04
	.globl ___fsadd_STK03
	.globl ___fsadd_STK02
	.globl ___fsadd_STK01
	.globl ___fsadd_STK00
	.globl ___fs2sint_STK02
	.globl ___fs2sint_STK01
	.globl ___fs2sint_STK00
	.globl ___sint2fs_STK00
	.globl ___fssub_STK06
	.globl ___fssub_STK05
	.globl ___fssub_STK04
	.globl ___fssub_STK03
	.globl ___fssub_STK02
	.globl ___fssub_STK01
	.globl ___fssub_STK00
	.globl ___fsdiv_STK06
	.globl ___fsdiv_STK05
	.globl ___fsdiv_STK04
	.globl ___fsdiv_STK03
	.globl ___fsdiv_STK02
	.globl ___fsdiv_STK01
	.globl ___fsdiv_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115A:NULL+0:-1:1
	;--cdb--W:_tancotf_STK00:NULL+0:14:0
	;--cdb--W:_tancotf_STK00:NULL+0:4446:0
	;--cdb--W:_tancotf_STK00:NULL+0:4450:0
	;--cdb--W:_tancotf_STK01:NULL+0:13:0
	;--cdb--W:_tancotf_STK01:NULL+0:4445:0
	;--cdb--W:_tancotf_STK01:NULL+0:4449:0
	;--cdb--W:_tancotf_STK02:NULL+0:12:0
	;--cdb--W:_tancotf_STK02:NULL+0:4444:0
	;--cdb--W:_tancotf_STK02:NULL+0:4448:0
	;--cdb--W:r0x115D:NULL+0:13:0
	;--cdb--W:r0x115C:NULL+0:14:0
	;--cdb--W:r0x115C:NULL+0:12:0
	;--cdb--W:r0x115B:NULL+0:13:0
	;--cdb--W:r0x115A:NULL+0:12:0
	;--cdb--W:r0x115E:NULL+0:14:0
	;--cdb--W:r0x1160:NULL+0:14:0
	;--cdb--W:r0x1160:NULL+0:12:0
	;--cdb--W:r0x1161:NULL+0:13:0
	;--cdb--W:r0x1162:NULL+0:14:0
	;--cdb--W:r0x1166:NULL+0:14:0
	;--cdb--W:r0x1165:NULL+0:13:0
	;--cdb--W:r0x1164:NULL+0:12:0
	;--cdb--W:r0x116A:NULL+0:14:0
	;--cdb--W:r0x1169:NULL+0:13:0
	;--cdb--W:r0x1168:NULL+0:12:0
	;--cdb--W:r0x115E:NULL+0:-1:1
	;--cdb--W:r0x115C:NULL+0:-1:1
	;--cdb--W:r0x1158:NULL+0:-1:1
	end
