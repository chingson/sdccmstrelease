;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"llabs.c"
	.module llabs
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$llabs$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$llabs$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; llabs-code 
.globl _llabs

;--------------------------------------------------------
	.FUNC _llabs:$PNUM 8:$L:r0x115B:$L:_llabs_STK00:$L:_llabs_STK01:$L:_llabs_STK02:$L:_llabs_STK03\
:$L:_llabs_STK04:$L:_llabs_STK05:$L:_llabs_STK06:$L:r0x115C:$L:r0x115D\
:$L:r0x115E:$L:r0x115F:$L:r0x1160:$L:r0x1161:$L:r0x1162\
:$L:r0x1163
;--------------------------------------------------------
;	.line	2; "llabs.c"	long long int llabs(long long int j)
_llabs:	;Function start
	STA	r0x115B
;	;.line	4; "llabs.c"	return (j < 0) ? -j : j;
	JPL	_00107_DS_
	SETB	_C
	CLRA	
	SUBB	_llabs_STK06
	STA	r0x115C
	CLRA	
	SUBB	_llabs_STK05
	STA	r0x115D
	CLRA	
	SUBB	_llabs_STK04
	STA	r0x115E
	CLRA	
	SUBB	_llabs_STK03
	STA	r0x115F
	CLRA	
	SUBB	_llabs_STK02
	STA	r0x1160
	CLRA	
	SUBB	_llabs_STK01
	STA	r0x1161
	CLRA	
	SUBB	_llabs_STK00
	STA	r0x1162
	CLRA	
	SUBB	r0x115B
	STA	r0x1163
	JMP	_00108_DS_
_00107_DS_:
	LDA	_llabs_STK06
	STA	r0x115C
	LDA	_llabs_STK05
	STA	r0x115D
	LDA	_llabs_STK04
	STA	r0x115E
	LDA	_llabs_STK03
	STA	r0x115F
	LDA	_llabs_STK02
	STA	r0x1160
	LDA	_llabs_STK01
	STA	r0x1161
	LDA	_llabs_STK00
	STA	r0x1162
	LDA	r0x115B
	STA	r0x1163
_00108_DS_:
	LDA	r0x115C
	STA	STK06
	LDA	r0x115D
	STA	STK05
	LDA	r0x115E
	STA	STK04
	LDA	r0x115F
	STA	STK03
	LDA	r0x1160
	STA	STK02
	LDA	r0x1161
	STA	STK01
	LDA	r0x1162
	STA	STK00
	LDA	r0x1163
;	;.line	5; "llabs.c"	}
	RET	
; exit point of _llabs
	.ENDFUNC _llabs
	;--cdb--S:G$llabs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:Lllabs.llabs$j$65536$1({8}SI:S),R,0,0,[_llabs_STK06,_llabs_STK05,_llabs_STK04,_llabs_STK03_llabs_STK02_llabs_STK01_llabs_STK00r0x115B]
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_llabs
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_llabs_0	udata
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_llabs_STK00:	.ds	1
	.globl _llabs_STK00
_llabs_STK01:	.ds	1
	.globl _llabs_STK01
_llabs_STK02:	.ds	1
	.globl _llabs_STK02
_llabs_STK03:	.ds	1
	.globl _llabs_STK03
_llabs_STK04:	.ds	1
	.globl _llabs_STK04
_llabs_STK05:	.ds	1
	.globl _llabs_STK05
_llabs_STK06:	.ds	1
	.globl _llabs_STK06
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	end
