;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_rrslonglong.c"
	.module _rrslonglong
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_rrslonglong$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$_rrslonglong$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _rrslonglong-code 
.globl __rrslonglong

;--------------------------------------------------------
	.FUNC __rrslonglong:$PNUM 9:$C:__shr_ulong:$C:__shr_ushort\
:$L:__rrslonglong_STK00:$L:__rrslonglong_STK01:$L:__rrslonglong_STK02:$L:__rrslonglong_STK03:$L:__rrslonglong_STK04\
:$L:__rrslonglong_STK05:$L:__rrslonglong_STK06:$L:__rrslonglong_STK07:$L:r0x1154:$L:__rrslonglong_l_65536_1\
:$L:r0x1156:$L:r0x1155:$L:r0x1158:$L:r0x1157:$L:r0x115A\
:$L:r0x1159:$L:r0x115C:$L:r0x115B:$L:r0x115D:$L:r0x115F\
:$L:r0x1160:$L:r0x1161:$L:r0x1162:$L:r0x1163:$L:r0x1164\

;--------------------------------------------------------
;	.line	35; "_rrslonglong.c"	long long _rrslonglong(long long l, char s)
__rrslonglong:	;Function start
	STA	(__rrslonglong_l_65536_1 + 7)
	LDA	__rrslonglong_STK00
	STA	(__rrslonglong_l_65536_1 + 6)
	LDA	__rrslonglong_STK01
	STA	(__rrslonglong_l_65536_1 + 5)
	LDA	__rrslonglong_STK02
	STA	(__rrslonglong_l_65536_1 + 4)
	LDA	__rrslonglong_STK03
	STA	(__rrslonglong_l_65536_1 + 3)
	LDA	__rrslonglong_STK04
	STA	(__rrslonglong_l_65536_1 + 2)
	LDA	__rrslonglong_STK05
	STA	(__rrslonglong_l_65536_1 + 1)
	LDA	__rrslonglong_STK06
	STA	__rrslonglong_l_65536_1
	LDA	__rrslonglong_STK07
	STA	r0x1154
;	;.line	37; "_rrslonglong.c"	int32_t *top = (uint32_t *)((char *)(&l) + 4);
	LDA	#(__rrslonglong_l_65536_1 + 0)
	ADD	#0x04
	STA	r0x1155
	CLRA	
	ADDC	#high (__rrslonglong_l_65536_1 + 0)
	STA	r0x1158
	LDA	r0x1155
	STA	r0x1157
;	;.line	38; "_rrslonglong.c"	uint16_t *middle = (uint16_t *)((char *)(&l) + 3);
	LDA	#(__rrslonglong_l_65536_1 + 0)
	ADD	#0x03
	STA	r0x1155
	CLRA	
	ADDC	#high (__rrslonglong_l_65536_1 + 0)
	STA	r0x115A
	LDA	r0x1155
	STA	r0x1159
;	;.line	39; "_rrslonglong.c"	uint32_t *bottom = (uint32_t *)(&l);
	LDA	#high (__rrslonglong_l_65536_1 + 0)
	STA	r0x115C
	LDA	#(__rrslonglong_l_65536_1 + 0)
	STA	r0x115B
;	;.line	40; "_rrslonglong.c"	uint16_t *b = (uint16_t *)(&l);
	ADD	#0x06
	STA	r0x1155
	CLRA	
	ADDC	#high (__rrslonglong_l_65536_1 + 0)
	STA	r0x1156
_00107_DS_:
;	;.line	42; "_rrslonglong.c"	for(;s >= 16; s-= 16)
	LDA	r0x1154
	ADD	#0xf0
	JNC	_00105_DS_
;	;.line	44; "_rrslonglong.c"	b[0] = b[1];
	LDA	#0x02
	ADD	#(__rrslonglong_l_65536_1 + 0)
	STA	r0x115F
	CLRA	
	ADDC	#high (__rrslonglong_l_65536_1 + 0)
	STA	r0x1160
	LDA	r0x115F
	STA	_ROMPL
	LDA	r0x1160
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1161
	LDA	@_ROMPINC
	STA	r0x1162
	LDA	#high (__rrslonglong_l_65536_1 + 0)
	STA	_ROMPH
	LDA	#(__rrslonglong_l_65536_1 + 0)
	STA	_ROMPL
	LDA	r0x1161
	STA	@_ROMPINC
	LDA	r0x1162
	STA	@_ROMPINC
;	;.line	45; "_rrslonglong.c"	b[1] = b[2];
	LDA	#0x04
	ADD	#(__rrslonglong_l_65536_1 + 0)
	STA	r0x1161
	CLRA	
	ADDC	#high (__rrslonglong_l_65536_1 + 0)
	STA	r0x1162
	LDA	r0x1161
	STA	_ROMPL
	LDA	r0x1162
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1163
	LDA	@_ROMPINC
	STA	r0x1164
	LDA	r0x1160
	STA	_ROMPH
	LDA	r0x115F
	STA	_ROMPL
	LDA	r0x1163
	STA	@_ROMPINC
	LDA	r0x1164
	STA	@_ROMPINC
;	;.line	46; "_rrslonglong.c"	b[2] = b[3];
	LDA	r0x1155
	STA	_ROMPL
	LDA	r0x1156
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115F
	LDA	@_ROMPINC
	STA	r0x1160
	LDA	r0x1162
	STA	_ROMPH
	LDA	r0x1161
	STA	_ROMPL
	LDA	r0x115F
	STA	@_ROMPINC
	LDA	r0x1160
	STA	@_ROMPINC
;	;.line	47; "_rrslonglong.c"	b[3] = (b[3] & 0x8000) ? 0xffff : 0x000000;
	LDA	r0x1155
	STA	r0x115F
	LDA	r0x1156
	STA	r0x1160
	LDA	r0x1155
	STA	_ROMPL
	LDA	r0x1156
	STA	_ROMPH
	LDA	@_ROMPINC
	LDA	@_ROMPINC
	JPL	_00111_DS_
	LDA	#0xff
	STA	r0x1161
	STA	r0x1162
	JMP	_00112_DS_
_00111_DS_:
	CLRA	
	STA	r0x1161
	STA	r0x1162
_00112_DS_:
	LDA	r0x1160
	STA	_ROMPH
	LDA	r0x115F
	STA	_ROMPL
	LDA	r0x1161
	STA	@_ROMPINC
	LDA	r0x1162
	STA	@_ROMPINC
;	;.line	42; "_rrslonglong.c"	for(;s >= 16; s-= 16)
	LDA	r0x1154
	ADD	#0xf0
	STA	r0x1154
	JMP	_00107_DS_
_00105_DS_:
;	;.line	50; "_rrslonglong.c"	(*bottom) >>= s;
	LDA	r0x115B
	STA	_ROMPL
	LDA	r0x115C
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1155
	LDA	@_ROMPINC
	STA	r0x1156
	LDA	@_ROMPINC
	STA	r0x115D
	LDA	@_ROMPINC
	STA	STK00
	LDA	r0x115D
	STA	STK01
	LDA	r0x1156
	STA	STK02
	LDA	r0x1155
	STA	_PTRCL
	LDA	r0x1154
	CALL	__shr_ulong
	LDA	STK00
	STA	r0x1162
	LDA	STK01
	STA	r0x1161
	LDA	STK02
	STA	r0x1160
	LDA	_PTRCL
	STA	r0x115F
	LDA	r0x115C
	STA	_ROMPH
	LDA	r0x115B
	STA	_ROMPL
	LDA	r0x115F
	STA	@_ROMPINC
	LDA	r0x1160
	STA	@_ROMPINC
	LDA	r0x1161
	STA	@_ROMPINC
	LDA	r0x1162
	STA	@_ROMPINC
;	;.line	51; "_rrslonglong.c"	(*bottom) |= ((uint32_t)((*middle) >> s) << 16);
	LDA	r0x1159
	STA	_ROMPL
	LDA	r0x115A
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1155
	LDA	@_ROMPINC
	STA	STK00
	LDA	r0x1155
	STA	_PTRCL
	LDA	r0x1154
	CALL	__shr_ushort
	LDA	_PTRCL
	ORA	r0x1161
	STA	r0x1161
	LDA	STK00
	ORA	r0x1162
	STA	r0x1162
	LDA	r0x115C
	STA	_ROMPH
	LDA	r0x115B
	STA	_ROMPL
	LDA	r0x115F
	STA	@_ROMPINC
	LDA	r0x1160
	STA	@_ROMPINC
	LDA	r0x1161
	STA	@_ROMPINC
	LDA	r0x1162
	STA	@_ROMPINC
;	;.line	52; "_rrslonglong.c"	(*top) |= (((*middle) & 0xffff0000) >> s);
	LDA	r0x1157
	STA	_ROMPL
	LDA	r0x1158
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1155
	LDA	@_ROMPINC
	STA	r0x1156
	LDA	@_ROMPINC
	STA	r0x115B
	LDA	@_ROMPINC
	STA	r0x115C
	LDA	r0x1159
	STA	_ROMPL
	LDA	r0x115A
	STA	_ROMPH
	LDA	@_ROMPINC
	LDA	@_ROMPINC
	CLRA	
	STA	STK00
	STA	STK01
	STA	STK02
	STA	_PTRCL
	LDA	r0x1154
	CALL	__shr_ulong
	LDA	_PTRCL
	ORA	r0x1155
	STA	r0x1155
	LDA	STK02
	ORA	r0x1156
	STA	r0x1156
	LDA	STK01
	ORA	r0x115B
	STA	r0x115B
	LDA	STK00
	ORA	r0x115C
	STA	r0x115C
	LDA	r0x1158
	STA	_ROMPH
	LDA	r0x1157
	STA	_ROMPL
	LDA	r0x1155
	STA	@_ROMPINC
	LDA	r0x1156
	STA	@_ROMPINC
	LDA	r0x115B
	STA	@_ROMPINC
	LDA	r0x115C
	STA	@_ROMPINC
;	;.line	54; "_rrslonglong.c"	return(l);
	LDA	__rrslonglong_l_65536_1
	STA	STK06
	LDA	(__rrslonglong_l_65536_1 + 1)
	STA	STK05
	LDA	(__rrslonglong_l_65536_1 + 2)
	STA	STK04
	LDA	(__rrslonglong_l_65536_1 + 3)
	STA	STK03
	LDA	(__rrslonglong_l_65536_1 + 4)
	STA	STK02
	LDA	(__rrslonglong_l_65536_1 + 5)
	STA	STK01
	LDA	(__rrslonglong_l_65536_1 + 6)
	STA	STK00
	LDA	(__rrslonglong_l_65536_1 + 7)
;	;.line	55; "_rrslonglong.c"	}
	RET	
; exit point of __rrslonglong
	.ENDFUNC __rrslonglong
	;--cdb--S:G$_rrslonglong$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:L_rrslonglong._rrslonglong$s$65536$1({1}SC:U),R,0,0,[r0x1154]
	;--cdb--S:L_rrslonglong._rrslonglong$l$65536$1({8}SI:S),E,0,0
	;--cdb--S:L_rrslonglong._rrslonglong$top$65536$2({2}DG,SL:S),R,0,0,[r0x1157,r0x1158]
	;--cdb--S:L_rrslonglong._rrslonglong$middle$65536$2({2}DG,SI:U),R,0,0,[r0x1159,r0x115A]
	;--cdb--S:L_rrslonglong._rrslonglong$bottom$65536$2({2}DG,SL:U),R,0,0,[r0x115B,r0x115C]
	;--cdb--S:L_rrslonglong._rrslonglong$b$65536$2({2}DG,SI:U),R,0,0,[r0x115D,r0x115E]
	;--cdb--S:G$_rrslonglong$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__shr_ulong
	.globl	__shr_ushort

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__rrslonglong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__rrslonglong_0	udata
r0x1154:	.ds	1
r0x1155:	.ds	1
r0x1156:	.ds	1
r0x1157:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__rrslonglong_l_65536_1:	.ds	8
__rrslonglong_STK00:	.ds	1
	.globl __rrslonglong_STK00
__rrslonglong_STK01:	.ds	1
	.globl __rrslonglong_STK01
__rrslonglong_STK02:	.ds	1
	.globl __rrslonglong_STK02
__rrslonglong_STK03:	.ds	1
	.globl __rrslonglong_STK03
__rrslonglong_STK04:	.ds	1
	.globl __rrslonglong_STK04
__rrslonglong_STK05:	.ds	1
	.globl __rrslonglong_STK05
__rrslonglong_STK06:	.ds	1
	.globl __rrslonglong_STK06
__rrslonglong_STK07:	.ds	1
	.globl __rrslonglong_STK07
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1155:NULL+0:-1:1
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x115F:NULL+0:-1:1
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x1164:NULL+0:-1:1
	;--cdb--W:r0x1159:NULL+0:-1:1
	;--cdb--W:r0x115A:NULL+0:-1:1
	;--cdb--W:r0x1154:NULL+0:4437:0
	;--cdb--W:r0x1156:NULL+0:4446:0
	;--cdb--W:r0x1155:NULL+0:4445:0
	;--cdb--W:r0x115A:NULL+0:4446:0
	;--cdb--W:r0x115A:NULL+0:12:0
	;--cdb--W:r0x1159:NULL+0:4445:0
	;--cdb--W:r0x115E:NULL+0:4438:0
	;--cdb--W:r0x115E:NULL+0:14:0
	;--cdb--W:r0x115E:NULL+0:0:0
	;--cdb--W:r0x115D:NULL+0:4437:0
	;--cdb--W:r0x115D:NULL+0:0:0
	;--cdb--W:r0x115F:NULL+0:4437:0
	;--cdb--W:r0x115F:NULL+0:4449:0
	;--cdb--W:r0x115F:NULL+0:13:0
	;--cdb--W:r0x1160:NULL+0:4438:0
	;--cdb--W:r0x1160:NULL+0:4450:0
	;--cdb--W:r0x1160:NULL+0:14:0
	;--cdb--W:r0x1161:NULL+0:4445:0
	;--cdb--W:r0x1161:NULL+0:4447:0
	;--cdb--W:r0x1162:NULL+0:4446:0
	;--cdb--W:r0x1162:NULL+0:4448:0
	;--cdb--W:r0x1163:NULL+0:4438:0
	;--cdb--W:r0x1164:NULL+0:4443:0
	;--cdb--W:r0x1166:NULL+0:14:0
	;--cdb--W:r0x1165:NULL+0:4444:0
	;--cdb--W:r0x1156:NULL+0:-1:1
	;--cdb--W:r0x115E:NULL+0:-1:1
	;--cdb--W:r0x115D:NULL+0:-1:1
	;--cdb--W:r0x1165:NULL+0:4445:0
	;--cdb--W:r0x1162:NULL+0:-1:1
	;--cdb--W:r0x115F:NULL+0:0:0
	;--cdb--W:r0x1160:NULL+0:0:0
	;--cdb--W:r0x1160:NULL+0:-1:1
	end
