;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_ulonglong2fs.c"
	.module _ulonglong2fs
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:F_ulonglong2fs$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$__ulonglong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$__ulonglong2fs$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _ulonglong2fs-code 
.globl ___ulonglong2fs

;--------------------------------------------------------
	.FUNC ___ulonglong2fs:$PNUM 8:$L:r0x115B:$L:___ulonglong2fs_STK00:$L:___ulonglong2fs_STK01:$L:___ulonglong2fs_STK02:$L:___ulonglong2fs_STK03\
:$L:___ulonglong2fs_STK04:$L:___ulonglong2fs_STK05:$L:___ulonglong2fs_STK06:$L:r0x115C:$L:r0x115D\
:$L:r0x115E:$L:r0x115F:$L:r0x1160:$L:r0x1161:$L:___ulonglong2fs_fl_65536_21\

;--------------------------------------------------------
;	.line	83; "_ulonglong2fs.c"	float __ulonglong2fs (unsigned long long a )
___ulonglong2fs:	;Function start
	STA	r0x115B
;	;.line	89; "_ulonglong2fs.c"	if (!a)
	LDA	___ulonglong2fs_STK06
	ORA	___ulonglong2fs_STK05
	ORA	___ulonglong2fs_STK04
	ORA	___ulonglong2fs_STK03
	ORA	___ulonglong2fs_STK02
	ORA	___ulonglong2fs_STK01
	ORA	___ulonglong2fs_STK00
	ORA	r0x115B
	JNZ	_00119_DS_
;	;.line	91; "_ulonglong2fs.c"	return 0.0;
	CLRA	
	STA	STK02
	STA	STK01
	STA	STK00
	JMP	_00115_DS_
_00119_DS_:
;	;.line	94; "_ulonglong2fs.c"	while (a & NORMLL) 
	LDA	#0x96
	STA	r0x115C
	CLRA	
	STA	r0x115D
_00107_DS_:
	LDA	___ulonglong2fs_STK03
	ORA	___ulonglong2fs_STK02
	ORA	___ulonglong2fs_STK01
	ORA	___ulonglong2fs_STK00
	ORA	r0x115B
	JZ	_00109_DS_
;	;.line	97; "_ulonglong2fs.c"	a >>= 1;
	LDA	r0x115B
	SHR	
	STA	r0x115B
	LDA	___ulonglong2fs_STK00
	ROR	
	STA	___ulonglong2fs_STK00
	LDA	___ulonglong2fs_STK01
	ROR	
	STA	___ulonglong2fs_STK01
	LDA	___ulonglong2fs_STK02
	ROR	
	STA	___ulonglong2fs_STK02
	LDA	___ulonglong2fs_STK03
	ROR	
	STA	___ulonglong2fs_STK03
	LDA	___ulonglong2fs_STK04
	ROR	
	STA	___ulonglong2fs_STK04
	LDA	___ulonglong2fs_STK05
	ROR	
	STA	___ulonglong2fs_STK05
	LDA	___ulonglong2fs_STK06
	ROR	
	STA	___ulonglong2fs_STK06
;	;.line	98; "_ulonglong2fs.c"	exp++;
	LDA	r0x115C
	INCA	
	STA	r0x115C
	CLRA	
	ADDC	r0x115D
	STA	r0x115D
	JMP	_00107_DS_
_00109_DS_:
;	;.line	101; "_ulonglong2fs.c"	b=a;
	LDA	___ulonglong2fs_STK06
	STA	r0x115E
	LDA	___ulonglong2fs_STK05
	STA	r0x115F
	LDA	___ulonglong2fs_STK04
	STA	r0x1160
	LDA	___ulonglong2fs_STK03
	STA	r0x1161
;	;.line	103; "_ulonglong2fs.c"	while (b < HIDDEN)
	LDA	r0x115C
	STA	___ulonglong2fs_STK06
	LDA	r0x115D
	STA	___ulonglong2fs_STK05
_00110_DS_:
	LDA	r0x115E
	CLRB	_C
	CLRA	
	ADDC	r0x115F
	LDA	r0x1160
	ADDC	#0x80
	LDA	r0x1161
	ADDC	#0xff
	JC	_00124_DS_
;	;.line	105; "_ulonglong2fs.c"	b <<= 1;
	LDA	r0x115E
	SHL	
	STA	r0x115E
	LDA	r0x115F
	ROL	
	STA	r0x115F
	LDA	r0x1160
	ROL	
	STA	r0x1160
	LDA	r0x1161
	ROL	
	STA	r0x1161
;	;.line	106; "_ulonglong2fs.c"	exp--;
	LDA	___ulonglong2fs_STK06
	DECA	
	STA	___ulonglong2fs_STK06
	LDA	#0xff
	ADDC	___ulonglong2fs_STK05
	STA	___ulonglong2fs_STK05
	JMP	_00110_DS_
_00124_DS_:
	LDA	___ulonglong2fs_STK06
	STA	___ulonglong2fs_STK04
	LDA	___ulonglong2fs_STK05
	STA	___ulonglong2fs_STK03
;	;.line	110; "_ulonglong2fs.c"	if ((b&0x7fffff)==0x7fffff) {
	LDA	#0x7f
	AND	r0x1160
	XOR	#0x7f
	JNZ	_00157_DS_
	LDA	r0x115F
	INCA	
	JNC	_00157_DS_
	LDA	r0x115E
	INCA	
_00157_DS_:
	JNZ	_00114_DS_
;	;.line	111; "_ulonglong2fs.c"	b=0;
	CLRA	
	STA	r0x115E
	STA	r0x115F
	STA	r0x1160
	STA	r0x1161
;	;.line	112; "_ulonglong2fs.c"	exp++;
	LDA	___ulonglong2fs_STK06
	INCA	
	STA	___ulonglong2fs_STK04
	CLRA	
	ADDC	___ulonglong2fs_STK05
	STA	___ulonglong2fs_STK03
_00114_DS_:
;	;.line	116; "_ulonglong2fs.c"	b &= ~HIDDEN ;
	LDA	#0x7f
	AND	r0x1160
	STA	r0x1160
;	;.line	118; "_ulonglong2fs.c"	fl.l = PACK(0,(unsigned long)exp, b);
	LDA	___ulonglong2fs_STK03
	ROR	
	LDA	___ulonglong2fs_STK04
	ROR	
	STA	r0x115B
	CLRA	
	ROR	
	ORA	r0x1160
	STA	r0x1160
	LDA	r0x115B
	ORA	r0x1161
	STA	r0x1161
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
	LDA	r0x115E
	STA	___ulonglong2fs_fl_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	r0x115F
	STA	(___ulonglong2fs_fl_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	r0x1160
	STA	(___ulonglong2fs_fl_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1161
	STA	(___ulonglong2fs_fl_65536_21 + 3)
;	;.line	120; "_ulonglong2fs.c"	return (fl.f);
	LDA	___ulonglong2fs_fl_65536_21
	STA	STK02
	LDA	(___ulonglong2fs_fl_65536_21 + 1)
	STA	STK01
	LDA	(___ulonglong2fs_fl_65536_21 + 2)
	STA	STK00
	LDA	(___ulonglong2fs_fl_65536_21 + 3)
_00115_DS_:
;	;.line	121; "_ulonglong2fs.c"	}
	RET	
; exit point of ___ulonglong2fs
	.ENDFUNC ___ulonglong2fs
	;--cdb--S:G$__ulonglong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:L_ulonglong2fs.__ulonglong2fs$a$65536$20({8}SI:U),R,0,0,[___ulonglong2fs_STK06,___ulonglong2fs_STK05,___ulonglong2fs_STK04,___ulonglong2fs_STK03___ulonglong2fs_STK02___ulonglong2fs_STK01___ulonglong2fs_STK00r0x115B]
	;--cdb--S:L_ulonglong2fs.__ulonglong2fs$exp$65536$21({2}SI:S),R,0,0,[___ulonglong2fs_STK04,___ulonglong2fs_STK03]
	;--cdb--S:L_ulonglong2fs.__ulonglong2fs$fl$65536$21({4}STfloat_long:S),E,0,0
	;--cdb--S:L_ulonglong2fs.__ulonglong2fs$b$65536$21({4}SL:U),R,0,0,[r0x115E,r0x115F,r0x1160,r0x1161]
	;--cdb--S:G$__ulonglong2fs$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___ulonglong2fs
	.globl	___ulonglong2fs_fl_65536_21
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
___ulonglong2fs_fl_65536_21:	.ds	4

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__ulonglong2fs_0	udata
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
___ulonglong2fs_STK00:	.ds	1
	.globl ___ulonglong2fs_STK00
___ulonglong2fs_STK01:	.ds	1
	.globl ___ulonglong2fs_STK01
___ulonglong2fs_STK02:	.ds	1
	.globl ___ulonglong2fs_STK02
___ulonglong2fs_STK03:	.ds	1
	.globl ___ulonglong2fs_STK03
___ulonglong2fs_STK04:	.ds	1
	.globl ___ulonglong2fs_STK04
___ulonglong2fs_STK05:	.ds	1
	.globl ___ulonglong2fs_STK05
___ulonglong2fs_STK06:	.ds	1
	.globl ___ulonglong2fs_STK06
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:___ulonglong2fs_STK02:NULL+0:-1:1
	;--cdb--W:___ulonglong2fs_STK01:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:0:0
	;--cdb--W:___ulonglong2fs_STK01:NULL+0:4447:0
	;--cdb--W:___ulonglong2fs_STK02:NULL+0:4446:0
	;--cdb--W:___ulonglong2fs_STK03:NULL+0:0:0
	;--cdb--W:___ulonglong2fs_STK04:NULL+0:0:0
	;--cdb--W:___ulonglong2fs_STK05:NULL+0:4453:0
	;--cdb--W:___ulonglong2fs_STK06:NULL+0:4454:0
	;--cdb--W:r0x115B:NULL+0:-1:1
	;--cdb--W:___ulonglong2fs_STK03:NULL+0:-1:1
	;--cdb--W:___ulonglong2fs_STK04:NULL+0:-1:1
	;--cdb--W:___ulonglong2fs_STK00:NULL+0:-1:1
	end
