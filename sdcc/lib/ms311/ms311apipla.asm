;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #bbc754a74 (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"D:\common\ms311sphlib"
;;	.file	"ms311apipla.c"
	.module ms311apipla
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Fms311apipla$adps[({0}S:S$predict$0$0({2}SI:S),Z,0,0)({2}S:S$index$0$0({1}SC:U),Z,0,0)]
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$api_play_start$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_start_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_stop_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_b12_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
; *** BIT REGION *** (pseudo)
	.area UDATA (DATA,REL,CON)
; *** BIT REGION ***END
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; ms311apipla-code 
.globl _api_play_start

;--------------------------------------------------------
	.FUNC _api_play_start:$PNUM 8:$C:_api_clear_filter_mem:$C:_api_u2l_page:$C:_api_b12_page\
:$L:r0x1163:$L:_api_play_start_STK00:$L:_api_play_start_STK01:$L:_api_play_start_STK02:$L:_api_play_start_STK03\
:$L:_api_play_start_STK04:$L:_api_play_start_STK05:$L:_api_play_start_STK06
;--------------------------------------------------------
;	.line	15; "ms311apipla.c"	BYTE api_play_start(USHORT start_page, USHORT end_page, USHORT period, BYTE ulaw, BYTE dacosr)
_api_play_start:	;Function start
	STA	r0x1163
	LDA	_api_play_start_STK06
;	;.line	17; "ms311apipla.c"	api_clear_filter_mem();
	CALL	_api_clear_filter_mem
;	;.line	18; "ms311apipla.c"	SPIH=start_page>>8;
	LDA	r0x1163
	STA	_SPIH
;	;.line	19; "ms311apipla.c"	SPIM=start_page&0xff;
	LDA	_api_play_start_STK00
	STA	_SPIM
;	;.line	20; "ms311apipla.c"	SPIL=0;
	CLRA	
	STA	_SPIL
;	;.line	21; "ms311apipla.c"	api_endpage=end_page;
	LDA	_api_play_start_STK02
	STA	_api_endpage
	LDA	_api_play_start_STK01
	STA	(_api_endpage + 1)
;	;.line	22; "ms311apipla.c"	api_ulaw=ulaw;
	LDA	_api_play_start_STK05
	STA	_api_ulaw
;	;.line	24; "ms311apipla.c"	SPIOP=0x10; // clear end-code
	LDA	#0x10
	STA	_SPIOP
;	;.line	25; "ms311apipla.c"	DMAH=0x40;
	LDA	#0x40
	STA	_DMAH
;	;.line	26; "ms311apipla.c"	ULAWC&=0x3f;
	DECA	
	AND	_ULAWC
	STA	_ULAWC
;	;.line	27; "ms311apipla.c"	ADCON=0x40;
	LDA	#0x40
	STA	_ADCON
;	;.line	28; "ms311apipla.c"	DAC_PH=period>>8;
	LDA	_api_play_start_STK03
	STA	_DAC_PH
;	;.line	29; "ms311apipla.c"	DAC_PL=period&0xff;
	LDA	_api_play_start_STK04
	STA	_DAC_PL
;	;.line	30; "ms311apipla.c"	OFFSETLH&=0x000f;
	LDA	_OFFSETLH
	AND	#0x0f
	STA	_OFFSETLH
	CLRA	
	STA	(_OFFSETLH + 1)
;	;.line	31; "ms311apipla.c"	ulaw &=0x7f;
	LDA	#0x7f
	AND	_api_play_start_STK05
	STA	_api_play_start_STK00
;	;.line	32; "ms311apipla.c"	if(ulaw==1)
	XOR	#0x01
	JNZ	_00115_DS_
;	;.line	34; "ms311apipla.c"	SPIOP=0x48; // read to 0x1xx
	LDA	#0x48
	STA	_SPIOP
;	;.line	35; "ms311apipla.c"	SPIOP=0x20;
	LDA	#0x20
	STA	_SPIOP
;	;.line	36; "ms311apipla.c"	if(SPIOP&0x80)
	LDA	_SPIOP
	JPL	_00106_DS_
;	;.line	39; "ms311apipla.c"	return 0; // fail
	CLRA	
	JMP	_00120_DS_
_00106_DS_:
;	;.line	41; "ms311apipla.c"	api_u2l_page(0);
	CLRA	
	CALL	_api_u2l_page
;	;.line	42; "ms311apipla.c"	api_u2l_page(1);
	LDA	#0x01
	CALL	_api_u2l_page
;	;.line	47; "ms311apipla.c"	DACON=dacosr|0x1a;
	LDA	_api_play_start_STK06
	ORA	#0x1a
	STA	_DACON
	JMP	_00117_DS_
_00115_DS_:
;	;.line	49; "ms311apipla.c"	}else if(ulaw==2)
	LDA	_api_play_start_STK00
	XOR	#0x02
	JNZ	_00112_DS_
;	;.line	51; "ms311apipla.c"	api_spi_index=0;
	CLRA	
	STA	_api_spi_index
;	;.line	52; "ms311apipla.c"	api_b12_page(0);
	CALL	_api_b12_page
;	;.line	53; "ms311apipla.c"	api_b12_page(1);
	LDA	#0x01
	CALL	_api_b12_page
;	;.line	54; "ms311apipla.c"	if(SPIMH>=api_endpage)
	SETB	_C
	LDA	_SPIMH
	SUBB	_api_endpage
	LDA	(_SPIMH + 1)
	SUBB	(_api_endpage + 1)
	JNC	_00108_DS_
;	;.line	55; "ms311apipla.c"	return 0;
	CLRA	
	JMP	_00120_DS_
_00108_DS_:
;	;.line	56; "ms311apipla.c"	DACON=dacosr|0x1a;
	LDA	_api_play_start_STK06
	ORA	#0x1a
	STA	_DACON
	JMP	_00117_DS_
_00112_DS_:
;	;.line	60; "ms311apipla.c"	SPIOP=0x88; // read page to 0x2xx
	LDA	#0x88
	STA	_SPIOP
;	;.line	61; "ms311apipla.c"	SPIOP=0x20; // address increase a page
	LDA	#0x20
	STA	_SPIOP
;	;.line	62; "ms311apipla.c"	SPIOP=0xc8; // read page to 0x3xx
	LDA	#0xc8
	STA	_SPIOP
;	;.line	63; "ms311apipla.c"	if(SPIOP&0x80)
	LDA	_SPIOP
	JPL	_00110_DS_
;	;.line	65; "ms311apipla.c"	return 0;
	CLRA	
	JMP	_00120_DS_
_00110_DS_:
;	;.line	67; "ms311apipla.c"	DACON=dacosr|0x3a;
	LDA	_api_play_start_STK06
	ORA	#0x3a
	STA	_DACON
;	;.line	68; "ms311apipla.c"	SPIOP=0x20;    		
	LDA	#0x20
	STA	_SPIOP
_00117_DS_:
;	;.line	71; "ms311apipla.c"	while(DMAL==0);
	LDA	_DMAL
	JZ	_00117_DS_
;	;.line	72; "ms311apipla.c"	DACON|=1; // enable PA finally
	LDA	_DACON
	ORA	#0x01
	STA	_DACON
;	;.line	73; "ms311apipla.c"	api_rampage=2; // 2xx~3xx
	LDA	#0x02
	STA	_api_rampage
;	;.line	74; "ms311apipla.c"	return 1;
	DECA	
_00120_DS_:
;	;.line	76; "ms311apipla.c"	}
	RET	
; exit point of _api_play_start
	.ENDFUNC _api_play_start
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_start_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_stop_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_b12_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$IOR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IODIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IO$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0LH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1LH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAHL$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$init_io_state$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_endpage$0$0({2}SI:U),E,0,0
	;--cdb--S:G$api_ulaw$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_rampage$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_spi_index$0$0({1}SC:U),E,0,0
	;--cdb--S:Lms311apipla.api_play_start$dacosr$65536$39({1}SC:U),R,0,0,[_api_play_start_STK06]
	;--cdb--S:Lms311apipla.api_play_start$ulaw$65536$39({1}SC:U),R,0,0,[_api_play_start_STK05]
	;--cdb--S:Lms311apipla.api_play_start$period$65536$39({2}SI:U),R,0,0,[_api_play_start_STK04,_api_play_start_STK03]
	;--cdb--S:Lms311apipla.api_play_start$end_page$65536$39({2}SI:U),R,0,0,[_api_play_start_STK02,_api_play_start_STK01]
	;--cdb--S:Lms311apipla.api_play_start$start_page$65536$39({2}SI:U),R,0,0,[_api_play_start_STK00,r0x1163]
	;--cdb--S:G$u2l_high$0$0({256}DA256d,SC:U),D,0,0
	;--cdb--S:G$u2l_low$0$0({256}DA256d,SC:U),D,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_api_clear_filter_mem
	.globl	_api_u2l_page
	.globl	_api_b12_page
	.globl	_IOR
	.globl	_IODIR
	.globl	_IO
	.globl	_IOWK
	.globl	_IOWKDR
	.globl	_TIMERC
	.globl	_THRLD
	.globl	_RAMP0L
	.globl	_RAMP0H
	.globl	_RAMP1L
	.globl	_RAMP1H
	.globl	_PTRCL
	.globl	_PTRCH
	.globl	_ROMPL
	.globl	_ROMPH
	.globl	_BEEPC
	.globl	_FILTERG
	.globl	_ULAWC
	.globl	_STACKL
	.globl	_STACKH
	.globl	_ADCON
	.globl	_DACON
	.globl	_SYSC
	.globl	_SPIM
	.globl	_SPIH
	.globl	_SPIOP
	.globl	_SPI_BANK
	.globl	_ADP_IND
	.globl	_ADP_VPL
	.globl	_ADP_VPH
	.globl	_ADL
	.globl	_ADH
	.globl	_ZC
	.globl	_ADCG
	.globl	_DAC_PL
	.globl	_DAC_PH
	.globl	_PAG
	.globl	_DMAL
	.globl	_DMAH
	.globl	_SPIL
	.globl	_IOMASK
	.globl	_IOCMP
	.globl	_IOCNT
	.globl	_LVDCON
	.globl	_LVRCON
	.globl	_OFFSETL
	.globl	_OFFSETH
	.globl	_RCCON
	.globl	_BGCON
	.globl	_PWRL
	.globl	_CRYPT
	.globl	_PWRH
	.globl	_PWRHL
	.globl	_IROMDL
	.globl	_IROMDH
	.globl	_IROMDLH
	.globl	_RECMUTE
	.globl	_SPKC
	.globl	_DCLAMP
	.globl	_SSPIC
	.globl	_SSPIL
	.globl	_SSPIM
	.globl	_SSPIH
	.globl	_RAMP0
	.globl	_RAMP0LH
	.globl	_RAMP1LH
	.globl	_RAMP0INC
	.globl	_RAMP1
	.globl	_DMAHL
	.globl	_RAMP1INC
	.globl	_RAMP1INC2
	.globl	_ROMP
	.globl	_ROMPINC
	.globl	_ROMPLH
	.globl	_ROMPINC2
	.globl	_ACC
	.globl	_RAMP0UW
	.globl	_RAMP1UW
	.globl	_ROMPUW
	.globl	_SPIMH
	.globl	_OFFSETLH
	.globl	_ADP_VPLH
	.globl	_ICE0
	.globl	_ICE1
	.globl	_ICE2
	.globl	_ICE3
	.globl	_ICE4
	.globl	_TOV
	.globl	_init_io_state
	.globl	_api_endpage
	.globl	_api_ulaw
	.globl	_api_rampage
	.globl	_api_spi_index
	.globl	_u2l_high
	.globl	_u2l_low

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_api_play_start
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_ms311apipla_0	udata
r0x1163:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_api_play_start_STK00:	.ds	1
	.globl _api_play_start_STK00
_api_play_start_STK01:	.ds	1
	.globl _api_play_start_STK01
_api_play_start_STK02:	.ds	1
	.globl _api_play_start_STK02
_api_play_start_STK03:	.ds	1
	.globl _api_play_start_STK03
_api_play_start_STK04:	.ds	1
	.globl _api_play_start_STK04
_api_play_start_STK05:	.ds	1
	.globl _api_play_start_STK05
_api_play_start_STK06:	.ds	1
	.globl _api_play_start_STK06
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x116B:NULL+0:-1:1
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:_api_play_start_STK03:NULL+0:-1:1
	;--cdb--W:_api_play_start_STK00:NULL+0:4463:0
	;--cdb--W:r0x116A:NULL+0:-1:1
	;--cdb--W:_api_play_start_STK04:NULL+0:-1:1
	;--cdb--W:_api_play_start_STK03:NULL+0:0:0
	;--cdb--W:_api_play_start_STK00:NULL+0:-1:1
	end
