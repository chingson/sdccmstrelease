;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_strtok.c"
	.module _strtok
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--F:G$strtok$0$0({2}DF,DG,SC:U),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _strtok-code 
.globl _strtok

;--------------------------------------------------------
	.FUNC _strtok:$PNUM 4:$C:_strchr\
:$L:r0x1156:$L:_strtok_STK00:$L:_strtok_STK01:$L:_strtok_STK02
;--------------------------------------------------------
;	.line	36; "_strtok.c"	char * strtok (char * str, char * control )
_strtok:	;Function start
	STA	r0x1156
;	;.line	41; "_strtok.c"	if ( str )
	ORA	_strtok_STK00
	JZ	_00106_DS_
;	;.line	42; "_strtok.c"	s = str;
	LDA	_strtok_STK00
	STA	_strtok_s_65536_22
	LDA	r0x1156
	STA	(_strtok_s_65536_22 + 1)
_00106_DS_:
;	;.line	43; "_strtok.c"	if ( !s )
	LDA	_strtok_s_65536_22
	ORA	(_strtok_s_65536_22 + 1)
	JNZ	_00112_DS_
;	;.line	44; "_strtok.c"	return NULL;
	CLRA	
	STA	STK00
	JMP	_00123_DS_
_00112_DS_:
;	;.line	46; "_strtok.c"	while (*s) {
	LDA	_strtok_s_65536_22
	STA	_ROMPL
	LDA	(_strtok_s_65536_22 + 1)
	STA	_ROMPH
	LDA	@_ROMPINC
	JZ	_00114_DS_
;	;.line	47; "_strtok.c"	if (strchr(control,*s))
	LDA	_strtok_s_65536_22
	STA	_ROMPL
	LDA	(_strtok_s_65536_22 + 1)
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_strchr_STK01
	LDA	_strtok_STK02
	STA	_strchr_STK00
	LDA	_strtok_STK01
	CALL	_strchr
	ORA	STK00
	JZ	_00114_DS_
;	;.line	48; "_strtok.c"	s++;
	LDA	_strtok_s_65536_22
	INCA	
	STA	_strtok_s_65536_22
	CLRA	
	ADDC	(_strtok_s_65536_22 + 1)
	STA	(_strtok_s_65536_22 + 1)
	JMP	_00112_DS_
_00114_DS_:
;	;.line	53; "_strtok.c"	s1 = s;
	LDA	_strtok_s_65536_22
	STA	_strtok_STK00
	LDA	(_strtok_s_65536_22 + 1)
	STA	r0x1156
_00117_DS_:
;	;.line	55; "_strtok.c"	while (*s) {
	LDA	_strtok_s_65536_22
	STA	_ROMPL
	LDA	(_strtok_s_65536_22 + 1)
	STA	_ROMPH
	LDA	@_ROMPINC
	JZ	_00119_DS_
;	;.line	56; "_strtok.c"	if (strchr(control,*s)) {
	LDA	_strtok_s_65536_22
	STA	_ROMPL
	LDA	(_strtok_s_65536_22 + 1)
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_strchr_STK01
	LDA	_strtok_STK02
	STA	_strchr_STK00
	LDA	_strtok_STK01
	CALL	_strchr
	ORA	STK00
	JZ	_00116_DS_
;	;.line	57; "_strtok.c"	*s++ = '\0';
	LDA	(_strtok_s_65536_22 + 1)
	STA	_ROMPH
	LDA	_strtok_s_65536_22
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
	LDA	_strtok_s_65536_22
	INCA	
	STA	_strtok_s_65536_22
	CLRA	
	ADDC	(_strtok_s_65536_22 + 1)
	STA	(_strtok_s_65536_22 + 1)
;	;.line	58; "_strtok.c"	return s1;
	LDA	_strtok_STK00
	STA	STK00
	LDA	r0x1156
	JMP	_00123_DS_
_00116_DS_:
;	;.line	60; "_strtok.c"	s++ ;
	LDA	_strtok_s_65536_22
	INCA	
	STA	_strtok_s_65536_22
	CLRA	
	ADDC	(_strtok_s_65536_22 + 1)
	STA	(_strtok_s_65536_22 + 1)
	JMP	_00117_DS_
_00119_DS_:
;	;.line	63; "_strtok.c"	s = NULL;
	CLRA	
	STA	_strtok_s_65536_22
	STA	(_strtok_s_65536_22 + 1)
;	;.line	65; "_strtok.c"	if (*s1)
	LDA	_strtok_STK00
	STA	_ROMPL
	LDA	r0x1156
	STA	_ROMPH
	LDA	@_ROMPINC
	JZ	_00121_DS_
;	;.line	66; "_strtok.c"	return s1;
	LDA	_strtok_STK00
	STA	STK00
	LDA	r0x1156
	JMP	_00123_DS_
_00121_DS_:
;	;.line	68; "_strtok.c"	return NULL;
	CLRA	
	STA	STK00
_00123_DS_:
;	;.line	69; "_strtok.c"	}
	RET	
; exit point of _strtok
	.ENDFUNC _strtok
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_strtok.strtok$control$65536$21({2}DG,SC:U),R,0,0,[_strtok_STK02,_strtok_STK01]
	;--cdb--S:L_strtok.strtok$str$65536$21({2}DG,SC:U),R,0,0,[_strtok_STK00,r0x1156]
	;--cdb--S:L_strtok.strtok$s$65536$22({2}DG,SC:U),E,0,0
	;--cdb--S:L_strtok.strtok$s1$65536$22({2}DG,SC:U),R,0,0,[_strtok_STK00,r0x1156]
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_strchr

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_strtok
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__strtok_0	udata
r0x1156:	.ds	1
_strtok_s_65536_22:	.ds	2
	.area DSEG (DATA); (local stack unassigned) 
_strtok_STK00:	.ds	1
	.globl _strtok_STK00
_strtok_STK01:	.ds	1
	.globl _strtok_STK01
_strtok_STK02:	.ds	1
	.globl _strtok_STK02
	.globl _strchr_STK01
	.globl _strchr_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:_strtok_STK00:NULL+0:14:0
	;--cdb--W:r0x1159:NULL+0:14:0
	;--cdb--W:r0x1159:NULL+0:-1:1
	;--cdb--W:r0x1156:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:-1:1
	;--cdb--W:r0x115A:NULL+0:-1:1
	;--cdb--W:_strtok_STK02:NULL+0:-1:1
	end
