;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #bbc754a74 (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"D:\common\ms311sphlib"
;;	.file	"dc_cancel.c"
	.module dc_cancel
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Fdc_cancel$adps[({0}S:S$predict$0$0({2}SI:S),Z,0,0)({2}S:S$index$0$0({1}SC:U),Z,0,0)]
	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_start_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_stop_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_b12_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
; *** BIT REGION *** (pseudo)
	.area UDATA (DATA,REL,CON)
; *** BIT REGION ***END
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; dc_cancel-code 
.globl _api_DC_cancel

;--------------------------------------------------------
	.FUNC _api_DC_cancel:$PNUM 3:$C:_api_enter_stdby_mode\
:$L:r0x115B:$L:_api_DC_cancel_STK00:$L:_api_DC_cancel_STK01:$L:r0x115E:$L:r0x115F\
:$L:r0x1160:$L:r0x1163
;--------------------------------------------------------
;	.line	10; "dc_cancel.c"	BYTE api_DC_cancel(BYTE dcdly, BYTE(*callback)(void))
_api_DC_cancel:	;Function start
	STA	r0x115B
;	;.line	16; "dc_cancel.c"	api_rampage=1;
	LDA	#0x01
	STA	_api_rampage
;	;.line	19; "dc_cancel.c"	DMAH=0X80; // reset dma address
	LDA	#0x80
	STA	_DMAH
;	;.line	21; "dc_cancel.c"	ADCON|=0x43; // MONO gain, linear
	LDA	_ADCON
	ORA	#0x43
	STA	_ADCON
;	;.line	22; "dc_cancel.c"	FILTERG=0x80; // when dc cancelling, FILTERG=0x80!!
	LDA	#0x80
	STA	_FILTERG
;	;.line	23; "dc_cancel.c"	for(i=0; i<dcdly; i++) // form mono linear 8k per page=16ms
	CLRA	
	STA	r0x115E
_00118_DS_:
	SETB	_C
	LDA	r0x115E
	SUBB	r0x115B
	JC	_00113_DS_
_00110_DS_:
;	;.line	25; "dc_cancel.c"	while(DMAH==api_rampage)
	LDA	_api_rampage
	XOR	_DMAH
	JNZ	_00112_DS_
;	;.line	27; "dc_cancel.c"	if(callback==NULL)
	LDA	_api_DC_cancel_STK01
	ORA	_api_DC_cancel_STK00
	JNZ	_00108_DS_
;	;.line	28; "dc_cancel.c"	api_enter_stdby_mode(0,0,0);
	CLRA	
	STA	_api_enter_stdby_mode_STK01
	STA	_api_enter_stdby_mode_STK00
	CALL	_api_enter_stdby_mode
	JMP	_00110_DS_
_00108_DS_:
;	;.line	30; "dc_cancel.c"	if(callback()!=0)
	CALL	_00159_DS_
	JMP	_00160_DS_
_00159_DS_:
	CALL	_00161_DS_
_00161_DS_:
	LDA	_api_DC_cancel_STK00
	STA	_STACKH
	LDA	_api_DC_cancel_STK01
	STA	_STACKL
	RET	
_00160_DS_:
	JZ	_00110_DS_
;	;.line	31; "dc_cancel.c"	return 0;
	CLRA	
	JMP	_00120_DS_
_00112_DS_:
;	;.line	34; "dc_cancel.c"	api_rampage=DMAH;
	LDA	_DMAH
	STA	_api_rampage
;	;.line	23; "dc_cancel.c"	for(i=0; i<dcdly; i++) // form mono linear 8k per page=16ms
	LDA	r0x115E
	INCA	
	STA	r0x115E
	JMP	_00118_DS_
_00113_DS_:
;	;.line	38; "dc_cancel.c"	ADCON&=0xfd; // stop dma
	LDA	#0xfd
	AND	_ADCON
	STA	_ADCON
;	;.line	39; "dc_cancel.c"	BGCON&=0xef;
	LDA	#0xef
	AND	_BGCON
	STA	_BGCON
;	;.line	41; "dc_cancel.c"	tmp0=0; // ulong
	CLRA	
	STA	r0x115B
	STA	_api_DC_cancel_STK01
	STA	_api_DC_cancel_STK00
	STA	r0x115E
;	;.line	42; "dc_cancel.c"	ROMPUW=0x8200;
	STA	_ROMPUW
	LDA	#0x82
	STA	(_ROMPUW + 1)
;	;.line	44; "dc_cancel.c"	do
	CLRA	
	STA	r0x115F
_00114_DS_:
;	;.line	46; "dc_cancel.c"	tmp1=ROMPINC2; // get a short from RAM;
	LDA	@_ROMPINC
	STA	r0x1160
	LDA	@_ROMPINC
;	;.line	47; "dc_cancel.c"	tmp1^=0x8000;
	XOR	#0x80
	STA	r0x1163
;	;.line	48; "dc_cancel.c"	tmp0+=tmp1;
	LDA	r0x115B
	ADD	r0x1160
	STA	r0x115B
	LDA	_api_DC_cancel_STK01
	ADDC	r0x1163
	STA	_api_DC_cancel_STK01
	CLRA	
	ADDC	_api_DC_cancel_STK00
	STA	_api_DC_cancel_STK00
	CLRA	
	ADDC	r0x115E
	STA	r0x115E
;	;.line	49; "dc_cancel.c"	} while(++i);
	LDA	r0x115F
	INCA	
	STA	r0x115F
	JNZ	_00114_DS_
;	;.line	50; "dc_cancel.c"	tmp0^=0x800000L;
	LDA	#0x80
	XOR	_api_DC_cancel_STK00
	STA	_api_DC_cancel_STK00
;	;.line	52; "dc_cancel.c"	OFFSETLH=(tmp1^0xffff)&0xfff0|(OFFSETLH&0xf); // no change lowest 4 LSB
	LDA	#0xff
	XOR	_api_DC_cancel_STK01
	STA	r0x115F
	LDA	#0xff
	XOR	_api_DC_cancel_STK00
	STA	r0x1160
	LDA	#0xf0
	AND	r0x115F
	STA	r0x115F
	LDA	_OFFSETLH
	AND	#0x0f
	ORA	r0x115F
	STA	_OFFSETLH
	LDA	r0x1160
	STA	(_OFFSETLH + 1)
;	;.line	53; "dc_cancel.c"	return 1;
	LDA	#0x01
_00120_DS_:
;	;.line	55; "dc_cancel.c"	}
	RET	
; exit point of _api_DC_cancel
	.ENDFUNC _api_DC_cancel
	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_start_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_stop_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_b12_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$IOR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IODIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IO$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0LH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1LH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAHL$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$init_io_state$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_rampage$0$0({1}SC:U),E,0,0
	;--cdb--S:Ldc_cancel.api_DC_cancel$callback$65536$39({2}DC,DF,SC:U),R,0,0,[_api_DC_cancel_STK01,_api_DC_cancel_STK00]
	;--cdb--S:Ldc_cancel.api_DC_cancel$dcdly$65536$39({1}SC:U),R,0,0,[r0x115B]
	;--cdb--S:Ldc_cancel.api_DC_cancel$i$65536$41({1}SC:U),R,0,0,[r0x115E]
	;--cdb--S:Ldc_cancel.api_DC_cancel$tmp0$65536$41({4}SL:U),R,0,0,[r0x115B,_api_DC_cancel_STK01,_api_DC_cancel_STK00,r0x115E]
	;--cdb--S:Ldc_cancel.api_DC_cancel$tmp1$65536$41({2}SI:U),R,0,0,[r0x1160,r0x1161]
	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_api_enter_stdby_mode
	.globl	_IOR
	.globl	_IODIR
	.globl	_IO
	.globl	_IOWK
	.globl	_IOWKDR
	.globl	_TIMERC
	.globl	_THRLD
	.globl	_RAMP0L
	.globl	_RAMP0H
	.globl	_RAMP1L
	.globl	_RAMP1H
	.globl	_PTRCL
	.globl	_PTRCH
	.globl	_ROMPL
	.globl	_ROMPH
	.globl	_BEEPC
	.globl	_FILTERG
	.globl	_ULAWC
	.globl	_STACKL
	.globl	_STACKH
	.globl	_ADCON
	.globl	_DACON
	.globl	_SYSC
	.globl	_SPIM
	.globl	_SPIH
	.globl	_SPIOP
	.globl	_SPI_BANK
	.globl	_ADP_IND
	.globl	_ADP_VPL
	.globl	_ADP_VPH
	.globl	_ADL
	.globl	_ADH
	.globl	_ZC
	.globl	_ADCG
	.globl	_DAC_PL
	.globl	_DAC_PH
	.globl	_PAG
	.globl	_DMAL
	.globl	_DMAH
	.globl	_SPIL
	.globl	_IOMASK
	.globl	_IOCMP
	.globl	_IOCNT
	.globl	_LVDCON
	.globl	_LVRCON
	.globl	_OFFSETL
	.globl	_OFFSETH
	.globl	_RCCON
	.globl	_BGCON
	.globl	_PWRL
	.globl	_CRYPT
	.globl	_PWRH
	.globl	_PWRHL
	.globl	_IROMDL
	.globl	_IROMDH
	.globl	_IROMDLH
	.globl	_RECMUTE
	.globl	_SPKC
	.globl	_DCLAMP
	.globl	_SSPIC
	.globl	_SSPIL
	.globl	_SSPIM
	.globl	_SSPIH
	.globl	_RAMP0
	.globl	_RAMP0LH
	.globl	_RAMP1LH
	.globl	_RAMP0INC
	.globl	_RAMP1
	.globl	_DMAHL
	.globl	_RAMP1INC
	.globl	_RAMP1INC2
	.globl	_ROMP
	.globl	_ROMPINC
	.globl	_ROMPLH
	.globl	_ROMPINC2
	.globl	_ACC
	.globl	_RAMP0UW
	.globl	_RAMP1UW
	.globl	_ROMPUW
	.globl	_SPIMH
	.globl	_OFFSETLH
	.globl	_ADP_VPLH
	.globl	_ICE0
	.globl	_ICE1
	.globl	_ICE2
	.globl	_ICE3
	.globl	_ICE4
	.globl	_TOV
	.globl	_init_io_state
	.globl	_api_rampage

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_api_DC_cancel
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_dc_cancel_0	udata
r0x115B:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1163:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_api_DC_cancel_STK00:	.ds	1
	.globl _api_DC_cancel_STK00
_api_DC_cancel_STK01:	.ds	1
	.globl _api_DC_cancel_STK01
	.globl _api_enter_stdby_mode_STK01
	.globl _api_enter_stdby_mode_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x1162:NULL+0:-1:1
	;--cdb--W:_api_DC_cancel_STK01:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:4447:0
	;--cdb--W:_api_DC_cancel_STK01:NULL+0:4448:0
	;--cdb--W:_api_DC_cancel_STK01:NULL+0:0:0
	;--cdb--W:r0x115F:NULL+0:4455:0
	;--cdb--W:r0x1160:NULL+0:4450:0
	;--cdb--W:r0x1160:NULL+0:4454:0
	;--cdb--W:r0x1161:NULL+0:4451:0
	;--cdb--W:r0x1162:NULL+0:4448:0
	;--cdb--W:r0x1164:NULL+0:0:0
	;--cdb--W:r0x1164:NULL+0:-1:1
	;--cdb--W:r0x1165:NULL+0:0:0
	;--cdb--W:r0x1165:NULL+0:-1:1
	;--cdb--W:r0x115F:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:-1:1
	end
