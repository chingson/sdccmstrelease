#include "ms326sphlib.h"

void api_normal_sleep(BYTE newpawk,BYTE newpawkdr, BYTE newpbwk, BYTE newpbwkdr, BYTE lvrdis, BYTE wkrst) // normal sleep need enable apor
{
    BYTE giesave;
    PAWKDR=newpawkdr; // dir write first!!
    PBWKDR=newpbwkdr;
    PAWK=newpawk;
    PBWK=newpbwk;
    ADP_IND=0x80;// following is 8 bit wide
    RPAGES=0xf0; // f0// just give a value
    PPAGES=0xf0;
    ADP_IND=0;// following is 6 bits only
    RPAGES=0x35;// 35
    PPAGES=0x35;
    RDMAH=0x80;
    PDMAH=0x80;
    ADCON=1;
    ADCON=0; // clear DMA wakeup bit
    SYSC2&=0x7f; // mbias off
    DACON=0;



    DMA_IL=0x80; // set to middle to prevent wakeup
    PAG=0; // this is important not zero will leak!!
    SYSC&=~SYSC_BIT_SKCMD;
    while(SYSC&SYSC_BIT_SKCMD);

    giesave=GIE;
    GIE=2; // WDT keep
    EA=0;
    DUMMYC=0x20; //RD/PH/OE
    TOUCHC=2; // TCIEN

    if(lvrdis)
    {
        LVRCON|=0x10;
        LVRCON|=0x18; // disable later
    }
    SYSC=0x05;
    if(wkrst)
    {
        __asm
        .dw 0x0000
        .dw 0x0000
        .dw 0xffff
        __endasm;
    }
    LVRCON&=0xF7;
    LVRCON&=0xe7;
    GIE=giesave;
}

