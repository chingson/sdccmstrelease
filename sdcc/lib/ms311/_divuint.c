
// divuint
#define MSB_SET(x) (x&0x8000)
extern volatile unsigned char STATUS;


unsigned int
_divuint (unsigned int x, unsigned int y)
{
  unsigned int reste = 0;
  unsigned char count = 16;

  do
  {
    // reste: x <- 0;
	if(x&0x8000)
	{
    		x <<= 1;
    		reste <<= 1;
      		reste |= 1;
	}
	else
	{
    		x <<= 1;
    		reste <<= 1;
	}

    if (reste >= y)
    {
      reste -= y;
      // x <- (result = 1)
      x |= 1;
    }
  }
  while (--count);
  return x;
}

