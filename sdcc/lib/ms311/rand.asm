;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"rand.c"
	.module rand
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$srand$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$rand$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--F:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0,0,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; rand-code 
.globl _srand

;--------------------------------------------------------
	.FUNC _srand:$PNUM 2:$L:r0x1163:$L:_srand_STK00
;--------------------------------------------------------
;	.line	39; "rand.c"	void srand(unsigned int seed)
_srand:	;Function start
	STA	r0x1163
;	;.line	41; "rand.c"	next = seed;
	LDA	_srand_STK00
	STA	_next
	LDA	r0x1163
	STA	(_next + 1)
	CLRA	
	STA	(_next + 2)
	STA	(_next + 3)
;	;.line	42; "rand.c"	}
	RET	
; exit point of _srand
	.ENDFUNC _srand
.globl _rand

;--------------------------------------------------------
	.FUNC _rand:$PNUM 0:$C:__mullong\
:$L:r0x1158:$L:r0x1156
;--------------------------------------------------------
;	.line	35; "rand.c"	next = next * 1103515245UL + 12345;
_rand:	;Function start
	LDA	_next
	STA	__mullong_STK06
	LDA	(_next + 1)
	STA	__mullong_STK05
	LDA	(_next + 2)
	STA	__mullong_STK04
	LDA	(_next + 3)
	STA	__mullong_STK03
	LDA	#0x6d
	STA	__mullong_STK02
	LDA	#0x4e
	STA	__mullong_STK01
	LDA	#0xc6
	STA	__mullong_STK00
	LDA	#0x41
	CALL	__mullong
	STA	r0x1158
	LDA	STK02
	ADD	#0x39
	STA	_next
	LDA	#0x30
	ADDC	STK01
	STA	(_next + 1)
	CLRA	
	ADDC	STK00
	STA	(_next + 2)
	CLRA	
	ADDC	r0x1158
;	;.line	36; "rand.c"	return (unsigned int)(next/65536) % (RAND_MAX + 1U);
	STA	(_next + 3)
	AND	#0x7f
	STA	r0x1156
	LDA	(_next + 2)
	STA	STK00
	LDA	r0x1156
;	;.line	37; "rand.c"	}
	RET	
; exit point of _rand
	.ENDFUNC _rand
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:Lrand.aligned_alloc$size$65536$15({2}SI:U),E,0,0
	;--cdb--S:Lrand.aligned_alloc$alignment$65536$15({2}SI:U),E,0,0
	;--cdb--S:Frand$next$0$0({4}SL:U),E,0,0
	;--cdb--S:Lrand.srand$seed$65536$31({2}SI:U),R,0,0,[_srand_STK00,r0x1163]
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__mullong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_srand
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_rand_0	udata
r0x1156:	.ds	1
r0x1158:	.ds	1
r0x1163:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
	.globl __mullong_STK06
	.globl __mullong_STK05
	.globl __mullong_STK04
	.globl __mullong_STK03
	.globl __mullong_STK02
	.globl __mullong_STK01
	.globl __mullong_STK00
_srand_STK00:	.ds	1
	.globl _srand_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------

	.area	IDATAROM	(CODE) ;rand-1-data
_next_shadow:
	.byte #0x01,#0x00,#0x00,#0x00	; 1

;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------

	.area	IDATA	(DATA) ;rand-0-data
_next:
	.ds	4

	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1155:NULL+0:-1:1
	;--cdb--W:r0x1157:NULL+0:-1:1
	;--cdb--W:r0x1158:NULL+0:-1:1
	;--cdb--W:r0x1157:NULL+0:14:0
	;--cdb--W:r0x1156:NULL+0:13:0
	;--cdb--W:r0x1155:NULL+0:4441:0
	end
