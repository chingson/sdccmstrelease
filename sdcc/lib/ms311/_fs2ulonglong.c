/*-------------------------------------------------------------------------
   _fs2ulong.c - Floating point library in optimized assembly for 8051

   Copyright (c) 2004, Paul Stoffregen, paul@pjrc.com

   This library is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any
   later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License 
   along with this library; see the file COPYING. If not, write to the
   Free Software Foundation, 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.

   As a special exception, if you link this library with other files,
   some of which are compiled with SDCC, to produce an executable,
   this library does not by itself cause the resulting executable to
   be covered by the GNU General Public License. This exception does
   not however invalidate any other reasons why the executable file
   might be covered by the GNU General Public License.
-------------------------------------------------------------------------*/


#define __SDCC_FLOAT_LIB
#include <float.h>


union float_long
{
  float f;
  long l;
};

/* convert float to unsigned long */
unsigned long long
__fs2ulonglong (float a1)
{
  signed char exp;
  unsigned long long l;
union float_long fl1;
  
  fl1.f = a1;
  
  if (!fl1.l || SIGN(fl1.l))
    return (0);

  exp = EXP (fl1.l) - EXCESS - 24;
  l = MANT (fl1.l);
  
  l >>= -exp;

  return l;
}

