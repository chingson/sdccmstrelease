;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"toupper.c"
	.module toupper
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$toupper$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isblank$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$islower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isupper$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; toupper-code 
.globl _toupper

;--------------------------------------------------------
	.FUNC _toupper:$PNUM 2:$L:r0x1155:$L:_toupper_STK00:$L:r0x1156:$L:r0x1157
;--------------------------------------------------------
;	.line	33; "toupper.c"	int toupper (int c)
_toupper:	;Function start
	STA	r0x1155
;	;.line	71; "/home/chingson/sdccofficial/sdcc/device/include/ctype.h"	return ((unsigned char)c >= 'a' && (unsigned char)c <= 'z');
	LDA	_toupper_STK00
	ADD	#0x9f
	JNC	_00108_DS_
	SETB	_C
	LDA	#0x7a
	SUBB	_toupper_STK00
	JNC	_00108_DS_
;	;.line	35; "toupper.c"	return (islower (c) ? c + ('A' - 'a') : c);
	LDA	#0xe0
	ADD	_toupper_STK00
	STA	r0x1156
	LDA	#0xff
	ADDC	r0x1155
	STA	r0x1157
	JMP	_00109_DS_
_00108_DS_:
	LDA	_toupper_STK00
	STA	r0x1156
	LDA	r0x1155
	STA	r0x1157
_00109_DS_:
	LDA	r0x1156
	STA	STK00
	LDA	r0x1157
;	;.line	36; "toupper.c"	}
	RET	
; exit point of _toupper
	.ENDFUNC _toupper
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:Ltoupper.isblank$c$65536$13({2}SI:S),E,0,0
	;--cdb--S:Ltoupper.isdigit$c$65536$15({2}SI:S),E,0,0
	;--cdb--S:Ltoupper.islower$c$65536$17({2}SI:S),E,0,0
	;--cdb--S:Ltoupper.isupper$c$65536$19({2}SI:S),E,0,0
	;--cdb--S:Ltoupper.toupper$c$65536$21({2}SI:S),R,0,0,[_toupper_STK00,r0x1155]
	;--cdb--S:Ltoupper.toupper$__1310720001$131072$22({2}SI:S),R,0,0,[]
	;--cdb--S:Ltoupper.toupper$__1310720002$131072$23({2}SI:S),R,0,0,[]
	;--cdb--S:Ltoupper.toupper$c$196608$24({2}SI:S),R,0,0,[]
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_toupper
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_toupper_0	udata
r0x1155:	.ds	1
r0x1156:	.ds	1
r0x1157:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_toupper_STK00:	.ds	1
	.globl _toupper_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1156:NULL+0:4440:0
	end
