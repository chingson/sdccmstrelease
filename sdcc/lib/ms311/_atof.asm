;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_atof.c"
	.module _atof
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$atof$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isblank$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$islower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isupper$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--F:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0,0,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _atof-code 
.globl _atof

;--------------------------------------------------------
	.FUNC _atof:$PNUM 2:$C:_isspace:$C:___fsmul:$C:___sint2fs:$C:___fsadd\
:$C:_toupper:$C:_atoi\
:$L:r0x1155:$L:_atof_STK00:$L:r0x1156:$L:r0x1157:$L:r0x1158\
:$L:r0x1159:$L:r0x115A:$L:r0x115B:$L:r0x115C:$L:r0x115D\
:$L:r0x115E:$L:r0x115F:$L:r0x1160:$L:r0x1161:$L:r0x1162\
:$L:r0x1164
;--------------------------------------------------------
;	.line	33; "_atof.c"	float atof(const char * s)
_atof:	;Function start
	STA	r0x1155
_00105_DS_:
;	;.line	40; "_atof.c"	while (isspace(*s)) s++;
	LDA	_atof_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_isspace_STK00
	CLRA	
	CALL	_isspace
	ORA	STK00
	JZ	_00159_DS_
	LDA	_atof_STK00
	INCA	
	STA	_atof_STK00
	CLRA	
	ADDC	r0x1155
	STA	r0x1155
	JMP	_00105_DS_
_00159_DS_:
	LDA	_atof_STK00
	STA	r0x1156
	LDA	r0x1155
	STA	r0x1157
;	;.line	43; "_atof.c"	if (*s == '-')
	LDA	_atof_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1158
	XOR	#0x2d
	JNZ	_00111_DS_
;	;.line	45; "_atof.c"	sign=1;
	LDA	#0x01
	STA	r0x1159
;	;.line	46; "_atof.c"	s++;
	LDA	_atof_STK00
	INCA	
	STA	r0x1156
	CLRA	
	ADDC	r0x1155
	STA	r0x1157
	JMP	_00112_DS_
_00111_DS_:
;	;.line	50; "_atof.c"	sign=0;
	CLRA	
	STA	r0x1159
;	;.line	51; "_atof.c"	if (*s == '+') s++;
	LDA	r0x1158
	XOR	#0x2b
	JNZ	_00112_DS_
	LDA	_atof_STK00
	INCA	
	STA	r0x1156
	CLRA	
	ADDC	r0x1155
	STA	r0x1157
_00112_DS_:
;	;.line	55; "_atof.c"	for (value=0.0; isdigit(*s); s++)
	CLRA	
	STA	_atof_STK00
	STA	r0x1155
	STA	r0x1158
	STA	r0x115A
_00130_DS_:
	LDA	r0x1156
	STA	_ROMPL
	LDA	r0x1157
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115B
;	;.line	62; "/home/chingson/sdccofficial/sdcc/device/include/ctype.h"	return ((unsigned char)c >= '0' && (unsigned char)c <= '9');
	ADD	#0xd0
	JNC	_00160_DS_
	SETB	_C
	LDA	#0x39
	SUBB	r0x115B
	JNC	_00160_DS_
;	;.line	57; "_atof.c"	value=10.0*value+(*s-'0');
	LDA	_atof_STK00
	STA	___fsmul_STK06
	LDA	r0x1155
	STA	___fsmul_STK05
	LDA	r0x1158
	STA	___fsmul_STK04
	LDA	r0x115A
	STA	___fsmul_STK03
	CLRA	
	STA	___fsmul_STK02
	STA	___fsmul_STK01
	LDA	#0x20
	STA	___fsmul_STK00
	LDA	#0x41
	CALL	___fsmul
	STA	r0x115E
	LDA	STK00
	STA	r0x115D
	LDA	STK01
	STA	r0x115C
	LDA	STK02
	STA	r0x115B
	LDA	r0x1156
	STA	_ROMPL
	LDA	r0x1157
	STA	_ROMPH
	LDA	@_ROMPINC
	ADD	#0xd0
	STA	r0x115F
	CLRA	
	ADDC	#0xff
	STA	r0x1162
	LDA	r0x115F
	STA	___sint2fs_STK00
	LDA	r0x1162
	CALL	___sint2fs
	STA	r0x1162
	LDA	STK02
	STA	___fsadd_STK06
	LDA	STK01
	STA	___fsadd_STK05
	LDA	STK00
	STA	___fsadd_STK04
	LDA	r0x1162
	STA	___fsadd_STK03
	LDA	r0x115B
	STA	___fsadd_STK02
	LDA	r0x115C
	STA	___fsadd_STK01
	LDA	r0x115D
	STA	___fsadd_STK00
	LDA	r0x115E
	CALL	___fsadd
	STA	r0x115A
	LDA	STK00
	STA	r0x1158
	LDA	STK01
	STA	r0x1155
	LDA	STK02
	STA	_atof_STK00
;	;.line	55; "_atof.c"	for (value=0.0; isdigit(*s); s++)
	LDA	r0x1156
	INCA	
	STA	r0x1156
	CLRA	
	ADDC	r0x1157
	STA	r0x1157
	JMP	_00130_DS_
_00160_DS_:
	LDA	r0x1156
	STA	r0x115B
	LDA	r0x1157
	STA	r0x115C
;	;.line	61; "_atof.c"	if (*s == '.')
	LDA	r0x1156
	STA	_ROMPL
	LDA	r0x1157
	STA	_ROMPH
	LDA	@_ROMPINC
	XOR	#0x2e
	JNZ	_00116_DS_
;	;.line	63; "_atof.c"	s++;
	LDA	r0x1156
	INCA	
	STA	r0x1156
	CLRA	
	ADDC	r0x1157
	STA	r0x1157
;	;.line	64; "_atof.c"	for (fraction=0.1; isdigit(*s); s++)
	LDA	#0xcd
	STA	r0x115D
	DECA	
	STA	r0x115E
	STA	r0x115F
	LDA	#0x3d
	STA	r0x1160
_00133_DS_:
	LDA	r0x1156
	STA	_ROMPL
	LDA	r0x1157
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1161
;	;.line	62; "/home/chingson/sdccofficial/sdcc/device/include/ctype.h"	return ((unsigned char)c >= '0' && (unsigned char)c <= '9');
	ADD	#0xd0
	JNC	_00161_DS_
	SETB	_C
	LDA	#0x39
	SUBB	r0x1161
	JNC	_00161_DS_
;	;.line	66; "_atof.c"	value+=(*s-'0')*fraction;
	LDA	r0x1156
	STA	_ROMPL
	LDA	r0x1157
	STA	_ROMPH
	LDA	@_ROMPINC
	ADD	#0xd0
	STA	r0x1161
	CLRA	
	ADDC	#0xff
	STA	r0x1164
	LDA	r0x1161
	STA	___sint2fs_STK00
	LDA	r0x1164
	CALL	___sint2fs
	STA	r0x1164
	LDA	r0x115D
	STA	___fsmul_STK06
	LDA	r0x115E
	STA	___fsmul_STK05
	LDA	r0x115F
	STA	___fsmul_STK04
	LDA	r0x1160
	STA	___fsmul_STK03
	LDA	STK02
	STA	___fsmul_STK02
	LDA	STK01
	STA	___fsmul_STK01
	LDA	STK00
	STA	___fsmul_STK00
	LDA	r0x1164
	CALL	___fsmul
	STA	r0x1164
	LDA	STK02
	STA	___fsadd_STK06
	LDA	STK01
	STA	___fsadd_STK05
	LDA	STK00
	STA	___fsadd_STK04
	LDA	r0x1164
	STA	___fsadd_STK03
	LDA	_atof_STK00
	STA	___fsadd_STK02
	LDA	r0x1155
	STA	___fsadd_STK01
	LDA	r0x1158
	STA	___fsadd_STK00
	LDA	r0x115A
	CALL	___fsadd
	STA	r0x115A
	LDA	STK00
	STA	r0x1158
	LDA	STK01
	STA	r0x1155
	LDA	STK02
	STA	_atof_STK00
;	;.line	67; "_atof.c"	fraction*=0.1;
	LDA	r0x115D
	STA	___fsmul_STK06
	LDA	r0x115E
	STA	___fsmul_STK05
	LDA	r0x115F
	STA	___fsmul_STK04
	LDA	r0x1160
	STA	___fsmul_STK03
	LDA	#0xcd
	STA	___fsmul_STK02
	DECA	
	STA	___fsmul_STK01
	STA	___fsmul_STK00
	LDA	#0x3d
	CALL	___fsmul
	STA	r0x1160
	LDA	STK00
	STA	r0x115F
	LDA	STK01
	STA	r0x115E
	LDA	STK02
	STA	r0x115D
;	;.line	64; "_atof.c"	for (fraction=0.1; isdigit(*s); s++)
	LDA	r0x1156
	INCA	
	STA	r0x1156
	CLRA	
	ADDC	r0x1157
	STA	r0x1157
	JMP	_00133_DS_
_00161_DS_:
	LDA	r0x1156
	STA	r0x115B
	LDA	r0x1157
	STA	r0x115C
_00116_DS_:
;	;.line	72; "_atof.c"	if (toupper(*s)=='E')
	LDA	r0x115B
	STA	_ROMPL
	LDA	r0x115C
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_toupper_STK00
	CLRA	
	CALL	_toupper
	JNZ	_00254_DS_
	LDA	STK00
	XOR	#0x45
_00254_DS_:
	JNZ	_00124_DS_
;	;.line	74; "_atof.c"	s++;
	LDA	r0x115B
	INCA	
	STA	r0x115B
	CLRA	
	ADDC	r0x115C
	STA	r0x115C
;	;.line	75; "_atof.c"	iexp=(signed char)atoi(s);
	LDA	r0x115B
	STA	_atoi_STK00
	LDA	r0x115C
	CALL	_atoi
	LDA	STK00
	STA	r0x115B
_00120_DS_:
;	;.line	77; "_atof.c"	while(iexp!=0)
	LDA	r0x115B
;	;.line	79; "_atof.c"	if(iexp<0)
	JZ	_00124_DS_
	JPL	_00118_DS_
;	;.line	81; "_atof.c"	value*=0.1;
	LDA	_atof_STK00
	STA	___fsmul_STK06
	LDA	r0x1155
	STA	___fsmul_STK05
	LDA	r0x1158
	STA	___fsmul_STK04
	LDA	r0x115A
	STA	___fsmul_STK03
	LDA	#0xcd
	STA	___fsmul_STK02
	DECA	
	STA	___fsmul_STK01
	STA	___fsmul_STK00
	LDA	#0x3d
	CALL	___fsmul
	STA	r0x115A
	LDA	STK00
	STA	r0x1158
	LDA	STK01
	STA	r0x1155
	LDA	STK02
	STA	_atof_STK00
;	;.line	82; "_atof.c"	iexp++;
	LDA	r0x115B
	INCA	
	STA	r0x115B
	JMP	_00120_DS_
_00118_DS_:
;	;.line	86; "_atof.c"	value*=10.0;
	LDA	_atof_STK00
	STA	___fsmul_STK06
	LDA	r0x1155
	STA	___fsmul_STK05
	LDA	r0x1158
	STA	___fsmul_STK04
	LDA	r0x115A
	STA	___fsmul_STK03
	CLRA	
	STA	___fsmul_STK02
	STA	___fsmul_STK01
	LDA	#0x20
	STA	___fsmul_STK00
	LDA	#0x41
	CALL	___fsmul
	STA	r0x115A
	LDA	STK00
	STA	r0x1158
	LDA	STK01
	STA	r0x1155
	LDA	STK02
	STA	_atof_STK00
;	;.line	87; "_atof.c"	iexp--;
	LDA	r0x115B
	DECA	
	STA	r0x115B
	JMP	_00120_DS_
_00124_DS_:
;	;.line	93; "_atof.c"	if(sign) value*=-1.0;
	LDA	r0x1159
	JZ	_00126_DS_
	LDA	r0x115A
	XOR	#0x80
	STA	r0x115A
_00126_DS_:
;	;.line	94; "_atof.c"	return (value);
	LDA	_atof_STK00
	STA	STK02
	LDA	r0x1155
	STA	STK01
	LDA	r0x1158
	STA	STK00
	LDA	r0x115A
;	;.line	95; "_atof.c"	}
	RET	
; exit point of _atof
	.ENDFUNC _atof
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:L_atof.isblank$c$65536$13({2}SI:S),E,0,0
	;--cdb--S:L_atof.isdigit$c$65536$15({2}SI:S),E,0,0
	;--cdb--S:L_atof.islower$c$65536$17({2}SI:S),E,0,0
	;--cdb--S:L_atof.isupper$c$65536$19({2}SI:S),E,0,0
	;--cdb--S:L_atof.aligned_alloc$size$65536$35({2}SI:U),E,0,0
	;--cdb--S:L_atof.aligned_alloc$alignment$65536$35({2}SI:U),E,0,0
	;--cdb--S:L_atof.atof$s$65536$49({2}DG,SC:U),R,0,0,[_atof_STK00]
	;--cdb--S:L_atof.atof$__1310720001$131072$50({2}SI:S),R,0,0,[]
	;--cdb--S:L_atof.atof$value$65536$50({4}SF:S),R,0,0,[_atof_STK00,r0x1155,r0x1158,r0x115A]
	;--cdb--S:L_atof.atof$fraction$65536$50({4}SF:S),R,0,0,[r0x115D,r0x115E,r0x115F,r0x1160]
	;--cdb--S:L_atof.atof$iexp$65536$50({1}SC:S),R,0,0,[r0x115B]
	;--cdb--S:L_atof.atof$sign$65536$50({1}:S),R,0,0,[r0x1159]
	;--cdb--S:L_atof.atof$__1966080002$196608$63({2}SI:S),R,0,0,[r0x115C,r0x115D]
	;--cdb--S:L_atof.atof$c$262144$64({2}SI:S),R,0,0,[]
	;--cdb--S:L_atof.atof$__1966080004$196608$55({2}SI:S),R,0,0,[]
	;--cdb--S:L_atof.atof$__2621440005$262144$66({2}SI:S),R,0,0,[r0x1162,r0x1163]
	;--cdb--S:L_atof.atof$c$327680$67({2}SI:S),R,0,0,[]
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_isspace
	.globl	_toupper
	.globl	_atoi
	.globl	___fsmul
	.globl	___sint2fs
	.globl	___fsadd

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_atof
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__atof_0	udata
r0x1155:	.ds	1
r0x1156:	.ds	1
r0x1157:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1164:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_atof_STK00:	.ds	1
	.globl _atof_STK00
	.globl _isspace_STK00
	.globl ___fsmul_STK06
	.globl ___fsmul_STK05
	.globl ___fsmul_STK04
	.globl ___fsmul_STK03
	.globl ___fsmul_STK02
	.globl ___fsmul_STK01
	.globl ___fsmul_STK00
	.globl ___sint2fs_STK00
	.globl ___fsadd_STK06
	.globl ___fsadd_STK05
	.globl ___fsadd_STK04
	.globl ___fsadd_STK03
	.globl ___fsadd_STK02
	.globl ___fsadd_STK01
	.globl ___fsadd_STK00
	.globl _toupper_STK00
	.globl _atoi_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1156:NULL+0:-1:1
	;--cdb--W:r0x115D:NULL+0:-1:1
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x1157:NULL+0:-1:1
	;--cdb--W:r0x1156:NULL+0:14:0
	;--cdb--W:r0x1157:NULL+0:4438:0
	;--cdb--W:r0x1158:NULL+0:0:0
	;--cdb--W:r0x115B:NULL+0:4444:0
	;--cdb--W:r0x115C:NULL+0:4443:0
	;--cdb--W:r0x115D:NULL+0:0:0
	;--cdb--W:r0x115F:NULL+0:12:0
	;--cdb--W:r0x1160:NULL+0:4447:0
	;--cdb--W:r0x1160:NULL+0:13:0
	;--cdb--W:r0x1161:NULL+0:0:0
	;--cdb--W:r0x1161:NULL+0:14:0
	;--cdb--W:r0x1161:NULL+0:4450:0
	;--cdb--W:r0x1161:NULL+0:12:0
	;--cdb--W:r0x1162:NULL+0:4449:0
	;--cdb--W:r0x1162:NULL+0:13:0
	;--cdb--W:r0x1163:NULL+0:0:0
	;--cdb--W:r0x1163:NULL+0:14:0
	;--cdb--W:r0x1158:NULL+0:-1:1
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x115F:NULL+0:-1:1
	end
