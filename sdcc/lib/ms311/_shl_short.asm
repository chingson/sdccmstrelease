;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.3.0 #8604 (Sep 15 2014) (MSVC)
; This file was generated Sat Oct 11 21:41:17 2014
;--------------------------------------------------------
; MST port for the MS322 series simple CPU
;--------------------------------------------------------
	.module _shl_short
	;.list	p=MS205
	;.radix dec
	.include "ms326sfr.def"
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
;--------------------------------------------------------
; Code Head 
;--------------------------------------------------------
	.area CSEG	 (CODE) 
.FUNC	__shl_short
;--------------------------------------------------------
; code
;--------------------------------------------------------
; sh ushort, assembly is the source!!
; input: _PTRCL is the data byte
; input: ACC is the shift num
__shl_short:	;Function start
; 2 exit points
	sta 	_ROMPL ; temp var
	add	#0xf0
	jc	shift_too_large
	lda	_ROMPL
loop_shl:
	jnz	cont_shift
	ret
cont_shift:
	lda	_PTRCL
	shl
	sta	_PTRCL
	lda	STK00
	rol
	sta	STK00
	lda	_ROMPL
	deca
	sta	_ROMPL
	jmp	loop_shl

	
shift_too_large:
	clra
	sta	_PTRCL
	sta	STK00
	ret

; exit point of _shl_short


;	code size estimation:
;	   21+   10 =    31 instructions (   31 byte)

;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__shl_short
	.globl  STK00

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	;end
