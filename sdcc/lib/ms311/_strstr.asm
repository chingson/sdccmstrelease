;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_strstr.c"
	.module _strstr
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--F:G$strstr$0$0({2}DF,DG,SC:U),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _strstr-code 
.globl _strstr

;--------------------------------------------------------
	.FUNC _strstr:$PNUM 4:$L:r0x1155:$L:_strstr_STK00:$L:_strstr_STK01:$L:_strstr_STK02:$L:r0x1158\
:$L:r0x1159:$L:r0x115A:$L:r0x115B:$L:r0x115C:$L:r0x115D\
:$L:r0x115E:$L:r0x115F
;--------------------------------------------------------
;	.line	31; "_strstr.c"	char * strstr ( char * str1, char * str2 )
_strstr:	;Function start
	STA	r0x1155
;	;.line	33; "_strstr.c"	char * cp = str1;
	LDA	_strstr_STK00
	STA	r0x1158
	LDA	r0x1155
	STA	r0x1159
;	;.line	37; "_strstr.c"	if ( !*str2 )
	LDA	_strstr_STK02
	STA	_ROMPL
	LDA	_strstr_STK01
	STA	_ROMPH
	LDA	@_ROMPINC
	JNZ	_00114_DS_
;	;.line	38; "_strstr.c"	return str1;
	LDA	_strstr_STK00
	STA	STK00
	LDA	r0x1155
	JMP	_00117_DS_
_00114_DS_:
;	;.line	40; "_strstr.c"	while (*cp)
	LDA	_strstr_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	JZ	_00116_DS_
;	;.line	45; "_strstr.c"	while ( *s1 && *s2 && !(*s1-*s2) )
	LDA	_strstr_STK00
	STA	r0x115A
	LDA	r0x1155
	STA	r0x115B
	LDA	_strstr_STK02
	STA	r0x115C
	LDA	_strstr_STK01
	STA	r0x115D
_00109_DS_:
	LDA	r0x115A
	STA	_ROMPL
	LDA	r0x115B
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115E
	JZ	_00111_DS_
	LDA	r0x115C
	STA	_ROMPL
	LDA	r0x115D
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115F
	JZ	_00111_DS_
	CLRA	
	SETB	_C
	LDA	r0x115E
	SUBB	r0x115F
	STA	r0x115F
	CLRA	
	SUBB	#0x00
	ORA	r0x115F
	JNZ	_00111_DS_
;	;.line	46; "_strstr.c"	s1++, s2++;
	LDA	r0x115A
	INCA	
	STA	r0x115A
	CLRA	
	ADDC	r0x115B
	STA	r0x115B
	LDA	r0x115C
	INCA	
	STA	r0x115C
	CLRA	
	ADDC	r0x115D
	STA	r0x115D
	JMP	_00109_DS_
_00111_DS_:
;	;.line	48; "_strstr.c"	if (!*s2)
	LDA	r0x115C
	STA	_ROMPL
	LDA	r0x115D
	STA	_ROMPH
	LDA	@_ROMPINC
	JNZ	_00113_DS_
;	;.line	49; "_strstr.c"	return( (char*)cp );
	LDA	r0x1158
	STA	STK00
	LDA	r0x1159
	JMP	_00117_DS_
_00113_DS_:
;	;.line	51; "_strstr.c"	cp++;
	LDA	_strstr_STK00
	INCA	
	STA	_strstr_STK00
	CLRA	
	ADDC	r0x1155
	STA	r0x1155
	LDA	_strstr_STK00
	STA	r0x1158
	LDA	r0x1155
	STA	r0x1159
	JMP	_00114_DS_
_00116_DS_:
;	;.line	54; "_strstr.c"	return (NULL) ;
	CLRA	
	STA	STK00
_00117_DS_:
;	;.line	55; "_strstr.c"	}
	RET	
; exit point of _strstr
	.ENDFUNC _strstr
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_strstr.strstr$str2$65536$21({2}DG,SC:U),R,0,0,[_strstr_STK02,_strstr_STK01]
	;--cdb--S:L_strstr.strstr$str1$65536$21({2}DG,SC:U),R,0,0,[_strstr_STK00,r0x1155]
	;--cdb--S:L_strstr.strstr$cp$65536$22({2}DG,SC:U),R,0,0,[r0x1158,r0x1159]
	;--cdb--S:L_strstr.strstr$s1$65536$22({2}DG,SC:U),R,0,0,[_strstr_STK00,r0x1155]
	;--cdb--S:L_strstr.strstr$s2$65536$22({2}DG,SC:U),R,0,0,[r0x115C,r0x115D]
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_strstr
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__strstr_0	udata
r0x1155:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_strstr_STK00:	.ds	1
	.globl _strstr_STK00
_strstr_STK01:	.ds	1
	.globl _strstr_STK01
_strstr_STK02:	.ds	1
	.globl _strstr_STK02
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115A:NULL+0:-1:1
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x115E:NULL+0:4447:0
	;--cdb--W:r0x1160:NULL+0:4446:0
	;--cdb--W:r0x1162:NULL+0:0:0
	;--cdb--W:r0x1162:NULL+0:-1:1
	;--cdb--W:r0x1161:NULL+0:0:0
	;--cdb--W:r0x1161:NULL+0:-1:1
	end
