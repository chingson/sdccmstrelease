;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_divsint.c"
	.module _divsint
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_divsint$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$_divsint$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$_divuint$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _divsint-code 
.globl __divsint

;--------------------------------------------------------
	.FUNC __divsint:$PNUM 4:$C:__divuint\
:$L:r0x1155:$L:__divsint_STK00:$L:__divsint_STK01:$L:__divsint_STK02:$L:r0x1158\

;--------------------------------------------------------
;	.line	207; "_divsint.c"	_divsint (int x, int y)
__divsint:	;Function start
	STA	r0x1155
;	;.line	209; "_divsint.c"	unsigned char flag=0;
	CLRA	
	STA	r0x1158
;	;.line	212; "_divsint.c"	if(x<0) { flag=1; x=-x;};
	LDA	r0x1155
	JPL	_00106_DS_
	LDA	#0x01
	STA	r0x1158
	SETB	_C
	CLRA	
	SUBB	__divsint_STK00
	STA	__divsint_STK00
	CLRA	
	SUBB	r0x1155
	STA	r0x1155
_00106_DS_:
;	;.line	213; "_divsint.c"	if(y<0) { flag^=1; y=-y;};
	LDA	__divsint_STK01
	JPL	_00108_DS_
	LDA	#0x01
	XOR	r0x1158
	STA	r0x1158
	SETB	_C
	CLRA	
	SUBB	__divsint_STK02
	STA	__divsint_STK02
	CLRA	
	SUBB	__divsint_STK01
	STA	__divsint_STK01
_00108_DS_:
;	;.line	214; "_divsint.c"	r = (unsigned int)(x) / (unsigned int)(y);
	LDA	__divsint_STK02
	STA	__divuint_STK02
	LDA	__divsint_STK01
	STA	__divuint_STK01
	LDA	__divsint_STK00
	STA	__divuint_STK00
	LDA	r0x1155
	CALL	__divuint
	STA	r0x1155
;	;.line	215; "_divsint.c"	if (flag)
	LDA	r0x1158
	JZ	_00110_DS_
;	;.line	216; "_divsint.c"	return -r;
	SETB	_C
	CLRA	
	SUBB	STK00
	STA	r0x1158
	CLRA	
	SUBB	r0x1155
	STA	__divsint_STK02
	LDA	r0x1158
	STA	STK00
	LDA	__divsint_STK02
	JMP	_00111_DS_
_00110_DS_:
;	;.line	217; "_divsint.c"	return r;
	LDA	r0x1155
_00111_DS_:
;	;.line	218; "_divsint.c"	}
	RET	
; exit point of __divsint
	.ENDFUNC __divsint
	;--cdb--S:G$_divsint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_divuint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_divsint._divsint$y$65536$1({2}SI:S),R,0,0,[__divsint_STK02,__divsint_STK01]
	;--cdb--S:L_divsint._divsint$x$65536$1({2}SI:S),R,0,0,[__divsint_STK00,r0x1155]
	;--cdb--S:L_divsint._divsint$flag$65536$2({1}SC:U),R,0,0,[r0x1158]
	;--cdb--S:L_divsint._divsint$r$65536$2({2}SI:S),R,0,0,[__divsint_STK00,r0x1155]
	;--cdb--S:G$_divsint$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__divuint

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__divsint
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__divsint_0	udata
r0x1155:	.ds	1
r0x1158:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__divsint_STK00:	.ds	1
	.globl __divsint_STK00
__divsint_STK01:	.ds	1
	.globl __divsint_STK01
__divsint_STK02:	.ds	1
	.globl __divsint_STK02
	.globl __divuint_STK02
	.globl __divuint_STK01
	.globl __divuint_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115A:NULL+0:-1:1
	;--cdb--W:r0x1155:NULL+0:4444:0
	;--cdb--W:__divsint_STK00:NULL+0:4445:0
	;--cdb--W:__divsint_STK00:NULL+0:14:0
	;--cdb--W:r0x1159:NULL+0:4440:0
	;--cdb--W:r0x1159:NULL+0:4443:0
	;--cdb--W:r0x115A:NULL+0:4437:0
	;--cdb--W:r0x1159:NULL+0:-1:1
	end
