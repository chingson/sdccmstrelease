;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_rrulonglong.c"
	.module _rrulonglong
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_rrulonglong$0$0({2}DF,SI:U),C,0,0
	;--cdb--F:G$_rrulonglong$0$0({2}DF,SI:U),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _rrulonglong-code 
.globl __rrulonglong

;--------------------------------------------------------
	.FUNC __rrulonglong:$PNUM 9:$C:__shr_ulong\
:$L:__rrulonglong_STK00:$L:__rrulonglong_STK01:$L:__rrulonglong_STK02:$L:__rrulonglong_STK03:$L:__rrulonglong_STK04\
:$L:__rrulonglong_STK05:$L:__rrulonglong_STK06:$L:__rrulonglong_STK07:$L:r0x1154:$L:__rrulonglong_l_65536_1\
:$L:r0x1156:$L:r0x1155:$L:r0x1158:$L:r0x1157:$L:r0x115A\
:$L:r0x1159:$L:r0x115C:$L:r0x115B:$L:r0x115D:$L:r0x115E\
:$L:r0x115F:$L:r0x1160:$L:r0x1161:$L:r0x1162:$L:r0x1165\
:$L:r0x1166:$L:r0x1167
;--------------------------------------------------------
;	.line	35; "_rrulonglong.c"	unsigned long long _rrulonglong(unsigned long long l, char s)
__rrulonglong:	;Function start
	STA	(__rrulonglong_l_65536_1 + 7)
	LDA	__rrulonglong_STK00
	STA	(__rrulonglong_l_65536_1 + 6)
	LDA	__rrulonglong_STK01
	STA	(__rrulonglong_l_65536_1 + 5)
	LDA	__rrulonglong_STK02
	STA	(__rrulonglong_l_65536_1 + 4)
	LDA	__rrulonglong_STK03
	STA	(__rrulonglong_l_65536_1 + 3)
	LDA	__rrulonglong_STK04
	STA	(__rrulonglong_l_65536_1 + 2)
	LDA	__rrulonglong_STK05
	STA	(__rrulonglong_l_65536_1 + 1)
	LDA	__rrulonglong_STK06
	STA	__rrulonglong_l_65536_1
	LDA	__rrulonglong_STK07
	STA	r0x1154
;	;.line	37; "_rrulonglong.c"	__data uint32_t *__data top = (__data uint32_t *)((__data char *)(&l) + 4);
	LDA	#(__rrulonglong_l_65536_1 + 0)
	ADD	#0x04
	STA	r0x1155
	CLRA	
	ADDC	#high (__rrulonglong_l_65536_1 + 0)
	STA	r0x1158
	LDA	r0x1155
	STA	r0x1157
;	;.line	38; "_rrulonglong.c"	__data uint16_t *__data middle = (__data uint16_t *)((__data char *)(&l) + 3);
	LDA	#(__rrulonglong_l_65536_1 + 0)
	ADD	#0x03
	STA	r0x1155
	CLRA	
	ADDC	#high (__rrulonglong_l_65536_1 + 0)
	STA	r0x115A
	LDA	r0x1155
	STA	r0x1159
;	;.line	39; "_rrulonglong.c"	__data uint32_t *__data bottom = (__data uint32_t *)(&l);
	LDA	#high (__rrulonglong_l_65536_1 + 0)
	STA	r0x1156
	LDA	#(__rrulonglong_l_65536_1 + 0)
	STA	r0x1155
_00107_DS_:
;	;.line	42; "_rrulonglong.c"	for(;s >= 16; s -= 16)
	LDA	r0x1154
	ADD	#0xf0
	JNC	_00105_DS_
;	;.line	44; "_rrulonglong.c"	b[0] = b[1];
	LDA	#0x02
	ADD	#(__rrulonglong_l_65536_1 + 0)
	STA	r0x115D
	CLRA	
	ADDC	#high (__rrulonglong_l_65536_1 + 0)
	STA	r0x115E
	LDA	r0x115D
	STA	_ROMPL
	LDA	r0x115E
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115F
	LDA	@_ROMPINC
	STA	r0x1160
	LDA	#(__rrulonglong_l_65536_1 + 0)
	STA	_ROMPL
	LDA	#high (__rrulonglong_l_65536_1 + 0)
	STA	_ROMPH
	LDA	r0x115F
	STA	@_ROMPINC
	LDA	r0x1160
	STA	@_ROMP
;	;.line	45; "_rrulonglong.c"	b[1] = b[2];
	LDA	#0x04
	ADD	#(__rrulonglong_l_65536_1 + 0)
	STA	r0x115F
	CLRA	
	ADDC	#high (__rrulonglong_l_65536_1 + 0)
	STA	r0x1160
	LDA	r0x115F
	STA	_ROMPL
	LDA	r0x1160
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1161
	LDA	@_ROMPINC
	STA	r0x1162
	LDA	r0x115D
	STA	_ROMPL
	LDA	r0x115E
	STA	_ROMPH
	LDA	r0x1161
	STA	@_ROMPINC
	LDA	r0x1162
	STA	@_ROMP
;	;.line	46; "_rrulonglong.c"	b[2] = b[3];
	LDA	#0x06
	ADD	#(__rrulonglong_l_65536_1 + 0)
	STA	r0x115D
	CLRA	
	ADDC	#high (__rrulonglong_l_65536_1 + 0)
	STA	r0x115E
	LDA	r0x115D
	STA	_ROMPL
	LDA	r0x115E
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1161
	LDA	@_ROMPINC
	STA	r0x1162
	LDA	r0x115F
	STA	_ROMPL
	LDA	r0x1160
	STA	_ROMPH
	LDA	r0x1161
	STA	@_ROMPINC
	LDA	r0x1162
	STA	@_ROMP
;	;.line	47; "_rrulonglong.c"	b[3] = 0x000000;
	LDA	r0x115D
	STA	_ROMPL
	LDA	r0x115E
	STA	_ROMPH
	CLRA	
	STA	@_ROMPINC
	STA	@_ROMPINC
;	;.line	42; "_rrulonglong.c"	for(;s >= 16; s -= 16)
	LDA	r0x1154
	ADD	#0xf0
	STA	r0x1154
	JMP	_00107_DS_
_00105_DS_:
;	;.line	50; "_rrulonglong.c"	(*bottom) >>= s;
	LDA	r0x1155
	STA	_ROMPL
	LDA	r0x1156
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115B
	LDA	@_ROMPINC
	STA	r0x115C
	LDA	@_ROMPINC
	STA	r0x115D
	LDA	@_ROMPINC
	STA	STK00
	LDA	r0x115D
	STA	STK01
	LDA	r0x115C
	STA	STK02
	LDA	r0x115B
	STA	_PTRCL
	LDA	r0x1154
	CALL	__shr_ulong
	LDA	_PTRCL
	STA	r0x115F
	LDA	r0x1155
	STA	_ROMPL
	LDA	r0x1156
	STA	_ROMPH
	LDA	r0x115F
	STA	@_ROMPINC
	LDA	STK02
	STA	@_ROMPINC
	LDA	STK01
	STA	@_ROMPINC
	LDA	STK00
	STA	@_ROMP
;	;.line	51; "_rrulonglong.c"	(*middle) |= (((*middle) & 0xffff0000ul) >> s);
	LDA	r0x1159
	STA	_ROMPL
	LDA	r0x115A
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1155
	LDA	@_ROMPINC
	STA	r0x1156
	CLRA	
	STA	STK00
	STA	STK01
	STA	STK02
	STA	_PTRCL
	LDA	r0x1154
	CALL	__shr_ulong
	LDA	_PTRCL
	ORA	r0x1155
	STA	r0x1155
	LDA	STK02
	ORA	r0x1156
	STA	r0x1156
	LDA	r0x1159
	STA	_ROMPL
	LDA	r0x115A
	STA	_ROMPH
	LDA	r0x1155
	STA	@_ROMPINC
	LDA	r0x1156
	STA	@_ROMP
;	;.line	52; "_rrulonglong.c"	(*top) |= (((*middle) & 0xffff0000ul) >> s);
	LDA	r0x1157
	STA	_ROMPL
	LDA	r0x1158
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1159
	LDA	@_ROMPINC
	STA	r0x115A
	LDA	@_ROMPINC
	STA	r0x115B
	LDA	@_ROMPINC
	STA	r0x115C
	CLRA	
	STA	STK00
	STA	STK01
	STA	STK02
	STA	_PTRCL
	LDA	r0x1154
	CALL	__shr_ulong
	LDA	_PTRCL
	ORA	r0x1159
	STA	r0x1154
	LDA	STK02
	ORA	r0x115A
	STA	r0x1165
	LDA	STK01
	ORA	r0x115B
	STA	r0x1166
	LDA	STK00
	ORA	r0x115C
	STA	r0x1167
	LDA	r0x1157
	STA	_ROMPL
	LDA	r0x1158
	STA	_ROMPH
	LDA	r0x1154
	STA	@_ROMPINC
	LDA	r0x1165
	STA	@_ROMPINC
	LDA	r0x1166
	STA	@_ROMPINC
	LDA	r0x1167
	STA	@_ROMP
;	;.line	54; "_rrulonglong.c"	return(l);
	LDA	__rrulonglong_l_65536_1
	STA	STK06
	LDA	(__rrulonglong_l_65536_1 + 1)
	STA	STK05
	LDA	(__rrulonglong_l_65536_1 + 2)
	STA	STK04
	LDA	(__rrulonglong_l_65536_1 + 3)
	STA	STK03
	LDA	(__rrulonglong_l_65536_1 + 4)
	STA	STK02
	LDA	(__rrulonglong_l_65536_1 + 5)
	STA	STK01
	LDA	(__rrulonglong_l_65536_1 + 6)
	STA	STK00
	LDA	(__rrulonglong_l_65536_1 + 7)
;	;.line	55; "_rrulonglong.c"	}
	RET	
; exit point of __rrulonglong
	.ENDFUNC __rrulonglong
	;--cdb--S:G$_rrulonglong$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_rrulonglong._rrulonglong$s$65536$1({1}SC:U),R,0,0,[r0x1154]
	;--cdb--S:L_rrulonglong._rrulonglong$l$65536$1({8}SI:U),E,0,0
	;--cdb--S:L_rrulonglong._rrulonglong$top$65536$2({2}DD,SL:U),R,0,0,[r0x1157,r0x1158]
	;--cdb--S:L_rrulonglong._rrulonglong$middle$65536$2({2}DD,SI:U),R,0,0,[r0x1159,r0x115A]
	;--cdb--S:L_rrulonglong._rrulonglong$bottom$65536$2({2}DD,SL:U),R,0,0,[r0x1155,r0x1156]
	;--cdb--S:L_rrulonglong._rrulonglong$b$65536$2({2}DD,SI:U),R,0,0,[r0x115B,r0x115C]
	;--cdb--S:G$_rrulonglong$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__shr_ulong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__rrulonglong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__rrulonglong_0	udata
r0x1154:	.ds	1
r0x1155:	.ds	1
r0x1156:	.ds	1
r0x1157:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__rrulonglong_l_65536_1:	.ds	8
__rrulonglong_STK00:	.ds	1
	.globl __rrulonglong_STK00
__rrulonglong_STK01:	.ds	1
	.globl __rrulonglong_STK01
__rrulonglong_STK02:	.ds	1
	.globl __rrulonglong_STK02
__rrulonglong_STK03:	.ds	1
	.globl __rrulonglong_STK03
__rrulonglong_STK04:	.ds	1
	.globl __rrulonglong_STK04
__rrulonglong_STK05:	.ds	1
	.globl __rrulonglong_STK05
__rrulonglong_STK06:	.ds	1
	.globl __rrulonglong_STK06
__rrulonglong_STK07:	.ds	1
	.globl __rrulonglong_STK07
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1155:NULL+0:-1:1
	;--cdb--W:r0x115D:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:-1:1
	;--cdb--W:r0x115C:NULL+0:-1:1
	;--cdb--W:r0x1162:NULL+0:-1:1
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x115F:NULL+0:-1:1
	;--cdb--W:r0x1160:NULL+0:-1:1
	;--cdb--W:r0x1156:NULL+0:0:0
	;--cdb--W:r0x115C:NULL+0:0:0
	;--cdb--W:r0x115C:NULL+0:4448:0
	;--cdb--W:r0x115B:NULL+0:4447:0
	;--cdb--W:r0x115F:NULL+0:4443:0
	;--cdb--W:r0x115F:NULL+0:4437:0
	;--cdb--W:r0x1160:NULL+0:4444:0
	;--cdb--W:r0x1160:NULL+0:12:0
	;--cdb--W:r0x1160:NULL+0:0:0
	;--cdb--W:r0x1161:NULL+0:4445:0
	;--cdb--W:r0x1161:NULL+0:13:0
	;--cdb--W:r0x1161:NULL+0:4451:0
	;--cdb--W:r0x1162:NULL+0:4446:0
	;--cdb--W:r0x1162:NULL+0:14:0
	;--cdb--W:r0x1162:NULL+0:4452:0
	;--cdb--W:r0x1163:NULL+0:4449:0
	;--cdb--W:r0x1164:NULL+0:4450:0
	;--cdb--W:r0x1156:NULL+0:-1:1
	;--cdb--W:r0x115E:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:0:0
	;--cdb--W:r0x115D:NULL+0:0:0
	;--cdb--W:r0x115E:NULL+0:0:0
	;--cdb--W:r0x1155:NULL+0:0:0
	;--cdb--W:r0x1161:NULL+0:0:0
	;--cdb--W:r0x1162:NULL+0:0:0
	end
