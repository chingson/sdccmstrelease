;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"mblen.c"
	.module mblen
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$mblen$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--F:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0,0,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; mblen-code 
.globl _mblen

;--------------------------------------------------------
	.FUNC _mblen:$PNUM 4:$L:r0x1155:$L:_mblen_STK00:$L:_mblen_STK01:$L:_mblen_STK02:$L:r0x1158\
:$L:r0x1159
;--------------------------------------------------------
;	.line	31; "mblen.c"	int mblen(const char *s, size_t n)
_mblen:	;Function start
	STA	r0x1155
;	;.line	36; "mblen.c"	if(!s)
	ORA	_mblen_STK00
	JNZ	_00106_DS_
;	;.line	37; "mblen.c"	return(0);
	CLRA	
	STA	STK00
	JMP	_00123_DS_
_00106_DS_:
;	;.line	39; "mblen.c"	if(!n)
	LDA	_mblen_STK02
	ORA	_mblen_STK01
	JNZ	_00108_DS_
;	;.line	40; "mblen.c"	return(-1);
	LDA	#0xff
	STA	STK00
	JMP	_00123_DS_
_00108_DS_:
;	;.line	42; "mblen.c"	c = *s;
	LDA	_mblen_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1158
;	;.line	44; "mblen.c"	if(!c)
	JNZ	_00110_DS_
;	;.line	45; "mblen.c"	return(0);
	CLRA	
	STA	STK00
	JMP	_00123_DS_
_00110_DS_:
;	;.line	47; "mblen.c"	if(c <= 0x7f)
	SETB	_C
	LDA	#0x7f
	SUBB	r0x1158
	JNC	_00130_DS_
;	;.line	48; "mblen.c"	return(1);
	LDA	#0x01
	STA	STK00
	CLRA	
	JMP	_00123_DS_
_00130_DS_:
;	;.line	50; "mblen.c"	while(c & 0x80)
	CLRA	
	STA	r0x1159
_00113_DS_:
	LDA	r0x1158
	JPL	_00115_DS_
;	;.line	52; "mblen.c"	c <<= 1;
	LDA	r0x1158
	SHL	
	STA	r0x1158
;	;.line	53; "mblen.c"	m++;
	LDA	r0x1159
	INCA	
	STA	r0x1159
	JMP	_00113_DS_
_00115_DS_:
;	;.line	56; "mblen.c"	if(m > n)
	SETB	_C
	LDA	_mblen_STK02
	SUBB	r0x1159
	LDA	_mblen_STK01
	SUBB	#0x00
	JC	_00117_DS_
;	;.line	57; "mblen.c"	return(-1);
	LDA	#0xff
	STA	STK00
	JMP	_00123_DS_
_00117_DS_:
;	;.line	59; "mblen.c"	n = m;
	LDA	r0x1159
	STA	_mblen_STK02
;	;.line	60; "mblen.c"	while(--m)
	LDA	r0x1159
	STA	r0x1158
_00120_DS_:
	LDA	r0x1158
	DECA	
	STA	r0x1158
	JZ	_00122_DS_
;	;.line	61; "mblen.c"	if((*++s & 0xc0) != 0x80)
	LDA	_mblen_STK00
	INCA	
	STA	_mblen_STK00
	CLRA	
	ADDC	r0x1155
	STA	r0x1155
	LDA	_mblen_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0xc0
	XOR	#0x80
	JZ	_00120_DS_
;	;.line	62; "mblen.c"	return(-1);
	LDA	#0xff
	STA	STK00
	JMP	_00123_DS_
_00122_DS_:
;	;.line	64; "mblen.c"	return(n);
	LDA	_mblen_STK02
	STA	STK00
	CLRA	
_00123_DS_:
;	;.line	65; "mblen.c"	}
	RET	
; exit point of _mblen
	.ENDFUNC _mblen
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:Lmblen.aligned_alloc$size$65536$15({2}SI:U),E,0,0
	;--cdb--S:Lmblen.aligned_alloc$alignment$65536$15({2}SI:U),E,0,0
	;--cdb--S:Lmblen.mblen$n$65536$29({2}SI:U),R,0,0,[_mblen_STK02,_mblen_STK01]
	;--cdb--S:Lmblen.mblen$s$65536$29({2}DG,SC:U),R,0,0,[_mblen_STK00,r0x1155]
	;--cdb--S:Lmblen.mblen$m$65536$30({1}SC:U),R,0,0,[r0x1159]
	;--cdb--S:Lmblen.mblen$c$65536$30({1}SC:U),R,0,0,[r0x1158]
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_mblen
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_mblen_0	udata
r0x1155:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_mblen_STK00:	.ds	1
	.globl _mblen_STK00
_mblen_STK01:	.ds	1
	.globl _mblen_STK01
_mblen_STK02:	.ds	1
	.globl _mblen_STK02
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115B:NULL+0:-1:1
	;--cdb--W:r0x1158:NULL+0:4441:0
	;--cdb--W:r0x115A:NULL+0:4440:0
	;--cdb--W:r0x115A:NULL+0:0:0
	;--cdb--W:r0x115A:NULL+0:4441:0
	;--cdb--W:r0x115C:NULL+0:0:0
	;--cdb--W:r0x115A:NULL+0:-1:1
	;--cdb--W:r0x115C:NULL+0:-1:1
	;--cdb--W:r0x1159:NULL+0:-1:1
	;--cdb--W:_mblen_STK01:NULL+0:0:0
	;--cdb--W:_mblen_STK01:NULL+0:-1:1
	end
