

#include"ms326.h"
#include"ms326typedef.h"
#include"ms326sphlib.h"

// amp mode routines
extern USHORT api_endpage;
extern BYTE api_mode;
extern BYTE api_rampage;
extern BYTE api_fifostart;// unit in 16 bytes
extern BYTE api_fifoend;

void api_amp_prepare(BYTE pagv, BYTE fgp, BYTE fgr, BYTE dacgcl, BYTE adcgv, BYTE en5k,BYTE spkcv, BYTE *fifo, BYTE fifolen)
{
    api_fifostart=(((USHORT)(fifo))&0x7ff)>>4;
    api_fifoend=api_fifostart+fifolen-1;

    ADP_IND=0x80;
    PPAGES=RPAGES=(api_fifostart&0xf)|(((BYTE)(api_fifoend&0xf))<<4);
    ADP_IND=0;
    PPAGES=RPAGES=(api_fifostart>>4)|(((BYTE)(api_fifoend&0xf0))>>1);


    PAG=pagv;
    DAC_PL=0xff;
    DAC_PH=0x1; // dac period 0x1ff!!
    FILTERGP=fgp;
    FILTERGR=fgr;
    DACGCL=dacgcl;
    ADCG=adcgv;
    SPKC=(SPKC&0xcf)|spkcv;

    RDMAH=0x80;
    PDMAH=0x80; // reset to recording place
    OFFSETLH =0x0000; // we skip DC offset, because cap outside
    LVDCON=(LVDCON&0xEF)|en5k;
    //ADCON=1;
    SYSC2|=0xA0; // mbias
    RCLKDIV=3; // OSR HIGH!!!
    ADCON=0x41; // OSR, dma0 wakeup
    DCLAMP=0xF8;

}

void api_amp_start(unsigned char mode) // mode=1 means use PA7, mode=2 means CEXT-out
{
    RCLKDIV=0x3; // OSR HIGH!!
    SYSC2|=0xA0; // mbias+OSR256
    ADCON=0x43; // enable dma, no xxx

    //DC_cancel(12, NULL, (BYTE*)((((USHORT)api_fifostart)<<4)|0x8000));

    while(RDMAL!=2); // minimize the delay
    PDMAH=0x80;
    // 256 align
    if(mode==1)
    {
        DACON=0x9a;
        SPKC|=0x48;
    }
    else if(mode==2)
    {
	    ULAWC|=0x10;
    }else
    {
        DACON=0x9b; // just enable it

    }
}

void api_amp_stop(void)
{
    DACON=0;
    ULAWC&=0xEF;
    SPKC&=~0x48;
    SYSC2&=0x7f; // mbias off
    ADCON=0;
}
