;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_isprint.c"
	.module _isprint
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isprint$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isblank$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$islower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isupper$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _isprint-code 
.globl _isprint

;--------------------------------------------------------
	.FUNC _isprint:$PNUM 2:$L:r0x1155:$L:_isprint_STK00
;--------------------------------------------------------
;	.line	31; "_isprint.c"	int isprint (int c)
_isprint:	;Function start
	STA	r0x1155
;	;.line	33; "_isprint.c"	if ( c >= 0x20 && c <= 0x7e )
	LDA	_isprint_STK00
	ADD	#0xe0
	LDA	r0x1155
	XOR	#0x80
	ADDC	#0x7f
	JNC	_00106_DS_
	SETB	_C
	LDA	#0x7e
	SUBB	_isprint_STK00
	CLRA	
	SUBSI	
	SUBB	r0x1155
	JNC	_00106_DS_
;	;.line	34; "_isprint.c"	return 1;
	LDA	#0x01
	STA	STK00
	CLRA	
	JMP	_00108_DS_
_00106_DS_:
;	;.line	35; "_isprint.c"	return 0;
	CLRA	
	STA	STK00
_00108_DS_:
;	;.line	36; "_isprint.c"	}
	RET	
; exit point of _isprint
	.ENDFUNC _isprint
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:L_isprint.isblank$c$65536$13({2}SI:S),E,0,0
	;--cdb--S:L_isprint.isdigit$c$65536$15({2}SI:S),E,0,0
	;--cdb--S:L_isprint.islower$c$65536$17({2}SI:S),E,0,0
	;--cdb--S:L_isprint.isupper$c$65536$19({2}SI:S),E,0,0
	;--cdb--S:L_isprint.isprint$c$65536$21({2}SI:S),R,0,0,[_isprint_STK00,r0x1155]
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_isprint
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__isprint_0	udata
r0x1155:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_isprint_STK00:	.ds	1
	.globl _isprint_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	end
