;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_fslt.c"
	.module _fslt
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:F_fslt$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$__fslt$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _fslt-code 
.globl ___fslt

;--------------------------------------------------------
	.FUNC ___fslt:$PNUM 8:$L:r0x1157:$L:___fslt_STK00:$L:___fslt_STK01:$L:___fslt_STK02:$L:___fslt_STK03\
:$L:___fslt_STK04:$L:___fslt_STK05:$L:___fslt_STK06:$L:___fslt_fl1_65536_21:$L:___fslt_fl2_65536_21\

;--------------------------------------------------------
;	.line	55; "_fslt.c"	char __fslt (float a1, float a2) _FS_REENTRANT
___fslt:	;Function start
	STA	r0x1157
	LDA	___fslt_STK06
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	59; "_fslt.c"	fl1.f = a1;
	LDA	___fslt_STK02
	STA	___fslt_fl1_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	___fslt_STK01
	STA	(___fslt_fl1_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	___fslt_STK00
	STA	(___fslt_fl1_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1157
	STA	(___fslt_fl1_65536_21 + 3)
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	60; "_fslt.c"	fl2.f = a2;
	LDA	___fslt_STK06
	STA	___fslt_fl2_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	___fslt_STK05
	STA	(___fslt_fl2_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	___fslt_STK04
	STA	(___fslt_fl2_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	___fslt_STK03
	STA	(___fslt_fl2_65536_21 + 3)
;	;.line	62; "_fslt.c"	if (fl1.l<0 && fl2.l<0) {
	LDA	(___fslt_fl1_65536_21 + 3)
	JPL	_00108_DS_
	LDA	(___fslt_fl2_65536_21 + 3)
	JPL	_00108_DS_
;	;.line	63; "_fslt.c"	if (fl2.l < fl1.l)
	SETB	_C
	LDA	___fslt_fl2_65536_21
	SUBB	___fslt_fl1_65536_21
	LDA	(___fslt_fl2_65536_21 + 1)
	SUBB	(___fslt_fl1_65536_21 + 1)
	LDA	(___fslt_fl2_65536_21 + 2)
	SUBB	(___fslt_fl1_65536_21 + 2)
	LDA	(___fslt_fl2_65536_21 + 3)
	SUBSI	
	SUBB	(___fslt_fl1_65536_21 + 3)
	JC	_00106_DS_
;	;.line	64; "_fslt.c"	return (1);
	LDA	#0x01
	JMP	_00112_DS_
_00106_DS_:
;	;.line	65; "_fslt.c"	return (0);
	CLRA	
	JMP	_00112_DS_
_00108_DS_:
;	;.line	68; "_fslt.c"	if (fl1.l < fl2.l)
	SETB	_C
	LDA	___fslt_fl1_65536_21
	SUBB	___fslt_fl2_65536_21
	LDA	(___fslt_fl1_65536_21 + 1)
	SUBB	(___fslt_fl2_65536_21 + 1)
	LDA	(___fslt_fl1_65536_21 + 2)
	SUBB	(___fslt_fl2_65536_21 + 2)
	LDA	(___fslt_fl1_65536_21 + 3)
	SUBSI	
	SUBB	(___fslt_fl2_65536_21 + 3)
	JC	_00111_DS_
;	;.line	69; "_fslt.c"	return (1);
	LDA	#0x01
	JMP	_00112_DS_
_00111_DS_:
;	;.line	70; "_fslt.c"	return (0);
	CLRA	
_00112_DS_:
;	;.line	71; "_fslt.c"	}
	RET	
; exit point of ___fslt
	.ENDFUNC ___fslt
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:L_fslt.__fslt$a2$65536$20({4}SF:S),R,0,0,[___fslt_STK06,___fslt_STK05,___fslt_STK04,___fslt_STK03]
	;--cdb--S:L_fslt.__fslt$a1$65536$20({4}SF:S),R,0,0,[___fslt_STK02,___fslt_STK01,___fslt_STK00,r0x1157]
	;--cdb--S:L_fslt.__fslt$fl1$65536$21({4}STfloat_long:S),E,0,0
	;--cdb--S:L_fslt.__fslt$fl2$65536$21({4}STfloat_long:S),E,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___fslt
	.globl	___fslt_fl1_65536_21
	.globl	___fslt_fl2_65536_21
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
___fslt_fl1_65536_21:	.ds	4

	.area DSEG(DATA)
___fslt_fl2_65536_21:	.ds	4

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__fslt_0	udata
r0x1157:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
___fslt_STK00:	.ds	1
	.globl ___fslt_STK00
___fslt_STK01:	.ds	1
	.globl ___fslt_STK01
___fslt_STK02:	.ds	1
	.globl ___fslt_STK02
___fslt_STK03:	.ds	1
	.globl ___fslt_STK03
___fslt_STK04:	.ds	1
	.globl ___fslt_STK04
___fslt_STK05:	.ds	1
	.globl ___fslt_STK05
___fslt_STK06:	.ds	1
	.globl ___fslt_STK06
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:___fslt_STK02:NULL+0:-1:1
	;--cdb--W:___fslt_STK01:NULL+0:-1:1
	;--cdb--W:___fslt_STK00:NULL+0:-1:1
	;--cdb--W:r0x1157:NULL+0:-1:1
	end
