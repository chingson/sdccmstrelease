;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_lltoa.c"
	.module _lltoa
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_ulltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$_ulltoa$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$_modulonglong$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_divulonglong$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_lltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$_lltoa$0$0({2}DF,SV:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _lltoa-code 
.globl __lltoa

;--------------------------------------------------------
	.FUNC __lltoa:$PNUM 7:$C:__ulltoa\
:$L:r0x119E:$L:__lltoa_STK00:$L:__lltoa_STK01:$L:__lltoa_STK02:$L:__lltoa_STK03\
:$L:__lltoa_STK04:$L:__lltoa_STK05:$L:r0x11A6:$L:r0x11A7:$L:r0x11A8\
:$L:r0x11A9
;--------------------------------------------------------
;	.line	34; "_lltoa.c"	void _lltoa(long value, __xdata char* string, unsigned char radix)
__lltoa:	;Function start
	STA	r0x119E
;	;.line	36; "_lltoa.c"	if (value < 0 && radix == 10) {
	JPL	_00146_DS_
	LDA	__lltoa_STK05
	XOR	#0x0a
	JNZ	_00146_DS_
;	;.line	37; "_lltoa.c"	*string++ = '-';
	LDA	__lltoa_STK04
	STA	_ROMPL
	LDA	__lltoa_STK03
	STA	_ROMPH
	LDA	#0x2d
	STA	@_ROMP
	LDA	__lltoa_STK04
	INCA	
	STA	__lltoa_STK04
	CLRA	
	ADDC	__lltoa_STK03
	STA	__lltoa_STK03
;	;.line	38; "_lltoa.c"	value = -value;
	SETB	_C
	CLRA	
	SUBB	__lltoa_STK02
	STA	__lltoa_STK02
	CLRA	
	SUBB	__lltoa_STK01
	STA	__lltoa_STK01
	CLRA	
	SUBB	__lltoa_STK00
	STA	__lltoa_STK00
	CLRA	
	SUBB	r0x119E
	STA	r0x119E
_00146_DS_:
;	;.line	40; "_lltoa.c"	_ulltoa(value, string, radix);
	LDA	r0x119E
	JPL	_00161_DS_
	LDA	#0xff
	JMP	_00162_DS_
_00161_DS_:
	CLRA	
_00162_DS_:
	STA	r0x11A6
	STA	r0x11A7
	STA	r0x11A8
	STA	r0x11A9
	LDA	__lltoa_STK05
	STA	__ulltoa_STK09
	LDA	__lltoa_STK04
	STA	__ulltoa_STK08
	LDA	__lltoa_STK03
	STA	__ulltoa_STK07
	LDA	__lltoa_STK02
	STA	__ulltoa_STK06
	LDA	__lltoa_STK01
	STA	__ulltoa_STK05
	LDA	__lltoa_STK00
	STA	__ulltoa_STK04
	LDA	r0x119E
	STA	__ulltoa_STK03
	LDA	r0x11A6
	STA	__ulltoa_STK02
	LDA	r0x11A7
	STA	__ulltoa_STK01
	LDA	r0x11A8
	STA	__ulltoa_STK00
	LDA	r0x11A9
	CALL	__ulltoa
;	;.line	41; "_lltoa.c"	}
	RET	
; exit point of __lltoa
	.ENDFUNC __lltoa
.globl __ulltoa

;--------------------------------------------------------
	.FUNC __ulltoa:$PNUM 11:$C:__modulonglong:$C:__divulonglong\
:$L:r0x115B:$L:__ulltoa_STK00:$L:__ulltoa_STK01:$L:__ulltoa_STK02:$L:__ulltoa_STK03\
:$L:__ulltoa_STK04:$L:__ulltoa_STK05:$L:__ulltoa_STK06:$L:__ulltoa_STK07:$L:__ulltoa_STK08\
:$L:__ulltoa_STK09:$L:r0x115F:$L:r0x1168:$L:r0x1170:$L:__ulltoa_buffer_65536_2\
:$L:r0x1171
;--------------------------------------------------------
;	.line	14; "_lltoa.c"	void _ulltoa(unsigned long long value, __xdata char* string, unsigned char radix)
__ulltoa:	;Function start
	STA	r0x115B
;	;.line	19; "_lltoa.c"	do {
	LDA	#0x20
	STA	r0x115F
_00107_DS_:
;	;.line	20; "_lltoa.c"	unsigned char c = '0' + (value % radix);
	LDA	__ulltoa_STK09
	STA	__modulonglong_STK14
	CLRA	
	STA	__modulonglong_STK13
	STA	__modulonglong_STK12
	STA	__modulonglong_STK11
	STA	__modulonglong_STK10
	CLRA	
	STA	__modulonglong_STK09
	STA	__modulonglong_STK08
	STA	__modulonglong_STK07
	LDA	__ulltoa_STK06
	STA	__modulonglong_STK06
	LDA	__ulltoa_STK05
	STA	__modulonglong_STK05
	LDA	__ulltoa_STK04
	STA	__modulonglong_STK04
	LDA	__ulltoa_STK03
	STA	__modulonglong_STK03
	LDA	__ulltoa_STK02
	STA	__modulonglong_STK02
	LDA	__ulltoa_STK01
	STA	__modulonglong_STK01
	LDA	__ulltoa_STK00
	STA	__modulonglong_STK00
	LDA	r0x115B
	CALL	__modulonglong
	LDA	STK06
	ADD	#0x30
	STA	r0x1168
;	;.line	21; "_lltoa.c"	if (c > (unsigned char)'9')
	SETB	_C
	LDA	#0x39
	SUBB	r0x1168
	JC	_00106_DS_
;	;.line	22; "_lltoa.c"	c += 'A' - '9' - 1;
	LDA	r0x1168
	ADD	#0x07
	STA	r0x1168
_00106_DS_:
;	;.line	23; "_lltoa.c"	buffer[--index] = c;
	LDA	r0x115F
	DECA	
	STA	r0x115F
	ADD	#(__ulltoa_buffer_65536_2 + 0)
	STA	r0x1170
	CLRA	
	ADDC	#high (__ulltoa_buffer_65536_2 + 0)
	STA	r0x1171
	LDA	r0x1170
	STA	_ROMPL
	LDA	r0x1171
	STA	_ROMPH
	LDA	r0x1168
	STA	@_ROMP
;	;.line	24; "_lltoa.c"	value /= radix;
	LDA	__ulltoa_STK09
	STA	__divulonglong_STK14
	CLRA	
	STA	__divulonglong_STK13
	STA	__divulonglong_STK12
	STA	__divulonglong_STK11
	STA	__divulonglong_STK10
	CLRA	
	STA	__divulonglong_STK09
	STA	__divulonglong_STK08
	STA	__divulonglong_STK07
	LDA	__ulltoa_STK06
	STA	__divulonglong_STK06
	LDA	__ulltoa_STK05
	STA	__divulonglong_STK05
	LDA	__ulltoa_STK04
	STA	__divulonglong_STK04
	LDA	__ulltoa_STK03
	STA	__divulonglong_STK03
	LDA	__ulltoa_STK02
	STA	__divulonglong_STK02
	LDA	__ulltoa_STK01
	STA	__divulonglong_STK01
	LDA	__ulltoa_STK00
	STA	__divulonglong_STK00
	LDA	r0x115B
	CALL	__divulonglong
	STA	r0x115B
	LDA	STK00
	STA	__ulltoa_STK00
	LDA	STK01
	STA	__ulltoa_STK01
	LDA	STK02
	STA	__ulltoa_STK02
	LDA	STK03
	STA	__ulltoa_STK03
	LDA	STK04
	STA	__ulltoa_STK04
	LDA	STK05
	STA	__ulltoa_STK05
	LDA	STK06
	STA	__ulltoa_STK06
;	;.line	25; "_lltoa.c"	} while (value);
	ORA	__ulltoa_STK05
	ORA	__ulltoa_STK04
	ORA	__ulltoa_STK03
	ORA	__ulltoa_STK02
	ORA	__ulltoa_STK01
	ORA	__ulltoa_STK00
	ORA	r0x115B
	JNZ	_00107_DS_
;	;.line	27; "_lltoa.c"	do {
	LDA	__ulltoa_STK08
	STA	__ulltoa_STK06
	LDA	__ulltoa_STK07
	STA	__ulltoa_STK05
	LDA	r0x115F
	STA	__ulltoa_STK04
_00110_DS_:
;	;.line	28; "_lltoa.c"	*string++ = buffer[index];
	LDA	__ulltoa_STK04
	ADD	#(__ulltoa_buffer_65536_2 + 0)
	STA	__ulltoa_STK03
	CLRA	
	ADDC	#high (__ulltoa_buffer_65536_2 + 0)
	STA	__ulltoa_STK02
	LDA	__ulltoa_STK03
	STA	_ROMPL
	LDA	__ulltoa_STK02
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	__ulltoa_STK01
	LDA	__ulltoa_STK06
	STA	_ROMPL
	LDA	__ulltoa_STK05
	STA	_ROMPH
	LDA	__ulltoa_STK01
	STA	@_ROMP
	LDA	__ulltoa_STK06
	INCA	
	STA	__ulltoa_STK06
	CLRA	
	ADDC	__ulltoa_STK05
	STA	__ulltoa_STK05
;	;.line	29; "_lltoa.c"	} while ( ++index != NUMBER_OF_DIGITS );
	LDA	__ulltoa_STK04
	INCA	
	STA	__ulltoa_STK04
	XOR	#0x20
	JNZ	_00110_DS_
;	;.line	31; "_lltoa.c"	*string = 0;  /* string terminator */
	LDA	__ulltoa_STK06
	STA	_ROMPL
	LDA	__ulltoa_STK05
	STA	_ROMPH
	CLRA	
	STA	@_ROMP
;	;.line	32; "_lltoa.c"	}
	RET	
; exit point of __ulltoa
	.ENDFUNC __ulltoa
	;--cdb--S:G$_ulltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_modulonglong$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_divulonglong$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_lltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:L_lltoa._ulltoa$radix$65536$1({1}SC:U),R,0,0,[__ulltoa_STK09]
	;--cdb--S:L_lltoa._ulltoa$string$65536$1({2}DX,SC:U),R,0,0,[__ulltoa_STK08,__ulltoa_STK07]
	;--cdb--S:L_lltoa._ulltoa$value$65536$1({8}SI:U),R,0,0,[__ulltoa_STK06,__ulltoa_STK05,__ulltoa_STK04,__ulltoa_STK03__ulltoa_STK02__ulltoa_STK01__ulltoa_STK00r0x115B]
	;--cdb--S:L_lltoa._ulltoa$buffer$65536$2({32}DA32d,SC:U),E,0,0
	;--cdb--S:L_lltoa._ulltoa$index$65536$2({1}SC:U),R,0,0,[r0x115F]
	;--cdb--S:L_lltoa._ulltoa$c$131072$3({1}SC:U),R,0,0,[r0x1168]
	;--cdb--S:L_lltoa._lltoa$radix$65536$5({1}SC:U),R,0,0,[__lltoa_STK05]
	;--cdb--S:L_lltoa._lltoa$string$65536$5({2}DX,SC:U),R,0,0,[__lltoa_STK04,__lltoa_STK03]
	;--cdb--S:L_lltoa._lltoa$value$65536$5({4}SL:S),R,0,0,[__lltoa_STK02,__lltoa_STK01,__lltoa_STK00,r0x119E]
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__modulonglong
	.globl	__divulonglong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__ulltoa
	.globl	__lltoa
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__lltoa_0	udata
r0x115B:	.ds	1
r0x115F:	.ds	1
r0x1168:	.ds	1
r0x1170:	.ds	1
r0x1171:	.ds	1
r0x119E:	.ds	1
r0x11A6:	.ds	1
r0x11A7:	.ds	1
r0x11A8:	.ds	1
r0x11A9:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__ulltoa_STK00:	.ds	1
	.globl __ulltoa_STK00
__ulltoa_STK01:	.ds	1
	.globl __ulltoa_STK01
__ulltoa_STK02:	.ds	1
	.globl __ulltoa_STK02
__ulltoa_STK03:	.ds	1
	.globl __ulltoa_STK03
__ulltoa_STK04:	.ds	1
	.globl __ulltoa_STK04
__ulltoa_STK05:	.ds	1
	.globl __ulltoa_STK05
__ulltoa_STK06:	.ds	1
	.globl __ulltoa_STK06
__ulltoa_STK07:	.ds	1
	.globl __ulltoa_STK07
__ulltoa_STK08:	.ds	1
	.globl __ulltoa_STK08
__ulltoa_STK09:	.ds	1
	.globl __ulltoa_STK09
	.globl __modulonglong_STK14
	.globl __modulonglong_STK13
	.globl __modulonglong_STK12
	.globl __modulonglong_STK11
	.globl __modulonglong_STK10
	.globl __modulonglong_STK09
	.globl __modulonglong_STK08
	.globl __modulonglong_STK07
	.globl __modulonglong_STK06
	.globl __modulonglong_STK05
	.globl __modulonglong_STK04
	.globl __modulonglong_STK03
	.globl __modulonglong_STK02
	.globl __modulonglong_STK01
	.globl __modulonglong_STK00
__ulltoa_buffer_65536_2:	.ds	32
	.globl __divulonglong_STK14
	.globl __divulonglong_STK13
	.globl __divulonglong_STK12
	.globl __divulonglong_STK11
	.globl __divulonglong_STK10
	.globl __divulonglong_STK09
	.globl __divulonglong_STK08
	.globl __divulonglong_STK07
	.globl __divulonglong_STK06
	.globl __divulonglong_STK05
	.globl __divulonglong_STK04
	.globl __divulonglong_STK03
	.globl __divulonglong_STK02
	.globl __divulonglong_STK01
	.globl __divulonglong_STK00
__lltoa_STK00:	.ds	1
	.globl __lltoa_STK00
__lltoa_STK01:	.ds	1
	.globl __lltoa_STK01
__lltoa_STK02:	.ds	1
	.globl __lltoa_STK02
__lltoa_STK03:	.ds	1
	.globl __lltoa_STK03
__lltoa_STK04:	.ds	1
	.globl __lltoa_STK04
__lltoa_STK05:	.ds	1
	.globl __lltoa_STK05
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x11A2:NULL+0:4524:0
	;--cdb--W:r0x11A3:NULL+0:4523:0
	;--cdb--W:r0x11A4:NULL+0:4522:0
	;--cdb--W:r0x11A5:NULL+0:4510:0
	;--cdb--W:r0x116F:NULL+0:-1:1
	;--cdb--W:r0x116E:NULL+0:-1:1
	;--cdb--W:r0x116D:NULL+0:-1:1
	;--cdb--W:r0x116C:NULL+0:-1:1
	;--cdb--W:r0x116B:NULL+0:-1:1
	;--cdb--W:r0x116A:NULL+0:-1:1
	;--cdb--W:r0x1169:NULL+0:-1:1
	;--cdb--W:r0x1168:NULL+0:-1:1
	;--cdb--W:r0x1170:NULL+0:-1:1
	;--cdb--W:r0x1160:NULL+0:4475:0
	;--cdb--W:r0x1168:NULL+0:8:0
	;--cdb--W:r0x1170:NULL+0:8:0
	;--cdb--W:r0x1161:NULL+0:0:0
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x1162:NULL+0:0:0
	;--cdb--W:r0x1162:NULL+0:-1:1
	;--cdb--W:r0x1163:NULL+0:0:0
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x1164:NULL+0:0:0
	;--cdb--W:r0x1164:NULL+0:-1:1
	;--cdb--W:r0x1165:NULL+0:0:0
	;--cdb--W:r0x1165:NULL+0:-1:1
	;--cdb--W:r0x1166:NULL+0:0:0
	;--cdb--W:r0x1166:NULL+0:-1:1
	;--cdb--W:r0x1167:NULL+0:0:0
	;--cdb--W:r0x1167:NULL+0:-1:1
	end
