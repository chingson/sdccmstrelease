;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_divslonglong.c"
	.module _divslonglong
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_divslonglong$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$_divslonglong$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$_divulonglong$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _divslonglong-code 
.globl __divslonglong

;--------------------------------------------------------
	.FUNC __divslonglong:$PNUM 16:$C:__divulonglong\
:$L:r0x115B:$L:__divslonglong_STK00:$L:__divslonglong_STK01:$L:__divslonglong_STK02:$L:__divslonglong_STK03\
:$L:__divslonglong_STK04:$L:__divslonglong_STK05:$L:__divslonglong_STK06:$L:__divslonglong_STK07:$L:__divslonglong_STK08\
:$L:__divslonglong_STK09:$L:__divslonglong_STK10:$L:__divslonglong_STK11:$L:__divslonglong_STK12:$L:__divslonglong_STK13\
:$L:__divslonglong_STK14:$L:r0x1164:$L:r0x1165
;--------------------------------------------------------
;	.line	36; "_divslonglong.c"	_divslonglong (long long numerator, long long denominator)
__divslonglong:	;Function start
	STA	r0x115B
;	;.line	38; "_divslonglong.c"	bool numeratorneg = (numerator < 0);
	XOR	#0x80
	ROL	
	CLRA	
	ROL	
	XOR	#0x01
	STA	r0x1164
;	;.line	39; "_divslonglong.c"	bool denominatorneg = (denominator < 0);
	LDA	__divslonglong_STK07
	XOR	#0x80
	ROL	
	CLRA	
	ROL	
	XOR	#0x01
	STA	r0x1165
;	;.line	42; "_divslonglong.c"	if (numeratorneg)
	LDA	r0x1164
	JZ	_00106_DS_
;	;.line	43; "_divslonglong.c"	numerator = -numerator;
	SETB	_C
	CLRA	
	SUBB	__divslonglong_STK06
	STA	__divslonglong_STK06
	CLRA	
	SUBB	__divslonglong_STK05
	STA	__divslonglong_STK05
	CLRA	
	SUBB	__divslonglong_STK04
	STA	__divslonglong_STK04
	CLRA	
	SUBB	__divslonglong_STK03
	STA	__divslonglong_STK03
	CLRA	
	SUBB	__divslonglong_STK02
	STA	__divslonglong_STK02
	CLRA	
	SUBB	__divslonglong_STK01
	STA	__divslonglong_STK01
	CLRA	
	SUBB	__divslonglong_STK00
	STA	__divslonglong_STK00
	CLRA	
	SUBB	r0x115B
	STA	r0x115B
_00106_DS_:
;	;.line	44; "_divslonglong.c"	if (denominatorneg)
	LDA	r0x1165
	JZ	_00108_DS_
;	;.line	45; "_divslonglong.c"	denominator = -denominator;
	SETB	_C
	CLRA	
	SUBB	__divslonglong_STK14
	STA	__divslonglong_STK14
	CLRA	
	SUBB	__divslonglong_STK13
	STA	__divslonglong_STK13
	CLRA	
	SUBB	__divslonglong_STK12
	STA	__divslonglong_STK12
	CLRA	
	SUBB	__divslonglong_STK11
	STA	__divslonglong_STK11
	CLRA	
	SUBB	__divslonglong_STK10
	STA	__divslonglong_STK10
	CLRA	
	SUBB	__divslonglong_STK09
	STA	__divslonglong_STK09
	CLRA	
	SUBB	__divslonglong_STK08
	STA	__divslonglong_STK08
	CLRA	
	SUBB	__divslonglong_STK07
	STA	__divslonglong_STK07
_00108_DS_:
;	;.line	47; "_divslonglong.c"	d = (unsigned long long)numerator / (unsigned long long)denominator;
	LDA	__divslonglong_STK14
	STA	__divulonglong_STK14
	LDA	__divslonglong_STK13
	STA	__divulonglong_STK13
	LDA	__divslonglong_STK12
	STA	__divulonglong_STK12
	LDA	__divslonglong_STK11
	STA	__divulonglong_STK11
	LDA	__divslonglong_STK10
	STA	__divulonglong_STK10
	LDA	__divslonglong_STK09
	STA	__divulonglong_STK09
	LDA	__divslonglong_STK08
	STA	__divulonglong_STK08
	LDA	__divslonglong_STK07
	STA	__divulonglong_STK07
	LDA	__divslonglong_STK06
	STA	__divulonglong_STK06
	LDA	__divslonglong_STK05
	STA	__divulonglong_STK05
	LDA	__divslonglong_STK04
	STA	__divulonglong_STK04
	LDA	__divslonglong_STK03
	STA	__divulonglong_STK03
	LDA	__divslonglong_STK02
	STA	__divulonglong_STK02
	LDA	__divslonglong_STK01
	STA	__divulonglong_STK01
	LDA	__divslonglong_STK00
	STA	__divulonglong_STK00
	LDA	r0x115B
	CALL	__divulonglong
	STA	r0x115B
;	;.line	49; "_divslonglong.c"	return ((numeratorneg ^ denominatorneg) ? -d : d);
	LDA	r0x1165
	XOR	r0x1164
	JZ	_00111_DS_
	SETB	_C
	CLRA	
	SUBB	STK06
	STA	__divslonglong_STK14
	CLRA	
	SUBB	STK05
	STA	__divslonglong_STK13
	CLRA	
	SUBB	STK04
	STA	__divslonglong_STK12
	CLRA	
	SUBB	STK03
	STA	__divslonglong_STK11
	CLRA	
	SUBB	STK02
	STA	__divslonglong_STK10
	CLRA	
	SUBB	STK01
	STA	__divslonglong_STK09
	CLRA	
	SUBB	STK00
	STA	__divslonglong_STK08
	CLRA	
	SUBB	r0x115B
	STA	__divslonglong_STK07
	JMP	_00112_DS_
_00111_DS_:
	LDA	STK06
	STA	__divslonglong_STK14
	LDA	STK05
	STA	__divslonglong_STK13
	LDA	STK04
	STA	__divslonglong_STK12
	LDA	STK03
	STA	__divslonglong_STK11
	LDA	STK02
	STA	__divslonglong_STK10
	LDA	STK01
	STA	__divslonglong_STK09
	LDA	STK00
	STA	__divslonglong_STK08
	LDA	r0x115B
	STA	__divslonglong_STK07
_00112_DS_:
	LDA	__divslonglong_STK14
	STA	STK06
	LDA	__divslonglong_STK13
	STA	STK05
	LDA	__divslonglong_STK12
	STA	STK04
	LDA	__divslonglong_STK11
	STA	STK03
	LDA	__divslonglong_STK10
	STA	STK02
	LDA	__divslonglong_STK09
	STA	STK01
	LDA	__divslonglong_STK08
	STA	STK00
	LDA	__divslonglong_STK07
;	;.line	50; "_divslonglong.c"	}
	RET	
; exit point of __divslonglong
	.ENDFUNC __divslonglong
	;--cdb--S:G$_divslonglong$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_divulonglong$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_divslonglong._divslonglong$denominator$65536$1({8}SI:S),R,0,0,[__divslonglong_STK14,__divslonglong_STK13,__divslonglong_STK12,__divslonglong_STK11__divslonglong_STK10__divslonglong_STK09__divslonglong_STK08__divslonglong_STK07]
	;--cdb--S:L_divslonglong._divslonglong$numerator$65536$1({8}SI:S),R,0,0,[__divslonglong_STK06,__divslonglong_STK05,__divslonglong_STK04,__divslonglong_STK03__divslonglong_STK02__divslonglong_STK01__divslonglong_STK00r0x115B]
	;--cdb--S:L_divslonglong._divslonglong$numeratorneg$65536$2({1}:S),R,0,0,[r0x1164]
	;--cdb--S:L_divslonglong._divslonglong$denominatorneg$65536$2({1}:S),R,0,0,[r0x1165]
	;--cdb--S:L_divslonglong._divslonglong$d$65536$2({8}SI:S),R,0,0,[__divslonglong_STK06,__divslonglong_STK05,__divslonglong_STK04,__divslonglong_STK03__divslonglong_STK02__divslonglong_STK01__divslonglong_STK00r0x115B]
	;--cdb--S:G$_divslonglong$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__divulonglong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__divslonglong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__divslonglong_0	udata
r0x115B:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__divslonglong_STK00:	.ds	1
	.globl __divslonglong_STK00
__divslonglong_STK01:	.ds	1
	.globl __divslonglong_STK01
__divslonglong_STK02:	.ds	1
	.globl __divslonglong_STK02
__divslonglong_STK03:	.ds	1
	.globl __divslonglong_STK03
__divslonglong_STK04:	.ds	1
	.globl __divslonglong_STK04
__divslonglong_STK05:	.ds	1
	.globl __divslonglong_STK05
__divslonglong_STK06:	.ds	1
	.globl __divslonglong_STK06
__divslonglong_STK07:	.ds	1
	.globl __divslonglong_STK07
__divslonglong_STK08:	.ds	1
	.globl __divslonglong_STK08
__divslonglong_STK09:	.ds	1
	.globl __divslonglong_STK09
__divslonglong_STK10:	.ds	1
	.globl __divslonglong_STK10
__divslonglong_STK11:	.ds	1
	.globl __divslonglong_STK11
__divslonglong_STK12:	.ds	1
	.globl __divslonglong_STK12
__divslonglong_STK13:	.ds	1
	.globl __divslonglong_STK13
__divslonglong_STK14:	.ds	1
	.globl __divslonglong_STK14
	.globl __divulonglong_STK14
	.globl __divulonglong_STK13
	.globl __divulonglong_STK12
	.globl __divulonglong_STK11
	.globl __divulonglong_STK10
	.globl __divulonglong_STK09
	.globl __divulonglong_STK08
	.globl __divulonglong_STK07
	.globl __divulonglong_STK06
	.globl __divulonglong_STK05
	.globl __divulonglong_STK04
	.globl __divulonglong_STK03
	.globl __divulonglong_STK02
	.globl __divulonglong_STK01
	.globl __divulonglong_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1164:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:4469:0
	;--cdb--W:__divslonglong_STK00:NULL+0:4470:0
	;--cdb--W:__divslonglong_STK00:NULL+0:14:0
	;--cdb--W:__divslonglong_STK01:NULL+0:4471:0
	;--cdb--W:__divslonglong_STK01:NULL+0:13:0
	;--cdb--W:__divslonglong_STK02:NULL+0:4472:0
	;--cdb--W:__divslonglong_STK02:NULL+0:12:0
	;--cdb--W:__divslonglong_STK03:NULL+0:4473:0
	;--cdb--W:__divslonglong_STK03:NULL+0:11:0
	;--cdb--W:__divslonglong_STK04:NULL+0:4474:0
	;--cdb--W:__divslonglong_STK04:NULL+0:10:0
	;--cdb--W:__divslonglong_STK05:NULL+0:4475:0
	;--cdb--W:__divslonglong_STK05:NULL+0:9:0
	;--cdb--W:__divslonglong_STK06:NULL+0:4476:0
	;--cdb--W:__divslonglong_STK06:NULL+0:8:0
	;--cdb--W:r0x1166:NULL+0:4468:0
	;--cdb--W:r0x1167:NULL+0:4467:0
	;--cdb--W:r0x1168:NULL+0:4466:0
	;--cdb--W:r0x1169:NULL+0:4465:0
	;--cdb--W:r0x116A:NULL+0:4464:0
	;--cdb--W:r0x116B:NULL+0:4463:0
	;--cdb--W:r0x116C:NULL+0:4462:0
	;--cdb--W:r0x116D:NULL+0:4443:0
	end
