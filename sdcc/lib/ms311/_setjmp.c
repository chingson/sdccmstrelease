/*-------------------------------------------------------------------------
   setjmp.c - source file for ANSI routines setjmp & longjmp

   Copyright (C) 1999, Sandeep Dutta . sandeep.dutta@usa.net

   This library is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any
   later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License 
   along with this library; see the file COPYING. If not, write to the
   Free Software Foundation, 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.

   As a special exception, if you link this library with other files,
   some of which are compiled with SDCC, to produce an executable,
   this library does not by itself cause the resulting executable to
   be covered by the GNU General Public License. This exception does
   not however invalidate any other reasons why the executable file
   might be covered by the GNU General Public License.
-------------------------------------------------------------------------*/

#include <ms311.h>
#include <setjmp.h>

static unsigned short __ramp0save;
static unsigned short __ramp1save;

int __setjmp (jmp_buf buf, unsigned char lowstk, unsigned char highstk)
{
    /* registers would have been saved on the
       stack anyway so we need to save SP
       and the return address */
	ROMPUW=(unsigned short)RAMP1UW;
	ROMPUW-=2;
	__ramp1save=0;
	__ramp1save=ROMPINC;
	__ramp1save|=(ROMPINC<<8);
	__ramp0save=ROMPUW-5;

    ROMPLH = buf;
    ROMPINC=lowstk;
    ROMPINC=highstk;
    ROMPINC=STACKL;
    ROMPINC=STACKH;
    ROMPINC=__ramp0save;
    ROMPINC=__ramp0save>>8;
    ROMPINC=__ramp1save;
    ROMPINC=__ramp1save>>8;
    return 0;
}
static int rv0;
static unsigned short stkchk;
_Noreturn void longjmp (jmp_buf buf, int rv)
{
    rv0=rv?rv:1;
    stkchk=0;
    ROMPLH = buf;
	__asm
		.globl STK00
		LDA	@_ROMPINC
		STA	_stkchk
		LDA	@_ROMPINC
		STA	(_stkchk+1)
back000:
		LDA	_STACKL
		XOR	_stkchk
		JNZ	next1;
		LDA	_STACKH
		XOR	(_stkchk+1)
		JZ	cont1;
		
next1:
		    LDA	#low(back000)
		    STA	_STACKL
		    LDA	#HIGH( back000)
		    STA	_STACKH
		    RET

cont1:
	call	cont2
cont2:
	    __endasm;
    STACKL=ROMPINC;
    STACKH=ROMPINC;
    RAMP0L=ROMPINC;
    RAMP0H=ROMPINC;
    RAMP1L=ROMPINC;
    RAMP1H=ROMPINC;
    __asm
	    LDA	_rv0
	    STA	STK00
	    LDA _rv0+1
	    ret
    __endasm;
}


