#--------------------------------------------------------------------------
#  pic14devices.txt - 14 bit 16Fxxx / 16Cxxx / 12Fxxx series device file
#  for SDCC
#
#  Copyright (C) 2006, Zik Saleeba <zik at zikzak.net>
#
#  This library is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the
#  Free Software Foundation; either version 2, or (at your option) any
#  later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License 
#  along with this library; see the file COPYING. If not, write to the
#  Free Software Foundation, 51 Franklin Street, Fifth Floor, Boston,
#   MA 02110-1301, USA.
#--------------------------------------------------------------------------

#
# dev = device name
# program = program memory in 14 bit words
# data = data memory in bytes
# eeprom = eeprom storage
# enhanced = 0 | 1
#   0: regular device (default)
#   1: indicate that this is an enhanced core (automatic context saving on IRQ)
# io = io lines
# maxram = maximum memmap address for unique general purpose registers
# bankmsk = mask for memmap bank selecting. 0x80 for two banks usable, 
#           0x180 for four.
# config = white-space separated list of config word addresses
# regmap = registers duplicated in multiple banks. First value is a bank bitmask,
#          following values are register addresses
# memmap <start> <end> <alias>
#	<start> - <end> mirrored in all banks set in <alias>
#	<alias> is a bitmask of bank bits (0x80, 0x100, 0x180)
#	Make sure to always provide at least one non-full (<alias> = <bankmsk>)
#	record or SDCC will assume that all usable memory is shared across all
#	banks!
#
#

#
# 10F series devices with 14 bit core
#

processor MS322, MS342
	program		4K
	data		256
	eeprom		0
	io		12
	maxram		0x5FF	
	config		0x2007

processor MSP61
	program		4K
	data		64
	eeprom		0
	io		12
	maxram		0x5FF	
	config		0x2007
	enhanced	1

processor MS205
	program		4K
	data		256
	eeprom		0
	io		8
	maxram		0x3FF	
	config		0x2007
	enhanced	2
