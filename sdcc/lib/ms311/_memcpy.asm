;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_memcpy.c"
	.module _memcpy
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--F:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0,0,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _memcpy-code 
.globl _memcpy

;--------------------------------------------------------
	.FUNC _memcpy:$PNUM 6:$L:r0x1155:$L:_memcpy_STK00:$L:_memcpy_STK01:$L:_memcpy_STK02:$L:_memcpy_STK03\
:$L:_memcpy_STK04:$L:r0x115A:$L:r0x115B:$L:r0x115D:$L:r0x115C\

;--------------------------------------------------------
;	.line	36; "_memcpy.c"	void * memcpy (void * dst, void * src, size_t acount)
_memcpy:	;Function start
	STA	r0x1155
;	;.line	38; "_memcpy.c"	void * ret = dst;
	LDA	_memcpy_STK00
	STA	r0x115A
	LDA	r0x1155
	STA	r0x115B
;	;.line	39; "_memcpy.c"	char * d = dst;
	LDA	r0x1155
	STA	r0x115D
	LDA	_memcpy_STK00
	STA	r0x115C
;	;.line	40; "_memcpy.c"	const char * s = src;
	LDA	_memcpy_STK01
	STA	r0x1155
	LDA	_memcpy_STK02
	STA	_memcpy_STK00
_00105_DS_:
;	;.line	45; "_memcpy.c"	while (acount--) {
	LDA	_memcpy_STK04
	STA	_memcpy_STK02
	LDA	_memcpy_STK03
	STA	_memcpy_STK01
	LDA	_memcpy_STK04
	DECA	
	STA	_memcpy_STK04
	LDA	#0xff
	ADDC	_memcpy_STK03
	STA	_memcpy_STK03
	LDA	_memcpy_STK02
	ORA	_memcpy_STK01
	JZ	_00107_DS_
;	;.line	46; "_memcpy.c"	*d++ = *s++;
	LDA	_memcpy_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_memcpy_STK02
	LDA	_memcpy_STK00
	INCA	
	STA	_memcpy_STK00
	CLRA	
	ADDC	r0x1155
	STA	r0x1155
	LDA	r0x115D
	STA	_ROMPH
	LDA	r0x115C
	STA	_ROMPL
	LDA	_memcpy_STK02
	STA	@_ROMPINC
	LDA	r0x115C
	INCA	
	STA	r0x115C
	CLRA	
	ADDC	r0x115D
	STA	r0x115D
	JMP	_00105_DS_
_00107_DS_:
;	;.line	49; "_memcpy.c"	return(ret);
	LDA	r0x115A
	STA	STK00
	LDA	r0x115B
;	;.line	50; "_memcpy.c"	}
	RET	
; exit point of _memcpy
	.ENDFUNC _memcpy
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_memcpy.memcpy$acount$65536$21({2}SI:U),R,0,0,[_memcpy_STK04,_memcpy_STK03]
	;--cdb--S:L_memcpy.memcpy$src$65536$21({2}DG,SV:S),R,0,0,[_memcpy_STK02,_memcpy_STK01]
	;--cdb--S:L_memcpy.memcpy$dst$65536$21({2}DG,SV:S),R,0,0,[_memcpy_STK00,r0x1155]
	;--cdb--S:L_memcpy.memcpy$ret$65536$22({2}DG,SV:S),R,0,0,[r0x115A,r0x115B]
	;--cdb--S:L_memcpy.memcpy$d$65536$22({2}DG,SC:U),R,0,0,[]
	;--cdb--S:L_memcpy.memcpy$s$65536$22({2}DG,SC:U),R,0,0,[]
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__memcpy_0	udata
r0x1155:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_memcpy_STK00:	.ds	1
	.globl _memcpy_STK00
_memcpy_STK01:	.ds	1
	.globl _memcpy_STK01
_memcpy_STK02:	.ds	1
	.globl _memcpy_STK02
_memcpy_STK03:	.ds	1
	.globl _memcpy_STK03
_memcpy_STK04:	.ds	1
	.globl _memcpy_STK04
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	end
