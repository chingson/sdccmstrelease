#define NUMBER_OF_DIGITS 32	/* eventually adapt if base 2 not needed */

#if NUMBER_OF_DIGITS < 32
# warning _ltoa() and _ultoa() are not save for radix 2
#endif

#if defined (__SDCC_mcs51) && defined (__SDCC_MODEL_SMALL) && !defined (__SDCC_STACK_AUTO)
# define MEMSPACE_BUFFER __xdata	/* eventually __pdata or __xdata */
# pragma nogcse
#else
# define MEMSPACE_BUFFER
#endif

void _ulltoa(unsigned long long value, __xdata char* string, unsigned char radix)
{
  char MEMSPACE_BUFFER buffer[NUMBER_OF_DIGITS];  /* no space for '\0' */
  unsigned char index = NUMBER_OF_DIGITS;

  do {
    unsigned char c = '0' + (value % radix);
    if (c > (unsigned char)'9')
       c += 'A' - '9' - 1;
    buffer[--index] = c;
    value /= radix;
  } while (value);

  do {
    *string++ = buffer[index];
  } while ( ++index != NUMBER_OF_DIGITS );

  *string = 0;  /* string terminator */
}

void _lltoa(long value, __xdata char* string, unsigned char radix)
{
  if (value < 0 && radix == 10) {
    *string++ = '-';
    value = -value;
  }
  _ulltoa(value, string, radix);
}

