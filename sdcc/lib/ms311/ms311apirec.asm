;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #bbc754a74 (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"D:\common\ms311sphlib"
;;	.file	"ms311apirec.c"
	.module ms311apirec
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Fms311apirec$adps[({0}S:S$predict$0$0({2}SI:S),Z,0,0)({2}S:S$index$0$0({1}SC:U),Z,0,0)]
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$api_rec_start$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$api_rec_prepare_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_b12_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
; *** BIT REGION *** (pseudo)
	.area UDATA (DATA,REL,CON)
; *** BIT REGION ***END
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; ms311apirec-code 
.globl _api_rec_stop

;--------------------------------------------------------
	.FUNC _api_rec_stop:$PNUM 1:$L:r0x117C
;--------------------------------------------------------
;	.line	61; "ms311apirec.c"	void api_rec_stop(BYTE add_end_code)
_api_rec_stop:	;Function start
	STA	r0x117C
;	;.line	63; "ms311apirec.c"	ADCON=0;
	CLRA	
	STA	_ADCON
;	;.line	64; "ms311apirec.c"	ULAWC&=0x3f;
	LDA	#0x3f
	AND	_ULAWC
	STA	_ULAWC
;	;.line	66; "ms311apirec.c"	if(!add_end_code)
	LDA	r0x117C
;	;.line	67; "ms311apirec.c"	return;
	JZ	_00144_DS_
;	;.line	68; "ms311apirec.c"	if((SPIM&0xf)!=0x0f)
	LDA	_SPIM
	AND	#0x0f
	XOR	#0x0f
;	;.line	69; "ms311apirec.c"	return;
	JNZ	_00144_DS_
;	;.line	70; "ms311apirec.c"	SPIOP=0x20;
	LDA	#0x20
	STA	_SPIOP
;	;.line	71; "ms311apirec.c"	if(SPIM==(BYTE)(api_endpage&0xff) && SPIH==(BYTE)(api_endpage>>8))
	LDA	_api_endpage
	XOR	_SPIM
	JNZ	_00142_DS_
	LDA	(_api_endpage + 1)
	XOR	_SPIH
;	;.line	72; "ms311apirec.c"	return;	
	JZ	_00144_DS_
_00142_DS_:
;	;.line	73; "ms311apirec.c"	SPIOP=1;
	LDA	#0x01
	STA	_SPIOP
;	;.line	74; "ms311apirec.c"	SPIOP=2;
	INCA	
	STA	_SPIOP
_00144_DS_:
;	;.line	75; "ms311apirec.c"	}
	RET	
; exit point of _api_rec_stop
	.ENDFUNC _api_rec_stop
.globl _api_rec_start

;--------------------------------------------------------
	.FUNC _api_rec_start:$PNUM 9:$C:_api_DC_cancel\
:$L:r0x1167:$L:_api_rec_start_STK00:$L:_api_rec_start_STK01:$L:_api_rec_start_STK02:$L:_api_rec_start_STK03\
:$L:_api_rec_start_STK04:$L:_api_rec_start_STK05:$L:_api_rec_start_STK06:$L:_api_rec_start_STK07
;--------------------------------------------------------
;	.line	23; "ms311apirec.c"	BYTE api_rec_start(BYTE dcdly, BYTE fg, BYTE ulaw, USHORT page_start, USHORT page_end,BYTE(*callback)(void))
_api_rec_start:	;Function start
	STA	r0x1167
;	;.line	25; "ms311apirec.c"	if(dcdly!=0xff)
	INCA	
	JZ	_00112_DS_
;	;.line	28; "ms311apirec.c"	if(!api_DC_cancel(dcdly,callback))
	LDA	_api_rec_start_STK07
	STA	_api_DC_cancel_STK01
	LDA	_api_rec_start_STK06
	STA	_api_DC_cancel_STK00
	LDA	r0x1167
	CALL	_api_DC_cancel
	JNZ	_00112_DS_
;	;.line	29; "ms311apirec.c"	return 0;
	CLRA	
	JMP	_00116_DS_
_00112_DS_:
;	;.line	35; "ms311apirec.c"	FILTERG=fg;
	LDA	_api_rec_start_STK00
	STA	_FILTERG
;	;.line	36; "ms311apirec.c"	SPIH=page_start>>8;
	LDA	_api_rec_start_STK02
	STA	_SPIH
;	;.line	37; "ms311apirec.c"	SPIM=page_start&0xff;
	LDA	_api_rec_start_STK03
	STA	_SPIM
;	;.line	38; "ms311apirec.c"	api_endpage=page_end;
	LDA	_api_rec_start_STK05
	STA	_api_endpage
	LDA	_api_rec_start_STK04
	STA	(_api_endpage + 1)
;	;.line	39; "ms311apirec.c"	api_ulaw=ulaw;
	LDA	_api_rec_start_STK01
	STA	_api_ulaw
;	;.line	40; "ms311apirec.c"	SPIL=0;
	CLRA	
	STA	_SPIL
;	;.line	41; "ms311apirec.c"	SPIOP=1; // erase it
	INCA	
	STA	_SPIOP
;	;.line	42; "ms311apirec.c"	SPIOP=2;
	LDA	#0x02
	STA	_SPIOP
;	;.line	45; "ms311apirec.c"	if(ulaw&1)
	LDA	_api_rec_start_STK01
	SHR	
	JNC	_00114_DS_
;	;.line	47; "ms311apirec.c"	ULAWC|=0xc0;
	LDA	_ULAWC
	ORA	#0xc0
	STA	_ULAWC
	JMP	_00115_DS_
_00114_DS_:
;	;.line	51; "ms311apirec.c"	ULAWC&=0x3f;
	LDA	#0x3f
	AND	_ULAWC
	STA	_ULAWC
;	;.line	52; "ms311apirec.c"	ADCON|=0x04;
	LDA	_ADCON
	ORA	#0x04
	STA	_ADCON
_00115_DS_:
;	;.line	54; "ms311apirec.c"	DMAH=0x80; // reset dma pointer	
	LDA	#0x80
	STA	_DMAH
;	;.line	55; "ms311apirec.c"	ADCON|=0x42; // DMA enable now, wakeup enable, too
	LDA	_ADCON
	ORA	#0x42
	STA	_ADCON
;	;.line	56; "ms311apirec.c"	api_rampage=1;
	LDA	#0x01
	STA	_api_rampage
_00116_DS_:
;	;.line	59; "ms311apirec.c"	}
	RET	
; exit point of _api_rec_start
	.ENDFUNC _api_rec_start
.globl _api_rec_prepare

;--------------------------------------------------------
	.FUNC _api_rec_prepare:$PNUM 3:$C:_api_clear_filter_mem\
:$L:r0x115A:$L:_api_rec_prepare_STK00:$L:_api_rec_prepare_STK01
;--------------------------------------------------------
;	.line	12; "ms311apirec.c"	void api_rec_prepare(BYTE osr, BYTE opag, BYTE en5k)
_api_rec_prepare:	;Function start
	STA	r0x115A
	LDA	_api_rec_prepare_STK01
;	;.line	14; "ms311apirec.c"	api_clear_filter_mem();
	CALL	_api_clear_filter_mem
;	;.line	15; "ms311apirec.c"	FILTERG=0x80; // for DC cancel
	LDA	#0x80
	STA	_FILTERG
;	;.line	16; "ms311apirec.c"	LVDCON|=en5k;
	LDA	_api_rec_prepare_STK01
	ORA	_LVDCON
	STA	_LVDCON
;	;.line	17; "ms311apirec.c"	ADCG=opag;
	LDA	_api_rec_prepare_STK00
	STA	_ADCG
;	;.line	18; "ms311apirec.c"	BGCON|=0x10; // mute
	LDA	_BGCON
	ORA	#0x10
	STA	_BGCON
;	;.line	19; "ms311apirec.c"	ADCON=osr|0x1;
	LDA	r0x115A
	ORA	#0x01
	STA	_ADCON
;	;.line	20; "ms311apirec.c"	OFFSETLH&=0x000f;
	LDA	_OFFSETLH
	AND	#0x0f
	STA	_OFFSETLH
	CLRA	
	STA	(_OFFSETLH + 1)
;	;.line	21; "ms311apirec.c"	}
	RET	
; exit point of _api_rec_prepare
	.ENDFUNC _api_rec_prepare
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_b12_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$IOR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IODIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IO$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0LH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1LH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAHL$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$init_io_state$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_endpage$0$0({2}SI:U),E,0,0
	;--cdb--S:G$api_ulaw$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_rampage$0$0({1}SC:U),E,0,0
	;--cdb--S:Lms311apirec.api_rec_prepare$en5k$65536$39({1}SC:U),R,0,0,[_api_rec_prepare_STK01]
	;--cdb--S:Lms311apirec.api_rec_prepare$opag$65536$39({1}SC:U),R,0,0,[_api_rec_prepare_STK00]
	;--cdb--S:Lms311apirec.api_rec_prepare$osr$65536$39({1}SC:U),R,0,0,[r0x115A]
	;--cdb--S:Lms311apirec.api_rec_start$callback$65536$41({2}DC,DF,SC:U),R,0,0,[_api_rec_start_STK07,_api_rec_start_STK06]
	;--cdb--S:Lms311apirec.api_rec_start$page_end$65536$41({2}SI:U),R,0,0,[_api_rec_start_STK05,_api_rec_start_STK04]
	;--cdb--S:Lms311apirec.api_rec_start$page_start$65536$41({2}SI:U),R,0,0,[_api_rec_start_STK03,_api_rec_start_STK02]
	;--cdb--S:Lms311apirec.api_rec_start$ulaw$65536$41({1}SC:U),R,0,0,[_api_rec_start_STK01]
	;--cdb--S:Lms311apirec.api_rec_start$fg$65536$41({1}SC:U),R,0,0,[_api_rec_start_STK00]
	;--cdb--S:Lms311apirec.api_rec_start$dcdly$65536$41({1}SC:U),R,0,0,[r0x1167]
	;--cdb--S:Lms311apirec.api_rec_stop$add_end_code$65536$47({1}SC:U),R,0,0,[r0x117C]
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_api_clear_filter_mem
	.globl	_api_DC_cancel
	.globl	_IOR
	.globl	_IODIR
	.globl	_IO
	.globl	_IOWK
	.globl	_IOWKDR
	.globl	_TIMERC
	.globl	_THRLD
	.globl	_RAMP0L
	.globl	_RAMP0H
	.globl	_RAMP1L
	.globl	_RAMP1H
	.globl	_PTRCL
	.globl	_PTRCH
	.globl	_ROMPL
	.globl	_ROMPH
	.globl	_BEEPC
	.globl	_FILTERG
	.globl	_ULAWC
	.globl	_STACKL
	.globl	_STACKH
	.globl	_ADCON
	.globl	_DACON
	.globl	_SYSC
	.globl	_SPIM
	.globl	_SPIH
	.globl	_SPIOP
	.globl	_SPI_BANK
	.globl	_ADP_IND
	.globl	_ADP_VPL
	.globl	_ADP_VPH
	.globl	_ADL
	.globl	_ADH
	.globl	_ZC
	.globl	_ADCG
	.globl	_DAC_PL
	.globl	_DAC_PH
	.globl	_PAG
	.globl	_DMAL
	.globl	_DMAH
	.globl	_SPIL
	.globl	_IOMASK
	.globl	_IOCMP
	.globl	_IOCNT
	.globl	_LVDCON
	.globl	_LVRCON
	.globl	_OFFSETL
	.globl	_OFFSETH
	.globl	_RCCON
	.globl	_BGCON
	.globl	_PWRL
	.globl	_CRYPT
	.globl	_PWRH
	.globl	_PWRHL
	.globl	_IROMDL
	.globl	_IROMDH
	.globl	_IROMDLH
	.globl	_RECMUTE
	.globl	_SPKC
	.globl	_DCLAMP
	.globl	_SSPIC
	.globl	_SSPIL
	.globl	_SSPIM
	.globl	_SSPIH
	.globl	_RAMP0
	.globl	_RAMP0LH
	.globl	_RAMP1LH
	.globl	_RAMP0INC
	.globl	_RAMP1
	.globl	_DMAHL
	.globl	_RAMP1INC
	.globl	_RAMP1INC2
	.globl	_ROMP
	.globl	_ROMPINC
	.globl	_ROMPLH
	.globl	_ROMPINC2
	.globl	_ACC
	.globl	_RAMP0UW
	.globl	_RAMP1UW
	.globl	_ROMPUW
	.globl	_SPIMH
	.globl	_OFFSETLH
	.globl	_ADP_VPLH
	.globl	_ICE0
	.globl	_ICE1
	.globl	_ICE2
	.globl	_ICE3
	.globl	_ICE4
	.globl	_TOV
	.globl	_init_io_state
	.globl	_api_endpage
	.globl	_api_ulaw
	.globl	_api_rampage

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_api_rec_stop
	.globl	_api_rec_start
	.globl	_api_rec_prepare
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_ms311apirec_0	udata
r0x115A:	.ds	1
r0x1167:	.ds	1
r0x117C:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_api_rec_prepare_STK00:	.ds	1
	.globl _api_rec_prepare_STK00
_api_rec_prepare_STK01:	.ds	1
	.globl _api_rec_prepare_STK01
_api_rec_start_STK00:	.ds	1
	.globl _api_rec_start_STK00
_api_rec_start_STK01:	.ds	1
	.globl _api_rec_start_STK01
_api_rec_start_STK02:	.ds	1
	.globl _api_rec_start_STK02
_api_rec_start_STK03:	.ds	1
	.globl _api_rec_start_STK03
_api_rec_start_STK04:	.ds	1
	.globl _api_rec_start_STK04
_api_rec_start_STK05:	.ds	1
	.globl _api_rec_start_STK05
_api_rec_start_STK06:	.ds	1
	.globl _api_rec_start_STK06
_api_rec_start_STK07:	.ds	1
	.globl _api_rec_start_STK07
	.globl _api_DC_cancel_STK01
	.globl _api_DC_cancel_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x117D:NULL+0:-1:1
	;--cdb--W:r0x117D:NULL+0:0:0
	;--cdb--W:r0x117C:NULL+0:-1:1
	;--cdb--W:_api_rec_start_STK00:NULL+0:-1:1
	;--cdb--W:r0x1167:NULL+0:4468:0
	;--cdb--W:r0x1171:NULL+0:4472:0
	;--cdb--W:r0x1170:NULL+0:4473:0
	;--cdb--W:r0x1167:NULL+0:-1:1
	;--cdb--W:_api_rec_prepare_STK00:NULL+0:-1:1
	;--cdb--W:_api_rec_prepare_STK00:NULL+0:0:0
	;--cdb--W:r0x115A:NULL+0:-1:1
	end
