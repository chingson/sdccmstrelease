;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"ceilf.c"
	.module ceilf
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Fceilf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$ceilf$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; ceilf-code 
.globl _ceilf

;--------------------------------------------------------
	.FUNC _ceilf:$PNUM 4:$C:___fs2slong:$C:___slong2fs:$C:___fslt\
:$L:r0x1157:$L:_ceilf_STK00:$L:_ceilf_STK01:$L:_ceilf_STK02:$L:r0x115B\
:$L:r0x115A:$L:r0x1159:$L:r0x1158:$L:r0x115F:$L:r0x115D\
:$L:r0x115C
;--------------------------------------------------------
;	.line	33; "ceilf.c"	float ceilf(float x) _FLOAT_FUNC_REENTRANT
_ceilf:	;Function start
	STA	r0x1157
;	;.line	36; "ceilf.c"	r=x;
	LDA	_ceilf_STK02
	STA	___fs2slong_STK02
	LDA	_ceilf_STK01
	STA	___fs2slong_STK01
	LDA	_ceilf_STK00
	STA	___fs2slong_STK00
	LDA	r0x1157
	CALL	___fs2slong
	STA	r0x115B
	LDA	STK00
	STA	r0x115A
	LDA	STK01
	STA	r0x1159
	LDA	STK02
	STA	r0x1158
;	;.line	37; "ceilf.c"	if (r<0)
	LDA	r0x115B
	JPL	_00106_DS_
;	;.line	38; "ceilf.c"	return r;
	LDA	r0x1158
	STA	___slong2fs_STK02
	LDA	r0x1159
	STA	___slong2fs_STK01
	LDA	r0x115A
	STA	___slong2fs_STK00
	LDA	r0x115B
	CALL	___slong2fs
	JMP	_00108_DS_
_00106_DS_:
;	;.line	40; "ceilf.c"	return (r+((r<x)?1:0));
	LDA	r0x1158
	STA	___slong2fs_STK02
	LDA	r0x1159
	STA	___slong2fs_STK01
	LDA	r0x115A
	STA	___slong2fs_STK00
	LDA	r0x115B
	CALL	___slong2fs
	STA	r0x115F
	LDA	_ceilf_STK02
	STA	___fslt_STK06
	LDA	_ceilf_STK01
	STA	___fslt_STK05
	LDA	_ceilf_STK00
	STA	___fslt_STK04
	LDA	r0x1157
	STA	___fslt_STK03
	LDA	STK02
	STA	___fslt_STK02
	LDA	STK01
	STA	___fslt_STK01
	LDA	STK00
	STA	___fslt_STK00
	LDA	r0x115F
	CALL	___fslt
	JZ	_00110_DS_
	LDA	#0x01
	STA	_ceilf_STK02
	CLRA	
	STA	_ceilf_STK01
	JMP	_00111_DS_
_00110_DS_:
	CLRA	
	STA	_ceilf_STK02
	STA	_ceilf_STK01
_00111_DS_:
	LDA	_ceilf_STK01
	JPL	_00121_DS_
	LDA	#0xff
	JMP	_00122_DS_
_00121_DS_:
	CLRA	
_00122_DS_:
	STA	r0x115C
	STA	r0x115D
	LDA	r0x1158
	ADD	_ceilf_STK02
	STA	r0x1158
	LDA	r0x1159
	ADDC	_ceilf_STK01
	STA	r0x1159
	LDA	r0x115A
	ADDC	r0x115C
	STA	r0x115A
	LDA	r0x115B
	ADDC	r0x115D
	STA	r0x115B
	LDA	r0x1158
	STA	___slong2fs_STK02
	LDA	r0x1159
	STA	___slong2fs_STK01
	LDA	r0x115A
	STA	___slong2fs_STK00
	LDA	r0x115B
	CALL	___slong2fs
_00108_DS_:
;	;.line	41; "ceilf.c"	}
	RET	
; exit point of _ceilf
	.ENDFUNC _ceilf
	;--cdb--S:Lceilf.ceilf$x$65536$25({4}SF:S),R,0,0,[_ceilf_STK02,_ceilf_STK01,_ceilf_STK00,r0x1157]
	;--cdb--S:Lceilf.ceilf$r$65536$26({4}SL:S),R,0,0,[r0x1158,r0x1159,r0x115A,r0x115B]
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	___fs2slong
	.globl	___slong2fs
	.globl	___fslt

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_ceilf
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_ceilf_0	udata
r0x1157:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115F:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_ceilf_STK00:	.ds	1
	.globl _ceilf_STK00
_ceilf_STK01:	.ds	1
	.globl _ceilf_STK01
_ceilf_STK02:	.ds	1
	.globl _ceilf_STK02
	.globl ___fs2slong_STK02
	.globl ___fs2slong_STK01
	.globl ___fs2slong_STK00
	.globl ___slong2fs_STK02
	.globl ___slong2fs_STK01
	.globl ___slong2fs_STK00
	.globl ___fslt_STK06
	.globl ___fslt_STK05
	.globl ___fslt_STK04
	.globl ___fslt_STK03
	.globl ___fslt_STK02
	.globl ___fslt_STK01
	.globl ___fslt_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115C:NULL+0:-1:1
	;--cdb--W:_ceilf_STK02:NULL+0:-1:1
	;--cdb--W:r0x1157:NULL+0:4449:0
	;--cdb--W:_ceilf_STK00:NULL+0:4450:0
	;--cdb--W:_ceilf_STK00:NULL+0:14:0
	;--cdb--W:_ceilf_STK01:NULL+0:13:0
	;--cdb--W:_ceilf_STK02:NULL+0:12:0
	;--cdb--W:r0x115E:NULL+0:14:0
	;--cdb--W:r0x115D:NULL+0:13:0
	;--cdb--W:r0x115C:NULL+0:12:0
	;--cdb--W:r0x115F:NULL+0:-1:1
	;--cdb--W:r0x1157:NULL+0:-1:1
	end
