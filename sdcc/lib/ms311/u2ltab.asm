;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #bbc754a74 (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"D:\common\ms311sphlib"
;;	.file	"u2ltab.c"
	.module u2ltab
	;.list	p=MS311
	.include "ms311sfr.def"
; *** BIT REGION *** (pseudo)
	.area UDATA (DATA,REL,CON)
; *** BIT REGION ***END
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; u2ltab-code 
	;--cdb--S:G$u2l_low$0$0({256}DA256d,SC:U),D,0,0
	;--cdb--S:G$u2l_high$0$0({256}DA256d,SC:U),D,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_u2l_low
	.globl	_u2l_high
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------

	.area	SEGC	(CODE) ;u2ltab-0-code

_u2l_low:
	.db #0x00	; 0
	.db #0x08	; 8
	.db #0x10	; 16
	.db #0x18	; 24
	.db #0x20	; 32
	.db #0x28	; 40
	.db #0x30	; 48	'0'
	.db #0x38	; 56	'8'
	.db #0x40	; 64
	.db #0x48	; 72	'H'
	.db #0x50	; 80	'P'
	.db #0x58	; 88	'X'
	.db #0x60	; 96
	.db #0x68	; 104	'h'
	.db #0x70	; 112	'p'
	.db #0x78	; 120	'x'
	.db #0x84	; 132
	.db #0x94	; 148
	.db #0xa4	; 164
	.db #0xb4	; 180
	.db #0xc4	; 196
	.db #0xd4	; 212
	.db #0xe4	; 228
	.db #0xf4	; 244
	.db #0x04	; 4
	.db #0x14	; 20
	.db #0x24	; 36
	.db #0x34	; 52	'4'
	.db #0x44	; 68	'D'
	.db #0x54	; 84	'T'
	.db #0x64	; 100	'd'
	.db #0x74	; 116	't'
	.db #0x8c	; 140
	.db #0xac	; 172
	.db #0xcc	; 204
	.db #0xec	; 236
	.db #0x0c	; 12
	.db #0x2c	; 44
	.db #0x4c	; 76	'L'
	.db #0x6c	; 108	'l'
	.db #0x8c	; 140
	.db #0xac	; 172
	.db #0xcc	; 204
	.db #0xec	; 236
	.db #0x0c	; 12
	.db #0x2c	; 44
	.db #0x4c	; 76	'L'
	.db #0x6c	; 108	'l'
	.db #0x9c	; 156
	.db #0xdc	; 220
	.db #0x1c	; 28
	.db #0x5c	; 92
	.db #0x9c	; 156
	.db #0xdc	; 220
	.db #0x1c	; 28
	.db #0x5c	; 92
	.db #0x9c	; 156
	.db #0xdc	; 220
	.db #0x1c	; 28
	.db #0x5c	; 92
	.db #0x9c	; 156
	.db #0xdc	; 220
	.db #0x1c	; 28
	.db #0x5c	; 92
	.db #0xbc	; 188
	.db #0x3c	; 60
	.db #0xbc	; 188
	.db #0x3c	; 60
	.db #0xbc	; 188
	.db #0x3c	; 60
	.db #0xbc	; 188
	.db #0x3c	; 60
	.db #0xbc	; 188
	.db #0x3c	; 60
	.db #0xbc	; 188
	.db #0x3c	; 60
	.db #0xbc	; 188
	.db #0x3c	; 60
	.db #0xbc	; 188
	.db #0x3c	; 60
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x7c	; 124
	.db #0x00	; 0
	.db #0xf8	; 248
	.db #0xf0	; 240
	.db #0xe8	; 232
	.db #0xe0	; 224
	.db #0xd8	; 216
	.db #0xd0	; 208
	.db #0xc8	; 200
	.db #0xc0	; 192
	.db #0xb8	; 184
	.db #0xb0	; 176
	.db #0xa8	; 168
	.db #0xa0	; 160
	.db #0x98	; 152
	.db #0x90	; 144
	.db #0x88	; 136
	.db #0x7c	; 124
	.db #0x6c	; 108	'l'
	.db #0x5c	; 92
	.db #0x4c	; 76	'L'
	.db #0x3c	; 60
	.db #0x2c	; 44
	.db #0x1c	; 28
	.db #0x0c	; 12
	.db #0xfc	; 252
	.db #0xec	; 236
	.db #0xdc	; 220
	.db #0xcc	; 204
	.db #0xbc	; 188
	.db #0xac	; 172
	.db #0x9c	; 156
	.db #0x8c	; 140
	.db #0x74	; 116	't'
	.db #0x54	; 84	'T'
	.db #0x34	; 52	'4'
	.db #0x14	; 20
	.db #0xf4	; 244
	.db #0xd4	; 212
	.db #0xb4	; 180
	.db #0x94	; 148
	.db #0x74	; 116	't'
	.db #0x54	; 84	'T'
	.db #0x34	; 52	'4'
	.db #0x14	; 20
	.db #0xf4	; 244
	.db #0xd4	; 212
	.db #0xb4	; 180
	.db #0x94	; 148
	.db #0x64	; 100	'd'
	.db #0x24	; 36
	.db #0xe4	; 228
	.db #0xa4	; 164
	.db #0x64	; 100	'd'
	.db #0x24	; 36
	.db #0xe4	; 228
	.db #0xa4	; 164
	.db #0x64	; 100	'd'
	.db #0x24	; 36
	.db #0xe4	; 228
	.db #0xa4	; 164
	.db #0x64	; 100	'd'
	.db #0x24	; 36
	.db #0xe4	; 228
	.db #0xa4	; 164
	.db #0x44	; 68	'D'
	.db #0xc4	; 196
	.db #0x44	; 68	'D'
	.db #0xc4	; 196
	.db #0x44	; 68	'D'
	.db #0xc4	; 196
	.db #0x44	; 68	'D'
	.db #0xc4	; 196
	.db #0x44	; 68	'D'
	.db #0xc4	; 196
	.db #0x44	; 68	'D'
	.db #0xc4	; 196
	.db #0x44	; 68	'D'
	.db #0xc4	; 196
	.db #0x44	; 68	'D'
	.db #0xc4	; 196
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132
	.db #0x84	; 132


	.area	SEGC	(CODE) ;u2ltab-1-code

_u2l_high:
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x01	; 1
	.db #0x01	; 1
	.db #0x01	; 1
	.db #0x01	; 1
	.db #0x01	; 1
	.db #0x01	; 1
	.db #0x01	; 1
	.db #0x01	; 1
	.db #0x01	; 1
	.db #0x01	; 1
	.db #0x01	; 1
	.db #0x01	; 1
	.db #0x02	; 2
	.db #0x02	; 2
	.db #0x02	; 2
	.db #0x02	; 2
	.db #0x02	; 2
	.db #0x02	; 2
	.db #0x02	; 2
	.db #0x02	; 2
	.db #0x03	; 3
	.db #0x03	; 3
	.db #0x03	; 3
	.db #0x03	; 3
	.db #0x03	; 3
	.db #0x03	; 3
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x05	; 5
	.db #0x05	; 5
	.db #0x05	; 5
	.db #0x05	; 5
	.db #0x06	; 6
	.db #0x06	; 6
	.db #0x06	; 6
	.db #0x06	; 6
	.db #0x07	; 7
	.db #0x07	; 7
	.db #0x07	; 7
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x09	; 9
	.db #0x09	; 9
	.db #0x0a	; 10
	.db #0x0a	; 10
	.db #0x0b	; 11
	.db #0x0b	; 11
	.db #0x0c	; 12
	.db #0x0c	; 12
	.db #0x0d	; 13
	.db #0x0d	; 13
	.db #0x0e	; 14
	.db #0x0e	; 14
	.db #0x0f	; 15
	.db #0x0f	; 15
	.db #0x10	; 16
	.db #0x11	; 17
	.db #0x12	; 18
	.db #0x13	; 19
	.db #0x14	; 20
	.db #0x15	; 21
	.db #0x16	; 22
	.db #0x17	; 23
	.db #0x18	; 24
	.db #0x19	; 25
	.db #0x1a	; 26
	.db #0x1b	; 27
	.db #0x1c	; 28
	.db #0x1d	; 29
	.db #0x1e	; 30
	.db #0x20	; 32
	.db #0x22	; 34
	.db #0x24	; 36
	.db #0x26	; 38
	.db #0x28	; 40
	.db #0x2a	; 42
	.db #0x2c	; 44
	.db #0x2e	; 46
	.db #0x30	; 48	'0'
	.db #0x32	; 50	'2'
	.db #0x34	; 52	'4'
	.db #0x36	; 54	'6'
	.db #0x38	; 56	'8'
	.db #0x3a	; 58
	.db #0x3c	; 60
	.db #0x3e	; 62
	.db #0x41	; 65	'A'
	.db #0x45	; 69	'E'
	.db #0x49	; 73	'I'
	.db #0x4d	; 77	'M'
	.db #0x51	; 81	'Q'
	.db #0x55	; 85	'U'
	.db #0x59	; 89	'Y'
	.db #0x5d	; 93
	.db #0x61	; 97	'a'
	.db #0x65	; 101	'e'
	.db #0x69	; 105	'i'
	.db #0x6d	; 109	'm'
	.db #0x71	; 113	'q'
	.db #0x75	; 117	'u'
	.db #0x79	; 121	'y'
	.db #0x7d	; 125
	.db #0x00	; 0
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xff	; 255
	.db #0xfe	; 254
	.db #0xfe	; 254
	.db #0xfe	; 254
	.db #0xfe	; 254
	.db #0xfe	; 254
	.db #0xfe	; 254
	.db #0xfe	; 254
	.db #0xfe	; 254
	.db #0xfe	; 254
	.db #0xfe	; 254
	.db #0xfe	; 254
	.db #0xfe	; 254
	.db #0xfd	; 253
	.db #0xfd	; 253
	.db #0xfd	; 253
	.db #0xfd	; 253
	.db #0xfd	; 253
	.db #0xfd	; 253
	.db #0xfd	; 253
	.db #0xfd	; 253
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfc	; 252
	.db #0xfb	; 251
	.db #0xfb	; 251
	.db #0xfb	; 251
	.db #0xfb	; 251
	.db #0xfa	; 250
	.db #0xfa	; 250
	.db #0xfa	; 250
	.db #0xfa	; 250
	.db #0xf9	; 249
	.db #0xf9	; 249
	.db #0xf9	; 249
	.db #0xf9	; 249
	.db #0xf8	; 248
	.db #0xf8	; 248
	.db #0xf8	; 248
	.db #0xf7	; 247
	.db #0xf7	; 247
	.db #0xf6	; 246
	.db #0xf6	; 246
	.db #0xf5	; 245
	.db #0xf5	; 245
	.db #0xf4	; 244
	.db #0xf4	; 244
	.db #0xf3	; 243
	.db #0xf3	; 243
	.db #0xf2	; 242
	.db #0xf2	; 242
	.db #0xf1	; 241
	.db #0xf1	; 241
	.db #0xf0	; 240
	.db #0xf0	; 240
	.db #0xef	; 239
	.db #0xee	; 238
	.db #0xed	; 237
	.db #0xec	; 236
	.db #0xeb	; 235
	.db #0xea	; 234
	.db #0xe9	; 233
	.db #0xe8	; 232
	.db #0xe7	; 231
	.db #0xe6	; 230
	.db #0xe5	; 229
	.db #0xe4	; 228
	.db #0xe3	; 227
	.db #0xe2	; 226
	.db #0xe1	; 225
	.db #0xdf	; 223
	.db #0xdd	; 221
	.db #0xdb	; 219
	.db #0xd9	; 217
	.db #0xd7	; 215
	.db #0xd5	; 213
	.db #0xd3	; 211
	.db #0xd1	; 209
	.db #0xcf	; 207
	.db #0xcd	; 205
	.db #0xcb	; 203
	.db #0xc9	; 201
	.db #0xc7	; 199
	.db #0xc5	; 197
	.db #0xc3	; 195
	.db #0xc1	; 193
	.db #0xbe	; 190
	.db #0xba	; 186
	.db #0xb6	; 182
	.db #0xb2	; 178
	.db #0xae	; 174
	.db #0xaa	; 170
	.db #0xa6	; 166
	.db #0xa2	; 162
	.db #0x9e	; 158
	.db #0x9a	; 154
	.db #0x96	; 150
	.db #0x92	; 146
	.db #0x8e	; 142
	.db #0x8a	; 138
	.db #0x86	; 134
	.db #0x82	; 130

;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------


	end
