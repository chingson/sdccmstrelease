;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.3.0 #8604 (Sep 15 2014) (MSVC)
; This file was generated Sat Oct 11 21:41:17 2014
;--------------------------------------------------------
; MST port for the MS322 series simple CPU
;--------------------------------------------------------
	.module _shl_longlong
	;.list	p=MS205
	;.radix dec
	.include "ms326sfr.def"
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
;--------------------------------------------------------
; Code Head 
;--------------------------------------------------------
	.area CSEG	 (CODE) 
.FUNC	__shl_longlong
;--------------------------------------------------------
; code
;--------------------------------------------------------
; sh ulong, assembly is the source!!
; input: _PTRCL is the data byte
; input STK00 is MSB!!
; input: ACC is the shift num
__shl_longlong:	;Function start
; 2 exit points
	sta 	_ROMPL ; temp var
	add	#0xc0
	jc	shift_too_large
	lda	_ROMPL
loop_shl:
	jnz	cont_shift
	ret
cont_shift:
	lda	_PTRCL
	shl
	sta	_PTRCL
	lda	STK06
	rol
	sta	STK06
	lda	STK05
	rol
	sta	STK05
	lda	STK04
	rol
	sta	STK04
	lda	STK03
	rol
	sta	STK03
	lda	STK02
	rol
	sta	STK02
	lda	STK01
	rol
	sta	STK01
	lda	STK00
	rol
	sta	STK00
	lda	_ROMPL
	deca
	sta	_ROMPL
	jmp	loop_shl

	
shift_too_large:
	clra
	sta	_PTRCL
	sta	STK00
	sta	STK01
	sta	STK02
	sta	STK03
	sta	STK04
	sta	STK05
	sta	STK06
	ret

; exit point of _shl_longlong


;	code size estimation:
;	   21+   10 =    31 instructions (   31 byte)

;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__shl_longlong
	.globl  STK00
	.globl  STK01
	.globl  STK02
	.globl  STK03
	.globl  STK04
	.globl  STK05
	.globl  STK06

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	;end
