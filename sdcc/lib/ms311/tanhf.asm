;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"tanhf.c"
	.module tanhf
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Ftanhf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$tanhf$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; tanhf-code 
.globl _tanhf

;--------------------------------------------------------
	.FUNC _tanhf:$PNUM 4:$C:_fabsf:$C:___fslt:$C:___fsadd:$C:_expf\
:$C:___fsdiv:$C:___fssub:$C:___fsmul\
:$L:r0x1157:$L:_tanhf_STK00:$L:_tanhf_STK01:$L:_tanhf_STK02:$L:r0x115B\
:$L:r0x115A:$L:r0x1159:$L:r0x1158:$L:r0x115C:$L:r0x115D\
:$L:r0x115E:$L:r0x115F:$L:r0x1160:$L:r0x1163:$L:r0x1162\
:$L:r0x1161:$L:r0x1167:$L:r0x1166:$L:r0x1165:$L:r0x1164\

;--------------------------------------------------------
;	.line	50; "tanhf.c"	float tanhf(float x) _FLOAT_FUNC_REENTRANT
_tanhf:	;Function start
	STA	r0x1157
;	;.line	54; "tanhf.c"	f=fabsf(x);
	LDA	_tanhf_STK02
	STA	_fabsf_STK02
	LDA	_tanhf_STK01
	STA	_fabsf_STK01
	LDA	_tanhf_STK00
	STA	_fabsf_STK00
	LDA	r0x1157
	CALL	_fabsf
	STA	r0x115B
	LDA	STK00
	STA	r0x115A
	LDA	STK01
	STA	r0x1159
	LDA	STK02
	STA	r0x1158
;	;.line	55; "tanhf.c"	if(f>SBIG) r=1.0;
	STA	___fslt_STK06
	LDA	r0x1159
	STA	___fslt_STK05
	LDA	r0x115A
	STA	___fslt_STK04
	LDA	r0x115B
	STA	___fslt_STK03
	LDA	#0xb0
	STA	___fslt_STK02
	LDA	#0x2c
	STA	___fslt_STK01
	LDA	#0x10
	STA	___fslt_STK00
	LDA	#0x41
	CALL	___fslt
	JZ	_00112_DS_
	CLRA	
	STA	r0x115C
	STA	r0x115D
	LDA	#0x80
	STA	r0x115E
	LDA	#0x3f
	STA	r0x115F
	JMP	_00113_DS_
_00112_DS_:
;	;.line	56; "tanhf.c"	else if(f>K1)
	LDA	r0x1158
	STA	___fslt_STK06
	LDA	r0x1159
	STA	___fslt_STK05
	LDA	r0x115A
	STA	___fslt_STK04
	LDA	r0x115B
	STA	___fslt_STK03
	LDA	#0x54
	STA	___fslt_STK02
	LDA	#0x9f
	STA	___fslt_STK01
	LDA	#0x0c
	STA	___fslt_STK00
	LDA	#0x3f
	CALL	___fslt
	JZ	_00109_DS_
;	;.line	58; "tanhf.c"	r=0.5-1.0/(expf(f+f)+1.0);
	LDA	r0x1158
	STA	___fsadd_STK06
	LDA	r0x1159
	STA	___fsadd_STK05
	LDA	r0x115A
	STA	___fsadd_STK04
	LDA	r0x115B
	STA	___fsadd_STK03
	LDA	r0x1158
	STA	___fsadd_STK02
	LDA	r0x1159
	STA	___fsadd_STK01
	LDA	r0x115A
	STA	___fsadd_STK00
	LDA	r0x115B
	CALL	___fsadd
	STA	r0x1163
	LDA	STK02
	STA	_expf_STK02
	LDA	STK01
	STA	_expf_STK01
	LDA	STK00
	STA	_expf_STK00
	LDA	r0x1163
	CALL	_expf
	STA	r0x1163
	CLRA	
	STA	___fsadd_STK06
	STA	___fsadd_STK05
	LDA	#0x80
	STA	___fsadd_STK04
	LDA	#0x3f
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1163
	CALL	___fsadd
	STA	r0x1163
	LDA	STK02
	STA	___fsdiv_STK06
	LDA	STK01
	STA	___fsdiv_STK05
	LDA	STK00
	STA	___fsdiv_STK04
	LDA	r0x1163
	STA	___fsdiv_STK03
	CLRA	
	STA	___fsdiv_STK02
	STA	___fsdiv_STK01
	LDA	#0x80
	STA	___fsdiv_STK00
	LDA	#0x3f
	CALL	___fsdiv
	STA	r0x1163
	LDA	STK02
	STA	___fssub_STK06
	LDA	STK01
	STA	___fssub_STK05
	LDA	STK00
	STA	___fssub_STK04
	LDA	r0x1163
	STA	___fssub_STK03
	CLRA	
	STA	___fssub_STK02
	STA	___fssub_STK01
	STA	___fssub_STK00
	LDA	#0x3f
	CALL	___fssub
	STA	r0x1163
;	;.line	59; "tanhf.c"	r+=r;
	LDA	STK02
	STA	___fsadd_STK06
	LDA	STK01
	STA	___fsadd_STK05
	LDA	STK00
	STA	___fsadd_STK04
	LDA	r0x1163
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1163
	CALL	___fsadd
	STA	r0x115F
	LDA	STK00
	STA	r0x115E
	LDA	STK01
	STA	r0x115D
	LDA	STK02
	STA	r0x115C
	JMP	_00113_DS_
_00109_DS_:
;	;.line	61; "tanhf.c"	else if(f<EPS) r=f;
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	LDA	#0x80
	STA	___fslt_STK04
	LDA	#0x39
	STA	___fslt_STK03
	LDA	r0x1158
	STA	___fslt_STK02
	LDA	r0x1159
	STA	___fslt_STK01
	LDA	r0x115A
	STA	___fslt_STK00
	LDA	r0x115B
	CALL	___fslt
	JZ	_00106_DS_
	LDA	r0x1158
	STA	r0x115C
	LDA	r0x1159
	STA	r0x115D
	LDA	r0x115A
	STA	r0x115E
	LDA	r0x115B
	STA	r0x115F
	JMP	_00113_DS_
_00106_DS_:
;	;.line	64; "tanhf.c"	g=f*f;
	LDA	r0x1158
	STA	___fsmul_STK06
	LDA	r0x1159
	STA	___fsmul_STK05
	LDA	r0x115A
	STA	___fsmul_STK04
	LDA	r0x115B
	STA	___fsmul_STK03
	LDA	r0x1158
	STA	___fsmul_STK02
	LDA	r0x1159
	STA	___fsmul_STK01
	LDA	r0x115A
	STA	___fsmul_STK00
	LDA	r0x115B
	CALL	___fsmul
	STA	r0x1163
	LDA	STK00
	STA	r0x1162
	LDA	STK01
	STA	r0x1161
	LDA	STK02
	STA	r0x1160
;	;.line	65; "tanhf.c"	r=f+f*(P(g)/Q(g));
	STA	___fsmul_STK06
	LDA	r0x1161
	STA	___fsmul_STK05
	LDA	r0x1162
	STA	___fsmul_STK04
	LDA	r0x1163
	STA	___fsmul_STK03
	LDA	#0xb2
	STA	___fsmul_STK02
	LDA	#0x11
	STA	___fsmul_STK01
	LDA	#0x7b
	STA	___fsmul_STK00
	LDA	#0xbb
	CALL	___fsmul
	STA	r0x1167
	LDA	#0xc6
	STA	___fsadd_STK06
	LDA	#0xe2
	STA	___fsadd_STK05
	LDA	#0x52
	STA	___fsadd_STK04
	LDA	#0xbf
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1167
	CALL	___fsadd
	STA	r0x1167
	LDA	r0x1160
	STA	___fsmul_STK06
	LDA	r0x1161
	STA	___fsmul_STK05
	LDA	r0x1162
	STA	___fsmul_STK04
	LDA	r0x1163
	STA	___fsmul_STK03
	LDA	STK02
	STA	___fsmul_STK02
	LDA	STK01
	STA	___fsmul_STK01
	LDA	STK00
	STA	___fsmul_STK00
	LDA	r0x1167
	CALL	___fsmul
	STA	r0x1167
	LDA	STK00
	STA	r0x1166
	LDA	STK01
	STA	r0x1165
	LDA	STK02
	STA	r0x1164
	LDA	#0x1a
	STA	___fsadd_STK06
	LDA	#0x2a
	STA	___fsadd_STK05
	LDA	#0x1e
	STA	___fsadd_STK04
	LDA	#0x40
	STA	___fsadd_STK03
	LDA	r0x1160
	STA	___fsadd_STK02
	LDA	r0x1161
	STA	___fsadd_STK01
	LDA	r0x1162
	STA	___fsadd_STK00
	LDA	r0x1163
	CALL	___fsadd
	STA	r0x1163
	LDA	STK02
	STA	___fsdiv_STK06
	LDA	STK01
	STA	___fsdiv_STK05
	LDA	STK00
	STA	___fsdiv_STK04
	LDA	r0x1163
	STA	___fsdiv_STK03
	LDA	r0x1164
	STA	___fsdiv_STK02
	LDA	r0x1165
	STA	___fsdiv_STK01
	LDA	r0x1166
	STA	___fsdiv_STK00
	LDA	r0x1167
	CALL	___fsdiv
	STA	r0x1163
	LDA	STK02
	STA	___fsmul_STK06
	LDA	STK01
	STA	___fsmul_STK05
	LDA	STK00
	STA	___fsmul_STK04
	LDA	r0x1163
	STA	___fsmul_STK03
	LDA	r0x1158
	STA	___fsmul_STK02
	LDA	r0x1159
	STA	___fsmul_STK01
	LDA	r0x115A
	STA	___fsmul_STK00
	LDA	r0x115B
	CALL	___fsmul
	STA	r0x1163
	LDA	STK02
	STA	___fsadd_STK06
	LDA	STK01
	STA	___fsadd_STK05
	LDA	STK00
	STA	___fsadd_STK04
	LDA	r0x1163
	STA	___fsadd_STK03
	LDA	r0x1158
	STA	___fsadd_STK02
	LDA	r0x1159
	STA	___fsadd_STK01
	LDA	r0x115A
	STA	___fsadd_STK00
	LDA	r0x115B
	CALL	___fsadd
	STA	r0x115F
	LDA	STK00
	STA	r0x115E
	LDA	STK01
	STA	r0x115D
	LDA	STK02
	STA	r0x115C
_00113_DS_:
;	;.line	67; "tanhf.c"	if(x<0.0) r=-r;
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	STA	___fslt_STK04
	STA	___fslt_STK03
	LDA	_tanhf_STK02
	STA	___fslt_STK02
	LDA	_tanhf_STK01
	STA	___fslt_STK01
	LDA	_tanhf_STK00
	STA	___fslt_STK00
	LDA	r0x1157
	CALL	___fslt
	JZ	_00115_DS_
	LDA	r0x115F
	XOR	#0x80
	STA	r0x115F
_00115_DS_:
;	;.line	68; "tanhf.c"	return r;
	LDA	r0x115C
	STA	STK02
	LDA	r0x115D
	STA	STK01
	LDA	r0x115E
	STA	STK00
	LDA	r0x115F
;	;.line	69; "tanhf.c"	}
	RET	
; exit point of _tanhf
	.ENDFUNC _tanhf
	;--cdb--S:Ltanhf.tanhf$x$65536$25({4}SF:S),R,0,0,[_tanhf_STK02,_tanhf_STK01,_tanhf_STK00,r0x1157]
	;--cdb--S:Ltanhf.tanhf$f$65536$26({4}SF:S),R,0,0,[r0x1158,r0x1159,r0x115A,r0x115B]
	;--cdb--S:Ltanhf.tanhf$g$65536$26({4}SF:S),R,0,0,[r0x1160,r0x1161,r0x1162,r0x1163]
	;--cdb--S:Ltanhf.tanhf$r$65536$26({4}SF:S),R,0,0,[r0x1160,r0x1161,r0x1162,r0x1163]
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$errno$0$0({2}SI:S),E,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_expf
	.globl	_fabsf
	.globl	___fslt
	.globl	___fsadd
	.globl	___fsdiv
	.globl	___fssub
	.globl	___fsmul
	.globl	_errno

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_tanhf
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_tanhf_0	udata
r0x1157:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_tanhf_STK00:	.ds	1
	.globl _tanhf_STK00
_tanhf_STK01:	.ds	1
	.globl _tanhf_STK01
_tanhf_STK02:	.ds	1
	.globl _tanhf_STK02
	.globl _fabsf_STK02
	.globl _fabsf_STK01
	.globl _fabsf_STK00
	.globl ___fslt_STK06
	.globl ___fslt_STK05
	.globl ___fslt_STK04
	.globl ___fslt_STK03
	.globl ___fslt_STK02
	.globl ___fslt_STK01
	.globl ___fslt_STK00
	.globl ___fsadd_STK06
	.globl ___fsadd_STK05
	.globl ___fsadd_STK04
	.globl ___fsadd_STK03
	.globl ___fsadd_STK02
	.globl ___fsadd_STK01
	.globl ___fsadd_STK00
	.globl _expf_STK02
	.globl _expf_STK01
	.globl _expf_STK00
	.globl ___fsdiv_STK06
	.globl ___fsdiv_STK05
	.globl ___fsdiv_STK04
	.globl ___fsdiv_STK03
	.globl ___fsdiv_STK02
	.globl ___fsdiv_STK01
	.globl ___fsdiv_STK00
	.globl ___fssub_STK06
	.globl ___fssub_STK05
	.globl ___fssub_STK04
	.globl ___fssub_STK03
	.globl ___fssub_STK02
	.globl ___fssub_STK01
	.globl ___fssub_STK00
	.globl ___fsmul_STK06
	.globl ___fsmul_STK05
	.globl ___fsmul_STK04
	.globl ___fsmul_STK03
	.globl ___fsmul_STK02
	.globl ___fsmul_STK01
	.globl ___fsmul_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115C:NULL+0:-1:1
	;--cdb--W:r0x1160:NULL+0:12:0
	;--cdb--W:r0x1162:NULL+0:14:0
	;--cdb--W:r0x1161:NULL+0:13:0
	;--cdb--W:r0x1166:NULL+0:14:0
	;--cdb--W:r0x1165:NULL+0:13:0
	;--cdb--W:r0x1164:NULL+0:12:0
	;--cdb--W:r0x1160:NULL+0:-1:1
	;--cdb--W:_tanhf_STK02:NULL+0:-1:1
	end
