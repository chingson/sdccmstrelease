;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_rlslonglong.c"
	.module _rlslonglong
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_rlslonglong$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$_rlslonglong$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _rlslonglong-code 
.globl __rlslonglong

;--------------------------------------------------------
	.FUNC __rlslonglong:$PNUM 9:$C:__shl_longlong\
:$L:r0x115B:$L:__rlslonglong_STK00:$L:__rlslonglong_STK01:$L:__rlslonglong_STK02:$L:__rlslonglong_STK03\
:$L:__rlslonglong_STK04:$L:__rlslonglong_STK05:$L:__rlslonglong_STK06:$L:__rlslonglong_STK07
;--------------------------------------------------------
;	.line	37; "_rlslonglong.c"	return((unsigned long long)(l) << s);
__rlslonglong:	;Function start
	STA	STK00
	LDA	__rlslonglong_STK00
	STA	STK01
	LDA	__rlslonglong_STK01
	STA	STK02
	LDA	__rlslonglong_STK02
	STA	STK03
	LDA	__rlslonglong_STK03
	STA	STK04
	LDA	__rlslonglong_STK04
	STA	STK05
	LDA	__rlslonglong_STK05
	STA	STK06
	LDA	__rlslonglong_STK06
	STA	_PTRCL
	LDA	__rlslonglong_STK07
	CALL	__shl_longlong
	LDA	STK00
	STA	r0x115B
	LDA	STK01
	STA	__rlslonglong_STK00
	LDA	STK02
	STA	__rlslonglong_STK01
	LDA	STK03
	STA	__rlslonglong_STK02
	LDA	STK04
	STA	__rlslonglong_STK03
	LDA	STK05
	STA	__rlslonglong_STK04
	LDA	STK06
	STA	__rlslonglong_STK05
	LDA	_PTRCL
	STA	STK06
	LDA	__rlslonglong_STK05
	STA	STK05
	LDA	__rlslonglong_STK04
	STA	STK04
	LDA	__rlslonglong_STK03
	STA	STK03
	LDA	__rlslonglong_STK02
	STA	STK02
	LDA	__rlslonglong_STK01
	STA	STK01
	LDA	__rlslonglong_STK00
	STA	STK00
	LDA	r0x115B
;	;.line	38; "_rlslonglong.c"	}
	RET	
; exit point of __rlslonglong
	.ENDFUNC __rlslonglong
	;--cdb--S:G$_rlslonglong$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:L_rlslonglong._rlslonglong$s$65536$1({1}SC:U),R,0,0,[__rlslonglong_STK07]
	;--cdb--S:L_rlslonglong._rlslonglong$l$65536$1({8}SI:S),R,0,0,[__rlslonglong_STK06,__rlslonglong_STK05,__rlslonglong_STK04,__rlslonglong_STK03__rlslonglong_STK02__rlslonglong_STK01__rlslonglong_STK00r0x115B]
	;--cdb--S:G$_rlslonglong$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__shl_longlong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__rlslonglong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__rlslonglong_0	udata
r0x115B:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__rlslonglong_STK00:	.ds	1
	.globl __rlslonglong_STK00
__rlslonglong_STK01:	.ds	1
	.globl __rlslonglong_STK01
__rlslonglong_STK02:	.ds	1
	.globl __rlslonglong_STK02
__rlslonglong_STK03:	.ds	1
	.globl __rlslonglong_STK03
__rlslonglong_STK04:	.ds	1
	.globl __rlslonglong_STK04
__rlslonglong_STK05:	.ds	1
	.globl __rlslonglong_STK05
__rlslonglong_STK06:	.ds	1
	.globl __rlslonglong_STK06
__rlslonglong_STK07:	.ds	1
	.globl __rlslonglong_STK07
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1164:NULL+0:-1:1
	;--cdb--W:__rlslonglong_STK06:NULL+0:-1:1
	;--cdb--W:__rlslonglong_STK00:NULL+0:4451:0
	;--cdb--W:__rlslonglong_STK01:NULL+0:4450:0
	;--cdb--W:__rlslonglong_STK02:NULL+0:4449:0
	;--cdb--W:__rlslonglong_STK03:NULL+0:4448:0
	;--cdb--W:__rlslonglong_STK04:NULL+0:4447:0
	;--cdb--W:__rlslonglong_STK05:NULL+0:4446:0
	;--cdb--W:__rlslonglong_STK06:NULL+0:4445:0
	;--cdb--W:r0x115D:NULL+0:4459:0
	;--cdb--W:r0x115E:NULL+0:4458:0
	;--cdb--W:r0x115F:NULL+0:4457:0
	;--cdb--W:r0x1160:NULL+0:4456:0
	;--cdb--W:r0x1161:NULL+0:4455:0
	;--cdb--W:r0x1162:NULL+0:4454:0
	;--cdb--W:r0x1163:NULL+0:4453:0
	;--cdb--W:r0x115B:NULL+0:-1:1
	end
