;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"atoll.c"
	.module atoll
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--F:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0,0,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isblank$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$islower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isupper$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$atoll$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$atoll$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$_mullonglong$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; atoll-code 
.globl _atoll

;--------------------------------------------------------
	.FUNC _atoll:$PNUM 2:$C:__mullonglong\
:$L:r0x1155:$L:_atoll_STK00:$L:r0x1156:$L:r0x1157:$L:r0x1158\
:$L:r0x1159:$L:r0x115A:$L:r0x115B:$L:r0x115C:$L:r0x115D\
:$L:r0x115E:$L:r0x115F:$L:r0x1160:$L:r0x1161:$L:r0x1166\
:$L:r0x1163:$L:r0x1162:$L:r0x1167:$L:r0x116A:$L:r0x116B\
:$L:r0x116C:$L:r0x116D:$L:r0x116E:$L:r0x116F:$L:r0x1170\

;--------------------------------------------------------
;	.line	34; "atoll.c"	long long int atoll(const char *nptr)
_atoll:	;Function start
	STA	r0x1155
;	;.line	36; "atoll.c"	long long int ret = 0;
	CLRA	
	STA	r0x1156
	STA	r0x1157
	STA	r0x1158
	STA	r0x1159
	STA	r0x115A
	STA	r0x115B
	STA	r0x115C
	STA	r0x115D
_00105_DS_:
;	;.line	39; "atoll.c"	while (isblank (*nptr))
	LDA	_atoll_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115E
;	;.line	53; "/home/chingson/sdccofficial/sdcc/device/include/ctype.h"	return ((unsigned char)c == ' ' || (unsigned char)c == '\t');
	ADD	#0xe0
	JZ	_00119_DS_
	LDA	r0x115E
	XOR	#0x09
	JNZ	_00135_DS_
_00119_DS_:
;	;.line	40; "atoll.c"	nptr++;
	LDA	_atoll_STK00
	INCA	
	STA	_atoll_STK00
	CLRA	
	ADDC	r0x1155
	STA	r0x1155
	JMP	_00105_DS_
_00135_DS_:
	LDA	_atoll_STK00
	STA	r0x115E
	LDA	r0x1155
	STA	r0x115F
;	;.line	42; "atoll.c"	neg = (*nptr == '-');
	LDA	_atoll_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1160
	XOR	#0x2d
	LDC	_Z
	CLRA	
	ROL	
	STA	_atoll_STK00
;	;.line	44; "atoll.c"	if (*nptr == '-' || *nptr == '+')
	LDA	r0x1160
	ADD	#0xd3
	JZ	_00108_DS_
	LDA	r0x1160
	XOR	#0x2b
	JNZ	_00133_DS_
_00108_DS_:
;	;.line	45; "atoll.c"	nptr++;
	LDA	r0x115E
	INCA	
	STA	r0x115E
	CLRA	
	ADDC	r0x115F
	STA	r0x115F
_00133_DS_:
;	;.line	47; "atoll.c"	while (isdigit (*nptr))
	LDA	r0x115E
	STA	r0x1155
	LDA	r0x115F
	STA	r0x115E
_00111_DS_:
	LDA	r0x1155
	STA	_ROMPL
	LDA	r0x115E
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115F
;	;.line	62; "/home/chingson/sdccofficial/sdcc/device/include/ctype.h"	return ((unsigned char)c >= '0' && (unsigned char)c <= '9');
	ADD	#0xd0
	JNC	_00113_DS_
	SETB	_C
	LDA	#0x39
	SUBB	r0x115F
	JNC	_00113_DS_
;	;.line	48; "atoll.c"	ret = ret * 10 + (*(nptr++) - '0');
	LDA	r0x1156
	STA	__mullonglong_STK14
	LDA	r0x1157
	STA	__mullonglong_STK13
	LDA	r0x1158
	STA	__mullonglong_STK12
	LDA	r0x1159
	STA	__mullonglong_STK11
	LDA	r0x115A
	STA	__mullonglong_STK10
	LDA	r0x115B
	STA	__mullonglong_STK09
	LDA	r0x115C
	STA	__mullonglong_STK08
	LDA	r0x115D
	STA	__mullonglong_STK07
	LDA	#0x0a
	STA	__mullonglong_STK06
	CLRA	
	STA	__mullonglong_STK05
	STA	__mullonglong_STK04
	STA	__mullonglong_STK03
	STA	__mullonglong_STK02
	CLRA	
	STA	__mullonglong_STK01
	STA	__mullonglong_STK00
	CALL	__mullonglong
	STA	r0x1166
	LDA	r0x1155
	STA	_ROMPL
	LDA	r0x115E
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1167
	LDA	r0x1155
	INCA	
	STA	r0x1155
	CLRA	
	ADDC	r0x115E
	STA	r0x115E
	LDA	#0xd0
	ADD	r0x1167
	STA	r0x1167
	CLRA	
	ADDC	#0xff
	STA	r0x116A
	JPL	_00177_DS_
	LDA	#0xff
	JMP	_00178_DS_
_00177_DS_:
	CLRA	
_00178_DS_:
	STA	r0x116B
	STA	r0x116C
	STA	r0x116D
	STA	r0x116E
	STA	r0x116F
	STA	r0x1170
	LDA	STK06
	ADD	r0x1167
	STA	r0x1156
	LDA	STK05
	ADDC	r0x116A
	STA	r0x1157
	LDA	STK04
	ADDC	r0x116B
	STA	r0x1158
	LDA	STK03
	ADDC	r0x116C
	STA	r0x1159
	LDA	STK02
	ADDC	r0x116D
	STA	r0x115A
	LDA	STK01
	ADDC	r0x116E
	STA	r0x115B
	LDA	STK00
	ADDC	r0x116F
	STA	r0x115C
	LDA	r0x1166
	ADDC	r0x1170
	STA	r0x115D
	JMP	_00111_DS_
_00113_DS_:
;	;.line	50; "atoll.c"	return (neg ? -ret : ret); // Since -LLONG_MIN is LLONG_MIN in sdcc, the result value always turns out ok.
	LDA	_atoll_STK00
	JZ	_00124_DS_
	SETB	_C
	CLRA	
	SUBB	r0x1156
	STA	_atoll_STK00
	CLRA	
	SUBB	r0x1157
	STA	r0x1155
	CLRA	
	SUBB	r0x1158
	STA	r0x115E
	CLRA	
	SUBB	r0x1159
	STA	r0x115F
	CLRA	
	SUBB	r0x115A
	STA	r0x1160
	CLRA	
	SUBB	r0x115B
	STA	r0x1161
	CLRA	
	SUBB	r0x115C
	STA	r0x1162
	CLRA	
	SUBB	r0x115D
	STA	r0x1163
	JMP	_00125_DS_
_00124_DS_:
	LDA	r0x1156
	STA	_atoll_STK00
	LDA	r0x1157
	STA	r0x1155
	LDA	r0x1158
	STA	r0x115E
	LDA	r0x1159
	STA	r0x115F
	LDA	r0x115A
	STA	r0x1160
	LDA	r0x115B
	STA	r0x1161
	LDA	r0x115C
	STA	r0x1162
	LDA	r0x115D
	STA	r0x1163
_00125_DS_:
	LDA	_atoll_STK00
	STA	STK06
	LDA	r0x1155
	STA	STK05
	LDA	r0x115E
	STA	STK04
	LDA	r0x115F
	STA	STK03
	LDA	r0x1160
	STA	STK02
	LDA	r0x1161
	STA	STK01
	LDA	r0x1162
	STA	STK00
	LDA	r0x1163
;	;.line	51; "atoll.c"	}
	RET	
; exit point of _atoll
	.ENDFUNC _atoll
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atoll$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_mullonglong$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:Latoll.aligned_alloc$size$65536$15({2}SI:U),E,0,0
	;--cdb--S:Latoll.aligned_alloc$alignment$65536$15({2}SI:U),E,0,0
	;--cdb--S:Latoll.isblank$c$65536$41({2}SI:S),E,0,0
	;--cdb--S:Latoll.isdigit$c$65536$43({2}SI:S),E,0,0
	;--cdb--S:Latoll.islower$c$65536$45({2}SI:S),E,0,0
	;--cdb--S:Latoll.isupper$c$65536$47({2}SI:S),E,0,0
	;--cdb--S:Latoll.atoll$nptr$65536$49({2}DG,SC:U),R,0,0,[_atoll_STK00,r0x115F]
	;--cdb--S:Latoll.atoll$__1310720004$131072$50({2}SI:S),R,0,0,[]
	;--cdb--S:Latoll.atoll$__1310720001$131072$50({2}SI:S),R,0,0,[]
	;--cdb--S:Latoll.atoll$ret$65536$50({8}SI:S),R,0,0,[r0x1156,r0x1157,r0x1158,r0x1159r0x115Ar0x115Br0x115Cr0x115D]
	;--cdb--S:Latoll.atoll$neg$65536$50({1}:S),R,0,0,[_atoll_STK00]
	;--cdb--S:Latoll.atoll$__1310720002$131072$51({2}SI:S),R,0,0,[r0x115F,r0x1160]
	;--cdb--S:Latoll.atoll$c$196608$52({2}SI:S),R,0,0,[]
	;--cdb--S:Latoll.atoll$__1310720005$131072$54({2}SI:S),R,0,0,[r0x1160,r0x1161]
	;--cdb--S:Latoll.atoll$c$196608$55({2}SI:S),R,0,0,[]
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__mullonglong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_atoll
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_atoll_0	udata
r0x1155:	.ds	1
r0x1156:	.ds	1
r0x1157:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116C:	.ds	1
r0x116D:	.ds	1
r0x116E:	.ds	1
r0x116F:	.ds	1
r0x1170:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_atoll_STK00:	.ds	1
	.globl _atoll_STK00
	.globl __mullonglong_STK14
	.globl __mullonglong_STK13
	.globl __mullonglong_STK12
	.globl __mullonglong_STK11
	.globl __mullonglong_STK10
	.globl __mullonglong_STK09
	.globl __mullonglong_STK08
	.globl __mullonglong_STK07
	.globl __mullonglong_STK06
	.globl __mullonglong_STK05
	.globl __mullonglong_STK04
	.globl __mullonglong_STK03
	.globl __mullonglong_STK02
	.globl __mullonglong_STK01
	.globl __mullonglong_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1160:NULL+0:-1:1
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x115E:NULL+0:4447:0
	;--cdb--W:r0x115F:NULL+0:4446:0
	;--cdb--W:r0x115F:NULL+0:4448:0
	;--cdb--W:r0x115F:NULL+0:8:0
	;--cdb--W:r0x1160:NULL+0:4447:0
	;--cdb--W:r0x1160:NULL+0:9:0
	;--cdb--W:r0x1161:NULL+0:10:0
	;--cdb--W:r0x1165:NULL+0:14:0
	;--cdb--W:r0x1164:NULL+0:13:0
	;--cdb--W:r0x1163:NULL+0:12:0
	;--cdb--W:r0x1162:NULL+0:11:0
	;--cdb--W:r0x1168:NULL+0:4455:0
	;--cdb--W:r0x1169:NULL+0:0:0
	;--cdb--W:r0x1169:NULL+0:-1:1
	end
