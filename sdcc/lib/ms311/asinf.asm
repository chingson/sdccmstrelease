;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"asinf.c"
	.module asinf
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Fasinf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$asinf$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$asincosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; asinf-code 
.globl _asinf

;--------------------------------------------------------
	.FUNC _asinf:$PNUM 4:$C:___fseq:$C:_asincosf\
:$L:r0x1157:$L:_asinf_STK00:$L:_asinf_STK01:$L:_asinf_STK02
;--------------------------------------------------------
;	.line	36; "asinf.c"	float asinf(float x) _FLOAT_FUNC_REENTRANT
_asinf:	;Function start
	STA	r0x1157
;	;.line	38; "asinf.c"	if (x == 1.0) return  HALF_PI;
	CLRA	
	STA	___fseq_STK06
	STA	___fseq_STK05
	LDA	#0x80
	STA	___fseq_STK04
	LDA	#0x3f
	STA	___fseq_STK03
	LDA	_asinf_STK02
	STA	___fseq_STK02
	LDA	_asinf_STK01
	STA	___fseq_STK01
	LDA	_asinf_STK00
	STA	___fseq_STK00
	LDA	r0x1157
	CALL	___fseq
	JZ	_00112_DS_
	LDA	#0xdb
	STA	STK02
	LDA	#0x0f
	STA	STK01
	LDA	#0xc9
	STA	STK00
	LDA	#0x3f
	JMP	_00114_DS_
_00112_DS_:
;	;.line	39; "asinf.c"	else if (x ==-1.0) return -HALF_PI;
	CLRA	
	STA	___fseq_STK06
	STA	___fseq_STK05
	LDA	#0x80
	STA	___fseq_STK04
	LDA	#0xbf
	STA	___fseq_STK03
	LDA	_asinf_STK02
	STA	___fseq_STK02
	LDA	_asinf_STK01
	STA	___fseq_STK01
	LDA	_asinf_STK00
	STA	___fseq_STK00
	LDA	r0x1157
	CALL	___fseq
	JZ	_00109_DS_
	LDA	#0xdb
	STA	STK02
	LDA	#0x0f
	STA	STK01
	LDA	#0xc9
	STA	STK00
	LDA	#0xbf
	JMP	_00114_DS_
_00109_DS_:
;	;.line	40; "asinf.c"	else if (x == 0.0) return 0.0;
	LDA	_asinf_STK02
	ORA	_asinf_STK01
	ORA	_asinf_STK00
	ORA	r0x1157
	JNZ	_00106_DS_
	CLRA	
	STA	STK02
	STA	STK01
	STA	STK00
	JMP	_00114_DS_
_00106_DS_:
;	;.line	41; "asinf.c"	else               return asincosf(x, false);
	CLRA	
	STA	_asincosf_STK03
	LDA	_asinf_STK02
	STA	_asincosf_STK02
	LDA	_asinf_STK01
	STA	_asincosf_STK01
	LDA	_asinf_STK00
	STA	_asincosf_STK00
	LDA	r0x1157
	CALL	_asincosf
_00114_DS_:
;	;.line	42; "asinf.c"	}
	RET	
; exit point of _asinf
	.ENDFUNC _asinf
	;--cdb--S:Lasinf.asinf$x$65536$26({4}SF:S),R,0,0,[_asinf_STK02,_asinf_STK01,_asinf_STK00,r0x1157]
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$asincosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_asincosf
	.globl	___fseq

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_asinf
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_asinf_0	udata
r0x1157:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_asinf_STK00:	.ds	1
	.globl _asinf_STK00
_asinf_STK01:	.ds	1
	.globl _asinf_STK01
_asinf_STK02:	.ds	1
	.globl _asinf_STK02
	.globl ___fseq_STK06
	.globl ___fseq_STK05
	.globl ___fseq_STK04
	.globl ___fseq_STK03
	.globl ___fseq_STK02
	.globl ___fseq_STK01
	.globl ___fseq_STK00
	.globl _asincosf_STK03
	.globl _asincosf_STK02
	.globl _asincosf_STK01
	.globl _asincosf_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:_asinf_STK00:NULL+0:14:0
	;--cdb--W:_asinf_STK01:NULL+0:13:0
	;--cdb--W:_asinf_STK02:NULL+0:12:0
	;--cdb--W:r0x1158:NULL+0:-1:1
	;--cdb--W:r0x1157:NULL+0:-1:1
	end
