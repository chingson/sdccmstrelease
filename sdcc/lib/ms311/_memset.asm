;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_memset.c"
	.module _memset
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--F:G$memset$0$0({2}DF,DG,SV:S),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _memset-code 
.globl _memset

;--------------------------------------------------------
	.FUNC _memset:$PNUM 5:$L:r0x1155:$L:_memset_STK00:$L:_memset_STK01:$L:_memset_STK02:$L:_memset_STK03\
:$L:r0x115A:$L:r0x1159:$L:r0x115B:$L:r0x115C
;--------------------------------------------------------
;	.line	34; "_memset.c"	void *memset (void *s, unsigned char c, size_t n)
_memset:	;Function start
	STA	r0x1155
;	;.line	36; "_memset.c"	unsigned char *ret = s;
	STA	r0x115A
	LDA	_memset_STK00
	STA	r0x1159
_00105_DS_:
;	;.line	38; "_memset.c"	while (n--)
	LDA	_memset_STK03
	STA	r0x115B
	LDA	_memset_STK02
	STA	r0x115C
	LDA	_memset_STK03
	DECA	
	STA	_memset_STK03
	LDA	#0xff
	ADDC	_memset_STK02
	STA	_memset_STK02
	LDA	r0x115B
	ORA	r0x115C
	JZ	_00107_DS_
;	;.line	40; "_memset.c"	*(unsigned char *) ret = c;
	LDA	r0x115A
	STA	_ROMPH
	LDA	r0x1159
	STA	_ROMPL
	LDA	_memset_STK01
	STA	@_ROMPINC
;	;.line	41; "_memset.c"	ret = ((unsigned char *) ret) + 1;
	LDA	r0x1159
	INCA	
	STA	r0x1159
	CLRA	
	ADDC	r0x115A
	STA	r0x115A
	JMP	_00105_DS_
_00107_DS_:
;	;.line	44; "_memset.c"	return s;
	LDA	_memset_STK00
	STA	STK00
	LDA	r0x1155
;	;.line	45; "_memset.c"	}
	RET	
; exit point of _memset
	.ENDFUNC _memset
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_memset.memset$n$65536$21({2}SI:U),R,0,0,[_memset_STK03,_memset_STK02]
	;--cdb--S:L_memset.memset$c$65536$21({1}SC:U),R,0,0,[_memset_STK01]
	;--cdb--S:L_memset.memset$s$65536$21({2}DG,SV:S),R,0,0,[_memset_STK00,r0x1155]
	;--cdb--S:L_memset.memset$ret$65536$22({2}DG,SC:U),R,0,0,[]
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_memset
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__memset_0	udata
r0x1155:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_memset_STK00:	.ds	1
	.globl _memset_STK00
_memset_STK01:	.ds	1
	.globl _memset_STK01
_memset_STK02:	.ds	1
	.globl _memset_STK02
_memset_STK03:	.ds	1
	.globl _memset_STK03
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	end
