;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #bbc754a74 (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"D:\common\ms311sphlib"
;;	.file	"api_amp.c"
	.module api_amp
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Fapi_amp$adps[({0}S:S$predict$0$0({2}SI:S),Z,0,0)({2}S:S$index$0$0({1}SC:U),Z,0,0)]
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$api_amp_start$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_start_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_stop_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_b12_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
; *** BIT REGION *** (pseudo)
	.area UDATA (DATA,REL,CON)
; *** BIT REGION ***END
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; api_amp-code 
.globl _api_amp_stop

;--------------------------------------------------------
	.FUNC _api_amp_stop:$PNUM 0
;--------------------------------------------------------
;	.line	44; "api_amp.c"	DACON=0;
_api_amp_stop:	;Function start
	CLRA	
	STA	_DACON
;	;.line	45; "api_amp.c"	ADCON=0;
	STA	_ADCON
;	;.line	46; "api_amp.c"	}
	RET	
; exit point of _api_amp_stop
	.ENDFUNC _api_amp_stop
.globl _api_amp_start

;--------------------------------------------------------
	.FUNC _api_amp_start:$PNUM 0
;--------------------------------------------------------
;	.line	39; "api_amp.c"	DACON=0x83; // just enable it
_api_amp_start:	;Function start
	LDA	#0x83
	STA	_DACON
;	;.line	40; "api_amp.c"	}
	RET	
; exit point of _api_amp_start
	.ENDFUNC _api_amp_start
.globl _api_amp_prepare

;--------------------------------------------------------
	.FUNC _api_amp_prepare:$PNUM 8:$C:_api_DC_cancel\
:$L:_api_amp_prepare_STK00:$L:_api_amp_prepare_STK01:$L:_api_amp_prepare_STK02:$L:_api_amp_prepare_STK03:$L:_api_amp_prepare_STK04\
:$L:_api_amp_prepare_STK05:$L:_api_amp_prepare_STK06
;--------------------------------------------------------
;	.line	9; "api_amp.c"	BYTE api_amp_prepare (BYTE pagv, BYTE fgv, BYTE adcgv, BYTE en5k, BYTE mode,BYTE dcdly, BYTE(*callback)(void) )
_api_amp_prepare:	;Function start
	STA	_PAG
	LDA	_api_amp_prepare_STK00
	STA	_FILTERG
	LDA	_api_amp_prepare_STK01
	STA	_ADCG
;	;.line	14; "api_amp.c"	DMAH=0x80; // reset to recording place
	LDA	#0x80
	STA	_DMAH
;	;.line	15; "api_amp.c"	OFFSETLH &=0x000f; // we skip DC offset, because cap outside
	LDA	_OFFSETLH
	AND	#0x0f
	STA	_OFFSETLH
	CLRA	
	STA	(_OFFSETLH + 1)
;	;.line	16; "api_amp.c"	LVDCON=(LVDCON&0xEF)|en5k;
	LDA	_LVDCON
	AND	#0xef
	ORA	_api_amp_prepare_STK02
	STA	_LVDCON
;	;.line	17; "api_amp.c"	if(dcdly!=0xff)
	LDA	_api_amp_prepare_STK04
	INCA	
	JZ	_00108_DS_
;	;.line	19; "api_amp.c"	if(!api_DC_cancel(dcdly,callback))
	LDA	_api_amp_prepare_STK06
	STA	_api_DC_cancel_STK01
	LDA	_api_amp_prepare_STK05
	STA	_api_DC_cancel_STK00
	LDA	_api_amp_prepare_STK04
	CALL	_api_DC_cancel
	JNZ	_00108_DS_
;	;.line	20; "api_amp.c"	return 0;
	CLRA	
	JMP	_00112_DS_
_00108_DS_:
;	;.line	22; "api_amp.c"	if(mode)
	LDA	_api_amp_prepare_STK03
	JZ	_00110_DS_
;	;.line	24; "api_amp.c"	ADCON=0x47; // no osr
	LDA	#0x47
	STA	_ADCON
;	;.line	25; "api_amp.c"	OFFSETL|=1; // special mode
	LDA	_OFFSETL
	ORA	#0x01
	STA	_OFFSETL
	JMP	_00111_DS_
_00110_DS_:
;	;.line	29; "api_amp.c"	ADCON=0xc7; // OSR, dma0 wakeup
	LDA	#0xc7
	STA	_ADCON
;	;.line	30; "api_amp.c"	OFFSETL&=0xFE;
	LDA	#0xfe
	AND	_OFFSETL
	STA	_OFFSETL
_00111_DS_:
;	;.line	32; "api_amp.c"	return 1;
	LDA	#0x01
_00112_DS_:
;	;.line	35; "api_amp.c"	}
	RET	
; exit point of _api_amp_prepare
	.ENDFUNC _api_amp_prepare
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_start_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_stop_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_b12_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$IOR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IODIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IO$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0LH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1LH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAHL$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$init_io_state$0$0({1}SC:U),E,0,0
	;--cdb--S:Lapi_amp.api_amp_prepare$callback$65536$39({2}DC,DF,SC:U),R,0,0,[_api_amp_prepare_STK06,_api_amp_prepare_STK05]
	;--cdb--S:Lapi_amp.api_amp_prepare$dcdly$65536$39({1}SC:U),R,0,0,[_api_amp_prepare_STK04]
	;--cdb--S:Lapi_amp.api_amp_prepare$mode$65536$39({1}SC:U),R,0,0,[_api_amp_prepare_STK03]
	;--cdb--S:Lapi_amp.api_amp_prepare$en5k$65536$39({1}SC:U),R,0,0,[_api_amp_prepare_STK02]
	;--cdb--S:Lapi_amp.api_amp_prepare$adcgv$65536$39({1}SC:U),R,0,0,[]
	;--cdb--S:Lapi_amp.api_amp_prepare$fgv$65536$39({1}SC:U),R,0,0,[]
	;--cdb--S:Lapi_amp.api_amp_prepare$pagv$65536$39({1}SC:U),R,0,0,[]
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_api_DC_cancel
	.globl	_IOR
	.globl	_IODIR
	.globl	_IO
	.globl	_IOWK
	.globl	_IOWKDR
	.globl	_TIMERC
	.globl	_THRLD
	.globl	_RAMP0L
	.globl	_RAMP0H
	.globl	_RAMP1L
	.globl	_RAMP1H
	.globl	_PTRCL
	.globl	_PTRCH
	.globl	_ROMPL
	.globl	_ROMPH
	.globl	_BEEPC
	.globl	_FILTERG
	.globl	_ULAWC
	.globl	_STACKL
	.globl	_STACKH
	.globl	_ADCON
	.globl	_DACON
	.globl	_SYSC
	.globl	_SPIM
	.globl	_SPIH
	.globl	_SPIOP
	.globl	_SPI_BANK
	.globl	_ADP_IND
	.globl	_ADP_VPL
	.globl	_ADP_VPH
	.globl	_ADL
	.globl	_ADH
	.globl	_ZC
	.globl	_ADCG
	.globl	_DAC_PL
	.globl	_DAC_PH
	.globl	_PAG
	.globl	_DMAL
	.globl	_DMAH
	.globl	_SPIL
	.globl	_IOMASK
	.globl	_IOCMP
	.globl	_IOCNT
	.globl	_LVDCON
	.globl	_LVRCON
	.globl	_OFFSETL
	.globl	_OFFSETH
	.globl	_RCCON
	.globl	_BGCON
	.globl	_PWRL
	.globl	_CRYPT
	.globl	_PWRH
	.globl	_PWRHL
	.globl	_IROMDL
	.globl	_IROMDH
	.globl	_IROMDLH
	.globl	_RECMUTE
	.globl	_SPKC
	.globl	_DCLAMP
	.globl	_SSPIC
	.globl	_SSPIL
	.globl	_SSPIM
	.globl	_SSPIH
	.globl	_RAMP0
	.globl	_RAMP0LH
	.globl	_RAMP1LH
	.globl	_RAMP0INC
	.globl	_RAMP1
	.globl	_DMAHL
	.globl	_RAMP1INC
	.globl	_RAMP1INC2
	.globl	_ROMP
	.globl	_ROMPINC
	.globl	_ROMPLH
	.globl	_ROMPINC2
	.globl	_ACC
	.globl	_RAMP0UW
	.globl	_RAMP1UW
	.globl	_ROMPUW
	.globl	_SPIMH
	.globl	_OFFSETLH
	.globl	_ADP_VPLH
	.globl	_ICE0
	.globl	_ICE1
	.globl	_ICE2
	.globl	_ICE3
	.globl	_ICE4
	.globl	_TOV
	.globl	_init_io_state

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_api_amp_prepare
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area DSEG (DATA); (local stack unassigned) 
_api_amp_prepare_STK00:	.ds	1
	.globl _api_amp_prepare_STK00
_api_amp_prepare_STK01:	.ds	1
	.globl _api_amp_prepare_STK01
_api_amp_prepare_STK02:	.ds	1
	.globl _api_amp_prepare_STK02
_api_amp_prepare_STK03:	.ds	1
	.globl _api_amp_prepare_STK03
_api_amp_prepare_STK04:	.ds	1
	.globl _api_amp_prepare_STK04
_api_amp_prepare_STK05:	.ds	1
	.globl _api_amp_prepare_STK05
_api_amp_prepare_STK06:	.ds	1
	.globl _api_amp_prepare_STK06
	.globl _api_DC_cancel_STK01
	.globl _api_DC_cancel_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1162:NULL+0:-1:1
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:_api_amp_prepare_STK02:NULL+0:4457:0
	;--cdb--W:r0x1161:NULL+0:4456:0
	;--cdb--W:r0x1162:NULL+0:0:0
	;--cdb--W:_api_amp_prepare_STK02:NULL+0:-1:1
	end
