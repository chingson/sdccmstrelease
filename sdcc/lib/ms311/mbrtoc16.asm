;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"mbrtoc16.c"
	.module mbrtoc16
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Fmbrtoc16$__00000000[({0}S:S$c$0$0({3}DA3d,SC:U),Z,0,0)]
	;--cdb--T:Fmbrtoc16$tm[]
	;--cdb--S:G$mbrtoc16$0$0({2}DF,SI:U),C,0,0
	;--cdb--F:G$mbrtoc16$0$0({2}DF,SI:U),C,0,0,0,0,0
	;--cdb--S:G$c16rtomb$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$mbrtoc32$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$c32rtomb$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__mbstoc16s$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__c16stombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcscmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wcslen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$btowc$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$wctob$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbsinit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbrlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$mbrtowc$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcrtomb$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; mbrtoc16-code 
.globl _mbrtoc16

;--------------------------------------------------------
	.FUNC _mbrtoc16:$PNUM 8:$C:_mbrtoc16:$C:_mbrtowc\
:$L:r0x1156:$L:_mbrtoc16_STK00:$L:_mbrtoc16_STK01:$L:_mbrtoc16_STK02:$L:_mbrtoc16_STK03\
:$L:_mbrtoc16_STK04:$L:_mbrtoc16_STK05:$L:_mbrtoc16_STK06:$L:r0x115E:$L:r0x115D\
:$L:r0x115F:$L:r0x1160:$L:r0x1161:$L:r0x1162:$L:_mbrtoc16_codepoint_65536_16\

;--------------------------------------------------------
;	.line	34; "mbrtoc16.c"	size_t mbrtoc16(char16_t *restrict pc16, const char *restrict s, size_t n, mbstate_t *restrict ps)
_mbrtoc16:	;Function start
	STA	r0x1156
;	;.line	41; "mbrtoc16.c"	if(!s)
	LDA	_mbrtoc16_STK02
	ORA	_mbrtoc16_STK01
	JNZ	_00106_DS_
;	;.line	42; "mbrtoc16.c"	return(mbrtoc16(0, "", 1, ps));
	LDA	_mbrtoc16_STK06
	STA	@_RAMP0INC
	LDA	_mbrtoc16_STK05
	STA	@_RAMP0INC
	LDA	#0x01
	STA	@_RAMP0INC
	CLRA	
	STA	@_RAMP0INC
	LDA	#(___str_0 + 0)
	STA	@_RAMP0INC
	LDA	#high (___str_0 + 0)
	STA	@_RAMP0INC
	CLRA	
	STA	@_RAMP0INC
	CALL	_mbrtoc16
	JMP	_00123_DS_
_00106_DS_:
;	;.line	44; "mbrtoc16.c"	if(!ps)
	LDA	_mbrtoc16_STK06
	ORA	_mbrtoc16_STK05
	JNZ	_00108_DS_
;	;.line	45; "mbrtoc16.c"	ps = &sps;
	LDA	#high (_mbrtoc16_sps_65536_16 + 0)
	STA	_mbrtoc16_STK05
	LDA	#(_mbrtoc16_sps_65536_16 + 0)
	STA	_mbrtoc16_STK06
_00108_DS_:
;	;.line	47; "mbrtoc16.c"	if(!ps->c[0] && (ps->c[1] || ps->c[2]))
	LDA	_mbrtoc16_STK06
	STA	_ROMPL
	LDA	_mbrtoc16_STK05
	STA	_ROMPH
	LDA	@_ROMPINC
	JNZ	_00112_DS_
	LDA	_mbrtoc16_STK06
	INCA	
	STA	r0x115D
	CLRA	
	ADDC	_mbrtoc16_STK05
	STA	r0x115E
	LDA	r0x115D
	STA	_ROMPL
	LDA	r0x115E
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115F
	JNZ	_00111_DS_
	LDA	#0x02
	ADD	_mbrtoc16_STK06
	STA	r0x1160
	CLRA	
	ADDC	_mbrtoc16_STK05
	STA	r0x1161
	LDA	r0x1160
	STA	_ROMPL
	LDA	r0x1161
	STA	_ROMPH
	LDA	@_ROMPINC
	JZ	_00112_DS_
_00111_DS_:
;	;.line	49; "mbrtoc16.c"	if(pc16)
	LDA	_mbrtoc16_STK00
	ORA	r0x1156
	JZ	_00110_DS_
;	;.line	50; "mbrtoc16.c"	*pc16 = ps->c[1] + (ps->c[2] << 8);
	LDA	r0x115F
	STA	r0x1160
	LDA	#0x02
	ADD	_mbrtoc16_STK06
	STA	r0x115F
	CLRA	
	ADDC	_mbrtoc16_STK05
	STA	r0x1162
	LDA	r0x115F
	STA	_ROMPL
	LDA	r0x1162
	STA	_ROMPH
	LDA	@_ROMPINC
	CLRB	_C
	STA	r0x1161
	LDA	r0x1156
	STA	_ROMPH
	LDA	_mbrtoc16_STK00
	STA	_ROMPL
	LDA	r0x1160
	STA	@_ROMPINC
	LDA	r0x1161
	STA	@_ROMPINC
_00110_DS_:
;	;.line	51; "mbrtoc16.c"	ps->c[1] = ps->c[2] = 0;
	LDA	#0x02
	ADD	_mbrtoc16_STK06
	STA	r0x115F
	CLRA	
	ADDC	_mbrtoc16_STK05
	STA	_ROMPH
	LDA	r0x115F
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
	LDA	r0x115E
	STA	_ROMPH
	LDA	r0x115D
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
;	;.line	52; "mbrtoc16.c"	return(-3);
	LDA	#0xfd
	STA	STK00
	LDA	#0xff
	JMP	_00123_DS_
_00112_DS_:
;	;.line	55; "mbrtoc16.c"	ret = mbrtowc(&codepoint, s, n, ps);
	LDA	_mbrtoc16_STK06
	STA	_mbrtowc_STK06
	LDA	_mbrtoc16_STK05
	STA	_mbrtowc_STK05
	LDA	_mbrtoc16_STK04
	STA	_mbrtowc_STK04
	LDA	_mbrtoc16_STK03
	STA	_mbrtowc_STK03
	LDA	_mbrtoc16_STK02
	STA	_mbrtowc_STK02
	LDA	_mbrtoc16_STK01
	STA	_mbrtowc_STK01
	LDA	#(_mbrtoc16_codepoint_65536_16 + 0)
	STA	_mbrtowc_STK00
	LDA	#high (_mbrtoc16_codepoint_65536_16 + 0)
	CALL	_mbrtowc
	STA	_mbrtoc16_STK01
;	;.line	57; "mbrtoc16.c"	if(ret > MB_LEN_MAX)
	SETB	_C
	LDA	#0x04
	SUBB	STK00
	CLRA	
	SUBB	_mbrtoc16_STK01
	JC	_00116_DS_
;	;.line	58; "mbrtoc16.c"	return(ret);
	LDA	_mbrtoc16_STK01
	JMP	_00123_DS_
_00116_DS_:
;	;.line	60; "mbrtoc16.c"	if (codepoint <= 0xffff) // Basic multilingual plane
	SETB	_C
	LDA	#0xff
	SUBB	_mbrtoc16_codepoint_65536_16
	LDA	#0xff
	SUBB	(_mbrtoc16_codepoint_65536_16 + 1)
	CLRA	
	SUBB	(_mbrtoc16_codepoint_65536_16 + 2)
	CLRA	
	SUBB	(_mbrtoc16_codepoint_65536_16 + 3)
	JNC	_00120_DS_
;	;.line	62; "mbrtoc16.c"	if(pc16)
	LDA	_mbrtoc16_STK00
	ORA	r0x1156
	JZ	_00118_DS_
;	;.line	63; "mbrtoc16.c"	*pc16 = codepoint;
	LDA	_mbrtoc16_codepoint_65536_16
	STA	_mbrtoc16_STK04
	LDA	(_mbrtoc16_codepoint_65536_16 + 1)
	STA	_mbrtoc16_STK03
	LDA	r0x1156
	STA	_ROMPH
	LDA	_mbrtoc16_STK00
	STA	_ROMPL
	LDA	_mbrtoc16_STK04
	STA	@_ROMPINC
	LDA	_mbrtoc16_STK03
	STA	@_ROMPINC
_00118_DS_:
;	;.line	64; "mbrtoc16.c"	return(ret);
	LDA	_mbrtoc16_STK01
	JMP	_00123_DS_
_00120_DS_:
;	;.line	67; "mbrtoc16.c"	codepoint -= 0x100000;
	CLRB	_C
	CLRA	
	ADDC	(_mbrtoc16_codepoint_65536_16 + 1)
	STA	(_mbrtoc16_codepoint_65536_16 + 1)
	LDA	#0xf0
	ADDC	(_mbrtoc16_codepoint_65536_16 + 2)
	STA	(_mbrtoc16_codepoint_65536_16 + 2)
	LDA	#0xff
	ADDC	(_mbrtoc16_codepoint_65536_16 + 3)
	STA	(_mbrtoc16_codepoint_65536_16 + 3)
;	;.line	68; "mbrtoc16.c"	if(pc16)
	LDA	_mbrtoc16_STK00
	ORA	r0x1156
	JZ	_00122_DS_
;	;.line	69; "mbrtoc16.c"	*pc16 = ((codepoint >> 10) & 0x3ff) + 0xd800;
	LDA	(_mbrtoc16_codepoint_65536_16 + 3)
	SHR	
	STA	r0x115D
	LDA	(_mbrtoc16_codepoint_65536_16 + 2)
	ROR	
	STA	_mbrtoc16_STK03
	LDA	(_mbrtoc16_codepoint_65536_16 + 1)
	ROR	
	STA	_mbrtoc16_STK04
	CLRA	
	SHR	
	LDA	r0x115D
	ROR	
	LDA	_mbrtoc16_STK03
	ROR	
	STA	_mbrtoc16_STK03
	LDA	_mbrtoc16_STK04
	ROR	
	STA	_mbrtoc16_STK04
	LDA	#0x03
	AND	_mbrtoc16_STK03
	ADD	#0xd8
	STA	_mbrtoc16_STK03
	LDA	r0x1156
	STA	_ROMPH
	LDA	_mbrtoc16_STK00
	STA	_ROMPL
	LDA	_mbrtoc16_STK04
	STA	@_ROMPINC
	LDA	_mbrtoc16_STK03
	STA	@_ROMPINC
_00122_DS_:
;	;.line	70; "mbrtoc16.c"	low_surrogate = (codepoint & 0x3ff) + 0xdc00;
	LDA	_mbrtoc16_codepoint_65536_16
	STA	_mbrtoc16_STK00
	LDA	(_mbrtoc16_codepoint_65536_16 + 1)
	AND	#0x03
	ADD	#0xdc
	STA	r0x1156
;	;.line	71; "mbrtoc16.c"	ps->c[0] = 0;
	LDA	_mbrtoc16_STK05
	STA	_ROMPH
	LDA	_mbrtoc16_STK06
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
;	;.line	72; "mbrtoc16.c"	ps->c[1] = low_surrogate & 0xff;
	LDA	_mbrtoc16_STK06
	INCA	
	STA	_mbrtoc16_STK04
	CLRA	
	ADDC	_mbrtoc16_STK05
	STA	_ROMPH
	LDA	_mbrtoc16_STK04
	STA	_ROMPL
	LDA	_mbrtoc16_STK00
	STA	@_ROMPINC
;	;.line	73; "mbrtoc16.c"	ps->c[2] = low_surrogate >> 8;
	LDA	#0x02
	ADD	_mbrtoc16_STK06
	STA	_mbrtoc16_STK06
	CLRA	
	ADDC	_mbrtoc16_STK05
	STA	_ROMPH
	LDA	_mbrtoc16_STK06
	STA	_ROMPL
	LDA	r0x1156
	STA	@_ROMPINC
;	;.line	75; "mbrtoc16.c"	return(ret);
	LDA	_mbrtoc16_STK01
_00123_DS_:
;	;.line	76; "mbrtoc16.c"	}
	RET	
; exit point of _mbrtoc16
	.ENDFUNC _mbrtoc16
	;--cdb--S:G$mbrtoc16$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$c16rtomb$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$mbrtoc32$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$c32rtomb$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__mbstoc16s$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__c16stombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcscmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wcslen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$btowc$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$wctob$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbsinit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbrlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$mbrtowc$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcrtomb$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:Lmbrtoc16.mbrtoc16$ps$65536$15({2}DG,ST__00000000:S),R,0,0,[_mbrtoc16_STK06,_mbrtoc16_STK05]
	;--cdb--S:Lmbrtoc16.mbrtoc16$n$65536$15({2}SI:U),R,0,0,[_mbrtoc16_STK04,_mbrtoc16_STK03]
	;--cdb--S:Lmbrtoc16.mbrtoc16$s$65536$15({2}DG,SC:U),R,0,0,[_mbrtoc16_STK02,_mbrtoc16_STK01]
	;--cdb--S:Lmbrtoc16.mbrtoc16$pc16$65536$15({2}DG,SI:U),R,0,0,[_mbrtoc16_STK00,r0x1156]
	;--cdb--S:Lmbrtoc16.mbrtoc16$codepoint$65536$16({4}SL:U),E,0,0
	;--cdb--S:Lmbrtoc16.mbrtoc16$ret$65536$16({2}SI:U),R,0,0,[_mbrtoc16_STK02,_mbrtoc16_STK01]
	;--cdb--S:Lmbrtoc16.mbrtoc16$sps$65536$16({3}ST__00000000:S),E,0,0
	;--cdb--S:Lmbrtoc16.mbrtoc16$low_surrogate$65536$16({2}SI:U),R,0,0,[_mbrtoc16_STK00,r0x1156]
	;--cdb--S:Fmbrtoc16$__str_0$0$0({1}DA1d,SC:U),D,0,0
	;--cdb--S:G$mbrtoc16$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_mbrtowc

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_mbrtoc16
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
_mbrtoc16_sps_65536_16:	.ds	3

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_mbrtoc16_0	udata
r0x1156:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_mbrtoc16_codepoint_65536_16:	.ds	4
	.globl _mbrtoc16_codepoint_65536_16
_mbrtoc16_STK00:	.ds	1
	.globl _mbrtoc16_STK00
_mbrtoc16_STK01:	.ds	1
	.globl _mbrtoc16_STK01
_mbrtoc16_STK02:	.ds	1
	.globl _mbrtoc16_STK02
_mbrtoc16_STK03:	.ds	1
	.globl _mbrtoc16_STK03
_mbrtoc16_STK04:	.ds	1
	.globl _mbrtoc16_STK04
_mbrtoc16_STK05:	.ds	1
	.globl _mbrtoc16_STK05
_mbrtoc16_STK06:	.ds	1
	.globl _mbrtoc16_STK06
	.globl _mbrtowc_STK06
	.globl _mbrtowc_STK05
	.globl _mbrtowc_STK04
	.globl _mbrtowc_STK03
	.globl _mbrtowc_STK02
	.globl _mbrtowc_STK01
	.globl _mbrtowc_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------

	.area	SEGC	(CODE) ;mbrtoc16-0-code

___str_0:
	.db 0x00 ; '.'
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1162:NULL+0:-1:1
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x115E:NULL+0:-1:1
	;--cdb--W:r0x115D:NULL+0:-1:1
	;--cdb--W:r0x1156:NULL+0:-1:1
	;--cdb--W:_mbrtoc16_STK03:NULL+0:-1:1
	;--cdb--W:_mbrtoc16_STK00:NULL+0:4457:0
	;--cdb--W:_mbrtoc16_STK02:NULL+0:14:0
	;--cdb--W:_mbrtoc16_STK04:NULL+0:4447:0
	;--cdb--W:_mbrtoc16_STK04:NULL+0:4438:0
	;--cdb--W:r0x115E:NULL+0:0:0
	;--cdb--W:r0x115D:NULL+0:14:0
	;--cdb--W:r0x115F:NULL+0:4451:0
	;--cdb--W:r0x115F:NULL+0:4457:0
	;--cdb--W:r0x115F:NULL+0:4453:0
	;--cdb--W:r0x1160:NULL+0:4456:0
	;--cdb--W:r0x1164:NULL+0:4451:0
	;--cdb--W:r0x1161:NULL+0:0:0
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x1160:NULL+0:-1:1
	;--cdb--W:_mbrtoc16_STK05:NULL+0:-1:1
	end
