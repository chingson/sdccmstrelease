;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"ldexpf.c"
	.module ldexpf
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Fldexpf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$ldexpf$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; ldexpf-code 
.globl _ldexpf

;--------------------------------------------------------
	.FUNC _ldexpf:$PNUM 6:$L:r0x1157:$L:_ldexpf_STK00:$L:_ldexpf_STK01:$L:_ldexpf_STK02:$L:_ldexpf_STK03\
:$L:_ldexpf_STK04:$L:_ldexpf_fl_65536_26:$L:r0x115A:$L:r0x115C:$L:r0x115D\
:$L:r0x115E:$L:r0x115F
;--------------------------------------------------------
;	.line	34; "ldexpf.c"	float ldexpf(float x, int pw2)
_ldexpf:	;Function start
	STA	r0x1157
	LDA	_ldexpf_STK04
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	39; "ldexpf.c"	fl.f = x;
	LDA	_ldexpf_STK02
	STA	_ldexpf_fl_65536_26
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	_ldexpf_STK01
	STA	(_ldexpf_fl_65536_26 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	_ldexpf_STK00
	STA	(_ldexpf_fl_65536_26 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1157
	STA	(_ldexpf_fl_65536_26 + 3)
;	;.line	41; "ldexpf.c"	e=(fl.l >> 23) & 0x000000ff;
	LDA	(_ldexpf_fl_65536_26 + 2)
	ROL	
	LDA	(_ldexpf_fl_65536_26 + 3)
	ROL	
	STA	r0x115A
;	;.line	42; "ldexpf.c"	e+=pw2;
	LDA	_ldexpf_STK03
	JPL	_00110_DS_
	LDA	#0xff
	JMP	_00111_DS_
_00110_DS_:
	CLRA	
_00111_DS_:
	STA	r0x115C
	STA	r0x115D
	LDA	r0x115A
	ADD	_ldexpf_STK04
	STA	_ldexpf_STK04
	CLRA	
	ADDC	_ldexpf_STK03
	CLRA	
	ADDC	r0x115C
	CLRA	
	ADDC	r0x115D
;	;.line	43; "ldexpf.c"	fl.l= ((e & 0xff) << 23) | (fl.l & 0x807fffff);
	CLRA	
	ROR	
	LDA	_ldexpf_STK04
	ROR	
	STA	r0x1157
	CLRA	
	ROR	
	STA	_ldexpf_STK00
	LDA	#0x7f
	AND	(_ldexpf_fl_65536_26 + 2)
	STA	r0x115E
	LDA	(_ldexpf_fl_65536_26 + 3)
	AND	#0x80
	STA	r0x115F
	LDA	r0x115E
	ORA	_ldexpf_STK00
	STA	_ldexpf_STK00
	LDA	r0x115F
	ORA	r0x1157
	STA	r0x1157
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
	LDA	_ldexpf_fl_65536_26
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	(_ldexpf_fl_65536_26 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	_ldexpf_STK00
	STA	(_ldexpf_fl_65536_26 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1157
	STA	(_ldexpf_fl_65536_26 + 3)
;	;.line	45; "ldexpf.c"	return(fl.f);
	LDA	_ldexpf_fl_65536_26
	STA	STK02
	LDA	(_ldexpf_fl_65536_26 + 1)
	STA	STK01
	LDA	(_ldexpf_fl_65536_26 + 2)
	STA	STK00
	LDA	(_ldexpf_fl_65536_26 + 3)
;	;.line	46; "ldexpf.c"	}
	RET	
; exit point of _ldexpf
	.ENDFUNC _ldexpf
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$errno$0$0({2}SI:S),E,0,0
	;--cdb--S:Lldexpf.ldexpf$pw2$65536$25({2}SI:S),R,0,0,[_ldexpf_STK04,_ldexpf_STK03]
	;--cdb--S:Lldexpf.ldexpf$x$65536$25({4}SF:S),R,0,0,[_ldexpf_STK02,_ldexpf_STK01,_ldexpf_STK00,r0x1157]
	;--cdb--S:Lldexpf.ldexpf$fl$65536$26({4}STfloat_long:S),E,0,0
	;--cdb--S:Lldexpf.ldexpf$e$65536$26({4}SL:S),R,0,0,[_ldexpf_STK04,_ldexpf_STK03,r0x115E,r0x115F]
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_errno

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_ldexpf
	.globl	_ldexpf_fl_65536_26
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
_ldexpf_fl_65536_26:	.ds	4

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_ldexpf_0	udata
r0x1157:	.ds	1
r0x115A:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_ldexpf_STK00:	.ds	1
	.globl _ldexpf_STK00
_ldexpf_STK01:	.ds	1
	.globl _ldexpf_STK01
_ldexpf_STK02:	.ds	1
	.globl _ldexpf_STK02
_ldexpf_STK03:	.ds	1
	.globl _ldexpf_STK03
_ldexpf_STK04:	.ds	1
	.globl _ldexpf_STK04
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:_ldexpf_STK02:NULL+0:-1:1
	;--cdb--W:_ldexpf_STK01:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:-1:1
	;--cdb--W:r0x115C:NULL+0:-1:1
	;--cdb--W:r0x115D:NULL+0:-1:1
	;--cdb--W:_ldexpf_STK03:NULL+0:-1:1
	;--cdb--W:r0x115E:NULL+0:-1:1
	;--cdb--W:r0x115F:NULL+0:-1:1
	;--cdb--W:_ldexpf_STK01:NULL+0:0:0
	;--cdb--W:_ldexpf_STK02:NULL+0:0:0
	;--cdb--W:r0x115A:NULL+0:4452:0
	;--cdb--W:r0x115B:NULL+0:4451:0
	;--cdb--W:_ldexpf_STK02:NULL+0:4442:0
	;--cdb--W:r0x1157:NULL+0:0:0
	;--cdb--W:_ldexpf_STK00:NULL+0:0:0
	;--cdb--W:_ldexpf_STK00:NULL+0:-1:1
	;--cdb--W:r0x1157:NULL+0:-1:1
	;--cdb--W:_ldexpf_STK03:NULL+0:0:0
	end
