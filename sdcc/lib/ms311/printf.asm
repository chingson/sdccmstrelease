;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"printf.c"
	.module printf
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$putcp$0$0({2}DF,SV:S),C,0,-4
	;--cdb--F:G$putcp$0$0({2}DF,SV:S),C,0,-4,0,0,0
	;--cdb--S:G$tfp_format$0$0({2}DF,SV:S),C,0,-2
	;--cdb--F:G$tfp_format$0$0({2}DF,SV:S),C,0,-2,0,0,0
	;--cdb--S:G$putchar$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$putchar$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$sprintf$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$sprintf$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:Fprintf$putch$0$0({2}DF,SV:S),C,0,-2
	;--cdb--F:Fprintf$putch$0$0({2}DF,SV:S),C,0,-2,0,0,0
	;--cdb--S:Fprintf$uli2a$0$0({2}DF,SV:S),C,0,-2
	;--cdb--F:Fprintf$uli2a$0$0({2}DF,SV:S),C,0,-2,0,0,0
	;--cdb--S:G$_divulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$_modulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:Fprintf$li2a$0$0({2}DF,SV:S),C,0,-2
	;--cdb--F:Fprintf$li2a$0$0({2}DF,SV:S),C,0,-2,0,0,0
	;--cdb--S:Fprintf$ui2a$0$0({2}DF,SV:S),C,0,-2
	;--cdb--F:Fprintf$ui2a$0$0({2}DF,SV:S),C,0,-2,0,0,0
	;--cdb--S:G$_divuint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_mulint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_moduint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:Fprintf$i2a$0$0({2}DF,SV:S),C,0,-2
	;--cdb--F:Fprintf$i2a$0$0({2}DF,SV:S),C,0,-2,0,0,0
	;--cdb--S:Fprintf$a2d$0$0({2}DF,SI:S),C,0,-2
	;--cdb--F:Fprintf$a2d$0$0({2}DF,SI:S),C,0,-2,0,0,0
	;--cdb--S:Fprintf$a2i$0$0({2}DF,SC:U),C,0,-2
	;--cdb--F:Fprintf$a2i$0$0({2}DF,SC:U),C,0,-2,0,0,0
	;--cdb--S:Fprintf$putchw$0$0({2}DF,SV:S),C,0,-2
	;--cdb--F:Fprintf$putchw$0$0({2}DF,SV:S),C,0,-2,0,0,0
	;--cdb--S:G$printf$0$0({2}DF,SV:S),C,0,-2
	;--cdb--F:G$printf$0$0({2}DF,SV:S),C,0,-2,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; printf-code 
.globl _putcp

;--------------------------------------------------------
	.FUNC _putcp:$PNUM 3:$L:r0x121F:$L:_putcp_STK00:$L:_putcp_STK01:$L:r0x1222:$L:r0x1221\
:$L:r0x1223:$L:r0x1224
;--------------------------------------------------------
;	.line	227; "printf.c"	*(*((char**)p))++ = c;
_putcp:	;Function start
	STA	r0x1222
	LDA	_putcp_STK00
	STA	r0x1221
	STA	_ROMPL
	LDA	r0x1222
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_putcp_STK00
	LDA	@_ROMPINC
	STA	r0x121F
	LDA	_putcp_STK00
	INCA	
	STA	r0x1223
	CLRA	
	ADDC	r0x121F
	STA	r0x1224
	LDA	r0x1222
	STA	_ROMPH
	LDA	r0x1221
	STA	_ROMPL
	LDA	r0x1223
	STA	@_ROMPINC
	LDA	r0x1224
	STA	@_ROMPINC
	LDA	r0x121F
	STA	_ROMPH
	LDA	_putcp_STK00
	STA	_ROMPL
	LDA	_putcp_STK01
	STA	@_ROMPINC
;	;.line	228; "printf.c"	}
	RET	
; exit point of _putcp
	.ENDFUNC _putcp
.globl _printf

;--------------------------------------------------------
	.FUNC_PSTK _printf:$C:_tfp_format\
:$L:r0x1217:$L:r0x121A
;--------------------------------------------------------
;	.line	218; "printf.c"	if(stdout_putf==NULL)
_printf:	;Function start
	LDA	_stdout_putf
	ORA	(_stdout_putf + 1)
	JNZ	_00541_DS_
;	;.line	219; "printf.c"	stdout_putf=putch;
	LDA	#low (_putch + 0)
	STA	_stdout_putf
	LDA	#high (_putch + 0)
	STA	(_stdout_putf + 1)
_00541_DS_:
;	;.line	220; "printf.c"	va_start(va,fmt);
	LDA	#0xfc
	ADD	_RAMP1L
	STA	r0x1217
	LDA	#0x7f
	ADDC	_RAMP1H
	STA	r0x121A
	LDA	r0x1217
;	;.line	221; "printf.c"	tfp_format(stdout_putp,stdout_putf,fmt,va);
	STA	_tfp_format_STK06
	LDA	r0x121A
	STA	_tfp_format_STK05
	LDA	@P1,-4
	STA	_tfp_format_STK04
	LDA	@P1,-3
	STA	_tfp_format_STK03
	LDA	_stdout_putf
	STA	_tfp_format_STK02
	LDA	(_stdout_putf + 1)
	STA	_tfp_format_STK01
	LDA	_stdout_putp
	STA	_tfp_format_STK00
	LDA	(_stdout_putp + 1)
	CALL	_tfp_format
;	;.line	223; "printf.c"	}
	RET	
; exit point of _printf
	.ENDFUNC _printf
.globl _tfp_format

;--------------------------------------------------------
	.FUNC _tfp_format:$PNUM 8:$C:_a2i:$C:_uli2a:$C:_ui2a:$C:_putchw\
:$C:_li2a:$C:_i2a\
:$L:r0x1205:$L:_tfp_format_STK00:$L:_tfp_format_STK01:$L:_tfp_format_STK02:$L:_tfp_format_STK03\
:$L:_tfp_format_STK04:$L:_tfp_format_STK05:$L:r0x1209:$L:_tfp_format_STK06:$L:r0x1208\
:$L:r0x120A:$L:r0x120B:$L:r0x120C:$L:r0x120D:$L:r0x120E\
:$L:_tfp_format_fmt_65536_33:$L:_tfp_format_w_196608_36:$L:r0x120F:$L:r0x1211:$L:r0x1212\
:$L:r0x1213:$L:_tfp_format_bf_65536_34
;--------------------------------------------------------
;	.line	129; "printf.c"	void tfp_format(void* putp,putcf putf,char *fmt, va_list va)
_tfp_format:	;Function start
	STA	r0x1205
	LDA	_tfp_format_STK03
	STA	(_tfp_format_fmt_65536_33 + 1)
	LDA	_tfp_format_STK04
	STA	_tfp_format_fmt_65536_33
	LDA	_tfp_format_STK05
	STA	r0x1209
	LDA	_tfp_format_STK06
	STA	r0x1208
_00385_DS_:
;	;.line	136; "printf.c"	while ((ch=*(fmt++))) {
	LDA	_tfp_format_fmt_65536_33
	STA	r0x120A
	LDA	(_tfp_format_fmt_65536_33 + 1)
	STA	r0x120B
	LDA	r0x120A
	STA	_ROMPL
	LDA	r0x120B
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x120C
	LDA	r0x120A
	INCA	
	STA	_tfp_format_fmt_65536_33
	CLRA	
	ADDC	r0x120B
	STA	(_tfp_format_fmt_65536_33 + 1)
	LDA	r0x120C
	STA	r0x120A
	LDA	r0x120C
	JZ	_00389_DS_
;	;.line	137; "printf.c"	if (ch!='%') 
	LDA	r0x120A
	ADD	#0xdb
	JZ	_00383_DS_
;	;.line	138; "printf.c"	putf(putp,ch);
	CALL	_00483_DS_
	JMP	_00484_DS_
_00483_DS_:
	CALL	_00485_DS_
_00485_DS_:
	LDA	_tfp_format_STK01
	STA	_STACKH
	LDA	_tfp_format_STK02
	STA	_STACKL
	LDA	r0x120A
	STA	@_RAMP0INC
	LDA	_tfp_format_STK00
	STA	@_RAMP0INC
	LDA	r0x1205
	RET	
_00484_DS_:
	JMP	_00385_DS_
_00383_DS_:
;	;.line	140; "printf.c"	char lz=0;
	CLRA	
;	;.line	142; "printf.c"	char lng=0;
	STA	r0x120A
;	;.line	144; "printf.c"	int w=0;
	STA	r0x120B
	STA	_tfp_format_w_196608_36
	STA	(_tfp_format_w_196608_36 + 1)
;	;.line	145; "printf.c"	ch=*(fmt++);
	LDA	_tfp_format_fmt_65536_33
	STA	r0x120C
	LDA	(_tfp_format_fmt_65536_33 + 1)
	STA	r0x120D
	LDA	r0x120C
	STA	_ROMPL
	LDA	r0x120D
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x120E
	LDA	r0x120C
	INCA	
	STA	_tfp_format_fmt_65536_33
	CLRA	
	ADDC	r0x120D
	STA	(_tfp_format_fmt_65536_33 + 1)
;	;.line	146; "printf.c"	if (ch=='0') {
	LDA	r0x120E
	XOR	#0x30
	JNZ	_00357_DS_
;	;.line	147; "printf.c"	ch=*(fmt++);
	LDA	_tfp_format_fmt_65536_33
	STA	r0x120C
	LDA	(_tfp_format_fmt_65536_33 + 1)
	STA	r0x120D
	LDA	r0x120C
	STA	_ROMPL
	LDA	r0x120D
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x120E
	LDA	r0x120C
	INCA	
	STA	_tfp_format_fmt_65536_33
	CLRA	
	ADDC	r0x120D
	STA	(_tfp_format_fmt_65536_33 + 1)
;	;.line	148; "printf.c"	lz=1;
	LDA	#0x01
	STA	r0x120A
_00357_DS_:
;	;.line	150; "printf.c"	if (ch>='0' && ch<='9') {
	LDA	r0x120E
	ADD	#0xd0
	JNC	_00359_DS_
	SETB	_C
	LDA	#0x39
	SUBB	r0x120E
	JNC	_00359_DS_
;	;.line	151; "printf.c"	ch=a2i(ch,&fmt,10,&w);
	LDA	#(_tfp_format_w_196608_36 + 0)
	STA	_a2i_STK05
	LDA	#high (_tfp_format_w_196608_36 + 0)
	STA	_a2i_STK04
	LDA	#0x0a
	STA	_a2i_STK03
	CLRA	
	STA	_a2i_STK02
	LDA	#(_tfp_format_fmt_65536_33 + 0)
	STA	_a2i_STK01
	LDA	#high (_tfp_format_fmt_65536_33 + 0)
	STA	_a2i_STK00
	LDA	r0x120E
	CALL	_a2i
	STA	r0x120E
_00359_DS_:
;	;.line	154; "printf.c"	if (ch=='l') {
	LDA	r0x120E
	XOR	#0x6c
	JNZ	_00362_DS_
;	;.line	155; "printf.c"	ch=*(fmt++);
	LDA	_tfp_format_fmt_65536_33
	STA	r0x120C
	LDA	(_tfp_format_fmt_65536_33 + 1)
	STA	r0x120D
	LDA	r0x120C
	STA	_ROMPL
	LDA	r0x120D
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x120E
	LDA	r0x120C
	INCA	
	STA	_tfp_format_fmt_65536_33
	CLRA	
	ADDC	r0x120D
	STA	(_tfp_format_fmt_65536_33 + 1)
;	;.line	156; "printf.c"	lng=1;
	LDA	#0x01
	STA	r0x120B
_00362_DS_:
;	;.line	159; "printf.c"	switch (ch) {
	LDA	r0x120E
	JZ	_00389_DS_
	XOR	#0x25
	JZ	_00379_DS_
	LDA	r0x120E
	XOR	#0x58
	LDC	_Z
	CLRA	
	ROL	
	STA	r0x120C
	JNZ	_00373_DS_
	LDA	r0x120E
	ADD	#0x9d
	JZ	_00377_DS_
	DECA	
	JZ	_00368_DS_
	ADD	#0xf1
	JZ	_00378_DS_
	ADD	#0xfe
	JZ	_00364_DS_
	ADD	#0xfd
	JZ	_00373_DS_
	JMP	_00385_DS_
_00364_DS_:
;	;.line	164; "printf.c"	if (lng)
	LDA	r0x120B
	JZ	_00366_DS_
;	;.line	165; "printf.c"	uli2a(va_arg(va, unsigned long int),10,0,bf);
	LDA	#0xfc
	ADD	r0x1208
	STA	r0x120D
	LDA	#0xff
	ADDC	r0x1209
	STA	r0x120F
	LDA	r0x120D
	STA	r0x1208
	LDA	r0x120F
	STA	r0x1209
	LDA	r0x120D
	STA	_ROMPL
	LDA	r0x120F
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x120D
	LDA	@_ROMPINC
	STA	r0x120F
	LDA	@_ROMPINC
	STA	r0x1212
	LDA	@_ROMPINC
	STA	r0x1213
	LDA	#(_tfp_format_bf_65536_34 + 0)
	STA	_uli2a_STK08
	LDA	#high (_tfp_format_bf_65536_34 + 0)
	STA	_uli2a_STK07
	CLRA	
	STA	_uli2a_STK06
	STA	_uli2a_STK05
	LDA	#0x0a
	STA	_uli2a_STK04
	CLRA	
	STA	_uli2a_STK03
	LDA	r0x120D
	STA	_uli2a_STK02
	LDA	r0x120F
	STA	_uli2a_STK01
	LDA	r0x1212
	STA	_uli2a_STK00
	LDA	r0x1213
	CALL	_uli2a
	JMP	_00367_DS_
_00366_DS_:
;	;.line	168; "printf.c"	ui2a(va_arg(va, unsigned int),10,0,bf);
	LDA	#0xfe
	ADD	r0x1208
	STA	r0x120D
	LDA	#0xff
	ADDC	r0x1209
	STA	r0x120F
	LDA	r0x120D
	STA	r0x1208
	LDA	r0x120F
	STA	r0x1209
	LDA	r0x120D
	STA	_ROMPL
	LDA	r0x120F
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x120D
	LDA	@_ROMPINC
	STA	r0x120F
	LDA	#(_tfp_format_bf_65536_34 + 0)
	STA	_ui2a_STK06
	LDA	#high (_tfp_format_bf_65536_34 + 0)
	STA	_ui2a_STK05
	CLRA	
	STA	_ui2a_STK04
	STA	_ui2a_STK03
	LDA	#0x0a
	STA	_ui2a_STK02
	CLRA	
	STA	_ui2a_STK01
	LDA	r0x120D
	STA	_ui2a_STK00
	LDA	r0x120F
	CALL	_ui2a
_00367_DS_:
;	;.line	169; "printf.c"	putchw(putp,putf,w,lz,bf);
	LDA	#(_tfp_format_bf_65536_34 + 0)
	STA	_putchw_STK07
	LDA	#high (_tfp_format_bf_65536_34 + 0)
	STA	_putchw_STK06
	LDA	r0x120A
	STA	_putchw_STK05
	LDA	_tfp_format_w_196608_36
	STA	_putchw_STK04
	LDA	(_tfp_format_w_196608_36 + 1)
	STA	_putchw_STK03
	LDA	_tfp_format_STK02
	STA	_putchw_STK02
	LDA	_tfp_format_STK01
	STA	_putchw_STK01
	LDA	_tfp_format_STK00
	STA	_putchw_STK00
	LDA	r0x1205
	CALL	_putchw
;	;.line	170; "printf.c"	break;
	JMP	_00385_DS_
_00368_DS_:
;	;.line	174; "printf.c"	if (lng)
	LDA	r0x120B
	JZ	_00370_DS_
;	;.line	175; "printf.c"	li2a(va_arg(va, unsigned long int),bf);
	LDA	#0xfc
	ADD	r0x1208
	STA	r0x120D
	LDA	#0xff
	ADDC	r0x1209
	STA	r0x120F
	LDA	r0x120D
	STA	r0x1208
	LDA	r0x120F
	STA	r0x1209
	LDA	r0x120D
	STA	_ROMPL
	LDA	r0x120F
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x120D
	LDA	@_ROMPINC
	STA	r0x120F
	LDA	@_ROMPINC
	STA	r0x1212
	LDA	@_ROMPINC
	STA	r0x1213
	LDA	#(_tfp_format_bf_65536_34 + 0)
	STA	_li2a_STK04
	LDA	#high (_tfp_format_bf_65536_34 + 0)
	STA	_li2a_STK03
	LDA	r0x120D
	STA	_li2a_STK02
	LDA	r0x120F
	STA	_li2a_STK01
	LDA	r0x1212
	STA	_li2a_STK00
	LDA	r0x1213
	CALL	_li2a
	JMP	_00371_DS_
_00370_DS_:
;	;.line	178; "printf.c"	i2a(va_arg(va, int),bf);
	LDA	#0xfe
	ADD	r0x1208
	STA	r0x120D
	LDA	#0xff
	ADDC	r0x1209
	STA	r0x120F
	LDA	r0x120D
	STA	r0x1208
	LDA	r0x120F
	STA	r0x1209
	LDA	r0x120D
	STA	_ROMPL
	LDA	r0x120F
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x120D
	LDA	@_ROMPINC
	STA	r0x120F
	LDA	#(_tfp_format_bf_65536_34 + 0)
	STA	_i2a_STK02
	LDA	#high (_tfp_format_bf_65536_34 + 0)
	STA	_i2a_STK01
	LDA	r0x120D
	STA	_i2a_STK00
	LDA	r0x120F
	CALL	_i2a
_00371_DS_:
;	;.line	179; "printf.c"	putchw(putp,putf,w,lz,bf);
	LDA	#(_tfp_format_bf_65536_34 + 0)
	STA	_putchw_STK07
	LDA	#high (_tfp_format_bf_65536_34 + 0)
	STA	_putchw_STK06
	LDA	r0x120A
	STA	_putchw_STK05
	LDA	_tfp_format_w_196608_36
	STA	_putchw_STK04
	LDA	(_tfp_format_w_196608_36 + 1)
	STA	_putchw_STK03
	LDA	_tfp_format_STK02
	STA	_putchw_STK02
	LDA	_tfp_format_STK01
	STA	_putchw_STK01
	LDA	_tfp_format_STK00
	STA	_putchw_STK00
	LDA	r0x1205
	CALL	_putchw
;	;.line	180; "printf.c"	break;
	JMP	_00385_DS_
_00373_DS_:
;	;.line	184; "printf.c"	if (lng)
	LDA	r0x120B
	JZ	_00375_DS_
;	;.line	185; "printf.c"	uli2a(va_arg(va, unsigned long int),16,(ch=='X'),bf);
	LDA	#0xfc
	ADD	r0x1208
	STA	r0x120B
	LDA	#0xff
	ADDC	r0x1209
	STA	r0x120D
	LDA	r0x120B
	STA	r0x1208
	LDA	r0x120D
	STA	r0x1209
	LDA	r0x120B
	STA	_ROMPL
	LDA	r0x120D
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x120B
	LDA	@_ROMPINC
	STA	r0x120D
	LDA	@_ROMPINC
	STA	r0x1211
	LDA	@_ROMPINC
	STA	r0x1212
	LDA	#(_tfp_format_bf_65536_34 + 0)
	STA	_uli2a_STK08
	LDA	#high (_tfp_format_bf_65536_34 + 0)
	STA	_uli2a_STK07
	LDA	r0x120C
	STA	_uli2a_STK06
	CLRA	
	STA	_uli2a_STK05
	LDA	#0x10
	STA	_uli2a_STK04
	CLRA	
	STA	_uli2a_STK03
	LDA	r0x120B
	STA	_uli2a_STK02
	LDA	r0x120D
	STA	_uli2a_STK01
	LDA	r0x1211
	STA	_uli2a_STK00
	LDA	r0x1212
	CALL	_uli2a
	JMP	_00376_DS_
_00375_DS_:
;	;.line	188; "printf.c"	ui2a(va_arg(va, unsigned int),16,(ch=='X'),bf);
	LDA	#0xfe
	ADD	r0x1208
	STA	r0x120B
	LDA	#0xff
	ADDC	r0x1209
	STA	r0x120D
	LDA	r0x120B
	STA	r0x1208
	LDA	r0x120D
	STA	r0x1209
	LDA	r0x120B
	STA	_ROMPL
	LDA	r0x120D
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x120B
	LDA	@_ROMPINC
	STA	r0x120D
	LDA	#(_tfp_format_bf_65536_34 + 0)
	STA	_ui2a_STK06
	LDA	#high (_tfp_format_bf_65536_34 + 0)
	STA	_ui2a_STK05
	LDA	r0x120C
	STA	_ui2a_STK04
	CLRA	
	STA	_ui2a_STK03
	LDA	#0x10
	STA	_ui2a_STK02
	CLRA	
	STA	_ui2a_STK01
	LDA	r0x120B
	STA	_ui2a_STK00
	LDA	r0x120D
	CALL	_ui2a
_00376_DS_:
;	;.line	189; "printf.c"	putchw(putp,putf,w,lz,bf);
	LDA	#(_tfp_format_bf_65536_34 + 0)
	STA	_putchw_STK07
	LDA	#high (_tfp_format_bf_65536_34 + 0)
	STA	_putchw_STK06
	LDA	r0x120A
	STA	_putchw_STK05
	LDA	_tfp_format_w_196608_36
	STA	_putchw_STK04
	LDA	(_tfp_format_w_196608_36 + 1)
	STA	_putchw_STK03
	LDA	_tfp_format_STK02
	STA	_putchw_STK02
	LDA	_tfp_format_STK01
	STA	_putchw_STK01
	LDA	_tfp_format_STK00
	STA	_putchw_STK00
	LDA	r0x1205
	CALL	_putchw
;	;.line	190; "printf.c"	break;
	JMP	_00385_DS_
_00377_DS_:
;	;.line	192; "printf.c"	putf(putp,(char)(va_arg(va, int)));
	LDA	#0xfe
	ADD	r0x1208
	STA	r0x120A
	LDA	#0xff
	ADDC	r0x1209
	STA	r0x120B
	LDA	r0x120A
	STA	r0x1208
	LDA	r0x120B
	STA	r0x1209
	LDA	r0x120A
	STA	_ROMPL
	LDA	r0x120B
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x120A
	CALL	_00526_DS_
	JMP	_00527_DS_
_00526_DS_:
	CALL	_00528_DS_
_00528_DS_:
	LDA	_tfp_format_STK01
	STA	_STACKH
	LDA	_tfp_format_STK02
	STA	_STACKL
	LDA	r0x120A
	STA	@_RAMP0INC
	LDA	_tfp_format_STK00
	STA	@_RAMP0INC
	LDA	r0x1205
	RET	
_00527_DS_:
;	;.line	193; "printf.c"	break;
	JMP	_00385_DS_
_00378_DS_:
;	;.line	195; "printf.c"	putchw(putp,putf,w,0,va_arg(va, char*));
	LDA	#0xfe
	ADD	r0x1208
	STA	r0x120A
	LDA	#0xff
	ADDC	r0x1209
	STA	r0x120C
	LDA	r0x120A
	STA	r0x1208
	LDA	r0x120C
	STA	r0x1209
	LDA	r0x120A
	STA	_ROMPL
	LDA	r0x120C
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x120A
	LDA	@_ROMPINC
	STA	r0x120C
	LDA	r0x120A
	STA	_putchw_STK07
	LDA	r0x120C
	STA	_putchw_STK06
	CLRA	
	STA	_putchw_STK05
	LDA	_tfp_format_w_196608_36
	STA	_putchw_STK04
	LDA	(_tfp_format_w_196608_36 + 1)
	STA	_putchw_STK03
	LDA	_tfp_format_STK02
	STA	_putchw_STK02
	LDA	_tfp_format_STK01
	STA	_putchw_STK01
	LDA	_tfp_format_STK00
	STA	_putchw_STK00
	LDA	r0x1205
	CALL	_putchw
;	;.line	196; "printf.c"	break;
	JMP	_00385_DS_
_00379_DS_:
;	;.line	198; "printf.c"	putf(putp,ch);
	CALL	_00533_DS_
	JMP	_00534_DS_
_00533_DS_:
	CALL	_00535_DS_
_00535_DS_:
	LDA	_tfp_format_STK01
	STA	_STACKH
	LDA	_tfp_format_STK02
	STA	_STACKL
	LDA	r0x120E
	STA	@_RAMP0INC
	LDA	_tfp_format_STK00
	STA	@_RAMP0INC
	LDA	r0x1205
	RET	
_00534_DS_:
;	;.line	201; "printf.c"	}
	JMP	_00385_DS_
_00389_DS_:
;	;.line	205; "printf.c"	}
	RET	
; exit point of _tfp_format
	.ENDFUNC _tfp_format

;--------------------------------------------------------
	.FUNC _putchw:$PNUM 9:$L:r0x11EF:$L:_putchw_STK00:$L:_putchw_STK01:$L:_putchw_STK02:$L:_putchw_STK03\
:$L:_putchw_STK04:$L:_putchw_STK05:$L:_putchw_STK06:$L:_putchw_STK07:$L:r0x11F7\
:$L:r0x11F8:$L:r0x11F9
;--------------------------------------------------------
;	.line	116; "printf.c"	static void putchw(void* putp,putcf putf,int n, char z, char* bf)
_putchw:	;Function start
	STA	r0x11EF
;	;.line	118; "printf.c"	char fc=z? '0' : ' ';
	LDA	_putchw_STK05
	JZ	_00299_DS_
	LDA	#0x30
	STA	_putchw_STK05
	CLRA	
	JMP	_00300_DS_
_00299_DS_:
	LDA	#0x20
	STA	_putchw_STK05
_00300_DS_:
	LDA	_putchw_STK05
	STA	r0x11F8
;	;.line	121; "printf.c"	while (*p++ && n > 0)
	LDA	_putchw_STK07
	STA	_putchw_STK05
	LDA	_putchw_STK06
	STA	r0x11F7
_00288_DS_:
	LDA	_putchw_STK05
	STA	_ROMPL
	LDA	r0x11F7
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x11F9
	LDA	_putchw_STK05
	INCA	
	STA	_putchw_STK05
	CLRA	
	ADDC	r0x11F7
	STA	r0x11F7
	LDA	r0x11F9
	JZ	_00291_DS_
	SETB	_C
	CLRA	
	SUBB	_putchw_STK04
	CLRA	
	SUBSI	
	SUBB	_putchw_STK03
	JC	_00291_DS_
;	;.line	122; "printf.c"	n--;
	LDA	_putchw_STK04
	DECA	
	STA	_putchw_STK04
	LDA	#0xff
	ADDC	_putchw_STK03
	STA	_putchw_STK03
	JMP	_00288_DS_
_00291_DS_:
;	;.line	123; "printf.c"	while (n-- > 0) 
	SETB	_C
	CLRA	
	SUBB	_putchw_STK04
	CLRA	
	SUBSI	
	SUBB	_putchw_STK03
	JC	_00308_DS_
	LDA	_putchw_STK04
	DECA	
	STA	_putchw_STK04
	LDA	#0xff
	ADDC	_putchw_STK03
	STA	_putchw_STK03
;	;.line	124; "printf.c"	putf(putp,fc);
	CALL	_00344_DS_
	JMP	_00345_DS_
_00344_DS_:
	CALL	_00346_DS_
_00346_DS_:
	LDA	_putchw_STK01
	STA	_STACKH
	LDA	_putchw_STK02
	STA	_STACKL
	LDA	r0x11F8
	STA	@_RAMP0INC
	LDA	_putchw_STK00
	STA	@_RAMP0INC
	LDA	r0x11EF
	RET	
_00345_DS_:
	JMP	_00291_DS_
_00308_DS_:
;	;.line	125; "printf.c"	while ((ch= *bf++))
	LDA	_putchw_STK07
	STA	_putchw_STK04
	LDA	_putchw_STK06
	STA	_putchw_STK03
_00294_DS_:
	LDA	_putchw_STK04
	STA	_ROMPL
	LDA	_putchw_STK03
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_putchw_STK05
	LDA	_putchw_STK04
	INCA	
	STA	_putchw_STK04
	CLRA	
	ADDC	_putchw_STK03
	STA	_putchw_STK03
	LDA	_putchw_STK05
	STA	_putchw_STK07
	LDA	_putchw_STK05
	JZ	_00297_DS_
;	;.line	126; "printf.c"	putf(putp,ch);
	CALL	_00349_DS_
	JMP	_00350_DS_
_00349_DS_:
	CALL	_00351_DS_
_00351_DS_:
	LDA	_putchw_STK01
	STA	_STACKH
	LDA	_putchw_STK02
	STA	_STACKL
	LDA	_putchw_STK07
	STA	@_RAMP0INC
	LDA	_putchw_STK00
	STA	@_RAMP0INC
	LDA	r0x11EF
	RET	
_00350_DS_:
	JMP	_00294_DS_
_00297_DS_:
;	;.line	127; "printf.c"	}
	RET	
; exit point of _putchw
	.ENDFUNC _putchw

;--------------------------------------------------------
	.FUNC _a2i:$PNUM 7:$C:_a2d:$C:__mulint\
:$L:r0x11D9:$L:_a2i_STK00:$L:_a2i_STK01:$L:_a2i_STK02:$L:_a2i_STK03\
:$L:_a2i_STK04:$L:_a2i_STK05:$L:r0x11E0:$L:r0x11E1:$L:r0x11E2\
:$L:r0x11E3:$L:r0x11E5:$L:r0x11E6:$L:r0x11E7
;--------------------------------------------------------
;	.line	101; "printf.c"	static char a2i(char ch, char** src,int base,int* nump)
_a2i:	;Function start
	STA	r0x11D9
;	;.line	103; "printf.c"	char* p= *src;
	LDA	_a2i_STK01
	STA	_ROMPL
	LDA	_a2i_STK00
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x11E0
	LDA	@_ROMPINC
	STA	r0x11E1
;	;.line	104; "printf.c"	int num=0;
	CLRA	
	STA	r0x11E2
	STA	r0x11E3
_00280_DS_:
;	;.line	106; "printf.c"	while ((digit=a2d(ch))>=0) {
	LDA	r0x11D9
	CALL	_a2d
	STA	r0x11E5
	LDA	STK00
	STA	r0x11E6
	LDA	r0x11E5
	STA	r0x11E7
	LDA	r0x11E5
	JMI	_00282_DS_
;	;.line	107; "printf.c"	if (digit>base) break;
	SETB	_C
	LDA	_a2i_STK03
	SUBB	r0x11E6
	LDA	_a2i_STK02
	SUBSI	
	SUBB	r0x11E7
	JNC	_00282_DS_
;	;.line	108; "printf.c"	num=num*base+digit;
	LDA	_a2i_STK03
	STA	__mulint_STK02
	LDA	_a2i_STK02
	STA	__mulint_STK01
	LDA	r0x11E2
	STA	__mulint_STK00
	LDA	r0x11E3
	CALL	__mulint
	STA	r0x11E5
	LDA	STK00
	ADD	r0x11E6
	STA	r0x11E2
	LDA	r0x11E5
	ADDC	r0x11E7
	STA	r0x11E3
;	;.line	109; "printf.c"	ch=*p++;
	LDA	r0x11E0
	STA	_ROMPL
	LDA	r0x11E1
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x11D9
	LDA	r0x11E0
	INCA	
	STA	r0x11E0
	CLRA	
	ADDC	r0x11E1
	STA	r0x11E1
	JMP	_00280_DS_
_00282_DS_:
;	;.line	111; "printf.c"	*src=p;
	LDA	_a2i_STK00
	STA	_ROMPH
	LDA	_a2i_STK01
	STA	_ROMPL
	LDA	r0x11E0
	STA	@_ROMPINC
	LDA	r0x11E1
	STA	@_ROMPINC
;	;.line	112; "printf.c"	*nump=num;
	LDA	_a2i_STK04
	STA	_ROMPH
	LDA	_a2i_STK05
	STA	_ROMPL
	LDA	r0x11E2
	STA	@_ROMPINC
	LDA	r0x11E3
	STA	@_ROMPINC
;	;.line	113; "printf.c"	return ch;
	LDA	r0x11D9
;	;.line	114; "printf.c"	}
	RET	
; exit point of _a2i
	.ENDFUNC _a2i

;--------------------------------------------------------
	.FUNC _a2d:$PNUM 1:$L:r0x11D5:$L:r0x11D6:$L:r0x11D7:$L:r0x11D8
;--------------------------------------------------------
;	.line	90; "printf.c"	static int a2d(char ch)
_a2d:	;Function start
	STA	r0x11D5
;	;.line	92; "printf.c"	if (ch>='0' && ch<='9') 
	ADD	#0xd0
	JNC	_00270_DS_
	SETB	_C
	LDA	#0x39
	SUBB	r0x11D5
	JNC	_00270_DS_
;	;.line	93; "printf.c"	return ch-'0';
	LDA	#0xd0
	ADD	r0x11D5
	STA	r0x11D6
	CLRA	
	ADDC	#0xff
	STA	r0x11D7
	LDA	r0x11D6
	STA	STK00
	LDA	r0x11D7
	JMP	_00273_DS_
_00270_DS_:
;	;.line	94; "printf.c"	else if (ch>='a' && ch<='f')
	LDA	r0x11D5
	ADD	#0x9f
	JNC	_00266_DS_
	SETB	_C
	LDA	#0x66
	SUBB	r0x11D5
	JNC	_00266_DS_
;	;.line	95; "printf.c"	return ch-'a'+10;
	LDA	#0xa9
	ADD	r0x11D5
	STA	r0x11D6
	CLRA	
	ADDC	#0xff
	STA	r0x11D7
	LDA	r0x11D6
	STA	STK00
	LDA	r0x11D7
	JMP	_00273_DS_
_00266_DS_:
;	;.line	96; "printf.c"	else if (ch>='A' && ch<='F')
	LDA	r0x11D5
	ADD	#0xbf
	JNC	_00262_DS_
	SETB	_C
	LDA	#0x46
	SUBB	r0x11D5
	JNC	_00262_DS_
;	;.line	97; "printf.c"	return ch-'A'+10;
	LDA	#0xc9
	ADD	r0x11D5
	STA	r0x11D5
	CLRA	
	ADDC	#0xff
	STA	r0x11D8
	LDA	r0x11D5
	STA	STK00
	LDA	r0x11D8
	JMP	_00273_DS_
_00262_DS_:
;	;.line	98; "printf.c"	else return -1;
	LDA	#0xff
	STA	STK00
_00273_DS_:
;	;.line	99; "printf.c"	}
	RET	
; exit point of _a2d
	.ENDFUNC _a2d

;--------------------------------------------------------
	.FUNC _i2a:$PNUM 4:$C:_ui2a\
:$L:r0x11CF:$L:_i2a_STK00:$L:_i2a_STK01:$L:_i2a_STK02
;--------------------------------------------------------
;	.line	81; "printf.c"	static void i2a (int num, char * bf)
_i2a:	;Function start
	STA	r0x11CF
;	;.line	83; "printf.c"	if (num<0) {
	JPL	_00249_DS_
;	;.line	84; "printf.c"	num=-num;
	SETB	_C
	CLRA	
	SUBB	_i2a_STK00
	STA	_i2a_STK00
	CLRA	
	SUBB	r0x11CF
	STA	r0x11CF
;	;.line	85; "printf.c"	*bf++ = '-';
	LDA	_i2a_STK01
	STA	_ROMPH
	LDA	_i2a_STK02
	STA	_ROMPL
	LDA	#0x2d
	STA	@_ROMPINC
	LDA	_i2a_STK02
	INCA	
	STA	_i2a_STK02
	CLRA	
	ADDC	_i2a_STK01
	STA	_i2a_STK01
_00249_DS_:
;	;.line	87; "printf.c"	ui2a(num,10,0,bf);
	LDA	_i2a_STK02
	STA	_ui2a_STK06
	LDA	_i2a_STK01
	STA	_ui2a_STK05
	CLRA	
	STA	_ui2a_STK04
	STA	_ui2a_STK03
	LDA	#0x0a
	STA	_ui2a_STK02
	CLRA	
	STA	_ui2a_STK01
	LDA	_i2a_STK00
	STA	_ui2a_STK00
	LDA	r0x11CF
	CALL	_ui2a
;	;.line	88; "printf.c"	}
	RET	
; exit point of _i2a
	.ENDFUNC _i2a

;--------------------------------------------------------
	.FUNC _ui2a:$PNUM 8:$C:__divuint:$C:__mulint:$C:__moduint\
:$L:r0x11AB:$L:_ui2a_STK00:$L:_ui2a_STK01:$L:_ui2a_STK02:$L:_ui2a_STK03\
:$L:_ui2a_STK04:$L:_ui2a_STK05:$L:_ui2a_STK06:$L:r0x11B2:$L:r0x11B3\
:$L:r0x11B5:$L:r0x11B4:$L:r0x11B7:$L:r0x11B6:$L:r0x11B8\
:$L:r0x11B9:$L:r0x11BA:$L:r0x11BB
;--------------------------------------------------------
;	.line	63; "printf.c"	static void ui2a(unsigned int num, unsigned int base, int uc,char * bf)
_ui2a:	;Function start
	STA	r0x11AB
;	;.line	66; "printf.c"	unsigned int d=1;
	LDA	#0x01
	STA	r0x11B2
	CLRA	
	STA	r0x11B3
_00190_DS_:
;	;.line	67; "printf.c"	while (num/d >= base)
	LDA	r0x11B2
	STA	__divuint_STK02
	LDA	r0x11B3
	STA	__divuint_STK01
	LDA	_ui2a_STK00
	STA	__divuint_STK00
	LDA	r0x11AB
	CALL	__divuint
	STA	r0x11B5
	LDA	STK00
	SETB	_C
	SUBB	_ui2a_STK02
	LDA	r0x11B5
	SUBB	_ui2a_STK01
	JNC	_00211_DS_
;	;.line	68; "printf.c"	d*=base;        
	LDA	_ui2a_STK02
	STA	__mulint_STK02
	LDA	_ui2a_STK01
	STA	__mulint_STK01
	LDA	r0x11B2
	STA	__mulint_STK00
	LDA	r0x11B3
	CALL	__mulint
	STA	r0x11B3
	LDA	STK00
	STA	r0x11B2
	JMP	_00190_DS_
_00211_DS_:
;	;.line	69; "printf.c"	while (d!=0) {
	CLRA	
	STA	r0x11B4
	STA	r0x11B5
_00197_DS_:
	LDA	r0x11B2
	ORA	r0x11B3
	JZ	_00199_DS_
;	;.line	70; "printf.c"	int dgt = num / d;
	LDA	r0x11B2
	STA	__divuint_STK02
	LDA	r0x11B3
	STA	__divuint_STK01
	LDA	_ui2a_STK00
	STA	__divuint_STK00
	LDA	r0x11AB
	CALL	__divuint
	STA	r0x11B7
	LDA	STK00
	STA	r0x11B6
;	;.line	71; "printf.c"	num%= d;
	LDA	r0x11B2
	STA	__moduint_STK02
	LDA	r0x11B3
	STA	__moduint_STK01
	LDA	_ui2a_STK00
	STA	__moduint_STK00
	LDA	r0x11AB
	CALL	__moduint
	STA	r0x11AB
	LDA	STK00
	STA	_ui2a_STK00
;	;.line	72; "printf.c"	d/=base;
	LDA	_ui2a_STK02
	STA	__divuint_STK02
	LDA	_ui2a_STK01
	STA	__divuint_STK01
	LDA	r0x11B2
	STA	__divuint_STK00
	LDA	r0x11B3
	CALL	__divuint
	STA	r0x11B3
	LDA	STK00
	STA	r0x11B2
;	;.line	73; "printf.c"	if (n || dgt>0 || d==0) {
	LDA	r0x11B4
	ORA	r0x11B5
	JNZ	_00193_DS_
	SETB	_C
	CLRA	
	SUBB	r0x11B6
	CLRA	
	SUBSI	
	SUBB	r0x11B7
	JNC	_00193_DS_
	LDA	r0x11B2
	ORA	r0x11B3
	JNZ	_00197_DS_
_00193_DS_:
;	;.line	74; "printf.c"	*bf++ = dgt+(dgt<10 ? '0' : (uc ? 'A' : 'a')-10);
	LDA	_ui2a_STK06
	STA	r0x11B8
	LDA	_ui2a_STK05
	STA	r0x11B9
	LDA	_ui2a_STK06
	INCA	
	STA	_ui2a_STK06
	CLRA	
	ADDC	_ui2a_STK05
	STA	_ui2a_STK05
	LDA	r0x11B6
	STA	r0x11BA
	LDA	r0x11B6
	ADD	#0xf6
	LDA	r0x11B7
	XOR	#0x80
	ADDC	#0x7f
	JC	_00202_DS_
	LDA	#0x30
	STA	r0x11B6
	CLRA	
	JMP	_00242_DS_
_00202_DS_:
	LDA	_ui2a_STK04
	ORA	_ui2a_STK03
	JZ	_00204_DS_
	LDA	#0x41
	STA	r0x11BB
	CLRA	
	JMP	_00205_DS_
_00204_DS_:
	LDA	#0x61
	STA	r0x11BB
_00205_DS_:
	LDA	r0x11BB
	ADD	#0xf6
	STA	r0x11B6
_00242_DS_:
	LDA	r0x11B6
	ADD	r0x11BA
	STA	r0x11BA
	LDA	r0x11B9
	STA	_ROMPH
	LDA	r0x11B8
	STA	_ROMPL
	LDA	r0x11BA
	STA	@_ROMPINC
;	;.line	75; "printf.c"	++n;
	LDA	r0x11B4
	INCA	
	STA	r0x11B4
	CLRA	
	ADDC	r0x11B5
	STA	r0x11B5
	JMP	_00197_DS_
_00199_DS_:
;	;.line	78; "printf.c"	*bf=0;
	LDA	_ui2a_STK05
	STA	_ROMPH
	LDA	_ui2a_STK06
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
;	;.line	79; "printf.c"	}
	RET	
; exit point of _ui2a
	.ENDFUNC _ui2a

;--------------------------------------------------------
	.FUNC _li2a:$PNUM 6:$C:_uli2a\
:$L:r0x11A2:$L:_li2a_STK00:$L:_li2a_STK01:$L:_li2a_STK02:$L:_li2a_STK03\
:$L:_li2a_STK04
;--------------------------------------------------------
;	.line	52; "printf.c"	static void li2a (long num, char * bf)
_li2a:	;Function start
	STA	r0x11A2
;	;.line	54; "printf.c"	if (num<0) {
	JPL	_00178_DS_
;	;.line	55; "printf.c"	num=-num;
	SETB	_C
	CLRA	
	SUBB	_li2a_STK02
	STA	_li2a_STK02
	CLRA	
	SUBB	_li2a_STK01
	STA	_li2a_STK01
	CLRA	
	SUBB	_li2a_STK00
	STA	_li2a_STK00
	CLRA	
	SUBB	r0x11A2
	STA	r0x11A2
;	;.line	56; "printf.c"	*bf++ = '-';
	LDA	_li2a_STK03
	STA	_ROMPH
	LDA	_li2a_STK04
	STA	_ROMPL
	LDA	#0x2d
	STA	@_ROMPINC
	LDA	_li2a_STK04
	INCA	
	STA	_li2a_STK04
	CLRA	
	ADDC	_li2a_STK03
	STA	_li2a_STK03
_00178_DS_:
;	;.line	58; "printf.c"	uli2a(num,10,0,bf);
	LDA	_li2a_STK04
	STA	_uli2a_STK08
	LDA	_li2a_STK03
	STA	_uli2a_STK07
	CLRA	
	STA	_uli2a_STK06
	STA	_uli2a_STK05
	LDA	#0x0a
	STA	_uli2a_STK04
	CLRA	
	STA	_uli2a_STK03
	LDA	_li2a_STK02
	STA	_uli2a_STK02
	LDA	_li2a_STK01
	STA	_uli2a_STK01
	LDA	_li2a_STK00
	STA	_uli2a_STK00
	LDA	r0x11A2
	CALL	_uli2a
;	;.line	59; "printf.c"	}
	RET	
; exit point of _li2a
	.ENDFUNC _li2a

;--------------------------------------------------------
	.FUNC _uli2a:$PNUM 10:$C:__divulong:$C:__mullong:$C:__modulong\
:$L:r0x116A:$L:_uli2a_STK00:$L:_uli2a_STK01:$L:_uli2a_STK02:$L:_uli2a_STK03\
:$L:_uli2a_STK04:$L:_uli2a_STK05:$L:_uli2a_STK06:$L:_uli2a_STK07:$L:_uli2a_STK08\
:$L:r0x1171:$L:r0x1172:$L:r0x1173:$L:r0x1174:$L:r0x1178\
:$L:r0x1177:$L:r0x1176:$L:r0x1175:$L:r0x1179:$L:r0x117A\
:$L:r0x117C:$L:r0x117D:$L:r0x117E
;--------------------------------------------------------
;	.line	34; "printf.c"	static void uli2a(unsigned long int num, unsigned int base, int uc,char * bf)
_uli2a:	;Function start
	STA	r0x116A
;	;.line	37; "printf.c"	unsigned long d=1;
	LDA	#0x01
	STA	r0x1171
	CLRA	
	STA	r0x1172
	STA	r0x1173
	STA	r0x1174
_00117_DS_:
;	;.line	38; "printf.c"	while (num/d >= base)
	LDA	r0x1171
	STA	__divulong_STK06
	LDA	r0x1172
	STA	__divulong_STK05
	LDA	r0x1173
	STA	__divulong_STK04
	LDA	r0x1174
	STA	__divulong_STK03
	LDA	_uli2a_STK02
	STA	__divulong_STK02
	LDA	_uli2a_STK01
	STA	__divulong_STK01
	LDA	_uli2a_STK00
	STA	__divulong_STK00
	LDA	r0x116A
	CALL	__divulong
	STA	r0x1178
	LDA	_uli2a_STK04
	STA	r0x1179
	LDA	_uli2a_STK03
	STA	r0x117A
	CLRA	
	STA	r0x117C
	SETB	_C
	LDA	STK02
	SUBB	r0x1179
	LDA	STK01
	SUBB	r0x117A
	LDA	STK00
	SUBB	#0x00
	LDA	r0x1178
	SUBB	r0x117C
	JNC	_00138_DS_
;	;.line	39; "printf.c"	d*=base;         
	LDA	r0x1179
	STA	__mullong_STK06
	LDA	r0x117A
	STA	__mullong_STK05
	CLRA	
	STA	__mullong_STK04
	LDA	r0x117C
	STA	__mullong_STK03
	LDA	r0x1171
	STA	__mullong_STK02
	LDA	r0x1172
	STA	__mullong_STK01
	LDA	r0x1173
	STA	__mullong_STK00
	LDA	r0x1174
	CALL	__mullong
	STA	r0x1174
	LDA	STK00
	STA	r0x1173
	LDA	STK01
	STA	r0x1172
	LDA	STK02
	STA	r0x1171
	JMP	_00117_DS_
_00138_DS_:
;	;.line	40; "printf.c"	while (d!=0) {
	LDA	_uli2a_STK08
	STA	_uli2a_STK04
	LDA	_uli2a_STK07
	STA	_uli2a_STK03
	CLRA	
	STA	_uli2a_STK08
	STA	_uli2a_STK07
_00124_DS_:
	LDA	r0x1171
	ORA	r0x1172
	ORA	r0x1173
	ORA	r0x1174
	JZ	_00126_DS_
;	;.line	41; "printf.c"	int dgt = num / d;
	LDA	r0x1171
	STA	__divulong_STK06
	LDA	r0x1172
	STA	__divulong_STK05
	LDA	r0x1173
	STA	__divulong_STK04
	LDA	r0x1174
	STA	__divulong_STK03
	LDA	_uli2a_STK02
	STA	__divulong_STK02
	LDA	_uli2a_STK01
	STA	__divulong_STK01
	LDA	_uli2a_STK00
	STA	__divulong_STK00
	LDA	r0x116A
	CALL	__divulong
	LDA	STK02
	STA	r0x117D
	LDA	STK01
	STA	r0x117E
;	;.line	42; "printf.c"	num%=d;
	LDA	r0x1171
	STA	__modulong_STK06
	LDA	r0x1172
	STA	__modulong_STK05
	LDA	r0x1173
	STA	__modulong_STK04
	LDA	r0x1174
	STA	__modulong_STK03
	LDA	_uli2a_STK02
	STA	__modulong_STK02
	LDA	_uli2a_STK01
	STA	__modulong_STK01
	LDA	_uli2a_STK00
	STA	__modulong_STK00
	LDA	r0x116A
	CALL	__modulong
	STA	r0x116A
	LDA	STK00
	STA	_uli2a_STK00
	LDA	STK01
	STA	_uli2a_STK01
	LDA	STK02
	STA	_uli2a_STK02
;	;.line	43; "printf.c"	d/=base;
	LDA	r0x1179
	STA	__divulong_STK06
	LDA	r0x117A
	STA	__divulong_STK05
	CLRA	
	STA	__divulong_STK04
	LDA	r0x117C
	STA	__divulong_STK03
	LDA	r0x1171
	STA	__divulong_STK02
	LDA	r0x1172
	STA	__divulong_STK01
	LDA	r0x1173
	STA	__divulong_STK00
	LDA	r0x1174
	CALL	__divulong
	STA	r0x1174
	LDA	STK00
	STA	r0x1173
	LDA	STK01
	STA	r0x1172
	LDA	STK02
	STA	r0x1171
;	;.line	44; "printf.c"	if (n || dgt>0|| d==0) {
	LDA	_uli2a_STK08
	ORA	_uli2a_STK07
	JNZ	_00120_DS_
	SETB	_C
	CLRA	
	SUBB	r0x117D
	CLRA	
	SUBSI	
	SUBB	r0x117E
	JNC	_00120_DS_
	LDA	r0x1171
	ORA	r0x1172
	ORA	r0x1173
	ORA	r0x1174
	JNZ	_00124_DS_
_00120_DS_:
;	;.line	45; "printf.c"	*bf++ = dgt+(dgt<10 ? '0' : (uc ? 'A' : 'a')-10);
	LDA	_uli2a_STK04
	STA	r0x1175
	LDA	_uli2a_STK03
	STA	r0x1176
	LDA	_uli2a_STK04
	INCA	
	STA	_uli2a_STK04
	CLRA	
	ADDC	_uli2a_STK03
	STA	_uli2a_STK03
	LDA	r0x117D
	STA	r0x1177
	LDA	r0x117D
	ADD	#0xf6
	LDA	r0x117E
	XOR	#0x80
	ADDC	#0x7f
	JC	_00129_DS_
	LDA	#0x30
	STA	r0x1178
	CLRA	
	JMP	_00171_DS_
_00129_DS_:
	LDA	_uli2a_STK06
	ORA	_uli2a_STK05
	JZ	_00131_DS_
	LDA	#0x41
	STA	r0x117E
	CLRA	
	JMP	_00132_DS_
_00131_DS_:
	LDA	#0x61
	STA	r0x117E
_00132_DS_:
	LDA	r0x117E
	ADD	#0xf6
	STA	r0x1178
_00171_DS_:
	LDA	r0x1178
	ADD	r0x1177
	STA	r0x1177
	LDA	r0x1176
	STA	_ROMPH
	LDA	r0x1175
	STA	_ROMPL
	LDA	r0x1177
	STA	@_ROMPINC
;	;.line	46; "printf.c"	++n;
	LDA	_uli2a_STK08
	INCA	
	STA	_uli2a_STK08
	CLRA	
	ADDC	_uli2a_STK07
	STA	_uli2a_STK07
	JMP	_00124_DS_
_00126_DS_:
;	;.line	49; "printf.c"	*bf=0;
	LDA	_uli2a_STK03
	STA	_ROMPH
	LDA	_uli2a_STK04
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
;	;.line	50; "printf.c"	}
	RET	
; exit point of _uli2a
	.ENDFUNC _uli2a

;--------------------------------------------------------
	.FUNC _putch:$PNUM 3:$C:_putchar\
:$L:_putch_STK01
;--------------------------------------------------------
;	.line	29; "printf.c"	putchar(c);
_putch:	;Function start
	LDA	_putch_STK01
	CALL	_putchar
;	;.line	30; "printf.c"	}
	RET	
; exit point of _putch
	.ENDFUNC _putch
.globl _sprintf

;--------------------------------------------------------
	.FUNC_PSTK _sprintf:$C:_tfp_format:$C:_putcp\
:$L:r0x1154:$L:r0x1155:$L:r0x1157:$L:r0x1156:$L:r0x1159\

;--------------------------------------------------------
;	.line	21; "printf.c"	va_start(va,fmt);
_sprintf:	;Function start
	LDA	#0xfa
	ADD	_RAMP1L
	STA	r0x1154
	LDA	#0x7f
	ADDC	_RAMP1H
	STA	r0x1157
	LDA	r0x1154
	STA	r0x1156
;	;.line	22; "printf.c"	tfp_format(&s,putcp,fmt,va);
	LDA	#0xfc
	ADD	_RAMP1L
	STA	r0x1154
	LDA	#0x7f
	ADDC	_RAMP1H
	STA	r0x1155
	STA	r0x1159
	LDA	r0x1156
	STA	_tfp_format_STK06
	LDA	r0x1157
	STA	_tfp_format_STK05
	LDA	@P1,-6
	STA	_tfp_format_STK04
	LDA	@P1,-5
	STA	_tfp_format_STK03
	LDA	#low (_putcp + 0)
	STA	_tfp_format_STK02
	LDA	#high (_putcp + 0)
	STA	_tfp_format_STK01
	LDA	r0x1154
	STA	_tfp_format_STK00
	LDA	r0x1159
	CALL	_tfp_format
;	;.line	23; "printf.c"	putcp(&s,0);
	CLRA	
	STA	_putcp_STK01
	LDA	r0x1154
	STA	_putcp_STK00
	LDA	r0x1155
	CALL	_putcp
;	;.line	25; "printf.c"	}
	RET	
; exit point of _sprintf
	.ENDFUNC _sprintf
.globl _putchar

;--------------------------------------------------------
	.FUNC _putchar:$PNUM 1
;--------------------------------------------------------
;	.line	16; "printf.c"	}
_putchar:	;Function start
	RET	
; exit point of _putchar
	.ENDFUNC _putchar
	;--cdb--S:Lprintf.sprintf$s$65536$8({2}DD,SC:U),B,1,-4
	;--cdb--S:Lprintf.sprintf$fmt$65536$8({2}DG,SC:U),B,1,-6
	;--cdb--S:Lprintf.sprintf$va$65536$9({2}DD,SC:U),R,0,0,[r0x1156,r0x1157]
	;--cdb--S:Lprintf.printf$fmt$65536$43({2}DG,SC:U),B,1,-4
	;--cdb--S:Lprintf.printf$va$65536$44({2}DD,SC:U),R,0,0,[r0x1219,r0x121A]
	;--cdb--S:G$putcp$0$0({2}DF,SV:S),C,0,-4
	;--cdb--S:G$tfp_format$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:G$putchar$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$sprintf$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:Fprintf$putch$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:Fprintf$uli2a$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:G$_divulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$_modulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:Fprintf$li2a$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:Fprintf$ui2a$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:G$_divuint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_mulint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_moduint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:Fprintf$i2a$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:Fprintf$a2d$0$0({2}DF,SI:S),C,0,-2
	;--cdb--S:Fprintf$a2i$0$0({2}DF,SC:U),C,0,-2
	;--cdb--S:Fprintf$putchw$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:G$printf$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:Fprintf$stdout_putf$0$0({2}DC,DF,SV:S),E,0,0
	;--cdb--S:Fprintf$stdout_putp$0$0({2}DG,SV:S),E,0,0
	;--cdb--S:Lprintf.putchar$c$65536$5({1}SC:U),R,0,0,[]
	;--cdb--S:Lprintf.putch$c$65536$10({1}SC:U),R,0,0,[_putch_STK01]
	;--cdb--S:Lprintf.putch$p$65536$10({2}DG,SV:S),R,0,0,[]
	;--cdb--S:Lprintf.uli2a$bf$65536$12({2}DG,SC:U),R,0,0,[_uli2a_STK08,_uli2a_STK07]
	;--cdb--S:Lprintf.uli2a$uc$65536$12({2}SI:S),R,0,0,[_uli2a_STK06,_uli2a_STK05]
	;--cdb--S:Lprintf.uli2a$base$65536$12({2}SI:U),R,0,0,[_uli2a_STK04,_uli2a_STK03]
	;--cdb--S:Lprintf.uli2a$num$65536$12({4}SL:U),R,0,0,[_uli2a_STK02,_uli2a_STK01,_uli2a_STK00,r0x116A]
	;--cdb--S:Lprintf.uli2a$n$65536$13({2}SI:S),R,0,0,[_uli2a_STK08,_uli2a_STK07]
	;--cdb--S:Lprintf.uli2a$d$65536$13({4}SL:U),R,0,0,[r0x1171,r0x1172,r0x1173,r0x1174]
	;--cdb--S:Lprintf.uli2a$dgt$131072$14({2}SI:S),R,0,0,[r0x117D,r0x117E]
	;--cdb--S:Lprintf.li2a$bf$65536$16({2}DG,SC:U),R,0,0,[_li2a_STK04,_li2a_STK03]
	;--cdb--S:Lprintf.li2a$num$65536$16({4}SL:S),R,0,0,[_li2a_STK02,_li2a_STK01,_li2a_STK00,r0x11A2]
	;--cdb--S:Lprintf.ui2a$bf$65536$19({2}DG,SC:U),R,0,0,[_ui2a_STK06,_ui2a_STK05]
	;--cdb--S:Lprintf.ui2a$uc$65536$19({2}SI:S),R,0,0,[_ui2a_STK04,_ui2a_STK03]
	;--cdb--S:Lprintf.ui2a$base$65536$19({2}SI:U),R,0,0,[_ui2a_STK02,_ui2a_STK01]
	;--cdb--S:Lprintf.ui2a$num$65536$19({2}SI:U),R,0,0,[_ui2a_STK00,r0x11AB]
	;--cdb--S:Lprintf.ui2a$n$65536$20({2}SI:S),R,0,0,[r0x11B4,r0x11B5]
	;--cdb--S:Lprintf.ui2a$d$65536$20({2}SI:U),R,0,0,[r0x11B2,r0x11B3]
	;--cdb--S:Lprintf.ui2a$dgt$131072$21({2}SI:S),R,0,0,[r0x11B6,r0x11B7]
	;--cdb--S:Lprintf.i2a$bf$65536$23({2}DG,SC:U),R,0,0,[_i2a_STK02,_i2a_STK01]
	;--cdb--S:Lprintf.i2a$num$65536$23({2}SI:S),R,0,0,[_i2a_STK00,r0x11CF]
	;--cdb--S:Lprintf.a2d$ch$65536$26({1}SC:U),R,0,0,[r0x11D5]
	;--cdb--S:Lprintf.a2i$nump$65536$28({2}DG,SI:S),R,0,0,[_a2i_STK05,_a2i_STK04]
	;--cdb--S:Lprintf.a2i$base$65536$28({2}SI:S),R,0,0,[_a2i_STK03,_a2i_STK02]
	;--cdb--S:Lprintf.a2i$src$65536$28({2}DG,DG,SC:U),R,0,0,[_a2i_STK01,_a2i_STK00]
	;--cdb--S:Lprintf.a2i$ch$65536$28({1}SC:U),R,0,0,[r0x11D9]
	;--cdb--S:Lprintf.a2i$p$65536$29({2}DG,SC:U),R,0,0,[]
	;--cdb--S:Lprintf.a2i$num$65536$29({2}SI:S),R,0,0,[r0x11E2,r0x11E3]
	;--cdb--S:Lprintf.a2i$digit$65536$29({2}SI:S),R,0,0,[r0x11E6,r0x11E7]
	;--cdb--S:Lprintf.putchw$bf$65536$31({2}DG,SC:U),R,0,0,[_putchw_STK07,_putchw_STK06]
	;--cdb--S:Lprintf.putchw$z$65536$31({1}SC:U),R,0,0,[_putchw_STK05]
	;--cdb--S:Lprintf.putchw$n$65536$31({2}SI:S),R,0,0,[_putchw_STK04,_putchw_STK03]
	;--cdb--S:Lprintf.putchw$putf$65536$31({2}DC,DF,SV:S),R,0,0,[_putchw_STK02,_putchw_STK01]
	;--cdb--S:Lprintf.putchw$putp$65536$31({2}DG,SV:S),R,0,0,[_putchw_STK00,r0x11EF]
	;--cdb--S:Lprintf.putchw$fc$65536$32({1}SC:U),R,0,0,[r0x11F8]
	;--cdb--S:Lprintf.putchw$ch$65536$32({1}SC:U),R,0,0,[_putchw_STK07]
	;--cdb--S:Lprintf.putchw$p$65536$32({2}DG,SC:U),R,0,0,[_putchw_STK05,r0x11F7]
	;--cdb--S:Lprintf.tfp_format$va$65536$33({2}DD,SC:U),R,0,0,[r0x1208,r0x1209]
	;--cdb--S:Lprintf.tfp_format$fmt$65536$33({2}DG,SC:U),E,0,0
	;--cdb--S:Lprintf.tfp_format$putf$65536$33({2}DC,DF,SV:S),R,0,0,[_tfp_format_STK02,_tfp_format_STK01]
	;--cdb--S:Lprintf.tfp_format$putp$65536$33({2}DG,SV:S),R,0,0,[_tfp_format_STK00,r0x1205]
	;--cdb--S:Lprintf.tfp_format$bf$65536$34({12}DA12d,SC:U),E,0,0
	;--cdb--S:Lprintf.tfp_format$ch$65536$34({1}SC:U),R,0,0,[r0x120E]
	;--cdb--S:Lprintf.tfp_format$lz$196608$36({1}SC:U),R,0,0,[r0x120A]
	;--cdb--S:Lprintf.tfp_format$lng$196608$36({1}SC:U),R,0,0,[r0x120B]
	;--cdb--S:Lprintf.tfp_format$w$196608$36({2}SI:S),E,0,0
	;--cdb--S:Lprintf.putcp$c$65536$45({1}SC:U),R,0,0,[_putcp_STK01]
	;--cdb--S:Lprintf.putcp$p$65536$45({2}DG,SV:S),R,0,0,[_putcp_STK00,r0x121F]
	;--cdb--S:G$putchar$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$tfp_format$0$0({2}DF,SV:S),C,0,-2
	;--cdb--S:G$putcp$0$0({2}DF,SV:S),C,0,-4
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__divulong
	.globl	__mullong
	.globl	__modulong
	.globl	__divuint
	.globl	__mulint
	.globl	__moduint

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_putcp
	.globl	_tfp_format
	.globl	_putchar
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_printf_0	udata
r0x1154:	.ds	1
r0x1155:	.ds	1
r0x1156:	.ds	1
r0x1157:	.ds	1
r0x1159:	.ds	1
r0x116A:	.ds	1
r0x1171:	.ds	1
r0x1172:	.ds	1
r0x1173:	.ds	1
r0x1174:	.ds	1
r0x1175:	.ds	1
r0x1176:	.ds	1
r0x1177:	.ds	1
r0x1178:	.ds	1
r0x1179:	.ds	1
r0x117A:	.ds	1
r0x117C:	.ds	1
r0x117D:	.ds	1
r0x117E:	.ds	1
r0x11A2:	.ds	1
r0x11AB:	.ds	1
r0x11B2:	.ds	1
r0x11B3:	.ds	1
r0x11B4:	.ds	1
r0x11B5:	.ds	1
r0x11B6:	.ds	1
r0x11B7:	.ds	1
r0x11B8:	.ds	1
r0x11B9:	.ds	1
r0x11BA:	.ds	1
r0x11BB:	.ds	1
r0x11CF:	.ds	1
r0x11D5:	.ds	1
r0x11D6:	.ds	1
r0x11D7:	.ds	1
r0x11D8:	.ds	1
r0x11D9:	.ds	1
r0x11E0:	.ds	1
r0x11E1:	.ds	1
r0x11E2:	.ds	1
r0x11E3:	.ds	1
r0x11E5:	.ds	1
r0x11E6:	.ds	1
r0x11E7:	.ds	1
r0x11EF:	.ds	1
r0x11F7:	.ds	1
r0x11F8:	.ds	1
r0x11F9:	.ds	1
r0x1205:	.ds	1
r0x1208:	.ds	1
r0x1209:	.ds	1
r0x120A:	.ds	1
r0x120B:	.ds	1
r0x120C:	.ds	1
r0x120D:	.ds	1
r0x120E:	.ds	1
r0x120F:	.ds	1
r0x1211:	.ds	1
r0x1212:	.ds	1
r0x1213:	.ds	1
r0x1217:	.ds	1
r0x121A:	.ds	1
r0x121F:	.ds	1
r0x1221:	.ds	1
r0x1222:	.ds	1
r0x1223:	.ds	1
r0x1224:	.ds	1
_stdout_putf:	.ds	2
_stdout_putp:	.ds	2
	.area DSEG (DATA); (local stack unassigned) 
_tfp_format_STK06:	.ds	1
	.globl _tfp_format_STK06
_tfp_format_STK05:	.ds	1
	.globl _tfp_format_STK05
_tfp_format_STK04:	.ds	1
	.globl _tfp_format_STK04
_tfp_format_STK03:	.ds	1
	.globl _tfp_format_STK03
_tfp_format_STK02:	.ds	1
	.globl _tfp_format_STK02
_tfp_format_STK01:	.ds	1
	.globl _tfp_format_STK01
_tfp_format_STK00:	.ds	1
	.globl _tfp_format_STK00
_putcp_STK01:	.ds	1
	.globl _putcp_STK01
_putcp_STK00:	.ds	1
	.globl _putcp_STK00
_putch_STK01:	.ds	1
_uli2a_STK00:	.ds	1
_uli2a_STK01:	.ds	1
_uli2a_STK02:	.ds	1
_uli2a_STK03:	.ds	1
_uli2a_STK04:	.ds	1
_uli2a_STK05:	.ds	1
_uli2a_STK06:	.ds	1
_uli2a_STK07:	.ds	1
_uli2a_STK08:	.ds	1
	.globl __divulong_STK06
	.globl __divulong_STK05
	.globl __divulong_STK04
	.globl __divulong_STK03
	.globl __divulong_STK02
	.globl __divulong_STK01
	.globl __divulong_STK00
	.globl __mullong_STK06
	.globl __mullong_STK05
	.globl __mullong_STK04
	.globl __mullong_STK03
	.globl __mullong_STK02
	.globl __mullong_STK01
	.globl __mullong_STK00
	.globl __modulong_STK06
	.globl __modulong_STK05
	.globl __modulong_STK04
	.globl __modulong_STK03
	.globl __modulong_STK02
	.globl __modulong_STK01
	.globl __modulong_STK00
_li2a_STK00:	.ds	1
_li2a_STK01:	.ds	1
_li2a_STK02:	.ds	1
_li2a_STK03:	.ds	1
_li2a_STK04:	.ds	1
_ui2a_STK00:	.ds	1
_ui2a_STK01:	.ds	1
_ui2a_STK02:	.ds	1
_ui2a_STK03:	.ds	1
_ui2a_STK04:	.ds	1
_ui2a_STK05:	.ds	1
_ui2a_STK06:	.ds	1
	.globl __divuint_STK02
	.globl __divuint_STK01
	.globl __divuint_STK00
	.globl __mulint_STK02
	.globl __mulint_STK01
	.globl __mulint_STK00
	.globl __moduint_STK02
	.globl __moduint_STK01
	.globl __moduint_STK00
_i2a_STK00:	.ds	1
_i2a_STK01:	.ds	1
_i2a_STK02:	.ds	1
_a2i_STK00:	.ds	1
_a2i_STK01:	.ds	1
_a2i_STK02:	.ds	1
_a2i_STK03:	.ds	1
_a2i_STK04:	.ds	1
_a2i_STK05:	.ds	1
_putchw_STK00:	.ds	1
_putchw_STK01:	.ds	1
_putchw_STK02:	.ds	1
_putchw_STK03:	.ds	1
_putchw_STK04:	.ds	1
_putchw_STK05:	.ds	1
_putchw_STK06:	.ds	1
_putchw_STK07:	.ds	1
_tfp_format_fmt_65536_33:	.ds	2
	.globl _tfp_format_fmt_65536_33
_tfp_format_w_196608_36:	.ds	2
	.globl _tfp_format_w_196608_36
_tfp_format_bf_65536_34:	.ds	12
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x121F:NULL+0:-1:1
	;--cdb--W:r0x1218:NULL+0:-1:1
	;--cdb--W:r0x1219:NULL+0:-1:1
	;--cdb--W:r0x120C:NULL+0:4618:0
	;--cdb--W:r0x120D:NULL+0:4619:0
	;--cdb--W:r0x120D:NULL+0:4618:0
	;--cdb--W:r0x1210:NULL+0:4621:0
	;--cdb--W:r0x1210:NULL+0:0:0
	;--cdb--W:r0x120F:NULL+0:4619:0
	;--cdb--W:r0x120F:NULL+0:4620:0
	;--cdb--W:r0x1211:NULL+0:4623:0
	;--cdb--W:r0x120D:NULL+0:-1:1
	;--cdb--W:r0x120C:NULL+0:-1:1
	;--cdb--W:r0x1210:NULL+0:-1:1
	;--cdb--W:r0x120F:NULL+0:-1:1
	;--cdb--W:r0x1211:NULL+0:-1:1
	;--cdb--W:r0x1214:NULL+0:-1:1
	;--cdb--W:r0x1213:NULL+0:-1:1
	;--cdb--W:r0x120B:NULL+0:-1:1
	;--cdb--W:r0x11F7:NULL+0:-1:1
	;--cdb--W:r0x11E4:NULL+0:14:0
	;--cdb--W:r0x11D6:NULL+0:4565:0
	;--cdb--W:r0x11D7:NULL+0:0:0
	;--cdb--W:r0x11D7:NULL+0:-1:1
	;--cdb--W:r0x11B4:NULL+0:-1:1
	;--cdb--W:r0x11B7:NULL+0:-1:1
	;--cdb--W:r0x11BC:NULL+0:-1:1
	;--cdb--W:r0x11BD:NULL+0:-1:1
	;--cdb--W:r0x11BB:NULL+0:-1:1
	;--cdb--W:r0x1178:NULL+0:-1:1
	;--cdb--W:r0x1177:NULL+0:-1:1
	;--cdb--W:r0x117D:NULL+0:-1:1
	;--cdb--W:r0x117F:NULL+0:-1:1
	;--cdb--W:r0x1180:NULL+0:-1:1
	;--cdb--W:r0x117E:NULL+0:-1:1
	;--cdb--W:r0x1177:NULL+0:14:0
	;--cdb--W:r0x1176:NULL+0:13:0
	;--cdb--W:r0x1175:NULL+0:12:0
	;--cdb--W:r0x117B:NULL+0:0:0
	;--cdb--W:r0x117B:NULL+0:-1:1
	;--cdb--W:r0x1155:NULL+0:-1:1
	;--cdb--W:r0x1157:NULL+0:4437:0
	;--cdb--W:r0x1156:NULL+0:4436:0
	;--cdb--W:r0x1158:NULL+0:4436:0
	end
