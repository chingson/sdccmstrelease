;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_mulint.c"
	.module _mulint
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_mulint$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$_mulint$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _mulint-code 
.globl __mulint

;--------------------------------------------------------
	.FUNC __mulint:$PNUM 4:$L:r0x1155:$L:__mulint_STK00:$L:__mulint_STK01:$L:__mulint_STK02:$L:r0x1158\
:$L:r0x1159:$L:r0x115A:$L:r0x115B
;--------------------------------------------------------
;	.line	33; "_mulint.c"	_mulint (int a, int b)
__mulint:	;Function start
	STA	r0x1155
;	;.line	35; "_mulint.c"	int result = 0;
	CLRA	
	STA	r0x1158
	STA	r0x1159
;	;.line	39; "_mulint.c"	for (i = 0; i < 8u; i++) {
	LDA	#0x08
	STA	r0x115A
_00113_DS_:
;	;.line	41; "_mulint.c"	if (a & 0x0001u) result += b;
	LDA	__mulint_STK00
	SHR	
	JNC	_00109_DS_
	LDA	r0x1158
	ADD	__mulint_STK02
	STA	r0x1158
	LDA	r0x1159
	ADDC	__mulint_STK01
	STA	r0x1159
_00109_DS_:
;	;.line	42; "_mulint.c"	if (sizeof (a) > 1 && (a & 0x00000100u)) result += (b << 8u);
	LDA	r0x1155
	SHR	
	JNC	_00108_DS_
	CLRB	_C
	LDA	r0x1159
	ADDC	__mulint_STK02
	STA	r0x1159
_00108_DS_:
;	;.line	45; "_mulint.c"	a = ((unsigned int)a) >> 1u;
	LDA	r0x1155
	SHR	
	STA	r0x1155
	LDA	__mulint_STK00
	ROR	
	STA	__mulint_STK00
;	;.line	46; "_mulint.c"	b <<= 1u;
	LDA	__mulint_STK02
	SHL	
	STA	__mulint_STK02
	LDA	__mulint_STK01
	ROL	
	STA	__mulint_STK01
	LDA	r0x115A
	DECA	
	STA	r0x115B
	STA	r0x115A
;	;.line	39; "_mulint.c"	for (i = 0; i < 8u; i++) {
	LDA	r0x115B
	JNZ	_00113_DS_
;	;.line	49; "_mulint.c"	return result;
	LDA	r0x1158
	STA	STK00
	LDA	r0x1159
;	;.line	50; "_mulint.c"	}
	RET	
; exit point of __mulint
	.ENDFUNC __mulint
	;--cdb--S:G$_mulint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:L_mulint._mulint$b$65536$1({2}SI:S),R,0,0,[__mulint_STK02,__mulint_STK01]
	;--cdb--S:L_mulint._mulint$a$65536$1({2}SI:S),R,0,0,[__mulint_STK00,r0x1155]
	;--cdb--S:L_mulint._mulint$result$65536$2({2}SI:S),R,0,0,[r0x1158,r0x1159]
	;--cdb--S:L_mulint._mulint$i$65536$2({1}SC:U),R,0,0,[r0x115A]
	;--cdb--S:G$_mulint$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__mulint
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__mulint_0	udata
r0x1155:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__mulint_STK00:	.ds	1
	.globl __mulint_STK00
__mulint_STK01:	.ds	1
	.globl __mulint_STK01
__mulint_STK02:	.ds	1
	.globl __mulint_STK02
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115C:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:4449:0
	;--cdb--W:r0x115B:NULL+0:4440:0
	;--cdb--W:r0x115B:NULL+0:4447:0
	;--cdb--W:r0x115C:NULL+0:4441:0
	;--cdb--W:r0x115C:NULL+0:4437:0
	;--cdb--W:r0x115C:NULL+0:4448:0
	;--cdb--W:r0x115E:NULL+0:4449:0
	;--cdb--W:r0x115D:NULL+0:0:0
	;--cdb--W:r0x115D:NULL+0:-1:1
	end
