;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"isgraph.c"
	.module isgraph
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isgraph$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isblank$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$islower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isupper$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; isgraph-code 
.globl _isgraph

;--------------------------------------------------------
	.FUNC _isgraph:$PNUM 2:$L:r0x1155:$L:_isgraph_STK00:$L:r0x1156
;--------------------------------------------------------
;	.line	33; "isgraph.c"	int isgraph (int c)
_isgraph:	;Function start
	STA	r0x1155
;	;.line	35; "isgraph.c"	return (c > ' ' && c <= '~');
	SETB	_C
	LDA	#0x20
	SUBB	_isgraph_STK00
	CLRA	
	SUBSI	
	SUBB	r0x1155
	JC	_00107_DS_
	SETB	_C
	LDA	#0x7e
	SUBB	_isgraph_STK00
	CLRA	
	SUBSI	
	SUBB	r0x1155
	JC	_00108_DS_
_00107_DS_:
	CLRA	
	STA	_isgraph_STK00
	JMP	_00109_DS_
_00108_DS_:
	LDA	#0x01
	STA	_isgraph_STK00
_00109_DS_:
	LDA	_isgraph_STK00
	JPL	_00116_DS_
	LDA	#0xff
	JMP	_00117_DS_
_00116_DS_:
	CLRA	
_00117_DS_:
	STA	r0x1156
	LDA	_isgraph_STK00
	STA	STK00
	LDA	r0x1156
;	;.line	36; "isgraph.c"	}
	RET	
; exit point of _isgraph
	.ENDFUNC _isgraph
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:Lisgraph.isblank$c$65536$13({2}SI:S),E,0,0
	;--cdb--S:Lisgraph.isdigit$c$65536$15({2}SI:S),E,0,0
	;--cdb--S:Lisgraph.islower$c$65536$17({2}SI:S),E,0,0
	;--cdb--S:Lisgraph.isupper$c$65536$19({2}SI:S),E,0,0
	;--cdb--S:Lisgraph.isgraph$c$65536$21({2}SI:S),R,0,0,[_isgraph_STK00,r0x1155]
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_isgraph
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_isgraph_0	udata
r0x1155:	.ds	1
r0x1156:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_isgraph_STK00:	.ds	1
	.globl _isgraph_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1155:NULL+0:4439:0
	end
