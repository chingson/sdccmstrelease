#include"ms326.h"
#include"ms326typedef.h"
#include"ms326sphlib.h"

// highlow=0 means 780~7ff, =1 means 700~77f
void api_clear_filter_mem(BYTE highlow)
{
    ROMPH=0x87; //
    if(highlow)
    {
        ROMPL=0x00;
    } else
    {
        ROMPL=0x80;
    }
    do {
        ROMPINC=0;
    } while(ROMPL&0x7f);
}

