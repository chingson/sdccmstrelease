;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_mullonglong.c"
	.module _mullonglong
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_mullonglong$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$_mullonglong$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _mullonglong-code 
.globl __mullonglong

;--------------------------------------------------------
	.FUNC __mullonglong:$PNUM 16:$C:__shr_ulonglong:$C:__mullong:$C:__shl_longlong\
:$L:r0x115B:$L:__mullonglong_STK00:$L:__mullonglong_STK01:$L:__mullonglong_STK02:$L:__mullonglong_STK03\
:$L:__mullonglong_STK04:$L:__mullonglong_STK05:$L:__mullonglong_STK06:$L:__mullonglong_STK07:$L:__mullonglong_STK08\
:$L:__mullonglong_STK09:$L:__mullonglong_STK10:$L:__mullonglong_STK11:$L:__mullonglong_STK12:$L:__mullonglong_STK13\
:$L:__mullonglong_STK14:$L:r0x1164:$L:r0x1165:$L:r0x1166:$L:r0x1167\
:$L:r0x1168:$L:r0x1169:$L:r0x116A:$L:r0x116B:$L:r0x116C\
:$L:r0x116D:$L:r0x116E:$L:r0x1175:$L:r0x1176:$L:r0x1179\
:$L:r0x1177:$L:r0x117E:$L:r0x117F:$L:r0x118D:$L:r0x118C\

;--------------------------------------------------------
;	.line	36; "_mullonglong.c"	long long _mullonglong(long long ll,long long lr)
__mullonglong:	;Function start
	STA	r0x115B
;	;.line	38; "_mullonglong.c"	unsigned long long ret = 0ull;
	CLRA	
	STA	r0x1164
	STA	r0x1165
	STA	r0x1166
	STA	r0x1167
	STA	r0x1168
	STA	r0x1169
	STA	r0x116A
	STA	r0x116B
;	;.line	41; "_mullonglong.c"	for (i = 0; i < sizeof (long long); i+=2)
	STA	r0x116C
_00110_DS_:
;	;.line	43; "_mullonglong.c"	unsigned long l = (((unsigned long long)ll) >> ((unsigned char)(i <<3 )))&0xffffL;
	LDA	r0x116C
	SHL	
	SHL	
	SHL	
	STA	r0x1176
	LDA	r0x115B
	STA	STK00
	LDA	__mullonglong_STK00
	STA	STK01
	LDA	__mullonglong_STK01
	STA	STK02
	LDA	__mullonglong_STK02
	STA	STK03
	LDA	__mullonglong_STK03
	STA	STK04
	LDA	__mullonglong_STK04
	STA	STK05
	LDA	__mullonglong_STK05
	STA	STK06
	LDA	__mullonglong_STK06
	STA	_PTRCL
	LDA	r0x1176
	CALL	__shr_ulonglong
	LDA	_PTRCL
	STA	r0x116D
	LDA	STK06
	STA	r0x116E
;	;.line	44; "_mullonglong.c"	for(j = 0; (i + j) < sizeof (long long); j+=2)
	CLRA	
	STA	r0x1175
_00108_DS_:
	LDA	r0x116C
	ADD	r0x1175
	STA	r0x1176
	CLRA	
	ROL	
	STA	r0x1177
	LDA	r0x1176
	ADD	#0xf8
	LDA	r0x1177
	XOR	#0x80
	ADDC	#0x7f
	JC	_00111_DS_
;	;.line	46; "_mullonglong.c"	unsigned long r = (((unsigned long long)lr) >> ((unsigned char)(j <<3)))&0xffffL;
	LDA	r0x1175
	STA	r0x117E
	SHL	
	SHL	
	SHL	
	STA	r0x117F
	LDA	__mullonglong_STK07
	STA	STK00
	LDA	__mullonglong_STK08
	STA	STK01
	LDA	__mullonglong_STK09
	STA	STK02
	LDA	__mullonglong_STK10
	STA	STK03
	LDA	__mullonglong_STK11
	STA	STK04
	LDA	__mullonglong_STK12
	STA	STK05
	LDA	__mullonglong_STK13
	STA	STK06
	LDA	__mullonglong_STK14
	STA	_PTRCL
	LDA	r0x117F
	CALL	__shr_ulonglong
	LDA	_PTRCL
;	;.line	47; "_mullonglong.c"	ret += ((unsigned long long)(l * r)) << ((unsigned char)((i + j) << 3 ));
	STA	__mullong_STK06
	LDA	STK06
	STA	__mullong_STK05
	CLRA	
	STA	__mullong_STK04
	STA	__mullong_STK03
	LDA	r0x116D
	STA	__mullong_STK02
	LDA	r0x116E
	STA	__mullong_STK01
	CLRA	
	STA	__mullong_STK00
	CALL	__mullong
	STA	r0x1179
	LDA	r0x116C
	ADD	r0x1175
	SHL	
	SHL	
	SHL	
	STA	r0x1176
	LDA	STK00
	STA	r0x118D
	LDA	STK01
	STA	r0x118C
	LDA	STK02
	STA	r0x117F
	CLRA	
	STA	STK00
	STA	STK01
	STA	STK02
	STA	STK03
	LDA	r0x1179
	STA	STK04
	LDA	r0x118D
	STA	STK05
	LDA	r0x118C
	STA	STK06
	LDA	r0x117F
	STA	_PTRCL
	LDA	r0x1176
	CALL	__shl_longlong
	LDA	_PTRCL
	ADD	r0x1164
	STA	r0x1164
	LDA	r0x1165
	ADDC	STK06
	STA	r0x1165
	LDA	r0x1166
	ADDC	STK05
	STA	r0x1166
	LDA	r0x1167
	ADDC	STK04
	STA	r0x1167
	LDA	r0x1168
	ADDC	STK03
	STA	r0x1168
	LDA	r0x1169
	ADDC	STK02
	STA	r0x1169
	LDA	r0x116A
	ADDC	STK01
	STA	r0x116A
	LDA	r0x116B
	ADDC	STK00
	STA	r0x116B
;	;.line	44; "_mullonglong.c"	for(j = 0; (i + j) < sizeof (long long); j+=2)
	LDA	#0x02
	ADD	r0x117E
	STA	r0x1175
	JMP	_00108_DS_
_00111_DS_:
;	;.line	41; "_mullonglong.c"	for (i = 0; i < sizeof (long long); i+=2)
	LDA	r0x116C
	ADD	#0x02
	STA	r0x116C
	ADD	#0xf8
	JNC	_00110_DS_
;	;.line	51; "_mullonglong.c"	return(ret);
	LDA	r0x1164
	STA	STK06
	LDA	r0x1165
	STA	STK05
	LDA	r0x1166
	STA	STK04
	LDA	r0x1167
	STA	STK03
	LDA	r0x1168
	STA	STK02
	LDA	r0x1169
	STA	STK01
	LDA	r0x116A
	STA	STK00
	LDA	r0x116B
;	;.line	52; "_mullonglong.c"	}
	RET	
; exit point of __mullonglong
	.ENDFUNC __mullonglong
	;--cdb--S:G$_mullonglong$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:L_mullonglong._mullonglong$lr$65536$1({8}SI:S),R,0,0,[__mullonglong_STK14,__mullonglong_STK13,__mullonglong_STK12,__mullonglong_STK11__mullonglong_STK10__mullonglong_STK09__mullonglong_STK08__mullonglong_STK07]
	;--cdb--S:L_mullonglong._mullonglong$ll$65536$1({8}SI:S),R,0,0,[__mullonglong_STK06,__mullonglong_STK05,__mullonglong_STK04,__mullonglong_STK03__mullonglong_STK02__mullonglong_STK01__mullonglong_STK00r0x115B]
	;--cdb--S:L_mullonglong._mullonglong$ret$65536$2({8}SI:U),R,0,0,[r0x1164,r0x1165,r0x1166,r0x1167r0x1168r0x1169r0x116Ar0x116B]
	;--cdb--S:L_mullonglong._mullonglong$i$65536$2({1}SC:U),R,0,0,[r0x116C]
	;--cdb--S:L_mullonglong._mullonglong$j$65536$2({1}SC:U),R,0,0,[r0x1175]
	;--cdb--S:L_mullonglong._mullonglong$l$196608$4({4}SL:U),R,0,0,[r0x116D,r0x116E,r0x116F,r0x1170]
	;--cdb--S:L_mullonglong._mullonglong$r$327680$6({4}SL:U),R,0,0,[r0x1180,r0x1181,r0x1182,r0x1183r0x1184r0x1185r0x1186r0x1187]
	;--cdb--S:G$_mullonglong$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__mullong
	.globl	__shr_ulonglong
	.globl	__shl_longlong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__mullonglong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__mullonglong_0	udata
r0x115B:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116C:	.ds	1
r0x116D:	.ds	1
r0x116E:	.ds	1
r0x1175:	.ds	1
r0x1176:	.ds	1
r0x1177:	.ds	1
r0x1179:	.ds	1
r0x117E:	.ds	1
r0x117F:	.ds	1
r0x118C:	.ds	1
r0x118D:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__mullonglong_STK00:	.ds	1
	.globl __mullonglong_STK00
__mullonglong_STK01:	.ds	1
	.globl __mullonglong_STK01
__mullonglong_STK02:	.ds	1
	.globl __mullonglong_STK02
__mullonglong_STK03:	.ds	1
	.globl __mullonglong_STK03
__mullonglong_STK04:	.ds	1
	.globl __mullonglong_STK04
__mullonglong_STK05:	.ds	1
	.globl __mullonglong_STK05
__mullonglong_STK06:	.ds	1
	.globl __mullonglong_STK06
__mullonglong_STK07:	.ds	1
	.globl __mullonglong_STK07
__mullonglong_STK08:	.ds	1
	.globl __mullonglong_STK08
__mullonglong_STK09:	.ds	1
	.globl __mullonglong_STK09
__mullonglong_STK10:	.ds	1
	.globl __mullonglong_STK10
__mullonglong_STK11:	.ds	1
	.globl __mullonglong_STK11
__mullonglong_STK12:	.ds	1
	.globl __mullonglong_STK12
__mullonglong_STK13:	.ds	1
	.globl __mullonglong_STK13
__mullonglong_STK14:	.ds	1
	.globl __mullonglong_STK14
	.globl __mullong_STK06
	.globl __mullong_STK05
	.globl __mullong_STK04
	.globl __mullong_STK03
	.globl __mullong_STK02
	.globl __mullong_STK01
	.globl __mullong_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x117D:NULL+0:-1:1
	;--cdb--W:r0x117C:NULL+0:-1:1
	;--cdb--W:r0x117B:NULL+0:-1:1
	;--cdb--W:r0x117A:NULL+0:-1:1
	;--cdb--W:r0x116F:NULL+0:-1:1
	;--cdb--W:r0x1170:NULL+0:-1:1
	;--cdb--W:r0x1187:NULL+0:-1:1
	;--cdb--W:r0x1186:NULL+0:-1:1
	;--cdb--W:r0x1185:NULL+0:-1:1
	;--cdb--W:r0x1184:NULL+0:-1:1
	;--cdb--W:r0x1178:NULL+0:-1:1
	;--cdb--W:r0x1179:NULL+0:-1:1
	;--cdb--W:r0x1176:NULL+0:-1:1
	;--cdb--W:r0x116D:NULL+0:-1:1
	;--cdb--W:r0x116D:NULL+0:4505:0
	;--cdb--W:r0x116E:NULL+0:4504:0
	;--cdb--W:r0x116F:NULL+0:4503:0
	;--cdb--W:r0x1170:NULL+0:4502:0
	;--cdb--W:r0x1171:NULL+0:4501:0
	;--cdb--W:r0x1172:NULL+0:4500:0
	;--cdb--W:r0x1173:NULL+0:4499:0
	;--cdb--W:r0x1174:NULL+0:4443:0
	;--cdb--W:r0x1175:NULL+0:4460:0
	;--cdb--W:r0x1175:NULL+0:4505:0
	;--cdb--W:r0x1176:NULL+0:4460:0
	;--cdb--W:r0x1176:NULL+0:4513:0
	;--cdb--W:r0x1176:NULL+0:4480:0
	;--cdb--W:r0x1176:NULL+0:12:0
	;--cdb--W:r0x1176:NULL+0:4479:0
	;--cdb--W:r0x117D:NULL+0:4443:0
	;--cdb--W:r0x117D:NULL+0:4506:0
	;--cdb--W:r0x117C:NULL+0:4499:0
	;--cdb--W:r0x117C:NULL+0:4507:0
	;--cdb--W:r0x117B:NULL+0:4500:0
	;--cdb--W:r0x117B:NULL+0:4508:0
	;--cdb--W:r0x117A:NULL+0:4501:0
	;--cdb--W:r0x117A:NULL+0:4509:0
	;--cdb--W:r0x1179:NULL+0:4502:0
	;--cdb--W:r0x1179:NULL+0:10:0
	;--cdb--W:r0x1179:NULL+0:0:0
	;--cdb--W:r0x1179:NULL+0:4510:0
	;--cdb--W:r0x1178:NULL+0:4503:0
	;--cdb--W:r0x1178:NULL+0:9:0
	;--cdb--W:r0x1178:NULL+0:4469:0
	;--cdb--W:r0x1178:NULL+0:4511:0
	;--cdb--W:r0x1178:NULL+0:14:0
	;--cdb--W:r0x1177:NULL+0:4504:0
	;--cdb--W:r0x1177:NULL+0:8:0
	;--cdb--W:r0x1177:NULL+0:4512:0
	;--cdb--W:r0x1177:NULL+0:4481:0
	;--cdb--W:r0x1177:NULL+0:13:0
	;--cdb--W:r0x117F:NULL+0:12:0
	;--cdb--W:r0x1187:NULL+0:4506:0
	;--cdb--W:r0x1186:NULL+0:4507:0
	;--cdb--W:r0x1185:NULL+0:4508:0
	;--cdb--W:r0x1184:NULL+0:4509:0
	;--cdb--W:r0x1183:NULL+0:4510:0
	;--cdb--W:r0x1183:NULL+0:10:0
	;--cdb--W:r0x1183:NULL+0:0:0
	;--cdb--W:r0x1182:NULL+0:4511:0
	;--cdb--W:r0x1182:NULL+0:9:0
	;--cdb--W:r0x1182:NULL+0:4473:0
	;--cdb--W:r0x1181:NULL+0:4512:0
	;--cdb--W:r0x1181:NULL+0:8:0
	;--cdb--W:r0x1181:NULL+0:14:0
	;--cdb--W:r0x1180:NULL+0:4513:0
	;--cdb--W:r0x1180:NULL+0:13:0
	;--cdb--W:r0x1188:NULL+0:0:0
	;--cdb--W:r0x1189:NULL+0:0:0
	;--cdb--W:r0x118A:NULL+0:0:0
	;--cdb--W:r0x118B:NULL+0:4470:0
	;--cdb--W:r0x1192:NULL+0:14:0
	;--cdb--W:r0x1191:NULL+0:13:0
	;--cdb--W:r0x1190:NULL+0:12:0
	;--cdb--W:r0x118F:NULL+0:0:0
	;--cdb--W:r0x118F:NULL+0:11:0
	;--cdb--W:r0x118E:NULL+0:4473:0
	;--cdb--W:r0x118E:NULL+0:10:0
	;--cdb--W:r0x118D:NULL+0:9:0
	;--cdb--W:r0x118C:NULL+0:8:0
	;--cdb--W:r0x1175:NULL+0:-1:1
	;--cdb--W:r0x1183:NULL+0:-1:1
	;--cdb--W:r0x1188:NULL+0:-1:1
	;--cdb--W:r0x1189:NULL+0:-1:1
	;--cdb--W:r0x118A:NULL+0:-1:1
	;--cdb--W:r0x118F:NULL+0:-1:1
	;--cdb--W:r0x1177:NULL+0:0:0
	;--cdb--W:r0x1182:NULL+0:0:0
	;--cdb--W:r0x1177:NULL+0:-1:1
	;--cdb--W:r0x1182:NULL+0:-1:1
	;--cdb--W:r0x1180:NULL+0:-1:1
	;--cdb--W:r0x1192:NULL+0:0:0
	;--cdb--W:r0x1192:NULL+0:-1:1
	;--cdb--W:r0x1191:NULL+0:0:0
	;--cdb--W:r0x1191:NULL+0:-1:1
	;--cdb--W:r0x1190:NULL+0:0:0
	;--cdb--W:r0x1190:NULL+0:-1:1
	;--cdb--W:r0x116F:NULL+0:0:0
	;--cdb--W:r0x1170:NULL+0:0:0
	end
