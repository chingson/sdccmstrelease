;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_strncmp.c"
	.module _strncmp
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$strncmp$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _strncmp-code 
.globl _strncmp

;--------------------------------------------------------
	.FUNC _strncmp:$PNUM 6:$L:r0x1155:$L:_strncmp_STK00:$L:_strncmp_STK01:$L:_strncmp_STK02:$L:_strncmp_STK03\
:$L:_strncmp_STK04:$L:r0x115A
;--------------------------------------------------------
;	.line	31; "_strncmp.c"	int strncmp ( char * first, char * last, size_t count )
_strncmp:	;Function start
	STA	r0x1155
;	;.line	33; "_strncmp.c"	if (!count)
	LDA	_strncmp_STK04
	ORA	_strncmp_STK03
	JNZ	_00109_DS_
;	;.line	34; "_strncmp.c"	return(0);
	CLRA	
	STA	STK00
	JMP	_00112_DS_
_00109_DS_:
;	;.line	36; "_strncmp.c"	while (--count && *first && *first == *last) {
	LDA	_strncmp_STK04
	DECA	
	STA	_strncmp_STK04
	LDA	#0xff
	ADDC	_strncmp_STK03
	STA	_strncmp_STK03
	ORA	_strncmp_STK04
	JZ	_00111_DS_
	LDA	_strncmp_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115A
	JZ	_00111_DS_
	LDA	_strncmp_STK02
	STA	_ROMPL
	LDA	_strncmp_STK01
	STA	_ROMPH
	LDA	@_ROMPINC
	XOR	r0x115A
	JNZ	_00111_DS_
;	;.line	37; "_strncmp.c"	first++;
	LDA	_strncmp_STK00
	INCA	
	STA	_strncmp_STK00
	CLRA	
	ADDC	r0x1155
	STA	r0x1155
;	;.line	38; "_strncmp.c"	last++;
	LDA	_strncmp_STK02
	INCA	
	STA	_strncmp_STK02
	CLRA	
	ADDC	_strncmp_STK01
	STA	_strncmp_STK01
	JMP	_00109_DS_
_00111_DS_:
;	;.line	41; "_strncmp.c"	return( *first - *last );
	LDA	_strncmp_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_strncmp_STK00
	LDA	_strncmp_STK02
	STA	_ROMPL
	LDA	_strncmp_STK01
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_strncmp_STK04
	SETB	_C
	LDA	_strncmp_STK00
	SUBB	_strncmp_STK04
	STA	_strncmp_STK00
	CLRA	
	SUBB	#0x00
	STA	r0x1155
	LDA	_strncmp_STK00
	STA	STK00
	LDA	r0x1155
_00112_DS_:
;	;.line	42; "_strncmp.c"	}
	RET	
; exit point of _strncmp
	.ENDFUNC _strncmp
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_strncmp.strncmp$count$65536$21({2}SI:U),R,0,0,[_strncmp_STK04,_strncmp_STK03]
	;--cdb--S:L_strncmp.strncmp$last$65536$21({2}DG,SC:U),R,0,0,[_strncmp_STK02,_strncmp_STK01]
	;--cdb--S:L_strncmp.strncmp$first$65536$21({2}DG,SC:U),R,0,0,[_strncmp_STK00,r0x1155]
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_strncmp
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__strncmp_0	udata
r0x1155:	.ds	1
r0x115A:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_strncmp_STK00:	.ds	1
	.globl _strncmp_STK00
_strncmp_STK01:	.ds	1
	.globl _strncmp_STK01
_strncmp_STK02:	.ds	1
	.globl _strncmp_STK02
_strncmp_STK03:	.ds	1
	.globl _strncmp_STK03
_strncmp_STK04:	.ds	1
	.globl _strncmp_STK04
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:_strncmp_STK01:NULL+0:0:0
	;--cdb--W:_strncmp_STK02:NULL+0:4448:0
	;--cdb--W:r0x115B:NULL+0:-1:1
	;--cdb--W:_strncmp_STK01:NULL+0:-1:1
	;--cdb--W:r0x1155:NULL+0:0:0
	;--cdb--W:_strncmp_STK04:NULL+0:-1:1
	;--cdb--W:r0x1155:NULL+0:-1:1
	end
