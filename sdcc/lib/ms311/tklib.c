#include<ms326.h>
#include <string.h>
#include<ms326sphlib.h>



BYTE tch_total_ch;
BYTE tch_mon;
touch_entry *rambufp1;
USHORT *procdatap;
unsigned long *baseline;
BYTE *threshold_ratio;


//const char defaultEntryValue[]={0x7, 0x4, 0x0, 0x8, 0x0, 0x1}
static BYTE wait_tk_int(BYTE stdby, BYTE (*callback)(void))
{
	while(!(TOUCHC&0x80))
	{
		if(callback!=NULL)
		{
			if(callback())
				return 1;
		}
		else if(stdby)
			api_enter_stdby_mode(0,0,0,0,0);
	}
	return 0;
		
}
USHORT api_tk_init(USHORT ch_en, BYTE mon, BYTE totalch, BYTE *bufp, USHORT *finalp, unsigned long *baselinei, unsigned char *th_ratio, BYTE (*callback)(void))
{
    touch_entry *tkramp;
    USHORT chk_channel;
    USHORT init_result=0;
    BYTE sw;
    BYTE dum=1;
    tch_mon = mon;
    procdatap = finalp;
    tch_total_ch=totalch+1;
    rambufp1=(touch_entry*)bufp;
    baseline=baselinei; // baseline

    PABSKIP=0xffff;
    TRAMBUFP=bufp; //msb missed
    memset(bufp, 0, tch_total_ch*8); // maximum 16*8=128
    PABTEN=ch_en;
    TOUCHC=TOUCHC_DEFAULT;
    DUMMYC=6; // dummy count enabled
    threshold_ratio=th_ratio;

    tkramp = (touch_entry*)bufp;
    for(chk_channel=1; chk_channel||dum; chk_channel<<=1)
    {

        if((chk_channel!=0) && !(chk_channel&ch_en))
            continue;
        // initial memory set

        tkramp->toff=7; // smallest
        tkramp->nmossw=4;
        tkramp->period=4096; // 2048 clock seems not long enough?
        tkramp->threshold=2048; // we take 0x200 as the threshold
        PABSKIP=chk_channel^0xffff;
        if(chk_channel==0)
        {
            DUMMYC=2; // enable dummy
        }
        for(sw=6; sw>=4; sw--) // small current, the count is large -> small
        {
            tkramp->nmossw=sw;

            // there is a bug canot clear in shuttle
            // but shoud be cleared
            tkramp->count=0;
            TOUCHC=TOUCHC_DEFAULT;

            do {
                //tkramp->count=0; // it is possible short to GND->it will hang
                // hang is hang
                if(wait_tk_int(1, callback))
                	goto drop;
                TOUCHC=TOUCHC_DEFAULT;
                //tkramp->count=0;
                if(wait_tk_int(1, callback))
                	goto drop;
                //tkramp->count=0;
                TOUCHC=TOUCHC_DEFAULT;
            } while((PABTR||(DUMMYC&1))&& (++(tkramp->toff)<29));
            if(!PABTR)
            {
                init_result|=chk_channel;
                break;
            }

        }
        tkramp++;
        if(chk_channel==0)
            dum=0;
    }
    // after PA,PB, there is dummy


drop:
    PABSKIP=0; // start touch monitor!!
    DUMMYC=2;

    // baseline calibration
    //rambufp1[tch_total_ch-1].count=0;
    //while(rambufp1[tch_total_ch-1].count==0)
    //{
        //if(callback!=NULL && callback())
            //return 0;
    //}
    if(wait_tk_int(1, callback))
    	return 0;
    if(wait_tk_int(1, callback))
    	return 0;
    //TOUCHC=0; // halt for some time that we calculate threshold
    
    for(sw=0; sw<tch_total_ch; sw++)
    {
        USHORT s = rambufp1[sw].count;
        baseline[sw]=((unsigned long)s) <<8;

        if(threshold_ratio==NULL)
            rambufp1[sw].threshold=s+(s>>5); // take 3 %
        else
            rambufp1[sw].threshold=((unsigned long)s*((unsigned long) threshold_ratio[sw]))>>7;

    }

    if(mon==1)// use PB SSPI
    {
        SSPIC=1;
    } else if(mon==2) // use PC SSPI
    {
        SSPIC=5;
    }
	TOUCHC=TOUCHC_DEFAULT;
    return init_result;

}
void api_tk_stop(void)
{
    TOUCHC=0;
    PABTEN=0;
}



