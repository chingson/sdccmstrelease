#include"ms326.h"
#include"ms326typedef.h"
#include"ms326sphlib.h"

extern BYTE api_rampage;
extern USHORT api_endpage;
extern BYTE api_mode;
extern BYTE api_fifostart;
extern BYTE api_fifoend;

//extern __code const BYTE ms326_nextrecpage[4];

//extern USHORT api_endpage;
BYTE api_rec_job_no_write(adpcm_state *the_state, USHORT *pwr)

{
    //BYTE halt_page;
    if(RDMAH==(api_rampage>>4))
        return 0;
    api_rampage+=0x10;
    if(api_rampage>=api_fifoend)
        api_rampage=api_fifostart;
    if(!(api_mode&1))
    {
        the_state->index=ADP_IND;
        the_state->predict=ADP_VPLH;
    }
    *pwr=PWRHL;
    return 1;
}
// only 1 or 2

void api_rec_write_prev(BYTE pagenum)
{
//	BYTE page_to_write=ms326_nextrecpage[api_rampage];
    //BYTE i;

    BYTE page_to_write=api_rampage-0x10;
    if(page_to_write<api_fifostart)
        page_to_write=api_fifoend;
    if(pagenum==1)
    {
        page_to_write-=0x10;
        if(page_to_write<api_fifostart)
            page_to_write=api_fifoend;
    }
    while(page_to_write!=api_rampage)
    {
        SPIOPRAH=page_to_write>>3;
        SPIOP=1;
        SPIOP=4;
        SPIOP=0x20;
        page_to_write+=0x10;
        if(page_to_write>=api_fifoend)
            page_to_write=api_fifostart;

    }


}
BYTE api_rec_job_do_write( USHORT * pwr, USHORT *endpage)
{
    BYTE halt_page;
    if(RDMAH!=api_rampage)
    {
again:
        SPIOPRAH=api_rampage>>3;
        SPIOP=1;
        SPIOP=(api_rampage<<6)|0x4;// write this page
        SPIOP=0x20; // increase a page
        halt_page=api_rampage>>4;
        // following line may need 80 clocks... fine
        //api_rampage=ms326_nextrecpage[api_rampage];
        api_rampage+=0x10;
        if(api_rampage>=api_fifoend)
            api_rampage=api_fifostart;

        if(SPIM==(BYTE)(api_endpage&0xff) && SPIH==(BYTE)(api_endpage>>8))
        {
            *endpage=api_endpage;
            return 0;
        }
        if(!(SPIM&0xf))
        {
            ADCON|=halt_page<<3;
            SPIOP=1;
            SPIOP=2;
            ADCON|=0x38;
        }
        if(RDMAH!=api_rampage)
            goto again;
        *pwr=PWRHL; // 2 bytes !!
        *endpage=SPIMH;
        return 2;
    }
    return 1;
}


BYTE api_play_start_with_state(USHORT start_page, USHORT end_page, USHORT period, BYTE ulaw, BYTE dacosr, adpcm_state * the_state)
{
    api_clear_filter_mem(0);
    SPIH=start_page>>8;
    SPIM=start_page&0xff;
    SPIL=0;
    api_endpage=end_page;
    api_mode=ulaw;

    SPIOP=0x10; // clear end-code
    PDMAH=0x80;
    ULAWC&=0x1f;
    ADCON=0x00;
    SYSC2 &=0x7f;
    DAC_PH=period>>8;
    DAC_PL=period&0xff;
    if(ulaw & 1) // ulaw need no state
    {
        SPIOP=0x48; // read to 0x1xx
        SPIOP=0x20;
        if(SPIOP&0x80)
        {

            return 0; // fail
        }
        ULAWC=0xa0;

        DACON=dacosr|0x1a;

    } else
    {
        ADP_VPLH=the_state->predict;
        ADP_IND=the_state->index;
        SPIOP=0x88; // read page to 0x2xx
        SPIOP=0x20; // address increase a page
        SPIOP=0xc8; // read page to 0x3xx
        if(SPIOP&0x80)
        {
            return 0;
        }
        DACON=dacosr|0x7a; // load state
        DACON=dacosr|0x3a;

    }
    SPIOP=0x20;// post increase
    while(PDMAL==0);
    DACON|=1; // enable PA finally
    api_rampage=2; // 2xx~3xx
    return 1;
}

