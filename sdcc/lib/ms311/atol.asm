;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"atol.c"
	.module atol
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--F:G$atol$0$0({2}DF,SL:S),C,0,0,0,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--F:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0,0,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isblank$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$islower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isupper$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; atol-code 
.globl _atol

;--------------------------------------------------------
	.FUNC _atol:$PNUM 2:$C:__mullong\
:$L:r0x1155:$L:_atol_STK00:$L:r0x1156:$L:r0x1157:$L:r0x1158\
:$L:r0x1159:$L:r0x115A:$L:r0x115B:$L:r0x115C:$L:r0x115E\
:$L:r0x115F:$L:r0x1162:$L:r0x1163:$L:r0x1164
;--------------------------------------------------------
;	.line	34; "atol.c"	long int atol(const char *nptr)
_atol:	;Function start
	STA	r0x1155
;	;.line	36; "atol.c"	long int ret = 0;
	CLRA	
	STA	r0x1156
	STA	r0x1157
	STA	r0x1158
	STA	r0x1159
_00105_DS_:
;	;.line	39; "atol.c"	while (isblank (*nptr))
	LDA	_atol_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115A
;	;.line	53; "/home/chingson/sdccofficial/sdcc/device/include/ctype.h"	return ((unsigned char)c == ' ' || (unsigned char)c == '\t');
	ADD	#0xe0
	JZ	_00119_DS_
	LDA	r0x115A
	XOR	#0x09
	JNZ	_00135_DS_
_00119_DS_:
;	;.line	40; "atol.c"	nptr++;
	LDA	_atol_STK00
	INCA	
	STA	_atol_STK00
	CLRA	
	ADDC	r0x1155
	STA	r0x1155
	JMP	_00105_DS_
_00135_DS_:
	LDA	_atol_STK00
	STA	r0x115A
	LDA	r0x1155
	STA	r0x115B
;	;.line	42; "atol.c"	neg = (*nptr == '-');
	LDA	_atol_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115C
	XOR	#0x2d
	LDC	_Z
	CLRA	
	ROL	
	STA	_atol_STK00
;	;.line	44; "atol.c"	if (*nptr == '-' || *nptr == '+')
	LDA	r0x115C
	ADD	#0xd3
	JZ	_00108_DS_
	LDA	r0x115C
	XOR	#0x2b
	JNZ	_00133_DS_
_00108_DS_:
;	;.line	45; "atol.c"	nptr++;
	LDA	r0x115A
	INCA	
	STA	r0x115A
	CLRA	
	ADDC	r0x115B
	STA	r0x115B
_00133_DS_:
;	;.line	47; "atol.c"	while (isdigit (*nptr))
	LDA	r0x115A
	STA	r0x1155
	LDA	r0x115B
	STA	r0x115A
_00111_DS_:
	LDA	r0x1155
	STA	_ROMPL
	LDA	r0x115A
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115B
;	;.line	62; "/home/chingson/sdccofficial/sdcc/device/include/ctype.h"	return ((unsigned char)c >= '0' && (unsigned char)c <= '9');
	ADD	#0xd0
	JNC	_00113_DS_
	SETB	_C
	LDA	#0x39
	SUBB	r0x115B
	JNC	_00113_DS_
;	;.line	48; "atol.c"	ret = ret * 10L + (*(nptr++) - '0');
	LDA	r0x1156
	STA	__mullong_STK06
	LDA	r0x1157
	STA	__mullong_STK05
	LDA	r0x1158
	STA	__mullong_STK04
	LDA	r0x1159
	STA	__mullong_STK03
	LDA	#0x0a
	STA	__mullong_STK02
	CLRA	
	STA	__mullong_STK01
	STA	__mullong_STK00
	CALL	__mullong
	STA	r0x115E
	LDA	r0x1155
	STA	_ROMPL
	LDA	r0x115A
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115F
	LDA	r0x1155
	INCA	
	STA	r0x1155
	CLRA	
	ADDC	r0x115A
	STA	r0x115A
	LDA	#0xd0
	ADD	r0x115F
	STA	r0x115F
	CLRA	
	ADDC	#0xff
	STA	r0x1162
	JPL	_00177_DS_
	LDA	#0xff
	JMP	_00178_DS_
_00177_DS_:
	CLRA	
_00178_DS_:
	STA	r0x1163
	STA	r0x1164
	LDA	STK02
	ADD	r0x115F
	STA	r0x1156
	LDA	STK01
	ADDC	r0x1162
	STA	r0x1157
	LDA	STK00
	ADDC	r0x1163
	STA	r0x1158
	LDA	r0x115E
	ADDC	r0x1164
	STA	r0x1159
	JMP	_00111_DS_
_00113_DS_:
;	;.line	50; "atol.c"	return (neg ? -ret : ret); // Since -LONG_MIN is LONG_MIN in sdcc, the result value always turns out ok.
	LDA	_atol_STK00
	JZ	_00124_DS_
	SETB	_C
	CLRA	
	SUBB	r0x1156
	STA	_atol_STK00
	CLRA	
	SUBB	r0x1157
	STA	r0x1155
	CLRA	
	SUBB	r0x1158
	STA	r0x115A
	CLRA	
	SUBB	r0x1159
	STA	r0x115B
	JMP	_00125_DS_
_00124_DS_:
	LDA	r0x1156
	STA	_atol_STK00
	LDA	r0x1157
	STA	r0x1155
	LDA	r0x1158
	STA	r0x115A
	LDA	r0x1159
	STA	r0x115B
_00125_DS_:
	LDA	_atol_STK00
	STA	STK02
	LDA	r0x1155
	STA	STK01
	LDA	r0x115A
	STA	STK00
	LDA	r0x115B
;	;.line	51; "atol.c"	}
	RET	
; exit point of _atol
	.ENDFUNC _atol
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:Latol.aligned_alloc$size$65536$15({2}SI:U),E,0,0
	;--cdb--S:Latol.aligned_alloc$alignment$65536$15({2}SI:U),E,0,0
	;--cdb--S:Latol.isblank$c$65536$41({2}SI:S),E,0,0
	;--cdb--S:Latol.isdigit$c$65536$43({2}SI:S),E,0,0
	;--cdb--S:Latol.islower$c$65536$45({2}SI:S),E,0,0
	;--cdb--S:Latol.isupper$c$65536$47({2}SI:S),E,0,0
	;--cdb--S:Latol.atol$nptr$65536$49({2}DG,SC:U),R,0,0,[_atol_STK00,r0x115B]
	;--cdb--S:Latol.atol$__1310720004$131072$50({2}SI:S),R,0,0,[]
	;--cdb--S:Latol.atol$__1310720001$131072$50({2}SI:S),R,0,0,[]
	;--cdb--S:Latol.atol$ret$65536$50({4}SL:S),R,0,0,[r0x1156,r0x1157,r0x1158,r0x1159]
	;--cdb--S:Latol.atol$neg$65536$50({1}:S),R,0,0,[_atol_STK00]
	;--cdb--S:Latol.atol$__1310720002$131072$51({2}SI:S),R,0,0,[r0x115B,r0x115C]
	;--cdb--S:Latol.atol$c$196608$52({2}SI:S),R,0,0,[]
	;--cdb--S:Latol.atol$__1310720005$131072$54({2}SI:S),R,0,0,[r0x115C,r0x115D]
	;--cdb--S:Latol.atol$c$196608$55({2}SI:S),R,0,0,[]
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__mullong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_atol
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_atol_0	udata
r0x1155:	.ds	1
r0x1156:	.ds	1
r0x1157:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_atol_STK00:	.ds	1
	.globl _atol_STK00
	.globl __mullong_STK06
	.globl __mullong_STK05
	.globl __mullong_STK04
	.globl __mullong_STK03
	.globl __mullong_STK02
	.globl __mullong_STK01
	.globl __mullong_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115C:NULL+0:-1:1
	;--cdb--W:r0x115D:NULL+0:-1:1
	;--cdb--W:r0x115A:NULL+0:4443:0
	;--cdb--W:r0x115B:NULL+0:4442:0
	;--cdb--W:r0x115B:NULL+0:4444:0
	;--cdb--W:r0x115B:NULL+0:12:0
	;--cdb--W:r0x115C:NULL+0:4443:0
	;--cdb--W:r0x115C:NULL+0:13:0
	;--cdb--W:r0x115D:NULL+0:14:0
	;--cdb--W:r0x1160:NULL+0:4447:0
	;--cdb--W:r0x1161:NULL+0:0:0
	;--cdb--W:r0x1161:NULL+0:-1:1
	end
