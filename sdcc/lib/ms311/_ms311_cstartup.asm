;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.3.0 #8604 (Sep 15 2014) (MSVC)
; This file was generated Sat Oct 11 21:41:17 2014
;--------------------------------------------------------
; MST port for the MS322 series simple CPU
;--------------------------------------------------------
	.module _ms311_cstartup
	;.list	p=MS326
	;.radix dec
	.include "ms311sfr.def"
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
	.area IDATA	(DATA)
	.area IDATAROM	(CODE)
	.area IDATAX	(XDATA)
	.area XDATAROM	(CODE)
;--------------------------------------------------------
; Code Head 
;--------------------------------------------------------
	.area CSEG	 (CODE) 
	.globl a_STKTOP
;--------------------------------------------------------
; code
;; Starting pCode block
__sdcc_gsinit_startup:	;Function startup
; set all ram zero,
; and copy the init data
	lda	#0x10
	ora	_SYSC ; for z check
	STA	_SYSC ; for z check
	clra
	sta	_RAMP0L
	sta	_RAMP0H
	sta	_PTRCL
	sta	_PTRCH
loop_clr_ram:
	sta	@_RAMP0INC ; 8 bytes are enough
	sta	@_RAMP0INC
	sta	@_RAMP0INC
	sta	@_RAMP0INC
	sta	@_RAMP0INC
	sta	@_RAMP0INC
	sta	@_RAMP0INC
	sta	@_RAMP0INC
	jnz	loop_clr_ram
	lda	#0xef ; initial use 0
	and	_SYSC
	sta	_SYSC

	lda	#l_IDATA
	ora	#>l_IDATA
	jz	copyx

	lda     #s_IDATAROM
	sta	_ROMPL
	lda     #>s_IDATAROM
	sta	_ROMPH

	lda	#s_IDATA
	sta	_RAMP0L
	lda	#>s_IDATA
	sta	_RAMP0H
	lda	#l_IDATA ; less than 256!!
	sta	_PTRCL
	lda	#>l_IDATA ; less than 256!!
	sta	_PTRCH
loop_copy:
	lda	@_ROMPINC
	sta	@_RAMP0INC
	lda	_PTRCL
	deca
	sta	_PTRCL
	jc	loop_copy
	lda	_PTRCH

	jz	copyx
	deca
	sta	_PTRCH
	jmp	loop_copy
copyx:
	lda	#l_IDATAX
	ora	#>l_IDATAX
	jz	tomain
	lda  #s_XDATAROM
	sta	_ROMPL
	lda  #>s_XDATAROM
	sta	_ROMPH
	lda	#s_IDATAX
	sta	_RAMP0L
	lda	#>s_IDATAX
	sta	_RAMP0H
	lda	#l_IDATAX ; 
	sta	_PTRCL
	lda	#>l_IDATAX ; 
	sta	_PTRCH
loop_copyx:
	lda	@_ROMPINC
	sta	@_RAMP0INC
	lda	_PTRCL
	deca
	sta	_PTRCL
	jc	loop_copyx
	lda	_PTRCH

	jz	tomain
	deca
	sta	_PTRCH
	jmp	loop_copyx
tomain:
	lda	_IO
	sta	_init_io_state
	lda	#a_STKTOP
	sta	_RAMP0L
	lda	#>a_STKTOP
	sta	_RAMP0H
	p02p1   ; copy p02p1
	jmp	_main


;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__sdcc_gsinit_startup
	.globl  _main
	.globl	_init_io_state
	.globl  l_XDATAROM
	.globl  s_XDATAROM
	.globl  l_IDATAX
	.globl  s_IDATAX
	.globl  l_IDATAROM
	.globl  s_IDATAROM
	.globl  l_IDATA
	.globl  s_IDATA
 .area UDATA (DATA,REL,CON); pre-def
_init_io_state: .DS 1
;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	;end
