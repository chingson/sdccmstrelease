;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"puts.c"
	.module puts
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$putchar$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$puts$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$puts$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; puts-code 
.globl _puts

;--------------------------------------------------------
	.FUNC _puts:$PNUM 2:$C:_putchar\
:$L:r0x1155:$L:_puts_STK00:$L:r0x1156:$L:r0x1157:$L:r0x1158\

;--------------------------------------------------------
;	.line	31; "puts.c"	int puts (char *s)
_puts:	;Function start
	STA	r0x1155
;	;.line	34; "puts.c"	while (*s){
	CLRA	
	STA	r0x1156
	STA	r0x1157
_00105_DS_:
	LDA	_puts_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1158
	JZ	_00107_DS_
;	;.line	35; "puts.c"	putchar(*s++);
	LDA	_puts_STK00
	INCA	
	STA	_puts_STK00
	CLRA	
	ADDC	r0x1155
	STA	r0x1155
	LDA	r0x1158
	CALL	_putchar
;	;.line	36; "puts.c"	i++;
	LDA	r0x1156
	INCA	
	STA	r0x1156
	CLRA	
	ADDC	r0x1157
	STA	r0x1157
	JMP	_00105_DS_
_00107_DS_:
;	;.line	38; "puts.c"	putchar('\n');
	LDA	#0x0a
	CALL	_putchar
;	;.line	39; "puts.c"	return i+1;
	LDA	r0x1156
	INCA	
	STA	r0x1156
	CLRA	
	ADDC	r0x1157
	STA	r0x1157
	LDA	r0x1156
	STA	STK00
	LDA	r0x1157
;	;.line	40; "puts.c"	}
	RET	
; exit point of _puts
	.ENDFUNC _puts
	;--cdb--S:G$putchar$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$puts$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:Lputs.puts$s$65536$2({2}DG,SC:U),R,0,0,[_puts_STK00]
	;--cdb--S:Lputs.puts$i$65536$3({2}SI:S),R,0,0,[r0x1156,r0x1157]
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_putchar

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_puts
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_puts_0	udata
r0x1155:	.ds	1
r0x1156:	.ds	1
r0x1157:	.ds	1
r0x1158:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_puts_STK00:	.ds	1
	.globl _puts_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	end
