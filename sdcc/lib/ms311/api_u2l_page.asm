;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #bbc754a74 (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"D:\common\ms311sphlib"
;;	.file	"api_u2l_page.c"
	.module api_u2l_page
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Fapi_u2l_page$adps[({0}S:S$predict$0$0({2}SI:S),Z,0,0)({2}S:S$index$0$0({1}SC:U),Z,0,0)]
	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_start_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_stop_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_b12_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
; *** BIT REGION *** (pseudo)
	.area UDATA (DATA,REL,CON)
; *** BIT REGION ***END
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; api_u2l_page-code 
.globl _api_u2l_page

;--------------------------------------------------------
	.FUNC _api_u2l_page:$PNUM 1:$L:r0x1156:$L:r0x1157:$L:r0x1158:$L:r0x1159
;--------------------------------------------------------
;	.line	22; "api_u2l_page.c"	if(highpage)
_api_u2l_page:	;Function start
	JZ	_00106_DS_
;	;.line	24; "api_u2l_page.c"	ROMPUW=0x8180;
	LDA	#0x80
	STA	_ROMPUW
	INCA	
	STA	(_ROMPUW + 1)
	JMP	_00108_DS_
_00106_DS_:
;	;.line	28; "api_u2l_page.c"	ROMPUW=0x8100;
	CLRA	
	STA	_ROMPUW
	LDA	#0x81
	STA	(_ROMPUW + 1)
_00108_DS_:
;	;.line	33; "api_u2l_page.c"	ulawcode=ROMP;
	LDA	@_ROMP
	STA	r0x1156
;	;.line	34; "api_u2l_page.c"	ROMPUW=(ROMPUW<<1)|0x8000;
	LDA	_ROMPUW
	SHL	
	STA	r0x1157
	LDA	(_ROMPUW + 1)
	ROL	
	ORA	#0x80
	STA	r0x1158
	LDA	r0x1157
	STA	_ROMPUW
	LDA	r0x1158
	STA	(_ROMPUW + 1)
;	;.line	36; "api_u2l_page.c"	ROMPINC=u2l_low[ulawcode]; // optimized by LDAT instruction
	LDA	r0x1156
	LDAT	#(_u2l_low + 0)
	STA	@_ROMPINC
;	;.line	37; "api_u2l_page.c"	ROMPINC=u2l_high[ulawcode];
	LDA	r0x1156
	LDAT	#(_u2l_high + 0)
	STA	@_ROMPINC
;	;.line	49; "api_u2l_page.c"	ROMPUW=((ROMPUW>>1)&0x3ff)|0x8000;
	LDA	(_ROMPUW + 1)
	SHR	
	STA	r0x1159
	LDA	_ROMPUW
	ROR	
	STA	r0x1156
	LDA	#0x03
	AND	r0x1159
	ORA	#0x80
	STA	r0x1159
	LDA	r0x1156
	STA	_ROMPUW
	LDA	r0x1159
	STA	(_ROMPUW + 1)
;	;.line	50; "api_u2l_page.c"	} while(ROMPL&0x7f);
	LDA	_ROMPL
	AND	#0x7f
	JNZ	_00108_DS_
;	;.line	51; "api_u2l_page.c"	}
	RET	
; exit point of _api_u2l_page
	.ENDFUNC _api_u2l_page
	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_start_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_stop_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_b12_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$IOR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IODIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IO$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0LH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1LH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAHL$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$init_io_state$0$0({1}SC:U),E,0,0
	;--cdb--S:Lapi_u2l_page.api_u2l_page$highpage$65536$39({1}SC:U),R,0,0,[r0x1156]
	;--cdb--S:Lapi_u2l_page.api_u2l_page$ulawcode$65536$40({1}SC:U),R,0,0,[r0x1156]
	;--cdb--S:G$u2l_high$0$0({256}DA256d,SC:U),D,0,0
	;--cdb--S:G$u2l_low$0$0({256}DA256d,SC:U),D,0,0
	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_IOR
	.globl	_IODIR
	.globl	_IO
	.globl	_IOWK
	.globl	_IOWKDR
	.globl	_TIMERC
	.globl	_THRLD
	.globl	_RAMP0L
	.globl	_RAMP0H
	.globl	_RAMP1L
	.globl	_RAMP1H
	.globl	_PTRCL
	.globl	_PTRCH
	.globl	_ROMPL
	.globl	_ROMPH
	.globl	_BEEPC
	.globl	_FILTERG
	.globl	_ULAWC
	.globl	_STACKL
	.globl	_STACKH
	.globl	_ADCON
	.globl	_DACON
	.globl	_SYSC
	.globl	_SPIM
	.globl	_SPIH
	.globl	_SPIOP
	.globl	_SPI_BANK
	.globl	_ADP_IND
	.globl	_ADP_VPL
	.globl	_ADP_VPH
	.globl	_ADL
	.globl	_ADH
	.globl	_ZC
	.globl	_ADCG
	.globl	_DAC_PL
	.globl	_DAC_PH
	.globl	_PAG
	.globl	_DMAL
	.globl	_DMAH
	.globl	_SPIL
	.globl	_IOMASK
	.globl	_IOCMP
	.globl	_IOCNT
	.globl	_LVDCON
	.globl	_LVRCON
	.globl	_OFFSETL
	.globl	_OFFSETH
	.globl	_RCCON
	.globl	_BGCON
	.globl	_PWRL
	.globl	_CRYPT
	.globl	_PWRH
	.globl	_PWRHL
	.globl	_IROMDL
	.globl	_IROMDH
	.globl	_IROMDLH
	.globl	_RECMUTE
	.globl	_SPKC
	.globl	_DCLAMP
	.globl	_SSPIC
	.globl	_SSPIL
	.globl	_SSPIM
	.globl	_SSPIH
	.globl	_RAMP0
	.globl	_RAMP0LH
	.globl	_RAMP1LH
	.globl	_RAMP0INC
	.globl	_RAMP1
	.globl	_DMAHL
	.globl	_RAMP1INC
	.globl	_RAMP1INC2
	.globl	_ROMP
	.globl	_ROMPINC
	.globl	_ROMPLH
	.globl	_ROMPINC2
	.globl	_ACC
	.globl	_RAMP0UW
	.globl	_RAMP1UW
	.globl	_ROMPUW
	.globl	_SPIMH
	.globl	_OFFSETLH
	.globl	_ADP_VPLH
	.globl	_ICE0
	.globl	_ICE1
	.globl	_ICE2
	.globl	_ICE3
	.globl	_ICE4
	.globl	_TOV
	.globl	_init_io_state
	.globl	_u2l_high
	.globl	_u2l_low

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_api_u2l_page
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_api_u2l_page_0	udata
r0x1156:	.ds	1
r0x1157:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1156:NULL+0:-1:1
	;--cdb--W:r0x1156:NULL+0:4439:0
	;--cdb--W:r0x1157:NULL+0:4441:0
	;--cdb--W:r0x1157:NULL+0:4438:0
	;--cdb--W:r0x1158:NULL+0:4441:0
	;--cdb--W:r0x1159:NULL+0:4439:0
	;--cdb--W:r0x115A:NULL+0:4440:0
	;--cdb--W:r0x1159:NULL+0:-1:1
	;--cdb--W:r0x1158:NULL+0:-1:1
	end
