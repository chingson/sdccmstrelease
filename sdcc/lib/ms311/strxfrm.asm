;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"strxfrm.c"
	.module strxfrm
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--F:G$strxfrm$0$0({2}DF,SI:U),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; strxfrm-code 
.globl _strxfrm

;--------------------------------------------------------
	.FUNC _strxfrm:$PNUM 6:$C:_strncpy:$C:_strlen\
:$L:r0x1155:$L:_strxfrm_STK00:$L:_strxfrm_STK01:$L:_strxfrm_STK02:$L:_strxfrm_STK03\
:$L:_strxfrm_STK04
;--------------------------------------------------------
;	.line	31; "strxfrm.c"	size_t strxfrm(char *dest, char *src, size_t n)
_strxfrm:	;Function start
	STA	r0x1155
;	;.line	33; "strxfrm.c"	strncpy(dest, src, n);
	LDA	_strxfrm_STK04
	STA	_strncpy_STK04
	LDA	_strxfrm_STK03
	STA	_strncpy_STK03
	LDA	_strxfrm_STK02
	STA	_strncpy_STK02
	LDA	_strxfrm_STK01
	STA	_strncpy_STK01
	LDA	_strxfrm_STK00
	STA	_strncpy_STK00
	LDA	r0x1155
	CALL	_strncpy
;	;.line	34; "strxfrm.c"	return(strlen(src) + 1);
	LDA	_strxfrm_STK02
	STA	_strlen_STK00
	LDA	_strxfrm_STK01
	CALL	_strlen
	STA	r0x1155
	LDA	STK00
	INCA	
	STA	_strxfrm_STK00
	CLRA	
	ADDC	r0x1155
	STA	r0x1155
	LDA	_strxfrm_STK00
	STA	STK00
	LDA	r0x1155
;	;.line	35; "strxfrm.c"	}
	RET	
; exit point of _strxfrm
	.ENDFUNC _strxfrm
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:Lstrxfrm.strxfrm$n$65536$21({2}SI:U),R,0,0,[_strxfrm_STK04,_strxfrm_STK03]
	;--cdb--S:Lstrxfrm.strxfrm$src$65536$21({2}DG,SC:U),R,0,0,[_strxfrm_STK02,_strxfrm_STK01]
	;--cdb--S:Lstrxfrm.strxfrm$dest$65536$21({2}DG,SC:U),R,0,0,[_strxfrm_STK00,r0x1155]
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_strncpy
	.globl	_strlen

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_strxfrm
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_strxfrm_0	udata
r0x1155:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_strxfrm_STK00:	.ds	1
	.globl _strxfrm_STK00
_strxfrm_STK01:	.ds	1
	.globl _strxfrm_STK01
_strxfrm_STK02:	.ds	1
	.globl _strxfrm_STK02
_strxfrm_STK03:	.ds	1
	.globl _strxfrm_STK03
_strxfrm_STK04:	.ds	1
	.globl _strxfrm_STK04
	.globl _strncpy_STK04
	.globl _strncpy_STK03
	.globl _strncpy_STK02
	.globl _strncpy_STK01
	.globl _strncpy_STK00
	.globl _strlen_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:_strxfrm_STK00:NULL+0:-1:1
	end
