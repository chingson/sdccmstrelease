;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_divulong.c"
	.module _divulong
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_divulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--F:G$_divulong$0$0({2}DF,SL:U),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _divulong-code 
.globl __divulong

;--------------------------------------------------------
	.FUNC __divulong:$PNUM 8:$L:r0x1157:$L:__divulong_STK00:$L:__divulong_STK01:$L:__divulong_STK02:$L:__divulong_STK03\
:$L:__divulong_STK04:$L:__divulong_STK05:$L:__divulong_STK06:$L:r0x115C:$L:r0x115D\
:$L:r0x115E:$L:r0x115F:$L:r0x1160:$L:r0x1161:$L:r0x1162\
:$L:r0x1163:$L:r0x1164
;--------------------------------------------------------
;	.line	6; "_divulong.c"	_divulong (unsigned long x, unsigned long y)
__divulong:	;Function start
	STA	r0x1157
;	;.line	8; "_divulong.c"	unsigned long reste = 0L;
	CLRA	
	STA	r0x115C
	STA	r0x115D
	STA	r0x115E
	STA	r0x115F
;	;.line	9; "_divulong.c"	unsigned char count = 32;
	LDA	#0x20
	STA	r0x1160
_00110_DS_:
;	;.line	14; "_divulong.c"	if(MSB_SET(x))
	LDA	r0x1157
	JPL	_00106_DS_
;	;.line	16; "_divulong.c"	x <<= 1;
	LDA	__divulong_STK02
	SHL	
	STA	__divulong_STK02
	LDA	__divulong_STK01
	ROL	
	STA	__divulong_STK01
	LDA	__divulong_STK00
	ROL	
	STA	__divulong_STK00
	LDA	r0x1157
	ROL	
	STA	r0x1157
;	;.line	17; "_divulong.c"	reste <<= 1;
	LDA	r0x115C
	SHL	
	STA	r0x1161
	LDA	r0x115D
	ROL	
	STA	r0x1162
	LDA	r0x115E
	ROL	
	STA	r0x1163
	LDA	r0x115F
	ROL	
	STA	r0x1164
;	;.line	18; "_divulong.c"	reste |= 1L;
	LDA	r0x1161
	ORA	#0x01
	STA	r0x115C
	LDA	r0x1162
	STA	r0x115D
	LDA	r0x1163
	STA	r0x115E
	LDA	r0x1164
	STA	r0x115F
	JMP	_00107_DS_
_00106_DS_:
;	;.line	21; "_divulong.c"	x <<= 1;
	LDA	__divulong_STK02
	SHL	
	STA	__divulong_STK02
	LDA	__divulong_STK01
	ROL	
	STA	__divulong_STK01
	LDA	__divulong_STK00
	ROL	
	STA	__divulong_STK00
	LDA	r0x1157
	ROL	
	STA	r0x1157
;	;.line	22; "_divulong.c"	reste <<= 1;
	LDA	r0x115C
	SHL	
	STA	r0x115C
	LDA	r0x115D
	ROL	
	STA	r0x115D
	LDA	r0x115E
	ROL	
	STA	r0x115E
	LDA	r0x115F
	ROL	
	STA	r0x115F
_00107_DS_:
;	;.line	25; "_divulong.c"	if (reste >= y)
	SETB	_C
	LDA	r0x115C
	SUBB	__divulong_STK06
	LDA	r0x115D
	SUBB	__divulong_STK05
	LDA	r0x115E
	SUBB	__divulong_STK04
	LDA	r0x115F
	SUBB	__divulong_STK03
	JNC	_00111_DS_
;	;.line	27; "_divulong.c"	reste -= y;
	SETB	_C
	LDA	r0x115C
	SUBB	__divulong_STK06
	STA	r0x115C
	LDA	r0x115D
	SUBB	__divulong_STK05
	STA	r0x115D
	LDA	r0x115E
	SUBB	__divulong_STK04
	STA	r0x115E
	LDA	r0x115F
	SUBB	__divulong_STK03
	STA	r0x115F
;	;.line	29; "_divulong.c"	x |= 1L;
	LDA	__divulong_STK02
	ORA	#0x01
	STA	__divulong_STK02
_00111_DS_:
;	;.line	32; "_divulong.c"	while (--count);
	LDA	r0x1160
	DECA	
	STA	r0x1161
	STA	r0x1160
	LDA	r0x1161
	JNZ	_00110_DS_
;	;.line	33; "_divulong.c"	return x;
	LDA	__divulong_STK02
	STA	STK02
	LDA	__divulong_STK01
	STA	STK01
	LDA	__divulong_STK00
	STA	STK00
	LDA	r0x1157
;	;.line	34; "_divulong.c"	}
	RET	
; exit point of __divulong
	.ENDFUNC __divulong
	;--cdb--S:G$_divulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$STATUS$0$0({1}SC:U),E,0,0
	;--cdb--S:L_divulong._divulong$y$65536$1({4}SL:U),R,0,0,[__divulong_STK06,__divulong_STK05,__divulong_STK04,__divulong_STK03]
	;--cdb--S:L_divulong._divulong$x$65536$1({4}SL:U),R,0,0,[__divulong_STK02,__divulong_STK01,__divulong_STK00,r0x1157]
	;--cdb--S:L_divulong._divulong$reste$65536$2({4}SL:U),R,0,0,[r0x1161,r0x1162,r0x1163,r0x1164]
	;--cdb--S:L_divulong._divulong$count$65536$2({1}SC:U),R,0,0,[r0x1160]
	;--cdb--S:G$_divulong$0$0({2}DF,SL:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_STATUS

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__divulong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__divulong_0	udata
r0x1157:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__divulong_STK00:	.ds	1
	.globl __divulong_STK00
__divulong_STK01:	.ds	1
	.globl __divulong_STK01
__divulong_STK02:	.ds	1
	.globl __divulong_STK02
__divulong_STK03:	.ds	1
	.globl __divulong_STK03
__divulong_STK04:	.ds	1
	.globl __divulong_STK04
__divulong_STK05:	.ds	1
	.globl __divulong_STK05
__divulong_STK06:	.ds	1
	.globl __divulong_STK06
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	end
