;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_ulong2fs.c"
	.module _ulong2fs
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:F_ulong2fs$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _ulong2fs-code 
.globl ___ulong2fs

;--------------------------------------------------------
	.FUNC ___ulong2fs:$PNUM 4:$L:r0x1157:$L:___ulong2fs_STK00:$L:___ulong2fs_STK01:$L:___ulong2fs_STK02:$L:r0x1158\
:$L:r0x1159:$L:r0x115A:$L:r0x115B:$L:r0x115F:$L:___ulong2fs_fl_65536_21\

;--------------------------------------------------------
;	.line	83; "_ulong2fs.c"	float __ulong2fs (unsigned long a )
___ulong2fs:	;Function start
	STA	r0x1157
;	;.line	88; "_ulong2fs.c"	if (!a)
	LDA	___ulong2fs_STK02
	ORA	___ulong2fs_STK01
	ORA	___ulong2fs_STK00
	ORA	r0x1157
	JNZ	_00119_DS_
;	;.line	90; "_ulong2fs.c"	return 0.0;
	CLRA	
	STA	STK02
	STA	STK01
	STA	STK00
	JMP	_00115_DS_
_00119_DS_:
;	;.line	93; "_ulong2fs.c"	while (a & NORM) 
	LDA	#0x96
	STA	r0x1158
	CLRA	
	STA	r0x1159
_00107_DS_:
;	;.line	96; "_ulong2fs.c"	a >>= 1;
	LDA	r0x1157
	JZ	_00110_DS_
	SHR	
	STA	r0x1157
	LDA	___ulong2fs_STK00
	ROR	
	STA	___ulong2fs_STK00
	LDA	___ulong2fs_STK01
	ROR	
	STA	___ulong2fs_STK01
	LDA	___ulong2fs_STK02
	ROR	
	STA	___ulong2fs_STK02
;	;.line	97; "_ulong2fs.c"	exp++;
	LDA	r0x1158
	INCA	
	STA	r0x1158
	CLRA	
	ADDC	r0x1159
	STA	r0x1159
	JMP	_00107_DS_
_00110_DS_:
;	;.line	100; "_ulong2fs.c"	while (a < HIDDEN)
	LDA	___ulong2fs_STK02
	CLRB	_C
	CLRA	
	ADDC	___ulong2fs_STK01
	LDA	___ulong2fs_STK00
	ADDC	#0x80
	LDA	r0x1157
	ADDC	#0xff
	JC	_00124_DS_
;	;.line	102; "_ulong2fs.c"	a <<= 1;
	LDA	___ulong2fs_STK02
	SHL	
	STA	___ulong2fs_STK02
	LDA	___ulong2fs_STK01
	ROL	
	STA	___ulong2fs_STK01
	LDA	___ulong2fs_STK00
	ROL	
	STA	___ulong2fs_STK00
	LDA	r0x1157
	ROL	
	STA	r0x1157
;	;.line	103; "_ulong2fs.c"	exp--;
	LDA	r0x1158
	DECA	
	STA	r0x1158
	LDA	#0xff
	ADDC	r0x1159
	STA	r0x1159
	JMP	_00110_DS_
_00124_DS_:
	LDA	r0x1158
	STA	r0x115A
	LDA	r0x1159
	STA	r0x115B
;	;.line	107; "_ulong2fs.c"	if ((a&0x7fffff)==0x7fffff) {
	LDA	#0x7f
	AND	___ulong2fs_STK00
	XOR	#0x7f
	JNZ	_00157_DS_
	LDA	___ulong2fs_STK01
	INCA	
	JNC	_00157_DS_
	LDA	___ulong2fs_STK02
	INCA	
_00157_DS_:
	JNZ	_00114_DS_
;	;.line	108; "_ulong2fs.c"	a=0;
	CLRA	
	STA	___ulong2fs_STK02
	STA	___ulong2fs_STK01
	STA	___ulong2fs_STK00
	STA	r0x1157
;	;.line	109; "_ulong2fs.c"	exp++;
	LDA	r0x1158
	INCA	
	STA	r0x115A
	CLRA	
	ADDC	r0x1159
	STA	r0x115B
_00114_DS_:
;	;.line	113; "_ulong2fs.c"	a &= ~HIDDEN ;
	LDA	#0x7f
	AND	___ulong2fs_STK00
	STA	___ulong2fs_STK00
;	;.line	115; "_ulong2fs.c"	fl.l = PACK(0,(unsigned long)exp, a);
	LDA	r0x115B
	ROR	
	LDA	r0x115A
	ROR	
	STA	r0x115F
	CLRA	
	ROR	
	ORA	___ulong2fs_STK00
	STA	___ulong2fs_STK00
	LDA	r0x115F
	ORA	r0x1157
	STA	r0x1157
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
	LDA	___ulong2fs_STK02
	STA	___ulong2fs_fl_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	___ulong2fs_STK01
	STA	(___ulong2fs_fl_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	___ulong2fs_STK00
	STA	(___ulong2fs_fl_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1157
	STA	(___ulong2fs_fl_65536_21 + 3)
;	;.line	117; "_ulong2fs.c"	return (fl.f);
	LDA	___ulong2fs_fl_65536_21
	STA	STK02
	LDA	(___ulong2fs_fl_65536_21 + 1)
	STA	STK01
	LDA	(___ulong2fs_fl_65536_21 + 2)
	STA	STK00
	LDA	(___ulong2fs_fl_65536_21 + 3)
_00115_DS_:
;	;.line	118; "_ulong2fs.c"	}
	RET	
; exit point of ___ulong2fs
	.ENDFUNC ___ulong2fs
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:L_ulong2fs.__ulong2fs$a$65536$20({4}SL:U),R,0,0,[___ulong2fs_STK02,___ulong2fs_STK01,___ulong2fs_STK00,r0x1157]
	;--cdb--S:L_ulong2fs.__ulong2fs$exp$65536$21({2}SI:S),R,0,0,[r0x115A,r0x115B]
	;--cdb--S:L_ulong2fs.__ulong2fs$fl$65536$21({4}STfloat_long:S),E,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___ulong2fs
	.globl	___ulong2fs_fl_65536_21
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
___ulong2fs_fl_65536_21:	.ds	4

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__ulong2fs_0	udata
r0x1157:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115F:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
___ulong2fs_STK00:	.ds	1
	.globl ___ulong2fs_STK00
___ulong2fs_STK01:	.ds	1
	.globl ___ulong2fs_STK01
___ulong2fs_STK02:	.ds	1
	.globl ___ulong2fs_STK02
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115C:NULL+0:-1:1
	;--cdb--W:r0x115D:NULL+0:-1:1
	;--cdb--W:r0x1158:NULL+0:4442:0
	;--cdb--W:r0x1159:NULL+0:4443:0
	;--cdb--W:r0x115A:NULL+0:0:0
	;--cdb--W:r0x115C:NULL+0:4450:0
	;--cdb--W:r0x115D:NULL+0:4449:0
	;--cdb--W:r0x115F:NULL+0:0:0
	;--cdb--W:r0x115F:NULL+0:-1:1
	;--cdb--W:r0x115A:NULL+0:-1:1
	;--cdb--W:r0x115E:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:0:0
	;--cdb--W:r0x115B:NULL+0:-1:1
	end
