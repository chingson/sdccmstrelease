;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_fs2ulong.c"
	.module _fs2ulong
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:F_fs2ulong$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--F:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _fs2ulong-code 
.globl ___fs2ulong

;--------------------------------------------------------
	.FUNC ___fs2ulong:$PNUM 4:$C:__shr_slong\
:$L:r0x1157:$L:___fs2ulong_STK00:$L:___fs2ulong_STK01:$L:___fs2ulong_STK02:$L:___fs2ulong_fl1_65536_21\
:$L:r0x1160:$L:r0x1165:$L:r0x1164:$L:r0x1163
;--------------------------------------------------------
;	.line	103; "_fs2ulong.c"	__fs2ulong (float a1)
___fs2ulong:	;Function start
	STA	r0x1157
	LDA	___fs2ulong_STK02
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	109; "_fs2ulong.c"	fl1.f = a1;
	LDA	___fs2ulong_STK02
	STA	___fs2ulong_fl1_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	___fs2ulong_STK01
	STA	(___fs2ulong_fl1_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	___fs2ulong_STK00
	STA	(___fs2ulong_fl1_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1157
	STA	(___fs2ulong_fl1_65536_21 + 3)
;	;.line	111; "_fs2ulong.c"	if (!fl1.l || SIGN(fl1.l))
	LDA	___fs2ulong_fl1_65536_21
	ORA	(___fs2ulong_fl1_65536_21 + 1)
	ORA	(___fs2ulong_fl1_65536_21 + 2)
	ORA	(___fs2ulong_fl1_65536_21 + 3)
	JZ	_00105_DS_
	LDA	(___fs2ulong_fl1_65536_21 + 3)
	AND	#0x80
	DECA	
	CLRA	
	ROL	
	JZ	_00106_DS_
_00105_DS_:
;	;.line	112; "_fs2ulong.c"	return (0);
	CLRA	
	STA	STK02
	STA	STK01
	STA	STK00
	JMP	_00108_DS_
_00106_DS_:
;	;.line	114; "_fs2ulong.c"	exp = EXP (fl1.l) - EXCESS - 24;
	LDA	(___fs2ulong_fl1_65536_21 + 2)
	ROL	
	LDA	(___fs2ulong_fl1_65536_21 + 3)
	ROL	
	CLRB	_C
	ADD	#0x6a
	STA	___fs2ulong_STK02
;	;.line	115; "_fs2ulong.c"	l = MANT (fl1.l);
	LDA	#0x7f
	AND	(___fs2ulong_fl1_65536_21 + 2)
	ORA	#0x80
	STA	r0x1160
;	;.line	117; "_fs2ulong.c"	l >>= -exp;
	SETB	_C
	CLRA	
	SUBB	___fs2ulong_STK02
	STA	___fs2ulong_STK02
	CLRA	
	STA	STK00
	LDA	r0x1160
	STA	STK01
	LDA	(___fs2ulong_fl1_65536_21 + 1)
	STA	STK02
	LDA	___fs2ulong_fl1_65536_21
	STA	_PTRCL
	LDA	___fs2ulong_STK02
	CALL	__shr_slong
	LDA	STK00
	STA	r0x1165
	LDA	STK01
	STA	r0x1164
	LDA	STK02
	STA	r0x1163
	LDA	_PTRCL
;	;.line	119; "_fs2ulong.c"	return l;
	STA	STK02
	LDA	r0x1163
	STA	STK01
	LDA	r0x1164
	STA	STK00
	LDA	r0x1165
_00108_DS_:
;	;.line	120; "_fs2ulong.c"	}
	RET	
; exit point of ___fs2ulong
	.ENDFUNC ___fs2ulong
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:L_fs2ulong.__fs2ulong$a1$65536$20({4}SF:S),R,0,0,[___fs2ulong_STK02,___fs2ulong_STK01,___fs2ulong_STK00,r0x1157]
	;--cdb--S:L_fs2ulong.__fs2ulong$exp$65536$21({1}SC:S),R,0,0,[]
	;--cdb--S:L_fs2ulong.__fs2ulong$l$65536$21({4}SL:S),R,0,0,[r0x1162,r0x1163,r0x1164,r0x1165]
	;--cdb--S:L_fs2ulong.__fs2ulong$fl1$65536$21({4}STfloat_long:S),E,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__shr_slong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___fs2ulong
	.globl	___fs2ulong_fl1_65536_21
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
___fs2ulong_fl1_65536_21:	.ds	4

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__fs2ulong_0	udata
r0x1157:	.ds	1
r0x1160:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
___fs2ulong_STK00:	.ds	1
	.globl ___fs2ulong_STK00
___fs2ulong_STK01:	.ds	1
	.globl ___fs2ulong_STK01
___fs2ulong_STK02:	.ds	1
	.globl ___fs2ulong_STK02
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1158:NULL+0:-1:1
	;--cdb--W:r0x1159:NULL+0:-1:1
	;--cdb--W:r0x115A:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:-1:1
	;--cdb--W:r0x115C:NULL+0:-1:1
	;--cdb--W:___fs2ulong_STK02:NULL+0:-1:1
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:4456:0
	;--cdb--W:r0x1165:NULL+0:4449:0
	;--cdb--W:r0x1164:NULL+0:4448:0
	;--cdb--W:r0x1161:NULL+0:0:0
	;--cdb--W:r0x1160:NULL+0:-1:1
	;--cdb--W:r0x1162:NULL+0:-1:1
	end
