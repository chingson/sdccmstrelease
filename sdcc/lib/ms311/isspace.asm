;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"isspace.c"
	.module isspace
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isspace$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isblank$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$islower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isupper$0$0({2}DF,SI:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; isspace-code 
.globl _isspace

;--------------------------------------------------------
	.FUNC _isspace:$PNUM 2:$L:r0x1155:$L:_isspace_STK00:$L:r0x1156
;--------------------------------------------------------
;	.line	37; "isspace.c"	int isspace (int c)
_isspace:	;Function start
	STA	r0x1155
;	;.line	39; "isspace.c"	return (c == ' ' || c == '\f' || c == '\n' || c == '\r' || c == '\t' || c == '\v');
	JNZ	_00146_DS_
	LDA	_isspace_STK00
	ADD	#0xe0
_00146_DS_:
	JZ	_00108_DS_
	LDA	r0x1155
	JNZ	_00147_DS_
	LDA	_isspace_STK00
	ADD	#0xf4
_00147_DS_:
	JZ	_00108_DS_
	LDA	r0x1155
	JNZ	_00148_DS_
	LDA	_isspace_STK00
	ADD	#0xf6
_00148_DS_:
	JZ	_00108_DS_
	LDA	r0x1155
	JNZ	_00149_DS_
	LDA	_isspace_STK00
	ADD	#0xf3
_00149_DS_:
	JZ	_00108_DS_
	LDA	r0x1155
	JNZ	_00150_DS_
	LDA	_isspace_STK00
	ADD	#0xf7
_00150_DS_:
	JZ	_00108_DS_
	LDA	r0x1155
	JNZ	_00151_DS_
	LDA	_isspace_STK00
	ADD	#0xf5
_00151_DS_:
	JZ	_00108_DS_
	CLRA	
	STA	_isspace_STK00
	JMP	_00109_DS_
_00108_DS_:
	LDA	#0x01
	STA	_isspace_STK00
_00109_DS_:
	LDA	_isspace_STK00
	JPL	_00152_DS_
	LDA	#0xff
	JMP	_00153_DS_
_00152_DS_:
	CLRA	
_00153_DS_:
	STA	r0x1156
	LDA	_isspace_STK00
	STA	STK00
	LDA	r0x1156
;	;.line	40; "isspace.c"	}
	RET	
; exit point of _isspace
	.ENDFUNC _isspace
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:Lisspace.isblank$c$65536$13({2}SI:S),E,0,0
	;--cdb--S:Lisspace.isdigit$c$65536$15({2}SI:S),E,0,0
	;--cdb--S:Lisspace.islower$c$65536$17({2}SI:S),E,0,0
	;--cdb--S:Lisspace.isupper$c$65536$19({2}SI:S),E,0,0
	;--cdb--S:Lisspace.isspace$c$65536$21({2}SI:S),R,0,0,[_isspace_STK00,r0x1155]
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_isspace
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_isspace_0	udata
r0x1155:	.ds	1
r0x1156:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_isspace_STK00:	.ds	1
	.globl _isspace_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1155:NULL+0:4439:0
	end
