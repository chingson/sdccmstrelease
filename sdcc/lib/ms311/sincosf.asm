;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"sincosf.c"
	.module sincosf
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Fsincosf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$sincosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$sincosf$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; sincosf-code 
.globl _sincosf

;--------------------------------------------------------
	.FUNC _sincosf:$PNUM 5:$C:_fabsf:$C:___fsadd:$C:___fslt:$C:___fsmul\
:$C:___fs2sint:$C:___sint2fs:$C:___fssub\
:$L:r0x1158:$L:_sincosf_STK00:$L:_sincosf_STK01:$L:_sincosf_STK02:$L:_sincosf_STK03\
:$L:r0x115D:$L:r0x115C:$L:r0x115B:$L:r0x115A:$L:r0x115E\
:$L:r0x115F:$L:r0x1161:$L:r0x1160:$L:r0x1165
;--------------------------------------------------------
;	.line	50; "sincosf.c"	float sincosf(float x, bool iscos)
_sincosf:	;Function start
	STA	r0x1158
;	;.line	56; "sincosf.c"	if(iscos)
	LDA	_sincosf_STK03
	JZ	_00109_DS_
;	;.line	58; "sincosf.c"	y=fabsf(x)+HALF_PI;
	LDA	_sincosf_STK02
	STA	_fabsf_STK02
	LDA	_sincosf_STK01
	STA	_fabsf_STK01
	LDA	_sincosf_STK00
	STA	_fabsf_STK00
	LDA	r0x1158
	CALL	_fabsf
	STA	r0x115D
	LDA	#0xdb
	STA	___fsadd_STK06
	LDA	#0x0f
	STA	___fsadd_STK05
	LDA	#0xc9
	STA	___fsadd_STK04
	LDA	#0x3f
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x115D
	CALL	___fsadd
	STA	r0x115D
	LDA	STK00
	STA	r0x115C
	LDA	STK01
	STA	r0x115B
	LDA	STK02
	STA	r0x115A
;	;.line	59; "sincosf.c"	sign=0;
	CLRA	
	STA	r0x115E
	JMP	_00110_DS_
_00109_DS_:
;	;.line	63; "sincosf.c"	if(x<0.0)
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	STA	___fslt_STK04
	STA	___fslt_STK03
	LDA	_sincosf_STK02
	STA	___fslt_STK02
	LDA	_sincosf_STK01
	STA	___fslt_STK01
	LDA	_sincosf_STK00
	STA	___fslt_STK00
	LDA	r0x1158
	CALL	___fslt
	JZ	_00106_DS_
;	;.line	64; "sincosf.c"	{ y=-x; sign=1; }
	LDA	r0x1158
	XOR	#0x80
	STA	r0x115D
	LDA	_sincosf_STK00
	STA	r0x115C
	LDA	_sincosf_STK01
	STA	r0x115B
	LDA	_sincosf_STK02
	STA	r0x115A
	LDA	#0x01
	STA	r0x115E
	JMP	_00110_DS_
_00106_DS_:
;	;.line	66; "sincosf.c"	{ y=x; sign=0; }
	LDA	_sincosf_STK02
	STA	r0x115A
	LDA	_sincosf_STK01
	STA	r0x115B
	LDA	_sincosf_STK00
	STA	r0x115C
	LDA	r0x1158
	STA	r0x115D
	CLRA	
	STA	r0x115E
_00110_DS_:
;	;.line	69; "sincosf.c"	if(y>YMAX)
	LDA	r0x115A
	STA	___fslt_STK06
	LDA	r0x115B
	STA	___fslt_STK05
	LDA	r0x115C
	STA	___fslt_STK04
	LDA	r0x115D
	STA	___fslt_STK03
	CLRA	
	STA	___fslt_STK02
	LDA	#0x0c
	STA	___fslt_STK01
	LDA	#0x49
	STA	___fslt_STK00
	LDA	#0x46
	CALL	___fslt
	JZ	_00112_DS_
;	;.line	71; "sincosf.c"	errno=ERANGE;
	LDA	#0x22
	STA	_errno
;	;.line	72; "sincosf.c"	return 0.0;
	CLRA	
	STA	(_errno + 1)
	STA	STK02
	STA	STK01
	STA	STK00
	CLRA	
	JMP	_00119_DS_
_00112_DS_:
;	;.line	76; "sincosf.c"	N=((y*iPI)+0.5); /*y is positive*/
	LDA	r0x115A
	STA	___fsmul_STK06
	LDA	r0x115B
	STA	___fsmul_STK05
	LDA	r0x115C
	STA	___fsmul_STK04
	LDA	r0x115D
	STA	___fsmul_STK03
	LDA	#0x83
	STA	___fsmul_STK02
	LDA	#0xf9
	STA	___fsmul_STK01
	LDA	#0xa2
	STA	___fsmul_STK00
	LDA	#0x3e
	CALL	___fsmul
	STA	r0x115D
	CLRA	
	STA	___fsadd_STK06
	STA	___fsadd_STK05
	STA	___fsadd_STK04
	LDA	#0x3f
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x115D
	CALL	___fsadd
	STA	r0x115D
	LDA	STK02
	STA	___fs2sint_STK02
	LDA	STK01
	STA	___fs2sint_STK01
	LDA	STK00
	STA	___fs2sint_STK00
	LDA	r0x115D
	CALL	___fs2sint
	STA	r0x115B
;	;.line	79; "sincosf.c"	if(N&1) sign^=1;
	LDA	STK00
	SHR	
	JNC	_00114_DS_
	LDA	#0x01
	XOR	r0x115E
	STA	r0x115E
_00114_DS_:
;	;.line	81; "sincosf.c"	XN=N;
	LDA	STK00
	STA	___sint2fs_STK00
	LDA	r0x115B
	CALL	___sint2fs
	STA	r0x115D
	LDA	STK00
	STA	r0x115C
	LDA	STK01
	STA	r0x115B
	LDA	STK02
	STA	r0x115A
;	;.line	83; "sincosf.c"	if(iscos) XN-=0.5;
	LDA	_sincosf_STK03
	JZ	_00116_DS_
	CLRA	
	STA	___fssub_STK06
	STA	___fssub_STK05
	STA	___fssub_STK04
	LDA	#0x3f
	STA	___fssub_STK03
	LDA	r0x115A
	STA	___fssub_STK02
	LDA	r0x115B
	STA	___fssub_STK01
	LDA	r0x115C
	STA	___fssub_STK00
	LDA	r0x115D
	CALL	___fssub
	STA	r0x115D
	LDA	STK00
	STA	r0x115C
	LDA	STK01
	STA	r0x115B
	LDA	STK02
	STA	r0x115A
_00116_DS_:
;	;.line	85; "sincosf.c"	y=fabsf(x);
	LDA	_sincosf_STK02
	STA	_fabsf_STK02
	LDA	_sincosf_STK01
	STA	_fabsf_STK01
	LDA	_sincosf_STK00
	STA	_fabsf_STK00
	LDA	r0x1158
	CALL	_fabsf
	STA	r0x1158
	LDA	STK00
	STA	_sincosf_STK00
	LDA	STK01
	STA	_sincosf_STK01
	LDA	STK02
	STA	_sincosf_STK02
;	;.line	86; "sincosf.c"	r=(int)y;
	STA	___fs2sint_STK02
	LDA	_sincosf_STK01
	STA	___fs2sint_STK01
	LDA	_sincosf_STK00
	STA	___fs2sint_STK00
	LDA	r0x1158
	CALL	___fs2sint
	STA	r0x115F
	LDA	STK00
	STA	___sint2fs_STK00
	LDA	r0x115F
	CALL	___sint2fs
	STA	r0x1161
	LDA	STK00
	STA	r0x1160
	LDA	STK01
	STA	r0x115F
	LDA	STK02
	STA	_sincosf_STK03
;	;.line	87; "sincosf.c"	g=y-r;
	STA	___fssub_STK06
	LDA	r0x115F
	STA	___fssub_STK05
	LDA	r0x1160
	STA	___fssub_STK04
	LDA	r0x1161
	STA	___fssub_STK03
	LDA	_sincosf_STK02
	STA	___fssub_STK02
	LDA	_sincosf_STK01
	STA	___fssub_STK01
	LDA	_sincosf_STK00
	STA	___fssub_STK00
	LDA	r0x1158
	CALL	___fssub
	STA	r0x1158
	LDA	STK00
	STA	_sincosf_STK00
	LDA	STK01
	STA	_sincosf_STK01
	LDA	STK02
	STA	_sincosf_STK02
;	;.line	88; "sincosf.c"	f=((r-XN*C1)+g)-XN*C2;
	LDA	r0x115A
	STA	___fsmul_STK06
	LDA	r0x115B
	STA	___fsmul_STK05
	LDA	r0x115C
	STA	___fsmul_STK04
	LDA	r0x115D
	STA	___fsmul_STK03
	CLRA	
	STA	___fsmul_STK02
	STA	___fsmul_STK01
	LDA	#0x49
	STA	___fsmul_STK00
	LDA	#0x40
	CALL	___fsmul
	STA	r0x1165
	LDA	STK02
	STA	___fssub_STK06
	LDA	STK01
	STA	___fssub_STK05
	LDA	STK00
	STA	___fssub_STK04
	LDA	r0x1165
	STA	___fssub_STK03
	LDA	_sincosf_STK03
	STA	___fssub_STK02
	LDA	r0x115F
	STA	___fssub_STK01
	LDA	r0x1160
	STA	___fssub_STK00
	LDA	r0x1161
	CALL	___fssub
	STA	r0x1161
	LDA	_sincosf_STK02
	STA	___fsadd_STK06
	LDA	_sincosf_STK01
	STA	___fsadd_STK05
	LDA	_sincosf_STK00
	STA	___fsadd_STK04
	LDA	r0x1158
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1161
	CALL	___fsadd
	STA	r0x1158
	LDA	STK00
	STA	_sincosf_STK00
	LDA	STK01
	STA	_sincosf_STK01
	LDA	STK02
	STA	_sincosf_STK02
	LDA	r0x115A
	STA	___fsmul_STK06
	LDA	r0x115B
	STA	___fsmul_STK05
	LDA	r0x115C
	STA	___fsmul_STK04
	LDA	r0x115D
	STA	___fsmul_STK03
	LDA	#0x22
	STA	___fsmul_STK02
	LDA	#0xaa
	STA	___fsmul_STK01
	LDA	#0x7d
	STA	___fsmul_STK00
	LDA	#0x3a
	CALL	___fsmul
	STA	r0x115C
	LDA	STK02
	STA	___fssub_STK06
	LDA	STK01
	STA	___fssub_STK05
	LDA	STK00
	STA	___fssub_STK04
	LDA	r0x115C
	STA	___fssub_STK03
	LDA	_sincosf_STK02
	STA	___fssub_STK02
	LDA	_sincosf_STK01
	STA	___fssub_STK01
	LDA	_sincosf_STK00
	STA	___fssub_STK00
	LDA	r0x1158
	CALL	___fssub
	STA	r0x1158
	LDA	STK00
	STA	_sincosf_STK00
	LDA	STK01
	STA	_sincosf_STK01
	LDA	STK02
	STA	_sincosf_STK02
;	;.line	90; "sincosf.c"	g=f*f;
	STA	___fsmul_STK06
	LDA	_sincosf_STK01
	STA	___fsmul_STK05
	LDA	_sincosf_STK00
	STA	___fsmul_STK04
	LDA	r0x1158
	STA	___fsmul_STK03
	LDA	_sincosf_STK02
	STA	___fsmul_STK02
	LDA	_sincosf_STK01
	STA	___fsmul_STK01
	LDA	_sincosf_STK00
	STA	___fsmul_STK00
	LDA	r0x1158
	CALL	___fsmul
	STA	r0x115C
	LDA	STK00
	STA	r0x115B
	LDA	STK01
	STA	r0x115A
	LDA	STK02
	STA	_sincosf_STK03
;	;.line	91; "sincosf.c"	if(g>EPS2) //Used to be if(fabsf(f)>EPS)
	STA	___fslt_STK06
	LDA	r0x115A
	STA	___fslt_STK05
	LDA	r0x115B
	STA	___fslt_STK04
	LDA	r0x115C
	STA	___fslt_STK03
	LDA	#0xf3
	STA	___fslt_STK02
	LDA	#0xff
	STA	___fslt_STK01
	LDA	#0x7f
	STA	___fslt_STK00
	LDA	#0x33
	CALL	___fslt
	JZ	_00118_DS_
;	;.line	93; "sincosf.c"	r=(((r4*g+r3)*g+r2)*g+r1)*g;
	LDA	_sincosf_STK03
	STA	___fsmul_STK06
	LDA	r0x115A
	STA	___fsmul_STK05
	LDA	r0x115B
	STA	___fsmul_STK04
	LDA	r0x115C
	STA	___fsmul_STK03
	LDA	#0x5b
	STA	___fsmul_STK02
	LDA	#0x9c
	STA	___fsmul_STK01
	LDA	#0x2e
	STA	___fsmul_STK00
	LDA	#0x36
	CALL	___fsmul
	STA	r0x1161
	LDA	#0x22
	STA	___fsadd_STK06
	LDA	#0xb2
	STA	___fsadd_STK05
	LDA	#0x4f
	STA	___fsadd_STK04
	LDA	#0xb9
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1161
	CALL	___fsadd
	STA	r0x1161
	LDA	_sincosf_STK03
	STA	___fsmul_STK06
	LDA	r0x115A
	STA	___fsmul_STK05
	LDA	r0x115B
	STA	___fsmul_STK04
	LDA	r0x115C
	STA	___fsmul_STK03
	LDA	STK02
	STA	___fsmul_STK02
	LDA	STK01
	STA	___fsmul_STK01
	LDA	STK00
	STA	___fsmul_STK00
	LDA	r0x1161
	CALL	___fsmul
	STA	r0x1161
	LDA	#0x3e
	STA	___fsadd_STK06
	LDA	#0x87
	STA	___fsadd_STK05
	LDA	#0x08
	STA	___fsadd_STK04
	LDA	#0x3c
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1161
	CALL	___fsadd
	STA	r0x1161
	LDA	_sincosf_STK03
	STA	___fsmul_STK06
	LDA	r0x115A
	STA	___fsmul_STK05
	LDA	r0x115B
	STA	___fsmul_STK04
	LDA	r0x115C
	STA	___fsmul_STK03
	LDA	STK02
	STA	___fsmul_STK02
	LDA	STK01
	STA	___fsmul_STK01
	LDA	STK00
	STA	___fsmul_STK00
	LDA	r0x1161
	CALL	___fsmul
	STA	r0x1161
	LDA	#0xa4
	STA	___fsadd_STK06
	LDA	#0xaa
	STA	___fsadd_STK05
	LDA	#0x2a
	STA	___fsadd_STK04
	LDA	#0xbe
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1161
	CALL	___fsadd
	STA	r0x1161
	LDA	_sincosf_STK03
	STA	___fsmul_STK06
	LDA	r0x115A
	STA	___fsmul_STK05
	LDA	r0x115B
	STA	___fsmul_STK04
	LDA	r0x115C
	STA	___fsmul_STK03
	LDA	STK02
	STA	___fsmul_STK02
	LDA	STK01
	STA	___fsmul_STK01
	LDA	STK00
	STA	___fsmul_STK00
	LDA	r0x1161
	CALL	___fsmul
	STA	r0x115C
;	;.line	94; "sincosf.c"	f+=f*r;
	LDA	STK02
	STA	___fsmul_STK06
	LDA	STK01
	STA	___fsmul_STK05
	LDA	STK00
	STA	___fsmul_STK04
	LDA	r0x115C
	STA	___fsmul_STK03
	LDA	_sincosf_STK02
	STA	___fsmul_STK02
	LDA	_sincosf_STK01
	STA	___fsmul_STK01
	LDA	_sincosf_STK00
	STA	___fsmul_STK00
	LDA	r0x1158
	CALL	___fsmul
	STA	r0x115C
	LDA	STK02
	STA	___fsadd_STK06
	LDA	STK01
	STA	___fsadd_STK05
	LDA	STK00
	STA	___fsadd_STK04
	LDA	r0x115C
	STA	___fsadd_STK03
	LDA	_sincosf_STK02
	STA	___fsadd_STK02
	LDA	_sincosf_STK01
	STA	___fsadd_STK01
	LDA	_sincosf_STK00
	STA	___fsadd_STK00
	LDA	r0x1158
	CALL	___fsadd
	STA	r0x1158
	LDA	STK00
	STA	_sincosf_STK00
	LDA	STK01
	STA	_sincosf_STK01
	LDA	STK02
	STA	_sincosf_STK02
_00118_DS_:
;	;.line	96; "sincosf.c"	return (sign?-f:f);
	LDA	r0x115E
	JZ	_00121_DS_
	LDA	r0x1158
	XOR	#0x80
	STA	r0x115C
	LDA	_sincosf_STK00
	STA	r0x115B
	LDA	_sincosf_STK01
	STA	r0x115A
	LDA	_sincosf_STK02
	STA	_sincosf_STK03
	JMP	_00122_DS_
_00121_DS_:
	LDA	_sincosf_STK02
	STA	_sincosf_STK03
	LDA	_sincosf_STK01
	STA	r0x115A
	LDA	_sincosf_STK00
	STA	r0x115B
	LDA	r0x1158
	STA	r0x115C
_00122_DS_:
	LDA	_sincosf_STK03
	STA	STK02
	LDA	r0x115A
	STA	STK01
	LDA	r0x115B
	STA	STK00
	LDA	r0x115C
_00119_DS_:
;	;.line	97; "sincosf.c"	}
	RET	
; exit point of _sincosf
	.ENDFUNC _sincosf
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$sincosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$errno$0$0({2}SI:S),E,0,0
	;--cdb--S:Lsincosf.sincosf$iscos$65536$25({1}:S),R,0,0,[_sincosf_STK03]
	;--cdb--S:Lsincosf.sincosf$x$65536$25({4}SF:S),R,0,0,[_sincosf_STK02,_sincosf_STK01,_sincosf_STK00,r0x1158]
	;--cdb--S:Lsincosf.sincosf$y$65536$26({4}SF:S),R,0,0,[_sincosf_STK02,_sincosf_STK01,_sincosf_STK00,r0x1158]
	;--cdb--S:Lsincosf.sincosf$f$65536$26({4}SF:S),R,0,0,[_sincosf_STK02,_sincosf_STK01,_sincosf_STK00,r0x1158]
	;--cdb--S:Lsincosf.sincosf$r$65536$26({4}SF:S),R,0,0,[_sincosf_STK03,r0x115A,r0x115B,r0x115C]
	;--cdb--S:Lsincosf.sincosf$g$65536$26({4}SF:S),R,0,0,[_sincosf_STK03,r0x115A,r0x115B,r0x115C]
	;--cdb--S:Lsincosf.sincosf$XN$65536$26({4}SF:S),R,0,0,[r0x115A,r0x115B,r0x115C,r0x115D]
	;--cdb--S:Lsincosf.sincosf$N$65536$26({2}SI:S),R,0,0,[r0x115A,r0x115B]
	;--cdb--S:Lsincosf.sincosf$sign$65536$26({1}SC:U),R,0,0,[r0x115E]
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_fabsf
	.globl	___fsadd
	.globl	___fslt
	.globl	___fsmul
	.globl	___fs2sint
	.globl	___sint2fs
	.globl	___fssub
	.globl	_errno

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_sincosf
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_sincosf_0	udata
r0x1158:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1165:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_sincosf_STK00:	.ds	1
	.globl _sincosf_STK00
_sincosf_STK01:	.ds	1
	.globl _sincosf_STK01
_sincosf_STK02:	.ds	1
	.globl _sincosf_STK02
_sincosf_STK03:	.ds	1
	.globl _sincosf_STK03
	.globl _fabsf_STK02
	.globl _fabsf_STK01
	.globl _fabsf_STK00
	.globl ___fsadd_STK06
	.globl ___fsadd_STK05
	.globl ___fsadd_STK04
	.globl ___fsadd_STK03
	.globl ___fsadd_STK02
	.globl ___fsadd_STK01
	.globl ___fsadd_STK00
	.globl ___fslt_STK06
	.globl ___fslt_STK05
	.globl ___fslt_STK04
	.globl ___fslt_STK03
	.globl ___fslt_STK02
	.globl ___fslt_STK01
	.globl ___fslt_STK00
	.globl ___fsmul_STK06
	.globl ___fsmul_STK05
	.globl ___fsmul_STK04
	.globl ___fsmul_STK03
	.globl ___fsmul_STK02
	.globl ___fsmul_STK01
	.globl ___fsmul_STK00
	.globl ___fs2sint_STK02
	.globl ___fs2sint_STK01
	.globl ___fs2sint_STK00
	.globl ___sint2fs_STK00
	.globl ___fssub_STK06
	.globl ___fssub_STK05
	.globl ___fssub_STK04
	.globl ___fssub_STK03
	.globl ___fssub_STK02
	.globl ___fssub_STK01
	.globl ___fssub_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115D:NULL+0:-1:1
	;--cdb--W:_sincosf_STK03:NULL+0:14:0
	;--cdb--W:_sincosf_STK03:NULL+0:12:0
	;--cdb--W:r0x115D:NULL+0:12:0
	;--cdb--W:r0x115C:NULL+0:14:0
	;--cdb--W:r0x115C:NULL+0:4446:0
	;--cdb--W:r0x115B:NULL+0:13:0
	;--cdb--W:r0x115B:NULL+0:14:0
	;--cdb--W:r0x115A:NULL+0:12:0
	;--cdb--W:r0x115A:NULL+0:14:0
	;--cdb--W:r0x115A:NULL+0:13:0
	;--cdb--W:r0x115F:NULL+0:13:0
	;--cdb--W:r0x1160:NULL+0:14:0
	;--cdb--W:r0x1164:NULL+0:14:0
	;--cdb--W:r0x1163:NULL+0:13:0
	;--cdb--W:r0x1162:NULL+0:12:0
	;--cdb--W:r0x115F:NULL+0:-1:1
	;--cdb--W:r0x115C:NULL+0:-1:1
	end
