;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"printf_large.c"
	.module printf_large
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Fprintf_large$__00000000[({0}S:S$byte$0$0({5}DA5d,SC:U),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)({0}S:S$ul$0$0({4}SL:U),Z,0,0)({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$ptr$0$0({2}DG,SC:U),Z,0,0)]
	;--cdb--S:G$_print_format$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$_print_format$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isblank$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isdigit$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$islower$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$isupper$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$printf_small$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$printf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$vprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$sprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$vsprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$puts$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$getchar$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$putchar$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:Fprintf_large$_output_char$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:Fprintf_large$_output_char$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:Fprintf_large$output_digit$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:Fprintf_large$output_digit$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:Fprintf_large$output_2digits$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:Fprintf_large$output_2digits$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:Fprintf_large$calculate_digit$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:Fprintf_large$calculate_digit$0$0({2}DF,SV:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; printf_large-code 
.globl __print_format

;--------------------------------------------------------
	.FUNC __print_format:$PNUM 8:$C:__output_char:$C:__mulchar:$C:_strlen:$C:_output_2digits\
:$C:_calculate_digit:$C:_output_digit\
:$L:__print_format_STK00:$L:__print_format_STK01:$L:__print_format_STK02:$L:__print_format_STK03:$L:__print_format_STK04\
:$L:__print_format_STK05:$L:__print_format_STK06:$L:r0x116A:$L:r0x116B:$L:r0x116C\
:$L:r0x116D:$L:r0x116E:$L:r0x116F:$L:r0x1170:$L:r0x1171\
:$L:r0x1172:$L:r0x1173:$L:r0x1174:$L:r0x1175:$L:r0x1176\
:$L:r0x1177:$L:r0x1178:$L:r0x1179:$L:r0x117A:$L:__print_format_store_262144_82\

;--------------------------------------------------------
;	.line	434; "printf_large.c"	_print_format (pfn_outputchar pfn, void* pvoid, const char *format, va_list ap)
__print_format:	;Function start
	STA	(_output_char + 1)
	LDA	__print_format_STK00
	STA	_output_char
	LDA	__print_format_STK01
	STA	(_p + 1)
	LDA	__print_format_STK02
	STA	_p
;	;.line	466; "printf_large.c"	charsOutputted = 0;
	CLRA	
	STA	_charsOutputted
	STA	(_charsOutputted + 1)
_00277_DS_:
;	;.line	475; "printf_large.c"	while( c=*format++ )
	LDA	__print_format_STK04
	STA	_ROMPL
	LDA	__print_format_STK03
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x116A
	LDA	__print_format_STK04
	INCA	
	STA	__print_format_STK04
	CLRA	
	ADDC	__print_format_STK03
	STA	__print_format_STK03
	LDA	r0x116A
	STA	r0x116B
	LDA	r0x116A
	JZ	_00279_DS_
;	;.line	477; "printf_large.c"	if ( c=='%' )
	LDA	r0x116B
	XOR	#0x25
	JNZ	_00275_DS_
;	;.line	479; "printf_large.c"	left_justify    = 0;
	CLRA	
;	;.line	480; "printf_large.c"	zero_padding    = 0;
	STA	r0x116A
;	;.line	481; "printf_large.c"	prefix_sign     = 0;
	STA	r0x116C
	STA	r0x116D
;	;.line	482; "printf_large.c"	prefix_space    = 0;
	STA	r0x116E
;	;.line	483; "printf_large.c"	signed_argument = 0;
	CLRA	
;	;.line	484; "printf_large.c"	char_argument   = 0;
	STA	r0x116F
;	;.line	485; "printf_large.c"	long_argument   = 0;
	STA	r0x1170
	STA	r0x1171
;	;.line	486; "printf_large.c"	float_argument  = 0;
	STA	r0x1172
;	;.line	487; "printf_large.c"	radix           = 0;
	CLRA	
	STA	r0x1173
;	;.line	488; "printf_large.c"	width           = 0;
	STA	r0x1174
;	;.line	489; "printf_large.c"	decimals        = -1;
	LDA	#0xff
	STA	r0x1175
;	;.line	491; "printf_large.c"	get_conversion_spec:
	LDA	__print_format_STK04
	STA	r0x1176
	LDA	__print_format_STK03
	STA	r0x1177
_00147_DS_:
;	;.line	493; "printf_large.c"	c = *format++;
	LDA	r0x1176
	STA	_ROMPL
	LDA	r0x1177
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1178
	LDA	r0x1176
	INCA	
	STA	r0x1176
	CLRA	
	ADDC	r0x1177
	STA	r0x1177
	LDA	r0x1176
	STA	__print_format_STK04
	LDA	r0x1177
	STA	__print_format_STK03
;	;.line	495; "printf_large.c"	if (c=='%')
	LDA	r0x1178
	XOR	#0x25
	JNZ	_00149_DS_
;	;.line	497; "printf_large.c"	OUTPUT_CHAR(c, p);
	LDA	r0x1178
	CALL	__output_char
;	;.line	498; "printf_large.c"	continue;
	JMP	_00277_DS_
_00149_DS_:
;	;.line	501; "printf_large.c"	if (isdigit(c))
	LDA	r0x1178
	ADD	#0xd0
	JNC	_00156_DS_
	SETB	_C
	LDA	#0x39
	SUBB	r0x1178
	JNC	_00156_DS_
;	;.line	503; "printf_large.c"	if (decimals==-1)
	LDA	r0x1175
	INCA	
	JNC	_00153_DS_
;	;.line	505; "printf_large.c"	width = 10*width + c - '0';
	LDA	#0x0a
	STA	__mulchar_STK00
	LDA	r0x1174
	CALL	__mulchar
	ADD	r0x1178
	ADD	#0xd0
	STA	r0x1174
;	;.line	506; "printf_large.c"	if (width == 0)
	JNZ	_00147_DS_
;	;.line	509; "printf_large.c"	zero_padding = 1;
	LDA	#0x01
	STA	r0x116C
	JMP	_00147_DS_
_00153_DS_:
;	;.line	514; "printf_large.c"	decimals = 10*decimals + c - '0';
	LDA	#0x0a
	STA	__mulchar_STK00
	LDA	r0x1175
	CALL	__mulchar
	ADD	r0x1178
	ADD	#0xd0
	STA	r0x1175
;	;.line	516; "printf_large.c"	goto get_conversion_spec;
	JMP	_00147_DS_
_00156_DS_:
;	;.line	519; "printf_large.c"	if (c=='.')
	LDA	r0x1178
	XOR	#0x2e
	JNZ	_00161_DS_
;	;.line	521; "printf_large.c"	if (decimals==-1)
	LDA	r0x1175
	INCA	
	JNC	_00147_DS_
;	;.line	522; "printf_large.c"	decimals=0;
	CLRA	
	STA	r0x1175
;	;.line	525; "printf_large.c"	goto get_conversion_spec;
	JMP	_00147_DS_
_00161_DS_:
;	;.line	528; "printf_large.c"	if (islower(c))
	LDA	r0x1178
	ADD	#0x9f
	JNC	_00163_DS_
	SETB	_C
	LDA	#0x7a
	SUBB	r0x1178
	JNC	_00163_DS_
;	;.line	530; "printf_large.c"	c = toupper(c);
	LDA	#0xdf
	AND	r0x1178
	STA	r0x1178
;	;.line	531; "printf_large.c"	lower_case = 1;
	LDA	#0x01
	STA	_lower_case
	JMP	_00164_DS_
_00163_DS_:
;	;.line	534; "printf_large.c"	lower_case = 0;
	CLRA	
	STA	_lower_case
_00164_DS_:
;	;.line	536; "printf_large.c"	switch( c )
	LDA	r0x1178
	ADD	#0xe0
	JZ	_00168_DS_
	ADD	#0xf5
	JZ	_00167_DS_
	ADD	#0xfe
	JZ	_00166_DS_
	ADD	#0xeb
	JZ	_00169_DS_
	DECA	
	JZ	_00175_DS_
	DECA	
	JZ	_00200_DS_
	ADD	#0xfe
	JZ	_00204_DS_
	ADD	#0xfe
	JZ	_00147_DS_
	DECA	
	JZ	_00200_DS_
	DECA	
	JZ	_00147_DS_
	ADD	#0xfe
	JZ	_00174_DS_
	ADD	#0xfd
	JZ	_00201_DS_
	DECA	
	JZ	_00198_DS_
	ADD	#0xfd
	JZ	_00179_DS_
	DECA	
	JZ	_00147_DS_
	DECA	
	JZ	_00202_DS_
	ADD	#0xfd
	JZ	_00203_DS_
	ADD	#0xfe
	JZ	_00147_DS_
	JMP	_00205_DS_
_00166_DS_:
;	;.line	539; "printf_large.c"	left_justify = 1;
	LDA	#0x01
	STA	r0x116A
;	;.line	540; "printf_large.c"	goto get_conversion_spec;
	JMP	_00147_DS_
_00167_DS_:
;	;.line	542; "printf_large.c"	prefix_sign = 1;
	LDA	#0x01
	STA	r0x116D
;	;.line	543; "printf_large.c"	goto get_conversion_spec;
	JMP	_00147_DS_
_00168_DS_:
;	;.line	545; "printf_large.c"	prefix_space = 1;
	LDA	#0x01
	STA	r0x116E
;	;.line	546; "printf_large.c"	goto get_conversion_spec;
	JMP	_00147_DS_
_00169_DS_:
;	;.line	548; "printf_large.c"	char_argument = 1;
	LDA	#0x01
	STA	r0x1170
;	;.line	549; "printf_large.c"	goto get_conversion_spec;
	JMP	_00147_DS_
_00174_DS_:
;	;.line	557; "printf_large.c"	long_argument = 1;
	LDA	#0x01
	STA	r0x1171
;	;.line	558; "printf_large.c"	goto get_conversion_spec;
	JMP	_00147_DS_
_00175_DS_:
;	;.line	561; "printf_large.c"	if( char_argument )
	LDA	r0x1170
	JZ	_00177_DS_
;	;.line	562; "printf_large.c"	c = va_arg(ap,char);
	LDA	__print_format_STK06
	DECA	
	STA	r0x1176
	LDA	#0xff
	ADDC	__print_format_STK05
	STA	r0x1177
	LDA	r0x1176
	STA	__print_format_STK06
	LDA	r0x1177
	STA	__print_format_STK05
	LDA	r0x1176
	STA	_ROMPL
	LDA	r0x1177
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1179
	JMP	_00178_DS_
_00177_DS_:
;	;.line	564; "printf_large.c"	c = va_arg(ap,int);
	LDA	#0xfe
	ADD	__print_format_STK06
	STA	r0x1176
	LDA	#0xff
	ADDC	__print_format_STK05
	STA	r0x1177
	LDA	r0x1176
	STA	__print_format_STK06
	LDA	r0x1177
	STA	__print_format_STK05
	LDA	r0x1176
	STA	_ROMPL
	LDA	r0x1177
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1179
_00178_DS_:
;	;.line	565; "printf_large.c"	OUTPUT_CHAR( c, p );
	LDA	r0x1179
	CALL	__output_char
;	;.line	566; "printf_large.c"	break;
	JMP	_00206_DS_
_00179_DS_:
;	;.line	569; "printf_large.c"	PTR = va_arg(ap,ptr_t);
	LDA	#0xfe
	ADD	__print_format_STK06
	STA	r0x1176
	LDA	#0xff
	ADDC	__print_format_STK05
	STA	r0x1177
	LDA	r0x1176
	STA	__print_format_STK06
	LDA	r0x1177
	STA	__print_format_STK05
	LDA	r0x1176
	STA	_ROMPL
	LDA	r0x1177
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1176
	LDA	@_ROMPINC
	STA	r0x1177
;;gen.c:9256: size=1, offset=0, AOP_TYPE(res)=9
	LDA	r0x1176
	STA	_value
;;gen.c:9256: size=0, offset=1, AOP_TYPE(res)=9
	LDA	r0x1177
	STA	(_value + 1)
;	;.line	582; "printf_large.c"	length = strlen(PTR);
	LDA	r0x1176
	STA	_strlen_STK00
	LDA	r0x1177
	CALL	_strlen
	LDA	STK00
	STA	r0x1179
;	;.line	584; "printf_large.c"	if ( decimals == -1 )
	LDA	r0x1175
	INCA	
	JNC	_00181_DS_
;	;.line	586; "printf_large.c"	decimals = length;
	LDA	r0x1179
	STA	r0x1175
_00181_DS_:
;	;.line	588; "printf_large.c"	if ( ( !left_justify ) && (length < width) )
	LDA	r0x116A
	JNZ	_00189_DS_
	SETB	_C
	LDA	r0x1179
	SUBB	r0x1174
	JC	_00189_DS_
;	;.line	590; "printf_large.c"	width -= length;
	SETB	_C
	LDA	r0x1174
	SUBB	r0x1179
	STA	r0x1176
_00182_DS_:
;	;.line	591; "printf_large.c"	while( width-- != 0 )
	LDA	r0x1176
	STA	r0x1177
	LDA	r0x1176
	DECA	
	STA	r0x1176
	LDA	r0x1177
	JZ	_00358_DS_
;	;.line	593; "printf_large.c"	OUTPUT_CHAR( ' ', p );
	LDA	#0x20
	CALL	__output_char
	JMP	_00182_DS_
_00358_DS_:
;	;.line	597; "printf_large.c"	while ( (c = *PTR)  && (decimals-- > 0))
	LDA	r0x1176
	STA	r0x1174
_00189_DS_:
	LDA	_value
	STA	_ROMPL
	LDA	(_value + 1)
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x117A
	STA	r0x1176
	LDA	r0x117A
	JZ	_00191_DS_
	SETB	_C
	CLRA	
	SUBSI	
	SUBB	r0x1175
	JC	_00191_DS_
	LDA	r0x1175
	DECA	
	STA	r0x1175
;	;.line	599; "printf_large.c"	OUTPUT_CHAR( c, p );
	LDA	r0x1176
	CALL	__output_char
;	;.line	600; "printf_large.c"	PTR++;
	LDA	_value
	INCA	
	STA	r0x1176
	CLRA	
	ADDC	(_value + 1)
	STA	r0x1177
;;gen.c:9256: size=1, offset=0, AOP_TYPE(res)=9
	LDA	r0x1176
	STA	_value
;;gen.c:9256: size=0, offset=1, AOP_TYPE(res)=9
	LDA	r0x1177
	STA	(_value + 1)
	JMP	_00189_DS_
_00191_DS_:
;	;.line	603; "printf_large.c"	if ( left_justify && (length < width))
	LDA	r0x116A
	JZ	_00206_DS_
	SETB	_C
	LDA	r0x1179
	SUBB	r0x1174
	JC	_00206_DS_
;	;.line	605; "printf_large.c"	width -= length;
	SETB	_C
	LDA	r0x1174
	SUBB	r0x1179
	STA	r0x1175
_00192_DS_:
;	;.line	606; "printf_large.c"	while( width-- != 0 )
	LDA	r0x1175
	STA	r0x1176
	LDA	r0x1175
	DECA	
	STA	r0x1175
	LDA	r0x1176
	JZ	_00360_DS_
;	;.line	608; "printf_large.c"	OUTPUT_CHAR( ' ', p );
	LDA	#0x20
	CALL	__output_char
	JMP	_00192_DS_
_00198_DS_:
;	;.line	614; "printf_large.c"	PTR = va_arg(ap,ptr_t);
	LDA	#0xfe
	ADD	__print_format_STK06
	STA	r0x1176
	LDA	#0xff
	ADDC	__print_format_STK05
	STA	r0x1177
	LDA	r0x1176
	STA	__print_format_STK06
	LDA	r0x1177
	STA	__print_format_STK05
	LDA	r0x1176
	STA	_ROMPL
	LDA	r0x1177
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1176
	LDA	@_ROMPINC
	STA	r0x1177
;;gen.c:9256: size=1, offset=0, AOP_TYPE(res)=9
	LDA	r0x1176
	STA	_value
;;gen.c:9256: size=0, offset=1, AOP_TYPE(res)=9
	LDA	r0x1177
	STA	(_value + 1)
;	;.line	658; "printf_large.c"	OUTPUT_CHAR('0', p);
	LDA	#0x30
	CALL	__output_char
;	;.line	659; "printf_large.c"	OUTPUT_CHAR('x', p);
	LDA	#0x78
	CALL	__output_char
;	;.line	660; "printf_large.c"	OUTPUT_2DIGITS( value.byte[1] );
	LDA	(_value + 1)
	CALL	_output_2digits
;	;.line	661; "printf_large.c"	OUTPUT_2DIGITS( value.byte[0] );
	LDA	_value
	CALL	_output_2digits
;	;.line	663; "printf_large.c"	break;
	JMP	_00206_DS_
_00200_DS_:
;	;.line	667; "printf_large.c"	signed_argument = 1;
	LDA	#0x01
	STA	r0x116F
;	;.line	668; "printf_large.c"	radix = 10;
	LDA	#0x0a
	STA	r0x1173
;	;.line	669; "printf_large.c"	break;
	JMP	_00206_DS_
_00201_DS_:
;	;.line	672; "printf_large.c"	radix = 8;
	LDA	#0x08
	STA	r0x1173
;	;.line	673; "printf_large.c"	break;
	JMP	_00206_DS_
_00202_DS_:
;	;.line	676; "printf_large.c"	radix = 10;
	LDA	#0x0a
	STA	r0x1173
;	;.line	677; "printf_large.c"	break;
	JMP	_00206_DS_
_00203_DS_:
;	;.line	680; "printf_large.c"	radix = 16;
	LDA	#0x10
	STA	r0x1173
;	;.line	681; "printf_large.c"	break;
	JMP	_00206_DS_
_00204_DS_:
;	;.line	684; "printf_large.c"	float_argument=1;
	LDA	#0x01
	STA	r0x1172
;	;.line	685; "printf_large.c"	break;
	JMP	_00206_DS_
_00205_DS_:
;	;.line	689; "printf_large.c"	OUTPUT_CHAR( c, p );
	LDA	r0x1178
	CALL	__output_char
;	;.line	872; "printf_large.c"	return charsOutputted;
	JMP	_00206_DS_
_00360_DS_:
;	;.line	691; "printf_large.c"	}
	LDA	r0x1175
	STA	r0x1174
_00206_DS_:
;	;.line	693; "printf_large.c"	if (float_argument)
	LDA	r0x1172
	JZ	_00272_DS_
;	;.line	695; "printf_large.c"	value.f = va_arg(ap, float);
	LDA	#0xfc
	ADD	__print_format_STK06
	STA	r0x1172
	LDA	#0xff
	ADDC	__print_format_STK05
	STA	r0x1175
	LDA	r0x1172
	STA	__print_format_STK06
	LDA	r0x1175
	STA	__print_format_STK05
	LDA	r0x1172
	STA	_ROMPL
	LDA	r0x1175
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1172
	LDA	@_ROMPINC
	STA	r0x1175
	LDA	@_ROMPINC
	STA	r0x1178
	LDA	@_ROMPINC
	STA	r0x1179
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
	LDA	r0x1172
	STA	_value
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	r0x1175
	STA	(_value + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	r0x1178
	STA	(_value + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1179
	STA	(_value + 3)
;	;.line	697; "printf_large.c"	PTR="<NO FLOAT>";
	LDA	#(___str_0 + 0)
;;gen.c:9256: size=1, offset=0, AOP_TYPE(res)=9
	LDA	#(___str_0 + 0)
	STA	_value
;;gen.c:9256: size=0, offset=1, AOP_TYPE(res)=9
	LDA	#high (___str_0 + 0)
	STA	(_value + 1)
_00207_DS_:
;	;.line	698; "printf_large.c"	while (c=*PTR++)
	LDA	_value
	STA	r0x1172
	LDA	(_value + 1)
	STA	r0x1175
	LDA	r0x1172
	INCA	
	STA	r0x1176
	CLRA	
	ADDC	r0x1175
	STA	r0x1177
;;gen.c:9256: size=1, offset=0, AOP_TYPE(res)=9
	LDA	r0x1176
	STA	_value
;;gen.c:9256: size=0, offset=1, AOP_TYPE(res)=9
	LDA	r0x1177
	STA	(_value + 1)
	LDA	r0x1172
	STA	_ROMPL
	LDA	r0x1175
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1176
	STA	r0x1172
	LDA	r0x1176
	JZ	_00277_DS_
;	;.line	700; "printf_large.c"	OUTPUT_CHAR (c, p);
	LDA	r0x1172
	CALL	__output_char
	JMP	_00207_DS_
_00272_DS_:
;	;.line	718; "printf_large.c"	else if (radix != 0)
	LDA	r0x1173
	JZ	_00277_DS_
;	;.line	726; "printf_large.c"	if (char_argument)
	LDA	r0x1170
	JZ	_00218_DS_
;	;.line	728; "printf_large.c"	value.l = va_arg(ap, char);
	LDA	__print_format_STK06
	DECA	
	STA	r0x1170
	LDA	#0xff
	ADDC	__print_format_STK05
	STA	r0x1176
	LDA	r0x1170
	STA	__print_format_STK06
	LDA	r0x1176
	STA	__print_format_STK05
	LDA	r0x1170
	STA	_ROMPL
	LDA	r0x1176
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_value
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	CLRA	
	STA	(_value + 1)
	STA	(_value + 2)
	STA	(_value + 3)
;	;.line	729; "printf_large.c"	if (!signed_argument)
	LDA	r0x116F
	JNZ	_00219_DS_
;	;.line	731; "printf_large.c"	value.l &= 0xFF;
	CLRA	
	STA	(_value + 1)
	STA	(_value + 2)
	STA	(_value + 3)
	JMP	_00219_DS_
_00218_DS_:
;	;.line	734; "printf_large.c"	else if (long_argument)
	LDA	r0x1171
	JZ	_00215_DS_
;	;.line	736; "printf_large.c"	value.l = va_arg(ap, long);
	LDA	#0xfc
	ADD	__print_format_STK06
	STA	r0x1170
	LDA	#0xff
	ADDC	__print_format_STK05
	STA	r0x1171
	LDA	r0x1170
	STA	__print_format_STK06
	LDA	r0x1171
	STA	__print_format_STK05
	LDA	r0x1170
	STA	_ROMPL
	LDA	r0x1171
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1170
	LDA	@_ROMPINC
	STA	r0x1171
	LDA	@_ROMPINC
	STA	r0x1178
	LDA	@_ROMPINC
	STA	r0x1179
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
	LDA	r0x1170
	STA	_value
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	r0x1171
	STA	(_value + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	r0x1178
	STA	(_value + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1179
	STA	(_value + 3)
	JMP	_00219_DS_
_00215_DS_:
;	;.line	740; "printf_large.c"	value.l = va_arg(ap, int);
	LDA	#0xfe
	ADD	__print_format_STK06
	STA	r0x1170
	LDA	#0xff
	ADDC	__print_format_STK05
	STA	r0x1171
	LDA	r0x1170
	STA	__print_format_STK06
	LDA	r0x1171
	STA	__print_format_STK05
	LDA	r0x1170
	STA	_ROMPL
	LDA	r0x1171
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1170
	LDA	@_ROMPINC
	STA	r0x1171
	JPL	_00728_DS_
	LDA	#0xff
	JMP	_00729_DS_
_00728_DS_:
	CLRA	
_00729_DS_:
	STA	r0x1178
	STA	r0x1179
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
	LDA	r0x1170
	STA	_value
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	r0x1171
	STA	(_value + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	r0x1178
	STA	(_value + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1179
	STA	(_value + 3)
;	;.line	741; "printf_large.c"	if (!signed_argument)
	LDA	r0x116F
	JNZ	_00219_DS_
;	;.line	743; "printf_large.c"	value.l &= 0xFFFF;
	CLRA	
	STA	(_value + 2)
	STA	(_value + 3)
_00219_DS_:
;	;.line	747; "printf_large.c"	if ( signed_argument )
	LDA	r0x116F
	JZ	_00224_DS_
;	;.line	749; "printf_large.c"	if (value.l < 0)
	LDA	(_value + 3)
	JPL	_00221_DS_
;	;.line	750; "printf_large.c"	value.l = -value.l;
	LDA	(_value + 3)
	SETB	_C
	CLRA	
	SUBB	_value
	STA	r0x1170
	CLRA	
	SUBB	(_value + 1)
	STA	r0x1171
	CLRA	
	SUBB	(_value + 2)
	STA	r0x1176
	CLRA	
	SUBB	(_value + 3)
	STA	r0x1177
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
	LDA	r0x1170
	STA	_value
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	r0x1171
	STA	(_value + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	r0x1176
	STA	(_value + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1177
	STA	(_value + 3)
	JMP	_00224_DS_
_00221_DS_:
;	;.line	752; "printf_large.c"	signed_argument = 0;
	CLRA	
	STA	r0x116F
_00224_DS_:
;	;.line	756; "printf_large.c"	lsd = 1;
	LDA	#0x01
	STA	r0x1170
;	;.line	758; "printf_large.c"	do {
	LDA	#(__print_format_store_262144_82 + 5)
	STA	r0x1171
	LDA	#high (__print_format_store_262144_82 + 5)
	STA	r0x1172
	CLRA	
	STA	r0x1175
;;gen.c:9256: size=0, offset=0, AOP_TYPE(res)=9
_00228_DS_:
;	;.line	759; "printf_large.c"	value.byte[4] = 0;
	CLRA	
	STA	(_value + 4)
;	;.line	763; "printf_large.c"	calculate_digit(radix);
	LDA	r0x1173
	CALL	_calculate_digit
;	;.line	765; "printf_large.c"	if (!lsd)
	LDA	r0x1170
	JNZ	_00226_DS_
;	;.line	767; "printf_large.c"	*pstore = (value.byte[4] << 4) | (value.byte[4] >> 4) | *pstore;
	LDA	(_value + 4)
	SWA	
	AND	#0xf0
	STA	r0x1176
	LDA	(_value + 4)
	SWA	
	AND	#0x0f
	ORA	r0x1176
	STA	r0x1176
	LDA	r0x1171
	STA	_ROMPL
	LDA	r0x1172
	STA	_ROMPH
	LDA	@_ROMPINC
	ORA	r0x1176
	STA	r0x1176
	LDA	r0x1172
	STA	_ROMPH
	LDA	r0x1171
	STA	_ROMPL
	LDA	r0x1176
	STA	@_ROMPINC
;	;.line	768; "printf_large.c"	pstore--;
	LDA	r0x1171
	DECA	
	STA	r0x1171
	LDA	#0xff
	ADDC	r0x1172
	STA	r0x1172
	JMP	_00227_DS_
_00226_DS_:
;	;.line	772; "printf_large.c"	*pstore = value.byte[4];
	LDA	r0x1172
	STA	_ROMPH
	LDA	r0x1171
	STA	_ROMPL
	LDA	(_value + 4)
	STA	@_ROMPINC
_00227_DS_:
;	;.line	774; "printf_large.c"	length++;
	LDA	r0x1175
	INCA	
	STA	r0x1175
;	;.line	775; "printf_large.c"	lsd = !lsd;
	LDA	r0x1170
	LDC	_Z
	CLRA	
	ROL	
	STA	r0x1170
;	;.line	776; "printf_large.c"	} while( value.ul );
	LDA	_value
	ORA	(_value + 1)
	ORA	(_value + 2)
	ORA	(_value + 3)
	JNZ	_00228_DS_
;	;.line	778; "printf_large.c"	if (width == 0)
	LDA	r0x1175
	STA	r0x1173
	LDA	r0x1174
	JNZ	_00232_DS_
;	;.line	783; "printf_large.c"	width = 1;
	LDA	#0x01
	STA	r0x1174
_00232_DS_:
;	;.line	787; "printf_large.c"	if (!zero_padding && !left_justify)
	LDA	r0x116C
	ORA	r0x116A
	JNZ	_00237_DS_
;	;.line	789; "printf_large.c"	while ( width > (unsigned char) (length+1) )
	LDA	r0x1174
	STA	r0x1175
_00233_DS_:
	LDA	r0x1173
	INCA	
	SETB	_C
	SUBB	r0x1175
	JC	_00362_DS_
;	;.line	791; "printf_large.c"	OUTPUT_CHAR( ' ', p );
	LDA	#0x20
	CALL	__output_char
;	;.line	792; "printf_large.c"	width--;
	LDA	r0x1175
	DECA	
	STA	r0x1175
	JMP	_00233_DS_
_00362_DS_:
	LDA	r0x1175
	STA	r0x1174
_00237_DS_:
;	;.line	796; "printf_large.c"	if (signed_argument) // this now means the original value was negative
	LDA	r0x116F
	JZ	_00247_DS_
;	;.line	798; "printf_large.c"	OUTPUT_CHAR( '-', p );
	LDA	#0x2d
	CALL	__output_char
;	;.line	800; "printf_large.c"	width--;
	LDA	r0x1174
	DECA	
	STA	r0x1174
	JMP	_00248_DS_
_00247_DS_:
;	;.line	802; "printf_large.c"	else if (length != 0)
	LDA	r0x1173
	JZ	_00248_DS_
;	;.line	805; "printf_large.c"	if (prefix_sign)
	LDA	r0x116D
	JZ	_00242_DS_
;	;.line	807; "printf_large.c"	OUTPUT_CHAR( '+', p );
	LDA	#0x2b
	CALL	__output_char
;	;.line	809; "printf_large.c"	width--;
	LDA	r0x1174
	DECA	
	STA	r0x1174
	JMP	_00248_DS_
_00242_DS_:
;	;.line	811; "printf_large.c"	else if (prefix_space)
	LDA	r0x116E
	JZ	_00248_DS_
;	;.line	813; "printf_large.c"	OUTPUT_CHAR( ' ', p );
	LDA	#0x20
	CALL	__output_char
;	;.line	815; "printf_large.c"	width--;
	LDA	r0x1174
	DECA	
	STA	r0x1174
_00248_DS_:
;	;.line	820; "printf_large.c"	if (!left_justify)
	LDA	r0x116A
	JNZ	_00256_DS_
;	;.line	822; "printf_large.c"	while ( width-- > length )
	LDA	r0x1174
	STA	r0x116D
_00249_DS_:
	LDA	r0x116D
	STA	r0x116E
	LDA	r0x116D
	DECA	
	STA	r0x116D
	SETB	_C
	LDA	r0x1173
	SUBB	r0x116E
	JC	_00363_DS_
;	;.line	824; "printf_large.c"	OUTPUT_CHAR( zero_padding ? '0' : ' ', p );
	LDA	r0x116C
	JZ	_00282_DS_
	LDA	#0x30
	STA	r0x116E
	CLRA	
	JMP	_00283_DS_
_00282_DS_:
	LDA	#0x20
	STA	r0x116E
_00283_DS_:
	LDA	r0x116E
	CALL	__output_char
	JMP	_00249_DS_
_00256_DS_:
;	;.line	830; "printf_large.c"	if (width > length)
	SETB	_C
	LDA	r0x1173
	SUBB	r0x1174
	JC	_00253_DS_
;	;.line	831; "printf_large.c"	width -= length;
	SETB	_C
	LDA	r0x1174
	SUBB	r0x1173
	STA	r0x116C
	JMP	_00355_DS_
_00253_DS_:
;	;.line	833; "printf_large.c"	width = 0;
	CLRA	
	STA	r0x116C
;	;.line	872; "printf_large.c"	return charsOutputted;
	JMP	_00355_DS_
_00363_DS_:
;	;.line	837; "printf_large.c"	while( length-- )
	LDA	r0x116D
	STA	r0x116C
_00355_DS_:
	LDA	r0x1171
	STA	r0x116D
	LDA	r0x1172
	STA	r0x116E
	LDA	r0x1173
	STA	r0x116F
_00261_DS_:
	LDA	r0x116F
	STA	r0x1171
	LDA	r0x116F
	DECA	
	STA	r0x116F
	LDA	r0x1171
	JZ	_00263_DS_
;	;.line	839; "printf_large.c"	lsd = !lsd;
	LDA	r0x1170
	LDC	_Z
	CLRA	
	ROL	
	STA	r0x1170
;	;.line	840; "printf_large.c"	if (!lsd)
	JNZ	_00259_DS_
;	;.line	842; "printf_large.c"	pstore++;
	LDA	r0x116D
	INCA	
	STA	r0x116D
	CLRA	
	ADDC	r0x116E
	STA	r0x116E
;	;.line	843; "printf_large.c"	value.byte[4] = *pstore >> 4;
	LDA	r0x116D
	STA	_ROMPL
	LDA	r0x116E
	STA	_ROMPH
	LDA	@_ROMPINC
	SWA	
	AND	#0x0f
	STA	r0x1172
;;gen.c:9256: size=0, offset=0, AOP_TYPE(res)=9
	LDA	r0x1172
	STA	(_value + 4)
	JMP	_00260_DS_
_00259_DS_:
;	;.line	847; "printf_large.c"	value.byte[4] = *pstore & 0x0F;
	LDA	r0x116D
	STA	_ROMPL
	LDA	r0x116E
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x0f
	STA	r0x1171
;;gen.c:9256: size=0, offset=0, AOP_TYPE(res)=9
	LDA	r0x1171
	STA	(_value + 4)
_00260_DS_:
;	;.line	853; "printf_large.c"	output_digit( value.byte[4] );
	LDA	(_value + 4)
	CALL	_output_digit
	JMP	_00261_DS_
_00263_DS_:
;	;.line	856; "printf_large.c"	if (left_justify)
	LDA	r0x116A
	JZ	_00277_DS_
;	;.line	858; "printf_large.c"	while (width-- > 0)
	LDA	r0x116C
	STA	r0x116A
_00264_DS_:
	LDA	r0x116A
	STA	r0x116C
	LDA	r0x116A
	DECA	
	STA	r0x116A
	LDA	r0x116C
	JZ	_00277_DS_
;	;.line	860; "printf_large.c"	OUTPUT_CHAR(' ', p);
	LDA	#0x20
	CALL	__output_char
	JMP	_00264_DS_
_00275_DS_:
;	;.line	868; "printf_large.c"	OUTPUT_CHAR( c, p );
	LDA	r0x116B
	CALL	__output_char
	JMP	_00277_DS_
_00279_DS_:
;	;.line	872; "printf_large.c"	return charsOutputted;
	LDA	_charsOutputted
	STA	STK00
	LDA	(_charsOutputted + 1)
;	;.line	873; "printf_large.c"	}
	RET	
; exit point of __print_format
	.ENDFUNC __print_format

;--------------------------------------------------------
	.FUNC _calculate_digit:$PNUM 1:$L:r0x115C:$L:r0x115D:$L:r0x115E:$L:r0x115F:$L:r0x1160\
:$L:r0x1161:$L:r0x1162:$L:r0x1163:$L:r0x1164
;--------------------------------------------------------
;	.line	189; "printf_large.c"	calculate_digit (unsigned char radix)
_calculate_digit:	;Function start
	STA	r0x115C
;	;.line	191; "printf_large.c"	register unsigned long ul = value.ul;
	LDA	_value
	STA	r0x115D
	LDA	(_value + 1)
	STA	r0x115E
	LDA	(_value + 2)
	STA	r0x115F
	LDA	(_value + 3)
	STA	r0x1160
;	;.line	192; "printf_large.c"	register unsigned char b4 = value.byte[4];
	LDA	(_value + 4)
	STA	r0x1161
;	;.line	193; "printf_large.c"	register unsigned char i = 32;
	LDA	#0x20
	STA	r0x1162
_00141_DS_:
;	;.line	197; "printf_large.c"	b4 = (b4 << 1);
	LDA	r0x1161
	SHL	
	STA	r0x1164
;	;.line	198; "printf_large.c"	b4 |= (ul >> 31) & 0x01;
	LDA	r0x1160
	AND	#0x80
	DECA	
	CLRA	
	ROL	
	ORA	r0x1164
	STA	r0x1161
;	;.line	199; "printf_large.c"	ul <<= 1;
	LDA	r0x115D
	SHL	
	STA	r0x115D
	LDA	r0x115E
	ROL	
	STA	r0x115E
	LDA	r0x115F
	ROL	
	STA	r0x115F
	LDA	r0x1160
	ROL	
	STA	r0x1160
;	;.line	201; "printf_large.c"	if (radix <= b4 )
	SETB	_C
	LDA	r0x1161
	SUBB	r0x115C
	JNC	_00142_DS_
;	;.line	203; "printf_large.c"	b4 -= radix;
	SETB	_C
	LDA	r0x1161
	SUBB	r0x115C
	STA	r0x1161
;	;.line	204; "printf_large.c"	ul |= 1;
	LDA	r0x115D
	ORA	#0x01
	STA	r0x115D
_00142_DS_:
;	;.line	206; "printf_large.c"	} while (--i);
	LDA	r0x1162
	DECA	
	STA	r0x1163
	STA	r0x1162
	LDA	r0x1163
	JNZ	_00141_DS_
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	207; "printf_large.c"	value.ul = ul;
	LDA	r0x115D
	STA	_value
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	r0x115E
	STA	(_value + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	r0x115F
	STA	(_value + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1160
	STA	(_value + 3)
;;gen.c:9256: size=0, offset=0, AOP_TYPE(res)=9
;	;.line	208; "printf_large.c"	value.byte[4] = b4;
	LDA	r0x1161
	STA	(_value + 4)
;	;.line	209; "printf_large.c"	}
	RET	
; exit point of _calculate_digit
	.ENDFUNC _calculate_digit

;--------------------------------------------------------
	.FUNC _output_2digits:$PNUM 1:$C:_output_digit\
:$L:r0x115A
;--------------------------------------------------------
;	.line	157; "printf_large.c"	output_2digits (unsigned char b)
_output_2digits:	;Function start
	STA	r0x115A
;	;.line	159; "printf_large.c"	output_digit( b>>4   );
	SWA	
	AND	#0x0f
	CALL	_output_digit
;	;.line	160; "printf_large.c"	output_digit( b&0x0F );
	LDA	#0x0f
	AND	r0x115A
	CALL	_output_digit
;	;.line	161; "printf_large.c"	}
	RET	
; exit point of _output_2digits
	.ENDFUNC _output_2digits

;--------------------------------------------------------
	.FUNC _output_digit:$PNUM 1:$C:__output_char\
:$L:r0x1158
;--------------------------------------------------------
;	.line	132; "printf_large.c"	register unsigned char c = n + (unsigned char)'0';
_output_digit:	;Function start
	ADD	#0x30
	STA	r0x1158
;	;.line	134; "printf_large.c"	if (c > (unsigned char)'9')
	SETB	_C
	LDA	#0x39
	SUBB	r0x1158
	JC	_00117_DS_
;	;.line	136; "printf_large.c"	c += (unsigned char)('A' - '0' - 10);
	LDA	#0x07
	ADD	r0x1158
	STA	r0x1158
;	;.line	137; "printf_large.c"	if (lower_case)
	LDA	_lower_case
	JZ	_00117_DS_
;	;.line	138; "printf_large.c"	c = tolower(c);
	LDA	r0x1158
	ORA	#0x20
	STA	r0x1158
_00117_DS_:
;	;.line	140; "printf_large.c"	_output_char( c );
	LDA	r0x1158
	CALL	__output_char
;	;.line	141; "printf_large.c"	}
	RET	
; exit point of _output_digit
	.ENDFUNC _output_digit

;--------------------------------------------------------
	.FUNC __output_char:$PNUM 1:$L:r0x1155
;--------------------------------------------------------
;	.line	105; "printf_large.c"	_output_char (unsigned char c)
__output_char:	;Function start
	STA	r0x1155
;	;.line	107; "printf_large.c"	output_char( c, p );
	CALL	_00107_DS_
	JMP	_00108_DS_
_00107_DS_:
	CALL	_00109_DS_
_00109_DS_:
	LDA	(_output_char + 1)
	STA	_STACKH
	LDA	_output_char
	STA	_STACKL
	LDA	_p
	STA	@_RAMP0INC
	LDA	(_p + 1)
	STA	@_RAMP0INC
	LDA	r0x1155
	RET	
_00108_DS_:
;	;.line	108; "printf_large.c"	charsOutputted++;
	LDA	_charsOutputted
	INCA	
	STA	_charsOutputted
	CLRA	
	ADDC	(_charsOutputted + 1)
	STA	(_charsOutputted + 1)
;	;.line	109; "printf_large.c"	}
	RET	
; exit point of __output_char
	.ENDFUNC __output_char
	;--cdb--S:G$_print_format$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$isalpha$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$iscntrl$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isgraph$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isprint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$ispunct$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isspace$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isalnum$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isxdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$tolower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$toupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isblank$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isdigit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$islower$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isupper$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$printf_small$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$printf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$vprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$sprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$vsprintf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$puts$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$getchar$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$putchar$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:Fprintf_large$_output_char$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:Fprintf_large$output_digit$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:Fprintf_large$output_2digits$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:Fprintf_large$calculate_digit$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:Lprintf_large.isblank$c$65536$33({2}SI:S),E,0,0
	;--cdb--S:Lprintf_large.isdigit$c$65536$35({2}SI:S),E,0,0
	;--cdb--S:Lprintf_large.islower$c$65536$37({2}SI:S),E,0,0
	;--cdb--S:Lprintf_large.isupper$c$65536$39({2}SI:S),E,0,0
	;--cdb--S:Fprintf_large$lower_case$0$0({1}:S),E,0,0
	;--cdb--S:Fprintf_large$output_char$0$0({2}DC,DF,SV:S),E,0,0
	;--cdb--S:Fprintf_large$p$0$0({2}DG,SV:S),E,0,0
	;--cdb--S:Fprintf_large$value$0$0({5}ST__00000000:S),E,0,0
	;--cdb--S:Fprintf_large$charsOutputted$0$0({2}SI:S),E,0,0
	;--cdb--S:Lprintf_large._output_char$c$65536$51({1}SC:U),R,0,0,[r0x1155]
	;--cdb--S:Lprintf_large.output_digit$n$65536$53({1}SC:U),R,0,0,[r0x1158]
	;--cdb--S:Lprintf_large.output_digit$c$65536$54({1}SC:U),R,0,0,[r0x1158]
	;--cdb--S:Lprintf_large.output_2digits$b$65536$56({1}SC:U),R,0,0,[r0x115A]
	;--cdb--S:Lprintf_large.calculate_digit$radix$65536$58({1}SC:U),R,0,0,[r0x115C]
	;--cdb--S:Lprintf_large.calculate_digit$ul$65536$59({4}SL:U),R,0,0,[r0x115D,r0x115E,r0x115F,r0x1160]
	;--cdb--S:Lprintf_large.calculate_digit$b4$65536$59({1}SC:U),R,0,0,[r0x1164]
	;--cdb--S:Lprintf_large.calculate_digit$i$65536$59({1}SC:U),R,0,0,[r0x1162]
	;--cdb--S:Lprintf_large._print_format$ap$65536$62({2}DD,SC:U),R,0,0,[__print_format_STK06,__print_format_STK05]
	;--cdb--S:Lprintf_large._print_format$format$65536$62({2}DG,SC:U),R,0,0,[__print_format_STK04,__print_format_STK03]
	;--cdb--S:Lprintf_large._print_format$pvoid$65536$62({2}DG,SV:S),R,0,0,[]
	;--cdb--S:Lprintf_large._print_format$pfn$65536$62({2}DC,DF,SV:S),R,0,0,[]
	;--cdb--S:Lprintf_large._print_format$left_justify$65536$63({1}:S),R,0,0,[r0x116A]
	;--cdb--S:Lprintf_large._print_format$zero_padding$65536$63({1}:S),R,0,0,[r0x116C]
	;--cdb--S:Lprintf_large._print_format$prefix_sign$65536$63({1}:S),R,0,0,[r0x116D]
	;--cdb--S:Lprintf_large._print_format$prefix_space$65536$63({1}:S),R,0,0,[r0x116E]
	;--cdb--S:Lprintf_large._print_format$signed_argument$65536$63({1}:S),R,0,0,[r0x116F]
	;--cdb--S:Lprintf_large._print_format$char_argument$65536$63({1}:S),R,0,0,[r0x1170]
	;--cdb--S:Lprintf_large._print_format$long_argument$65536$63({1}:S),R,0,0,[r0x1171]
	;--cdb--S:Lprintf_large._print_format$float_argument$65536$63({1}:S),R,0,0,[r0x1172]
	;--cdb--S:Lprintf_large._print_format$lsd$65536$63({1}:S),R,0,0,[r0x1170]
	;--cdb--S:Lprintf_large._print_format$radix$65536$63({1}SC:U),R,0,0,[r0x1173]
	;--cdb--S:Lprintf_large._print_format$width$65536$63({1}SC:U),R,0,0,[]
	;--cdb--S:Lprintf_large._print_format$decimals$65536$63({1}SC:S),R,0,0,[r0x1175]
	;--cdb--S:Lprintf_large._print_format$length$65536$63({1}SC:U),R,0,0,[r0x1173]
	;--cdb--S:Lprintf_large._print_format$c$65536$63({1}SC:U),R,0,0,[r0x1179]
	;--cdb--S:Lprintf_large._print_format$store$262144$82({6}DA6d,SC:U),E,0,0
	;--cdb--S:Lprintf_large._print_format$pstore$262144$82({2}DG,SC:U),R,0,0,[r0x1171,r0x1172]
	;--cdb--S:Fprintf_large$__str_0$0$0({11}DA11d,SC:U),D,0,0
	;--cdb--S:G$_print_format$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_strlen
	.globl	__mulchar

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__print_format
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
_value:	.ds	5

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_printf_large_0	udata
r0x1155:	.ds	1
r0x1158:	.ds	1
r0x115A:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116C:	.ds	1
r0x116D:	.ds	1
r0x116E:	.ds	1
r0x116F:	.ds	1
r0x1170:	.ds	1
r0x1171:	.ds	1
r0x1172:	.ds	1
r0x1173:	.ds	1
r0x1174:	.ds	1
r0x1175:	.ds	1
r0x1176:	.ds	1
r0x1177:	.ds	1
r0x1178:	.ds	1
r0x1179:	.ds	1
r0x117A:	.ds	1
_charsOutputted:	.ds	2
_output_char:	.ds	2
_p:	.ds	2
_lower_case:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__print_format_STK00:	.ds	1
	.globl __print_format_STK00
__print_format_STK01:	.ds	1
	.globl __print_format_STK01
__print_format_STK02:	.ds	1
	.globl __print_format_STK02
__print_format_STK03:	.ds	1
	.globl __print_format_STK03
__print_format_STK04:	.ds	1
	.globl __print_format_STK04
__print_format_STK05:	.ds	1
	.globl __print_format_STK05
__print_format_STK06:	.ds	1
	.globl __print_format_STK06
	.globl __mulchar_STK00
	.globl _strlen_STK00
__print_format_store_262144_82:	.ds	6
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------

	.area	SEGC	(CODE) ;printf_large-0-code

___str_0:
	.db 0x3c ; '<'
	.db 0x4e ; 'N'
	.db 0x4f ; 'O'
	.db 0x20 ; ' '
	.db 0x46 ; 'F'
	.db 0x4c ; 'L'
	.db 0x4f ; 'O'
	.db 0x41 ; 'A'
	.db 0x54 ; 'T'
	.db 0x3e ; '>'
	.db 0x00 ; '.'
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1179:NULL+0:-1:1
	;--cdb--W:r0x117A:NULL+0:-1:1
	;--cdb--W:r0x1177:NULL+0:-1:1
	;--cdb--W:r0x1176:NULL+0:-1:1
	;--cdb--W:r0x1178:NULL+0:-1:1
	;--cdb--W:r0x1170:NULL+0:-1:1
	;--cdb--W:r0x1171:NULL+0:-1:1
	;--cdb--W:r0x116F:NULL+0:-1:1
	;--cdb--W:r0x1170:NULL+0:4471:0
	;--cdb--W:r0x1175:NULL+0:4462:0
	;--cdb--W:r0x1176:NULL+0:14:0
	;--cdb--W:r0x1176:NULL+0:4466:0
	;--cdb--W:r0x1176:NULL+0:0:0
	;--cdb--W:r0x1176:NULL+0:4464:0
	;--cdb--W:r0x1177:NULL+0:4469:0
	;--cdb--W:r0x1177:NULL+0:4465:0
	;--cdb--W:r0x1177:NULL+0:0:0
	;--cdb--W:r0x1178:NULL+0:0:0
	;--cdb--W:r0x1179:NULL+0:4468:0
	;--cdb--W:r0x1179:NULL+0:4470:0
	;--cdb--W:r0x117A:NULL+0:4470:0
	;--cdb--W:r0x117A:NULL+0:4471:0
	;--cdb--W:r0x117B:NULL+0:4471:0
	;--cdb--W:r0x1175:NULL+0:-1:1
	;--cdb--W:r0x1172:NULL+0:-1:1
	;--cdb--W:r0x1179:NULL+0:0:0
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:-1:1
	;--cdb--W:r0x115A:NULL+0:-1:1
	;--cdb--W:r0x1158:NULL+0:-1:1
	end
