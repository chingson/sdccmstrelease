;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"mbrtowc.c"
	.module mbrtowc
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Fmbrtowc$__00000000[({0}S:S$c$0$0({3}DA3d,SC:U),Z,0,0)]
	;--cdb--T:Fmbrtowc$tm[]
	;--cdb--S:G$mbrtowc$0$0({2}DF,SI:U),C,0,0
	;--cdb--F:G$mbrtowc$0$0({2}DF,SI:U),C,0,0,0,0,0
	;--cdb--S:G$wcscmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wcslen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$btowc$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$wctob$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbsinit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbrlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcrtomb$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; mbrtowc-code 
.globl _mbrtowc

;--------------------------------------------------------
	.FUNC _mbrtowc:$PNUM 8:$C:_mbrtowc:$C:__shr_sshort\
:$L:r0x1156:$L:_mbrtowc_STK00:$L:_mbrtowc_STK01:$L:_mbrtowc_STK02:$L:_mbrtowc_STK03\
:$L:_mbrtowc_STK04:$L:_mbrtowc_STK05:$L:_mbrtowc_STK06:$L:r0x115E:$L:r0x115D\
:$L:r0x115F:$L:r0x1160:$L:_mbrtowc_mbseq_65536_10:$L:r0x1162:$L:r0x1161\
:$L:r0x1163:$L:r0x1164:$L:r0x1165:$L:r0x1166:$L:r0x1167\
:$L:r0x1168:$L:r0x1169
;--------------------------------------------------------
;	.line	32; "mbrtowc.c"	size_t mbrtowc(wchar_t *restrict pwc, const char *restrict s, size_t n, mbstate_t *restrict ps)
_mbrtowc:	;Function start
	STA	r0x1156
;	;.line	41; "mbrtowc.c"	if(!s)
	LDA	_mbrtowc_STK02
	ORA	_mbrtowc_STK01
	JNZ	_00106_DS_
;	;.line	42; "mbrtowc.c"	return(mbrtowc(0, "", 1, ps));
	LDA	_mbrtowc_STK06
	STA	@_RAMP0INC
	LDA	_mbrtowc_STK05
	STA	@_RAMP0INC
	LDA	#0x01
	STA	@_RAMP0INC
	CLRA	
	STA	@_RAMP0INC
	LDA	#(___str_0 + 0)
	STA	@_RAMP0INC
	LDA	#high (___str_0 + 0)
	STA	@_RAMP0INC
	CLRA	
	STA	@_RAMP0INC
	CALL	_mbrtowc
	JMP	_00147_DS_
_00106_DS_:
;	;.line	43; "mbrtowc.c"	if(!n)
	LDA	_mbrtowc_STK04
	ORA	_mbrtowc_STK03
	JZ	_00130_DS_
;	;.line	45; "mbrtowc.c"	if(!ps)
	LDA	_mbrtowc_STK06
	ORA	_mbrtowc_STK05
	JNZ	_00110_DS_
;	;.line	47; "mbrtowc.c"	ps = &sps;
	LDA	#high (_mbrtowc_sps_65536_10 + 0)
	STA	_mbrtowc_STK05
	LDA	#(_mbrtowc_sps_65536_10 + 0)
	STA	_mbrtowc_STK06
_00110_DS_:
;	;.line	50; "mbrtowc.c"	for(i = 0; ps->c[i] && i < 3; i++)
	CLRA	
	STA	r0x115D
_00133_DS_:
	LDA	r0x115D
	ADD	_mbrtowc_STK06
	STA	r0x115E
	CLRA	
	ADDC	_mbrtowc_STK05
	STA	r0x115F
	LDA	r0x115E
	STA	_ROMPL
	LDA	r0x115F
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1160
	JZ	_00175_DS_
	LDA	r0x115D
	ADD	#0xfd
	JC	_00175_DS_
;	;.line	51; "mbrtowc.c"	mbseq[i] = ps->c[i];
	LDA	r0x115D
	ADD	#(_mbrtowc_mbseq_65536_10 + 0)
	STA	r0x115E
	CLRA	
	ADDC	#high (_mbrtowc_mbseq_65536_10 + 0)
	STA	r0x115F
	LDA	r0x115E
	STA	_ROMPL
	LDA	r0x115F
	STA	_ROMPH
	LDA	r0x1160
	STA	@_ROMP
;	;.line	50; "mbrtowc.c"	for(i = 0; ps->c[i] && i < 3; i++)
	LDA	r0x115D
	INCA	
	STA	r0x115D
	JMP	_00133_DS_
_00175_DS_:
;	;.line	53; "mbrtowc.c"	seqlen = 1;
	LDA	#0x01
	STA	r0x115E
;	;.line	54; "mbrtowc.c"	first_byte = ps->c[0] ? ps->c[0] : *s;
	LDA	_mbrtowc_STK06
	STA	_ROMPL
	LDA	_mbrtowc_STK05
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115F
	JNZ	_00150_DS_
	LDA	_mbrtowc_STK02
	STA	_ROMPL
	LDA	_mbrtowc_STK01
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115F
_00150_DS_:
;	;.line	56; "mbrtowc.c"	if(first_byte & 0x80)
	LDA	r0x115F
	JPL	_00116_DS_
;	;.line	58; "mbrtowc.c"	while (first_byte & (0x80 >> seqlen))
	LDA	#0x01
	STA	r0x1160
_00112_DS_:
	CLRA	
	STA	STK00
	LDA	#0x80
	STA	_PTRCL
	LDA	r0x1160
	CALL	__shr_sshort
	LDA	_PTRCL
	AND	r0x115F
	STA	r0x1161
	CLRA	
	AND	STK00
	ORA	r0x1161
	JZ	_00176_DS_
;	;.line	59; "mbrtowc.c"	seqlen++;
	LDA	r0x1160
	INCA	
	STA	r0x1160
	JMP	_00112_DS_
_00176_DS_:
	LDA	r0x1160
	STA	r0x115E
;	;.line	60; "mbrtowc.c"	first_byte &= (0xff >> (seqlen + 1));
	LDA	r0x1160
	INCA	
	STA	r0x1160
	CLRA	
	STA	STK00
	DECA	
	STA	_PTRCL
	LDA	r0x1160
	CALL	__shr_sshort
	LDA	_PTRCL
	AND	r0x115F
	STA	r0x115F
_00116_DS_:
;	;.line	63; "mbrtowc.c"	if(seqlen > 4)
	SETB	_C
	LDA	#0x04
	SUBB	r0x115E
	JNC	_00130_DS_
;	;.line	66; "mbrtowc.c"	if(i + n < seqlen) // Incomplete multibyte character
	LDA	r0x115D
	STA	r0x1160
	CLRA	
	STA	r0x1161
	LDA	r0x1160
	ADD	_mbrtowc_STK04
	STA	r0x1162
	LDA	r0x1161
	ADDC	_mbrtowc_STK03
	STA	r0x1163
	SETB	_C
	LDA	r0x1162
	SUBB	r0x115E
	LDA	r0x1163
	SUBB	#0x00
	JC	_00166_DS_
;	;.line	69; "mbrtowc.c"	ps->c[i] = *s++;
	LDA	_mbrtowc_STK02
	STA	r0x1162
	LDA	_mbrtowc_STK01
	STA	r0x1163
	LDA	r0x115D
	STA	r0x1164
_00136_DS_:
;	;.line	68; "mbrtowc.c"	for(;n-- ; i++)
	LDA	_mbrtowc_STK04
	STA	r0x1165
	LDA	_mbrtowc_STK03
	STA	r0x1166
	LDA	_mbrtowc_STK04
	DECA	
	STA	_mbrtowc_STK04
	LDA	#0xff
	ADDC	_mbrtowc_STK03
	STA	_mbrtowc_STK03
	LDA	r0x1165
	ORA	r0x1166
	JZ	_00119_DS_
;	;.line	69; "mbrtowc.c"	ps->c[i] = *s++;
	LDA	r0x1164
	ADD	_mbrtowc_STK06
	STA	r0x1165
	CLRA	
	ADDC	_mbrtowc_STK05
	STA	r0x1166
	LDA	r0x1162
	STA	_ROMPL
	LDA	r0x1163
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1167
	LDA	r0x1162
	INCA	
	STA	r0x1162
	CLRA	
	ADDC	r0x1163
	STA	r0x1163
	LDA	r0x1166
	STA	_ROMPH
	LDA	r0x1165
	STA	_ROMPL
	LDA	r0x1167
	STA	@_ROMPINC
;	;.line	68; "mbrtowc.c"	for(;n-- ; i++)
	LDA	r0x1164
	INCA	
	STA	r0x1164
	JMP	_00136_DS_
_00119_DS_:
;	;.line	70; "mbrtowc.c"	return(-2);
	LDA	#0xfe
	STA	STK00
	INCA	
	JMP	_00147_DS_
_00166_DS_:
;	;.line	73; "mbrtowc.c"	for(j = 0; j < i; j++)
	CLRA	
	STA	_mbrtowc_STK04
_00139_DS_:
	SETB	_C
	LDA	_mbrtowc_STK04
	SUBB	r0x115D
	JC	_00122_DS_
;	;.line	74; "mbrtowc.c"	ps->c[j] = 0;
	LDA	_mbrtowc_STK04
	ADD	_mbrtowc_STK06
	STA	_mbrtowc_STK03
	CLRA	
	ADDC	_mbrtowc_STK05
	STA	_ROMPH
	LDA	_mbrtowc_STK03
	STA	_ROMPL
	CLRA	
	STA	@_ROMPINC
;	;.line	73; "mbrtowc.c"	for(j = 0; j < i; j++)
	LDA	_mbrtowc_STK04
	INCA	
	STA	_mbrtowc_STK04
	JMP	_00139_DS_
_00122_DS_:
;	;.line	76; "mbrtowc.c"	for(n = 1, i = i ? i : 1; i < seqlen; i++, n++)
	LDA	#0x01
	STA	_mbrtowc_STK04
	CLRA	
	STA	_mbrtowc_STK03
	LDA	r0x115D
	JZ	_00151_DS_
	LDA	r0x1160
	STA	_mbrtowc_STK06
	LDA	r0x1161
	JMP	_00152_DS_
_00151_DS_:
	LDA	#0x01
	STA	_mbrtowc_STK06
_00152_DS_:
	LDA	_mbrtowc_STK06
	STA	r0x115D
	LDA	#0x01
	STA	_mbrtowc_STK06
	CLRA	
	STA	_mbrtowc_STK05
_00142_DS_:
	SETB	_C
	LDA	r0x115D
	SUBB	r0x115E
	JC	_00125_DS_
;	;.line	78; "mbrtowc.c"	mbseq[i] = *s++;
	LDA	r0x115D
	ADD	#(_mbrtowc_mbseq_65536_10 + 0)
	STA	r0x1160
	CLRA	
	ADDC	#high (_mbrtowc_mbseq_65536_10 + 0)
	STA	r0x1161
	LDA	_mbrtowc_STK02
	STA	_ROMPL
	LDA	_mbrtowc_STK01
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1162
	LDA	_mbrtowc_STK02
	INCA	
	STA	_mbrtowc_STK02
	CLRA	
	ADDC	_mbrtowc_STK01
	STA	_mbrtowc_STK01
	LDA	r0x1160
	STA	_ROMPL
	LDA	r0x1161
	STA	_ROMPH
	LDA	r0x1162
	STA	@_ROMP
;	;.line	79; "mbrtowc.c"	if((mbseq[i] & 0xc0) != 0x80)
	LDA	#0xc0
	AND	r0x1162
	XOR	#0x80
	JNZ	_00130_DS_
;	;.line	76; "mbrtowc.c"	for(n = 1, i = i ? i : 1; i < seqlen; i++, n++)
	LDA	r0x115D
	INCA	
	STA	r0x115D
	LDA	_mbrtowc_STK06
	INCA	
	STA	_mbrtowc_STK06
	CLRA	
	ADDC	_mbrtowc_STK05
	STA	_mbrtowc_STK05
	LDA	_mbrtowc_STK06
	STA	_mbrtowc_STK04
	LDA	_mbrtowc_STK05
	STA	_mbrtowc_STK03
	JMP	_00142_DS_
_00125_DS_:
;	;.line	83; "mbrtowc.c"	codepoint = first_byte;
	LDA	r0x115F
	STA	_mbrtowc_STK02
	CLRA	
	STA	_mbrtowc_STK01
	STA	_mbrtowc_STK06
	STA	_mbrtowc_STK05
;	;.line	85; "mbrtowc.c"	for(s = mbseq + 1, seqlen--; seqlen; seqlen--)
	LDA	#high (_mbrtowc_mbseq_65536_10 + 1)
	STA	r0x115F
	LDA	#(_mbrtowc_mbseq_65536_10 + 1)
	STA	r0x115D
	LDA	r0x115E
	DECA	
	STA	r0x115E
_00145_DS_:
	LDA	r0x115E
	JZ	_00126_DS_
;	;.line	87; "mbrtowc.c"	codepoint <<= 6;
	LDA	_mbrtowc_STK05
	SWA	
	AND	#0xf0
	STA	r0x1163
	LDA	_mbrtowc_STK06
	SWA	
	STA	_PTRCL
	AND	#0x0f
	ORA	r0x1163
	STA	r0x1163
	LDA	_PTRCL
	AND	#0xf0
	STA	r0x1162
	LDA	_mbrtowc_STK01
	SWA	
	STA	_PTRCL
	AND	#0x0f
	ORA	r0x1162
	STA	r0x1162
	LDA	_PTRCL
	AND	#0xf0
	STA	r0x1161
	LDA	_mbrtowc_STK02
	SWA	
	STA	_PTRCL
	AND	#0x0f
	ORA	r0x1161
	STA	r0x1161
	LDA	_PTRCL
	AND	#0xf0
	SHL	
	STA	r0x1160
	LDA	r0x1161
	ROL	
	STA	r0x1161
	LDA	r0x1162
	ROL	
	STA	r0x1162
	LDA	r0x1163
	ROL	
	STA	r0x1163
	LDA	r0x1160
	SHL	
	STA	r0x1160
	LDA	r0x1161
	ROL	
	STA	r0x1161
	LDA	r0x1162
	ROL	
	STA	r0x1162
	LDA	r0x1163
	ROL	
	STA	r0x1163
;	;.line	88; "mbrtowc.c"	codepoint |= (*s & 0x3f);
	LDA	r0x115D
	STA	_ROMPL
	LDA	r0x115F
	STA	_ROMPH
	LDA	@_ROMPINC
	AND	#0x3f
	STA	r0x1164
	CLRA	
	JPL	_00322_DS_
	LDA	#0xff
	JMP	_00323_DS_
_00322_DS_:
	CLRA	
_00323_DS_:
	STA	r0x1168
	STA	r0x1169
	LDA	r0x1164
	ORA	r0x1160
	STA	_mbrtowc_STK02
	LDA	r0x1161
	STA	_mbrtowc_STK01
	LDA	r0x1168
	ORA	r0x1162
	STA	_mbrtowc_STK06
	LDA	r0x1169
	ORA	r0x1163
	STA	_mbrtowc_STK05
;	;.line	89; "mbrtowc.c"	s++;
	LDA	r0x115D
	INCA	
	STA	r0x115D
	CLRA	
	ADDC	r0x115F
	STA	r0x115F
;	;.line	85; "mbrtowc.c"	for(s = mbseq + 1, seqlen--; seqlen; seqlen--)
	LDA	r0x115E
	DECA	
	STA	r0x115E
	JMP	_00145_DS_
_00126_DS_:
;	;.line	92; "mbrtowc.c"	if(codepoint >= 0xd800 && codepoint <= 0xdfff) // UTF-16 surrogate.
	CLRB	_C
	LDA	_mbrtowc_STK01
	ADDC	#0x28
	LDA	_mbrtowc_STK06
	ADDC	#0xff
	LDA	_mbrtowc_STK05
	ADDC	#0xff
	JNC	_00128_DS_
	SETB	_C
	LDA	#0xff
	SUBB	_mbrtowc_STK02
	LDA	#0xdf
	SUBB	_mbrtowc_STK01
	CLRA	
	SUBB	_mbrtowc_STK06
	CLRA	
	SUBB	_mbrtowc_STK05
	JNC	_00128_DS_
;	;.line	93; "mbrtowc.c"	return(-1);
	LDA	#0xff
	STA	STK00
	JMP	_00147_DS_
_00128_DS_:
;	;.line	95; "mbrtowc.c"	*pwc = codepoint;
	LDA	r0x1156
	STA	_ROMPH
	LDA	_mbrtowc_STK00
	STA	_ROMPL
	LDA	_mbrtowc_STK02
	STA	@_ROMPINC
	LDA	_mbrtowc_STK01
	STA	@_ROMPINC
	LDA	_mbrtowc_STK06
	STA	@_ROMPINC
	LDA	_mbrtowc_STK05
	STA	@_ROMPINC
;	;.line	96; "mbrtowc.c"	return(n);
	LDA	_mbrtowc_STK04
	STA	STK00
	LDA	_mbrtowc_STK03
	JMP	_00147_DS_
_00130_DS_:
;	;.line	99; "mbrtowc.c"	errno = EILSEQ;
	LDA	#0x54
	STA	_errno
	CLRA	
	STA	(_errno + 1)
;	;.line	100; "mbrtowc.c"	return(-1);
	DECA	
	STA	STK00
	LDA	#0xff
_00147_DS_:
;	;.line	101; "mbrtowc.c"	}
	RET	
; exit point of _mbrtowc
	.ENDFUNC _mbrtowc
	;--cdb--S:G$mbrtowc$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcscmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wcslen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$btowc$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$wctob$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbsinit$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbrlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcrtomb$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$errno$0$0({2}SI:S),E,0,0
	;--cdb--S:Lmbrtowc.mbrtowc$ps$65536$9({2}DG,ST__00000000:S),R,0,0,[_mbrtowc_STK06,_mbrtowc_STK05]
	;--cdb--S:Lmbrtowc.mbrtowc$n$65536$9({2}SI:U),R,0,0,[_mbrtowc_STK04,_mbrtowc_STK03]
	;--cdb--S:Lmbrtowc.mbrtowc$s$65536$9({2}DG,SC:U),R,0,0,[_mbrtowc_STK02,_mbrtowc_STK01]
	;--cdb--S:Lmbrtowc.mbrtowc$pwc$65536$9({2}DG,SL:U),R,0,0,[_mbrtowc_STK00,r0x1156]
	;--cdb--S:Lmbrtowc.mbrtowc$first_byte$65536$10({1}SC:U),R,0,0,[r0x115F]
	;--cdb--S:Lmbrtowc.mbrtowc$seqlen$65536$10({1}SC:U),R,0,0,[]
	;--cdb--S:Lmbrtowc.mbrtowc$mbseq$65536$10({4}DA4d,SC:U),E,0,0
	;--cdb--S:Lmbrtowc.mbrtowc$codepoint$65536$10({4}SL:U),R,0,0,[r0x1160,r0x1161,r0x1162,r0x1163]
	;--cdb--S:Lmbrtowc.mbrtowc$i$65536$10({1}SC:U),R,0,0,[r0x115D]
	;--cdb--S:Lmbrtowc.mbrtowc$j$65536$10({1}SC:U),R,0,0,[_mbrtowc_STK04]
	;--cdb--S:Lmbrtowc.mbrtowc$sps$65536$10({3}ST__00000000:S),E,0,0
	;--cdb--S:Fmbrtowc$__str_0$0$0({1}DA1d,SC:U),D,0,0
	;--cdb--S:G$mbrtowc$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_errno
	.globl	__shr_sshort

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_mbrtowc
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
_mbrtowc_sps_65536_10:	.ds	3

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_mbrtowc_0	udata
r0x1156:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_mbrtowc_STK00:	.ds	1
	.globl _mbrtowc_STK00
_mbrtowc_STK01:	.ds	1
	.globl _mbrtowc_STK01
_mbrtowc_STK02:	.ds	1
	.globl _mbrtowc_STK02
_mbrtowc_STK03:	.ds	1
	.globl _mbrtowc_STK03
_mbrtowc_STK04:	.ds	1
	.globl _mbrtowc_STK04
_mbrtowc_STK05:	.ds	1
	.globl _mbrtowc_STK05
_mbrtowc_STK06:	.ds	1
	.globl _mbrtowc_STK06
_mbrtowc_mbseq_65536_10:	.ds	4
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------

	.area	SEGC	(CODE) ;mbrtowc-0-code

___str_0:
	.db 0x00 ; '.'
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1162:NULL+0:-1:1
	;--cdb--W:_mbrtowc_STK05:NULL+0:-1:1
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x1166:NULL+0:-1:1
	;--cdb--W:r0x115D:NULL+0:14:0
	;--cdb--W:r0x1160:NULL+0:4449:0
	;--cdb--W:r0x1160:NULL+0:4450:0
	;--cdb--W:r0x1162:NULL+0:0:0
	;--cdb--W:r0x1162:NULL+0:14:0
	;--cdb--W:r0x1162:NULL+0:4448:0
	;--cdb--W:r0x1161:NULL+0:128:0
	;--cdb--W:r0x1161:NULL+0:255:0
	;--cdb--W:r0x1163:NULL+0:4447:0
	;--cdb--W:r0x1163:NULL+0:4449:0
	;--cdb--W:r0x1163:NULL+0:0:0
	;--cdb--W:r0x1164:NULL+0:0:0
	;--cdb--W:r0x1164:NULL+0:4446:0
	;--cdb--W:r0x1165:NULL+0:0:0
	;--cdb--W:r0x1165:NULL+0:4452:0
	;--cdb--W:r0x1166:NULL+0:4455:0
	;--cdb--W:r0x1167:NULL+0:0:0
	;--cdb--W:r0x115E:NULL+0:-1:1
	;--cdb--W:r0x115D:NULL+0:-1:1
	;--cdb--W:r0x1164:NULL+0:-1:1
	;--cdb--W:r0x1165:NULL+0:-1:1
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x1167:NULL+0:-1:1
	;--cdb--W:r0x1160:NULL+0:4445:0
	;--cdb--W:r0x1161:NULL+0:0:0
	end
