;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_mullong.c"
	.module _mullong
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--F:G$_mullong$0$0({2}DF,SL:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _mullong-code 
.globl __mullong

;--------------------------------------------------------
	.FUNC __mullong:$PNUM 8:$L:r0x1157:$L:__mullong_STK00:$L:__mullong_STK01:$L:__mullong_STK02:$L:__mullong_STK03\
:$L:__mullong_STK04:$L:__mullong_STK05:$L:__mullong_STK06:$L:r0x115C:$L:r0x115D\
:$L:r0x115E:$L:r0x115F:$L:r0x1160:$L:r0x1161
;--------------------------------------------------------
;	.line	33; "_mullong.c"	_mullong (long a, long b)
__mullong:	;Function start
	STA	r0x1157
;	;.line	35; "_mullong.c"	long result = 0;
	CLRA	
	STA	r0x115C
	STA	r0x115D
	STA	r0x115E
	STA	r0x115F
;	;.line	39; "_mullong.c"	for (i = 0; i < 8u; i++) {
	LDA	#0x08
	STA	r0x1160
_00119_DS_:
;	;.line	41; "_mullong.c"	if (a & 0x0001u) result += b;
	LDA	__mullong_STK02
	SHR	
	JNC	_00109_DS_
	LDA	r0x115C
	ADD	__mullong_STK06
	STA	r0x115C
	LDA	r0x115D
	ADDC	__mullong_STK05
	STA	r0x115D
	LDA	r0x115E
	ADDC	__mullong_STK04
	STA	r0x115E
	LDA	r0x115F
	ADDC	__mullong_STK03
	STA	r0x115F
_00109_DS_:
;	;.line	42; "_mullong.c"	if (sizeof (a) > 1 && (a & 0x00000100ul)) result += (b << 8u);
	LDA	__mullong_STK01
	SHR	
	JNC	_00112_DS_
	LDA	r0x115D
	ADD	__mullong_STK06
	STA	r0x115D
	LDA	r0x115E
	ADDC	__mullong_STK05
	STA	r0x115E
	LDA	r0x115F
	ADDC	__mullong_STK04
	STA	r0x115F
_00112_DS_:
;	;.line	43; "_mullong.c"	if (sizeof (a) > 2 && (a & 0x00010000ul)) result += (b << 16u);
	LDA	__mullong_STK00
	SHR	
	JNC	_00115_DS_
	LDA	r0x115E
	ADD	__mullong_STK06
	STA	r0x115E
	LDA	r0x115F
	ADDC	__mullong_STK05
	STA	r0x115F
_00115_DS_:
;	;.line	44; "_mullong.c"	if (sizeof (a) > 3 && (a & 0x01000000ul)) result += (b << 24u);
	LDA	r0x1157
	SHR	
	JNC	_00114_DS_
	LDA	r0x115F
	ADD	__mullong_STK06
	STA	r0x115F
_00114_DS_:
;	;.line	45; "_mullong.c"	a = ((unsigned long)a) >> 1u;
	LDA	r0x1157
	SHR	
	STA	r0x1157
	LDA	__mullong_STK00
	ROR	
	STA	__mullong_STK00
	LDA	__mullong_STK01
	ROR	
	STA	__mullong_STK01
	LDA	__mullong_STK02
	ROR	
	STA	__mullong_STK02
;	;.line	46; "_mullong.c"	b <<= 1u;
	LDA	__mullong_STK06
	SHL	
	STA	__mullong_STK06
	LDA	__mullong_STK05
	ROL	
	STA	__mullong_STK05
	LDA	__mullong_STK04
	ROL	
	STA	__mullong_STK04
	LDA	__mullong_STK03
	ROL	
	STA	__mullong_STK03
	LDA	r0x1160
	DECA	
	STA	r0x1161
	STA	r0x1160
;	;.line	39; "_mullong.c"	for (i = 0; i < 8u; i++) {
	LDA	r0x1161
	JNZ	_00119_DS_
;	;.line	49; "_mullong.c"	return result;
	LDA	r0x115C
	STA	STK02
	LDA	r0x115D
	STA	STK01
	LDA	r0x115E
	STA	STK00
	LDA	r0x115F
;	;.line	50; "_mullong.c"	}
	RET	
; exit point of __mullong
	.ENDFUNC __mullong
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:L_mullong._mullong$b$65536$1({4}SL:S),R,0,0,[__mullong_STK06,__mullong_STK05,__mullong_STK04,__mullong_STK03]
	;--cdb--S:L_mullong._mullong$a$65536$1({4}SL:S),R,0,0,[__mullong_STK02,__mullong_STK01,__mullong_STK00,r0x1157]
	;--cdb--S:L_mullong._mullong$result$65536$2({4}SL:S),R,0,0,[r0x115C,r0x115D,r0x115E,r0x115F]
	;--cdb--S:L_mullong._mullong$i$65536$2({1}SC:U),R,0,0,[r0x1160]
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__mullong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__mullong_0	udata
r0x1157:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__mullong_STK00:	.ds	1
	.globl __mullong_STK00
__mullong_STK01:	.ds	1
	.globl __mullong_STK01
__mullong_STK02:	.ds	1
	.globl __mullong_STK02
__mullong_STK03:	.ds	1
	.globl __mullong_STK03
__mullong_STK04:	.ds	1
	.globl __mullong_STK04
__mullong_STK05:	.ds	1
	.globl __mullong_STK05
__mullong_STK06:	.ds	1
	.globl __mullong_STK06
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x1164:NULL+0:4457:0
	;--cdb--W:r0x1164:NULL+0:4458:0
	;--cdb--W:r0x1164:NULL+0:4459:0
	;--cdb--W:r0x1164:NULL+0:4439:0
	;--cdb--W:r0x1163:NULL+0:4458:0
	;--cdb--W:r0x1163:NULL+0:4459:0
	;--cdb--W:r0x1163:NULL+0:0:0
	;--cdb--W:r0x1163:NULL+0:4453:0
	;--cdb--W:r0x1162:NULL+0:4459:0
	;--cdb--W:r0x1162:NULL+0:0:0
	;--cdb--W:r0x1162:NULL+0:4454:0
	;--cdb--W:r0x1161:NULL+0:4455:0
	;--cdb--W:r0x1162:NULL+0:-1:1
	;--cdb--W:r0x1163:NULL+0:-1:1
	end
