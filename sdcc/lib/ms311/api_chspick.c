#include <ms326.h>

void api_chspick(unsigned char clk) __critical
{
    __asm
    call	here
    jmp	tail
    here:
    sta	_SPICK
    ret
    tail:
    __endasm;

}
