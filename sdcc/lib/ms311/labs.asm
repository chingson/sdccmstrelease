;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"labs.c"
	.module labs
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--F:G$labs$0$0({2}DF,SL:S),C,0,0,0,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--F:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0,0,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; labs-code 
.globl _labs

;--------------------------------------------------------
	.FUNC _labs:$PNUM 4:$L:r0x1157:$L:_labs_STK00:$L:_labs_STK01:$L:_labs_STK02:$L:r0x1158\
:$L:r0x1159:$L:r0x115A:$L:r0x115B
;--------------------------------------------------------
;	.line	63; "labs.c"	long int labs(long int j)
_labs:	;Function start
	STA	r0x1157
;	;.line	65; "labs.c"	return (j < 0) ? -j : j;
	JPL	_00107_DS_
	SETB	_C
	CLRA	
	SUBB	_labs_STK02
	STA	r0x1158
	CLRA	
	SUBB	_labs_STK01
	STA	r0x1159
	CLRA	
	SUBB	_labs_STK00
	STA	r0x115A
	CLRA	
	SUBB	r0x1157
	STA	r0x115B
	JMP	_00108_DS_
_00107_DS_:
	LDA	_labs_STK02
	STA	r0x1158
	LDA	_labs_STK01
	STA	r0x1159
	LDA	_labs_STK00
	STA	r0x115A
	LDA	r0x1157
	STA	r0x115B
_00108_DS_:
	LDA	r0x1158
	STA	STK02
	LDA	r0x1159
	STA	STK01
	LDA	r0x115A
	STA	STK00
	LDA	r0x115B
;	;.line	66; "labs.c"	}
	RET	
; exit point of _labs
	.ENDFUNC _labs
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:Llabs.aligned_alloc$size$65536$15({2}SI:U),E,0,0
	;--cdb--S:Llabs.aligned_alloc$alignment$65536$15({2}SI:U),E,0,0
	;--cdb--S:Llabs.labs$j$65536$29({4}SL:S),R,0,0,[_labs_STK02,_labs_STK01,_labs_STK00,r0x1157]
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_labs
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_labs_0	udata
r0x1157:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_labs_STK00:	.ds	1
	.globl _labs_STK00
_labs_STK01:	.ds	1
	.globl _labs_STK01
_labs_STK02:	.ds	1
	.globl _labs_STK02
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	end
