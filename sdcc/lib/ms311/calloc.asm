;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"calloc.c"
	.module calloc
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--F:G$calloc$0$0({2}DF,DX,SV:S),C,0,0,0,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--F:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0,0,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; calloc-code 
.globl _calloc

;--------------------------------------------------------
	.FUNC _calloc:$PNUM 4:$C:__mullong:$C:_malloc:$C:_memset\
:$L:r0x1155:$L:_calloc_STK00:$L:_calloc_STK01:$L:_calloc_STK02:$L:r0x1158\
:$L:r0x1159
;--------------------------------------------------------
;	.line	35; "calloc.c"	void XDATA *calloc (size_t nmemb, size_t size)
_calloc:	;Function start
	STA	r0x1155
;	;.line	39; "calloc.c"	unsigned long msize = (unsigned long)nmemb * (unsigned long)size;
	LDA	_calloc_STK02
	STA	__mullong_STK06
	LDA	_calloc_STK01
	STA	__mullong_STK05
	CLRA	
	STA	__mullong_STK04
	STA	__mullong_STK03
	LDA	_calloc_STK00
	STA	__mullong_STK02
	LDA	r0x1155
	STA	__mullong_STK01
	CLRA	
	STA	__mullong_STK00
	CALL	__mullong
	STA	_calloc_STK01
;	;.line	44; "calloc.c"	if (msize > SIZE_MAX)
	SETB	_C
	LDA	#0xff
	SUBB	STK02
	LDA	#0xff
	SUBB	STK01
	CLRA	
	SUBB	STK00
	CLRA	
	SUBB	_calloc_STK01
	JC	_00106_DS_
;	;.line	45; "calloc.c"	return(0);
	CLRA	
	STA	STK00
	JMP	_00109_DS_
_00106_DS_:
;	;.line	47; "calloc.c"	if (ptr = malloc(msize))
	LDA	STK02
	STA	r0x1158
	LDA	STK01
	STA	r0x1159
	LDA	r0x1158
	STA	_malloc_STK00
	LDA	r0x1159
	CALL	_malloc
	STA	r0x1155
	LDA	STK00
	STA	_calloc_STK02
	LDA	r0x1155
	STA	_calloc_STK01
	LDA	STK00
	ORA	r0x1155
	JZ	_00108_DS_
;	;.line	48; "calloc.c"	memset(ptr, 0, msize);
	LDA	r0x1158
	STA	_memset_STK03
	LDA	r0x1159
	STA	_memset_STK02
	CLRA	
	STA	_memset_STK01
	LDA	_calloc_STK02
	STA	_memset_STK00
	LDA	_calloc_STK01
	CALL	_memset
_00108_DS_:
;	;.line	50; "calloc.c"	return(ptr);
	LDA	_calloc_STK02
	STA	STK00
	LDA	_calloc_STK01
_00109_DS_:
;	;.line	51; "calloc.c"	}
	RET	
; exit point of _calloc
	.ENDFUNC _calloc
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:Lcalloc.aligned_alloc$size$65536$15({2}SI:U),E,0,0
	;--cdb--S:Lcalloc.aligned_alloc$alignment$65536$15({2}SI:U),E,0,0
	;--cdb--S:Lcalloc.calloc$size$65536$49({2}SI:U),R,0,0,[_calloc_STK02,_calloc_STK01]
	;--cdb--S:Lcalloc.calloc$nmemb$65536$49({2}SI:U),R,0,0,[_calloc_STK00,r0x1155]
	;--cdb--S:Lcalloc.calloc$ptr$65536$50({2}DX,SV:S),R,0,0,[_calloc_STK02,_calloc_STK01]
	;--cdb--S:Lcalloc.calloc$msize$65536$50({4}SL:U),R,0,0,[_calloc_STK00,r0x1155,_calloc_STK02,_calloc_STK01]
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_malloc
	.globl	_memset
	.globl	__mullong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_calloc
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_calloc_0	udata
r0x1155:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_calloc_STK00:	.ds	1
	.globl _calloc_STK00
_calloc_STK01:	.ds	1
	.globl _calloc_STK01
_calloc_STK02:	.ds	1
	.globl _calloc_STK02
	.globl __mullong_STK06
	.globl __mullong_STK05
	.globl __mullong_STK04
	.globl __mullong_STK03
	.globl __mullong_STK02
	.globl __mullong_STK01
	.globl __mullong_STK00
	.globl _malloc_STK00
	.globl _memset_STK03
	.globl _memset_STK02
	.globl _memset_STK01
	.globl _memset_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1155:NULL+0:4447:0
	;--cdb--W:r0x1155:NULL+0:13:0
	;--cdb--W:_calloc_STK00:NULL+0:4448:0
	;--cdb--W:_calloc_STK00:NULL+0:12:0
	;--cdb--W:_calloc_STK00:NULL+0:14:0
	;--cdb--W:_calloc_STK02:NULL+0:14:0
	;--cdb--W:r0x1158:NULL+0:4446:0
	;--cdb--W:r0x1159:NULL+0:4437:0
	;--cdb--W:r0x115C:NULL+0:0:0
	;--cdb--W:r0x115C:NULL+0:-1:1
	;--cdb--W:r0x115A:NULL+0:0:0
	;--cdb--W:r0x115D:NULL+0:0:0
	;--cdb--W:r0x115A:NULL+0:-1:1
	;--cdb--W:r0x115D:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:0:0
	;--cdb--W:r0x115B:NULL+0:-1:1
	end
