;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_memmove.c"
	.module _memmove
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--F:G$memmove$0$0({2}DF,DG,SV:S),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _memmove-code 
.globl _memmove

;--------------------------------------------------------
	.FUNC _memmove:$PNUM 6:$L:r0x1155:$L:_memmove_STK00:$L:_memmove_STK01:$L:_memmove_STK02:$L:_memmove_STK03\
:$L:_memmove_STK04:$L:r0x115A:$L:r0x115B:$L:r0x115C:$L:r0x115D\
:$L:r0x115E:$L:r0x115F:$L:r0x1160:$L:r0x1161:$L:r0x1162\
:$L:r0x1163
;--------------------------------------------------------
;	.line	41; "_memmove.c"	void * memmove (void * dst, void * src, size_t acount)
_memmove:	;Function start
	STA	r0x1155
;	;.line	43; "_memmove.c"	void * ret = dst;
	LDA	_memmove_STK00
	STA	r0x115A
	LDA	r0x1155
	STA	r0x115B
;	;.line	47; "_memmove.c"	if ((uintptr_t)src < (uintptr_t)dst) {
	SETB	_C
	LDA	_memmove_STK02
	SUBB	_memmove_STK00
	LDA	_memmove_STK01
	SUBB	r0x1155
	JC	_00112_DS_
;	;.line	51; "_memmove.c"	d = ((char *)dst)+acount-1;
	LDA	_memmove_STK00
	ADD	_memmove_STK04
	STA	r0x115C
	LDA	r0x1155
	ADDC	_memmove_STK03
	STA	r0x115D
	LDA	r0x115C
	DECA	
	STA	r0x115C
	LDA	#0xff
	ADDC	r0x115D
	STA	r0x115D
;	;.line	52; "_memmove.c"	s = ((char *)src)+acount-1;
	LDA	_memmove_STK02
	ADD	_memmove_STK04
	STA	r0x115E
	LDA	_memmove_STK01
	ADDC	_memmove_STK03
	STA	r0x115F
	LDA	r0x115E
	DECA	
	STA	r0x115E
	LDA	#0xff
	ADDC	r0x115F
	STA	r0x115F
;	;.line	53; "_memmove.c"	while (acount--) {
	LDA	_memmove_STK04
	STA	r0x1160
	LDA	_memmove_STK03
	STA	r0x1161
_00105_DS_:
	LDA	r0x1160
	STA	r0x1162
	LDA	r0x1161
	STA	r0x1163
	LDA	r0x1160
	DECA	
	STA	r0x1160
	LDA	#0xff
	ADDC	r0x1161
	STA	r0x1161
	LDA	r0x1162
	ORA	r0x1163
	JZ	_00113_DS_
;	;.line	54; "_memmove.c"	*d-- = *s--;
	LDA	r0x115E
	STA	_ROMPL
	LDA	r0x115F
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1162
	LDA	r0x115E
	DECA	
	STA	r0x115E
	LDA	#0xff
	ADDC	r0x115F
	STA	r0x115F
	LDA	r0x115D
	STA	_ROMPH
	LDA	r0x115C
	STA	_ROMPL
	LDA	r0x1162
	STA	@_ROMPINC
	LDA	r0x115C
	DECA	
	STA	r0x115C
	LDA	#0xff
	ADDC	r0x115D
	STA	r0x115D
	JMP	_00105_DS_
_00112_DS_:
;	;.line	61; "_memmove.c"	d = dst;
	LDA	r0x1155
	STA	r0x115D
	LDA	_memmove_STK00
	STA	r0x115C
;	;.line	62; "_memmove.c"	s = src;
	LDA	_memmove_STK01
	STA	r0x1155
	LDA	_memmove_STK02
	STA	_memmove_STK00
;	;.line	63; "_memmove.c"	while (acount--) {
	LDA	_memmove_STK04
	STA	_memmove_STK02
	LDA	_memmove_STK03
	STA	_memmove_STK01
_00108_DS_:
	LDA	_memmove_STK02
	STA	_memmove_STK04
	LDA	_memmove_STK01
	STA	_memmove_STK03
	LDA	_memmove_STK02
	DECA	
	STA	_memmove_STK02
	LDA	#0xff
	ADDC	_memmove_STK01
	STA	_memmove_STK01
	LDA	_memmove_STK04
	ORA	_memmove_STK03
	JZ	_00113_DS_
;	;.line	64; "_memmove.c"	*d++ = *s++;
	LDA	_memmove_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_memmove_STK04
	LDA	_memmove_STK00
	INCA	
	STA	_memmove_STK00
	CLRA	
	ADDC	r0x1155
	STA	r0x1155
	LDA	r0x115D
	STA	_ROMPH
	LDA	r0x115C
	STA	_ROMPL
	LDA	_memmove_STK04
	STA	@_ROMPINC
	LDA	r0x115C
	INCA	
	STA	r0x115C
	CLRA	
	ADDC	r0x115D
	STA	r0x115D
	JMP	_00108_DS_
_00113_DS_:
;	;.line	68; "_memmove.c"	return(ret);
	LDA	r0x115A
	STA	STK00
	LDA	r0x115B
;	;.line	69; "_memmove.c"	}
	RET	
; exit point of _memmove
	.ENDFUNC _memmove
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_memmove.memmove$acount$65536$21({2}SI:U),R,0,0,[_memmove_STK04,_memmove_STK03]
	;--cdb--S:L_memmove.memmove$src$65536$21({2}DG,SV:S),R,0,0,[_memmove_STK02,_memmove_STK01]
	;--cdb--S:L_memmove.memmove$dst$65536$21({2}DG,SV:S),R,0,0,[_memmove_STK00,r0x1155]
	;--cdb--S:L_memmove.memmove$ret$65536$22({2}DG,SV:S),R,0,0,[r0x115A,r0x115B]
	;--cdb--S:L_memmove.memmove$d$65536$22({2}DG,SC:U),R,0,0,[]
	;--cdb--S:L_memmove.memmove$s$65536$22({2}DG,SC:U),R,0,0,[]
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_memmove
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__memmove_0	udata
r0x1155:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_memmove_STK00:	.ds	1
	.globl _memmove_STK00
_memmove_STK01:	.ds	1
	.globl _memmove_STK01
_memmove_STK02:	.ds	1
	.globl _memmove_STK02
_memmove_STK03:	.ds	1
	.globl _memmove_STK03
_memmove_STK04:	.ds	1
	.globl _memmove_STK04
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115C:NULL+0:-1:1
	;--cdb--W:r0x115E:NULL+0:-1:1
	;--cdb--W:r0x115C:NULL+0:4454:0
	;--cdb--W:r0x115D:NULL+0:4453:0
	;--cdb--W:r0x115D:NULL+0:4437:0
	;--cdb--W:r0x115E:NULL+0:4452:0
	;--cdb--W:r0x115F:NULL+0:4437:0
	;--cdb--W:r0x115F:NULL+0:4453:0
	end
