;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_moduint.c"
	.module _moduint
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_moduint$0$0({2}DF,SI:U),C,0,0
	;--cdb--F:G$_moduint$0$0({2}DF,SI:U),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _moduint-code 
.globl __moduint

;--------------------------------------------------------
	.FUNC __moduint:$PNUM 4:$L:r0x1155:$L:__moduint_STK00:$L:__moduint_STK01:$L:__moduint_STK02:$L:r0x1158\
:$L:r0x1159
;--------------------------------------------------------
;	.line	6; "_moduint.c"	_moduint (unsigned int a, unsigned int b)
__moduint:	;Function start
	STA	r0x1155
;	;.line	8; "_moduint.c"	unsigned char count = 0;
	CLRA	
	STA	r0x1158
;	;.line	10; "_moduint.c"	while (!MSB_SET(b))
	STA	r0x1159
_00107_DS_:
	LDA	__moduint_STK01
	JMI	_00112_DS_
;	;.line	12; "_moduint.c"	b <<= 1;
	LDA	__moduint_STK02
	SHL	
	STA	__moduint_STK02
	LDA	__moduint_STK01
	ROL	
	STA	__moduint_STK01
;	;.line	13; "_moduint.c"	if (b > a)
	SETB	_C
	LDA	__moduint_STK00
	SUBB	__moduint_STK02
	LDA	r0x1155
	SUBB	__moduint_STK01
	JC	_00106_DS_
;	;.line	15; "_moduint.c"	b >>=1;
	LDA	__moduint_STK01
	SHR	
	STA	__moduint_STK01
	LDA	__moduint_STK02
	ROR	
	STA	__moduint_STK02
;	;.line	16; "_moduint.c"	break;
	JMP	_00112_DS_
_00106_DS_:
;	;.line	18; "_moduint.c"	count++;
	LDA	r0x1159
	INCA	
	STA	r0x1159
	STA	r0x1158
	JMP	_00107_DS_
_00112_DS_:
;	;.line	22; "_moduint.c"	if (a >= b)
	SETB	_C
	LDA	__moduint_STK00
	SUBB	__moduint_STK02
	LDA	r0x1155
	SUBB	__moduint_STK01
	JNC	_00111_DS_
;	;.line	23; "_moduint.c"	a -= b;
	SETB	_C
	LDA	__moduint_STK00
	SUBB	__moduint_STK02
	STA	__moduint_STK00
	LDA	r0x1155
	SUBB	__moduint_STK01
	STA	r0x1155
_00111_DS_:
;	;.line	24; "_moduint.c"	b >>= 1;
	LDA	__moduint_STK01
	SHR	
	STA	__moduint_STK01
	LDA	__moduint_STK02
	ROR	
	STA	__moduint_STK02
;	;.line	26; "_moduint.c"	while (count--);
	LDA	r0x1158
	STA	r0x1159
	LDA	r0x1158
	DECA	
	STA	r0x1158
	LDA	r0x1159
	JNZ	_00112_DS_
;	;.line	27; "_moduint.c"	return a;
	LDA	__moduint_STK00
	STA	STK00
	LDA	r0x1155
;	;.line	28; "_moduint.c"	}
	RET	
; exit point of __moduint
	.ENDFUNC __moduint
	;--cdb--S:G$_moduint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$STATUS$0$0({1}SC:U),E,0,0
	;--cdb--S:L_moduint._moduint$b$65536$1({2}SI:U),R,0,0,[__moduint_STK02,__moduint_STK01]
	;--cdb--S:L_moduint._moduint$a$65536$1({2}SI:U),R,0,0,[__moduint_STK00,r0x1155]
	;--cdb--S:L_moduint._moduint$count$65536$2({1}SC:U),R,0,0,[r0x1158]
	;--cdb--S:G$_moduint$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_STATUS

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__moduint
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__moduint_0	udata
r0x1155:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__moduint_STK00:	.ds	1
	.globl __moduint_STK00
__moduint_STK01:	.ds	1
	.globl __moduint_STK01
__moduint_STK02:	.ds	1
	.globl __moduint_STK02
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	end
