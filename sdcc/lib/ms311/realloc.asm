;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"realloc.c"
	.module realloc
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Frealloc$header[({0}S:S$next$0$0({2}DX,STheader:S),Z,0,0)({2}S:S$next_free$0$0({2}DX,STheader:S),Z,0,0)]
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--F:G$realloc$0$0({2}DF,DX,SV:S),C,0,0,0,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--F:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0,0,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__sdcc_heap_init$0$0({2}DF,SV:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; realloc-code 
.globl _realloc

;--------------------------------------------------------
	.FUNC _realloc:$PNUM 4:$C:___sdcc_heap_init:$C:_malloc:$C:_free:$C:_memmove\
:$C:_memcpy\
:$L:r0x1155:$L:_realloc_STK00:$L:_realloc_STK01:$L:_realloc_STK02:$L:r0x1159\
:$L:r0x1158:$L:r0x115A:$L:r0x115B:$L:r0x115C:$L:r0x115D\
:$L:r0x115E:$L:r0x115F:$L:r0x1161:$L:r0x1160:$L:r0x1163\
:$L:r0x1162:$L:r0x1165:$L:r0x1164:$L:r0x1167:$L:r0x1166\
:$L:r0x1169:$L:r0x1168:$L:r0x116A:$L:r0x116B:$L:r0x116C\
:$L:r0x116D:$XL:___sdcc_heap_free
;--------------------------------------------------------
;	.line	47; "realloc.c"	void XDATA *realloc(void *ptr, size_t size)
_realloc:	;Function start
	STA	r0x1155
;	;.line	54; "realloc.c"	if(!__sdcc_heap_free)
	LDA	#(___sdcc_heap_free + 0)
	STA	_ROMPL
	LDA	#high (___sdcc_heap_free + 0)
	STA	_ROMPH
	LDA	@_ROMPINC
	ORA	@_ROMPINC
	JNZ	_00106_DS_
;	;.line	55; "realloc.c"	__sdcc_heap_init();
	CALL	___sdcc_heap_init
_00106_DS_:
;	;.line	57; "realloc.c"	if(!ptr)
	LDA	_realloc_STK00
	ORA	r0x1155
	JNZ	_00108_DS_
;	;.line	58; "realloc.c"	return(malloc(size));
	LDA	_realloc_STK02
	STA	_malloc_STK00
	LDA	_realloc_STK01
	CALL	_malloc
	JMP	_00138_DS_
_00108_DS_:
;	;.line	60; "realloc.c"	if(!size)
	LDA	_realloc_STK02
	ORA	_realloc_STK01
	JNZ	_00110_DS_
;	;.line	62; "realloc.c"	free(ptr);
	LDA	_realloc_STK00
	STA	_free_STK00
	LDA	r0x1155
	CALL	_free
;	;.line	63; "realloc.c"	return(0);
	CLRA	
	STA	STK00
	JMP	_00138_DS_
_00110_DS_:
;	;.line	66; "realloc.c"	prev_free = 0, pf = 0;
	CLRA	
	STA	r0x1158
	STA	r0x1159
	STA	r0x115A
	STA	r0x115B
;	;.line	67; "realloc.c"	for(h = __sdcc_heap_free, f = &__sdcc_heap_free; h && h < ptr; prev_free = h, pf = f, f = &(h->next_free), h = h->next_free); // Find adjacent blocks in free list
	LDA	#(___sdcc_heap_free + 0)
	STA	_ROMPL
	LDA	#high (___sdcc_heap_free + 0)
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115C
	LDA	@_ROMPINC
	STA	r0x115D
	LDA	#(___sdcc_heap_free + 0)
	STA	r0x115E
	LDA	#high (___sdcc_heap_free + 0)
	STA	r0x115F
_00136_DS_:
	LDA	r0x115C
	ORA	r0x115D
	JZ	_00111_DS_
	SETB	_C
	LDA	r0x115C
	SUBB	_realloc_STK00
	LDA	r0x115D
	SUBB	r0x1155
	JC	_00111_DS_
	LDA	r0x115C
	STA	r0x1158
	LDA	r0x115D
	STA	r0x1159
	LDA	r0x115E
	STA	r0x115A
	LDA	r0x115F
	STA	r0x115B
	LDA	#0x02
	ADD	r0x115C
	STA	r0x1160
	CLRA	
	ADDC	r0x115D
	STA	r0x1161
	LDA	r0x1160
	STA	r0x115E
	LDA	r0x1161
	STA	r0x115F
	LDA	r0x1160
	STA	_ROMPL
	LDA	r0x1161
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115C
	LDA	@_ROMPINC
	STA	r0x115D
	JMP	_00136_DS_
_00111_DS_:
;	;.line	70; "realloc.c"	if(!size || size + offsetof(struct header, next_free) < size)
	LDA	_realloc_STK02
	ORA	_realloc_STK01
	JZ	_00112_DS_
	LDA	#0x02
	ADD	_realloc_STK02
	STA	r0x1160
	CLRA	
	ADDC	_realloc_STK01
	STA	r0x1161
	SETB	_C
	LDA	r0x1160
	SUBB	_realloc_STK02
	LDA	r0x1161
	SUBB	_realloc_STK01
	JC	_00113_DS_
_00112_DS_:
;	;.line	71; "realloc.c"	return(0);
	CLRA	
	STA	STK00
	JMP	_00138_DS_
_00113_DS_:
;	;.line	73; "realloc.c"	if(blocksize < sizeof(struct header)) // Requiring a minimum size makes it easier to implement free(), and avoid memory leaks.
	LDA	r0x1160
	ADD	#0xfc
	LDA	r0x1161
	ADDC	#0xff
	JC	_00116_DS_
;	;.line	74; "realloc.c"	blocksize = sizeof(struct header);
	LDA	#0x04
	STA	r0x1160
	CLRA	
	STA	r0x1161
_00116_DS_:
;	;.line	76; "realloc.c"	h = (void XDATA *)((char XDATA *)(ptr) - offsetof(struct header, next_free));
	LDA	_realloc_STK00
	ADD	#0xfe
	STA	r0x1162
	LDA	#0xff
	ADDC	r0x1155
	STA	r0x1163
;	;.line	77; "realloc.c"	oldblocksize = (char XDATA *)(h->next) - (char XDATA *)h;
	LDA	r0x1162
	STA	_ROMPL
	LDA	r0x1163
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1164
	LDA	@_ROMPINC
	STA	r0x1165
	LDA	r0x1163
	STA	r0x1169
	LDA	r0x1162
	STA	r0x1168
	SETB	_C
	LDA	r0x1164
	SUBB	r0x1168
	STA	r0x1166
	LDA	r0x1165
	SUBB	r0x1169
	STA	r0x1167
;	;.line	79; "realloc.c"	maxblocksize = oldblocksize;
	LDA	r0x1166
	STA	r0x116A
	LDA	r0x1167
	STA	r0x116B
;	;.line	80; "realloc.c"	if(prev_free && prev_free->next == h) // Can merge with previous block
	LDA	r0x1158
	ORA	r0x1159
	JZ	_00118_DS_
	LDA	r0x1158
	STA	_ROMPL
	LDA	r0x1159
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x116C
	LDA	@_ROMPINC
	STA	r0x116D
	LDA	r0x1162
	XOR	r0x116C
	JNZ	_00265_DS_
	LDA	r0x1163
	XOR	r0x116D
_00265_DS_:
	JNZ	_00118_DS_
;	;.line	81; "realloc.c"	maxblocksize += (char XDATA *)h - (char XDATA *)prev_free;
	SETB	_C
	LDA	r0x1168
	SUBB	r0x1158
	STA	r0x1168
	LDA	r0x1169
	SUBB	r0x1159
	STA	r0x1169
	LDA	r0x1166
	ADD	r0x1168
	STA	r0x116A
	LDA	r0x1167
	ADDC	r0x1169
	STA	r0x116B
_00118_DS_:
;	;.line	82; "realloc.c"	if(next_free == h->next) // Can merge with next block
	LDA	r0x1164
	XOR	r0x115C
	JNZ	_00268_DS_
	LDA	r0x1165
	XOR	r0x115D
_00268_DS_:
	JNZ	_00121_DS_
;	;.line	83; "realloc.c"	maxblocksize += (char XDATA *)(next_free->next) - (char XDATA *)next_free;
	LDA	r0x115C
	STA	_ROMPL
	LDA	r0x115D
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1164
	LDA	@_ROMPINC
	STA	r0x1165
	SETB	_C
	LDA	r0x1164
	SUBB	r0x115C
	STA	r0x1168
	LDA	r0x1165
	SUBB	r0x115D
	STA	r0x1169
	LDA	r0x116A
	ADD	r0x1168
	STA	r0x116A
	LDA	r0x116B
	ADDC	r0x1169
	STA	r0x116B
_00121_DS_:
;	;.line	85; "realloc.c"	if(blocksize <= maxblocksize) // Can resize in place.
	SETB	_C
	LDA	r0x116A
	SUBB	r0x1160
	LDA	r0x116B
	SUBB	r0x1161
	JNC	_00131_DS_
;	;.line	87; "realloc.c"	if(prev_free && prev_free->next == h) // Always move into previous block to defragment
	LDA	r0x1158
	ORA	r0x1159
	JZ	_00123_DS_
	LDA	r0x1158
	STA	_ROMPL
	LDA	r0x1159
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1164
	LDA	@_ROMPINC
	STA	r0x1165
	LDA	r0x1162
	XOR	r0x1164
	JNZ	_00273_DS_
	LDA	r0x1163
	XOR	r0x1165
_00273_DS_:
	JNZ	_00123_DS_
;	;.line	89; "realloc.c"	memmove(prev_free, h, blocksize <= oldblocksize ? blocksize : oldblocksize);
	SETB	_C
	LDA	r0x1166
	SUBB	r0x1160
	LDA	r0x1167
	SUBB	r0x1161
	JNC	_00140_DS_
	LDA	r0x1160
	STA	r0x116C
	LDA	r0x1161
	STA	r0x116D
	JMP	_00141_DS_
_00140_DS_:
	LDA	r0x1166
	STA	r0x116C
	LDA	r0x1167
	STA	r0x116D
_00141_DS_:
	LDA	r0x116C
	STA	_memmove_STK04
	LDA	r0x116D
	STA	_memmove_STK03
	LDA	r0x1162
	STA	_memmove_STK02
	LDA	r0x1163
	STA	_memmove_STK01
	LDA	r0x1158
	STA	_memmove_STK00
	LDA	r0x1159
	CALL	_memmove
;	;.line	90; "realloc.c"	h = prev_free;
	LDA	r0x1158
	STA	r0x1162
	LDA	r0x1159
	STA	r0x1163
;	;.line	91; "realloc.c"	*pf = next_free;
	LDA	r0x115A
	STA	_ROMPL
	LDA	r0x115B
	STA	_ROMPH
	LDA	r0x115C
	STA	@_ROMPINC
	LDA	r0x115D
	STA	@_ROMP
;	;.line	92; "realloc.c"	f = pf;
	LDA	r0x115A
	STA	r0x115E
	LDA	r0x115B
	STA	r0x115F
_00123_DS_:
;	;.line	95; "realloc.c"	if(next_free && next_free == h->next) // Merge with following block
	LDA	r0x115C
	ORA	r0x115D
	JZ	_00126_DS_
	LDA	r0x1162
	STA	_ROMPL
	LDA	r0x1163
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1158
	LDA	@_ROMPINC
	STA	r0x1159
	LDA	r0x1158
	XOR	r0x115C
	JNZ	_00274_DS_
	LDA	r0x1159
	XOR	r0x115D
_00274_DS_:
	JNZ	_00126_DS_
;	;.line	97; "realloc.c"	h->next = next_free->next;
	LDA	r0x115C
	STA	_ROMPL
	LDA	r0x115D
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1158
	LDA	@_ROMPINC
	STA	r0x1159
	LDA	r0x1162
	STA	_ROMPL
	LDA	r0x1163
	STA	_ROMPH
	LDA	r0x1158
	STA	@_ROMPINC
	LDA	r0x1159
	STA	@_ROMP
;	;.line	98; "realloc.c"	*f = next_free->next_free;
	LDA	#0x02
	ADD	r0x115C
	STA	r0x115C
	CLRA	
	ADDC	r0x115D
	STA	r0x115D
	LDA	r0x115C
	STA	_ROMPL
	LDA	r0x115D
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1158
	LDA	@_ROMPINC
	STA	r0x1159
	LDA	r0x115E
	STA	_ROMPL
	LDA	r0x115F
	STA	_ROMPH
	LDA	r0x1158
	STA	@_ROMPINC
	LDA	r0x1159
	STA	@_ROMP
_00126_DS_:
;	;.line	101; "realloc.c"	if(maxblocksize >= blocksize + sizeof(struct header)) // Create new block from free space
	LDA	#0x04
	ADD	r0x1160
	STA	r0x1158
	CLRA	
	ADDC	r0x1161
	STA	r0x1159
	SETB	_C
	LDA	r0x116A
	SUBB	r0x1158
	LDA	r0x116B
	SUBB	r0x1159
	JNC	_00129_DS_
;	;.line	103; "realloc.c"	header_t *const newheader = (header_t *const)((char XDATA *)h + blocksize);
	LDA	r0x1162
	ADD	r0x1160
	STA	r0x1160
	LDA	r0x1163
	ADDC	r0x1161
	STA	r0x1159
	LDA	r0x1160
	STA	r0x1158
;	;.line	104; "realloc.c"	newheader->next = h->next;
	LDA	r0x1162
	STA	_ROMPL
	LDA	r0x1163
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x1160
	LDA	@_ROMPINC
	STA	r0x1161
	LDA	r0x1158
	STA	_ROMPL
	LDA	r0x1159
	STA	_ROMPH
	LDA	r0x1160
	STA	@_ROMPINC
	LDA	r0x1161
	STA	@_ROMP
;	;.line	105; "realloc.c"	newheader->next_free = *f;
	LDA	#0x02
	ADD	r0x1158
	STA	r0x115A
	CLRA	
	ADDC	r0x1159
	STA	r0x115B
	LDA	r0x115E
	STA	_ROMPL
	LDA	r0x115F
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115C
	LDA	@_ROMPINC
	STA	r0x115D
	LDA	r0x115A
	STA	_ROMPL
	LDA	r0x115B
	STA	_ROMPH
	LDA	r0x115C
	STA	@_ROMPINC
	LDA	r0x115D
	STA	@_ROMP
;	;.line	106; "realloc.c"	*f = newheader;
	LDA	r0x115E
	STA	_ROMPL
	LDA	r0x115F
	STA	_ROMPH
	LDA	r0x1158
	STA	@_ROMPINC
	LDA	r0x1159
	STA	@_ROMP
;	;.line	107; "realloc.c"	h->next = newheader;
	LDA	r0x1162
	STA	_ROMPL
	LDA	r0x1163
	STA	_ROMPH
	LDA	r0x1158
	STA	@_ROMPINC
	LDA	r0x1159
	STA	@_ROMP
_00129_DS_:
;	;.line	110; "realloc.c"	return(&(h->next_free));
	LDA	#0x02
	ADD	r0x1162
	STA	r0x1162
	CLRA	
	ADDC	r0x1163
	STA	r0x1163
	LDA	r0x1162
	STA	STK00
	LDA	r0x1163
	JMP	_00138_DS_
_00131_DS_:
;	;.line	113; "realloc.c"	if(ret = malloc(size))
	LDA	_realloc_STK02
	STA	_malloc_STK00
	LDA	_realloc_STK01
	CALL	_malloc
	STA	r0x1159
	LDA	STK00
	STA	r0x115A
	LDA	r0x1159
	STA	r0x115B
	LDA	STK00
	ORA	r0x1159
	JZ	_00133_DS_
;	;.line	115; "realloc.c"	size_t oldsize = oldblocksize - offsetof(struct header, next_free);
	LDA	#0xfe
	ADD	r0x1166
	STA	r0x1158
	LDA	#0xff
	ADDC	r0x1167
	STA	r0x1159
;	;.line	116; "realloc.c"	memcpy(ret, ptr, size <= oldsize ? size : oldsize);
	SETB	_C
	LDA	r0x1158
	SUBB	_realloc_STK02
	LDA	r0x1159
	SUBB	_realloc_STK01
	JC	_00143_DS_
	LDA	r0x1158
	STA	_realloc_STK02
	LDA	r0x1159
	STA	_realloc_STK01
_00143_DS_:
	LDA	_realloc_STK02
	STA	_memcpy_STK04
	LDA	_realloc_STK01
	STA	_memcpy_STK03
	LDA	_realloc_STK00
	STA	_memcpy_STK02
	LDA	r0x1155
	STA	_memcpy_STK01
	LDA	r0x115A
	STA	_memcpy_STK00
	LDA	r0x115B
	CALL	_memcpy
;	;.line	117; "realloc.c"	free(ptr);
	LDA	_realloc_STK00
	STA	_free_STK00
	LDA	r0x1155
	CALL	_free
;	;.line	118; "realloc.c"	return(ret);
	LDA	r0x115A
	STA	STK00
	LDA	r0x115B
	JMP	_00138_DS_
_00133_DS_:
;	;.line	121; "realloc.c"	return(0);
	CLRA	
	STA	STK00
_00138_DS_:
;	;.line	122; "realloc.c"	}
	RET	
; exit point of _realloc
	.ENDFUNC _realloc
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$atof$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atoi$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$atol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtol$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$strtoul$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ultoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_ltoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$rand$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$srand$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$calloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$malloc$0$0({2}DF,DX,SV:S),C,0,0
	;--cdb--S:G$aligned_alloc$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$free$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$bsearch$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$qsort$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$abs$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$labs$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$mblen$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbtowc$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$wctomb$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$mbstowcs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$wcstombs$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__sdcc_heap_init$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:Lrealloc.aligned_alloc$size$65536$15({2}SI:U),E,0,0
	;--cdb--S:Lrealloc.aligned_alloc$alignment$65536$15({2}SI:U),E,0,0
	;--cdb--S:Lrealloc.realloc$size$65536$50({2}SI:U),R,0,0,[_realloc_STK02,_realloc_STK01]
	;--cdb--S:Lrealloc.realloc$ptr$65536$50({2}DG,SV:S),R,0,0,[_realloc_STK00,r0x1155]
	;--cdb--S:Lrealloc.realloc$ret$65536$51({2}DX,SV:S),R,0,0,[r0x115A,r0x115B]
	;--cdb--S:Lrealloc.realloc$h$65536$51({2}DX,STheader:S),R,0,0,[r0x1162,r0x1163]
	;--cdb--S:Lrealloc.realloc$next_free$65536$51({2}DX,STheader:S),R,0,0,[r0x115C,r0x115D]
	;--cdb--S:Lrealloc.realloc$prev_free$65536$51({2}DX,STheader:S),R,0,0,[r0x1158,r0x1159]
	;--cdb--S:Lrealloc.realloc$f$65536$51({2}DX,DX,STheader:S),R,0,0,[r0x115E,r0x115F]
	;--cdb--S:Lrealloc.realloc$pf$65536$51({2}DX,DX,STheader:S),R,0,0,[r0x115A,r0x115B]
	;--cdb--S:Lrealloc.realloc$blocksize$65536$51({2}SI:U),R,0,0,[r0x1160,r0x1161]
	;--cdb--S:Lrealloc.realloc$oldblocksize$65536$51({2}SI:U),R,0,0,[r0x1166,r0x1167]
	;--cdb--S:Lrealloc.realloc$maxblocksize$65536$51({2}SI:U),R,0,0,[r0x116A,r0x116B]
	;--cdb--S:Lrealloc.realloc$newheader$196608$57({2}DX,STheader:S),R,0,0,[r0x1158,r0x1159]
	;--cdb--S:Lrealloc.realloc$oldsize$131072$58({2}SI:U),R,0,0,[r0x1158,r0x1159]
	;--cdb--S:G$__sdcc_heap_free$0$0({2}DX,STheader:S),F,0,0
	;--cdb--S:G$realloc$0$0({2}DF,DX,SV:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_malloc
	.globl	_free
	.globl	_memcpy
	.globl	_memmove
	.globl	___sdcc_heap_init
	.globl	___sdcc_heap_free

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_realloc
	.globl	_realloc_h_65536_51
	.globl	_realloc_next_free_65536_51
	.globl	_realloc_prev_free_65536_51
	.globl	_realloc_f_65536_51
	.globl	_realloc_pf_65536_51
	.globl	_realloc_newheader_196608_57

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
_realloc_h_65536_51:	.ds	2

	.area DSEG(DATA)
_realloc_next_free_65536_51:	.ds	2

	.area DSEG(DATA)
_realloc_prev_free_65536_51:	.ds	2

	.area DSEG(DATA)
_realloc_f_65536_51:	.ds	2

	.area DSEG(DATA)
_realloc_pf_65536_51:	.ds	2

	.area DSEG(DATA)
_realloc_newheader_196608_57:	.ds	2

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_realloc_0	udata
r0x1155:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
r0x1169:	.ds	1
r0x116A:	.ds	1
r0x116B:	.ds	1
r0x116C:	.ds	1
r0x116D:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_realloc_STK00:	.ds	1
	.globl _realloc_STK00
_realloc_STK01:	.ds	1
	.globl _realloc_STK01
_realloc_STK02:	.ds	1
	.globl _realloc_STK02
	.globl _malloc_STK00
	.globl _free_STK00
	.globl _memmove_STK04
	.globl _memmove_STK03
	.globl _memmove_STK02
	.globl _memmove_STK01
	.globl _memmove_STK00
	.globl _memcpy_STK04
	.globl _memcpy_STK03
	.globl _memcpy_STK02
	.globl _memcpy_STK01
	.globl _memcpy_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1158:NULL+0:-1:1
	;--cdb--W:r0x1162:NULL+0:-1:1
	;--cdb--W:r0x1159:NULL+0:4451:0
	;--cdb--W:r0x1158:NULL+0:4450:0
	;--cdb--W:r0x1158:NULL+0:14:0
	;--cdb--W:r0x115C:NULL+0:4442:0
	;--cdb--W:r0x115D:NULL+0:4443:0
	;--cdb--W:r0x1161:NULL+0:4437:0
	;--cdb--W:r0x1160:NULL+0:4462:0
	;--cdb--W:r0x1163:NULL+0:4437:0
	;--cdb--W:r0x1165:NULL+0:4451:0
	;--cdb--W:r0x1165:NULL+0:4445:0
	;--cdb--W:r0x1165:NULL+0:4457:0
	;--cdb--W:r0x1165:NULL+0:4441:0
	;--cdb--W:r0x1164:NULL+0:4450:0
	;--cdb--W:r0x1164:NULL+0:4444:0
	;--cdb--W:r0x1164:NULL+0:4456:0
	;--cdb--W:r0x1164:NULL+0:4440:0
	;--cdb--W:r0x1167:NULL+0:4453:0
	;--cdb--W:r0x1166:NULL+0:4452:0
	;--cdb--W:r0x1169:NULL+0:4453:0
	;--cdb--W:r0x1169:NULL+0:4451:0
	;--cdb--W:r0x1168:NULL+0:4452:0
	;--cdb--W:r0x1168:NULL+0:4450:0
	;--cdb--W:r0x116C:NULL+0:4440:0
	;--cdb--W:r0x116C:NULL+0:4456:0
	;--cdb--W:r0x116D:NULL+0:4441:0
	;--cdb--W:r0x116D:NULL+0:4457:0
	;--cdb--W:r0x1159:NULL+0:-1:1
	;--cdb--W:r0x1161:NULL+0:-1:1
	end
