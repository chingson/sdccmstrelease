;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.3.0 #8604 (Sep 15 2014) (MSVC)
; This file was generated Sat Oct 11 21:41:17 2014
;--------------------------------------------------------
; MST port for the MS322 series simple CPU
;--------------------------------------------------------
	.module _shr_ulong
	;.list	p=MS205
	;.radix dec
	.include "ms326sfr.def"
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
;--------------------------------------------------------
; Code Head 
;--------------------------------------------------------
	.area CSEG	 (CODE) 
;--------------------------------------------------------
; code
;--------------------------------------------------------
; sh ulong, assembly is the source!!
; input: _PTRCL is the data byte
; input STK00 is MSB!!
; input: ACC is the shift num
.FUNC	__shr_ulong
__shr_ulong:	;Function start
; 2 exit points
	sta 	_ROMPL ; temp var
	add	#0xe0
	jc	shift_too_large
	lda	_ROMPL
loop_shr:
	jnz	cont_shift
	ret
cont_shift:
	lda	STK00
	shr
	sta	STK00
	lda	STK01
	ror
	sta	STK01
	lda	STK02
	ror
	sta	STK02
	lda	_PTRCL
	ror
	sta	_PTRCL
	lda	_ROMPL
	deca
	sta	_ROMPL
	jmp	loop_shr

	
shift_too_large:
	clra
	sta	_PTRCL
	sta	STK00
	sta	STK01
	sta	STK02
	ret

; exit point of _shr_ulong


;	code size estimation:
;	   21+   10 =    31 instructions (   31 byte)

;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__shr_ulong
	.globl  STK00
	.globl  STK01
	.globl  STK02

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	;end
