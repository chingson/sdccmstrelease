;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_strcat.c"
	.module _strcat
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--F:G$strcat$0$0({2}DF,DG,SC:U),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _strcat-code 
.globl _strcat

;--------------------------------------------------------
	.FUNC _strcat:$PNUM 4:$L:r0x1155:$L:_strcat_STK00:$L:_strcat_STK01:$L:_strcat_STK02:$L:r0x1158\
:$L:r0x1159:$L:r0x115A
;--------------------------------------------------------
;	.line	31; "_strcat.c"	char * strcat (char * dst, char * src )
_strcat:	;Function start
	STA	r0x1155
;	;.line	35; "_strcat.c"	while( *cp )
	LDA	_strcat_STK00
	STA	r0x1158
	LDA	r0x1155
	STA	r0x1159
_00105_DS_:
	LDA	r0x1158
	STA	_ROMPL
	LDA	r0x1159
	STA	_ROMPH
	LDA	@_ROMPINC
	JZ	_00108_DS_
;	;.line	36; "_strcat.c"	cp++;                   /* find end of dst */
	LDA	r0x1158
	INCA	
	STA	r0x1158
	CLRA	
	ADDC	r0x1159
	STA	r0x1159
	JMP	_00105_DS_
_00108_DS_:
;	;.line	38; "_strcat.c"	while( *cp++ = *src++ ) ;       /* Copy src to end of dst */
	LDA	_strcat_STK02
	STA	_ROMPL
	LDA	_strcat_STK01
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115A
	LDA	_strcat_STK02
	INCA	
	STA	_strcat_STK02
	CLRA	
	ADDC	_strcat_STK01
	STA	_strcat_STK01
	LDA	r0x1159
	STA	_ROMPH
	LDA	r0x1158
	STA	_ROMPL
	LDA	r0x115A
	STA	@_ROMPINC
	LDA	r0x1158
	INCA	
	STA	r0x1158
	CLRA	
	ADDC	r0x1159
	STA	r0x1159
	LDA	r0x115A
	JNZ	_00108_DS_
;	;.line	40; "_strcat.c"	return( dst );                  /* return dst */
	LDA	_strcat_STK00
	STA	STK00
	LDA	r0x1155
;	;.line	41; "_strcat.c"	}
	RET	
; exit point of _strcat
	.ENDFUNC _strcat
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_strcat.strcat$src$65536$21({2}DG,SC:U),R,0,0,[_strcat_STK02,_strcat_STK01]
	;--cdb--S:L_strcat.strcat$dst$65536$21({2}DG,SC:U),R,0,0,[_strcat_STK00,r0x1155]
	;--cdb--S:L_strcat.strcat$cp$65536$22({2}DG,SC:U),R,0,0,[r0x1158,r0x1159]
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_strcat
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__strcat_0	udata
r0x1155:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_strcat_STK00:	.ds	1
	.globl _strcat_STK00
_strcat_STK01:	.ds	1
	.globl _strcat_STK01
_strcat_STK02:	.ds	1
	.globl _strcat_STK02
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115A:NULL+0:-1:1
	end
