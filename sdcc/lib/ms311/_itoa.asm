;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_itoa.c"
	.module _itoa
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$_uitoa$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$_moduint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_divuint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$_itoa$0$0({2}DF,SV:S),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _itoa-code 
.globl __itoa

;--------------------------------------------------------
	.FUNC __itoa:$PNUM 5:$C:__uitoa\
:$L:r0x116C:$L:__itoa_STK00:$L:__itoa_STK01:$L:__itoa_STK02:$L:__itoa_STK03\

;--------------------------------------------------------
;	.line	66; "_itoa.c"	void _itoa(int value, __data char* string, unsigned char radix)
__itoa:	;Function start
	STA	r0x116C
;	;.line	68; "_itoa.c"	if (value < 0 && radix == 10) {
	JPL	_00154_DS_
	LDA	__itoa_STK03
	XOR	#0x0a
	JNZ	_00154_DS_
;	;.line	69; "_itoa.c"	*string++ = '-';
	LDA	__itoa_STK02
	STA	_ROMPL
	LDA	__itoa_STK01
	STA	_ROMPH
	LDA	#0x2d
	STA	@_ROMP
	LDA	__itoa_STK02
	INCA	
	STA	__itoa_STK02
	CLRA	
	ADDC	__itoa_STK01
	STA	__itoa_STK01
;	;.line	70; "_itoa.c"	value = -value;
	SETB	_C
	CLRA	
	SUBB	__itoa_STK00
	STA	__itoa_STK00
	CLRA	
	SUBB	r0x116C
	STA	r0x116C
_00154_DS_:
;	;.line	72; "_itoa.c"	_uitoa(value, string, radix);
	LDA	__itoa_STK03
	STA	__uitoa_STK03
	LDA	__itoa_STK02
	STA	__uitoa_STK02
	LDA	__itoa_STK01
	STA	__uitoa_STK01
	LDA	__itoa_STK00
	STA	__uitoa_STK00
	LDA	r0x116C
	CALL	__uitoa
;	;.line	73; "_itoa.c"	}
	RET	
; exit point of __itoa
	.ENDFUNC __itoa
.globl __uitoa

;--------------------------------------------------------
	.FUNC __uitoa:$PNUM 5:$C:__moduint:$C:__divuint\
:$L:r0x1155:$L:__uitoa_STK00:$L:__uitoa_STK01:$L:__uitoa_STK02:$L:__uitoa_STK03\
:$L:r0x1159:$L:r0x115A:$L:r0x115B:$L:r0x115C:$L:r0x115D\
:$L:r0x115E:$L:r0x1160
;--------------------------------------------------------
;	.line	40; "_itoa.c"	void _uitoa(unsigned int value,__data char* string, unsigned char radix)
__uitoa:	;Function start
	STA	r0x1155
;	;.line	45; "_itoa.c"	do {
	CLRA	
	STA	r0x1159
_00107_DS_:
;	;.line	46; "_itoa.c"	string[index] = '0' + (value % radix);
	LDA	r0x1159
	ADD	__uitoa_STK02
	STA	r0x115A
	CLRA	
	ADDC	__uitoa_STK01
	STA	r0x115B
	LDA	__uitoa_STK03
	STA	__moduint_STK02
	CLRA	
	STA	__moduint_STK01
	LDA	__uitoa_STK00
	STA	__moduint_STK00
	LDA	r0x1155
	CALL	__moduint
	LDA	STK00
	ADD	#0x30
	STA	r0x1160
	LDA	r0x115A
	STA	_ROMPL
	LDA	r0x115B
	STA	_ROMPH
	LDA	r0x1160
	STA	@_ROMP
;	;.line	47; "_itoa.c"	if (string[index] > '9')
	LDA	r0x115A
	STA	_ROMPL
	LDA	r0x115B
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115E
	SETB	_C
	LDA	#0x39
	SUBB	r0x115E
	JC	_00106_DS_
;	;.line	48; "_itoa.c"	string[index] += 'A' - '9' - 1;
	LDA	r0x115A
	STA	_ROMPL
	LDA	r0x115B
	STA	_ROMPH
	LDA	@_ROMPINC
	ADD	#0x07
	STA	r0x115E
	LDA	r0x115A
	STA	_ROMPL
	LDA	r0x115B
	STA	_ROMPH
	LDA	r0x115E
	STA	@_ROMP
_00106_DS_:
;	;.line	49; "_itoa.c"	value /= radix;
	LDA	__uitoa_STK03
	STA	__divuint_STK02
	CLRA	
	STA	__divuint_STK01
	LDA	__uitoa_STK00
	STA	__divuint_STK00
	LDA	r0x1155
	CALL	__divuint
	STA	r0x1155
	LDA	STK00
	STA	__uitoa_STK00
;	;.line	50; "_itoa.c"	++index;
	LDA	r0x1159
	INCA	
	STA	r0x1159
;	;.line	51; "_itoa.c"	} while (value != 0);
	LDA	__uitoa_STK00
	ORA	r0x1155
	JNZ	_00107_DS_
;	;.line	54; "_itoa.c"	string[index--] = '\0';
	LDA	r0x1159
	DECA	
	STA	__uitoa_STK00
	LDA	r0x1159
	ADD	__uitoa_STK02
	STA	r0x1159
	CLRA	
	ADDC	__uitoa_STK01
	STA	r0x1155
	LDA	r0x1159
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	CLRA	
	STA	@_ROMP
;	;.line	57; "_itoa.c"	while (index > i) {
	STA	r0x1155
_00110_DS_:
	SETB	_C
	LDA	r0x1155
	SUBB	__uitoa_STK00
	JC	_00113_DS_
;	;.line	58; "_itoa.c"	char tmp = string[i];
	LDA	r0x1155
	ADD	__uitoa_STK02
	STA	__uitoa_STK03
	CLRA	
	ADDC	__uitoa_STK01
	STA	r0x1159
	LDA	__uitoa_STK03
	STA	_ROMPL
	LDA	r0x1159
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115A
;	;.line	59; "_itoa.c"	string[i] = string[index];
	LDA	__uitoa_STK00
	ADD	__uitoa_STK02
	STA	r0x115B
	CLRA	
	ADDC	__uitoa_STK01
	STA	r0x115C
	LDA	r0x115B
	STA	_ROMPL
	LDA	r0x115C
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115D
	LDA	__uitoa_STK03
	STA	_ROMPL
	LDA	r0x1159
	STA	_ROMPH
	LDA	r0x115D
	STA	@_ROMP
;	;.line	60; "_itoa.c"	string[index] = tmp;
	LDA	r0x115B
	STA	_ROMPL
	LDA	r0x115C
	STA	_ROMPH
	LDA	r0x115A
	STA	@_ROMP
;	;.line	61; "_itoa.c"	++i;
	LDA	r0x1155
	INCA	
	STA	r0x1155
;	;.line	62; "_itoa.c"	--index;
	LDA	__uitoa_STK00
	DECA	
	STA	__uitoa_STK00
	JMP	_00110_DS_
_00113_DS_:
;	;.line	64; "_itoa.c"	}
	RET	
; exit point of __uitoa
	.ENDFUNC __uitoa
	;--cdb--S:G$_uitoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$_moduint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_divuint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$_itoa$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:L_itoa._uitoa$radix$65536$1({1}SC:U),R,0,0,[__uitoa_STK03]
	;--cdb--S:L_itoa._uitoa$string$65536$1({2}DD,SC:U),R,0,0,[__uitoa_STK02,__uitoa_STK01]
	;--cdb--S:L_itoa._uitoa$value$65536$1({2}SI:U),R,0,0,[__uitoa_STK00,r0x1155]
	;--cdb--S:L_itoa._uitoa$index$65536$2({1}SC:U),R,0,0,[]
	;--cdb--S:L_itoa._uitoa$i$65536$2({1}SC:U),R,0,0,[r0x1155]
	;--cdb--S:L_itoa._uitoa$tmp$131072$4({1}SC:U),R,0,0,[r0x115A]
	;--cdb--S:L_itoa._itoa$radix$65536$5({1}SC:U),R,0,0,[__itoa_STK03]
	;--cdb--S:L_itoa._itoa$string$65536$5({2}DD,SC:U),R,0,0,[__itoa_STK02,__itoa_STK01]
	;--cdb--S:L_itoa._itoa$value$65536$5({2}SI:S),R,0,0,[__itoa_STK00,r0x116C]
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__moduint
	.globl	__divuint

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__uitoa
	.globl	__itoa
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__itoa_0	udata
r0x1155:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x1160:	.ds	1
r0x116C:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__uitoa_STK00:	.ds	1
	.globl __uitoa_STK00
__uitoa_STK01:	.ds	1
	.globl __uitoa_STK01
__uitoa_STK02:	.ds	1
	.globl __uitoa_STK02
__uitoa_STK03:	.ds	1
	.globl __uitoa_STK03
	.globl __moduint_STK02
	.globl __moduint_STK01
	.globl __moduint_STK00
	.globl __divuint_STK02
	.globl __divuint_STK01
	.globl __divuint_STK00
__itoa_STK00:	.ds	1
	.globl __itoa_STK00
__itoa_STK01:	.ds	1
	.globl __itoa_STK01
__itoa_STK02:	.ds	1
	.globl __itoa_STK02
__itoa_STK03:	.ds	1
	.globl __itoa_STK03
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115F:NULL+0:-1:1
	;--cdb--W:r0x115E:NULL+0:-1:1
	;--cdb--W:r0x1160:NULL+0:-1:1
	;--cdb--W:r0x115C:NULL+0:4452:0
	;--cdb--W:r0x115D:NULL+0:0:0
	;--cdb--W:r0x115E:NULL+0:14:0
	;--cdb--W:r0x1160:NULL+0:14:0
	;--cdb--W:r0x115D:NULL+0:-1:1
	end
