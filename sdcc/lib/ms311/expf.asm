;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"expf.c"
	.module expf
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Fexpf$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:S),Z,0,0)]
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$expf$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; expf-code 
.globl _expf

;--------------------------------------------------------
	.FUNC _expf:$PNUM 4:$C:___fslt:$C:___fsmul:$C:___fs2sint:$C:___sint2fs\
:$C:___fssub:$C:___fsadd:$C:___fsdiv:$C:_ldexpf\
:$L:r0x1158:$L:_expf_STK00:$L:_expf_STK01:$L:_expf_STK02:$L:r0x1159\
:$L:r0x115A:$L:r0x115B:$L:r0x115C:$L:r0x115D:$L:r0x115F\
:$L:r0x115E:$L:r0x1163
;--------------------------------------------------------
;	.line	330; "expf.c"	float expf(float x) _FLOAT_FUNC_REENTRANT
_expf:	;Function start
	STA	r0x1158
;	;.line	336; "expf.c"	if(x>=0.0)
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	STA	___fslt_STK04
	STA	___fslt_STK03
	LDA	_expf_STK02
	STA	___fslt_STK02
	LDA	_expf_STK01
	STA	___fslt_STK01
	LDA	_expf_STK00
	STA	___fslt_STK00
	LDA	r0x1158
	CALL	___fslt
	JNZ	_00106_DS_
;	;.line	337; "expf.c"	{ y=x;  sign=0; }
	LDA	_expf_STK02
	STA	r0x1159
	LDA	_expf_STK01
	STA	r0x115A
	LDA	_expf_STK00
	STA	r0x115B
	LDA	r0x1158
	STA	r0x115C
	CLRA	
	STA	r0x115D
	JMP	_00107_DS_
_00106_DS_:
;	;.line	339; "expf.c"	{ y=-x; sign=1; }
	LDA	r0x1158
	XOR	#0x80
	STA	r0x1158
	LDA	_expf_STK02
	STA	r0x1159
	LDA	_expf_STK01
	STA	r0x115A
	LDA	_expf_STK00
	STA	r0x115B
	LDA	r0x1158
	STA	r0x115C
	LDA	#0x01
	STA	r0x115D
_00107_DS_:
;	;.line	341; "expf.c"	if(y<EXPEPS) return 1.0;
	LDA	#0x95
	STA	___fslt_STK06
	LDA	#0xbf
	STA	___fslt_STK05
	LDA	#0xd6
	STA	___fslt_STK04
	LDA	#0x33
	STA	___fslt_STK03
	LDA	r0x1159
	STA	___fslt_STK02
	LDA	r0x115A
	STA	___fslt_STK01
	LDA	r0x115B
	STA	___fslt_STK00
	LDA	r0x115C
	CALL	___fslt
	JZ	_00109_DS_
	CLRA	
	STA	STK02
	STA	STK01
	LDA	#0x80
	STA	STK00
	LDA	#0x3f
	JMP	_00122_DS_
_00109_DS_:
;	;.line	343; "expf.c"	if(y>BIGX)
	LDA	r0x1159
	STA	___fslt_STK06
	LDA	r0x115A
	STA	___fslt_STK05
	LDA	r0x115B
	STA	___fslt_STK04
	LDA	r0x115C
	STA	___fslt_STK03
	LDA	#0x18
	STA	___fslt_STK02
	LDA	#0x72
	STA	___fslt_STK01
	LDA	#0xb1
	STA	___fslt_STK00
	LDA	#0x42
	CALL	___fslt
	JZ	_00114_DS_
;	;.line	345; "expf.c"	if(sign)
	LDA	r0x115D
	JZ	_00111_DS_
;	;.line	347; "expf.c"	errno=ERANGE;
	LDA	#0x22
	STA	_errno
	CLRA	
	STA	(_errno + 1)
;	;.line	349; "expf.c"	;
	DECA	
	STA	STK02
	LDA	#0xff
	STA	STK01
	LDA	#0x7f
	STA	STK00
	JMP	_00122_DS_
_00111_DS_:
;	;.line	353; "expf.c"	return 0.0;
	CLRA	
	STA	STK02
	STA	STK01
	STA	STK00
	JMP	_00122_DS_
_00114_DS_:
;	;.line	357; "expf.c"	z=y*K1;
	LDA	r0x1159
	STA	___fsmul_STK06
	LDA	r0x115A
	STA	___fsmul_STK05
	LDA	r0x115B
	STA	___fsmul_STK04
	LDA	r0x115C
	STA	___fsmul_STK03
	LDA	#0x3b
	STA	___fsmul_STK02
	LDA	#0xaa
	STA	___fsmul_STK01
	LDA	#0xb8
	STA	___fsmul_STK00
	LDA	#0x3f
	CALL	___fsmul
	STA	r0x1158
	LDA	STK00
	STA	_expf_STK00
	LDA	STK01
	STA	_expf_STK01
	LDA	STK02
	STA	_expf_STK02
;	;.line	358; "expf.c"	n=z;
	STA	___fs2sint_STK02
	LDA	_expf_STK01
	STA	___fs2sint_STK01
	LDA	_expf_STK00
	STA	___fs2sint_STK00
	LDA	r0x1158
	CALL	___fs2sint
	STA	r0x115F
	LDA	STK00
	STA	r0x115E
;	;.line	360; "expf.c"	if(n<0) --n;
	LDA	r0x115F
	JPL	_00116_DS_
	LDA	r0x115E
	DECA	
	STA	r0x115E
	LDA	#0xff
	ADDC	r0x115F
	STA	r0x115F
_00116_DS_:
;	;.line	361; "expf.c"	if(z-n>=0.5) ++n;
	LDA	r0x115E
	STA	___sint2fs_STK00
	LDA	r0x115F
	CALL	___sint2fs
	STA	r0x1163
	LDA	STK02
	STA	___fssub_STK06
	LDA	STK01
	STA	___fssub_STK05
	LDA	STK00
	STA	___fssub_STK04
	LDA	r0x1163
	STA	___fssub_STK03
	LDA	_expf_STK02
	STA	___fssub_STK02
	LDA	_expf_STK01
	STA	___fssub_STK01
	LDA	_expf_STK00
	STA	___fssub_STK00
	LDA	r0x1158
	CALL	___fssub
	STA	r0x1158
	CLRA	
	STA	___fslt_STK06
	STA	___fslt_STK05
	STA	___fslt_STK04
	LDA	#0x3f
	STA	___fslt_STK03
	LDA	STK02
	STA	___fslt_STK02
	LDA	STK01
	STA	___fslt_STK01
	LDA	STK00
	STA	___fslt_STK00
	LDA	r0x1158
	CALL	___fslt
	JNZ	_00118_DS_
	LDA	r0x115E
	INCA	
	STA	r0x115E
	CLRA	
	ADDC	r0x115F
	STA	r0x115F
_00118_DS_:
;	;.line	362; "expf.c"	xn=n;
	LDA	r0x115E
	STA	___sint2fs_STK00
	LDA	r0x115F
	CALL	___sint2fs
	STA	r0x1158
	LDA	STK00
	STA	_expf_STK00
	LDA	STK01
	STA	_expf_STK01
	LDA	STK02
	STA	_expf_STK02
;	;.line	363; "expf.c"	g=((y-xn*C1))-xn*C2;
	STA	___fsmul_STK06
	LDA	_expf_STK01
	STA	___fsmul_STK05
	LDA	_expf_STK00
	STA	___fsmul_STK04
	LDA	r0x1158
	STA	___fsmul_STK03
	CLRA	
	STA	___fsmul_STK02
	LDA	#0x80
	STA	___fsmul_STK01
	LDA	#0x31
	STA	___fsmul_STK00
	LDA	#0x3f
	CALL	___fsmul
	STA	r0x1163
	LDA	STK02
	STA	___fssub_STK06
	LDA	STK01
	STA	___fssub_STK05
	LDA	STK00
	STA	___fssub_STK04
	LDA	r0x1163
	STA	___fssub_STK03
	LDA	r0x1159
	STA	___fssub_STK02
	LDA	r0x115A
	STA	___fssub_STK01
	LDA	r0x115B
	STA	___fssub_STK00
	LDA	r0x115C
	CALL	___fssub
	STA	r0x115C
	LDA	STK00
	STA	r0x115B
	LDA	STK01
	STA	r0x115A
	LDA	STK02
	STA	r0x1159
	LDA	_expf_STK02
	STA	___fsmul_STK06
	LDA	_expf_STK01
	STA	___fsmul_STK05
	LDA	_expf_STK00
	STA	___fsmul_STK04
	LDA	r0x1158
	STA	___fsmul_STK03
	LDA	#0x83
	STA	___fsmul_STK02
	LDA	#0x80
	STA	___fsmul_STK01
	LDA	#0x5e
	STA	___fsmul_STK00
	LDA	#0xb9
	CALL	___fsmul
	STA	r0x1158
	LDA	STK02
	STA	___fssub_STK06
	LDA	STK01
	STA	___fssub_STK05
	LDA	STK00
	STA	___fssub_STK04
	LDA	r0x1158
	STA	___fssub_STK03
	LDA	r0x1159
	STA	___fssub_STK02
	LDA	r0x115A
	STA	___fssub_STK01
	LDA	r0x115B
	STA	___fssub_STK00
	LDA	r0x115C
	CALL	___fssub
	STA	r0x1158
	LDA	STK00
	STA	_expf_STK00
	LDA	STK01
	STA	_expf_STK01
	LDA	STK02
	STA	_expf_STK02
;	;.line	364; "expf.c"	z=g*g;
	STA	___fsmul_STK06
	LDA	_expf_STK01
	STA	___fsmul_STK05
	LDA	_expf_STK00
	STA	___fsmul_STK04
	LDA	r0x1158
	STA	___fsmul_STK03
	LDA	_expf_STK02
	STA	___fsmul_STK02
	LDA	_expf_STK01
	STA	___fsmul_STK01
	LDA	_expf_STK00
	STA	___fsmul_STK00
	LDA	r0x1158
	CALL	___fsmul
	STA	r0x115C
	LDA	STK00
	STA	r0x115B
	LDA	STK01
	STA	r0x115A
	LDA	STK02
	STA	r0x1159
;	;.line	365; "expf.c"	r=P(z)*g;
	STA	___fsmul_STK06
	LDA	r0x115A
	STA	___fsmul_STK05
	LDA	r0x115B
	STA	___fsmul_STK04
	LDA	r0x115C
	STA	___fsmul_STK03
	LDA	#0x08
	STA	___fsmul_STK02
	LDA	#0x53
	STA	___fsmul_STK01
	LDA	#0x88
	STA	___fsmul_STK00
	LDA	#0x3b
	CALL	___fsmul
	STA	r0x1163
	CLRA	
	STA	___fsadd_STK06
	STA	___fsadd_STK05
	LDA	#0x80
	STA	___fsadd_STK04
	LDA	#0x3e
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1163
	CALL	___fsadd
	STA	r0x1163
	LDA	_expf_STK02
	STA	___fsmul_STK06
	LDA	_expf_STK01
	STA	___fsmul_STK05
	LDA	_expf_STK00
	STA	___fsmul_STK04
	LDA	r0x1158
	STA	___fsmul_STK03
	LDA	STK02
	STA	___fsmul_STK02
	LDA	STK01
	STA	___fsmul_STK01
	LDA	STK00
	STA	___fsmul_STK00
	LDA	r0x1163
	CALL	___fsmul
	STA	r0x1158
	LDA	STK00
	STA	_expf_STK00
	LDA	STK01
	STA	_expf_STK01
	LDA	STK02
	STA	_expf_STK02
;	;.line	366; "expf.c"	r=0.5+(r/(Q(z)-r));
	LDA	r0x1159
	STA	___fsmul_STK06
	LDA	r0x115A
	STA	___fsmul_STK05
	LDA	r0x115B
	STA	___fsmul_STK04
	LDA	r0x115C
	STA	___fsmul_STK03
	LDA	#0x5b
	STA	___fsmul_STK02
	LDA	#0xbf
	STA	___fsmul_STK01
	LDA	#0x4c
	STA	___fsmul_STK00
	LDA	#0x3d
	CALL	___fsmul
	STA	r0x115C
	CLRA	
	STA	___fsadd_STK06
	STA	___fsadd_STK05
	STA	___fsadd_STK04
	LDA	#0x3f
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x115C
	CALL	___fsadd
	STA	r0x115C
	LDA	_expf_STK02
	STA	___fssub_STK06
	LDA	_expf_STK01
	STA	___fssub_STK05
	LDA	_expf_STK00
	STA	___fssub_STK04
	LDA	r0x1158
	STA	___fssub_STK03
	LDA	STK02
	STA	___fssub_STK02
	LDA	STK01
	STA	___fssub_STK01
	LDA	STK00
	STA	___fssub_STK00
	LDA	r0x115C
	CALL	___fssub
	STA	r0x115C
	LDA	STK02
	STA	___fsdiv_STK06
	LDA	STK01
	STA	___fsdiv_STK05
	LDA	STK00
	STA	___fsdiv_STK04
	LDA	r0x115C
	STA	___fsdiv_STK03
	LDA	_expf_STK02
	STA	___fsdiv_STK02
	LDA	_expf_STK01
	STA	___fsdiv_STK01
	LDA	_expf_STK00
	STA	___fsdiv_STK00
	LDA	r0x1158
	CALL	___fsdiv
	STA	r0x1158
	CLRA	
	STA	___fsadd_STK06
	STA	___fsadd_STK05
	STA	___fsadd_STK04
	LDA	#0x3f
	STA	___fsadd_STK03
	LDA	STK02
	STA	___fsadd_STK02
	LDA	STK01
	STA	___fsadd_STK01
	LDA	STK00
	STA	___fsadd_STK00
	LDA	r0x1158
	CALL	___fsadd
	STA	r0x1158
;	;.line	368; "expf.c"	n++;
	LDA	r0x115E
	INCA	
	STA	r0x115E
	CLRA	
	ADDC	r0x115F
	STA	r0x115F
;	;.line	369; "expf.c"	z=ldexpf(r, n);
	LDA	r0x115E
	STA	_ldexpf_STK04
	LDA	r0x115F
	STA	_ldexpf_STK03
	LDA	STK02
	STA	_ldexpf_STK02
	LDA	STK01
	STA	_ldexpf_STK01
	LDA	STK00
	STA	_ldexpf_STK00
	LDA	r0x1158
	CALL	_ldexpf
	STA	r0x1158
;	;.line	370; "expf.c"	if(sign)
	LDA	r0x115D
	JZ	_00120_DS_
;	;.line	371; "expf.c"	return 1.0/z;
	LDA	STK02
	STA	___fsdiv_STK06
	LDA	STK01
	STA	___fsdiv_STK05
	LDA	STK00
	STA	___fsdiv_STK04
	LDA	r0x1158
	STA	___fsdiv_STK03
	CLRA	
	STA	___fsdiv_STK02
	STA	___fsdiv_STK01
	LDA	#0x80
	STA	___fsdiv_STK00
	LDA	#0x3f
	CALL	___fsdiv
	JMP	_00122_DS_
_00120_DS_:
;	;.line	373; "expf.c"	return z;
	LDA	r0x1158
_00122_DS_:
;	;.line	374; "expf.c"	}
	RET	
; exit point of _expf
	.ENDFUNC _expf
	;--cdb--S:Lexpf.expf$x$65536$25({4}SF:S),R,0,0,[_expf_STK02,_expf_STK01,_expf_STK00,r0x1158]
	;--cdb--S:Lexpf.expf$n$65536$26({2}SI:S),R,0,0,[r0x115E,r0x115F]
	;--cdb--S:Lexpf.expf$xn$65536$26({4}SF:S),R,0,0,[r0x1155,r0x1156,r0x1157,r0x1158]
	;--cdb--S:Lexpf.expf$g$65536$26({4}SF:S),R,0,0,[r0x1155,r0x1156,r0x1157,r0x1158]
	;--cdb--S:Lexpf.expf$r$65536$26({4}SF:S),R,0,0,[r0x1155,r0x1156,r0x1157,r0x1158]
	;--cdb--S:Lexpf.expf$z$65536$26({4}SF:S),R,0,0,[r0x1159,r0x115A,r0x115B,r0x115C]
	;--cdb--S:Lexpf.expf$y$65536$26({4}SF:S),R,0,0,[r0x1159,r0x115A,r0x115B,r0x115C]
	;--cdb--S:Lexpf.expf$sign$65536$26({1}:S),R,0,0,[r0x115D]
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$cotf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$asinf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$acosf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atanf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$atan2f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sinhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$coshf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$tanhf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$logf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$log10f$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$powf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$sqrtf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$fabsf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$frexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ldexpf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$ceilf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$floorf$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$modff$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$isnan$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$isinf$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$errno$0$0({2}SI:S),E,0,0
	;--cdb--S:G$expf$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_ldexpf
	.globl	___fslt
	.globl	___fsmul
	.globl	___fs2sint
	.globl	___sint2fs
	.globl	___fssub
	.globl	___fsadd
	.globl	___fsdiv
	.globl	_errno

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_expf
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_expf_0	udata
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1163:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_expf_STK00:	.ds	1
	.globl _expf_STK00
_expf_STK01:	.ds	1
	.globl _expf_STK01
_expf_STK02:	.ds	1
	.globl _expf_STK02
	.globl ___fslt_STK06
	.globl ___fslt_STK05
	.globl ___fslt_STK04
	.globl ___fslt_STK03
	.globl ___fslt_STK02
	.globl ___fslt_STK01
	.globl ___fslt_STK00
	.globl ___fsmul_STK06
	.globl ___fsmul_STK05
	.globl ___fsmul_STK04
	.globl ___fsmul_STK03
	.globl ___fsmul_STK02
	.globl ___fsmul_STK01
	.globl ___fsmul_STK00
	.globl ___fs2sint_STK02
	.globl ___fs2sint_STK01
	.globl ___fs2sint_STK00
	.globl ___sint2fs_STK00
	.globl ___fssub_STK06
	.globl ___fssub_STK05
	.globl ___fssub_STK04
	.globl ___fssub_STK03
	.globl ___fssub_STK02
	.globl ___fssub_STK01
	.globl ___fssub_STK00
	.globl ___fsadd_STK06
	.globl ___fsadd_STK05
	.globl ___fsadd_STK04
	.globl ___fsadd_STK03
	.globl ___fsadd_STK02
	.globl ___fsadd_STK01
	.globl ___fsadd_STK00
	.globl ___fsdiv_STK06
	.globl ___fsdiv_STK05
	.globl ___fsdiv_STK04
	.globl ___fsdiv_STK03
	.globl ___fsdiv_STK02
	.globl ___fsdiv_STK01
	.globl ___fsdiv_STK00
	.globl _ldexpf_STK04
	.globl _ldexpf_STK03
	.globl _ldexpf_STK02
	.globl _ldexpf_STK01
	.globl _ldexpf_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:_expf_STK00:NULL+0:14:0
	;--cdb--W:_expf_STK01:NULL+0:13:0
	;--cdb--W:_expf_STK02:NULL+0:12:0
	;--cdb--W:r0x1159:NULL+0:12:0
	;--cdb--W:r0x115A:NULL+0:13:0
	;--cdb--W:r0x115B:NULL+0:14:0
	;--cdb--W:r0x1162:NULL+0:14:0
	;--cdb--W:r0x1161:NULL+0:13:0
	;--cdb--W:r0x1160:NULL+0:12:0
	;--cdb--W:r0x1159:NULL+0:-1:1
	;--cdb--W:_expf_STK02:NULL+0:-1:1
	;--cdb--W:r0x115C:NULL+0:-1:1
	end
