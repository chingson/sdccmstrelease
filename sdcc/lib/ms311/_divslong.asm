;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_divslong.c"
	.module _divslong
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_divslong$0$0({2}DF,SL:S),C,0,0
	;--cdb--F:G$_divslong$0$0({2}DF,SL:S),C,0,0,0,0,0
	;--cdb--S:G$_divulong$0$0({2}DF,SL:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _divslong-code 
.globl __divslong

;--------------------------------------------------------
	.FUNC __divslong:$PNUM 8:$C:__divulong\
:$L:r0x1157:$L:__divslong_STK00:$L:__divslong_STK01:$L:__divslong_STK02:$L:__divslong_STK03\
:$L:__divslong_STK04:$L:__divslong_STK05:$L:__divslong_STK06:$L:r0x115C:$L:r0x115D\
:$L:r0x115E:$L:r0x115F:$L:r0x1160:$L:r0x1161
;--------------------------------------------------------
;	.line	2; "_divslong.c"	_divslong (long x, long y)
__divslong:	;Function start
	STA	r0x1157
;	;.line	6; "_divslong.c"	r = (unsigned long)(x < 0 ? -x : x) / (unsigned long)(y < 0 ? -y : y);
	XOR	#0x80
	ROL	
	CLRA	
	ROL	
	XOR	#0x01
	STA	r0x115C
	JZ	_00110_DS_
	SETB	_C
	CLRA	
	SUBB	__divslong_STK02
	STA	r0x115D
	CLRA	
	SUBB	__divslong_STK01
	STA	r0x115E
	CLRA	
	SUBB	__divslong_STK00
	STA	r0x115F
	CLRA	
	SUBB	r0x1157
	STA	r0x1160
	JMP	_00111_DS_
_00110_DS_:
	LDA	__divslong_STK02
	STA	r0x115D
	LDA	__divslong_STK01
	STA	r0x115E
	LDA	__divslong_STK00
	STA	r0x115F
	LDA	r0x1157
	STA	r0x1160
_00111_DS_:
	LDA	r0x115D
	STA	__divslong_STK02
	LDA	r0x115E
	STA	__divslong_STK01
	LDA	r0x115F
	STA	__divslong_STK00
	LDA	r0x1160
	STA	r0x1157
	LDA	__divslong_STK03
	XOR	#0x80
	ROL	
	CLRA	
	ROL	
	XOR	#0x01
	STA	r0x115D
	JZ	_00112_DS_
	SETB	_C
	CLRA	
	SUBB	__divslong_STK06
	STA	r0x115E
	CLRA	
	SUBB	__divslong_STK05
	STA	r0x115F
	CLRA	
	SUBB	__divslong_STK04
	STA	r0x1160
	CLRA	
	SUBB	__divslong_STK03
	STA	r0x1161
	JMP	_00113_DS_
_00112_DS_:
	LDA	__divslong_STK06
	STA	r0x115E
	LDA	__divslong_STK05
	STA	r0x115F
	LDA	__divslong_STK04
	STA	r0x1160
	LDA	__divslong_STK03
	STA	r0x1161
_00113_DS_:
	LDA	r0x115E
	STA	__divulong_STK06
	LDA	r0x115F
	STA	__divulong_STK05
	LDA	r0x1160
	STA	__divulong_STK04
	LDA	r0x1161
	STA	__divulong_STK03
	LDA	__divslong_STK02
	STA	__divulong_STK02
	LDA	__divslong_STK01
	STA	__divulong_STK01
	LDA	__divslong_STK00
	STA	__divulong_STK00
	LDA	r0x1157
	CALL	__divulong
	STA	r0x1157
;	;.line	7; "_divslong.c"	if ((x < 0) ^ (y < 0))
	LDA	r0x115D
	XOR	r0x115C
	JZ	_00106_DS_
;	;.line	8; "_divslong.c"	return -r;
	SETB	_C
	CLRA	
	SUBB	STK02
	STA	r0x115C
	CLRA	
	SUBB	STK01
	STA	__divslong_STK06
	CLRA	
	SUBB	STK00
	STA	__divslong_STK05
	CLRA	
	SUBB	r0x1157
	STA	__divslong_STK04
	LDA	r0x115C
	STA	STK02
	LDA	__divslong_STK06
	STA	STK01
	LDA	__divslong_STK05
	STA	STK00
	LDA	__divslong_STK04
	JMP	_00108_DS_
_00106_DS_:
;	;.line	10; "_divslong.c"	return r;
	LDA	r0x1157
_00108_DS_:
;	;.line	11; "_divslong.c"	}
	RET	
; exit point of __divslong
	.ENDFUNC __divslong
	;--cdb--S:G$_divslong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$_divulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:L_divslong._divslong$y$65536$1({4}SL:S),R,0,0,[__divslong_STK06,__divslong_STK05,__divslong_STK04,__divslong_STK03]
	;--cdb--S:L_divslong._divslong$x$65536$1({4}SL:S),R,0,0,[__divslong_STK02,__divslong_STK01,__divslong_STK00,r0x1157]
	;--cdb--S:L_divslong._divslong$r$65536$2({4}SL:S),R,0,0,[__divslong_STK02,__divslong_STK01,__divslong_STK00,r0x1157]
	;--cdb--S:G$_divslong$0$0({2}DF,SL:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__divulong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__divslong
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__divslong_0	udata
r0x1157:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__divslong_STK00:	.ds	1
	.globl __divslong_STK00
__divslong_STK01:	.ds	1
	.globl __divslong_STK01
__divslong_STK02:	.ds	1
	.globl __divslong_STK02
__divslong_STK03:	.ds	1
	.globl __divslong_STK03
__divslong_STK04:	.ds	1
	.globl __divslong_STK04
__divslong_STK05:	.ds	1
	.globl __divslong_STK05
__divslong_STK06:	.ds	1
	.globl __divslong_STK06
	.globl __divulong_STK06
	.globl __divulong_STK05
	.globl __divulong_STK04
	.globl __divulong_STK03
	.globl __divulong_STK02
	.globl __divulong_STK01
	.globl __divulong_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115C:NULL+0:-1:1
	;--cdb--W:__divslong_STK00:NULL+0:14:0
	;--cdb--W:__divslong_STK01:NULL+0:13:0
	;--cdb--W:__divslong_STK02:NULL+0:12:0
	;--cdb--W:__divslong_STK03:NULL+0:4449:0
	;--cdb--W:__divslong_STK04:NULL+0:4448:0
	;--cdb--W:__divslong_STK05:NULL+0:4447:0
	;--cdb--W:__divslong_STK06:NULL+0:4446:0
	end
