;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_mulchar.c"
	.module _mulchar
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_mulchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$_mulchar$0$0({2}DF,SC:U),C,0,0,0,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _mulchar-code 
.globl __mulchar

;--------------------------------------------------------
	.FUNC __mulchar:$PNUM 2:$L:r0x1154:$L:__mulchar_STK00:$L:r0x1156:$L:r0x1157:$L:r0x1158\

;--------------------------------------------------------
;	.line	33; "_mulchar.c"	_mulchar (char a, char b)
__mulchar:	;Function start
	STA	r0x1154
;	;.line	35; "_mulchar.c"	char  result = 0;
	CLRA	
	STA	r0x1156
;	;.line	39; "_mulchar.c"	for (i = 0; i < 8u; i++) {
	LDA	#0x08
	STA	r0x1157
_00110_DS_:
;	;.line	41; "_mulchar.c"	if (a & 0x01u) result += b;
	LDA	r0x1154
	SHR	
	JNC	_00106_DS_
	LDA	r0x1156
	ADD	__mulchar_STK00
	STA	r0x1156
_00106_DS_:
;	;.line	45; "_mulchar.c"	a = ((unsigned int)a) >> 1u;
	CLRA	
	SHR	
	LDA	r0x1154
	ROR	
	STA	r0x1154
;	;.line	46; "_mulchar.c"	b <<= 1u;
	LDA	__mulchar_STK00
	SHL	
	STA	__mulchar_STK00
	LDA	r0x1157
	DECA	
	STA	r0x1158
	STA	r0x1157
;	;.line	39; "_mulchar.c"	for (i = 0; i < 8u; i++) {
	LDA	r0x1158
	JNZ	_00110_DS_
;	;.line	49; "_mulchar.c"	return result;
	LDA	r0x1156
;	;.line	50; "_mulchar.c"	}
	RET	
; exit point of __mulchar
	.ENDFUNC __mulchar
	;--cdb--S:G$_mulchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:L_mulchar._mulchar$b$65536$1({1}SC:U),R,0,0,[__mulchar_STK00]
	;--cdb--S:L_mulchar._mulchar$a$65536$1({1}SC:U),R,0,0,[r0x1154]
	;--cdb--S:L_mulchar._mulchar$result$65536$2({1}SC:U),R,0,0,[r0x1156]
	;--cdb--S:L_mulchar._mulchar$i$65536$2({1}SC:U),R,0,0,[r0x1157]
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__mulchar
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__mulchar_0	udata
r0x1154:	.ds	1
r0x1156:	.ds	1
r0x1157:	.ds	1
r0x1158:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__mulchar_STK00:	.ds	1
	.globl __mulchar_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115B:NULL+0:-1:1
	;--cdb--W:r0x1158:NULL+0:4436:0
	;--cdb--W:r0x1159:NULL+0:0:0
	;--cdb--W:r0x1159:NULL+0:-1:1
	;--cdb--W:r0x115A:NULL+0:-1:1
	end
