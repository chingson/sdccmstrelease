;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_strcspn.c"
	.module _strcspn
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--F:G$strcspn$0$0({2}DF,SI:U),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _strcspn-code 
.globl _strcspn

;--------------------------------------------------------
	.FUNC _strcspn:$PNUM 4:$C:_strchr\
:$L:r0x1155:$L:_strcspn_STK00:$L:_strcspn_STK01:$L:_strcspn_STK02:$L:r0x1158\
:$L:r0x1159:$L:r0x115A:$L:r0x115B
;--------------------------------------------------------
;	.line	31; "_strcspn.c"	size_t strcspn ( char * string, char * control )
_strcspn:	;Function start
	STA	r0x1155
;	;.line	36; "_strcspn.c"	while (ch = *string) {
	CLRA	
	STA	r0x1158
	STA	r0x1159
_00108_DS_:
	LDA	_strcspn_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115A
	STA	r0x115B
	LDA	r0x115A
	JZ	_00110_DS_
;	;.line	37; "_strcspn.c"	if (strchr(control,ch))
	LDA	r0x115B
	STA	_strchr_STK01
	LDA	_strcspn_STK02
	STA	_strchr_STK00
	LDA	_strcspn_STK01
	CALL	_strchr
	ORA	STK00
	JNZ	_00110_DS_
;	;.line	40; "_strcspn.c"	count++;
	LDA	r0x1158
	INCA	
	STA	r0x1158
	CLRA	
	ADDC	r0x1159
	STA	r0x1159
;	;.line	41; "_strcspn.c"	string++;
	LDA	_strcspn_STK00
	INCA	
	STA	_strcspn_STK00
	CLRA	
	ADDC	r0x1155
	STA	r0x1155
	JMP	_00108_DS_
_00110_DS_:
;	;.line	44; "_strcspn.c"	return count;
	LDA	r0x1158
	STA	STK00
	LDA	r0x1159
;	;.line	45; "_strcspn.c"	}  
	RET	
; exit point of _strcspn
	.ENDFUNC _strcspn
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_strcspn.strcspn$control$65536$21({2}DG,SC:U),R,0,0,[_strcspn_STK02,_strcspn_STK01]
	;--cdb--S:L_strcspn.strcspn$string$65536$21({2}DG,SC:U),R,0,0,[_strcspn_STK00]
	;--cdb--S:L_strcspn.strcspn$count$65536$22({2}SI:U),R,0,0,[r0x1158,r0x1159]
	;--cdb--S:L_strcspn.strcspn$ch$65536$22({1}SC:U),R,0,0,[r0x115B]
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_strchr

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_strcspn
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__strcspn_0	udata
r0x1155:	.ds	1
r0x1158:	.ds	1
r0x1159:	.ds	1
r0x115A:	.ds	1
r0x115B:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_strcspn_STK00:	.ds	1
	.globl _strcspn_STK00
_strcspn_STK01:	.ds	1
	.globl _strcspn_STK01
_strcspn_STK02:	.ds	1
	.globl _strcspn_STK02
	.globl _strchr_STK01
	.globl _strchr_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x115A:NULL+0:14:0
	;--cdb--W:r0x115B:NULL+0:-1:1
	end
