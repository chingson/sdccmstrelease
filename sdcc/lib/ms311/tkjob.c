
#include<ms326.h>
#include <string.h>
#include<ms326sphlib.h>



extern BYTE tch_total_ch;
extern BYTE tch_mon;
extern USHORT *procdatap;
extern touch_entry *rambufp1;
BYTE lastcmd=0;
BYTE cmd_now=0;
BYTE extrd;
USHORT rompsave=0;
BYTE modified=0;
extern unsigned long *baseline;
extern BYTE *threshold_ratio;


BYTE api_tk_job(void)
{
    // copy to the prodcatap
    touch_entry *tkramp;
    USHORT *usp;
    BYTE i;


    if(TOUCHC&0x80 )
    {
	if(  rambufp1[0].count==0)
	{
        TOUCHC=TOUCHC_DEFAULT;
	return 0; // for sync!!
	}
        if(extrd==0)
        {
            tkramp=rambufp1;
            usp=procdatap;
            for(i=0; i<tch_total_ch; i++)
            {
                *usp=tkramp->count-(baseline[i]>>8);
                usp++;
		*usp=(USHORT)((baseline[i]>>8)&0xffff); // get the baseline
                usp++;
                tkramp++;
            }
            extrd=1; // 1 when new data 0 by ext sspi
        }
        modified=1;
        // update baseline
        tkramp=rambufp1;
        for(i=0; i<tch_total_ch; i++)
        {

            unsigned long baselinel = baseline[i];
            unsigned long lowbase = (baselinel-(baselinel>>5))>>8;
            BYTE ratio;

            if(tkramp->count<tkramp->threshold )
            {
                if(tkramp->count<(unsigned short)lowbase)
                    baselinel=((unsigned long)tkramp->count)<<8;
                else
                {
                    baselinel = baselinel - (baselinel>>8);
                    baselinel = baselinel + tkramp->count;
                }
                if(threshold_ratio!=NULL)
                    ratio = threshold_ratio[i];
                else
                    ratio = 0x87; // we take 5%


                tkramp->threshold=(USHORT)((baselinel*((unsigned long) ratio))>>15);
                baseline[i]=baselinel;
            }
            tkramp++;
        }

        rambufp1[0].count=0;
        TOUCHC=TOUCHC_DEFAULT;
    }
    if(SSPIH==lastcmd)
    {
        //api_enter_stdby_mode(0,0,0x08,0,0);
        /*
        if(!sleep_timer)
        {
        PIOA=0xff;
        PAR=0xff;
        PADIR=0;
        PAIF=0;
        TIMERC=0;

        api_enter_dsleep_mode(0);
        }
        */
        return modified;
    }
    ROMPUW=rompsave;
    lastcmd=SSPIH;
    cmd_now=lastcmd&0x1F;
    switch(cmd_now)
    {
    case 1: // read memory, 2 bytes
        SSPIL=ROMPINC;
        SSPIM=ROMPINC;
        //SSPIH=ROMPINC;
        break;
    case 2: // SETPTR .. there are only 2 bytes
        ROMPL=SSPIL;
        ROMPH=SSPIM;
        break;
    case 3: // write mem, inc
        ROMPINC=SSPIM;
        break;

    // hwp seems not working for RAM
    case 4: // READ SFR
        // use RAMCODE
        // ram code at 0x1XX
        // LDA XXX
        // RET
        if(SSPIM==0x7E) //
        {
            SSPIM=HWD;
            break;
        }
        if(SSPIM==0x7f)
        {
            SSPIM=HWDINC;
            break;
        }
        if(SSPIM==0x4E) // spidat
        {
            SPIM=SPIDAT;
        }
        ROMPUW=0x8100;
        ROMPINC=0x02;
        ROMPINC=SSPIM;
        ROMPINC=0xc0;
        PRG_RAM=1;
        __asm
        .DB 0xb0
        .DB 0x00
        .DB 0x12; // sta
        .DB	0x3b; // SPIM
        __endasm;
        PRG_RAM=0;
        break;
    case 5: // write SFR
            if(SSPIM==0x7E)
            {
                HWD=SSPIL;
                break;
            }
        if(SSPIM==0x7F)
        {
            HWDINC=SSPIL;
            break;
        }
        if(SSPIM==0x4e)
        {
            SPIDAT=SSPIL;
            break;
        }
        ROMPUW=0x8100;
        ROMPINC=0x0;
        ROMPINC=SSPIL;
        ROMPINC=0x12; // STA
        ROMPINC=SSPIM;
        ROMPINC=0xc0; // RET
        PRG_RAM=1;
        __asm
        .DB 0xb0
        .DB 0x00
        __endasm;
        PRG_RAM=0;
        break;


    // following is touch commands
    case 6: // read touch regisers
            if(SSPIM==0)
            {
                SSPIL=extrd;
                SSPIM=0x55;
            } else if(SSPIM==1)
            {
                SSPIM=((USHORT)procdatap)>>8;
                SSPIL=((USHORT)procdatap)&0xff;
            } else if(SSPIM==2)
            {
                extrd=0;
                SSPIM=0x66;
                SSPIL=0x33;

            } else if(SSPIM==3)
            {
                SSPIL=PATEN;
                SSPIM=PBTEN;
            } else if(SSPIM==4)
            {
                SSPIL=((USHORT)rambufp1)&0xff;
                SSPIM=((USHORT)rambufp1)>>8;
            } else if(SSPIM==5)
            {
                SSPIL=((USHORT)baseline)&0xff;
                SSPIM=((USHORT)baseline)>>8;
            } else if(SSPIM==6)
            {
                SSPIL=((USHORT)threshold_ratio)&0xff;
                SSPIM=((USHORT)threshold_ratio)>>8;
            } else
            {
                SSPIL=0xCC;
                SSPIM=0xDD;
            }
        break;
    // depends on SSPIM
    case 7:
        SSPIL=0xaa;
        SSPIM=0xbb;
        break;

    default:
        SSPIL=0x12;
        SSPIM=0x34;
        break;

    }
    SSPIH=lastcmd;
    rompsave=ROMPUW;
    return modified;
}



