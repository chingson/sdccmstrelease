;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_divschar.c"
	.module _divschar
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$_divsuchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$_divsuchar$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$_divuschar$0$0({2}DF,SC:S),C,0,0
	;--cdb--F:G$_divuschar$0$0({2}DF,SC:S),C,0,0,0,0,0
	;--cdb--S:G$_divschar$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$_divschar$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$_divsint$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _divschar-code 
.globl __divsuchar

;--------------------------------------------------------
	.FUNC __divsuchar:$PNUM 2:$C:__divsint\
:$L:r0x1163:$L:__divsuchar_STK00:$L:r0x1167
;--------------------------------------------------------
;	.line	43; "_divschar.c"	_divsuchar (signed char x, signed char y)
__divsuchar:	;Function start
	STA	r0x1163
;	;.line	45; "_divschar.c"	return ((int)((unsigned char)x) / (int)y);
	LDA	__divsuchar_STK00
	JPL	_00127_DS_
	LDA	#0xff
	JMP	_00128_DS_
_00127_DS_:
	CLRA	
_00128_DS_:
	STA	r0x1167
	LDA	__divsuchar_STK00
	STA	__divsint_STK02
	LDA	r0x1167
	STA	__divsint_STK01
	LDA	r0x1163
	STA	__divsint_STK00
	CLRA	
	CALL	__divsint
	LDA	STK00
;	;.line	46; "_divschar.c"	}
	RET	
; exit point of __divsuchar
	.ENDFUNC __divsuchar
.globl __divuschar

;--------------------------------------------------------
	.FUNC __divuschar:$PNUM 2:$C:__divsint\
:$L:r0x115D:$L:__divuschar_STK00:$L:r0x1160
;--------------------------------------------------------
;	.line	37; "_divschar.c"	_divuschar (unsigned char x, unsigned char y)
__divuschar:	;Function start
	STA	r0x115D
;	;.line	39; "_divschar.c"	return ((int)((signed char)x) / (int)y);
	JPL	_00117_DS_
	LDA	#0xff
	JMP	_00118_DS_
_00117_DS_:
	CLRA	
_00118_DS_:
	STA	r0x1160
	LDA	__divuschar_STK00
	STA	__divsint_STK02
	CLRA	
	STA	__divsint_STK01
	LDA	r0x115D
	STA	__divsint_STK00
	LDA	r0x1160
	CALL	__divsint
	LDA	STK00
;	;.line	40; "_divschar.c"	}
	RET	
; exit point of __divuschar
	.ENDFUNC __divuschar
.globl __divschar

;--------------------------------------------------------
	.FUNC __divschar:$PNUM 2:$C:__divsint\
:$L:r0x1154:$L:__divschar_STK00:$L:r0x1157:$L:r0x1158
;--------------------------------------------------------
;	.line	31; "_divschar.c"	_divschar (signed char x, signed char y)
__divschar:	;Function start
	STA	r0x1154
;	;.line	33; "_divschar.c"	return ((int)x / (int)y);
	JPL	_00107_DS_
	LDA	#0xff
	JMP	_00108_DS_
_00107_DS_:
	CLRA	
_00108_DS_:
	STA	r0x1157
	LDA	__divschar_STK00
	JPL	_00109_DS_
	LDA	#0xff
	JMP	_00110_DS_
_00109_DS_:
	CLRA	
_00110_DS_:
	STA	r0x1158
	LDA	__divschar_STK00
	STA	__divsint_STK02
	LDA	r0x1158
	STA	__divsint_STK01
	LDA	r0x1154
	STA	__divsint_STK00
	LDA	r0x1157
	CALL	__divsint
;	;.line	34; "_divschar.c"	}
	RET	
; exit point of __divschar
	.ENDFUNC __divschar
	;--cdb--S:G$_divsuchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$_divuschar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$_divschar$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_divsint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:L_divschar._divschar$y$65536$1({1}SC:S),R,0,0,[__divschar_STK00]
	;--cdb--S:L_divschar._divschar$x$65536$1({1}SC:S),R,0,0,[r0x1154]
	;--cdb--S:L_divschar._divuschar$y$65536$3({1}SC:U),R,0,0,[__divuschar_STK00]
	;--cdb--S:L_divschar._divuschar$x$65536$3({1}SC:U),R,0,0,[r0x115D]
	;--cdb--S:L_divschar._divsuchar$y$65536$5({1}SC:S),R,0,0,[__divsuchar_STK00]
	;--cdb--S:L_divschar._divsuchar$x$65536$5({1}SC:S),R,0,0,[r0x1163]
	;--cdb--S:G$_divschar$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$_divuschar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$_divsuchar$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__divsint

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__divsuchar
	.globl	__divuschar
	.globl	__divschar
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__divschar_0	udata
r0x1154:	.ds	1
r0x1157:	.ds	1
r0x1158:	.ds	1
r0x115D:	.ds	1
r0x1160:	.ds	1
r0x1163:	.ds	1
r0x1167:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
__divschar_STK00:	.ds	1
	.globl __divschar_STK00
	.globl __divsint_STK02
	.globl __divsint_STK01
	.globl __divsint_STK00
__divuschar_STK00:	.ds	1
	.globl __divuschar_STK00
__divsuchar_STK00:	.ds	1
	.globl __divsuchar_STK00
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1165:NULL+0:-1:1
	;--cdb--W:__divsuchar_STK00:NULL+0:-1:1
	;--cdb--W:r0x1163:NULL+0:14:0
	;--cdb--W:r0x1165:NULL+0:4456:0
	;--cdb--W:r0x1165:NULL+0:14:0
	;--cdb--W:r0x1166:NULL+0:0:0
	;--cdb--W:r0x1166:NULL+0:-1:1
	;--cdb--W:r0x115F:NULL+0:-1:1
	;--cdb--W:__divuschar_STK00:NULL+0:-1:1
	;--cdb--W:r0x115D:NULL+0:14:0
	;--cdb--W:r0x115F:NULL+0:4450:0
	;--cdb--W:r0x115F:NULL+0:14:0
	;--cdb--W:r0x1161:NULL+0:0:0
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x1154:NULL+0:4441:0
	;--cdb--W:r0x1154:NULL+0:14:0
	;--cdb--W:r0x1156:NULL+0:4436:0
	;--cdb--W:__divschar_STK00:NULL+0:-1:1
	end
