;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_fsadd.c"
	.module _fsadd
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:F_fsadd$float_long[({0}S:S$f$0$0({4}SF:S),Z,0,0)({0}S:S$l$0$0({4}SL:U),Z,0,0)]
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--F:G$__fsadd$0$0({2}DF,SF:S),C,0,0,0,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _fsadd-code 
.globl ___fsadd

;--------------------------------------------------------
	.FUNC ___fsadd:$PNUM 8:$C:__shr_slong\
:$L:r0x1157:$L:___fsadd_STK00:$L:___fsadd_STK01:$L:___fsadd_STK02:$L:___fsadd_STK03\
:$L:___fsadd_STK04:$L:___fsadd_STK05:$L:___fsadd_STK06:$L:r0x115C:$L:r0x115D\
:$L:r0x115E:$L:r0x115F:$L:___fsadd_fl1_65536_21:$L:___fsadd_fl2_65536_21:$L:r0x1160\
:$L:r0x1161:$L:r0x1162:$L:r0x1163:$L:r0x1164:$L:r0x1165\
:$L:r0x1166:$L:r0x1167:$L:r0x1168
;--------------------------------------------------------
;	.line	53; "_fsadd.c"	float __fsadd (float a1, float a2) 
___fsadd:	;Function start
	STA	r0x1157
;	;.line	57; "_fsadd.c"	unsigned long sign = 0;
	CLRA	
	STA	r0x115C
	STA	r0x115D
	STA	r0x115E
	STA	r0x115F
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	60; "_fsadd.c"	fl1.f = a1;
	LDA	___fsadd_STK02
	STA	___fsadd_fl1_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	___fsadd_STK01
	STA	(___fsadd_fl1_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	___fsadd_STK00
	STA	(___fsadd_fl1_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x1157
	STA	(___fsadd_fl1_65536_21 + 3)
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
;	;.line	61; "_fsadd.c"	fl2.f = a2;
	LDA	___fsadd_STK06
	STA	___fsadd_fl2_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	___fsadd_STK05
	STA	(___fsadd_fl2_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	___fsadd_STK04
	STA	(___fsadd_fl2_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	___fsadd_STK03
	STA	(___fsadd_fl2_65536_21 + 3)
;	;.line	64; "_fsadd.c"	if (!fl1.l)
	LDA	___fsadd_fl1_65536_21
	ORA	(___fsadd_fl1_65536_21 + 1)
	ORA	(___fsadd_fl1_65536_21 + 2)
	ORA	(___fsadd_fl1_65536_21 + 3)
	JNZ	_00106_DS_
;	;.line	65; "_fsadd.c"	return (fl2.f);
	LDA	___fsadd_fl2_65536_21
	STA	STK02
	LDA	(___fsadd_fl2_65536_21 + 1)
	STA	STK01
	LDA	(___fsadd_fl2_65536_21 + 2)
	STA	STK00
	LDA	(___fsadd_fl2_65536_21 + 3)
	JMP	_00133_DS_
_00106_DS_:
;	;.line	66; "_fsadd.c"	if (!fl2.l)
	LDA	___fsadd_fl2_65536_21
	ORA	(___fsadd_fl2_65536_21 + 1)
	ORA	(___fsadd_fl2_65536_21 + 2)
	ORA	(___fsadd_fl2_65536_21 + 3)
	JNZ	_00108_DS_
;	;.line	67; "_fsadd.c"	return (fl1.f);
	LDA	___fsadd_fl1_65536_21
	STA	STK02
	LDA	(___fsadd_fl1_65536_21 + 1)
	STA	STK01
	LDA	(___fsadd_fl1_65536_21 + 2)
	STA	STK00
	LDA	(___fsadd_fl1_65536_21 + 3)
	JMP	_00133_DS_
_00108_DS_:
;	;.line	69; "_fsadd.c"	exp1 = EXP (fl1.l);
	LDA	(___fsadd_fl1_65536_21 + 2)
	ROL	
	LDA	(___fsadd_fl1_65536_21 + 3)
	ROL	
	CLRB	_C
	STA	___fsadd_STK02
	CLRA	
	STA	___fsadd_STK01
;	;.line	70; "_fsadd.c"	exp2 = EXP (fl2.l);
	LDA	(___fsadd_fl2_65536_21 + 2)
	ROL	
	LDA	(___fsadd_fl2_65536_21 + 3)
	ROL	
	CLRB	_C
;	;.line	72; "_fsadd.c"	if (exp1 > exp2 + 25)
	STA	___fsadd_STK06
	ADD	#0x19
	STA	r0x1160
	CLRA	
	ROL	
	STA	r0x1161
	SETB	_C
	LDA	r0x1160
	SUBB	___fsadd_STK02
	LDA	r0x1161
	SUBSI	
	SUBB	___fsadd_STK01
	JC	_00110_DS_
;	;.line	73; "_fsadd.c"	return (fl1.f);
	LDA	___fsadd_fl1_65536_21
	STA	STK02
	LDA	(___fsadd_fl1_65536_21 + 1)
	STA	STK01
	LDA	(___fsadd_fl1_65536_21 + 2)
	STA	STK00
	LDA	(___fsadd_fl1_65536_21 + 3)
	JMP	_00133_DS_
_00110_DS_:
;	;.line	74; "_fsadd.c"	if (exp2 > exp1 + 25)
	LDA	#0x19
	ADD	___fsadd_STK02
	STA	r0x1160
	CLRA	
	ADDC	___fsadd_STK01
	STA	r0x1161
	SETB	_C
	LDA	r0x1160
	SUBB	___fsadd_STK06
	LDA	r0x1161
	SUBSI	
	SUBB	#0x00
	JC	_00112_DS_
;	;.line	75; "_fsadd.c"	return (fl2.f);
	LDA	___fsadd_fl2_65536_21
	STA	STK02
	LDA	(___fsadd_fl2_65536_21 + 1)
	STA	STK01
	LDA	(___fsadd_fl2_65536_21 + 2)
	STA	STK00
	LDA	(___fsadd_fl2_65536_21 + 3)
	JMP	_00133_DS_
_00112_DS_:
;	;.line	77; "_fsadd.c"	mant1 = MANT (fl1.l);
	LDA	___fsadd_fl1_65536_21
	STA	r0x1160
	LDA	(___fsadd_fl1_65536_21 + 1)
	STA	r0x1161
	LDA	#0x7f
	AND	(___fsadd_fl1_65536_21 + 2)
	STA	r0x1162
	CLRA	
	STA	r0x1163
	LDA	r0x1162
	ORA	#0x80
	STA	r0x1162
;	;.line	78; "_fsadd.c"	mant2 = MANT (fl2.l);
	LDA	___fsadd_fl2_65536_21
	STA	r0x1164
	LDA	(___fsadd_fl2_65536_21 + 1)
	STA	r0x1165
	LDA	#0x7f
	AND	(___fsadd_fl2_65536_21 + 2)
	STA	r0x1166
	CLRA	
	STA	r0x1167
	LDA	r0x1166
	ORA	#0x80
	STA	r0x1166
;	;.line	80; "_fsadd.c"	if (SIGN (fl1.l))
	LDA	(___fsadd_fl1_65536_21 + 3)
	AND	#0x80
	DECA	
	CLRA	
	ROL	
	JZ	_00114_DS_
;	;.line	81; "_fsadd.c"	mant1 = -mant1;
	SETB	_C
	CLRA	
	SUBB	r0x1160
	STA	r0x1160
	CLRA	
	SUBB	r0x1161
	STA	r0x1161
	CLRA	
	SUBB	r0x1162
	STA	r0x1162
	CLRA	
	SUBB	r0x1163
	STA	r0x1163
_00114_DS_:
;	;.line	82; "_fsadd.c"	if (SIGN (fl2.l))
	LDA	(___fsadd_fl2_65536_21 + 3)
	AND	#0x80
	DECA	
	CLRA	
	ROL	
	JZ	_00116_DS_
;	;.line	83; "_fsadd.c"	mant2 = -mant2;
	SETB	_C
	CLRA	
	SUBB	r0x1164
	STA	r0x1164
	CLRA	
	SUBB	r0x1165
	STA	r0x1165
	CLRA	
	SUBB	r0x1166
	STA	r0x1166
	CLRA	
	SUBB	r0x1167
	STA	r0x1167
_00116_DS_:
;	;.line	85; "_fsadd.c"	if (exp1 > exp2)
	SETB	_C
	LDA	___fsadd_STK06
	SUBB	___fsadd_STK02
	CLRA	
	SUBSI	
	SUBB	___fsadd_STK01
	JC	_00118_DS_
;	;.line	87; "_fsadd.c"	mant2 >>= exp1 - exp2;
	SETB	_C
	LDA	___fsadd_STK02
	SUBB	___fsadd_STK06
	STA	r0x1168
	LDA	r0x1167
	STA	STK00
	LDA	r0x1166
	STA	STK01
	LDA	r0x1165
	STA	STK02
	LDA	r0x1164
	STA	_PTRCL
	LDA	r0x1168
	CALL	__shr_slong
	LDA	STK00
	STA	r0x1167
	LDA	STK01
	STA	r0x1166
	LDA	STK02
	STA	r0x1165
	LDA	_PTRCL
	STA	r0x1164
	JMP	_00119_DS_
_00118_DS_:
;	;.line	91; "_fsadd.c"	mant1 >>= exp2 - exp1;
	SETB	_C
	LDA	___fsadd_STK06
	SUBB	___fsadd_STK02
	STA	r0x1168
	LDA	r0x1163
	STA	STK00
	LDA	r0x1162
	STA	STK01
	LDA	r0x1161
	STA	STK02
	LDA	r0x1160
	STA	_PTRCL
	LDA	r0x1168
	CALL	__shr_slong
	LDA	STK00
	STA	r0x1163
	LDA	STK01
	STA	r0x1162
	LDA	STK02
	STA	r0x1161
	LDA	_PTRCL
	STA	r0x1160
;	;.line	92; "_fsadd.c"	exp1 = exp2;
	LDA	___fsadd_STK06
	STA	___fsadd_STK02
	CLRA	
	STA	___fsadd_STK01
_00119_DS_:
;	;.line	94; "_fsadd.c"	mant1 += mant2;
	LDA	r0x1160
	ADD	r0x1164
	STA	r0x1160
	LDA	r0x1161
	ADDC	r0x1165
	STA	r0x1161
	LDA	r0x1162
	ADDC	r0x1166
	STA	___fsadd_STK06
	LDA	r0x1163
	ADDC	r0x1167
	STA	___fsadd_STK05
;	;.line	96; "_fsadd.c"	if (mant1 < 0)
	JPL	_00123_DS_
;	;.line	98; "_fsadd.c"	mant1 = -mant1;
	SETB	_C
	CLRA	
	SUBB	r0x1160
	STA	r0x1160
	CLRA	
	SUBB	r0x1161
	STA	r0x1161
	CLRA	
	SUBB	___fsadd_STK06
	STA	___fsadd_STK06
	CLRA	
	SUBB	___fsadd_STK05
	STA	___fsadd_STK05
;	;.line	99; "_fsadd.c"	sign = SIGNBIT;
	CLRA	
	STA	r0x115C
	STA	r0x115D
	STA	r0x115E
	LDA	#0x80
	STA	r0x115F
	JMP	_00125_DS_
_00123_DS_:
;	;.line	101; "_fsadd.c"	else if (!mant1)
	LDA	r0x1160
	ORA	r0x1161
	ORA	___fsadd_STK06
	ORA	___fsadd_STK05
	JNZ	_00125_DS_
;	;.line	102; "_fsadd.c"	return (0);
	CLRA	
	STA	STK02
	STA	STK01
	STA	STK00
	JMP	_00133_DS_
_00125_DS_:
;	;.line	105; "_fsadd.c"	while (mant1<HIDDEN) {
	LDA	r0x1160
	CLRB	_C
	CLRA	
	ADDC	r0x1161
	LDA	___fsadd_STK06
	ADDC	#0x80
	LDA	___fsadd_STK05
	ADDC	#0xff
	JC	_00130_DS_
;	;.line	106; "_fsadd.c"	mant1 <<= 1;
	LDA	r0x1160
	SHL	
	STA	r0x1160
	LDA	r0x1161
	ROL	
	STA	r0x1161
	LDA	___fsadd_STK06
	ROL	
	STA	___fsadd_STK06
	LDA	___fsadd_STK05
	ROL	
	STA	___fsadd_STK05
;	;.line	107; "_fsadd.c"	exp1--;
	LDA	___fsadd_STK02
	DECA	
	STA	___fsadd_STK02
	LDA	#0xff
	ADDC	___fsadd_STK01
	STA	___fsadd_STK01
	JMP	_00125_DS_
_00130_DS_:
;	;.line	111; "_fsadd.c"	while (mant1 & 0xff000000) {
	LDA	___fsadd_STK05
	JZ	_00132_DS_
;	;.line	112; "_fsadd.c"	if (mant1&1)
	LDA	r0x1160
	SHR	
	JNC	_00129_DS_
;	;.line	113; "_fsadd.c"	mant1 += 2;
	LDA	#0x02
	ADD	r0x1160
	STA	r0x1160
	CLRA	
	ADDC	r0x1161
	STA	r0x1161
	CLRA	
	ADDC	___fsadd_STK06
	STA	___fsadd_STK06
	CLRA	
	ADDC	___fsadd_STK05
	STA	___fsadd_STK05
_00129_DS_:
;	;.line	114; "_fsadd.c"	mant1 >>= 1 ;
	LDA	___fsadd_STK05
	SHRS	
	STA	___fsadd_STK05
	LDA	___fsadd_STK06
	ROR	
	STA	___fsadd_STK06
	LDA	r0x1161
	ROR	
	STA	r0x1161
	LDA	r0x1160
	ROR	
	STA	r0x1160
;	;.line	115; "_fsadd.c"	exp1++;
	LDA	___fsadd_STK02
	INCA	
	STA	___fsadd_STK02
	CLRA	
	ADDC	___fsadd_STK01
	STA	___fsadd_STK01
	JMP	_00130_DS_
_00132_DS_:
;	;.line	119; "_fsadd.c"	mant1 &= ~HIDDEN;
	LDA	#0x7f
	AND	___fsadd_STK06
	STA	___fsadd_STK06
;	;.line	122; "_fsadd.c"	fl1.l = PACK (sign, (unsigned long)( exp1), mant1);
	LDA	___fsadd_STK01
	ROR	
	LDA	___fsadd_STK02
	ROR	
	STA	r0x1167
	CLRA	
	ROR	
	ORA	r0x115E
	STA	r0x115E
	LDA	r0x1167
	ORA	r0x115F
	STA	r0x115F
	LDA	r0x1160
	ORA	r0x115C
	STA	r0x115C
	LDA	r0x1161
	ORA	r0x115D
	STA	r0x115D
	LDA	___fsadd_STK06
	ORA	r0x115E
	STA	r0x115E
	LDA	___fsadd_STK05
	ORA	r0x115F
	STA	r0x115F
;;gen.c:9256: size=3, offset=0, AOP_TYPE(res)=9
	LDA	r0x115C
	STA	___fsadd_fl1_65536_21
;;gen.c:9256: size=2, offset=1, AOP_TYPE(res)=9
	LDA	r0x115D
	STA	(___fsadd_fl1_65536_21 + 1)
;;gen.c:9256: size=1, offset=2, AOP_TYPE(res)=9
	LDA	r0x115E
	STA	(___fsadd_fl1_65536_21 + 2)
;;gen.c:9256: size=0, offset=3, AOP_TYPE(res)=9
	LDA	r0x115F
	STA	(___fsadd_fl1_65536_21 + 3)
;	;.line	124; "_fsadd.c"	return (fl1.f);
	LDA	___fsadd_fl1_65536_21
	STA	STK02
	LDA	(___fsadd_fl1_65536_21 + 1)
	STA	STK01
	LDA	(___fsadd_fl1_65536_21 + 2)
	STA	STK00
	LDA	(___fsadd_fl1_65536_21 + 3)
_00133_DS_:
;	;.line	125; "_fsadd.c"	}
	RET	
; exit point of ___fsadd
	.ENDFUNC ___fsadd
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uchar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__schar2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__uint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__sint2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__ulong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__slong2fs$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fs2uchar$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fs2schar$0$0({2}DF,SC:S),C,0,0
	;--cdb--S:G$__fs2uint$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$__fs2sint$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$__fs2ulong$0$0({2}DF,SL:U),C,0,0
	;--cdb--S:G$__fs2slong$0$0({2}DF,SL:S),C,0,0
	;--cdb--S:G$__fssub$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsmul$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fsdiv$0$0({2}DF,SF:S),C,0,0
	;--cdb--S:G$__fslt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fseq$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$__fsgt$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:L_fsadd.__fsadd$a2$65536$20({4}SF:S),R,0,0,[___fsadd_STK06,___fsadd_STK05,___fsadd_STK04,___fsadd_STK03]
	;--cdb--S:L_fsadd.__fsadd$a1$65536$20({4}SF:S),R,0,0,[___fsadd_STK02,___fsadd_STK01,___fsadd_STK00,r0x1157]
	;--cdb--S:L_fsadd.__fsadd$mant1$65536$21({4}SL:S),R,0,0,[r0x1160,r0x1161,___fsadd_STK06,___fsadd_STK05]
	;--cdb--S:L_fsadd.__fsadd$mant2$65536$21({4}SL:S),R,0,0,[r0x1164,r0x1165,r0x1166,r0x1167]
	;--cdb--S:L_fsadd.__fsadd$exp1$65536$21({2}SI:S),R,0,0,[___fsadd_STK02,___fsadd_STK01]
	;--cdb--S:L_fsadd.__fsadd$exp2$65536$21({2}SI:S),R,0,0,[___fsadd_STK06,___fsadd_STK05]
	;--cdb--S:L_fsadd.__fsadd$sign$65536$21({4}SL:U),R,0,0,[r0x115C,r0x115D,r0x115E,r0x115F]
	;--cdb--S:L_fsadd.__fsadd$fl1$65536$21({4}STfloat_long:S),E,0,0
	;--cdb--S:L_fsadd.__fsadd$fl2$65536$21({4}STfloat_long:S),E,0,0
	;--cdb--S:G$__fsadd$0$0({2}DF,SF:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__shr_slong

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	___fsadd
	.globl	___fsadd_fl1_65536_21
	.globl	___fsadd_fl2_65536_21
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
	.area DSEG(DATA)
___fsadd_fl1_65536_21:	.ds	4

	.area DSEG(DATA)
___fsadd_fl2_65536_21:	.ds	4

;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__fsadd_0	udata
r0x1157:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
r0x115E:	.ds	1
r0x115F:	.ds	1
r0x1160:	.ds	1
r0x1161:	.ds	1
r0x1162:	.ds	1
r0x1163:	.ds	1
r0x1164:	.ds	1
r0x1165:	.ds	1
r0x1166:	.ds	1
r0x1167:	.ds	1
r0x1168:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
___fsadd_STK00:	.ds	1
	.globl ___fsadd_STK00
___fsadd_STK01:	.ds	1
	.globl ___fsadd_STK01
___fsadd_STK02:	.ds	1
	.globl ___fsadd_STK02
___fsadd_STK03:	.ds	1
	.globl ___fsadd_STK03
___fsadd_STK04:	.ds	1
	.globl ___fsadd_STK04
___fsadd_STK05:	.ds	1
	.globl ___fsadd_STK05
___fsadd_STK06:	.ds	1
	.globl ___fsadd_STK06
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:___fsadd_STK02:NULL+0:-1:1
	;--cdb--W:___fsadd_STK01:NULL+0:-1:1
	;--cdb--W:___fsadd_STK04:NULL+0:-1:1
	;--cdb--W:___fsadd_STK03:NULL+0:-1:1
	;--cdb--W:___fsadd_STK06:NULL+0:-1:1
	;--cdb--W:___fsadd_STK05:NULL+0:-1:1
	;--cdb--W:r0x1162:NULL+0:-1:1
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x1167:NULL+0:-1:1
	;--cdb--W:r0x1168:NULL+0:-1:1
	;--cdb--W:r0x1169:NULL+0:-1:1
	;--cdb--W:r0x116A:NULL+0:-1:1
	;--cdb--W:r0x1164:NULL+0:-1:1
	;--cdb--W:r0x1165:NULL+0:-1:1
	;--cdb--W:___fsadd_STK01:NULL+0:0:0
	;--cdb--W:___fsadd_STK01:NULL+0:4449:0
	;--cdb--W:___fsadd_STK02:NULL+0:0:0
	;--cdb--W:___fsadd_STK02:NULL+0:4448:0
	;--cdb--W:r0x1162:NULL+0:4448:0
	;--cdb--W:r0x1162:NULL+0:4463:0
	;--cdb--W:r0x1162:NULL+0:4467:0
	;--cdb--W:r0x1163:NULL+0:4449:0
	;--cdb--W:r0x1163:NULL+0:4462:0
	;--cdb--W:r0x1163:NULL+0:4466:0
	;--cdb--W:r0x1164:NULL+0:4467:0
	;--cdb--W:r0x1165:NULL+0:4466:0
	;--cdb--W:r0x1168:NULL+0:4463:0
	;--cdb--W:r0x1168:NULL+0:4467:0
	;--cdb--W:r0x1169:NULL+0:4467:0
	;--cdb--W:r0x1169:NULL+0:4463:0
	;--cdb--W:r0x1161:NULL+0:-1:1
	;--cdb--W:r0x1160:NULL+0:-1:1
	;--cdb--W:r0x116C:NULL+0:-1:1
	;--cdb--W:___fsadd_STK05:NULL+0:0:0
	;--cdb--W:r0x1166:NULL+0:-1:1
	end
