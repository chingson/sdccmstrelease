#include "ms326sphlib.h"


void stdbynow(void)
{
    SYSC|=SYSC_BIT_STDBY;
}
void api_enter_stdby_mode(BYTE newpawk, BYTE newpawkdr, BYTE newpbwk, BYTE newpbwkdr, BYTE timeroff)
{
    if(timeroff)
        api_timer_off();
    PAWKDR=newpawkdr; // DIR set first!!
    PBWKDR=newpbwkdr;

    PAWK=newpawk;
    PBWK=newpbwk;

    stdbynow();
    if(timeroff)
        TIMERC=1;// timer back, no clear!!
}
