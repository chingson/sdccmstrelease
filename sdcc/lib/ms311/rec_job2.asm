;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.3 #bbc754a74 (MSVC)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"D:\common\ms311sphlib"
;;	.file	"rec_job2.c"
	.module rec_job2
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--T:Frec_job2$adps[({0}S:S$predict$0$0({2}SI:S),Z,0,0)({2}S:S$index$0$0({1}SC:U),Z,0,0)]
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--F:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0,0,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--F:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0,0,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_start_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_stop_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_b12_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
; *** BIT REGION *** (pseudo)
	.area UDATA (DATA,REL,CON)
; *** BIT REGION ***END
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; rec_job2-code 
.globl _api_play_start_with_state

;--------------------------------------------------------
	.FUNC _api_play_start_with_state:$PNUM 10:$C:_api_clear_filter_mem:$C:_api_u2l_page\
:$L:r0x117F:$L:_api_play_start_with_state_STK00:$L:_api_play_start_with_state_STK01:$L:_api_play_start_with_state_STK02:$L:_api_play_start_with_state_STK03\
:$L:_api_play_start_with_state_STK04:$L:_api_play_start_with_state_STK05:$L:_api_play_start_with_state_STK06:$L:_api_play_start_with_state_STK07:$L:_api_play_start_with_state_STK08\

;--------------------------------------------------------
;	.line	82; "rec_job2.c"	BYTE api_play_start_with_state(USHORT start_page, USHORT end_page, USHORT period, BYTE ulaw, BYTE dacosr, adpcm_state * the_state)
_api_play_start_with_state:	;Function start
	STA	r0x117F
	LDA	_api_play_start_with_state_STK08
;	;.line	84; "rec_job2.c"	api_clear_filter_mem();
	CALL	_api_clear_filter_mem
;	;.line	85; "rec_job2.c"	SPIH=start_page>>8;
	LDA	r0x117F
	STA	_SPIH
;	;.line	86; "rec_job2.c"	SPIM=start_page&0xff;
	LDA	_api_play_start_with_state_STK00
	STA	_SPIM
;	;.line	87; "rec_job2.c"	SPIL=0;
	CLRA	
	STA	_SPIL
;	;.line	88; "rec_job2.c"	api_endpage=end_page;
	LDA	_api_play_start_with_state_STK02
	STA	_api_endpage
	LDA	_api_play_start_with_state_STK01
	STA	(_api_endpage + 1)
;	;.line	89; "rec_job2.c"	api_ulaw=ulaw;
	LDA	_api_play_start_with_state_STK05
	STA	_api_ulaw
;	;.line	91; "rec_job2.c"	SPIOP=0x10; // clear end-code
	LDA	#0x10
	STA	_SPIOP
;	;.line	92; "rec_job2.c"	DMAH=0x40;
	LDA	#0x40
	STA	_DMAH
;	;.line	93; "rec_job2.c"	ULAWC&=0x3f;
	DECA	
	AND	_ULAWC
	STA	_ULAWC
;	;.line	94; "rec_job2.c"	ADCON=0x40;
	LDA	#0x40
	STA	_ADCON
;	;.line	95; "rec_job2.c"	DAC_PH=period>>8;
	LDA	_api_play_start_with_state_STK03
	STA	_DAC_PH
;	;.line	96; "rec_job2.c"	DAC_PL=period&0xff;
	LDA	_api_play_start_with_state_STK04
	STA	_DAC_PL
;	;.line	97; "rec_job2.c"	if(ulaw & 1) // ulaw need no state
	LDA	_api_play_start_with_state_STK05
	SHR	
	JNC	_00146_DS_
;	;.line	99; "rec_job2.c"	SPIOP=0x48; // read to 0x1xx
	LDA	#0x48
	STA	_SPIOP
;	;.line	100; "rec_job2.c"	SPIOP=0x20;
	LDA	#0x20
	STA	_SPIOP
;	;.line	101; "rec_job2.c"	if(SPIOP&0x80)
	LDA	_SPIOP
	JPL	_00142_DS_
;	;.line	104; "rec_job2.c"	return 0; // fail
	CLRA	
	JMP	_00151_DS_
_00142_DS_:
;	;.line	106; "rec_job2.c"	api_u2l_page(0);
	CLRA	
	CALL	_api_u2l_page
;	;.line	107; "rec_job2.c"	api_u2l_page(1);
	LDA	#0x01
	CALL	_api_u2l_page
;	;.line	112; "rec_job2.c"	DACON=dacosr|0x1a;
	LDA	_api_play_start_with_state_STK06
	ORA	#0x1a
	STA	_DACON
	JMP	_00147_DS_
_00146_DS_:
;	;.line	116; "rec_job2.c"	ADP_VPLH=the_state->predict;
	LDA	_api_play_start_with_state_STK08
	STA	_ROMPL
	LDA	_api_play_start_with_state_STK07
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_ADP_VPLH
	LDA	@_ROMPINC
	STA	(_ADP_VPLH + 1)
;	;.line	117; "rec_job2.c"	ADP_IND=the_state->index;
	LDA	#0x02
	ADD	_api_play_start_with_state_STK08
	STA	_api_play_start_with_state_STK08
	CLRA	
	ADDC	_api_play_start_with_state_STK07
	STA	_api_play_start_with_state_STK07
	LDA	_api_play_start_with_state_STK08
	STA	_ROMPL
	LDA	_api_play_start_with_state_STK07
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_ADP_IND
;	;.line	118; "rec_job2.c"	SPIOP=0x88; // read page to 0x2xx
	LDA	#0x88
	STA	_SPIOP
;	;.line	119; "rec_job2.c"	SPIOP=0x20; // address increase a page
	LDA	#0x20
	STA	_SPIOP
;	;.line	120; "rec_job2.c"	SPIOP=0xc8; // read page to 0x3xx
	LDA	#0xc8
	STA	_SPIOP
;	;.line	121; "rec_job2.c"	if(SPIOP&0x80)
	LDA	_SPIOP
	JPL	_00144_DS_
;	;.line	123; "rec_job2.c"	return 0;
	CLRA	
	JMP	_00151_DS_
_00144_DS_:
;	;.line	125; "rec_job2.c"	DACON=dacosr|0x7a; // load state
	LDA	_api_play_start_with_state_STK06
	ORA	#0x7a
	STA	_DACON
;	;.line	126; "rec_job2.c"	DACON=dacosr|0x3a;
	LDA	_api_play_start_with_state_STK06
	ORA	#0x3a
	STA	_DACON
_00147_DS_:
;	;.line	129; "rec_job2.c"	SPIOP=0x20;// post increase
	LDA	#0x20
	STA	_SPIOP
_00148_DS_:
;	;.line	130; "rec_job2.c"	while(DMAL==0);
	LDA	_DMAL
	JZ	_00148_DS_
;	;.line	131; "rec_job2.c"	DACON|=1; // enable PA finally
	LDA	_DACON
	ORA	#0x01
	STA	_DACON
;	;.line	132; "rec_job2.c"	api_rampage=2; // 2xx~3xx
	LDA	#0x02
	STA	_api_rampage
;	;.line	133; "rec_job2.c"	return 1;	
	DECA	
_00151_DS_:
;	;.line	134; "rec_job2.c"	}
	RET	
; exit point of _api_play_start_with_state
	.ENDFUNC _api_play_start_with_state
.globl _api_rec_job_do_write

;--------------------------------------------------------
	.FUNC _api_rec_job_do_write:$PNUM 4:$L:r0x116A:$L:_api_rec_job_do_write_STK00:$L:_api_rec_job_do_write_STK01:$L:_api_rec_job_do_write_STK02:$L:r0x116D\

;--------------------------------------------------------
;	.line	47; "rec_job2.c"	BYTE api_rec_job_do_write( USHORT * pwr, USHORT *endpage)
_api_rec_job_do_write:	;Function start
	STA	r0x116A
;	;.line	50; "rec_job2.c"	if(DMAH!=api_rampage)
	LDA	_api_rampage
	XOR	_DMAH
	JZ	_00135_DS_
_00126_DS_:
;	;.line	53; "rec_job2.c"	SPIOP=1;
	LDA	#0x01
	STA	_SPIOP
;	;.line	54; "rec_job2.c"	SPIOP=(api_rampage<<6)|0x4;// write this page
	LDA	_api_rampage
	SWA	
	AND	#0xf0
	SHL	
	SHL	
	ORA	#0x04
	STA	_SPIOP
;	;.line	55; "rec_job2.c"	SPIOP=0x20; // increase a page
	LDA	#0x20
	STA	_SPIOP
;	;.line	56; "rec_job2.c"	halt_page=api_rampage;
	LDA	_api_rampage
	STA	r0x116D
;	;.line	58; "rec_job2.c"	api_rampage=ms311_nextrecpage[api_rampage];
	LDA	_api_rampage
	LDAT	#(_ms311_nextrecpage + 0)
	STA	_api_rampage
;	;.line	60; "rec_job2.c"	if(SPIM==(BYTE)(api_endpage&0xff) && SPIH==(BYTE)(api_endpage>>8))
	LDA	_api_endpage
	XOR	_SPIM
	JNZ	_00128_DS_
	LDA	(_api_endpage + 1)
	XOR	_SPIH
	JNZ	_00128_DS_
;	;.line	62; "rec_job2.c"	*endpage=api_endpage;
	LDA	_api_rec_job_do_write_STK01
	STA	_ROMPH
	LDA	_api_rec_job_do_write_STK02
	STA	_ROMPL
	LDA	_api_endpage
	STA	@_ROMPINC
	LDA	(_api_endpage + 1)
	STA	@_ROMPINC
;	;.line	63; "rec_job2.c"	return 0;
	CLRA	
	JMP	_00136_DS_
_00128_DS_:
;	;.line	65; "rec_job2.c"	if(!(SPIM&0xf))
	LDA	_SPIM
	AND	#0x0f
	JNZ	_00131_DS_
;	;.line	67; "rec_job2.c"	ADCON|=halt_page<<3;
	LDA	r0x116D
	SHL	
	SHL	
	SHL	
	ORA	_ADCON
	STA	_ADCON
;	;.line	68; "rec_job2.c"	SPIOP=1;
	LDA	#0x01
	STA	_SPIOP
;	;.line	69; "rec_job2.c"	SPIOP=2;
	INCA	
	STA	_SPIOP
;	;.line	70; "rec_job2.c"	ADCON&=0xe7;
	LDA	#0xe7
	AND	_ADCON
	STA	_ADCON
_00131_DS_:
;	;.line	72; "rec_job2.c"	if(DMAH!=api_rampage)
	LDA	_api_rampage
	XOR	_DMAH
	JNZ	_00126_DS_
;	;.line	74; "rec_job2.c"	*pwr=PWRHL; // 2 bytes !!
	LDA	r0x116A
	STA	_ROMPH
	LDA	_api_rec_job_do_write_STK00
	STA	_ROMPL
	LDA	_PWRHL
	STA	@_ROMPINC
	LDA	(_PWRHL + 1)
	STA	@_ROMPINC
;	;.line	75; "rec_job2.c"	*endpage=SPIMH;
	LDA	_api_rec_job_do_write_STK01
	STA	_ROMPH
	LDA	_api_rec_job_do_write_STK02
	STA	_ROMPL
	LDA	_SPIMH
	STA	@_ROMPINC
	LDA	(_SPIMH + 1)
	STA	@_ROMPINC
;	;.line	76; "rec_job2.c"	return 2;
	LDA	#0x02
	JMP	_00136_DS_
_00135_DS_:
;	;.line	78; "rec_job2.c"	return 1;
	LDA	#0x01
_00136_DS_:
;	;.line	79; "rec_job2.c"	}
	RET	
; exit point of _api_rec_job_do_write
	.ENDFUNC _api_rec_job_do_write
.globl _api_rec_write_prev

;--------------------------------------------------------
	.FUNC _api_rec_write_prev:$PNUM 1:$L:r0x1163:$L:r0x1166
;--------------------------------------------------------
;	.line	30; "rec_job2.c"	void api_rec_write_prev(BYTE pagenum)
_api_rec_write_prev:	;Function start
	STA	r0x1163
;	;.line	32; "rec_job2.c"	BYTE page_to_write=ms311_nextrecpage[api_rampage];
	LDA	_api_rampage
	LDAT	#(_ms311_nextrecpage + 0)
	STA	r0x1166
;	;.line	34; "rec_job2.c"	if(pagenum==1)
	LDA	r0x1163
	XOR	#0x01
	JNZ	_00118_DS_
;	;.line	35; "rec_job2.c"	page_to_write=ms311_nextrecpage[page_to_write];
	LDA	r0x1166
	LDAT	#(_ms311_nextrecpage + 0)
	STA	r0x1166
_00118_DS_:
;	;.line	36; "rec_job2.c"	while(page_to_write!=api_rampage)
	LDA	_api_rampage
	XOR	r0x1166
	JZ	_00121_DS_
;	;.line	38; "rec_job2.c"	SPIOP=1;
	LDA	#0x01
	STA	_SPIOP
;	;.line	39; "rec_job2.c"	SPIOP=(page_to_write<<6)|4;
	LDA	r0x1166
	SWA	
	AND	#0xf0
	SHL	
	SHL	
	ORA	#0x04
	STA	_SPIOP
;	;.line	40; "rec_job2.c"	SPIOP=0x20;
	LDA	#0x20
	STA	_SPIOP
;	;.line	41; "rec_job2.c"	page_to_write=ms311_nextrecpage[page_to_write];
	LDA	r0x1166
	LDAT	#(_ms311_nextrecpage + 0)
	STA	r0x1166
	JMP	_00118_DS_
_00121_DS_:
;	;.line	46; "rec_job2.c"	}
	RET	
; exit point of _api_rec_write_prev
	.ENDFUNC _api_rec_write_prev
.globl _api_rec_job_no_write

;--------------------------------------------------------
	.FUNC _api_rec_job_no_write:$PNUM 4:$L:r0x1156:$L:_api_rec_job_no_write_STK00:$L:_api_rec_job_no_write_STK01:$L:_api_rec_job_no_write_STK02:$L:r0x1159\

;--------------------------------------------------------
;	.line	12; "rec_job2.c"	BYTE api_rec_job_no_write(adpcm_state *the_state, USHORT *pwr)
_api_rec_job_no_write:	;Function start
	STA	r0x1156
;	;.line	16; "rec_job2.c"	if(DMAH==api_rampage)
	LDA	_api_rampage
	XOR	_DMAH
	JNZ	_00106_DS_
;	;.line	17; "rec_job2.c"	return 0;
	CLRA	
	JMP	_00111_DS_
_00106_DS_:
;	;.line	18; "rec_job2.c"	if(++api_rampage==4)
	LDA	_api_rampage
	INCA	
	STA	_api_rampage
	XOR	#0x04
	JNZ	_00108_DS_
;	;.line	19; "rec_job2.c"	api_rampage=1;
	LDA	#0x01
	STA	_api_rampage
_00108_DS_:
;	;.line	20; "rec_job2.c"	if(!(api_ulaw&1))
	LDA	_api_ulaw
	SHR	
	JC	_00110_DS_
;	;.line	22; "rec_job2.c"	the_state->index=ADP_IND;
	LDA	#0x02
	ADD	_api_rec_job_no_write_STK00
	STA	r0x1159
	CLRA	
	ADDC	r0x1156
	STA	_ROMPH
	LDA	r0x1159
	STA	_ROMPL
	LDA	_ADP_IND
	STA	@_ROMPINC
;	;.line	23; "rec_job2.c"	the_state->predict=ADP_VPLH;
	LDA	r0x1156
	STA	_ROMPH
	LDA	_api_rec_job_no_write_STK00
	STA	_ROMPL
	LDA	_ADP_VPLH
	STA	@_ROMPINC
	LDA	(_ADP_VPLH + 1)
	STA	@_ROMPINC
_00110_DS_:
;	;.line	25; "rec_job2.c"	*pwr=PWRHL;
	LDA	_api_rec_job_no_write_STK01
	STA	_ROMPH
	LDA	_api_rec_job_no_write_STK02
	STA	_ROMPL
	LDA	_PWRHL
	STA	@_ROMPINC
	LDA	(_PWRHL + 1)
	STA	@_ROMPINC
;	;.line	26; "rec_job2.c"	return 1;
	LDA	#0x01
_00111_DS_:
;	;.line	27; "rec_job2.c"	}
	RET	
; exit point of _api_rec_job_no_write
	.ENDFUNC _api_rec_job_no_write
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_prepare$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_prepare_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_start_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_stop_alc$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_job_alc$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_set_vol$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_play_start$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_job$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_start1$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_beep_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_on$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_timer_off$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_stdby_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_enter_dsleep_mode$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_normal_sleep$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_clear_filter_mem$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_u2l_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_b12_page$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_prepare$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_amp_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_start$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_amp_rec_stop$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_DC_cancel$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$brk$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$IOR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IODIR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IO$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOWK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOWKDR$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TIMERC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$THRLD$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1L$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1H$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PTRCH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BEEPC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$FILTERG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ULAWC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$STACKH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DACON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SYSC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIOP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPI_BANK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_IND$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADP_VPH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ZC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ADCG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DAC_PH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PAG$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOMASK$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IOCNT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVDCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$LVRCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$OFFSETH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RCCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$BGCON$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$CRYPT$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$PWRHL$0$0({2}SI:U),E,0,0
	;--cdb--S:G$IROMDL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$IROMDLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$RECMUTE$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SPKC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DCLAMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIL$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIM$0$0({1}SC:U),E,0,0
	;--cdb--S:G$SSPIH$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0LH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1LH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP0INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$DMAHL$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$RAMP1INC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP1INC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMP$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPINC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ROMPLH$0$0({2}DG,SC:U),E,0,0
	;--cdb--S:G$ROMPINC2$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ACC$0$0({1}SC:U),E,0,0
	;--cdb--S:G$RAMP0UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$RAMP1UW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ROMPUW$0$0({2}SI:U),E,0,0
	;--cdb--S:G$SPIMH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$OFFSETLH$0$0({2}SI:U),E,0,0
	;--cdb--S:G$ADP_VPLH$0$0({2}SI:S),E,0,0
	;--cdb--S:G$ICE0$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE1$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE2$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE3$0$0({1}SC:U),E,0,0
	;--cdb--S:G$ICE4$0$0({1}SC:U),E,0,0
	;--cdb--S:G$TOV$0$0({1}SC:U),E,0,0
	;--cdb--S:G$init_io_state$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_rampage$0$0({1}SC:U),E,0,0
	;--cdb--S:G$api_endpage$0$0({2}SI:U),E,0,0
	;--cdb--S:G$api_ulaw$0$0({1}SC:U),E,0,0
	;--cdb--S:Lrec_job2.api_rec_job_no_write$pwr$65536$39({2}DG,SI:U),R,0,0,[_api_rec_job_no_write_STK02,_api_rec_job_no_write_STK01]
	;--cdb--S:Lrec_job2.api_rec_job_no_write$the_state$65536$39({2}DG,STadps:S),R,0,0,[_api_rec_job_no_write_STK00,r0x1156]
	;--cdb--S:Lrec_job2.api_rec_write_prev$pagenum$65536$42({1}SC:U),R,0,0,[r0x1163]
	;--cdb--S:Lrec_job2.api_rec_write_prev$page_to_write$65536$43({1}SC:U),R,0,0,[r0x1166]
	;--cdb--S:Lrec_job2.api_rec_job_do_write$endpage$65536$45({2}DG,SI:U),R,0,0,[_api_rec_job_do_write_STK02,_api_rec_job_do_write_STK01]
	;--cdb--S:Lrec_job2.api_rec_job_do_write$pwr$65536$45({2}DG,SI:U),R,0,0,[_api_rec_job_do_write_STK00,r0x116A]
	;--cdb--S:Lrec_job2.api_rec_job_do_write$halt_page$65536$46({1}SC:U),R,0,0,[r0x116D]
	;--cdb--S:Lrec_job2.api_play_start_with_state$the_state$65536$50({2}DG,STadps:S),R,0,0,[_api_play_start_with_state_STK08,_api_play_start_with_state_STK07]
	;--cdb--S:Lrec_job2.api_play_start_with_state$dacosr$65536$50({1}SC:U),R,0,0,[_api_play_start_with_state_STK06]
	;--cdb--S:Lrec_job2.api_play_start_with_state$ulaw$65536$50({1}SC:U),R,0,0,[_api_play_start_with_state_STK05]
	;--cdb--S:Lrec_job2.api_play_start_with_state$period$65536$50({2}SI:U),R,0,0,[_api_play_start_with_state_STK04,_api_play_start_with_state_STK03]
	;--cdb--S:Lrec_job2.api_play_start_with_state$end_page$65536$50({2}SI:U),R,0,0,[_api_play_start_with_state_STK02,_api_play_start_with_state_STK01]
	;--cdb--S:Lrec_job2.api_play_start_with_state$start_page$65536$50({2}SI:U),R,0,0,[_api_play_start_with_state_STK00,r0x117F]
	;--cdb--S:G$ms311_nextrecpage$0$0({4}DA4d,SC:U),D,0,0
	;--cdb--S:G$api_rec_job_no_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_rec_write_prev$0$0({2}DF,SV:S),C,0,0
	;--cdb--S:G$api_rec_job_do_write$0$0({2}DF,SC:U),C,0,0
	;--cdb--S:G$api_play_start_with_state$0$0({2}DF,SC:U),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	_api_clear_filter_mem
	.globl	_api_u2l_page
	.globl	_IOR
	.globl	_IODIR
	.globl	_IO
	.globl	_IOWK
	.globl	_IOWKDR
	.globl	_TIMERC
	.globl	_THRLD
	.globl	_RAMP0L
	.globl	_RAMP0H
	.globl	_RAMP1L
	.globl	_RAMP1H
	.globl	_PTRCL
	.globl	_PTRCH
	.globl	_ROMPL
	.globl	_ROMPH
	.globl	_BEEPC
	.globl	_FILTERG
	.globl	_ULAWC
	.globl	_STACKL
	.globl	_STACKH
	.globl	_ADCON
	.globl	_DACON
	.globl	_SYSC
	.globl	_SPIM
	.globl	_SPIH
	.globl	_SPIOP
	.globl	_SPI_BANK
	.globl	_ADP_IND
	.globl	_ADP_VPL
	.globl	_ADP_VPH
	.globl	_ADL
	.globl	_ADH
	.globl	_ZC
	.globl	_ADCG
	.globl	_DAC_PL
	.globl	_DAC_PH
	.globl	_PAG
	.globl	_DMAL
	.globl	_DMAH
	.globl	_SPIL
	.globl	_IOMASK
	.globl	_IOCMP
	.globl	_IOCNT
	.globl	_LVDCON
	.globl	_LVRCON
	.globl	_OFFSETL
	.globl	_OFFSETH
	.globl	_RCCON
	.globl	_BGCON
	.globl	_PWRL
	.globl	_CRYPT
	.globl	_PWRH
	.globl	_PWRHL
	.globl	_IROMDL
	.globl	_IROMDH
	.globl	_IROMDLH
	.globl	_RECMUTE
	.globl	_SPKC
	.globl	_DCLAMP
	.globl	_SSPIC
	.globl	_SSPIL
	.globl	_SSPIM
	.globl	_SSPIH
	.globl	_RAMP0
	.globl	_RAMP0LH
	.globl	_RAMP1LH
	.globl	_RAMP0INC
	.globl	_RAMP1
	.globl	_DMAHL
	.globl	_RAMP1INC
	.globl	_RAMP1INC2
	.globl	_ROMP
	.globl	_ROMPINC
	.globl	_ROMPLH
	.globl	_ROMPINC2
	.globl	_ACC
	.globl	_RAMP0UW
	.globl	_RAMP1UW
	.globl	_ROMPUW
	.globl	_SPIMH
	.globl	_OFFSETLH
	.globl	_ADP_VPLH
	.globl	_ICE0
	.globl	_ICE1
	.globl	_ICE2
	.globl	_ICE3
	.globl	_ICE4
	.globl	_TOV
	.globl	_init_io_state
	.globl	_api_rampage
	.globl	_api_endpage
	.globl	_api_ulaw
	.globl	_ms311_nextrecpage

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_api_play_start_with_state
	.globl	_api_rec_job_do_write
	.globl	_api_rec_write_prev
	.globl	_api_rec_job_no_write
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL_rec_job2_0	udata
r0x1156:	.ds	1
r0x1159:	.ds	1
r0x1163:	.ds	1
r0x1166:	.ds	1
r0x116A:	.ds	1
r0x116D:	.ds	1
r0x117F:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_api_rec_job_no_write_STK00:	.ds	1
	.globl _api_rec_job_no_write_STK00
_api_rec_job_no_write_STK01:	.ds	1
	.globl _api_rec_job_no_write_STK01
_api_rec_job_no_write_STK02:	.ds	1
	.globl _api_rec_job_no_write_STK02
_api_rec_job_do_write_STK00:	.ds	1
	.globl _api_rec_job_do_write_STK00
_api_rec_job_do_write_STK01:	.ds	1
	.globl _api_rec_job_do_write_STK01
_api_rec_job_do_write_STK02:	.ds	1
	.globl _api_rec_job_do_write_STK02
_api_play_start_with_state_STK00:	.ds	1
	.globl _api_play_start_with_state_STK00
_api_play_start_with_state_STK01:	.ds	1
	.globl _api_play_start_with_state_STK01
_api_play_start_with_state_STK02:	.ds	1
	.globl _api_play_start_with_state_STK02
_api_play_start_with_state_STK03:	.ds	1
	.globl _api_play_start_with_state_STK03
_api_play_start_with_state_STK04:	.ds	1
	.globl _api_play_start_with_state_STK04
_api_play_start_with_state_STK05:	.ds	1
	.globl _api_play_start_with_state_STK05
_api_play_start_with_state_STK06:	.ds	1
	.globl _api_play_start_with_state_STK06
_api_play_start_with_state_STK07:	.ds	1
	.globl _api_play_start_with_state_STK07
_api_play_start_with_state_STK08:	.ds	1
	.globl _api_play_start_with_state_STK08
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:r0x1189:NULL+0:-1:1
	;--cdb--W:r0x117F:NULL+0:-1:1
	;--cdb--W:_api_play_start_with_state_STK00:NULL+0:4493:0
	;--cdb--W:r0x1188:NULL+0:4479:0
	;--cdb--W:_api_play_start_with_state_STK00:NULL+0:-1:1
	;--cdb--W:r0x116D:NULL+0:-1:1
	;--cdb--W:r0x116E:NULL+0:-1:1
	;--cdb--W:r0x1170:NULL+0:-1:1
	;--cdb--W:r0x116E:NULL+0:4461:0
	;--cdb--W:r0x1163:NULL+0:-1:1
	;--cdb--W:r0x1164:NULL+0:-1:1
	;--cdb--W:r0x115A:NULL+0:-1:1
	end
