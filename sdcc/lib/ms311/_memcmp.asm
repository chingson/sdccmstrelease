;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.8.0 #b49c20b92 (Linux)
;--------------------------------------------------------
; Port for MSHINE CPU
;--------------------------------------------------------
;	;CCFROM:"/home/chingson/sdccofficial/sdcc/device/lib/ms311"
;;	.file	"_memcmp.c"
	.module _memcmp
	;.list	p=MS311
	.include "ms311sfr.def"
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--F:G$memcmp$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CODE (code,REL,CON) ; _memcmp-code 
.globl _memcmp

;--------------------------------------------------------
	.FUNC _memcmp:$PNUM 6:$L:r0x1155:$L:_memcmp_STK00:$L:_memcmp_STK01:$L:_memcmp_STK02:$L:_memcmp_STK03\
:$L:_memcmp_STK04:$L:r0x115A:$L:r0x115C:$L:r0x115D
;--------------------------------------------------------
;	.line	31; "_memcmp.c"	int memcmp (void * buf1, void * buf2, size_t count)
_memcmp:	;Function start
	STA	r0x1155
;	;.line	33; "_memcmp.c"	if (!count)
	LDA	_memcmp_STK04
	ORA	_memcmp_STK03
	JNZ	_00108_DS_
;	;.line	34; "_memcmp.c"	return(0);
	CLRA	
	STA	STK00
	JMP	_00111_DS_
_00108_DS_:
;	;.line	36; "_memcmp.c"	while ( --count && *((char *)buf1) == *((char *)buf2) ) {
	LDA	_memcmp_STK04
	DECA	
	STA	_memcmp_STK04
	LDA	#0xff
	ADDC	_memcmp_STK03
	STA	_memcmp_STK03
	ORA	_memcmp_STK04
	JZ	_00110_DS_
	LDA	_memcmp_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	r0x115C
	LDA	_memcmp_STK02
	STA	_ROMPL
	LDA	_memcmp_STK01
	STA	_ROMPH
	LDA	@_ROMPINC
	XOR	r0x115C
	JNZ	_00110_DS_
;	;.line	37; "_memcmp.c"	buf1 = (char *)buf1 + 1;
	LDA	_memcmp_STK00
	INCA	
	STA	r0x115A
	CLRA	
	ADDC	r0x1155
	STA	r0x1155
	LDA	r0x115A
	STA	_memcmp_STK00
;	;.line	38; "_memcmp.c"	buf2 = (char *)buf2 + 1;
	LDA	_memcmp_STK02
	INCA	
	STA	r0x115D
	CLRA	
	ADDC	_memcmp_STK01
	STA	_memcmp_STK01
	LDA	r0x115D
	STA	_memcmp_STK02
	JMP	_00108_DS_
_00110_DS_:
;	;.line	41; "_memcmp.c"	return( *((unsigned char *)buf1) - *((unsigned char *)buf2) );
	LDA	_memcmp_STK00
	STA	_ROMPL
	LDA	r0x1155
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_memcmp_STK00
	LDA	_memcmp_STK02
	STA	_ROMPL
	LDA	_memcmp_STK01
	STA	_ROMPH
	LDA	@_ROMPINC
	STA	_memcmp_STK02
	SETB	_C
	LDA	_memcmp_STK00
	SUBB	_memcmp_STK02
	STA	_memcmp_STK04
	CLRA	
	SUBB	#0x00
	STA	_memcmp_STK03
	LDA	_memcmp_STK04
	STA	STK00
	LDA	_memcmp_STK03
_00111_DS_:
;	;.line	42; "_memcmp.c"	}
	RET	
; exit point of _memcmp
	.ENDFUNC _memcmp
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$memcpy$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$memmove$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strcpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncpy$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strncat$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strncmp$0$0({2}DF,SI:S),C,0,0
	;--cdb--S:G$strxfrm$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$memchr$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strcspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strpbrk$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strrchr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strspn$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:G$strstr$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$strtok$0$0({2}DF,DG,SC:U),C,0,0
	;--cdb--S:G$memset$0$0({2}DF,DG,SV:S),C,0,0
	;--cdb--S:G$strlen$0$0({2}DF,SI:U),C,0,0
	;--cdb--S:L_memcmp.memcmp$count$65536$21({2}SI:U),R,0,0,[_memcmp_STK04,_memcmp_STK03]
	;--cdb--S:L_memcmp.memcmp$buf2$65536$21({2}DG,SV:S),R,0,0,[_memcmp_STK02,_memcmp_STK01]
	;--cdb--S:L_memcmp.memcmp$buf1$65536$21({2}DG,SV:S),R,0,0,[_memcmp_STK00,r0x1155]
	;--cdb--S:G$memcmp$0$0({2}DF,SI:S),C,0,0
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	;.globl PSAVE
	;.globl SSAVE
	.globl WSAVE
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	_memcmp
	.globl	_memcpy

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__memcmp_0	udata
r0x1155:	.ds	1
r0x115A:	.ds	1
r0x115C:	.ds	1
r0x115D:	.ds	1
	.area DSEG (DATA); (local stack unassigned) 
_memcmp_STK00:	.ds	1
	.globl _memcmp_STK00
_memcmp_STK01:	.ds	1
	.globl _memcmp_STK01
_memcmp_STK02:	.ds	1
	.globl _memcmp_STK02
_memcmp_STK03:	.ds	1
	.globl _memcmp_STK03
_memcmp_STK04:	.ds	1
	.globl _memcmp_STK04
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data - mirror
;--------------------------------------------------------
	;Following is optimization info, 
	;xxcdbxxW:dst:src+offset:srclit:just-remove
	;--cdb--W:_memcmp_STK00:NULL+0:4450:0
	;--cdb--W:_memcmp_STK03:NULL+0:4437:0
	;--cdb--W:_memcmp_STK04:NULL+0:4448:0
	;--cdb--W:r0x115B:NULL+0:4437:0
	;--cdb--W:r0x115A:NULL+0:4448:0
	;--cdb--W:r0x115A:NULL+0:4449:0
	;--cdb--W:r0x115A:NULL+0:0:0
	;--cdb--W:r0x115E:NULL+0:4449:0
	;--cdb--W:r0x115D:NULL+0:4450:0
	;--cdb--W:r0x115F:NULL+0:-1:1
	;--cdb--W:r0x115A:NULL+0:-1:1
	;--cdb--W:r0x115B:NULL+0:-1:1
	;--cdb--W:_memcmp_STK03:NULL+0:0:0
	;--cdb--W:r0x115E:NULL+0:-1:1
	;--cdb--W:_memcmp_STK03:NULL+0:-1:1
	end
