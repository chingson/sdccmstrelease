#ifndef __MS311SPHLIB_H__
#define __MS311SPHLIB_H__

#include "ms311.h"
#include "ms311typedef.h"

typedef struct adps{
	short predict;
	BYTE index; } 
	adpcm_state;
	

// recording functions
void api_rec_prepare(BYTE osr, BYTE opag, BYTE en5k);
void api_rec_prepare_alc(BYTE osr, BYTE opagmin, BYTE opagmax, BYTE nosatincseg, BYTE en5k);




// api_rec_start may have call back because it needs time
// to do DC cancellation
// at that time, it will call the callback function
// if callback return NON-ZERO, 
// recording will be terminated
// and return 0
// otherwise return 1
// that is, rec_start return 1 means ok
// return 0 means SPI is not touched
BYTE api_rec_start(BYTE dcdly, BYTE fg, BYTE ulaw, 
	USHORT page_start, USHORT page_end, BYTE (*callback)(void));
BYTE api_rec_start_alc(BYTE dcdly, BYTE fg, BYTE ulaw, 
	USHORT page_start, USHORT page_end, BYTE (*callback)(void));
	
// rec_stop need a parameter means if "add-end-code"
// required if need , set "add-endcode" to 1,
// if nothing is recorded yet, add_endcode must be set to 0
void api_rec_stop(BYTE add_endcode);
void api_rec_stop_alc(BYTE add_endcode);

// rec_job: return 0 means finish, return 1 means nothing update, return 2 means 
// a frame is updated
BYTE api_rec_job(void);
BYTE api_rec_job_alc(void);


// playing functions
void api_set_vol(BYTE pagv, BYTE fg);
// play_start: return 1 means ok, 0 means too short
BYTE api_play_start(USHORT start_page, USHORT end_page, 
	USHORT period,BYTE ulaw, BYTE dacosr);
// play_job: return 0 means finish, return 1 means no update, return 2 means
// a frame is updated
BYTE api_play_job(void);

void api_play_stop(void);


// begin write when loud
// when a fram is updated, it returns 1
// otherwise return 0
// if new frame, it will send back new index/predict (adpcm state)
// and the power (high byte) of last 256 byte (ram)
BYTE api_rec_job_no_write(adpcm_state *the_state, USHORT *pwr);

// do_write: if writeprev=1, it writes 1 previous page, 2 means write 2 prev page
// be careful about the application, because
// pwrh is not fifoed, and when erasing, cpu cannot operate
void api_rec_write_prev(BYTE pagenum);
BYTE api_rec_job_do_write(USHORT *pwr, USHORT *endpage);
BYTE api_play_start_with_state(USHORT start_page, USHORT end_page, 
	USHORT period,BYTE ulaw, BYTE dacosr, adpcm_state *the_state);


// beep function
void api_beep_start(BYTE bv);
void api_beep_start1(BYTE bv,BYTE pagv);

void api_beep_stop(void);

// timer function
void api_timer_on(BYTE reload);
void api_timer_off(void);

// timeroff means disable timer before stdby,
// but turns it back after wakeup 
void api_enter_stdby_mode(BYTE newiowk, BYTE newiowkdr, BYTE timeroff);
void api_enter_dsleep_mode(void); // deep sleep no need any parameters
void api_normal_sleep(BYTE newiowk, BYTE newiowkdr, BYTE lvrdis);
void api_clear_filter_mem(void);
void api_u2l_page(BYTE highpage);
void api_b12_page(BYTE highpage);
// new version change!!
BYTE api_amp_prepare(BYTE pagv, BYTE fgv, BYTE adcgv,BYTE en5k, BYTE mode, BYTE dcdly, BYTE(*callback)(void));
void api_amp_start(void);
void api_amp_stop(void);

void api_amp_rec_start(USHORT page_start, USHORT page_end);
//void api_amp_rec_job(void);
#define api_amp_rec_job api_rec_job
void api_amp_rec_stop(BYTE add_end_code);
BYTE api_DC_cancel(BYTE dcdly, BYTE(*callback)(void));




void brk(void);// break to ice control, for ice only


#define API_SPI_ERASE(x) { SPIH=x>>8;SPIM=x&0xff;SPIL=0;SPIOP=1;SPIOP=2;}
#define API_SPI_WRITE_PAGE(x,y) { SPIH=x>>8;SPIM=x&0xff;SPIL=0;SPIOP=1;\
		SPIOP=(y<<6)|4;}
		// read page need not wr-en!!
#define API_SPI_READ_PAGE(x,y) { SPIH=x>>8;SPIM=x&0xff;SPIL=0;	SPIOP=(y<<6)|8;}

#define API_USE_ERC {RCCON=(RCCON&0x98)|3;}
#define API_USE_IRC {RCCON=(RCCON&0x98)|5;}


// constants
#define API_EN5K_ON  0x10
#define API_EN5K_OFF 0x00
#define API_ADCG_LOUD 0xF2
#define API_ADCG_QUIET 0xC0
#define API_REC_FG_DEFAULT 0xd0
#define API_REC_FG_QUIET 0xC0

#define API_PAGV_DEFAULT 0x3F
#define API_ULAWEN 1
#define API_ULAWOFF 0
#define API_PLAYG_DEFAULT 0x78

#define API_AD_OSR128 0x80
#define API_AD_OSR64 0x00

#define API_DAOSR_HIGH 0x04
#define API_DAOSR_LOW 0x00

#define API_AMP_MODE_SPECIAL 1
#define API_AMP_MODE_ULAW 2



#ifndef NULL
#define NULL (void*)0
#endif
// special macro need new compiler support
//
#define API_PSTARTH(x) api_play_start(x ## _STARTPAGE, x ## _ENDPAGE, x ## _PERIOD,x ## _ULAW , API_DAOSR_HIGH)
#define API_PSTARTL(x) api_play_start(x ## _STARTPAGE, x ## _ENDPAGE, x ## _PERIOD,x ## _ULAW , API_DAOSR_LOW)

#define API_PSTARTH_NOSAT(x) api_play_start(x ## _STARTPAGE, x ## _ENDPAGE, x ## _PERIOD,x ## _ULAW|0x80 , API_DAOSR_HIGH)
#define API_PSTARTL_NOSAT(x) api_play_start(x ## _STARTPAGE, x ## _ENDPAGE, x ## _PERIOD,x ## _ULAW|0x80 , API_DAOSR_LOW)

#endif
