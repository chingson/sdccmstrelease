#ifndef __MS311_SFR_H__
#define __MS311_SFR_H__
extern volatile unsigned char IOR  	;
extern volatile unsigned char IODIR	;
extern volatile unsigned char IO	;
extern volatile unsigned char IOWK	;
extern volatile unsigned char IOWKDR ;
extern volatile unsigned char TIMERC ;
extern volatile unsigned char THRLD	;
extern volatile unsigned char RAMP0L ;
extern volatile unsigned char RAMP0H ;
extern volatile unsigned char RAMP1L ;
extern volatile unsigned char RAMP1H ;
extern volatile unsigned char PTRCL	;
extern volatile unsigned char PTRCH	;
extern volatile unsigned char ROMPL 	;
extern volatile unsigned char ROMPH 	;
extern volatile unsigned char BEEPC 	;
extern volatile unsigned char FILTERG 	;
extern volatile unsigned char ULAWC 	;
extern volatile unsigned char STACKL ;
extern volatile unsigned char STACKH ;
extern volatile unsigned char ADCON	;
extern volatile unsigned char DACON  ;
extern volatile unsigned char SYSC 	;
extern volatile unsigned char SPIM	;
extern volatile unsigned char SPIH	;
extern volatile unsigned char SPIOP	;
extern volatile unsigned char SPI_BANK;
extern volatile unsigned char ADP_IND;
extern volatile unsigned char ADP_VPL;
extern volatile unsigned char ADP_VPH;
extern volatile unsigned char ADL	;
extern volatile unsigned char ADH	;
extern volatile unsigned char ZC	;
extern volatile unsigned char ADCG	;
extern volatile unsigned char DAC_PL	;
extern volatile unsigned char DAC_PH ;
extern volatile unsigned char PAG	;
extern volatile unsigned char DMAL	;
extern volatile unsigned char DMAH	;
extern volatile unsigned char SPIL	;
extern volatile unsigned char IOMASK ;
extern volatile unsigned char IOCMP  ;
extern volatile unsigned char IOCNT  ;
extern volatile unsigned char LVDCON ;
extern volatile unsigned char LVRCON ;
extern volatile unsigned char OFFSETL;
extern volatile unsigned char OFFSETH;
extern volatile unsigned char RCCON  ;
extern volatile unsigned char BGCON  ;
extern volatile unsigned char PWRL	;
extern volatile unsigned char CRYPT  ;
extern volatile unsigned char PWRH	;
extern volatile unsigned short PWRHL;
extern volatile unsigned char IROMDL ;
extern volatile unsigned char IROMDH ;
extern volatile unsigned short IROMDLH ;
extern volatile unsigned char RECMUTE;
extern volatile unsigned char SPKC;
extern volatile unsigned char DCLAMP;
//; fake registers below;
extern volatile unsigned char  RAMP0	;
extern unsigned char *RAMP0LH;
extern unsigned char *RAMP1LH;
extern volatile unsigned char RAMP0INC;
extern volatile unsigned char  RAMP1  ;
extern volatile unsigned char * DMAHL  ;
extern volatile unsigned char RAMP1INC;
extern volatile unsigned short RAMP1INC2;

extern volatile unsigned char ROMP	;
extern volatile unsigned char ROMPINC;
extern volatile unsigned char * ROMPLH;
extern volatile unsigned short ROMPINC2;
extern volatile unsigned char ACC	;
extern volatile unsigned short RAMP0UW;
extern volatile unsigned short RAMP1UW;
extern volatile unsigned short ROMPUW;

extern volatile unsigned short SPIMH;


extern volatile unsigned short OFFSETLH;
// ok, this one is signed
extern volatile signed short ADP_VPLH;

extern volatile unsigned char ICE0;
extern volatile unsigned char ICE1;
extern volatile unsigned char ICE2;
extern volatile unsigned char ICE3;
extern volatile unsigned char ICE4;


// special bit has only timer overflow
extern volatile unsigned char TOV;
extern unsigned char _PARA_STK;
extern unsigned char init_io_state;
#endif
