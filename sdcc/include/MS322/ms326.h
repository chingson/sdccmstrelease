#ifndef __MS326_SFR_H__
#define __MS326_SFR_H__
extern volatile unsigned char PAR  	;
extern volatile unsigned char PADIR	;
extern volatile unsigned char PIOA	;
extern volatile unsigned char PAWK	;
extern volatile unsigned char PAWKDR ;
extern volatile unsigned char TIMERC ;
extern volatile unsigned char THRLD	;
extern volatile unsigned char RAMP0L ;
extern volatile unsigned char RAMP0H ;
extern volatile unsigned char RAMP1L ;
extern volatile unsigned char RAMP1H ;
extern volatile unsigned char PTRCL	;
extern volatile unsigned char PTRCH	;
extern volatile unsigned char ROMPL 	;
extern volatile unsigned char ROMPH 	;
extern volatile unsigned char BEEPC 	;
extern volatile unsigned char FILTERGR 	;
extern volatile unsigned char ULAWC 	;
extern volatile unsigned char STACKL ;
extern volatile unsigned char STACKH ;
extern volatile unsigned char ADCON	;
extern volatile unsigned char DACON  ;
extern volatile unsigned char SYSC 	;
extern volatile unsigned char SPIM	;
extern volatile unsigned char SPIH	;
extern volatile unsigned short SPIMH;
extern volatile unsigned char SPIOP	;
extern volatile unsigned char SPI_BANK;
extern volatile unsigned char ADP_IND;
extern volatile unsigned char ADP_VPL;
extern volatile unsigned char ADP_VPH;
extern volatile unsigned char ADL	;
extern volatile unsigned char ADH	;
extern volatile unsigned char ZC	;
extern volatile unsigned char ADCG	;
extern volatile unsigned char DAC_PL	;
extern volatile unsigned char DAC_PH ;
extern volatile unsigned char PAG	;
extern volatile unsigned char RDMAL	;
extern volatile unsigned char RDMAH	;
extern volatile unsigned char SPIL	;
extern volatile unsigned char IOMASK ;
extern volatile unsigned char IOCMP  ;
extern volatile unsigned char IOCNT  ;
extern volatile unsigned char LVDCON ;
extern volatile unsigned char LVDCTH ;
extern volatile unsigned char LVRCON ;
extern volatile unsigned char OFFSETL;
extern volatile unsigned char OFFSETH;
extern volatile unsigned char RCCON  ;
extern volatile unsigned char BGCON  ;
extern volatile unsigned char PWRL	;
extern volatile unsigned char CRYPT  ;
extern volatile unsigned char PWRH	;
extern volatile unsigned short PWRHL;
extern volatile unsigned char IROMDL ;
extern volatile unsigned char IROMDH ;
extern volatile unsigned char RECMUTE;
extern volatile unsigned char SPKC;
extern volatile unsigned char DCLAMP;

extern volatile unsigned char SSPIC;
extern volatile unsigned char SSPIL;
extern volatile unsigned char SSPIM;
extern volatile unsigned char SSPIH;
extern volatile unsigned char PBR;
extern volatile unsigned char PBDIR;
extern volatile unsigned char PIOB;
extern volatile unsigned char PBWK;
extern volatile unsigned char PBWKDR;
extern volatile unsigned char PAIE;
extern volatile unsigned char PBIE;
extern volatile unsigned char PAIF;
extern volatile unsigned char PBIF;
extern volatile unsigned char GIE;
extern volatile unsigned char GIF;
extern volatile unsigned char WDTL;
extern volatile unsigned char WDTH;
extern volatile unsigned char RPAGES;
extern volatile unsigned char PPAGES;
extern volatile unsigned char DMA_IL;
extern volatile unsigned char FILTERGP;
extern volatile unsigned char SPIDAT;
extern volatile unsigned char RSPIC;
extern volatile unsigned char RCLKDIV;
extern volatile unsigned char PCR;
extern volatile unsigned char PCDIR;
extern volatile unsigned char PIOC;
extern volatile unsigned char CMPCON;
extern volatile unsigned char INTVC;
extern volatile unsigned char INTV;
extern volatile unsigned char DMICON;
extern volatile unsigned char PRG_RAM;

extern volatile unsigned char PDMAL;
extern volatile unsigned char PDMAH;
extern volatile unsigned short PDMALH;
extern volatile unsigned char SPIDMAC;
extern volatile unsigned char SDMAAL;
extern volatile unsigned char SDMAAH;
extern volatile unsigned short SDMAALH;
extern volatile unsigned char ECRAL;
extern volatile unsigned char ECRAH;
extern volatile unsigned short ECRALH;
extern volatile unsigned char ECOAL;
extern volatile unsigned char ECOAH;
extern volatile unsigned short ECOALH;
extern volatile unsigned char ECLEN;
extern volatile unsigned char ECCON;
extern volatile unsigned char ECMODE;

extern volatile unsigned char SPIOPRAH;
extern volatile unsigned short SDMALEN;
extern volatile unsigned short MULA;
extern volatile unsigned char MULAL;
extern volatile unsigned char MULAH;
extern volatile unsigned short MULB;
extern volatile unsigned char MULBL;
extern volatile unsigned char MULBH;
extern volatile unsigned long MULO;
extern volatile unsigned short MULO1;
extern volatile unsigned char MULSHIFT;
extern volatile signed short L2UBUF;
extern volatile unsigned char L2UBUFL;
extern volatile unsigned char L2UBUFH;
extern volatile unsigned char ULAWD;
extern volatile unsigned char L2USH;
extern volatile unsigned char U2LSH;
extern volatile unsigned char INTPRI;

extern volatile unsigned char *LPWMRA;
extern volatile unsigned char LPWMRAL;
extern volatile unsigned char LPWMRAH;
extern volatile unsigned char LPWMEN;
extern volatile unsigned char LPWMINV;
extern volatile unsigned char SPICK;
extern volatile unsigned char SYSC2;

extern volatile unsigned char HWPSEL;
extern volatile unsigned char HWPAL;
extern volatile unsigned char HWPAH;
extern volatile unsigned short  HWPA;
extern volatile unsigned char HWD;
extern volatile unsigned char HWDINC;

extern volatile unsigned char PATEN;
extern volatile unsigned char PBTEN;
extern volatile unsigned short PABTEN; // compiler fake register
extern volatile unsigned char TRAMAL;
extern volatile unsigned char TRAMAH;
extern volatile unsigned char *TRAMBUFP; // assume short..?
extern volatile unsigned char PASKIP;
extern volatile unsigned char PBSKIP;
extern volatile unsigned short PABSKIP;
extern volatile unsigned char PATR;
extern volatile unsigned char PBTR;
extern volatile unsigned short PABTR;
extern volatile unsigned char TOUCHC;
extern volatile unsigned char DUMMYC;
extern volatile unsigned char IRCD;
extern volatile unsigned char FPWMEN;
extern volatile unsigned char FPWMDUTY;
extern volatile unsigned char FPWMPER;
extern volatile unsigned char DACGCL;

extern volatile unsigned long RECPWR; 

extern volatile unsigned char ICE0;
extern volatile unsigned char ICE1;
extern volatile unsigned char ICE2;
extern volatile unsigned char ICE3;
extern volatile unsigned char ICE4;



//; fake registers below;
extern volatile unsigned char  RAMP0	;
extern volatile unsigned char RAMP0INC;
extern volatile unsigned char  RAMP1  ;
extern volatile unsigned char * RDMALH  ;
extern volatile unsigned char * HWPALH;
extern volatile unsigned char RAMP1INC;
extern volatile unsigned short RAMP1INC2;

extern volatile unsigned char ROMP	;
extern volatile unsigned char ROMP	;
extern volatile unsigned char ROMPINC;
extern volatile unsigned short ROMPINC2;
extern volatile unsigned char ACC	;
extern volatile unsigned char * RAMP0UW;
extern volatile unsigned short RAMP1UW;
extern  unsigned char volatile * ROMPLH;
extern volatile unsigned short ROMPUW;


extern volatile unsigned short OFFSETLH;
// ok, this one is signed
extern volatile signed short ADP_VPLH;


// special bit has only timer overflow
extern volatile unsigned char TOV;
extern volatile unsigned char EA;
extern volatile unsigned char OV;


#define SYSC_BIT_SLEEP 0x01
#define SYSC_BIT_SKCMD 0x02
#define SYSC_BIT_LPASS 0x04
#define SYSC_BIT_PINRSTEN 0x08
#define SYSC_BIT_STDBY 0x80
#define SYSC_BIT_DSLP 0x40
#define SYSC_BITS_ZSEL 0x18


#define SYSC2_BIT_PWRSEL 0x1 // set this bit pwrhl is for playing
#define SYSC2_BIT_BZCKSLOW 0x2 // set this bit clock slow when wait busy
#define SYSC2_BIT_CS2C 0x4 // set ths bit CS high has 2 FOSC clock
#define SYSC2_BIT_DMAI7 0x8 // set tis bit wakeup will match 7bit dma address
#define SYSC2_BIT_MBIAS 0x80

#endif
