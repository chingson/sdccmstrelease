#ifndef __MS326SPHLIB_H__
#define __MS326SPHLIB_H__

#include <ms326.h>
#include <ms326typedef.h>

#ifndef __SDCC
#define __code
#define __data
#define __xdata
#define __interrupt
#define __critical
#endif

typedef struct adps{
	short predict;
	BYTE index; } 
	adpcm_state;

typedef struct pwmleds {
	BYTE period;
	BYTE counter;
	BYTE threshold;
} ledpwm_entry;


/*
case (ram_offset[2:0])
	   0: offt<=data_from_ram[3:0];
	   1: nmosn<=data_from_ram[3:0];
	   2: period[7:0] <=data_from_ram;
	   3: period[`TMSB:8]<=data_from_ram[`TMSB-8:0];
	   4: threshold[7:0] <=data_from_ram;

*/


typedef struct touchen {
	BYTE toff;
	BYTE nmossw;
	USHORT period;
	USHORT threshold;
	USHORT count;
}touch_entry;

// recording functions
// rec fifo is the fifo address
// if SPI recording, this address must be 256 byte aliged
// FIFOLEN unit is in 16 byte

// rate 0/1: set rclkdiv for ice at default, no change rate
// rate 2/3: clear/set ADCON.6, no change rclkdiv, ice will be incompatible 
void api_rec_prepare(BYTE rate, BYTE opag, BYTE fg, BYTE en5k, BYTE spkcv, BYTE* recfifo, BYTE fifolen);
void api_rec_prepare_pre_erase(BYTE rate, BYTE opag, BYTE fg, 
BYTE en5k, BYTE spkcv, BYTE *fifostart, BYTE fifolen, USHORT start_spi_page, USHORT final_spi_page
,BYTE (*callback)(void));


BYTE api_rec_start_no_erase( BYTE recmode, USHORT page_start, USHORT page_end );


void api_rec_stop_noerase(BYTE add_end_code);

// api_rec_start may have call back because it needs time
// to do DC cancellation
// at that time, it will call the callback function
// if callback return NON-ZERO, 
// recording will be terminated
// and return 0
// otherwise return 1
// that is, rec_start return 1 means ok
// return 0 means SPI is not touched
BYTE api_rec_start(BYTE recmode, 
	USHORT page_start, USHORT page_end, BYTE (*callback) (void));
	
// rec_stop need a parameter means if "add-end-code"
// required if need , set "add-endcode" to 1,
// if nothing is recorded yet, add_endcode must be set to 0
void api_rec_stop(BYTE add_endcode);
// rec_job: return 0 means finish, return 1 means nothing update, return 2 means 
// a frame is updated
BYTE api_rec_job(void);
BYTE api_rec_job_noer(void);


// playing functions
void api_set_vol(BYTE pagv, BYTE fg);
// play_start: return 1 means ok, 0 means too short
BYTE api_play_start(USHORT start_page, USHORT end_page, USHORT period, 
BYTE ulaw, BYTE dacosr, BYTE dacgcl, BYTE *fifo);// use 256 bytes fixed

// play_job: return 0 means finish, return 1 means no update, return 2 means
// a frame is updated
BYTE api_play_job(void);

void api_play_stop(void);


// begin write when loud
// when a fram is updated, it returns 1
// otherwise return 0
// if new frame, it will send back new index/predict (adpcm state)
// and the power (high byte) of last 256 byte (ram)
BYTE api_rec_job_no_write(adpcm_state *the_state, USHORT *pwr);

// do_write: if writeprev=1, it writes 1 previous page, 2 means write 2 prev page
// be careful about the application, because
// pwrh is not fifoed, and when erasing, cpu cannot operate
void api_rec_write_prev(BYTE pagenum);
BYTE api_rec_job_do_write(USHORT *pwr, USHORT *endpage);
BYTE api_play_start_with_state(USHORT start_page, USHORT end_page, 
	USHORT period,BYTE ulaw, BYTE dacosr, adpcm_state *the_state);


// beep function
void api_beep_start(BYTE bv, USHORT period);
void api_beep_start1(BYTE bv,USHORT period, BYTE pagv);
//************************
// 2021
// use ULAW to play BEEP waveform in RAM
void api_beep_start2(BYTE bv,USHORT perioddac, BYTE periodwav, BYTE pagv, USHORT buframa);
// **********************

void api_beep_stop(void);

// timer function
void api_timer_on(BYTE reload);
void api_timer_off(void);

// timeroff means disable timer before stdby,
// but turns it back after wakeup 
//BYTE DC_cancel(BYTE dcdly, BYTE (*callback)(void), BYTE* fifop);
//BYTE DC_cancel_noerase(BYTE dcdly, BYTE (*callback)(void), BYTE* fifop);


void api_enter_stdby_mode(BYTE newpawk, BYTE newpawkdr, BYTE newpbwk, BYTE newpbwkdr, BYTE timeroff);
void api_enter_dsleep_mode(void); // dsleep mode can wake up by PA67/PB0..5 only
void api_normal_sleep(BYTE newpawk, BYTE newpawkdr, BYTE newpbwk, BYTE newpbwkdr, BYTE lvrdis, BYTE wkrst);

// filter memory at 0x780~0x7ff, highlow=0 means 780~7ff, =1 means 700~77f
void api_clear_filter_mem(BYTE highlow);

void api_amp_prepare(BYTE pagv, BYTE fgpv,BYTE fgrv, BYTE dacgcl, BYTE adcgv,BYTE en5k,BYTE spkcv, BYTE *fifo, BYTE fifolen);
void api_amp_start(BYTE use_PA7);
void api_amp_stop(void);
void brk(void);// break to ice control, for ice only
void api_chspick(BYTE clk);


// touch key
USHORT api_tk_init(USHORT ch_en, BYTE mon, BYTE totalch, BYTE *bufp, USHORT *procbufp, unsigned long *baseline, BYTE *th_ratio, BYTE  (*callback)(void));
void api_tk_stop(void);
BYTE api_tk_job(void);


#define API_SPI_ERASE(x) { SPIH=x>>8;SPIM=x&0xff;SPIL=0;SPIOP=1;SPIOP=0x02; }

#define API_SPI_WRITE_PAGE(x,y) { SPIH=x>>8;SPIM=x&0xff;SPIL=0;SPIOP=1;\
		SPIOPRAH=y<<1; SPIOP=4;}
		// read page need not wr-en!!
#define API_SPI_READ_PAGE(x,y) { SPIH=x>>8;SPIM=x&0xff;SPIL=0;	SPIOPRAH=y<<1;SPIOP=8;}

// read half page y uint is 128
#define API_SPI_READ_HALFPAGE(x,y) { SPIH=x>>8;SPIM=x&0xff;SPIL=0;	SPIOPRAH=y;SPIOP=0x48;}


#define API_USE_ERC { WDTL=4;IRCD=0x10;RCCON=0x3;}
#define API_USE_IRC { WDTL=4;IRCD=0x10;RCCON=0x5;}
#define API_USE_XTL { WDTL=4;RCCON=0x11;}
#define API_USE_EOSC { WDTL=4; RCCON=0x9;} // use 9!!


// constants
#define API_EN5K_ON  0x10
#define API_EN5K_OFF 0x00
#define API_ADCG_LOUD 0xF2
#define API_ADCG_QUIET 0xC0
#define API_REC_FG_DEFAULT 0xd0
#define API_REC_FG_QUIET 0xC0

#define API_PAGV_DEFAULT 0x7F
#define api_modeEN 1
#define api_modeOFF 0
#define API_PLAYG_DEFAULT 0x78


#define API_DAOSR_HIGH 0x04
#define API_DAOSR_LOW 0x00

#define API_RECMODE_ULAW 1
#define API_RECMODE_ERASE_SUSP 2
//#define API_RECMODE_ALC 4
#define API_RECMODE_16B 8
// trying digital mic
#define API_RECMODE_DMI 0x10 

#define API_PLAYMODE_ULAW 1
#define API_PLAYMODE_12BIT 2
#define API_PLAYMODE_NOF 8
#define API_PLAYMODE_PA7 0x10
#define API_PLAYMODE_REPEAT 0x20

#define API_ADC_OSR128 1
//#define API_ADC_OSR64 0
#define API_ADC_OSR256 2

#define API_DACGCL_OFF 0
#define API_DACGCL_NORMAL 1
#define API_DACGCL_HIGH 3

#define TOUCHC_DEFAULT 0x49


#ifndef NULL
#define NULL (void*)0
#endif
// special macro need new compiler support
//
#define API_PSTARTH(x) api_play_start(x ## _STARTPAGE, x ## _ENDPAGE, x ## _PERIOD,x ## _ULAW , API_DAOSR_HIGH, 3, (unsigned char *)0x8680)
#define API_PSTARTL(x) api_play_start(x ## _STARTPAGE, x ## _ENDPAGE, x ## _PERIOD,x ## _ULAW , API_DAOSR_LOW, 3, (unsigned char*)0x8680)


#define API_PSTARTH_NOF(x) api_play_start(x ## _STARTPAGE, x ## _ENDPAGE, x ## _PERIOD,x ## _ULAW|API_PLAYMODE_NOF, API_DAOSR_HIGH, 3, (unsigned char*)0x8680)
#define API_PSTARTH_PA7(x) api_play_start(x ## _STARTPAGE, x ## _ENDPAGE, x ## _PERIOD,x ## _ULAW|API_PLAYMODE_PA7, API_DAOSR_HIGH, 3, (unsigned char*)0x8680)



//2021 add multi-sector req


#endif
