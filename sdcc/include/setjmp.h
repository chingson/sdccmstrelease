/*-------------------------------------------------------------------------
   setjmp.h - header file for setjmp & longjmp ANSI routines

   Copyright (C) 1999, Sandeep Dutta . sandeep.dutta@usa.net

   This library is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any
   later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this library; see the file COPYING. If not, write to the
   Free Software Foundation, 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.

   As a special exception, if you link this library with other files,
   some of which are compiled with SDCC, to produce an executable,
   this library does not by itself cause the resulting executable to
   be covered by the GNU General Public License. This exception does
   not however invalidate any other reasons why the executable file
   might be covered by the GNU General Public License.
-------------------------------------------------------------------------*/

#ifndef __SDCC_SETJMP_H
#define __SDCC_SETJMP_H
// we need 8 byte, pc address 2 byte, old stack 2 byte, sp 2 byte fp 2 byte
typedef unsigned char jmp_buf[8]; /* 2 for the stack pointer, 2 for the return address, 2 for the frame pointer. */
extern volatile unsigned char STACKL;
extern volatile unsigned char STACKH;

int __setjmp (jmp_buf, unsigned char, unsigned char);
#define setjmp(jump_buf)  __setjmp(jump_buf, STACKL, STACKH)

_Noreturn void longjmp(jmp_buf, int);


#endif

